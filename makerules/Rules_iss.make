# Copyright Texas Instruments

# Default build environment, windows or linux
ifeq ($(OS), )
  OS := Linux
endif

IMGS_ID := IMGS_MICRON_MT9J003

PLATFORM := ti814x-evm

# Configure to build Iss Examples since conflict of Utils with RDK
ISS_EXAMPLES_DIR := YES

#CODEGEN_PATH_M3   := $(TOOLS_INSTALL_DIR)/cgt470_4_9_2
#iss_PATH          := $(ISS_TOOL_DIR)/$(ISS_REL_TAG)
iss_algo_PATH      := $(iss_PATH)/packages/ti/psp/iss/alg
#edma3lld_PATH     := $(TOOLS_INSTALL_DIR)/edma3_lld_02_11_02_04
#fc_PATH           := $(TOOLS_INSTALL_DIR)/framework_components_3_21_02_32
#bios_PATH         := $(TOOLS_INSTALL_DIR)/sysbios_6_32_05_54
#xdc_PATH          := $(TOOLS_INSTALL_DIR)/xdctools_3_22_04_46
#ipc_PATH 	       := $(TOOLS_INSTALL_DIR)/ipc_1_23_05_40
#syslink_PATH      := $(TOOLS_INSTALL_DIR)/syslink_2_00_04_83
#hdvpss_PATH       := $(TOOLS_INSTALL_DIR)/hdvpss_01_00_01_36
#xdais_PATH        := $(TOOLS_INSTALL_DIR)/xdais_7_21_01_07
TIMMOSAL_PATH      := $(iss_PATH)/packages/ti/psp/iss/timmosal

ISS_INSTALL_DIR  := $(iss_PATH)

ROOTDIR := $(iss_PATH)

ifeq ($(CORE), )
  CORE := m3vpss
endif

# Default platform
ifeq ($(PLATFORM), )
  PLATFORM := ti814x-evm
endif

# Default profile
ifeq ($(PROFILE_$(CORE)), )
  #PROFILE_$(CORE) := debug
  #PROFILE_$(CORE)	:= whole_program_debug
  PROFILE_$(CORE) := release
endif

XDCPATH = $(bios_PATH)/packages;$(xdc_PATH)/packages;$(ipc_PATH)/packages;$(iss_PATH)/packages;$(hdvpss_PATH)/packages;$(xdais_PATH)/packages;$(edma3lld_PATH)/packages;

# Default klockwork build flag
ifeq ($(KW_BUILD), )
  KW_BUILD := no
endif

export OS
export PLATFORM
export CORE
export PROFILE_$(CORE)
export CODEGEN_PATH_M3
export bios_PATH
export edma3lld_PATH
export xdc_PATH
export iss_PATH
export iss_algo_PATH
export ipc_PATH
export hdvpss_PATH
export ROOTDIR
export XDCPATH
export xdais_PATH
export KW_BUILD
export IMGS_ID
export TIMMOSAL_PATH

include $(ROOTDIR)/makerules/build_config.mk
include $(ROOTDIR)/makerules/env.mk
include $(ROOTDIR)/makerules/platform.mk
include $(iss_PATH)/component.mk
