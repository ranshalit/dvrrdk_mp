CPCMD=cp -r
CPGIT=rsync -Cav --include=core
copy_ti_tools_common:
	-mkdir -p $(dvr_rdk_BASE)/target
	-mkdir -p $(dvr_rdk_BASE)/tftphome
	-mkdir -p $(dvr_rdk_BASE)/pre_built_binary
	-mkdir -p $(dvr_rdk_BASE)/pre_built_binary/ti816x_evm
	-mkdir -p $(dvr_rdk_BASE)/pre_built_binary/ti816x_dvr
	-mkdir -p $(dvr_rdk_BASE)/pre_built_binary/ti814x_evm
	-mkdir -p $(dvr_rdk_BASE)/pre_built_binary/ti814x_dvr
	-mkdir -p $(dvr_rdk_BASE)/pre_built_binary/ti810x_evm
	-mkdir -p $(dvr_rdk_BASE)/pre_built_binary/ti810x_dvr
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/bios
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/syslink
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/xdc
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/ipc
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/framework_components
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/xdais
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/codecs
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/ivahd_hdvicp
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/linux_devkit
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/linux_lsp
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/cgt_m3
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/cgt_dsp
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/cgt_a8/arago
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/edma3lld
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/rpe
	${CPCMD} $(bios_PATH) $(dvr_rdk_BASE)/ti_tools/bios/
	${CPCMD} $(xdc_PATH) $(dvr_rdk_BASE)/ti_tools/xdc/
	${CPCMD} $(ipc_PATH) $(dvr_rdk_BASE)/ti_tools/ipc/
	${CPCMD} $(fc_PATH) $(dvr_rdk_BASE)/ti_tools/framework_components/
	${CPCMD} $(xdais_PATH) $(dvr_rdk_BASE)/ti_tools/xdais/
	${CPCMD} $(h264dec_DIR) $(dvr_rdk_BASE)/ti_tools/codecs/
	${CPCMD} $(mpeg4dec_DIR) $(dvr_rdk_BASE)/ti_tools/codecs/
	${CPCMD} $(mpeg4enc_DIR) $(dvr_rdk_BASE)/ti_tools/codecs/
	${CPCMD} $(jpegdec_DIR) $(dvr_rdk_BASE)/ti_tools/codecs/
	${CPCMD} $(h264enc_DIR) $(dvr_rdk_BASE)/ti_tools/codecs/
	${CPCMD} $(jpegenc_DIR) $(dvr_rdk_BASE)/ti_tools/codecs/
	${CPCMD} $(mpeg2dec_DIR) $(dvr_rdk_BASE)/ti_tools/codecs/
	${CPCMD} $(hdvicplib_PATH) $(dvr_rdk_BASE)/ti_tools/ivahd_hdvicp/
	${CPCMD} $(CODEGEN_PATH_M3) $(dvr_rdk_BASE)/ti_tools/cgt_m3/
	${CPCMD} $(CODEGEN_PATH_DSP) $(dvr_rdk_BASE)/ti_tools/cgt_dsp/
	${CPCMD} $(CODEGEN_PATH_A8) $(dvr_rdk_BASE)/ti_tools/cgt_a8/arago/
	${CPCMD} $(edma3lld_PATH) $(dvr_rdk_BASE)/ti_tools/edma3lld/
	${CPCMD} $(rpe_PATH) $(dvr_rdk_BASE)/ti_tools/rpe
	${CPCMD} $(aaclcdec_PATH) $(dvr_rdk_BASE)/ti_tools/codecs/
	${CPCMD} $(aaclcenc_PATH) $(dvr_rdk_BASE)/ti_tools/codecs/
	${CPCMD} $(mtdutils_PATH) $(dvr_rdk_BASE)/ti_tools/
	${CPCMD} $(watermark_lib_PATH) $(dvr_rdk_BASE)/ti_tools/codecs/
	rm -rf $(dvr_rdk_BASE)/dvr_rdk/internal_docs
	rm -rf $(dvr_rdk_BASE)/dvr_rdk/.??*
# Remove dvrapp, module later since dvrapp is also part of release now
	# rm -rf $(dvr_rdk_BASE)/dvr_rdk/dvrapp
# 	rm -rf $(dvr_rdk_BASE)/dvr_rdk/module
	rm -rf $(hdvpss_PATH)/.??*
	mv $(dvr_rdk_BASE)/dvr_rdk/bin/samplelogo_640x480.bmp $(dvr_rdk_BASE)/tftphome
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/linux_lsp/kernel
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/linux_lsp/uboot
	-mkdir -p $(dvr_rdk_BASE)/ti_tools/linux_lsp/collaterals
	${CPGIT} $(KERNELDIR) $(dvr_rdk_BASE)/ti_tools/linux_lsp/kernel/
	${CPGIT} $(UBOOTDIR) $(dvr_rdk_BASE)/ti_tools/linux_lsp/uboot/
	${CPCMD} $(LSP_COLLATERALS) $(dvr_rdk_BASE)/ti_tools/linux_lsp/collaterals
	${CPGIT} $(syslink_PATH) $(dvr_rdk_BASE)/ti_tools/syslink/
	${CPCMD} $(DEV_COLLATERALS) $(dvr_rdk_BASE)/ti_tools/
	mv $(dvr_rdk_BASE)/dvr_rdk/Rules.make.release $(dvr_rdk_BASE)/dvr_rdk/Rules.make

#for binary base release, it'll strip all the unnecessary stuff in the ti_tools	
TMP_TOOLS=../tmp
DEL_POOL=../del_pool
strip_clean_ti_tools:
	mkdir -p $(DEL_POOL) 
	mv $(TI_SW_ROOT)/xdc $(DEL_POOL)
	mv $(TI_SW_ROOT)/bios $(DEL_POOL)
	mv $(TI_SW_ROOT)/cgt_m3 $(DEL_POOL)
	mv $(TI_SW_ROOT)/framework_components $(DEL_POOL)
	mv $(TI_SW_ROOT)/hdvpss $(DEL_POOL)
	mv $(TI_SW_ROOT)/cgt_dsp $(DEL_POOL)
	mv $(TI_SW_ROOT)/cgt_a8 $(DEL_POOL)
	mv $(TI_SW_ROOT)/edma3lld $(DEL_POOL)
	mv $(TI_SW_ROOT)/ivahd_hdvicp $(DEL_POOL)
	mkdir -p $(hdvpss_PATH)
	touch $(hdvpss_PATH)/component.mk
	rm $(DEL_POOL) $(TMP_TOOLS) -rf

strip_xdais:
	mkdir -p $(DEL_POOL) 
	mkdir -p $(TMP_TOOLS)/$(xdais_PATH)/packages/ti/xdais/ 
	${CPCMD} $(xdais_PATH)/packages/ti/xdais/* $(TMP_TOOLS)/$(xdais_PATH)/packages/ti/xdais/
	mv $(TI_SW_ROOT)/xdais $(DEL_POOL) 
	mkdir $(TI_SW_ROOT)/xdais
	mv $(TMP_TOOLS)/$(xdais_PATH) $(TI_SW_ROOT)/xdais/

strip_syslink:
	mkdir -p $(DEL_POOL) 
	mkdir -p $(TMP_TOOLS)/$(syslink_PATH)/packages/ti/syslink/ 
	${CPCMD} $(syslink_PATH)/packages/ti/syslink/* $(TMP_TOOLS)/$(syslink_PATH)/packages/ti/syslink/
	mkdir -p $(DEL_POOL)/syslink
	mv $(syslink_PATH) $(DEL_POOL)/syslink
	mkdir -p $(TI_SW_ROOT)/syslink
	mv $(TMP_TOOLS)/$(syslink_PATH) $(TI_SW_ROOT)/syslink/

strip_ipc:
	mkdir -p $(DEL_POOL) 
	mkdir -p $(TMP_TOOLS)/$(ipc_PATH)/packages/ti/ipc/ 
	${CPCMD} $(ipc_PATH)/packages/ti/ipc/* $(TMP_TOOLS)/$(ipc_PATH)/packages/ti/ipc/
	mkdir -p $(DEL_POOL)/ipc
	mv $(ipc_PATH) $(DEL_POOL)/ipc
	mkdir -p $(TI_SW_ROOT)/ipc
	mv $(TMP_TOOLS)/$(ipc_PATH) $(TI_SW_ROOT)/ipc/

strip_codecs:
	mkdir -p $(DEL_POOL) 
	mkdir -p $(DEL_POOL)/codec
	mv $(aaclcenc_PATH) $(DEL_POOL)/codec
	mv $(aaclcdec_PATH) $(DEL_POOL)/codec
	mkdir -p $(TMP_TOOLS)/codec
	${CPCMD} $(h264dec_DIR)/500.V.H264AVC.D.HP.IVAHD.02.00/IVAHD_001/Inc $(TMP_TOOLS)/codec/
	mv $(h264dec_DIR)/500.V.H264AVC.D.HP.IVAHD.02.00/IVAHD_001/* $(DEL_POOL)
	mv $(TMP_TOOLS)/codec/Inc/ $(h264dec_DIR)/500.V.H264AVC.D.HP.IVAHD.02.00/IVAHD_001/
	rm -rf $(TMP_TOOLS)/codec/* $(DEL_POOL)/*
	${CPCMD} $(h264enc_DIR)/500.V.H264AVC.E.IVAHD.02.00/IVAHD_001/Inc $(TMP_TOOLS)/codec/
	mv $(h264enc_DIR)/500.V.H264AVC.E.IVAHD.02.00/IVAHD_001/* $(DEL_POOL)
	mv $(TMP_TOOLS)/codec/Inc/ $(h264enc_DIR)/500.V.H264AVC.E.IVAHD.02.00/IVAHD_001/
	rm -rf $(TMP_TOOLS)/codec/* $(DEL_POOL)/*
	${CPCMD} $(jpegdec_DIR)/500.V.MJPEG.D.IVAHD.01.00/IVAHD_001/inc $(TMP_TOOLS)/codec/
	mv $(jpegdec_DIR)/500.V.MJPEG.D.IVAHD.01.00/IVAHD_001/* $(DEL_POOL)
	mv $(TMP_TOOLS)/codec/inc/ $(jpegdec_DIR)/500.V.MJPEG.D.IVAHD.01.00/IVAHD_001/
	rm -rf $(TMP_TOOLS)/codec/* $(DEL_POOL)/*
	${CPCMD} $(jpegenc_DIR)/500.V.MJPEG.E.IVAHD.01.00/IVAHD_001/inc $(TMP_TOOLS)/codec/
	mv $(jpegenc_DIR)/500.V.MJPEG.E.IVAHD.01.00/IVAHD_001/* $(DEL_POOL)
	mv $(TMP_TOOLS)/codec/inc/ $(jpegenc_DIR)/500.V.MJPEG.E.IVAHD.01.00/IVAHD_001/
	rm -rf $(TMP_TOOLS)/codec/* $(DEL_POOL)/*
	${CPCMD} $(mpeg4dec_DIR)/500.V.MPEG4.D.ASP.IVAHD.01.00/IVAHD_001/Inc $(TMP_TOOLS)/codec/
	mv $(mpeg4dec_DIR)/500.V.MPEG4.D.ASP.IVAHD.01.00/IVAHD_001/* $(DEL_POOL)
	mv $(TMP_TOOLS)/codec/Inc/ $(mpeg4dec_DIR)/500.V.MPEG4.D.ASP.IVAHD.01.00/IVAHD_001/
	rm $(DEL_POOL) $(TMP_TOOLS) -rf
	${CPCMD} $(mpeg4enc_DIR)/500.V.MPEG4.E.SP.IVAHD.01.00/IVAHD_001/Inc $(TMP_TOOLS)/codec/
	mv $(mpeg4enc_DIR)/500.V.MPEG4.E.SP.IVAHD.01.00/IVAHD_001/* $(DEL_POOL)
	mv $(TMP_TOOLS)/codec/Inc/ $(mpeg4enc_DIR)/500.V.MPEG4.E.SP.IVAHD.01.00/IVAHD_001/
	rm $(DEL_POOL) $(TMP_TOOLS) -rf
	${CPCMD} $(mpeg2dec_DIR)/500.V.MPEG2.D.ASP.IVAHD.01.00/IVAHD_001/Inc $(TMP_TOOLS)/codec/
	mv $(mpeg2dec_DIR)/500.V.MPEG2.D.ASP.IVAHD.01.00/IVAHD_001/* $(DEL_POOL)
	mv $(TMP_TOOLS)/codec/Inc/ $(mpeg2dec_DIR)/500.V.MPEG2.D.ASP.IVAHD.01.00/IVAHD_001/
	rm $(DEL_POOL) $(TMP_TOOLS) -rf
