

uboot_build:
	$(MAKE) -C$(UBOOTDIR) CROSS_COMPILE=$(CODEGEN_PREFIX) ARCH=arm $(UBOOT_TARGET)

uboot_build_cent:
	$(MAKE) -C$(UBOOTDIR) CROSS_COMPILE=$(CODEGEN_PREFIX) ARCH=arm

uboot_clean:
	$(MAKE) -C$(UBOOTDIR) CROSS_COMPILE=$(CODEGEN_PREFIX) ARCH=arm distclean

uboot:
ifeq ($(DVR_RDK_BOARD_TYPE),DM816X_UD_DVR)
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8168_dvr_config
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.bin $(TFTP_HOME)/u-boot_dvr.bin
	cp $(UBOOTDIR)/u-boot.noxip.bin $(TFTP_HOME)/uboot_NAND_$(DVR_RDK_BOARD_TYPE)
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8168_dvr_min_sd
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.noxip.bin $(TFTP_HOME)/MLO
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM816X_TI_EVM)
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8168_evm_config
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.noxip.bin $(TFTP_HOME)/uboot_NAND_$(DVR_RDK_BOARD_TYPE)
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM814X_TI_EVM)
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8148_evm_min_uart
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.uart $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.uart
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8148_evm_min_nand
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.nand $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.nand
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8148_evm_config_nand
	$(MAKE) -fbuild_uboot.mk uboot_build_cent
	cp $(UBOOTDIR)/u-boot.bin $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).bin
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM814X_BCH_DVR)
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8148_dvr_min_uart
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.uart $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.uart
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8148_dvr_min_nand
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.nand $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.nand
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8148_dvr_config_nand
	$(MAKE) -fbuild_uboot.mk uboot_build_cent
	cp $(UBOOTDIR)/u-boot.bin $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).bin
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM810X_TI_EVM)
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti813x_evm_min_sd
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.sd $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.sd	 
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti813x_evm_min_uart
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.uart $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.uart
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti813x_evm_min_nand
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.nand $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.nand
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti813x_evm_config_nand
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.bin $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).bin
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti813x_evm_config_sd
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.bin $(TFTP_HOME)/u-boot_SD_$(DVR_RDK_BOARD_TYPE).bin
endif

ifeq ($(DVR_RDK_BOARD_TYPE),DM810X_BCH_DVR) 
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8107_bch_dvr_min_uart
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.uart $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.uart
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=t8107_bch_dvr_min_nand
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.nand $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.nand
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8107_bch_dvr_config_nand
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.bin $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).bin
endif

ifeq ($(DVR_RDK_BOARD_TYPE),DM810X_UD_DVR) 
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8107_dvr_min_uart
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.uart $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.uart
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8107_dvr_min_nand
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.nand $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.nand
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8107_dvr_min_sd
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.sd $(TFTP_HOME)/MLO
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8107_dvr_config_nand
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.bin $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).bin
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8107_dvr_config_sd
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.bin $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).sd.bin
endif

ifeq ($(DVR_RDK_BOARD_TYPE),DM810X_BCH_120_DVR) 
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8107_bch_120_dvr_min_uart
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.uart $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.uart
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8107_bch_120_dvr_min_nand
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.min.nand $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).min.nand
	$(MAKE) -fbuild_uboot.mk uboot_clean
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=ti8107_bch_120_dvr_config_nand
	$(MAKE) -fbuild_uboot.mk uboot_build UBOOT_TARGET=u-boot.ti
	cp $(UBOOTDIR)/u-boot.bin $(TFTP_HOME)/u-boot_$(DVR_RDK_BOARD_TYPE).bin
endif


