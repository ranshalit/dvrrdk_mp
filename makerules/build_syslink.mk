
ifeq ($(PLATFORM),ti816x-evm)
SYSLINK_DEVICE=TI816X
endif    
ifeq ($(PLATFORM),ti814x-evm)	
SYSLINK_DEVICE=TI814X
endif
ifeq ($(PLATFORM),ti810x-evm)	
SYSLINK_DEVICE=TI813X
endif

syslink_build:
	cp syslink_products.mak $(syslink_PATH)/products.mak
	$(MAKE) -C$(syslink_PATH) $(TARGET)

syslink_kermod:
	$(MAKE) -fbuild_syslink.mk syslink_build DEVICE=$(SYSLINK_DEVICE) TARGET=syslink-driver	
	-mkdir -p $(TARGET_FS_DIR)/kermod
	cp $(syslink_PATH)/packages/ti/syslink/bin/$(SYSLINK_DEVICE)/syslink.ko $(TARGET_FS_DIR)/kermod/.

syslink_rtos:
	$(MAKE) -fbuild_syslink.mk syslink_build DEVICE=$(SYSLINK_DEVICE) TARGET=syslink-rtos	

syslink_linux:
	$(MAKE) -fbuild_syslink.mk syslink_build DEVICE=$(SYSLINK_DEVICE) TARGET=syslink-hlos	

syslink_kermod_clean:
	$(MAKE) -fbuild_syslink.mk syslink_build DEVICE=$(SYSLINK_DEVICE) TARGET=syslink-driver-clean

syslink_clean:
	$(MAKE) -fbuild_syslink.mk syslink_build DEVICE=$(SYSLINK_DEVICE) TARGET=clean

syslink_info:
	$(MAKE) -fbuild_syslink.mk syslink_build DEVICE=$(SYSLINK_DEVICE) TARGET=.show-products


