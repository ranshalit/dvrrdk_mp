

lsp_build:
	$(MAKE) -C$(KERNELDIR) CROSS_COMPILE=$(CODEGEN_PREFIX) ARCH=arm $(TARGET)

lsp_menuconfig:
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=menuconfig

lsp_config:
ifeq ($(DVR_RDK_BOARD_TYPE),DM816X_UD_DVR)
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=ti8168_dvr_defconfig
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM816X_TI_EVM)
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=ti8168_evm_defconfig
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM814X_TI_EVM)
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=ti8148_evm_defconfig
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM814X_BCH_DVR)
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=ti814x_dvr_defconfig
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM810X_TI_EVM)
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=dm385_evm_defconfig
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM810X_BCH_DVR)
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=ti810X_dvr_defconfig
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM810X_BCH_120_DVR)
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=ti810X_bch_120_dvr_defconfig
endif
ifeq ($(DVR_RDK_BOARD_TYPE),DM810X_UD_DVR)
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=ti8107_dvr_defconfig
endif

lsp:
	$(MAKE) -fbuild_lsp.mk lsp_config
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=uImage
ifeq ($(PLATFORM),ti816x-evm)
ifeq ($(DDR_MEM),DDR_MEM_2048M)
	echo 'Doing mkimage...'
	mkimage -A arm -O linux -T kernel -C none -a 0xc0008000 -e 0xc0008000 -n 'Linux-2.6.37' -d $(KERNELDIR)/arch/arm/boot/zImage  $(KERNELDIR)/arch/arm/boot/uImage
endif
endif
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=modules
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=headers_install
	-mkdir -p $(TFTP_HOME)/
	cp $(KERNELDIR)/arch/arm/boot/uImage $(TFTP_HOME)/uImage_$(DVR_RDK_BOARD_TYPE)
	-mkdir -p $(TARGET_FS_DIR)/kermod
	cp $(KERNELDIR)/drivers/video/ti81xx/vpss/vpss.ko $(TARGET_FS_DIR)/kermod/.
	cp $(KERNELDIR)/drivers/video/ti81xx/ti81xxfb/ti81xxfb.ko $(TARGET_FS_DIR)/kermod/.
	cp $(KERNELDIR)/drivers/video/ti81xx/ti81xxhdmi/ti81xxhdmi.ko $(TARGET_FS_DIR)/kermod/.

lsp_clean:
	$(MAKE) -fbuild_lsp.mk lsp_build TARGET=distclean
