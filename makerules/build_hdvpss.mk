

#-----------------------------------------------------------------------------------------------------------------
# HDVENC_OUTPUT_SWAP variable has been added to hdvpss build rule for initial revisions of DM8107 DVR and alpha 
# versions of DM8107 EVM. This ensures right colors are seen on VGA output. The issue with color swapping should 
# be fixed in later revisions of DM8107 DVR and in beta versions of DM8107 EVM.
#
# If the DVR_RDK_BOARD_TYPE is DM8107_EVM and the evm is of alpha version, ensure HDVENC_OUTPUT_SWAP=1  
#-----------------------------------------------------------------------------------------------------------------

hdvpss:
	$(MAKE) -C $(hdvpss_PATH)/packages/ti/psp/vps $(TARGET) CORE=m3vpss HDVENC_OUTPUT_SWAP=0
ifeq ($(DVR_RDK_BOARD_TYPE),DM810X_BCH_DVR)
	$(MAKE) -C $(hdvpss_PATH)/packages/ti/psp/vps $(TARGET) CORE=m3vpss HDVENC_OUTPUT_SWAP=1
endif
	$(MAKE) -C $(hdvpss_PATH)/packages/ti/psp/i2c $(TARGET) CORE=m3vpss
	$(MAKE) -C $(hdvpss_PATH)/packages/ti/psp/devices $(TARGET) CORE=m3vpss
	$(MAKE) -C $(hdvpss_PATH)/packages/ti/psp/platforms $(TARGET) CORE=m3vpss
	$(MAKE) -C $(hdvpss_PATH)/packages/ti/psp/proxyServer $(TARGET) CORE=m3vpss

hdvpss_clean:
	$(MAKE) -fbuild_hdvpss.mk hdvpss TARGET=clean



	