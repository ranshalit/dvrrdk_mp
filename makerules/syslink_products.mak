
SYSLINK_INSTALL_DIR = $(syslink_PATH)

# List of supported devices (choose one): OMAP3530, TI816X, TI814X, OMAPL1XX,
#    TI813X, TI811X
#
#DEVICE = 

# Master core (GPP) OS type (choose one): Linux, Qnx, Bios
#
GPPOS = Linux

# SysLink HLOS driver options variable (choose one): 0 or 1
#
SYSLINK_BUILD_DEBUG=0
SYSLINK_BUILD_OPTIMIZE=1
SYSLINK_TRACE_ENABLE=1

# SysLink HLOS driver Notify options (choose one): NOTIFYDRIVERSHM,
#    NOTIFYDRIVERCIRC
#
SYSLINK_NOTIFYDRIVER=NOTIFYDRIVERSHM

# SysLink HLOS driver MessageQ Transport options (choose one): TRANSPORTSHM,
#    TRANSPORTSHMNOTIFY, TRANSPORTSHMCIRC
#
SYSLINK_TRANSPORT=TRANSPORTSHM

# Set SDK type when building for a TI SDK kit (choose one): EZSDK or NONE
#
SDK = NONE

# Define root dir to install SysLink driver and samples for target file-system
#
EXEC_DIR = _your_filesys_

# Define file format for loader and slave executables (choose one): COFF, ELF
#
LOADER = ELF

# Optional: recommended to install all dependent components in one folder.
#
DEPOT = _your_depot_folder_

# Define the product variables for the device you will be using.
#

LINUXKERNEL             = $(KERNELDIR)
CGT_ARM_INSTALL_DIR     = $(CODEGEN_PATH_A8)
CGT_ARM_PREFIX          = $(CODEGEN_PREFIX)
IPC_INSTALL_DIR         = $(ipc_PATH)
BIOS_INSTALL_DIR        = $(bios_PATH)
XDC_INSTALL_DIR         = $(xdc_PATH)
CGT_C674_ELF_INSTALL_DIR= $(CODEGEN_PATH_DSP)
CGT_M3_ELF_INSTALL_DIR  = $(CODEGEN_PATH_M3)

######## For TI816X device ########
ifeq ("$(DEVICE)","TI816X")

# SYS/BIOS timer frequency (ti.sysbios.timers.dmtimer.Timer.intFreq)
TI81XXDSP_DMTIMER_FREQ  = 32768

######## For TI814X device ########
else ifeq ("$(DEVICE)","TI814X")

# SYS/BIOS timer frequency (ti.sysbios.timers.dmtimer.Timer.intFreq)
TI81XXDSP_DMTIMER_FREQ  = 20000000

######## For TI813X device ########
else ifeq ("$(DEVICE)","TI813X")


######## End of device specific variables ########
else ifeq ($(MAKECMDGOALS), clean)
else ifeq ($(MAKECMDGOALS), clobber)
else ifeq ($(MAKECMDGOALS), .show-products)
else ifeq ($(MAKECMDGOALS), help)
else
    $(error DEVICE is set to "$(DEVICE)", which is invalid. Set this in <SysLink Install>/products.mak. Refer to the SysLink Install Guide for more information)
endif

# Use this goal to print your product variables.
.show-products:
	@echo "DEPOT                    = $(DEPOT)"
	@echo "DEVICE                   = $(DEVICE)"
	@echo "GPPOS                    = $(GPPOS)"
	@echo "SDK                      = $(SDK)"
	@echo "TI81XXDSP_DMTIMER_FREQ   = $(TI81XXDSP_DMTIMER_FREQ)"
	@echo "SYSLINK_BUILD_DEBUG      = $(SYSLINK_BUILD_DEBUG)"
	@echo "SYSLINK_BUILD_OPTIMIZE   = $(SYSLINK_BUILD_OPTIMIZE)"
	@echo "SYSLINK_TRACE_ENABLE     = $(SYSLINK_TRACE_ENABLE)"
	@echo "LOADER                   = $(LOADER)"
	@echo "SYSLINK_INSTALL_DIR      = $(SYSLINK_INSTALL_DIR)"
	@echo "IPC_INSTALL_DIR          = $(IPC_INSTALL_DIR)"
	@echo "BIOS_INSTALL_DIR         = $(BIOS_INSTALL_DIR)"
	@echo "XDC_INSTALL_DIR          = $(XDC_INSTALL_DIR)"
	@echo "LINUXKERNEL              = $(LINUXKERNEL)"
	@echo "QNX_INSTALL_DIR          = $(QNX_INSTALL_DIR)"
	@echo "CGT_ARM_PREFIX           = $(CGT_ARM_PREFIX)"
	@echo "CGT_C64P_INSTALL_DIR     = $(CGT_C64P_INSTALL_DIR)"
	@echo "CGT_C64P_ELF_INSTALL_DIR = $(CGT_C64P_INSTALL_DIR)"
	@echo "CGT_C674_INSTALL_DIR     = $(CGT_C674_INSTALL_DIR)"
	@echo "CGT_C674_ELF_INSTALL_DIR = $(CGT_C674_ELF_INSTALL_DIR)"
	@echo "CGT_M3_ELF_INSTALL_DIR   = $(CGT_M3_ELF_INSTALL_DIR)"
	@echo "CGT_A8_ELF_INSTALL_DIR   = $(CGT_A8_ELF_INSTALL_DIR)"
	@echo "EXEC_DIR                 = $(EXEC_DIR)"
