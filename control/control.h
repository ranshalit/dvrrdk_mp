/*
 * control.h
 *
 *  Created on: May 22, 2014
 *      Author: ubuntu
 */

#ifndef CONTROL_H_
#define CONTROL_H_

/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/



#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define CONTROL_OPCODE_CONFIG			(1)
#define CONTROL_OPCODE_INIT				(2)
#define CONTROL_OPCODE_LOAD				(3)
#define CONTROL_OPCODE_START			(4)
#define CONTROL_OPCODE_STOP				(5)
#define CONTROL_OPCODE_SHOW_FILES		(6)
#define CONTROL_OPCODE_RM_FILE			(7)
#define CONTROL_OPCODE_FTP_FILE			(8)
#define CONTROL_OPCODE_CP_FILE_TO_USB	(9)
#define CONTROL_OPCODE_MEM_STATUS		(10)
#define CONTROL_OPCODE_REBOOT			(11)
#define CONTROL_OPCODE_EXIT				(12)
#define CONTROL_OPCODE_RM_ALL			(13)
#define CONTROL_OPCODE_CP_ALL_TO_USB	(14)
#define CONTROL_OPCODE_RUN_CMD_LISTENER (15)
#define CONTROL_OPCODE_NUM_OF_TASKS		(16)

int GetFifoAppComm();
int HandleControlCmd(int opcode);


#if defined (__cplusplus)
extern "C" {
#endif


#if defined (__cplusplus)
}
#endif



#endif /* CONTROL_H_ */
