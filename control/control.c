

#include<stdio.h>
#include<fcntl.h>
#include<stdlib.h>
#include<ti_media_std.h>
#include <sys/wait.h>
#include <control.h>
#include <sys/stat.h>
//#include <icd_msg.h>
/*ranran testing */
/* module for communicate between 2 process using FIFOs.
 * One process for control master, and the other process is the control slave, which is actually the application in firmware
 * listening to control commands.
 * for sending, the FIFO information is composed of 2 integers, the first is "opcode", the second argument. can add in future more arguments
 * for receiving, the FIFO information is composed of one integer, '1' means operation ended (successfully),
 * Note: not all menu option must be done in firmware,
 * file related operation seems to be good enough to be done in the control process.
 */
#define BLOCKING_CMD 		0
#define NON_BLOCKING_CMD 	1

#define MAX_INPUT_STR_SIZE (256)

#define COMMIT_SSD_PATH_DEFAULT 		"/media/sda1"
#define COMMIT_USB_PATH_DEFAULT 		"/media/sdb1"
#define COMMIT_MAX_FILE_SIZE_DEFAULT 	(100000000)
/* default partition 0 , means real size */
#define PARTITION_SIZE_DEFAULT			(0)
#define COMMIT_IS_CYCLIC_DEFAULT		(FALSE)
#define COMMIT_ALERT_THRESHOLD_DEFAULT	(70)
/*
typedef struct _control_state
{
	int   state;
	pid_t pid;
}control_state;

control_state controlTable[RECORD_MAX_TASKS];
*/
char g_ssdFilePath[256] = {0};
char g_usbFilePath[256] = {0};
char g_IpAddr[256] = FTP_SERVER;
UInt64 g_filesize = COMMIT_MAX_FILE_SIZE_DEFAULT;
UInt32 g_partitionsize = PARTITION_SIZE_DEFAULT;
Bool g_isCyclic = COMMIT_IS_CYCLIC_DEFAULT;
UInt32 g_alertLimit = COMMIT_ALERT_THRESHOLD_DEFAULT;

int fifo_server,fifo_client;

int runcmd(int is_non_blocking, char* arg1, char* arg2, char* arg3, char* arg4, char* arg5, char* arg6);
int runcmd2(int is_non_blocking, char* arg1, char* arg2, char* arg3, char* arg4, char* arg5, char* arg6, char* arg7, char* arg8, char* arg9);
/* ranran */
char gDemo_runMenuCommit[] = {
    "\r\n =================="
    "\r\n Video Grabber Menu"
    "\r\n =================="
    "\r\n"
	"\r\n 1: Configure setting"
    "\r\n 2: Initialize"
	"\r\n 3: Load firmware"
    "\r\n 4: Start record"
    "\r\n 5: Stop record"
	"\r\n 6: Show file recording list"
	"\r\n 7: Delete file recording"
	"\r\n 8: Backup file recording to Host"
	"\r\n 9: Backup file recording to USB Device"
	"\r\n i: Get information of disk in use [percentage]"
    "\r\n"
	"\r\n r: Reboot firmware"
	"\r\n e: Exit"
    "\r\n"
    "\r\n Enter Choice: "
};


char Demo_getChar()
{
    char buffer[MAX_INPUT_STR_SIZE];

    fflush(stdin);
    fgets(buffer, MAX_INPUT_STR_SIZE, stdin);

    return(buffer[0]);
}


/* tbd better merge to one generic function ranran */
int runcmd(int is_non_blocking,char* arg1, char* arg2, char* arg3, char* arg4, char* arg5, char* arg6)
{
	return runcmd2(is_non_blocking, arg1,arg2,arg3,arg4,arg5,arg6,NULL,NULL,NULL);
}


int runcmd2(int is_non_blocking, char* arg1, char* arg2, char* arg3, char* arg4, char* arg5, char* arg6, char* arg7, char* arg8, char* arg9)
{
	pid_t child_pid, www;
	char  * parmList[9];
	int status = 0;
	parmList[0] = arg1;
	parmList[1] = arg2;
	parmList[2] = arg3;
	parmList[3] = arg4;
	parmList[4] = arg5;
	parmList[5] = arg6;
	parmList[6] = arg7;
	parmList[7] = arg8;
	parmList[8] = arg9;

	 child_pid = fork();
	 if (child_pid == -1)
	 {
		 printf("error, failed to fork\n");
	 }
	 else if (child_pid > 0)
	 {
	     /* This is run by the parent.  Wait for the child
	        to terminate. */
               /* Code executed by parent */
		 if (is_non_blocking == 1)
		 {
			 return status;
		 }
		 do {
			www = waitpid(child_pid, &status, WUNTRACED | WCONTINUED);
			if (www == -1) {
				perror("waitpid");
				exit(EXIT_FAILURE);
			}

			if (WIFEXITED(status)) {
				printf("exited, status=%d\n", WEXITSTATUS(status));
			} else if (WIFSIGNALED(status)) {
				printf("killed by signal %d\n", WTERMSIG(status));
			} else if (WIFSTOPPED(status)) {
				printf("stopped by signal %d\n", WSTOPSIG(status));
			} else if (WIFCONTINUED(status)) {
				printf("continued\n");
			}
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
		 printf("child termination status %d\n",status);

	     return status;
	 }
	 else
	 {
		 /* child */
		/* execv(parmList[0], parmList);*/

		execl(parmList[0], parmList[0], parmList[1], parmList[2],parmList[3],parmList[4],parmList[5], parmList[6],parmList[7],parmList[8], NULL,NULL);
		printf("Return not expected. Must be an execv error.n");
	    /* If execv returns, it must have failed. */

	    printf("Unknown command\n");
	    _exit(EXIT_FAILURE);
	 }
	 return 0;
}


/* ranran */
int Demo_getFileName(char* filename, char *path, char *dirPath)
{
    int status=0;

    /* ranran removed from code */

    printf(" \n");
    printf(" Enter file write path : ");

    fflush(stdin);
    fgets(filename, MAX_INPUT_STR_SIZE, stdin);


    printf(" \n");

    /* remove \n from the path name */
    filename[ strlen(filename)-1 ] = 0;
    strcpy(path, dirPath);
    strcat(path,"/");
    strcat(path,filename);
    printf("path %s",path);
#if 0
    if(!Demo_isPathValid(path))
    {
        printf(" WARNING: Invalid path [%s], \n",path);

        status = -1;

    }
#endif
    if(status==0)
    {
        printf(" Selected file write path [%s] \n", path);
    }

    printf(" \n");

    return status;


}


/* ranran */
int Demo_getIpAddr(char* ipaddr)
{
    int status=0;

    fflush(stdin);
    fgets(ipaddr, MAX_INPUT_STR_SIZE, stdin);
    printf(" \n");
    /* remove \n from the path name */
    ipaddr[ strlen(ipaddr)-1 ] = 0;
    printf(" \n");

    return status;
}
#if 0
int Demo_Commit()
{

	Bool done = FALSE;
	char ch;
	char filename[64];
	char fullpath[128];
	char fullpathDst[128];
	int status;
    char cwd[128];
    int reply;
    int buf[10];
    int count;

    strcpy(g_ssdFilePath, COMMIT_SSD_PATH_DEFAULT);
    strcpy(g_usbFilePath, COMMIT_USB_PATH_DEFAULT);
    g_filesize = COMMIT_MAX_FILE_SIZE_DEFAULT;
    g_isCyclic = COMMIT_IS_CYCLIC_DEFAULT;
    g_alertLimit = COMMIT_ALERT_THRESHOLD_DEFAULT; /*percent */


	while(!done)
	{
		buf[0] = 0;

		printf(gDemo_runMenuCommit);

		ch = Demo_getChar();

		switch(ch)
		{
			case '1':
				printf(" Enter  is cyclic(y/n) : ");
				ch = Demo_getChar();
				if (ch == 'y' || ch == 'Y')
				{
					printf(" Using cyclic file save ");
					g_isCyclic = TRUE;
				}
				else
				{
					printf(" Using non-cyclic file save ");
					g_isCyclic = FALSE;
				}

				buf[0] = 1;
				buf[1] = g_isCyclic;
				write(fifo_server,buf ,sizeof(int)*2);

				break;

			case '2':
				buf[0] = 2;
				write(fifo_server,buf ,sizeof(int)*2);
				break;
			case '3':
				buf[0] = 3;
				write(fifo_server,buf ,sizeof(int));
				break;

			case '4':
				buf[0] = 4;
			    write(fifo_server,buf ,sizeof(int));
				break;

			case '5':
				buf[0] = 5;
				write(fifo_server,buf,sizeof(int));
				break;

			case '6':
				runcmd(BLOCKING_CMD, "/bin/ls", "-al", g_ssdFilePath, NULL,NULL, NULL);
				break;

			case '7':
				status = Demo_getFileName(filename, fullpath, g_ssdFilePath);
				if (status == 0)
				{
					runcmd(BLOCKING_CMD,"/bin/rm", fullpath, NULL, NULL,NULL, NULL);
				}
				break;

			case '8':
				if (g_IpAddr == 0)
				{
					printf("Please configure Host ip address first(option 1)\n");
					break;
				}
				status = Demo_getFileName(filename, fullpath, g_ssdFilePath);
				if (status == 0)
				{
					if (getcwd(cwd, sizeof(cwd)) == NULL)
					{
						printf("unexpected error cwd %s",cwd);
						exit(1);
					}
					chdir(g_ssdFilePath);
					runcmd2(BLOCKING_CMD, "/usr/bin/ftpput",   "-u", "root", "-p" ,"123", "-P", "14147", g_IpAddr, filename );
					chdir(cwd);
				}
				break;

			case '9':
				printf("Enter file to copy\n");
				status = Demo_getFileName(filename, fullpath, g_ssdFilePath);
				printf("Enter folder in USB device\n");
				status = Demo_getFileName(filename, fullpathDst, g_usbFilePath);
				runcmd2(BLOCKING_CMD, "/bin/cp",  fullpath, fullpathDst, NULL, NULL, NULL, NULL, NULL, NULL);

				break;

			case 'i':
				buf[0] = 9;
				write(fifo_server,buf,sizeof(int));
				break;

			case 'r':
				runcmd(BLOCKING_CMD, "/sbin/reboot", NULL, NULL, NULL,NULL, NULL);
				break;

			case 'e':
                done = TRUE;
                printf("Bye...\n");
				break;

			default:
				printf("illegal value\n");
				break;
		}
		if (buf[0] != 0)
		{
			/* wait for reply */
			count = read (fifo_client,&reply,sizeof(int));
			printf("\n ***Reply from server is %d bytes %d***\n",reply,count);
		}
	}

	return 0;

}

int main(int argc, char **argv)
{
	printf("version 0.1.6 %s %s\n",__DATE__ ,__TIME__ );

	/* get communication FIFO */
	GetFifoAppComm();

	/* user menu for various control on application */
	return Demo_Commit();

}
#endif

/*
static
unsigned long long GetFreeSpace()
{
    struct statvfs statBuf;
    int status;
    unsigned long long freeSize;

    status = statvfs(COMMIT_SSD_PATH_DEFAULT,&statBuf);
    return (((float) statBuf.f_bfree) / ((float) statBuf.f_blocks)) * 100;

}
*/

int GetFifoAppComm()
{

	fifo_server=open("../fifo_server",O_RDWR);
	if(fifo_server < 0) {
		printf("Error in opening file");
		exit(-1);
	}
	fifo_client=open("../fifo_client",O_RDWR);

	if(fifo_client < 0) {
		printf("Error in opening file");
		exit(-1);
	}
	return 0;
}


int HandleControlCmd(int opcode)
{

	Bool done = FALSE;
	char ch;
	char filename[64];
	char fullpath[128];
	char fullpathDst[128];
	int status;
    char cwd[128];
    int reply;
    int buf[10];
    int count;

    strcpy(g_ssdFilePath, COMMIT_SSD_PATH_DEFAULT);
    strcpy(g_usbFilePath, COMMIT_USB_PATH_DEFAULT);
    g_filesize = COMMIT_MAX_FILE_SIZE_DEFAULT;
    g_isCyclic = COMMIT_IS_CYCLIC_DEFAULT;
    g_alertLimit = COMMIT_ALERT_THRESHOLD_DEFAULT; /*percent */


    /*controlTable[i].state = STATUS_INPROCESS;*/
	buf[0] = 0;

	switch(opcode)
	{
		case CONTROL_OPCODE_CONFIG:
			printf(" Enter  is cyclic(y/n) : ");
			ch = Demo_getChar();
			if (ch == 'y' || ch == 'Y')
			{
				printf(" Using cyclic file save ");
				g_isCyclic = TRUE;
			}
			else
			{
				printf(" Using non-cyclic file save ");
				g_isCyclic = FALSE;
			}

			buf[0] = 1;
			buf[1] = g_isCyclic;
			count = write(fifo_server,buf ,sizeof(int)*2);
			if (count < sizeof(int))
			{
				printf("failed in CONTROL_OPCODE_MEM_STATUS %d\n", count);
			}
			break;

		case CONTROL_OPCODE_INIT:
		case CONTROL_OPCODE_LOAD:
		case CONTROL_OPCODE_START:
		case CONTROL_OPCODE_STOP:
		case CONTROL_OPCODE_RUN_CMD_LISTENER:
			printf("sending command to application\n");
			buf[0] = opcode;
			count = write(fifo_server,buf ,sizeof(int)*2);
			if (count < sizeof(int)*2)
			{
				printf("failed in CONTROL_OPCODE_INIT/LOAD/START %d\n", count);
			}
			break;

		case CONTROL_OPCODE_SHOW_FILES:
			status = runcmd(BLOCKING_CMD, "/bin/ls", "-al", g_ssdFilePath, NULL,NULL, NULL);
			break;

		case CONTROL_OPCODE_RM_FILE:
			status = Demo_getFileName(filename, fullpath, g_ssdFilePath);
			if (status == 0)
			{
				status = runcmd(BLOCKING_CMD, "/bin/rm", fullpath, NULL, NULL,NULL, NULL);
			}
			break;

		case CONTROL_OPCODE_FTP_FILE:
			if (g_IpAddr == 0)
			{
				printf("Please configure Host ip address first(option 1)\n");
				break;
			}
			status = Demo_getFileName(filename, fullpath, g_ssdFilePath);
			if (status == 0)
			{
				if (getcwd(cwd, sizeof(cwd)) == NULL)
				{
					printf("unexpected error cwd %s",cwd);
					exit(1);
				}
				chdir(g_ssdFilePath);
				status = runcmd2(BLOCKING_CMD, "/usr/bin/ftpput",   "-u", "root", "-p" ,"123", "-P", "14147", g_IpAddr, filename );
				chdir(cwd);
			}
			break;

		case CONTROL_OPCODE_CP_FILE_TO_USB:
			printf("Enter file to copy\n");
			status = Demo_getFileName(filename, fullpath, g_ssdFilePath);
			printf("Enter folder in USB device\n");
			status = Demo_getFileName(filename, fullpathDst, g_usbFilePath);
			status = runcmd2(BLOCKING_CMD, "/bin/cp",  fullpath, fullpathDst, NULL, NULL, NULL, NULL, NULL, NULL);

			break;

		case CONTROL_OPCODE_MEM_STATUS:
			buf[0] = 9;
			count = write(fifo_server,buf,sizeof(int));
			if (count < sizeof(int))
			{
				printf("failed in CONTROL_OPCODE_MEM_STATUS %d\n", count);
			}
			break;

		case CONTROL_OPCODE_REBOOT:
			status = runcmd(BLOCKING_CMD, "/sbin/reboot", NULL, NULL, NULL,NULL, NULL);
			break;

		case CONTROL_OPCODE_EXIT:
			done = TRUE;
			printf("Bye...\n");
			break;

		case CONTROL_OPCODE_RM_ALL:
			strcat(g_ssdFilePath,"/");
			status = runcmd(BLOCKING_CMD, "/bin/rm",  g_ssdFilePath, "-r", "-f",NULL, NULL);
			break;

		case CONTROL_OPCODE_CP_ALL_TO_USB:
			status = runcmd2(BLOCKING_CMD, "/bin/cp",  g_ssdFilePath, g_usbFilePath, NULL, NULL, NULL, NULL, NULL, NULL);
			break;

		default:
			printf("illegal value\n");
			break;
	}

	if (buf[0] != 0)
	{
		/* wait for reply */
		count = read (fifo_client,&reply,sizeof(int));
		if (count < sizeof(int))
		{
			printf("failed in pipe read %d\n", count);
		}
		printf("\n ***Reply from server is %d bytes %d***\n",reply,count);
	/*	controlTable[i].state = (reply==0) ? STATUS_END_OK:STATUS_END_FAIL;*/
	}


	return status;

}

