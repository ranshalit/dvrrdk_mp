
/*
 * commit_msg.h
 *
 *  Created on: Jul 7, 2014
 *      Author: ubuntu
 */

#ifndef COMMIT_MSG_H_
#define COMMIT_MSG_H_

#include "commit.h"
/*
 * icd_msg.h
 *
 *  Created on: Jul 2, 2014
 *      Author: ubuntu
 */



#define MAX_KEEPALIVE_TIMEOUT			10

#define GSDU_KEEP_ALIVE_OPCODE			0x1
#define GSDU_PROCESS_STATUS				0x2
#define GSDU_SINGLE_ERROR				0x3
#define GSDU_SYSTEM_ERROR				0x4
#define GSDU_SOFTWARE_VERSION			0x5
#define GSDU_GENERAL_STATUS				0x6

#define HIU_KEEP_ALIVE_OPCODE			0x1
#define HIU_PROCESS_CMD					0x2
#define HIU_INFO_REQ_CMD				0x3
#define HIU_HIU_INFO_STATUS				0x4

#define PROCESS_ENTER 	1
#define PROCESS_EXIT 	2

#define RECORD_PROCESS_RECORD 		1
#define RECORD_PROCESS_COPY 		2
#define RECORD_PROCESS_DELETE 		3
#define RECORD_PROCESS_DELETE_FILE 	4
#define RECORD_PROCESS_UMOUNT 		5

#define INFO_REQ_ERROR_TABLE 	1
#define INFO_REQ_VERSION	 	2

#define STATUS_NOT_INPROCESS  	0
#define STATUS_INPROCESS  		1
#define STATUS_END_OK 			2
#define STATUS_END_FAIL 		3
#define STATUS_ABORT 			4


#pragma pack (1)
typedef struct _keep_alive
{
	unsigned short length;
	unsigned short opcode;
}keep_alive;



typedef struct _gsdu_process_status
{
	unsigned short length;
	unsigned short opcode;
	unsigned char process_type;
	unsigned char process_state;
}gsdu_process_status;

typedef struct _gsdu_general_status
{
	unsigned short length;
	unsigned short opcode;
	unsigned int capacity;
	unsigned char usb_connect_status;
}gsdu_general_status;

typedef struct _gsdu_version
{
	unsigned short length;
	unsigned short opcode;
	unsigned char major_number;
	unsigned char minor_number;
}gsdu_version;

typedef struct _gsdu_single_fault
{
	unsigned short length;
	unsigned short opcode;
	unsigned short fault_number;
	unsigned char  error_status; /* 1- not exist, 2 - exist */
}gsdu_single_fault;

typedef struct _gsdu_error_table
{
	unsigned short length;
	unsigned short opcode;
	unsigned short number_errors;
	unsigned short error_table[MAX_NUM_ERRORS];
}gsdu_error_table;

typedef struct _hiu_process_cmd
{
	unsigned short length;
	unsigned short opcode;
	unsigned char process_type;
	unsigned char process_cmd;
}hiu_process_cmd;

typedef struct _hiu_info_req
{
	unsigned short length;
	unsigned short opcode;
	unsigned char info_req;
}hiu_info_req;


#pragma pack ()






#endif /* COMMIT_MSG_H_ */
