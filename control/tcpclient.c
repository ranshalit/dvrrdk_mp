/************tcpclient.c************************/
/* Header files needed to use the sockets API. */
/* File contains Macro, Data Type and */
/* Structure definitions along with Function */
/* prototypes. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <control.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include "icd_msg.h"
#include <signal.h>
static int socket_id;

keep_alive keep_alive_msg;
gsdu_process_status gsdu_process_status_msg;

#define MAX_NUM_ERRORS	32
#define MAX_BUF_LEGTH 	256

#define THR_HANDLE_IDLE 	0
#define THR_HANDLE_NEW_MSG 	1
#define THR_EXIT	 		2

/* Default host name of server system. Change it to your default */
/* server hostname or IP.  If the user do not supply the hostname */
/* as an argument, the_server_name_or_IP will be used as default*/


/* Server's port number */
#define SERVPORT 4200

#define READ_LENGTH_STATE 	0
#define READ_OPCODE_STATE 	1
#define READ_MESSAGE_STATE 	2

/* Big endian */
/*
#define DATA_HTONS(val) htons(val)
#define DATA_HTONL(val) htonl(val)
#define DATA_NTOHS(val) ntohs(val)
#define DATA_NTOHL(val) ntohl(val)
*/
/* Little endian */
#define DATA_HTONS(val) (val)
#define DATA_HTONL(val) (val)
#define DATA_NTOHS(val) (val)
#define DATA_NTOHL(val) (val)


typedef struct _thr_msgInfo
{
	unsigned short opcode;
	char buf[MAX_BUF_LEGTH];
	int length;
}thr_msgInfo;

static thr_msgInfo thrMsgInfo = {0};
static int thr_handler_trigger = 0;
static int thr_handler_result = 0;
static int thr_handler_process = 0;
static int thr_handler_process_type = 0;

static int thr_status_start = 0;




int HandleMsg(unsigned short opcode, char* msg, int length);
void *handler_thr_entry(void *cmd);
int HandleProcessCmd(hiu_process_cmd* msg, int length);




/* Pass in 1 parameter which is either the */
/* address or host name of the server, or */
/* set the server name in the #define SERVER ... */
int main(int argc, char *argv[])
{
	/* Variable and structure definitions. */
	int rc, i, flags, state = READ_LENGTH_STATE;
	unsigned short msg_size;
	unsigned short msg_opcode;
	struct sockaddr_in serveraddr;
	char buffer[MAX_BUF_LEGTH];
	char server[255];
	int wait_for_keep_alive = 0;
	int totalcnt = 0;
	struct hostent *hostp;


	keep_alive_msg.length = DATA_HTONS(2);
	keep_alive_msg.opcode = DATA_HTONS(GSDU_KEEP_ALIVE_OPCODE);

	gsdu_process_status_msg.length = DATA_HTONS(4);
	gsdu_process_status_msg.opcode = DATA_HTONS(GSDU_PROCESS_STATUS);

	/* this variable is our reference to the second thread */
	pthread_t handler_thread_id;


	if(pthread_create(&handler_thread_id, NULL, handler_thr_entry, &thr_handler_trigger))
	{
		fprintf(stderr, "Error creating thread\n");
		return 1;
	}

	/* ranran TBD !! */
	 GetFifoAppComm();
	/*InitControlTable();*/
	signal(SIGPIPE, SIG_IGN);
start_connect:
    printf("start_connect\n");
	wait_for_keep_alive = 0;
	/* The socket() function returns a socket */
	/* descriptor representing an endpoint. */
	/* The statement also identifies that the */
	/* INET (Internet Protocol) address family */
	/* with the TCP transport (SOCK_STREAM) */
	/* will be used for this socket. */
	/******************************************/
	/* get a socket descriptor */
	if((socket_id = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("Client-socket() error");
		exit(-1);
	}
	else
		printf("Client-socket() OK\n");
	/* ranran added - no blocking */
	/*fcntl(socket_id, F_SETFL, O_NONBLOCK);*/
	/*If the server hostname is supplied*/
	if(argc > 1)
	{
		/*Use the supplied argument*/
		strcpy(server, argv[1]);
		printf("Connecting to the  %s, port %d ...\n", server, SERVPORT);
	}
	else
	/*Use the default server name or IP*/
		strcpy(server, SERVER_IP);

	memset(&serveraddr, 0x00, sizeof(struct sockaddr_in));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(SERVPORT);

	if((serveraddr.sin_addr.s_addr = inet_addr(server)) == (unsigned long)INADDR_NONE)
	{

		/* When passing the host name of the server as a */
		/* parameter to this program, use the gethostbyname() */
		/* function to retrieve the address of the host server. */
		/***************************************************/
		/* get host address */
		hostp = gethostbyname(server);
		if(hostp == (struct hostent *)NULL)
		{
			printf("HOST NOT FOUND --> ");
			/* h_errno is usually defined */
			/* in netdb.h */
			printf("h_errno = %d\n",h_errno);
			printf("---This is a client program---\n");
			printf("Command usage: %s <server name or IP>\n", argv[0]);
			close(socket_id);
			exit(-1);
		}
		memcpy(&serveraddr.sin_addr, hostp->h_addr, sizeof(serveraddr.sin_addr));
	}

	/* After the socket descriptor is received, the */
	/* connect() function is used to establish a */
	/* connection to the server. */
	/***********************************************/
	/* connect() to server. */

	if((rc = connect(socket_id, (struct sockaddr *)&serveraddr, sizeof(serveraddr))) < 0)
	{
		printf("Client-connect() error \n");
		close(socket_id);
		goto start_connect;
	}
	else
	{
		printf("Connection established...\n");
	}

	flags = fcntl(socket_id, F_GETFL);
	fcntl(socket_id, F_SETFL, flags | O_NONBLOCK);

	/* Send string to the server using */
	/* the write() function. */
	/*********************************************/
	/* Write() some string to the server. */
#if 0
	printf("Sending some string to the  %s...\n", server);
	rc = write(socket_id, data, sizeof(data));

	if(rc < 0)
	{
		perror("Client-write() error");
		rc = getsockopt(socket_id, SOL_SOCKET, SO_ERROR, &temp, &length);
		if(rc == 0)
		{
			/* Print out the asynchronously received error. */
			errno = temp;
			perror("SO_ERROR was");
		}
		close(socket_id);
		exit(-1);
	}
	else
	{
		printf("Client-write() is OK\n");
		printf("String successfully sent lol!\n");
		printf("Waiting the %s to echo back...\n", server);
	}
#endif
	state = READ_LENGTH_STATE;
	while(1)
	{

		rc = write(socket_id, &keep_alive_msg, sizeof(keep_alive_msg));
		if(rc < sizeof(keep_alive_msg))
		{
			printf("not expected failed in write #3\n");
		}
		if(thr_handler_result == HIU_PROCESS_CMD)
		{
			thr_handler_result = 0;
		}
		totalcnt = 0;

		if (state == READ_OPCODE_STATE)
			goto read_opcode;
		else if (state == READ_MESSAGE_STATE)
			goto read_message;
		/* read length  */

read_length:
		while(totalcnt < 2)
		{
			rc = read(socket_id, &buffer[totalcnt], 2-totalcnt);
			if (rc <= 0)
			{
				goto inc_keep_alive;
			}
			if(rc > 0)
				totalcnt += rc;
		}
		state = READ_OPCODE_STATE;
		totalcnt = 0;
		memcpy(&msg_size,&buffer[0],2 );
		msg_size = DATA_NTOHS(msg_size);
		if (msg_size < 2)
		{
			printf("unexpected message size\n");
		}
		/* read opcode */
read_opcode:
		while(totalcnt < 2)
		{
			rc = read(socket_id, &buffer[totalcnt+2], 2-totalcnt);
			if (rc <= 0)
			{
				goto inc_keep_alive;
			}
			if(rc > 0)
				totalcnt += rc;
		}
		state = READ_MESSAGE_STATE;
		totalcnt = 0;
		memcpy(&msg_opcode,&buffer[2],2 );
		msg_opcode = DATA_NTOHS(msg_opcode);
		/* read message */
read_message:
		while(totalcnt < msg_size-2)
		{
			/* Wait for the server to echo the */
			/* string by using the read() function. */
			/***************************************/
			/* Read data from the server. */
			rc = read(socket_id, &buffer[totalcnt+4], msg_size-2-totalcnt);
			if (rc <= 0)
			{
				goto inc_keep_alive;
			}
			if(rc > 0)
				totalcnt += rc;
		}
		printf("msg: ");
		for(i=0;i<rc;i++)
			printf("0x%x ",buffer[i]);
		printf("\n");

		state = READ_LENGTH_STATE;
		if (msg_opcode == HIU_KEEP_ALIVE_OPCODE)
		{
			wait_for_keep_alive = 0;
		}
		else
		{
			/* for now can only do one job at a time. no more ! */
			/* handle message in another thread */
			if (thr_handler_trigger == THR_HANDLE_NEW_MSG)
			{
				printf("unexpected. not finished previous handling\n");
			}
			else
			{
				memcpy(thrMsgInfo.buf, buffer, (msg_size+2));
				thrMsgInfo.length =  (msg_size+2);
				thrMsgInfo.opcode = msg_opcode;
				printf("opcode 0x%x length %d\n",msg_opcode, msg_size);
				printf("buffer:");
				for(i=0;i<(msg_size+2) ;i++)
					printf("0x%x ",buffer[i]);
				printf("\n");
				thr_handler_trigger = THR_HANDLE_NEW_MSG;

			}

		}
inc_keep_alive:
		/* check if keep alive is not received for too long */
		wait_for_keep_alive++;
		if (wait_for_keep_alive == MAX_KEEPALIVE_TIMEOUT)
		{
			printf("not received alive for too long. restart...\n");
			state = READ_LENGTH_STATE;
			goto start_connect;
		}
		sleep(1);


	}
	/* wait for the second thread to finish */
	if(pthread_join(handler_thread_id, NULL))
	{

		fprintf(stderr, "Error joining thread\n");
		return 2;

	}
	printf("exiting...\n");
	/* When the data has been read, close() */
	/* the socket descriptor. */
	/****************************************/
	/* Close socket descriptor from client side. */
	close(socket_id);
	exit(0);
	return 0;
}



/* this function is run by the second thread */
void *handler_thr_entry(void *cmd)
{
	int rc = 0;
	int ret = 0;
	while(1)
	{
		if((thr_handler_trigger) == THR_EXIT)
		{
			break;
		}
		else if((thr_handler_trigger) == THR_HANDLE_NEW_MSG)
		{
			/* send "in process" */

			gsdu_process_status_msg.process_state = STATUS_INPROCESS;
			gsdu_process_status_msg.opcode = thrMsgInfo.opcode;
			rc = write(socket_id, &gsdu_process_status_msg, sizeof(gsdu_process_status_msg));
			if(rc < sizeof(gsdu_process_status_msg))
			{
				printf("writing through socket failed #1\n");
			}

			/* handle task and wait */
			ret = HandleMsg(thrMsgInfo.opcode, thrMsgInfo.buf, thrMsgInfo.length);

			/* send result process */
			gsdu_process_status_msg.process_state = thr_handler_process;
			rc = write(socket_id, &gsdu_process_status_msg, sizeof(gsdu_process_status_msg));
			if(rc < sizeof(gsdu_process_status_msg))
			{
				printf("writing through socket failed #3\n");
			}


			thr_handler_trigger = THR_HANDLE_IDLE;
		}
	}
	/* the function must return something - NULL will do */
	return NULL;

}



int HandleMsg(unsigned short opcode, char* msg, int length)
{
	int status = 0;
	switch(opcode)
	{
		case HIU_PROCESS_CMD:
			status = HandleProcessCmd((hiu_process_cmd *) msg, length);
			break;

		case HIU_INFO_REQ_CMD:
			printf("TBD HIU_INFO_REQ_CMD not done yet\n");
			break;

		case HIU_HIU_INFO_STATUS:
			printf("TBD HIU_HIU_INFO_STATUS not done yet\n");
			break;

		default:
			printf("unexpected message opcode 0x%x",opcode);
			break;

	}
	return status;
}



int HandleProcessCmd(hiu_process_cmd* msg, int length)
{
	int status = 0;

	/* no need ntohs */
	switch(msg->process_cmd)
	{
		case RECORD_PROCESS_RECORD:
			if(msg->process_type == 0)
			{
			printf("HandleProcessCmd handle start record\n ");
			status = HandleControlCmd(CONTROL_OPCODE_START);
			}
			else
			{
				printf("HandleProcessCmd handle stop record\n ");
				status = HandleControlCmd(CONTROL_OPCODE_STOP);
			}
			break;
		case RECORD_PROCESS_COPY:
			printf("HandleProcessCmd handle start copy\n ");
			status = HandleControlCmd(CONTROL_OPCODE_CP_ALL_TO_USB);
			break;
		case RECORD_PROCESS_DELETE:
			printf("HandleProcessCmd handle start delete\n ");
			status = HandleControlCmd(CONTROL_OPCODE_RM_ALL);
			break;
		case 5:
			printf("HandleProcessCmd handle show files\n ");
			status = HandleControlCmd(CONTROL_OPCODE_SHOW_FILES);
			break;

		default:
			printf("process command unfamiliar. ignore 0x%x\n",msg->process_cmd);
			break;
	}
	return status;
}
