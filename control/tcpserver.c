/************tcpserver.c************************/
/* header files needed to use the sockets API */
/* File contain Macro, Data Type and Structure */
/***********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/fcntl.h>
#include <signal.h>
#include "icd_msg.h"

/* BufferLength is 100 bytes */
#define BufferLength 100
/* Server port number */
#define SERVPORT 4200

/* Big endian */
#if 0
#define DATA_HTONS(val) htons(val)
#define DATA_HTONL(val) htonl(val)
#define DATA_NTOHS(val) ntohs(val)
#define DATA_NTOHL(val) ntohl(val)
#else
/* Little endian */
#define DATA_HTONS(val) (val)
#define DATA_HTONL(val) (val)
#define DATA_NTOHS(val) (val)
#define DATA_NTOHL(val) (val)
#endif

keep_alive keep_alive_msg;
hiu_process_cmd process_msg;
hiu_info_req info_req_msg;
keep_alive test_msg;

int mycnt = 0;
void OSA_waitMsecs(UInt32 msecs)
{
    struct timespec delayTime, remainTime;
    int ret;

    delayTime.tv_sec  = msecs/1000;
    delayTime.tv_nsec = (msecs%1000)*1000000;

    do
    {
        ret = nanosleep(&delayTime, &remainTime);
        if(ret < 0 && remainTime.tv_sec > 0 && remainTime.tv_nsec > 0)
        {
            /* restart for remaining time */
            delayTime = remainTime;
        }
        else
        {
            break;
        }
    } while(1);
}


int main()
{
	int i =0, wait_for_keep_alive = 0;
	/* Variable and structure definitions. */
	int sd, sd2, rc, length = sizeof(int);
	int totalcnt = 0, on = 1;
	char temp;
	int type;
	char buffer[BufferLength];
	struct sockaddr_in serveraddr;
	struct sockaddr_in their_addr;
	int msg_size, msg_opcode;
	fd_set read_fd;
	struct timeval timeout;
	int cmd_fd;
	int flags;
	int fifo_cmd;
	char buf[10];
	int state = 0;
	int count = 0, value = 0, opcode = 0;
int cmd = 0;
	keep_alive_msg.length = DATA_HTONS(2);
	keep_alive_msg.opcode = DATA_HTONS(HIU_KEEP_ALIVE_OPCODE);
	process_msg.length = DATA_HTONS(4);
	process_msg.opcode = DATA_HTONS(HIU_PROCESS_CMD);
	info_req_msg.length = DATA_HTONS(3);
	info_req_msg.opcode = DATA_HTONS(HIU_INFO_REQ_CMD);
	test_msg.length = DATA_HTONS(2);


	system("rm cmd_pipe");

	cmd_fd = mkfifo("cmd_pipe",0777);
	if(cmd_fd<0) {
	 printf("Unable to create a fifo");
	 exit(-1);
	 }
	fifo_cmd = open("cmd_pipe",O_RDWR);
	if(fifo_cmd<1) {
	 printf("Error opening server file\n");
	 }
	flags = fcntl(fifo_cmd, F_GETFL);
	fcntl(fifo_cmd, F_SETFL, flags | O_NONBLOCK);



	timeout.tv_sec = 15;
	timeout.tv_usec = 0;
	signal(SIGPIPE, SIG_IGN);

start_connect:
	/* The socket() function returns a socket descriptor */
	/* representing an endpoint. The statement also */
	/* identifies that the INET (Internet Protocol) */
	/* address family with the TCP transport (SOCK_STREAM) */
	/* will be used for this socket. */
	/************************************************/
	/* Get a socket descriptor */
	if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("Server-socket() error\n");
		/* Just exit */
		close(sd);
		goto start_connect;
	}
	else
		printf("Server-socket() is OK\n");

	/* The setsockopt() function is used to allow */
	/* the local address to be reused when the server */
	/* is restarted before the required wait time */
	/* expires. */
	/***********************************************/
	/* Allow socket descriptor to be reusable */
	if((rc = setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on))) < 0)
	{
		perror("Server-setsockopt() error\n");
		close(sd);
		goto start_connect;
	}
	else
		printf("Server-setsockopt() is OK\n");

	/* bind to an address */
	memset(&serveraddr, 0x00, sizeof(struct sockaddr_in));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(SERVPORT);
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);

	printf("Using %s, listening at %d\n", inet_ntoa(serveraddr.sin_addr), SERVPORT);

	/* After the socket descriptor is created, a bind() */
	/* function gets a unique name for the socket. */
	/* In this example, the user sets the */
	/* s_addr to zero, which allows the system to */
	/* connect to any client that used port 3005. */
	if((rc = bind(sd, (struct sockaddr *)&serveraddr, sizeof(serveraddr))) < 0)
	{
		perror("Server-bind() error");
		/* Close the socket descriptor */
		close(sd);
		/* and just exit */
		goto start_connect;
	}
	else
		printf("Server-bind() is OK\n");
start_listen:
	wait_for_keep_alive  = 0;
	/* The listen() function allows the server to accept */
	/* incoming client connections. In this example, */
	/* the backlog is set to 10. This means that the */
	/* system can queue up to 10 connection requests before */
	/* the system starts rejecting incoming requests.*/
	/*************************************************/
	/* Up to 10 clients can be queued */
	if((rc = listen(sd, 10)) < 0)
	{
		perror("Server-listen() error");
		close(sd);
		goto start_connect;
	}
	else
		printf("Server-Ready for client connection...\n");

	/* The server will accept a connection request */
	/* with this accept() function, provided the */
	/* connection request does the following: */
	/* - Is part of the same address family */
	/* - Uses streams sockets (TCP) */
	/* - Attempts to connect to the specified port */
	/***********************************************/
	/* accept() the incoming connection request. */
	int sin_size = sizeof(struct sockaddr_in);
	if((sd2 = accept(sd, (struct sockaddr *)&their_addr, &sin_size)) < 0)
	{
		perror("Server-accept() error");
		close(sd);
		goto start_connect;
	}
	else
		printf("Server-accept() is OK\n");

	/*client IP*/
	printf("Server-new socket, sd2 is OK...\n");
	printf("Got connection from the  client: %s\n", inet_ntoa(their_addr.sin_addr));

	/* The select() function allows the process to */
	/* wait for an event to occur and to wake up */
	/* the process when the event occurs. In this */
	/* example, the system notifies the process */
	/* only when data is available to read. */
	/***********************************************/
	/* Wait for up to 15 seconds on */
	/* select() for data to be read. */
	flags = fcntl(sd2, F_GETFL);
	fcntl(sd2, F_SETFL, flags | O_NONBLOCK);
	FD_ZERO(&read_fd);
	FD_SET(sd2, &read_fd);
	char mychar[2];
	mychar[1] = '\0';

	while(1)
	{
		printf(".");
		count = read(fifo_cmd, buf , 3);
		if (count > 0)
		{
			printf("read %d chars!\n",count);
			opcode = 0;

			if (0 == strncmp(buf,"212",3))
			{
				printf("#1\n");


				opcode = 2;
				cmd = 1;
				type = 2;
			}
			if (0 == strncmp(buf,"211",3))
			{
				opcode = 2;
				cmd = 1;
				type = 1;
			}
			if (0 == strncmp(buf,"222",3))
			{
				opcode = 2;
				cmd = 2;
				type = 2;
			}
			if (0 == strncmp(buf,"221",3))
			{
				opcode = 2;
				cmd = 2;
				type = 1;
			}
			if (0 == strncmp(buf,"232",3))
			{
				opcode = 2;
				cmd = 3;
				type = 2;
			}
			if (0 == strncmp(buf,"231",3))
			{
				opcode = 2;
				cmd = 3;
				type = 1;
			}
			if (0 == strncmp(buf,"31",2))
			{
				opcode = 3;
				cmd = 1;
			}
			if (0 == strncmp(buf,"32",2))
			{
				opcode = 3;
				cmd = 2;
			}
			if (0 == strncmp(buf,"41",2))
			{
				opcode = 4;
				cmd = 1;
			}
			if (0 == strncmp(buf,"51",2))
			{
				opcode = 5;
				cmd = 1;
			}
			if (opcode == 2)
			{
				printf("<< process cmd %d  type %d\n", cmd, type);
				process_msg.process_cmd = cmd;
				process_msg.process_type= type;
				rc = write(sd2, &process_msg, sizeof(process_msg));
				if(rc != sizeof(process_msg))
				{

					printf("not expected failed in write procee_msg #3 %d\n", rc);

				}
			}
			else if (opcode == 3)
			{
				printf("<< info_req_msg cmd %d \n", cmd);
				info_req_msg.info_req = cmd;

				rc = write(sd2, &info_req_msg, sizeof(info_req_msg));
				if(rc != sizeof(info_req_msg))
				{

					printf("not expected failed in write procee_msg #4 %d\n", rc);
					goto start_listen;
				}
			}
			else if (opcode == 4)
			{
				printf("<< info_req_msg cmd %d \n", cmd);
				test_msg.opcode = DATA_HTONS(4);

				rc = write(sd2, &test_msg, sizeof(test_msg));
				if(rc != sizeof(test_msg))
				{

					printf("not expected failed in write procee_msg #5 %d\n", rc);
					goto start_listen;
				}
			}
			else if (opcode == 5)
			{
				printf("<< info_req_msg cmd %d \n", cmd);
				test_msg.opcode = DATA_HTONS(5);

				rc = write(sd2, &test_msg, sizeof(test_msg));
				if(rc != sizeof(test_msg))
				{

					printf("not expected failed in write procee_msg #5 %d\n", rc);
					goto start_listen;
				}
			}
			else
			{
				printf("nothing to do\n");
			}
		}

		/*rc = select(sd2+1, &read_fd, NULL, NULL, &timeout);
		if((rc == 1) && (FD_ISSET(sd2, &read_fd)))*/
		{
			/* Read data from the client. */

			totalcnt = 0;
			if (state == 1)
				goto read_opcode;
			else if (state == 2)
				goto read_message;
read_length:
			while(totalcnt < 2)
			{
				rc = read(sd2, &buffer[totalcnt], 2-totalcnt);
				if (rc <= 0)
				{
					goto inc_keep_alive;
				}
				if(rc > 0)
					totalcnt += rc;
			}
			state = 1;
			totalcnt = 0;
			memcpy(&msg_size,&buffer[0],2 );

			//printf(">> 0x%x 0x%x ",buffer[0],buffer[1]);
			msg_size = DATA_NTOHS(msg_size);
			if (msg_size < 2)
			{
				printf("unexpected message size");
			}

read_opcode:
			/* read opcode */
			while(totalcnt < 2)
			{
				rc = read(sd2, &buffer[totalcnt+2], 2-totalcnt);
				if(rc > 0)
					totalcnt += rc;
			}
			//printf(">> 0x%x 0x%x ",buffer[2],buffer[3]);
			memcpy(&msg_opcode,&buffer[2],2 );
			msg_opcode = DATA_NTOHS(msg_opcode);
			state = 2;
			totalcnt = 0;
read_message:
			/* read message */
			while(totalcnt < (msg_size-2))
			{

				/* Wait for the server to echo the */
				/* string by using the read() function. */
				/***************************************/
				/* Read data from the server. */
				rc = read(sd2, &buffer[totalcnt+4], (msg_size-2)-totalcnt);
				if(rc > 0)
					totalcnt += rc;
			}
			printf(">> ");
			for(i=0;i<(msg_size+2);i++)
				printf("0x%x ",buffer[i]);
			printf("\n");
			state = 0;



			if (msg_opcode == GSDU_KEEP_ALIVE_OPCODE)
			{
				wait_for_keep_alive = 0;
				rc = write(sd2, &keep_alive_msg, sizeof(keep_alive_msg));
				if(rc != sizeof(keep_alive_msg))
				{

					printf("not expected failed in write keep alive #9 %d\n", rc);
					goto start_listen;
				}
			}


			if (msg_opcode == GSDU_PROCESS_STATUS)
			{
				gsdu_process_status * msg = (gsdu_process_status*) buffer;
				printf(">> process type %d state %d \n", msg->process_type, msg->process_state);
			}
			if (msg_opcode == GSDU_SINGLE_ERROR)
			{
				gsdu_single_fault * msg = (gsdu_single_fault*) buffer;
				printf(">> fault number %d status %d\n", DATA_NTOHS(msg->fault_number), msg->error_status);
			}
			if (msg_opcode == GSDU_SYSTEM_ERROR)
			{
				gsdu_error_table * msg = (gsdu_error_table*) buffer;
				printf(">> number of errors %d\n", DATA_NTOHS(msg->number_errors));
				printf(">> error: ");
				for(i=0; i<DATA_NTOHS(msg->number_errors); i++)
				{
					printf("%d ",DATA_NTOHS(msg->error_table[i]));
				}
				printf("\n");
			}
			if (msg_opcode == GSDU_SOFTWARE_VERSION)
			{
				gsdu_version * msg = (gsdu_version*) buffer;
				printf(">> version major 0x%x minor 0x%x\n",msg->major_number, msg->minor_number);
			}
			if (msg_opcode == GSDU_GENERAL_STATUS)
			{
				gsdu_general_status * msg = (gsdu_general_status*) buffer;
				printf(">> general info capacity %d usb_connected %d\n", DATA_NTOHL(msg->capacity), msg->usb_connect_status);
			}


		}

#if 0
		else if (rc < 0)
		{
			perror("Server-select() error");
			goto start_listen;
		}
		/* rc == 0 */
		else
		{
			printf("Server-select() timed out.\n");
			goto start_listen;
		}
#endif

inc_keep_alive:
		if (mycnt++ == 10)
		{
			mycnt = 0;
			/* check if keep alive is not received for too long */
			wait_for_keep_alive++;
			if (wait_for_keep_alive == MAX_KEEPALIVE_TIMEOUT)
			{
				printf("not recieved alive for too long. restart...\n");
				close(sd);
				goto start_connect;
			}
		}
		OSA_waitMsecs(100);
		/*sleep(1);*/
	}
	printf("exiting\n");
	/* When the data has been sent, close() */
	/* the socket descriptor that was returned */
	/* from the accept() verb and close() the */
	/* original socket descriptor. */
	/*****************************************/
	/* Close the connection to the client and */
	/* close the server listening socket. */
	/******************************************/
	close(sd2);
	close(sd);
	exit(0);
	return 0;
}
