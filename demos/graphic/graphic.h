/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2011 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#ifndef _GRAPHIC_H_
#define _GRAPHIC_H_

#include "mcfw/interfaces/ti_vdis.h"

//#define GRPX_PLANE_DEBUG
#ifdef MANPACK_RES
#define GRPX_PLANE_GRID_WIDTH    (240)
#define GRPX_PLANE_GRID_HEIGHT   (320)
#else
#define GRPX_PLANE_GRID_WIDTH    (1280)
#define GRPX_PLANE_GRID_HEIGHT   ( 720)
#endif
#define GRPX_PLANE_GRID_COLOR           (0) //(0xFF)
#define GRPX_PLANE_GRID_COLOR_BLANK     (0xFF)
#define MAX_POLY_PTS                    16

#define sgn(x) ((x<0)?-1:((x>0)?1:0)) /* macro to return the sign of a number */

/* In this mode 3x3 tile is made where in 1st two coloum are of same width and
 * last colum is of lesser width. This limitation is from SCD block size i.e. 32 and no. of blocks which are there in CIF stream*/
#define GRPX_PLANE_GRID_SCD_TILE 1

typedef enum {
    GRPX_FORMAT_RGB565 = 0,
    GRPX_FORMAT_RGB888 = 1,
    GRPX_FORMAT_MAX

}grpx_plane_type;

/**
  @struct Pixel_Coordinate
  @brief  This structure defines a point in terms of
          X & Y cordinate.
*/
typedef struct
{
    int x;
    int y;
} Pixel_Coordinate;

/**
    \brief Polygon/Zone configuration - part of AlgLink_ScdChVaParams
*/
typedef struct
{
    Bool presentOnDisplay;;
    /**< Polygon present/drawn on screen.
           1 - Present on Screen
           0 - Not-Presnet/Erased from screen.
         */

    UInt8 color;
    
    int noOfPoints;
    /**< Number of points in the current polygon/zone.
         Max 16 points are supported by Algorithm
         */
    int winWidth;
    /**< Width of a Channel in the current mosaic layout */

    int windHeight;
    /**< Height of a Channel in the current mosaic layout */

    int startX;
    /**< StartX of Polygon in the current mosaic layout */

    int startY;
    /**< StartX of Polygon in the current mosaic layout */

    Pixel_Coordinate pixLocation[MAX_POLY_PTS];
    /**< X,Y - Location of a pixel in a frame.
    */
} Polygon_Points;


int grpx_fb_init(grpx_plane_type planeType);
void grpx_fb_exit(void);

int grpx_fb_draw_grid(int width, int height, int startX, int startY, int numBlkPerRow);
int grpx_fb_draw_grid_exit(int width, int height, int startX, int startY, int numBlkPerRow);

int grpx_fb_draw_line(int startX,
                      int startY,
                     int endX,
                     int endY,
                     int color);

int grpx_fb_draw_line_exit(int startX,
                      int startY,
                     int endX,
                     int endY,
                     int color);

int grpx_fb_draw_polygon_abs(Polygon_Points * polygonPoints);
int grpx_fb_draw_polygon_abs_exit(Polygon_Points * polygonPoints);

int grpx_fb_draw_polygon(Polygon_Points * polygonPoints);
int grpx_fb_draw_polygon_exit(Polygon_Points * polygonPoints);

int grpx_fb_draw_osd(int startX, int startY, int width, int height, int numChar,unsigned char * bufAddr);
int grpx_fb_draw_osd_exit(int startX, int startY, int width, int height, int numChar, unsigned char * bufAddr);

int grpx_fb_draw_box(
                    int width,
                    int height,
                    int startX,
                    int startY);

int grpx_fb_draw_box_exit(
                            int width,
                            int height,
                            int startX,
                            int startY);

Int32 grpx_fb_scale(VDIS_DEV devId,
                   UInt32 startX,
                   UInt32 startY,
                   UInt32 outWidth,
                   UInt32 outHeight);

int grpx_fb_stop(UInt32 devId);
int grpx_fb_start(UInt32 devId);

//Int32 grpx_fb_demo();

int grpx_link_draw_grid(int width, int height, int startX, int startY, int numBlkPerRow);
int grpx_link_draw_grid_exit(int width, int height, int startX, int startY, int numBlkPerRow);

int grpx_link_draw_box(
                    int width,
                    int height,
                    int startX,
                    int startY);

int grpx_link_draw_box_exit(
                            int width,
                            int height,
                            int startX,
                            int startY);

int grpx_link_init(grpx_plane_type planeType);
void grpx_link_exit(void);
int grpx_link_draw(VDIS_DEV devId);
Int32 grpx_link_scale(VDIS_DEV devId,
                      UInt32 startX,
                      UInt32 startY,
                      UInt32 outWidth,
                      UInt32 outHeight);

int grpx_link_stop(UInt32 devId);
int grpx_link_start(UInt32 devId);

// Int32 grpx_link_demo();

#if 1

#define grpx_init            grpx_fb_init
#define grpx_exit            grpx_fb_exit
#define grpx_scale           grpx_fb_scale
#define grpx_draw_grid       grpx_fb_draw_grid
#define grpx_draw_grid_exit  grpx_fb_draw_grid_exit
#define grpx_draw_box        grpx_fb_draw_box
#define grpx_draw_box_exit   grpx_fb_draw_box_exit
#define grpx_stop            grpx_fb_stop
#define grpx_start           grpx_fb_start

#else

#define grpx_init           grpx_link_init
#define grpx_exit           grpx_link_exit
#define grpx_draw           grpx_link_draw
#define grpx_scale          grpx_link_scale
#define grpx_demo           grpx_link_demo
#define grpx_stop           grpx_link_stop
#define grpx_start          grpx_link_start
#define grpx_draw_grid      grpx_link_draw_grid
#define grpx_draw_grid_exit grpx_link_draw_grid_exit
#define grpx_draw_box       grpx_link_draw_box
#define grpx_draw_box_exit  grpx_link_draw_box_exit



#endif

#endif /*   _GRAPHIC_H_ */

