/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2011 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 Defines referenced header files
-----------------------------------------------------------------------------*/


#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <linux/fb.h>
#include <linux/ti81xxfb.h>

#define GRPX_SC_MARGIN_OFFSET   (3)

#include "ti_media_std.h"
#include "graphic.h"
#include "graphic_priv.h"

#include "demos/mcfw_api_demos/mcfw_demo/demo.h"
#include "demos/mcfw_api_demos/mcfw_demo/demo_swms.h"

#if defined(TI_816X_BUILD)
#include "graphic_single_buf_nontied.h"
#define GRPX_FB_SINGLE_BUFFER_NON_TIED_GRPX
#endif

/* This option for DM814x has become obsolete from 4.0 GA */
#if     0       // #if defined(TI_814X_BUILD)
#include "graphic_single_buf_tied.h"
#define GRPX_FB_SINGLE_BUFFER_TIED_GRPX
#endif

#if defined(TI_8107_BUILD) || defined (TI_814X_BUILD)
#include "graphic_separate_buf_nontied.h"
#include "graphics_single_buf.h"
#define GRPX_FB_SEPARATE_BUFFER_NON_TIED_GRPX
// #define NUM_GRPX_DISPLAYS   2
#endif

/*----------------------------------------------------------------------------
 local function
-----------------------------------------------------------------------------*/

int disp_getregparams(int display_fd)
{
    int ret;

    struct ti81xxfb_region_params regp;

    memset(&regp, 0, sizeof(regp));

    ret = ioctl(display_fd, TIFB_GET_PARAMS, &regp);
    if (ret < 0) {
        eprintf("TIFB_GET_PARAMS\n");
        return ret;
    }

    dprintf("\n");
    dprintf("Reg Params Info\n");
    dprintf("---------------\n");
    dprintf("region %d, postion %d x %d, prioirty %d\n",
        regp.ridx,
        regp.pos_x,
        regp.pos_y,
        regp.priority);
    dprintf("first %d, last %d\n",
        regp.firstregion,
        regp.lastregion);
    dprintf("sc en %d, sten en %d\n",
        regp.scalaren,
        regp.stencilingen);
    dprintf("tran en %d, type %d, key %d\n",
        regp.transen,
        regp.transtype,
        regp.transcolor);
    dprintf("blend %d, alpha %d\n"
        ,regp.blendtype,
        regp.blendalpha);
    dprintf("bb en %d, alpha %d\n",
        regp.bben,
        regp.bbalpha);
    dprintf("\n");
    return 0;
}

int disp_fbinfo(int fd)
{
    struct fb_fix_screeninfo fixinfo;
    struct fb_var_screeninfo varinfo, org_varinfo;
    int size;
    int ret;

    /* Get fix screen information. Fix screen information gives
     * fix information like panning step for horizontal and vertical
     * direction, line length, memory mapped start address and length etc.
     */
    ret = ioctl(fd, FBIOGET_FSCREENINFO, &fixinfo);
    if (ret < 0) {
        eprintf("FBIOGET_FSCREENINFO !!!\n");
        return -1;
    }

    {
        dprintf("\n");
        dprintf("Fix Screen Info\n");
        dprintf("---------------\n");
        dprintf("Line Length - %d\n", fixinfo.line_length);
        dprintf("Physical Address = %lx\n",fixinfo.smem_start);
        dprintf("Buffer Length = %d\n",fixinfo.smem_len);
        dprintf("\n");
    }

    /* Get variable screen information. Variable screen information
     * gives informtion like size of the image, bites per pixel,
     * virtual size of the image etc. */
    ret = ioctl(fd, FBIOGET_VSCREENINFO, &varinfo);
    if (ret < 0) {
        eprintf("FBIOGET_VSCREENINFO !!!\n");
        return -1;
    }

    {
        dprintf("\n");
        dprintf("Var Screen Info\n");
        dprintf("---------------\n");
        dprintf("Xres - %d\n", varinfo.xres);
        dprintf("Yres - %d\n", varinfo.yres);
        dprintf("Xres Virtual - %d\n", varinfo.xres_virtual);
        dprintf("Yres Virtual - %d\n", varinfo.yres_virtual);
        dprintf("Bits Per Pixel - %d\n", varinfo.bits_per_pixel);
        dprintf("Pixel Clk - %d\n", varinfo.pixclock);
        dprintf("Rotation - %d\n", varinfo.rotate);
        dprintf("\n");
    }

    disp_getregparams(fd);

    memcpy(&org_varinfo, &varinfo, sizeof(varinfo));

    /*
     * Set the resolution which read before again to prove the
     * FBIOPUT_VSCREENINFO ioctl.
     */

    ret = ioctl(fd, FBIOPUT_VSCREENINFO, &org_varinfo);
    if (ret < 0) {
        eprintf("FBIOPUT_VSCREENINFO !!!\n");
        return -1;
    }

    /* It is better to get fix screen information again. its because
     * changing variable screen info may also change fix screen info. */
    ret = ioctl(fd, FBIOGET_FSCREENINFO, &fixinfo);
    if (ret < 0) {
        eprintf("FBIOGET_FSCREENINFO !!!\n");
        return -1;
    }

    size = varinfo.xres*varinfo.yres*(varinfo.bits_per_pixel/8);
    dprintf("\n");
    dprintf("### BUF SIZE = %d Bytes !!! \n", size);
    dprintf("\n");

    return size;
}

int draw_fill_color(unsigned char *buf_addr, int curWidth, int curHeight)
{
    unsigned int i, j;
    unsigned char *p;
    app_grpx_t *grpx = &grpx_obj;

    if(buf_addr==NULL)
        return -1;

    p = (unsigned char *)buf_addr;
    for(i = 0; i < curHeight; i++) {
        for(j = 0; j < curWidth; j++) {
            switch(grpx->planeType)
            {
                case GRPX_FORMAT_RGB565:
                    *p++  = (RGB_KEY_16BIT_GRAY >> 0 ) & 0xFF;
                    *p++  = (RGB_KEY_16BIT_GRAY >> 8 ) & 0xFF;
                break;
                case GRPX_FORMAT_RGB888:
                    *p++  = (RGB_KEY_24BIT_GRAY >> 0  ) & 0xFF;
                    *p++  = (RGB_KEY_24BIT_GRAY >> 8  ) & 0xFF;
                    *p++  = (RGB_KEY_24BIT_GRAY >> 16 ) & 0xFF;
                    *p++  = 0x00;
                break;
                case GRPX_FORMAT_MAX:
                default:
                break;
            }
        }
    }

    return 0;
}
/* function to draw box on grapics plane */
int draw_grid(unsigned char *buf_addr, int flag, int width, int height, int startX, int startY, int numBlkPerRow)
{
    unsigned int i, j, k;
    unsigned char *p;
    int            lineWidth;
    int            numHoriGridLines;
    int            numVerGridLines;
    int            horiGridSpace;
    int            verGridSpace;
    int            planeWidth;
    int            color;
    int            x;

    app_grpx_t *grpx = &grpx_obj;

    if(buf_addr==NULL)
        return -1;

    planeWidth  = GRPX_PLANE_GRID_WIDTH;
    numHoriGridLines = 4; /* for 3x3 Grid, Need to make it configurable*/
    numVerGridLines  = 4; /* for 3x3 Grid, Need to make it configurable*/
    horiGridSpace    = width/(numVerGridLines- 1);
    verGridSpace     = height/(numHoriGridLines - 1);
    lineWidth        = 2;  /* In pixels */

    startX = startX / 100;
    startY = startY / 100;

    width = width / 100;
    height = height / 100;

    x = (100*planeWidth)/width;                 /*Required factor for vertical spacing(last tile is narrower) */

    p = (unsigned char *)(buf_addr)+ ((startX * grpx->bytes_per_pixel)+( startY * grpx->bytes_per_pixel * planeWidth));    /* New buffer address according to offsets*/

    if(flag == TRUE)
       color = GRPX_PLANE_GRID_COLOR;
    else
       color = GRPX_PLANE_GRID_COLOR_BLANK;

    for(k=0; k<numHoriGridLines; k++)
    {
       p = (unsigned char *)(buf_addr)
                           +  ((startX * grpx->bytes_per_pixel)+( startY * grpx->bytes_per_pixel * planeWidth)) /* To go to point where windows starts */
                           +  (k * grpx->bytes_per_pixel * (planeWidth * ((height/(numHoriGridLines - 1)))));   /* To go to next Horizontal Line       */

       /* Except first horizontal line, come back 2 lines (lineWidth) up
        * in top direction to start drawing line */
       if(k !=0)
          p = p - ( grpx->bytes_per_pixel * lineWidth * planeWidth);


        for(i=0; i<lineWidth; i++)
        {
            for(j=0; j<width; j++)
            {
                if(grpx->planeType == GRPX_FORMAT_RGB565)
                {
                    {
                        *p = color;
                        *(p + 1) = color;
                    }
                }
                if(grpx->planeType == GRPX_FORMAT_RGB888)
                {
                    {

                        *p = color;
                        *(p + 1) = color;
                        *(p + 2) = color;
                        *(p + 3) = color;
                    }
                }
                p += grpx->bytes_per_pixel;
             }
             p += grpx->bytes_per_pixel * (planeWidth - width);
         }

    }
    for(k=0; k<numVerGridLines; k++)
    {
#if GRPX_PLANE_GRID_SCD_TILE
       int verSpacing;
       if(k == 3)
       {
         verSpacing = width - lineWidth;
       }
       else
       {
#if 1
         float numBlk;
         int numHorBlkPerGridBox;

         /* Below double operation for ceiling is done to make left 2 colum of
          * same width and last colum will be adjusted as per resoultion
          * CIF has 11 block per row in SCD and QCIF has 6 block of SCD
          * In case of CIF colum will be (4 + 4 + 3) and QCIF (2 + 2 + 2)
          * Example 11%3 = 3.7;  ceil(3.7) = 4               */

         numBlk = ((float)numBlkPerRow)/((float)(numVerGridLines - 1)); /* fractional No. */
         numHorBlkPerGridBox = ceil(numBlk);          /* Round to next integer*/
#else
         int numBlk;
         int numHorBlkPerGridBox;

         /* Below operation for ceiling is done to make left 2 colum of
          * same width and last colum will be adjusted as per resoultion
          * CIF has 11 block per row in SCD and QCIF has 6 block of SCD
          * In case of CIF colum will be (4 + 4 + 3) and QCIF (2 + 2 + 2) */

         /* Calcualte remainder and subtract from denominator to get no
          * which would be added to Numerator to round to next divisible no.
          * Example 11%3 = 2;  3-2 = 1;    11+1 = 12;    12/3 = 4
          * Example 6%3 = 0;                6+0 = 6;      6/3 = 2 */

         if(numBlkPerRow%(numVerGridLines - 1))
            numBlk =(numVerGridLines - 1) - (numBlkPerRow%(numVerGridLines - 1));
         else
            numBlk = 0;

         numHorBlkPerGridBox = (numBlkPerRow + numBlk)/(numVerGridLines - 1);          /* Round to next integer*/
#endif
         /*Offset_2_Jump_2_Nxt_Verlin -> k *   Block Size calc  *  Num Blk per tile  */
         verSpacing = k * (((planeWidth/numBlkPerRow) * 100)/x) * numHorBlkPerGridBox;
       }
       p = (unsigned char *)(buf_addr) + (grpx->bytes_per_pixel * verSpacing)            /* New buffer address according to the offsets*/
       + ((startX * grpx->bytes_per_pixel)+( startY * grpx->bytes_per_pixel * planeWidth));
#else
        p = (unsigned char *)(buf_addr) + (grpx->bytes_per_pixel * (k * (width/(numVerGridLines - 1)) - lineWidth))
       + ((startX * grpx->bytes_per_pixel)+( startY * grpx->bytes_per_pixel * planeWidth));
#endif
        for(i=0; i<height; i++)
        {
            for(j=0; j<lineWidth; j++)
            {
                if(grpx->planeType == GRPX_FORMAT_RGB565)
                {
                    {
                        *p = color;
                        *(p + 1) = color;
                    }
                }
                if(grpx->planeType == GRPX_FORMAT_RGB888)
                {
                    {

                        *p = color;
                        *(p + 1) = color;
                        *(p + 2) = color;
                        *(p + 3) = color;
                    }
                }
                p += grpx->bytes_per_pixel;
             }
             p += (grpx->bytes_per_pixel * (planeWidth - lineWidth));
         }
    }
    return 0;

}

int draw_osd(unsigned char *buf_addr, int flag, int startX, int startY, int width, int height, int numChar, unsigned char *osdBufAddr)
{
    unsigned int i, j, k;
    unsigned char *p;
    unsigned char color;
    int            planeWidth;
    app_grpx_t *grpx = &grpx_obj;

    if(buf_addr==NULL)
        return -1;

    planeWidth  = GRPX_PLANE_GRID_WIDTH;

    startX = startX / 100;
    startY = startY / 100;

    for(i = 0; i < numChar; i++)
    {
       p = (unsigned char *)(buf_addr) + ((startX * grpx->bytes_per_pixel)+( startY * grpx->bytes_per_pixel * planeWidth)) + (i * width * grpx->bytes_per_pixel);
       for(j = 0; j < height; j++)
       {
          for(k = 0; k < width; k++)
          {
             if(flag == TRUE)
             {
                color = *osdBufAddr++;
                *p = color;
                color = *osdBufAddr++;
                *(p + 1) = color;

             }
             else
             {
                color = GRPX_PLANE_GRID_COLOR_BLANK;
                *p       = color;
                *(p + 1) = color;
             }
             p += grpx->bytes_per_pixel;
          }
          p += grpx->bytes_per_pixel * (planeWidth - width);
       }
    }
    return 0;
}
/* function to draw box on grapics plane */
int draw_line(unsigned char *buf_addr, int flag,
             int            x1,
             int            y1,
             int            x2,
             int            y2,
             int            color)  /* Not using color input.*/
{
    unsigned int i;
    unsigned char *p;
    int            startX;
    int            startY;
    int            endX;
    int            endY;
    int            lineWidth;
    int            planeWidth;
    int            dx,dy;
    int            sdx,sdy;
    int            dxabs,dyabs;
    int            x,y;
    int            px,py;

    app_grpx_t *grpx = &grpx_obj;

    if(buf_addr==NULL)
        return -1;

    planeWidth  = GRPX_PLANE_GRID_WIDTH;

    lineWidth        = 10;  /* In pixels */

    dx = x2-x1;      /* the horizontal distance of the line */
    dy = y2-y1;      /* the vertical distance of the line */
    dxabs = abs(dx);
    dyabs = abs(dy);
    sdx = sgn(dx);
    sdy = sgn(dy);
    x = dyabs>>1;
    y = dxabs>>1;
    px = x1;
    py = y1;

    startX = x1;
    endX   = x2;

    startY = y1;
    endY   = y2;

    p = (unsigned char *)(buf_addr) + ((startX * grpx->bytes_per_pixel)+( startY * grpx->bytes_per_pixel * planeWidth));

    if(flag == TRUE)
       color = 0x9F;//GRPX_PLANE_GRID_COLOR;
    else
       color = GRPX_PLANE_GRID_COLOR_BLANK;

    if (dxabs>=dyabs) /* the line is more horizontal than vertical */
    {
      for(i = 0; i < dxabs; i++)
      {
        y += dyabs;
        if (y >= dxabs)
        {
          y -= dxabs;
          py += sdy;
        }
        px += sdx;
        p  = (unsigned char *)(buf_addr) + ((px * grpx->bytes_per_pixel)+( py * grpx->bytes_per_pixel * planeWidth));
        *p = color;
        *(p + 1) = color;
      }
    }
    else /* the line is more vertical than horizontal */
    {
      for(i = 0; i < dyabs; i++)
      {
        x += dxabs;
        if (x >= dyabs)
        {
          x -= dyabs;
          px += sdx;
        }
        py += sdy;
        p  = (unsigned char *)(buf_addr) + ((px * grpx->bytes_per_pixel)+( py * grpx->bytes_per_pixel * planeWidth));
        *p = color;
        *(p + 1) = color;
      }
    }

    return 0;
}

int grpx_convertPercentToAbs(Polygon_Points * polygonPoints, Polygon_Points * polygonPnts)
{
    int i;
    polygonPnts->noOfPoints = polygonPoints->noOfPoints;
    for(i = 0; i < (polygonPoints->noOfPoints); i++)
    {
       polygonPnts->pixLocation[i].x = (polygonPoints->pixLocation[i].x * GRPX_PLANE_GRID_WIDTH)/100;
       polygonPnts->pixLocation[i].y = (polygonPoints->pixLocation[i].y * GRPX_PLANE_GRID_HEIGHT)/100;
       polygonPnts->pixLocation[i].x = (polygonPnts->pixLocation[i].x * polygonPoints->winWidth)/(GRPX_PLANE_GRID_WIDTH * 100);
       polygonPnts->pixLocation[i].y = (polygonPnts->pixLocation[i].y * polygonPoints->windHeight)/(GRPX_PLANE_GRID_HEIGHT * 100);
       polygonPnts->pixLocation[i].x += polygonPoints->startX / 100;
       polygonPnts->pixLocation[i].y += polygonPoints->startY / 100;

    }
    return 0;
}

int draw_polygon(unsigned char *buf_addr, int flag, Polygon_Points * polygonPoints)
{
    int i;
    for(i = 0; i < (polygonPoints->noOfPoints - 1); i++)
    {
       draw_line(buf_addr, flag, 
                 polygonPoints->pixLocation[i].x, 
                 polygonPoints->pixLocation[i].y,
                 polygonPoints->pixLocation[i+1].x, 
                 polygonPoints->pixLocation[i+1].y,
                 polygonPoints->color
                );  /* Not using color input.*/
    }
    return 0;
}
/* function to draw box on grapics plane */
int draw_box(unsigned char *buf_addr, int flag,
             int            startX,
             int            startY,
             int            width,
             int            height)
{
    unsigned int i, j, k;
    unsigned char *p;
    int            lineWidth;
    int            numHoriGridLines;
    int            numVerGridLines;
    int            planeWidth;
    int            color;
    app_grpx_t *grpx = &grpx_obj;

    if(buf_addr==NULL)
        return -1;

    planeWidth  = GRPX_PLANE_GRID_WIDTH;

    numHoriGridLines = 2;
    numVerGridLines  = 2;
    lineWidth        = 1;  /* In pixels */

    p = (unsigned char *)(buf_addr) + ((startX * grpx->bytes_per_pixel)+( startY * grpx->bytes_per_pixel * planeWidth));

    if(flag == TRUE)
       color = GRPX_PLANE_GRID_COLOR;
    else
       color = GRPX_PLANE_GRID_COLOR_BLANK;

    for(k=0; k<numHoriGridLines; k++)
    {
        for(i=0; i<lineWidth; i++)
        {
            for(j=0; j<width; j++)
            {
                if(grpx->planeType == GRPX_FORMAT_RGB565)
                {
                    {
                        *p = color;
                        *(p + 1) = color;
                    }
                }
                if(grpx->planeType == GRPX_FORMAT_RGB888)
                {
                    {

                        *p = color;
                        *(p + 1) = color;
                        *(p + 2) = color;
                        *(p + 3) = color;
                    }
                }
                p += grpx->bytes_per_pixel;
             }
             p += (grpx->bytes_per_pixel * (planeWidth - width));
         }
         p += grpx->bytes_per_pixel * (planeWidth * (height - lineWidth));
    }

    for(k=0; k<numVerGridLines; k++)
    {
        p = (unsigned char *)(buf_addr) + \
                  ((((k * width)  + startX) * grpx->bytes_per_pixel) +  \
                       ( startY * grpx->bytes_per_pixel * planeWidth));
        for(i=0; i<height; i++)
        {
            for(j=0; j<lineWidth; j++)
            {
                if(grpx->planeType == GRPX_FORMAT_RGB565)
                {
                    {
                        *p = color;
                        *(p + 1) = color;
                    }
                }
                if(grpx->planeType == GRPX_FORMAT_RGB888)
                {
                    {

                        *p = color;
                        *(p + 1) = color;
                        *(p + 2) = color;
                        *(p + 3) = color;
                    }
                }
                p += grpx->bytes_per_pixel;
             }
             p += (grpx->bytes_per_pixel * (planeWidth - lineWidth));
         }
    }
    return 0;
}

int draw_img(unsigned char *buf_addr,
             unsigned char *img_addr,
             int            sx,
             int            sy,
             int            wi,
             int            ht,
             int            planeWidth)
{
    unsigned int i, j;
    unsigned char *p;
    app_grpx_t *grpx = &grpx_obj;

    if(buf_addr==NULL || img_addr==NULL)
        return -1;

    p = (unsigned char *)(buf_addr + ((sx * grpx->bytes_per_pixel)+( sy * grpx->bytes_per_pixel * planeWidth)));

    for(j=0; j<ht; j++)
    {
        for(i=0; i<wi; i++)
        {
            if(grpx->planeType == GRPX_FORMAT_RGB565)
            {
                {
                    *p = *img_addr;
                    *(p + 1) = *(img_addr + 1);
                }
            }
            if(grpx->planeType == GRPX_FORMAT_RGB888)
            {
                {

                    *p = *img_addr;
                    *(p + 1) = *(img_addr + 1);
                    *(p + 2) = *(img_addr + 2);
                    *(p + 3) = *(img_addr + 3);
                }
            }
            p        += grpx->bytes_per_pixel;
            img_addr += grpx->bytes_per_pixel;
        }
        p += ((planeWidth-wi) * grpx->bytes_per_pixel);
    }

    return 0;
}

Int32 grpx_fb_scale(VDIS_DEV devId,
                    UInt32   startX,
                    UInt32   startY,
                    UInt32   outWidth,
                    UInt32   outHeight)
{

    struct ti81xxfb_scparams scparams;
    Int32                    fd = 0, status = 0;
#ifdef ENABLE_FBDEV_2
    Int32                    fd2 = 0;
    int r2 = -1;
#endif
    app_grpx_t *grpx = &grpx_obj;
    int dummy;
    struct ti81xxfb_region_params  regp;
    char buffer[10];
    int r = -1;
    VSYS_PARAMS_S sysInfo;

    #if defined(TI_814X_BUILD)
    if((devId == VDIS_DEV_HDMI) || (devId == VDIS_DEV_DVO2))
        devId = VDIS_DEV_HDMI;
    else
    if((devId == VDIS_DEV_SD))
        devId = VDIS_DEV_SD;
    else
        return 0; /* not supported */
    #endif

    #if defined(TI_8107_BUILD)
    if((devId == VDIS_DEV_HDMI) || (devId == VDIS_DEV_HDCOMP))
        devId = VDIS_DEV_HDMI;
    else
    if((devId == VDIS_DEV_SD))
        devId = VDIS_DEV_SD;
    else
        return 0; /* not supported */
    #endif

    #ifdef TI_816X_BUILD
    if(devId==VDIS_DEV_HDMI)
        devId = VDIS_DEV_HDMI;
    else
        return 0;
    #endif

    Vsys_getContext(&sysInfo);

    if (devId == VDIS_DEV_HDMI)
    {
        fd = grpx->fd;
        Vdis_isGrpxOn(0, buffer, &r);
    }
#ifdef ENABLE_FBDEV_2
//    if (devId == VDIS_DEV_SD)
    {
        fd2 = grpx->fd2;
        Vdis_isGrpxOn(2, buffer, &r2);
    }
#endif
    /* Set Scalar Params for resolution conversion
     * inHeight and inWidth should remain same based on grpx buffer type
     */

#if defined(GRPX_FB_SEPARATE_BUFFER_NON_TIED_GRPX)
    if (devId == VDIS_DEV_HDMI){
        if ((sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) ||
            (sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH))
        {
            scparams.inwidth  = GRPX_PLANE_WIDTH;
            scparams.inheight = GRPX_PLANE_HEIGHT;
        }
        else
        {
            scparams.inwidth  = GRPX_PLANE_HD_WIDTH;
            scparams.inheight = GRPX_PLANE_HD_HEIGHT;
        }
    }
    if (devId == VDIS_DEV_SD){
        scparams.inwidth  = GRPX_PLANE_SD_WIDTH;
        scparams.inheight = GRPX_PLANE_SD_HEIGHT;
    }
#endif

#if defined(GRPX_FB_SINGLE_BUFFER_TIED_GRPX) || defined (GRPX_FB_SINGLE_BUFFER_NON_TIED_GRPX)
     scparams.inwidth  = GRPX_PLANE_WIDTH;
     scparams.inheight = GRPX_PLANE_HEIGHT;
#endif

    // this "-GRPX_SC_MARGIN_OFFSET" is needed since scaling can result in +2 extra pixels, so we compensate by doing -2 here
    scparams.outwidth = outWidth - GRPX_SC_MARGIN_OFFSET;
    scparams.outheight = outHeight - GRPX_SC_MARGIN_OFFSET;
    scparams.coeff = NULL;

    if (ioctl(fd, TIFB_GET_PARAMS, &regp) < 0) {
        eprintf("TIFB_GET_PARAMS !!!\n");
    }

    regp.pos_x = startX;
    regp.pos_y = startY;
    regp.transen = TI81XXFB_FEATURE_ENABLE;
    regp.transcolor = RGB_KEY_24BIT_GRAY;
    regp.scalaren = TI81XXFB_FEATURE_ENABLE;

    /*not call the IOCTL, ONLY if 100% sure that GRPX is off*/
    if (!((r == 0) && (atoi(buffer) == 0))) {
        if (ioctl(fd, FBIO_WAITFORVSYNC, &dummy)) {
            eprintf("FBIO_WAITFORVSYNC !!!\n");
            return -1;
        }
    }
    if ((status = ioctl(fd, TIFB_SET_SCINFO, &scparams)) < 0) {
        eprintf("TIFB_SET_SCINFO !!!\n");
    }


    if (ioctl(fd, TIFB_SET_PARAMS, &regp) < 0) {
        eprintf("TIFB_SET_PARAMS !!!\n");
    }
#ifdef ENABLE_FBDEV_2
    /*not call the IOCTL, ONLY if 100% sure that GRPX is off*/
    if (!((r2 == 0) && (atoi(buffer) == 0))) {
        if (ioctl(fd2, FBIO_WAITFORVSYNC, &dummy)) {
            eprintf("FBIO_WAITFORVSYNC !!!\n");
            return -1;
        }
    }
    if ((status = ioctl(fd2, TIFB_SET_SCINFO, &scparams)) < 0) {
        eprintf("TIFB_SET_SCINFO !!!\n");
    }


    if (ioctl(fd2, TIFB_SET_PARAMS, &regp) < 0) {
        eprintf("TIFB_SET_PARAMS !!!\n");
    }
#endif
    return (status);

}

int grpx_fb_init(grpx_plane_type planeType)
{
    app_grpx_t *grpx = &grpx_obj;
    VSYS_PARAMS_S sysInfo;

    Vsys_getContext(&sysInfo);

    memset(grpx, 0, sizeof(app_grpx_t));

    // need to start and stop FBDev once for the RGB565 and SC to take effect

    if(planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }
    else
    {
        grpx->planeType = planeType;
    }

    /* For TI816x */
#if defined(GRPX_FB_SINGLE_BUFFER_NON_TIED_GRPX)
    grpx_fb_start_singleBufNonTied();
#endif

    /* For TI814x - obsolete fom 4.0 GA onwards */
#if defined(GRPX_FB_SINGLE_BUFFER_TIED_GRPX)
    grpx_fb_start_singleBufTied();
#endif

/* Both DM814x & DM8107 use Non Tied GRPX now. In older releases, DM814x used tied GRPX.
  * In DM8107, due to write back capture used for SDTV (showing same content as HDTV) in few use cases, 
  * single buffer gfx is valid and used.
  * In DM814x, write back capture not used and gfx is not available for SDTV for 4ch use case
  */
#if defined (GRPX_FB_SEPARATE_BUFFER_NON_TIED_GRPX)
    if ((sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) ||
        (sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH))
    {
        grpx_fb_start_singleBuf();
    }
    else
    {
        grpx_fb_start_separateBufNonTied();
    }
#endif


    return 0;
}

void grpx_fb_exit(void)
{
    VSYS_PARAMS_S sysInfo;

    Vsys_getContext(&sysInfo);

    dprintf("\n");
    dprintf("grpx_fb_exit ... \n");

#if defined(GRPX_FB_SINGLE_BUFFER_NON_TIED_GRPX)
    grpx_fb_stop_singleBufNonTied();
#endif

#if defined(GRPX_FB_SINGLE_BUFFER_TIED_GRPX)
    grpx_fb_stop_singleBufTied();
#endif

#if defined (GRPX_FB_SEPARATE_BUFFER_NON_TIED_GRPX)
    if ((sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) ||
        (sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH))
    {
        grpx_fb_stop_singleBuf();
    }
    else
    {
        grpx_fb_stop_separateBufNonTied();
    }
#endif

    return;
}

int grpx_fb_draw_grid(int width,int height, int startX, int startY, int numBlkPerRow)
{
    VDIS_DEV devId;
    app_grpx_t *grpx = &grpx_obj;

#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_grid ... \n");
#endif
    devId = VDIS_DEV_HDMI;

    if(grpx->planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }

    if (devId == VDIS_DEV_HDMI)
    {
#if GRPX_PLANE_DEBUG
       printf(" Width %d height %d StartX %d StartY %d BlkPerRow %d\n",
                        width, height, startX, startY, numBlkPerRow );
#endif
        draw_grid(grpx->buf[0], TRUE, width, height, startX, startY, numBlkPerRow);

//       grpx_grid_fb_start_singleBufNonTied();
    }
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_grid ... Done !!! \n");
#endif
    return 0;
}


int grpx_fb_draw_grid_exit(int width,int height, int startX, int startY, int numBlkPerRow)
{
    printf("\n");
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_grid_exit ... \n");
#endif
    {
        app_grpx_t *grpx = &grpx_obj;

#ifdef GRPX_PLANE_DEBUG
       printf("Exit Width %d height %d StartX %d StartY %d BlkPerRow %d\n",
                        width, height, startX, startY, numBlkPerRow );
#endif
        draw_grid(grpx->buf[0], FALSE, width, height, startX, startY, numBlkPerRow);

#ifdef GRPX_PLANE_DEBUG
        dprintf("grpx_fb_grid_exit ... Done!!!\n");
#endif
        printf("\n");

     }
//    grpx_grid_fb_stop_singleBufNonTied();

    return 0;
}

int grpx_fb_draw_line(int startX,
                      int startY,
                     int endX,
                     int endY,
                     int color)/* Not using color input.*/

{
    VDIS_DEV devId;
    app_grpx_t *grpx = &grpx_obj;

//    dprintf("grpx_fb_draw_line ... \n");

    devId = VDIS_DEV_HDMI;

    if(grpx->planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }

    if (devId == VDIS_DEV_HDMI)
    {
#if 0
       printf(" startX %d startY %d endX %d endY %d \n",
                        startX, startY, endX, endY );
#endif
       draw_line(grpx->buf[0], TRUE,
                 startX,
                 startY,
                 endX,
                 endY,
                 color/* Not using color input.*/
                 );

//       grpx_grid_fb_start_singleBufNonTied();
    }
//    dprintf("grpx_fb_draw_line ... Done !!! \n");

    return 0;
}

int grpx_fb_draw_line_exit(int startX,
                      int startY,
                     int endX,
                     int endY,
                     int color)/* Not using color input.*/

{
    VDIS_DEV devId;
    app_grpx_t *grpx = &grpx_obj;
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_line_exit ... \n");
#endif
    devId = VDIS_DEV_HDMI;

    if(grpx->planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }

    if (devId == VDIS_DEV_HDMI)
    {
#ifdef GRPX_PLANE_DEBUG
       printf(" Line Draw: startX %d startY %d endX %d endY %d \n",
                        startX, startY, endX, endY );
#endif
       draw_line(grpx->buf[0], FALSE,
                 startX,
                 startY,
                 endX,
                 endY,
                 color/* Not using color input.*/
                 );

//       grpx_grid_fb_start_singleBufNonTied();
    }
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_line_exit ... Done !!! \n");
#endif
    return 0;
}

int grpx_fb_draw_polygon_abs(Polygon_Points * polygonPoints)
{
    VDIS_DEV devId;
    app_grpx_t *grpx = &grpx_obj;

#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_polygon_abs ... \n");
#endif
    devId = VDIS_DEV_HDMI;

    if(grpx->planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }

    if (devId == VDIS_DEV_HDMI)
    {
       draw_polygon(grpx->buf[0], TRUE, polygonPoints);

//       grpx_grid_fb_start_singleBufNonTied();
    }
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_polygon_abs ... Done !!! \n");
#endif
    return 0;
}

int grpx_fb_draw_polygon_abs_exit(Polygon_Points * polygonPoints)
{
    VDIS_DEV devId;
    app_grpx_t *grpx = &grpx_obj;

#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_polygon_abs_exit ... \n");
#endif  
    devId = VDIS_DEV_HDMI;

    if(grpx->planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }

    if (devId == VDIS_DEV_HDMI)
    {
       draw_polygon(grpx->buf[0], FALSE, polygonPoints);

//       grpx_grid_fb_start_singleBufNonTied();
    }
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_polygon_abs_exit ... Done !!! \n");
#endif
    return 0;
}

int grpx_fb_draw_polygon(Polygon_Points * polygonPoints)
{
    VDIS_DEV devId;
    app_grpx_t *grpx = &grpx_obj;
    Polygon_Points polygonPnts;
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_polygon ... \n");
#endif
    devId = VDIS_DEV_HDMI;

    if(grpx->planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }

    if (devId == VDIS_DEV_HDMI)
    {
       grpx_convertPercentToAbs(polygonPoints, &polygonPnts);
       draw_polygon(grpx->buf[0], TRUE, &polygonPnts);

//       grpx_grid_fb_start_singleBufNonTied();
    }
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_polygon ... Done !!! \n");
#endif
    return 0;
}


int grpx_fb_draw_polygon_exit(Polygon_Points * polygonPoints)
{
    VDIS_DEV devId;
    app_grpx_t *grpx = &grpx_obj;
    Polygon_Points polygonPnts;
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_polygon_exit ... \n");
#endif
    devId = VDIS_DEV_HDMI;

    if(grpx->planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }

    if (devId == VDIS_DEV_HDMI)
    {
       grpx_convertPercentToAbs(polygonPoints, &polygonPnts);
       draw_polygon(grpx->buf[0], FALSE, &polygonPnts);

//       grpx_grid_fb_start_singleBufNonTied();
    }
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_polygon_exit ... Done !!! \n");
#endif
    return 0;
}

int grpx_fb_draw_osd(int startX, int startY, int width, int height, int numChar, unsigned char * bufAddr)
{
    VDIS_DEV devId;
    app_grpx_t *grpx = &grpx_obj;
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_osd ... \n");
#endif
    devId = VDIS_DEV_HDMI;

    if(grpx->planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }
#ifdef GRPX_PLANE_DEBUG
       printf(" Draw OSD: Width %d height %d startX %d startY %d Buff Addr %d\n",
                       width, height, startX, startY, (UInt32)bufAddr);
#endif

    if (devId == VDIS_DEV_HDMI)
    {
#ifdef ENABLE_FBDEV_2
       draw_osd(grpx->buf[2], TRUE, startX, startY, width, height, numChar, bufAddr);
#else
       draw_osd(grpx->buf[0], TRUE, startX, startY, width, height, numChar, bufAddr);
#endif
    }
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_osd ... Done !!! \n");
#endif
    return 0;
}


int grpx_fb_draw_osd_exit(int startX, int startY, int width, int height, int numChar, unsigned char * bufAddr)
{
    VDIS_DEV devId;
    app_grpx_t *grpx = &grpx_obj;

#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_osd_exit ... \n");
#endif
    devId = VDIS_DEV_HDMI;

    if(grpx->planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }
#ifdef GRPX_PLANE_DEBUG
       printf(" Width %d height %d startX %d startY %d BuffAddr %d\n",
                       width, height, startX, startY, (UInt32)bufAddr);
#endif

    if (devId == VDIS_DEV_HDMI)
    {
#ifdef ENABLE_FBDEV_2
       draw_osd(grpx->buf[2], FALSE, startX, startY, width, height, numChar, bufAddr);
#else
       draw_osd(grpx->buf[0], FALSE, startX, startY, width, height, numChar, bufAddr);
#endif
    }
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_draw_osd_exit ... Done !!! \n");
#endif
    return 0;
}
int grpx_fb_draw_box(int width,
                     int height,
                     int startX,
                     int startY)

{
    VDIS_DEV devId;
    app_grpx_t *grpx = &grpx_obj;

    devId = VDIS_DEV_HDMI;

    if(grpx->planeType >= GRPX_FORMAT_MAX)
    {
        return -1;
    }
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_box ... \n");
#endif

    if (devId == VDIS_DEV_HDMI)
    {
       draw_box(grpx->buf[0], TRUE,
                 startX,
                 startY,
                 width,
                 height
                 );
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_box ...Done \n");
#endif

//       grpx_grid_fb_start_singleBufNonTied();
    }

    return 0;
}

int grpx_fb_draw_box_exit(int width,
                          int height,
                          int startX,
                          int startY)
{
#ifdef GRPX_PLANE_DEBUG
    dprintf("grpx_fb_box_exit ... \n");
#endif
    {
        app_grpx_t *grpx = &grpx_obj;

        draw_box(grpx->buf[0], FALSE,
                 startX,
                 startY,
                 width,
                 height
                 );
#ifdef GRPX_PLANE_DEBUG
        dprintf("grpx_fb_box_exit ... Done!!!\n");
#endif
     }
//    grpx_grid_fb_stop_singleBufNonTied();

    return 0;
}


int grpx_fb_stop(UInt32 displayId)
{
    #if defined(TI_814X_BUILD)
    /* Disable graphics through sysfs entries */
    if (displayId == VDIS_DEV_HDMI || displayId == VDIS_DEV_DVO2 ) {
        Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX0, VDIS_OFF);
    }
    #endif

    #if defined(TI_8107_BUILD)
    /* Disable graphics through sysfs entries */
    if (displayId == VDIS_DEV_HDMI || displayId == VDIS_DEV_HDCOMP ) {
        Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX0, VDIS_OFF);
    }
    #endif

    #ifdef TI_816X_BUILD
    /* Disable graphics through sysfs entries */
    Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX0, VDIS_OFF);
    Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX1, VDIS_OFF);
    #endif

//    if (displayId == VDIS_DEV_SD ) 
    {
        Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX2, VDIS_OFF);
    }

    return 0;
}

int grpx_fb_start(UInt32 displayId)
{
    #if defined(TI_814X_BUILD)
    /* Enable graphics through sysfs entries */
    if (displayId == VDIS_DEV_HDMI || displayId == VDIS_DEV_DVO2 ) {
        Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX0, VDIS_ON);
    }
    #endif

    #if defined(TI_8107_BUILD)
    /* Enable graphics through sysfs entries */
    if (displayId == VDIS_DEV_HDMI || displayId == VDIS_DEV_HDCOMP) {
        Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX0, VDIS_ON);
    }
    #endif

    #ifdef TI_816X_BUILD
    /* Enable graphics through sysfs entries */
    Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX0, VDIS_ON);
    Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX1, VDIS_ON);
    #endif

//    if (displayId == VDIS_DEV_SD ) 
    {
        Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX2, VDIS_ON);
    }

    return 0;
}

#if 0
Int32 grpx_fb_demo()
{
    UInt32 devId;
    UInt32 outWidth, outHeight;
    UInt32 startX, startY;
    UInt32 offsetX, offsetY;
    UInt32 loopCount, i;
    UInt32 runCount;

    devId = VDIS_DEV_SD;

    runCount = 10000;

    loopCount = 100;
    offsetX = offsetY = 1;

    /* putting in a loop for test */
    while(runCount--)
    {
        /* putting in another loop to change size and position every few msecs */
        for(i=1; i<=loopCount; i++)
        {
            Demo_swMsGetOutSize(Vdis_getResolution(devId), &outWidth, &outHeight);

            startX = offsetX*i;
            startY = offsetY*i;

            outWidth  -= startX*2;
            outHeight -= startY*2;

            grpx_fb_scale(devId, startX, startY, outWidth, outHeight);
        }
        for(i=loopCount; i>=1; i--)
        {
            Demo_swMsGetOutSize(Vdis_getResolution(devId), &outWidth, &outHeight);

            startX = offsetX*i;
            startY = offsetY*i;

            outWidth  -= startX*2;
            outHeight -= startY*2;

            grpx_fb_scale(devId, startX, startY, outWidth, outHeight);
        }

        /* restore to original */
        Demo_swMsGetOutSize(Vdis_getResolution(devId), &outWidth, &outHeight);

        dprintf("[reset] %d x %d\n", outWidth, outHeight);
        grpx_fb_scale(devId, 0, 0, outWidth, outHeight);
    }

    return 0;
}
#endif

