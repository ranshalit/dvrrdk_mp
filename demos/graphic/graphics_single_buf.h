/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2011 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#ifndef _GRAPHIC_SINGLEBUF_H_
#define _GRAPHIC_SINGLEBUF_H_

#define NUM_GRPX_DISPLAYS      (2)


#define GRPX_PLANE_WIDTH    (800)
#define GRPX_PLANE_HEIGHT   (600)

#define GRPX_STARTX_0        (20u)
#define GRPX_STARTY_0        (20u)
#define GRPX_STARTX_1        (GRPX_PLANE_WIDTH-250)
#define GRPX_STARTY_1        (GRPX_STARTY_0)
#define GRPX_STARTX_2        (GRPX_STARTX_0)
#define GRPX_STARTY_2        (GRPX_PLANE_HEIGHT-75)
#define GRPX_STARTX_3        (GRPX_STARTX_1)
#define GRPX_STARTY_3        (GRPX_STARTY_2)


Int32 grpx_fb_start_singleBuf();
Int32 grpx_fb_stop_singleBuf();
Int32 grpx_fb_draw_singleBuf();

#endif /*   _GRAPHIC_H_ */

