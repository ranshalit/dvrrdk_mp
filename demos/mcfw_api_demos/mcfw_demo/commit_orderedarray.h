/*
 * commit_orderedarray.h
 *
 *  Created on: Jun 30, 2014
 *      Author: ubuntu
 */

#ifndef COMMIT_ORDEREDARRAY_H_
#define COMMIT_ORDEREDARRAY_H_

void add_file_to_array(char* path);
void remove_file_from_array(char* path, unsigned long long *length);
int init_ordered_array(char* path);

#endif /* COMMIT_ORDEREDARRAY_H_ */
