/* Compile options needed: none
 *
 * This example program uses the C run-time library function qsort()
 * to sort an array of structures.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <commit_orderedarray.h>
#include <errno.h>

#define MAX_PATH_SIZE (64)
#define MAX_ARRAY_SIZE (256)
typedef int (*compfn)(const void*, const void*);

struct fileInfo {
				char path[MAX_PATH_SIZE];
                time_t create_time;
              };

struct fileInfo g_fileInfoArray[MAX_ARRAY_SIZE];
unsigned int tail_index = 0;
unsigned int head_index = 0;

void printarray(void);
int  compare(struct fileInfo *, struct fileInfo *);

void sort_ordered_array()
{
	int length = 0;

	if (tail_index >= head_index)
	{
		length = tail_index - head_index;
	}
	else
	{
		length = MAX_ARRAY_SIZE - (head_index - tail_index);
	}
	printf("start sort length %d",length);
	qsort((void *) g_fileInfoArray,              // Beginning address of array
		   length,                                 // Number of elements in array
	sizeof(struct fileInfo),              // Size of each element
	(compfn)compare );                  // Pointer to compare function

	printf("\nList after sorting:\n");
	printarray();
}

int compare(struct fileInfo *elem1, struct fileInfo *elem2)
{
   if ( elem1->create_time < elem2->create_time)
      return -1;

   else if (elem1->create_time > elem2->create_time)
      return 1;

   else
      return 0;
}

void printarray(void)
{
	int i, length, currIndex;
	if (tail_index >= head_index)
	{
		length = tail_index - head_index;
	}
	else
	{
		length = MAX_ARRAY_SIZE - (head_index - tail_index);
	}
	currIndex = head_index;
	for(i=0; i < length ; i++)
	{
		printf("index %d path %s create %ld  \n",
				currIndex , g_fileInfoArray[currIndex].path, g_fileInfoArray[currIndex].create_time);
		currIndex++;
		if (currIndex >= MAX_ARRAY_SIZE)
		{
			currIndex = 0;
		}
	}
}

/* This is just a sample code, modify it to meet your need */
int init_ordered_array(char* path)
{

	DIR* FD;
    struct dirent* in_file;
    char fullPath[128];

    head_index = 0;
    tail_index = 0;

    /* Scanning the in directory */
    if (NULL == (FD = opendir (path)))
    {
        fprintf(stderr, "Error : Failed to open input directory - %s\n", strerror(errno));

        return 1;
    }
    while ((in_file = readdir(FD)))
    {

        if (!strcmp (in_file->d_name, "."))
            continue;
        if (!strcmp (in_file->d_name, ".."))
            continue;
        /* Open directory entry file for common operation */
        /* TODO : change permissions to meet your need! */
        strcpy(fullPath, path);
        strcat(fullPath,"/");
        strcat(fullPath, in_file->d_name);
        add_file_to_array(fullPath);
    }
    sort_ordered_array();

    return 0;
}

int del_ordered_array(char* path)
{

    head_index = 0;
    tail_index = 0;

    return 0;
}

void add_file_to_array(char* path)
{
	struct stat buf;
	int length;
	/* add to head */

	printf("added file to array %s\n",path);
	stat(path, &buf);

	g_fileInfoArray[tail_index].create_time = buf.st_ctime;

	if (strlen(path) > MAX_PATH_SIZE)
	{
		printf("illegal length");
		exit(10);
	}
	strncpy(g_fileInfoArray[tail_index].path, path, MAX_PATH_SIZE);
	if (tail_index >= head_index)
	{
		length = tail_index - head_index;
	}
	else
	{
		length = MAX_ARRAY_SIZE - (head_index - tail_index);
	}
	if (length >= (MAX_ARRAY_SIZE-1) )
	{
		printf("error. Can not add any more files\n");
		exit(11);
	}

	tail_index++;
	if (tail_index >= MAX_ARRAY_SIZE)
	{
		tail_index = 0;
	}
	printarray();
}

void remove_file_from_array(char* path, unsigned long long *length)
{
	struct stat buf;

	if (tail_index == head_index)
	{
		printf("no file to remove\n");
		return;
	}

	strncpy(path, g_fileInfoArray[head_index].path, MAX_PATH_SIZE);
	stat(path, &buf);
	*length =  buf.st_size;
	printf("file removed %s size %lu\n",path, buf.st_size);
	/* remove only now */
	head_index++;
	if (head_index >= MAX_ARRAY_SIZE)
	{
		head_index = 0;
	}
	printarray();
}
