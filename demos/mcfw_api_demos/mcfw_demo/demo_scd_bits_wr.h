/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2011 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/



#ifndef _SCD_BIT_WR_H_
#define _SCD_BIT_WR_H_

#include <demo.h>
#include<math.h>

//#define IPC_BITS_DEBUG

#define DEMO_SCD_MOTION_DETECTION_SENSITIVITY(x,y)           ((x * y)/5) // 20%
#define DEMO_SCD_MOTION_DETECTION_SENSITIVITY_STEP           (10)        // 10%

#define DEMO_SCD_MOTION_TRACK_DEBUG  0
#define DEMO_SCD_MOTION_TRACK_DEBUG1 1

//#define DEMO_SCD_VA_TXT_NUM_OSD

#define SCD_BUF_HEAP_SR_ID          (0)
#define MAX_SCD_VA_EVENT            (4)
#define SCD_BUFFER_ALIGNMENT        (128)

#define MAX_SCD_VA_EVENT_GPX_DISPLAY_WIDTH    (186)
#define MAX_SCD_VA_EVENT_GPX_DISPLAY_HEIGHT   (14)
#define MAX_SCD_VA_EVENT_GPX_DISPLAY_BUF_SIZE (OSA_align((MAX_SCD_VA_EVENT_GPX_DISPLAY_WIDTH*MAX_SCD_VA_EVENT_GPX_DISPLAY_HEIGHT*2),SCD_BUFFER_ALIGNMENT))

//#define MAX_SCD_VA_EVENT_GPX_CHAR_WIDTH   (14)
//#define MAX_SCD_VA_EVENT_GPX_CHAR_HEIGHT  (14)
//#define MAX_SCD_VA_EVENT_GPX_CHAR_SIZE    (MAX_SCD_VA_EVENT_GPX_CHAR_WIDTH * MAX_SCD_VA_EVENT_GPX_CHAR_HEIGHT * 2)
//#define MAX_SCD_VA_EVENT_GPX_DISPLAY_TEXT_BUFF_LENGTH    (2048)
//#define MAX_SCD_VA_EVENT_GPX_DISPLAY_TEXT_BUF_SIZE (MAX_SCD_VA_EVENT_GPX_DISPLAY_TEXT_BUFF_LENGTH*MAX_SCD_VA_EVENT_GPX_DISPLAY_HEIGHT*2)

#define DEMO_SCD_MOTION_TRACK_GRPX_WIDTH  (1280) 
#define DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT  (720) 

#define DEMO_SCD_VA_MAX_POLGON                 (4)
#define DEMO_SCD_VA_MAX_OBJECTS                (64)

typedef struct {
    UInt32 width;
    UInt32 height;
    UInt32 startX;
    UInt32 startY;
} Scd_VaEventDisplayInfo;


typedef struct {
    Bool   initialise;
    UInt32 totalDataSize;
    UInt32 numFrames;
    UInt32 maxWidth;
    UInt32 maxHeight;
    Bool   chOnMosaic;
    UInt32  numHorzBlks;
    UInt32  curWinWidth;
    UInt32  curWinHeight;
    UInt32  curWinStartX;
    UInt32  curWinStartY;
    UInt32  scaledWinWidth;
    UInt32  scaledWinHeight; 
    UInt32  scaledWinStartX;
    UInt32  scaledWinStartY;

    UInt32  prevWinWidth;
    UInt32  prevWinHeight;
    UInt32  prevWinStartX;
    UInt32  prevWinStartY;
    UInt32  displayCnt;
    UInt32  prevFrameObjects;
    UInt32  prevFramePolygons;
    UInt32  scdVaEvent;
    UInt32  updateStaticObj;
    Polygon_Points staticObject[DEMO_SCD_VA_MAX_POLGON];
    Polygon_Points dynamicObject[DEMO_SCD_VA_MAX_OBJECTS];
    Scd_VaEventDisplayInfo eventInfoData;

    Bool    enableMotionTracking;
    Bool    enableGpxEvntDisplay;
    Bool    updateGrid;
    Bool    gridPresent;
    UInt32  prevWinIdTrack;
    UInt32  blkTag[ALG_LINK_SCD_MAX_BLOCKS_IN_FRAME];
} Scd_ChInfo;

/**
    \brief SCD VA Event type
*/
typedef enum {
    DEMO_SCD_VA_EVT_TZ = 0,
    DEMO_SCD_VA_EVT_OC,
    DEMO_SCD_VA_EVT_TD,
    DEMO_SCD_VA_EVT_IMD,
    DEMO_SCD_VA_EVT_SMETA
} Demo_ScdVaEventType;

typedef struct {

    OSA_ThrHndl wrThrHndl;
    OSA_SemHndl wrSem;
    Bool        exitWrThr;
    Bool        isWrThrStopDone;

    Scd_ChInfo chInfo[VENC_CHN_MAX];

    UInt32 statsStartTime;
#if DEMO_SCD_ENABLE_FILE_WRITE
    Bool fileWriteEnable;
    char fileWriteName[512];
    UInt32  chId;
#endif
//    UInt32  winId[VCAP_CHN_MAX];
//    UInt32  chTag[VCAP_CHN_MAX];
    UInt32  fileWriteChn;
    Bool    enableMotionTracking;
    UInt32  chIdTrack;
    UInt32  numberOfWindows;
    UInt32  prevNumberOfWindows;
    UInt32  startChId;
    Bool    layoutUpdated;

    UInt32  displayWidth;
    UInt32  displayHeight;
    Vsys_AllocBufInfo bufInfo[MAX_SCD_VA_EVENT];
    Vsys_AllocBufInfo chTxtBufInfo;
    Vsys_AllocBufInfo numBufInfo;
} Scd_Ctrl;


extern Scd_Ctrl gScd_ctrl;


Int32 Scd_resetStatistics();
Int32 Scd_printStatistics(Bool resetStats);

Int32 Scd_bitsWriteCreate(UInt32 useCase, UInt32 scdResolution);
Int32 Scd_bitsWriteDelete();
Void Scd_bitsWriteStop();
Void Scd_enableMotionTracking();
Void Scd_disableMotionTracking();
Bool Scd_isMotionTrackingEnabled();
Void Scd_trackLayout(UInt32 numberOfWindows, UInt32 startChId);
int Scd_TrackMotionOnDisplay(VALG_FRAMERESULTBUF_S *pBuf);
Void Scd_updateLayoutParam();
Int32 Scd_VaProcessMetaData(UInt32 winIdx, AlgLink_ScdResult * pObjScdResult, Scd_ChInfo  *pChInfo);
Void Scd_mosaicUpdated();

//Bool Scd_isMotionTrackingEnabled();                       /* not required */

Int32 Scd_trackCh(UInt32 tCh,  UInt32 flag);                              /* To receive the entered(tracking) channel id for switch through TrackChId */
void Scd_windowGrid(Bool flag, UInt32 numHorzBlks, Scd_ChInfo  *pChInfo);    /* To draw or undrwa the grid in caso of channel/layout switch */


#endif
