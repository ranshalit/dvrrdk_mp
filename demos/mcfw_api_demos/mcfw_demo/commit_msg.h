/*
 * commit_msg.h
 *
 *  Created on: Jul 7, 2014
 *      Author: ubuntu
 */

#ifndef COMMIT_MSG_H_
#define COMMIT_MSG_H_

/*
 * icd_msg.h
 *
 *  Created on: Jul 2, 2014
 *      Author: ubuntu
 */



#define MAX_KEEPALIVE_TIMEOUT			10

#define GSDU_KEEP_ALIVE_OPCODE			0x1
#define GSDU_PROCESS_STATUS				0x2
#define GSDU_SINGLE_ERROR				0x3
#define GSDU_SYSTEM_ERROR				0x4
#define GSDU_SOFTWARE_VERSION			0x5
#define GSDU_GENERAL_STATUS				0x6

#define HIU_KEEP_ALIVE_OPCODE			0x1
#define HIU_PROCESS_CMD					0x2
#define HIU_INFO_REQ_CMD				0x3
#define HIU_HIU_INFO_STATUS				0x4

#define PROCESS_ENTER 	1
#define PROCESS_EXIT 	2

#define RECORD_PROCESS_RECORD 	1
#define RECORD_PROCESS_COPY 	2
#define RECORD_PROCESS_DELETE 	3
#define RECORD_PROCESS_DELETE_FILE 	4
#define RECORD_PROCESS_UMOUNT 		5

#define INFO_REQ_ERROR_TABLE 	1
#define INFO_REQ_VERSION	 	2

#define STATUS_NOT_INPROCESS  	0
#define STATUS_INPROCESS  		1
#define STATUS_END_OK 			2
#define STATUS_END_FAIL 		3
#define STATUS_ABORT 			4


#pragma pack (1)
typedef struct _keep_alive
{
	unsigned short length;
	unsigned short opcode;
}keep_alive;



typedef struct _gsdu_process_status
{
	unsigned short length;
	unsigned short opcode;
	unsigned char process_type;
	unsigned char process_state;
}gsdu_process_status;

typedef struct _gsdu_general_status
{
	unsigned short length;
	unsigned short opcode;
	unsigned int capacity;
	unsigned char usb_connect_status;
}gsdu_general_status;

typedef struct _gsdu_version
{
	unsigned short length;
	unsigned short opcode;
	unsigned char major_number;
	unsigned char minor_number;
}gsdu_version;

typedef struct _gsdu_single_fault
{
	unsigned short length;
	unsigned short opcode;
	unsigned short fault_number;
	unsigned char  error_status; /* 1- not exist, 2 - exist */
}gsdu_single_fault;

typedef struct _gsdu_error_table
{
	unsigned short length;
	unsigned short opcode;
	unsigned short number_errors;
	unsigned short error_table[MAX_NUM_ERRORS];
}gsdu_error_table;

typedef struct _hiu_process_cmd
{
	unsigned short length;
	unsigned short opcode;
	unsigned char process_type;
	unsigned char process_cmd;
}hiu_process_cmd;

typedef struct _hiu_info_req
{
	unsigned short length;
	unsigned short opcode;
	unsigned char info_req;
}hiu_info_req;
/*
1		Weapon System Status:
0- Operational
1- Boresight
2- Calibration
2		Safety switch state – motion enable / disable	0 = off- motion disabled
1 = on- motion enabled
3		Arm switch state– fire enable / disable	0 = off- fire disabled
1 = on- fire enabled
4		Override switch state
Spare for SRCG – always 0	0 = off
1 = on
5		Selected Weapon station:
1- the first / left
2- the second / right
6		Weapon System Mode:
0- Manual
1- Power
2-Stab
 7		Selected ammunition on the selected weapon station
1 – Ball
2 – Tracer (future)
3 – SLP (future)
4 – SLP-T (future)
8		Selected fire type on the selected weapon station
1 – single
2 – Burst
3 - Full
9		Selected camera on the selected weapon station
1 – CCD (day camera)
2 – TI (Night camera)
3 -  Laser (LRF camera)
4 – BS camera
10		Selected FOV of the selected camera on the selected weapon station
1 – NFOV
2 – WFOV
3 – VWFOV
11		Day in Month 1-31
12		Month 1-12
13		Year
14		Hour 1-23
15		Minutes 1-60
*/

typedef struct _hiu_process_msg
{
	unsigned short length;
	unsigned short opcode;
	unsigned char weapon_status;
	unsigned char safet_switch_state;
	unsigned char arm_switch_state;
	unsigned char override_switch_state;
	unsigned char selected_switch_state;
	unsigned char weapon_system_mode;
	unsigned char selected_ammuniation;
	unsigned char selected_fire_type;
	unsigned char selected_camera;
	unsigned char selected_fov;
	unsigned char tod_day;
	unsigned char tod_month;
	unsigned char tod_year;
	unsigned char tod_hour;
	unsigned char tod_min;

}hiu_status_info_msg;
#pragma pack ()






#endif /* COMMIT_MSG_H_ */
