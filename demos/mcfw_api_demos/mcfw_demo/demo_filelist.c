/*
 * demo_filelist.c
 *
 *  Created on: Jun 29, 2014
 *      Author: ubuntu
 */
/* ranran */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
/*

#include <sys/stat.h>
struct stat structstat;
stat(filename, &structstat)
printf("last modified time: %s", ctime(&structstat.st_mtime));*/

#include "commit_linked_list.h"
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>


struct commit_list g_mylist;



struct commit_list{
	int	mtime;
	struct list_head list;
	};

void add_file_to_list(FILE* fd)
{
	/* add to head */
	time_t t = time(NULL);

    struct commit_list *new;
	new = (struct commit_list *)malloc(sizeof(struct commit_list));
	/*tm_sec	int	seconds after the minute	0-61*
tm_min	int	minutes after the hour	0-59
tm_hour	int	hours since midnight	0-23
tm_mday	int	day of the month	1-31
tm_mon	int	months since January	0-11
tm_year	int	years since 1900
tm_wday	int	days since Sunday	0-6
tm_yday	int	days since January 1	0-365
tm_isdst	int	Daylight Saving Time flag	*/
	/* TBD */
	new->mtime = t;
	list_add(&new->list, &g_mylist.list);
}


void add_to_list(struct commit_list *mylist, struct commit_list *new)
{

    struct commit_list *entry;
	struct list_head *pos;


	list_for_each(pos, &mylist->list){

		/* at this point: pos->next points to the next item's 'list' variable and
		 * pos->prev points to the previous item's 'list' variable. Here item is
		 * of type struct kool_list. But we need to access the item itself not the
		 * variable 'list' in the item! macro list_entry() does just that. See "How
		 * does this work?" below for an explanation of how this is done.
		 */
		 entry = list_entry(pos, struct commit_list, list);
		 if (entry->mtime < new->mtime)
		 {
			 list_add_tail(&(new->list), pos);
			         	break;
			 /*__list_add(struct list_head *new,
  			 			      struct list_head *prev,
			 			      struct list_head *next);*/
		 }

	}
	list_add_tail(&new->list, &mylist->list);
}


void print_list(struct commit_list *mylist)
{
	struct list_head *pos;
	struct commit_list *entry;
	printf("the list: ");
	list_for_each(pos, &mylist->list){

		/* at this point: pos->next points to the next item's 'list' variable and
		 * pos->prev points to the previous item's 'list' variable. Here item is
		 * of type struct kool_list. But we need to access the item itself not the
		 * variable 'list' in the item! macro list_entry() does just that. See "How
		 * does this work?" below for an explanation of how this is done.
		 */
		entry = list_entry(pos, struct commit_list, list);

		 /* given a pointer to struct list_head, type of data structure it is part of,
		  * and it's name (struct list_head's name in the data structure) it returns a
		  * pointer to the data structure in which the pointer is part of.
		  * For example, in the above line list_entry() will return a pointer to the
		  * struct kool_list item it is embedded in!
		  */

		 printf("%d", entry->mtime);

	}
	printf("\n");
}

/* This is just a sample code, modify it to meet your need */
int addFilesIntoList(struct commit_list *mylist,char* path)
{
    DIR* FD;
    struct dirent* in_file;
    char fullPath[128];
    struct stat structstat;
    struct commit_list *new;



    strcpy(fullPath, path);


    /* Scanning the in directory */
    if (NULL == (FD = opendir (path)))
    {
        fprintf(stderr, "Error : Failed to open input directory - %s\n", strerror(errno));

        return 1;
    }
    while ((in_file = readdir(FD)))
    {

        if (!strcmp (in_file->d_name, "."))
            continue;
        if (!strcmp (in_file->d_name, ".."))
            continue;
        /* Open directory entry file for common operation */
        /* TODO : change permissions to meet your need! */

        strcat(fullPath, in_file->d_name);
        stat(fullPath, &structstat);
        printf("last modified time: %s", ctime(&structstat.st_mtime));
        new = (struct commit_list *)malloc(sizeof(struct commit_list));
        new->mtime = structstat.st_mtime;
        add_to_list(mylist, new);


    }



    return 0;
}

int test2()
{

	struct commit_list mylist;
	INIT_LIST_HEAD(&mylist.list);
	struct commit_list *new;


	/* 1 */
	new = (struct commit_list *)malloc(sizeof(struct commit_list));
	new->mtime = 40;
	add_to_list(&mylist, new);
	print_list(new);
	/* 2 */
	new = (struct commit_list *)malloc(sizeof(struct commit_list));
	new->mtime = 30;
	add_to_list(&mylist, new);
	print_list(new);
	/* 3 */
	new = (struct commit_list *)malloc(sizeof(struct commit_list));
	new->mtime = 50;
	add_to_list(&mylist, new);
	print_list(new);
	/* 4 */
	new = (struct commit_list *)malloc(sizeof(struct commit_list));
	new->mtime = 35;
	add_to_list(&mylist, new);
	print_list(new);
	/* 5 */
	new = (struct commit_list *)malloc(sizeof(struct commit_list));
	new->mtime = 45;
	add_to_list(&mylist, new);
	print_list(&mylist);
	/* 6 */
	new = (struct commit_list *)malloc(sizeof(struct commit_list));
	new->mtime = 70;
	add_to_list(&mylist, new);
	print_list(&mylist);
	/* 7 */
	new = (struct commit_list *)malloc(sizeof(struct commit_list));
	new->mtime = 20;
	add_to_list(&mylist, new);
	print_list(&mylist);

	/* 8 !!! add to head ! */
	list_add(&new->list, &g_mylist.list);
	new->mtime = 22;
	list_add(&new->list, &g_mylist.list);
	print_list(&mylist);

	/* 9 */
	list_del(mylist.list.prev);
	print_list(new);
	free(new);
	/* 10 */
	list_del(mylist.list.prev);
	print_list(new);
	free(new);
	/* 11 */
	list_del(mylist.list.prev);
	print_list(new);
	free(new);
	return 0;
}



int init_list(char* path){





	INIT_LIST_HEAD(&g_mylist.list);
	/* or you could have declared this with the following macro
	 * LIST_HEAD(mylist); which declares and initializes the list
	 */
	addFilesIntoList(&g_mylist, path);
#if 0

	/* adding elements to mylist */
	for(i=5; i!=0; --i){
		tmp= (struct kool_list *)malloc(sizeof(struct kool_list));

		/* INIT_LIST_HEAD(&tmp->list);
		 *
		 * this initializes a dynamically allocated list_head. we
		 * you can omit this if subsequent call is add_list() or
		 * anything along that line because the next, prev
		 * fields get initialized in those functions.
		 */
		printf("enter to and from:");
		scanf("%d %d", &tmp->to, &tmp->from);

		/* add the new item 'tmp' to the list of items in mylist */
		list_add(&(tmp->list), &(mylist.list));
		/* you can also use list_add_tail() which adds new items to
		 * the tail end of the list
		 */
	}
	printf("\n");


	/* now you have a circularly linked list of items of type struct kool_list.
	 * now let us go through the items and print them out
	 */


	/* list_for_each() is a macro for a for loop.
	 * first parameter is used as the counter in for loop. in other words, inside the
	 * loop it points to the current item's list_head.
	 * second parameter is the pointer to the list. it is not manipulated by the macro.
	 */
	printf("traversing the list using list_for_each()\n");
	list_for_each(pos, &mylist.list){

		/* at this point: pos->next points to the next item's 'list' variable and
		 * pos->prev points to the previous item's 'list' variable. Here item is
		 * of type struct kool_list. But we need to access the item itself not the
		 * variable 'list' in the item! macro list_entry() does just that. See "How
		 * does this work?" below for an explanation of how this is done.
		 */
		 tmp= list_entry(pos, struct kool_list, list);

		 /* given a pointer to struct list_head, type of data structure it is part of,
		  * and it's name (struct list_head's name in the data structure) it returns a
		  * pointer to the data structure in which the pointer is part of.
		  * For example, in the above line list_entry() will return a pointer to the
		  * struct kool_list item it is embedded in!
		  */

		 printf("to= %d from= %d\n", tmp->to, tmp->from);

	}
	printf("\n");
	/* since this is a circularly linked list. you can traverse the list in reverse order
	 * as well. all you need to do is replace 'list_for_each' with 'list_for_each_prev'
	 * everything else remain the same!
	 *
	 * Also you can traverse the list using list_for_each_entry() to iterate over a given
	 * type of entries. For example:
	 */
	printf("traversing the list using list_for_each_entry()\n");
	list_for_each_entry(tmp, &mylist.list, list)
		 printf("to= %d from= %d\n", tmp->to, tmp->from);
	printf("\n");


	/* now let's be good and free the kool_list items. since we will be removing items
	 * off the list using list_del() we need to use a safer version of the list_for_each()
	 * macro aptly named list_for_each_safe(). Note that you MUST use this macro if the loop
	 * involves deletions of items (or moving items from one list to another).
	 */
	printf("deleting the list using list_for_each_safe()\n");
	list_for_each_safe(pos, q, &mylist.list){
		 tmp= list_entry(pos, struct kool_list, list);
		 printf("freeing item to= %d from= %d\n", tmp->to, tmp->from);
		 list_del(pos);
		 free(tmp);
	}
#endif
	return 0;
}
