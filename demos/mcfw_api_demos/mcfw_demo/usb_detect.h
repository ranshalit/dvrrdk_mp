/*
 * libusbdetect-1.0 API
 * Dec 2011
 * Elbit Systems Land & C4I
 */

#ifndef USB_DETECT_H
#define USB_DETECT_H


#define DEV_OPT_NUM 5
#define HID_CLASS 3
#define NETWORK_CLASS 2
#define MASS_STORGE_CLASS 8

/*!
\brief	DEVTYPE will be used as an enumeration for possible devices connected
*/
typedef enum
{
    STORAGE = 0,
    NETWORK = 1,
    MOUSE =   2,
    AHHD  =   3,
    LVDS   =  4,
    OTHER  =  5
}
DEVTYPE;


/*!
\brief	before using any of the functions, call init_usb_detect
*/
int init_usb_detect();


/*!
\brief	get_current_dev() will ceck which devices are connected at the moment
		It will change dev_array so that dev_array[DEV] is 1
		If the one  DEV device is connected
		For example: if a mouse and an AHHD are connected,
		get_current_dev will assign the array like this:
		dev_array[0] = 0
		dev_array[1] = 1
		dev_array[2] = 1
		dev_array[3] = 0
		If there was a problem- the function will return -1. otherwise - 0
*/
int get_current_dev(int dev_array[DEV_OPT_NUM]);



/*!
\brief 	block_for_dev() gives an alert when there is a change in connected device
		it will return once an event was detected, with:
		event_type = 1 -> connection
		event_type = 0 -> disconnection
		device_changed will hold the type of device that was changed
		If there was a problem- the function will return -1. otherwise - 0
*/
int wait_for_usb_event(DEVTYPE* device_changed, int* event_type);


/*!
\brief  print_dev_type will print_dev_type gets DEVTYPE and prints the device type to stdout
*/
void print_dev_type(DEVTYPE val);

/*!
\brief when finished, call exit_usb_detect
*/
void exit_usb_detect();


#endif
