/* ranran new file */
/**
  \file demo_vcap_venc_vdec_vdis.c
  \brief
  */
#include <sys/time.h>
#include <demo_vcap_venc_vdec_vdis.h>
#include <demo_scd_bits_wr.h>

/* Setting secondary out <CIF> for 30 frames - this is the validated frame rate;
any higher number will impact performance. */


#define     CIF_FPS_ENC_NTSC         (30)
#define     CIF_FPS_ENC_PAL          (25)

#define     CIF_BITRATE         (500)
#define     MJPEG_BITRATE       (100)



static Int64 get_current_time_to_msec(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);

    return ((Int64)tv.tv_sec*1000 + tv.tv_usec/1000);
}

/* used in 814x 16ch usecase */
#define H264_PRIMARY_STREAM_PROFILE     VENC_CHN_MAIN_PROFILE
#define H264_SECONDARY_STREAM_PROFILE   VENC_CHN_BASELINE_PROFILE

/* used to set resolution / buf size of ipcbits for varying resolutions based on usecase */
static VcapVencVdecVdis_chBufInfo ipcBitsChInfo[MCFW_IPCBITS_MAX_CHANNELS];


static Void CommitVcapVencVdecVdis_setChannels( int *pEnable2ndOut)
{
    Int32 i;

    printf("ranran CommitVcapVencVdecVdis_setChannels 1");
    memset(ipcBitsChInfo, 0, sizeof(ipcBitsChInfo));


            printf("ranran CommitVcapVencVdecVdis_setChannels 22");
            gDemo_info.maxVcapChannels = 1;
            gDemo_info.maxVdisChannels = 1;
            gDemo_info.maxVencChannels = 1;
            gDemo_info.maxVdecChannels = 0;
            gDemo_info.VsysNumChs      = 1;
            gDemo_info.usecase = DEMO_COMMIT_EXPERIMENTAL;

            for(i=0; i<gDemo_info.maxVencChannels; i++)
            {
                ipcBitsChInfo[i].width = MCFW_IPCBITS_HD_WIDTH;
                ipcBitsChInfo[i].height = MCFW_IPCBITS_HD_HEIGHT;
                ipcBitsChInfo[i].chBufSize = ipcBitsChInfo[i].width * ipcBitsChInfo[i].height;
            }

}



Void CommitVideo_start( Bool doProgressiveVenc, Bool enableSecondaryOut, int demoId)
{
    UInt32 i, resolutionClass;
    VSYS_PARAMS_S vsysParams;
    VCAP_PARAMS_S vcapParams;
    VENC_PARAMS_S vencParams;
    VDEC_PARAMS_S vdecParams;
    VDIS_PARAMS_S vdisParams;
    Bool forceLowCostScale = FALSE, enableFWrite = FALSE, enableLayerWrite = FALSE;
    Int32 Enable2ndOut = enableSecondaryOut;


    UInt64 wallTimeBase;

    Vsys_params_init(&vsysParams);
    Vcap_params_init(&vcapParams);
    Venc_params_init(&vencParams);
    Vdec_params_init(&vdecParams);
    Vdis_params_init(&vdisParams);

    CommitVcapVencVdecVdis_setChannels( &Enable2ndOut);


    vcapParams.numChn = gDemo_info.maxVcapChannels;
    vencParams.numPrimaryChn = gDemo_info.maxVencChannels;
    vencParams.numSecondaryChn = 0;
    vdecParams.numChn = gDemo_info.maxVdecChannels;
    vdisParams.numChannels = gDemo_info.maxVdisChannels;
    vsysParams.numChs  = gDemo_info.VsysNumChs;

    enableSecondaryOut = (Bool)Enable2ndOut;


    resolutionClass = DEMO_SCD_RESOLUTION_CLASS_QCIF;
    if( doProgressiveVenc)
    {
		vsysParams.systemUseCase = VSYS_USECASE_COMMIT_VCAP_VENC_VDIS;


        if (enableSecondaryOut == FALSE)
        {
            vsysParams.enableSecondaryOut = FALSE;
            vsysParams.enableNsf     = FALSE;
        }
        else
        {
            vsysParams.enableSecondaryOut = TRUE;
            vsysParams.enableNsf     = TRUE;
            vsysParams.enableMjpegEnc = TRUE;
        }
        vsysParams.enableCapture = TRUE;
        vsysParams.enableNullSrc = FALSE;

        vsysParams.enableOsd     = TRUE;


        vsysParams.enableScd     = FALSE;
        vsysParams.numDeis       = 1;
        vsysParams.numSwMs       = 2;
        vsysParams.numDisplays   = 2;
        vsysParams.enableAVsync  = TRUE;

    }

    if (enableSecondaryOut)
    {
        vencParams.numSecondaryChn = gDemo_info.maxVencChannels;
        /*enableMjpegEnc should always be true in all usecases*/
        gDemo_info.maxVencChannels *= 3;
    }

    printf ("--------------- CHANNEL DETAILS-------------\n");
    printf ("Capture Channels => %d\n", vcapParams.numChn);
    printf ("Enc Channels => Primary %d, Secondary %d\n", vencParams.numPrimaryChn, vencParams.numSecondaryChn);
    printf ("Dec Channels => %d\n", vdecParams.numChn);
    printf ("Disp Channels => %d\n", vdisParams.numChannels);
    printf ("-------------------------------------------\n");

    Vsys_enableFastUsecaseSwitch(FALSE);
    if ((vsysParams.systemUseCase == VSYS_USECASE_MULTICHN_HD_VCAP_VENC) ||
    	(vsysParams.systemUseCase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS))
    {
    	printf("ranran osd disable !!!!!!!!!!\n");
        vsysParams.enableScd     = FALSE;
        vsysParams.enableOsd     = FALSE;
        vsysParams.numDeis       = 1;
        vsysParams.numSwMs       = 1;
        vsysParams.numDisplays   = 1;
        vsysParams.enableAVsync  = FALSE;
    }

    /* Override the context here as needed */
    Vsys_init(&vsysParams);

    /* Override the context here as needed */
    Vcap_init(&vcapParams);

    /*Enabling generation of motion vector for channel 0 only,
         * for other channels please add to the below line*/

    vencParams.encChannelParams[0].enableAnalyticinfo = 1;
    //vencParams.encChannelParams[1].enableAnalyticinfo = 1;
    vencParams.encChannelParams[0].maxBitRate = -1;

    /*Note:Specific for h264 Encoder: Enabling this flag adds svc extension
          headers to the stream, not all decoders are generally able to play back such a stream. */
    /* Needs to be enabled to IH264_SVC_EXTENSION_FLAG_ENABLE for the
          svc extension headers to be present in the stream*/
    vencParams.encChannelParams[0].enableSVCExtensionFlag =
                                       VENC_IH264_SVC_EXTENSION_FLAG_DISABLE;

    /** Note: For the encoder to give out frames with SVC-T properties,
             number of layers needs to be increased as per requirement.
             A flag enableLayerWrite needs to be set if for any channel the Layer is increased to beyond 1.
             This check has also been done for Channel 0, just before VcapVencVdecVdis_ipcBitsInit call.

             For example for Channel 0 it has been set to LAYERS_4,
             file stream dump will generate four(Base + 3) files with differing frame-rates**/
    /** WARNING: SVC-T cannot be used if B Frame is enabled */
    vencParams.encChannelParams[0].numTemporalLayer = VENC_TEMPORAL_LAYERS_4;

    vencParams.encChannelParams[0].dynamicParam.frameRate = 30 * 1000;


    /* Override the context here as needed */
    Venc_init(&vencParams);


    /* Override the context here as needed */
    Vdec_init(&vdecParams);

    /* Override the context here as needed */
    vdisParams.deviceParams[VDIS_DEV_HDMI].resolution   = DEMO_HD_DISPLAY_DEFAULT_STD;
    vdisParams.deviceParams[VDIS_DEV_DVO2].resolution = DEMO_HD_DISPLAY_DEFAULT_STD;
    vdisParams.deviceParams[VDIS_DEV_HDCOMP].resolution = DEMO_HD_DISPLAY_DEFAULT_STD;
    vdisParams.mosaicLayoutResolution[VDIS_DEV_HDMI] =
        vdisParams.deviceParams[VDIS_DEV_HDMI].resolution;
    vdisParams.mosaicLayoutResolution[VDIS_DEV_DVO2] =
        vdisParams.deviceParams[VDIS_DEV_DVO2].resolution;
    vdisParams.mosaicLayoutResolution[VDIS_DEV_HDCOMP] =
        vdisParams.deviceParams[VDIS_DEV_HDCOMP].resolution;

    Vdis_tiedVencInit(VDIS_DEV_DVO2, VDIS_DEV_HDCOMP, &vdisParams);

    vdisParams.deviceParams[VDIS_DEV_SD].resolution     = VSYS_STD_NTSC;
    vdisParams.mosaicLayoutResolution[VDIS_DEV_SD] =
        vdisParams.deviceParams[VDIS_DEV_SD].resolution;

    vdisParams.enableLayoutGridDraw = FALSE;

    if (vsysParams.systemUseCase == VSYS_USECASE_MULTICHN_INTERLACED_VCAP_VDIS_VENC_VDEC)
        forceLowCostScale = TRUE;


    i = 0;
    /* set for 2 displays */

        Demo_swMsGenerateLayout(VDIS_DEV_HDMI, 0, gDemo_info.maxVdisChannels,
						  DEMO_LAYOUT_MODE_1CH,
                          &vdisParams.mosaicParams[VDIS_DEV_HDMI], forceLowCostScale,
                          gDemo_info.Type,
                          Vdis_getSwMsLayoutResolution(VDIS_DEV_HDMI));

    vdisParams.mosaicParams[VDIS_DEV_HDMI].userSetDefaultSWMLayout = TRUE;
    /* Set swMS outputFPS as 60 or 50 for this default 7x1 layout for TI814X */

    Demo_swMsSetOutputFPS(&vdisParams.mosaicParams[VDIS_DEV_HDMI], Demo_swMsGetOutputFPS(&vdisParams.mosaicParams[VDIS_DEV_HDMI])*2);


        Demo_swMsGenerateLayout(VDIS_DEV_SD, 0, gDemo_info.maxVdisChannels,
        		DEMO_LAYOUT_MODE_1CH,
                          &vdisParams.mosaicParams[VDIS_DEV_SD], forceLowCostScale,
                          gDemo_info.Type,
                          Vdis_getSwMsLayoutResolution(VDIS_DEV_SD));

    vdisParams.mosaicParams[VDIS_DEV_SD].userSetDefaultSWMLayout = TRUE;

    Demo_swMsSetOutputFPS(&vdisParams.mosaicParams[VDIS_DEV_SD], Demo_swMsGetOutputFPS(&vdisParams.mosaicParams[VDIS_DEV_SD])*2);



    Vdis_init(&vdisParams);

    enableFWrite = TRUE;

    /* Init the application specific module which will handle bitstream exchange */


    /*h264 Enc: Needs to be checked for any channel where this flag is enabled,
         * right now just checking for channel 0, default value for other codecs is 0*/
    /* ranran if(vencParams.encChannelParams[0].numTemporalLayer > 1)*/
        enableLayerWrite = FALSE; /*ranran */

    VcapVencVdecVdis_ipcBitsSetFileExtension("h264");
    VcapVencVdecVdis_ipcBitsInit(ipcBitsChInfo, enableFWrite, enableLayerWrite , TRUE);

    /* Start audio capture */
    /* ranran CommitDemo_startAudio(demoId); */

    wallTimeBase = get_current_time_to_msec();
    //wallTimeBase = 0;
    Vdis_setWallTimeBase(wallTimeBase);

    /* Configure display in order to start grpx before video */
    Vsys_configureDisplay();

#if USE_FBDEV
    grpx_init(GRPX_FORMAT_RGB565);
#endif


    /* Create Link instances and connects compoent blocks */
    Vsys_create();


    /* Start components in reverse order */
    Vdis_start();
    Vdec_start();
    Venc_start();
    Vcap_start();




}



Void CommitVideo_stop()
{
    VSYS_PARAMS_S contextInf;
    Vsys_getContext(&contextInf);

    Vsys_enableFastUsecaseSwitch(FALSE);

    printf("++++++++ VcapVencVdecVdis_stop()\n");

    /* Stop components */

    VcapVencVdecVdis_ipcBitsStop();

    Vcap_stop();
    Venc_stop();
    Vdec_stop();
    Vdis_stop();


#if USE_FBDEV
     grpx_exit();
#endif


    Vsys_delete();

    Vsys_deConfigureDisplay();

    VcapVencVdecVdis_ipcBitsExit();

    /* De-initialize components */
    Vcap_exit();
    Venc_exit();
    Vdec_exit();
    Vdis_exit();

    Vsys_exit();

}
