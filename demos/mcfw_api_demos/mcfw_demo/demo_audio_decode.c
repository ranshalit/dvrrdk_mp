#include <demo_audio.h>

static OSA_ThrHndl     gAppDecodeThread;
static Bool            gAppDecodeThreadActive = FALSE;
static Bool            gAppDecodeThreadExitFlag = FALSE;
static Audio_DecInfo   gDecInfo;

static Void writeToFile (FILE *outputFile, ADEC_PROCESS_PARAMS_S *pPrm)
{
    /* This is a work around to skip initial 2 invalid frames from decoder */
    static Int32 frameCount = 0;

    if (pPrm->numSamples <= 0)
        return;

#ifdef  DSP_RPE_AUDIO_ENABLE
    if (frameCount > 1)
    {
        if(pPrm->channelMode == IAUDIO_1_0)
        {
            fwrite(pPrm->outBuf.dataBuf, pPrm->bytesPerSample, pPrm->numSamples, outputFile);
        }
        else 
        {
            if(pPrm->pcmFormat == IAUDIO_INTERLEAVED)
            {
              fwrite(pPrm->outBuf.dataBuf, pPrm->bytesPerSample, pPrm->numSamples*2, outputFile);
            }
            else
            { 
                UInt32 i;
                UInt8 * outL = (UInt8 *) pPrm->outBuf.dataBuf;
                UInt8 * outR = (UInt8 *)((UInt8 *) pPrm->outBuf.dataBuf + 
                                             (pPrm->numSamples * pPrm->bytesPerSample)) ;
                for(i=0;i<(pPrm->numSamples * pPrm->bytesPerSample); i+= pPrm->bytesPerSample)
                {
                  /*--------------------------------------------------------------*/
                  /* Write the left anf right channels                            */
                  /*--------------------------------------------------------------*/
                  fwrite(&outL[i], pPrm->bytesPerSample, 1, outputFile);
                  fwrite(&outR[i], pPrm->bytesPerSample, 1, outputFile);
                }
            }
        }
    }
#else
    fwrite(pPrm->outBuf.dataBuf, pPrm->bytesPerSample, pPrm->numSamples, outputFile);
#endif
    frameCount++;
}

static void Demo_printDecodeStreamParams (ADEC_PROCESS_PARAMS_S *pPrm)
{
    printf ("\n\n------------ Decoded Stream------------\n");

#ifdef  DSP_RPE_AUDIO_ENABLE
    if(pPrm->channelMode == IAUDIO_1_0)
    {
        printf ("Channel Mode = MONO\n");
    }
    else 
    {
        printf ("Channel Mode = STEREO, ");
        if(pPrm->pcmFormat == IAUDIO_INTERLEAVED)
            printf (" INTERLEAVED\n");
        else
            printf (" PLANAR\n");
    }
#endif
    printf ("\n\n");
}

static Void *App_decodeTaskFxn(Void * prm)
{
    FILE                  *in = NULL, *out = NULL;
    UInt8                 *inBuf = NULL;
    UInt8                 *outBuf = NULL;
    ADEC_PROCESS_PARAMS_S decPrm;
    Int32                 rdIdx, to_read, readBytes;
    ADEC_CREATE_PARAMS_S  adecParams;
    Void                  *decHandle = NULL;
    Int32                 frameCnt = 0, totalSamples = 0;
    Int32                 inBufSize, outBufSize;
    Audio_DecInfo         *info = prm;
    Bool                  isSharedRegion = FALSE;
    char                  threadName[128];

    snprintf(threadName,sizeof(threadName),"%s_%x",__func__,0);
    threadName[sizeof(threadName) - 1] = 0;
    OSA_printTID(threadName);

    if (!prm)
    {
        printf ("ADEC task failed <invalid param>...........\n");
        return NULL;
    }

    memset(&decPrm, 0, sizeof(decPrm));
    memset(&adecParams, 0, sizeof(adecParams));

    adecParams.decoderType = (info->decodeType == 0) ? AUDIO_CODEC_TYPE_AAC_LC : AUDIO_CODEC_TYPE_G711;
    
    adecParams.desiredChannelMode = info->numChannels;

    /* For AAC-LC, 0 means mono & 2 means stereo */
    if (adecParams.decoderType == AUDIO_CODEC_TYPE_AAC_LC)
       adecParams.desiredChannelMode -= 1;

    decHandle = Adec_create(&adecParams);
    if (decHandle)
    {
        printf ("ADEC Create done...........\n");
    }
    else
    {
        printf ("ADEC Create failed...........\n");
        printf ("\n********Decode APP Task Exitting....\n");
        gAppDecodeThreadActive = FALSE;
        gAppDecodeThreadExitFlag = TRUE;
        return NULL;
    }
    gAppDecodeThreadExitFlag = FALSE;

    if (adecParams.decoderType == AUDIO_CODEC_TYPE_AAC_LC)
        isSharedRegion = TRUE;

    inBufSize = MAX_INPUT_BUFFER / 2;
    if (inBufSize < adecParams.minInBufSize)
        inBufSize = adecParams.minInBufSize;

    outBufSize = MAX_OUTPUT_BUFFER;
    if (outBufSize < adecParams.minOutBufSize)
        outBufSize = adecParams.minOutBufSize;

    in = fopen(info->inFile, "rb");
    if (!in)
    {
        printf ("File <%s> Open Error....\n", info->inFile);
    }  

    out = fopen(info->outFile, "wb");    
    if (!out)
    {
        printf ("File <%s> Open Error....\n", info->outFile);
    }  

    inBuf = App_allocBuf(inBufSize, isSharedRegion);
    outBuf = App_allocBuf(outBufSize, isSharedRegion); 

    if (in && out && inBuf && outBuf)
    {
        printf ("\n\n=============== Starting Decode ===================\n");
        sleep(1);

        rdIdx = 0;
        to_read = inBufSize;
        decPrm.outBuf.dataBuf = outBuf;

        while (gAppDecodeThreadActive == TRUE)
        {
            readBytes = fread (inBuf + rdIdx, 1, to_read, in);
            readBytes += rdIdx;
            if (readBytes)
            {
                decPrm.inBuf.dataBufSize = readBytes;
                decPrm.inBuf.dataBuf = inBuf;
                decPrm.outBuf.dataBufSize = outBufSize;
                if (Adec_process(decHandle, &decPrm) < 0)
                {
                    printf ("DEC: Decode process failed... Corrupted handled!!!! ....\n");
                    break;
                }
                if (decPrm.inBuf.dataBufSize <= 0)
                {
                    if (totalSamples <= 0)
                        printf ("DEC: Decoder didnt consume bytes <%d>... exiting....\n", readBytes);
                    printf ("=============== Decode completed, %d samples generated ================\n", totalSamples);
                    break;
                }

                if ((totalSamples == 0) && (adecParams.decoderType == AUDIO_CODEC_TYPE_AAC_LC))
                {
                    Demo_printDecodeStreamParams(&decPrm);
                }

                if (out)
                {
                    writeToFile(out, &decPrm);
                }
                rdIdx = (readBytes-decPrm.inBuf.dataBufSize);
                memmove(inBuf, inBuf+decPrm.inBuf.dataBufSize, rdIdx);
                to_read = decPrm.inBuf.dataBufSize;
                {
//                    printf ("DEC - %d, Samples Generated - %d <total %d>, bytesConsumed - %d\n", 
//                            frameCnt, decPrm.numSamples, totalSamples, decPrm.inBuf.dataBufSize);              
                }
                frameCnt++;
                totalSamples += decPrm.numSamples;
            }
            else
            {
                printf ("=============== Decode completed, %d samples generated ================\n", totalSamples);
                break;
            }
        }
    }
    else
    {
        printf ("\n\n=============== Decode not starting.... file / mem error ============\n");
    }
    prm = prm;
    if (in)
        fclose(in);
    if (out)
        fclose(out);
    if (inBuf)
        App_freeBuf(inBuf, inBufSize, isSharedRegion);
    if (outBuf)
        App_freeBuf(outBuf, outBufSize, isSharedRegion);
    Adec_delete(decHandle);

    printf ("\n********Decode APP Task Exitting....\n");
    gAppDecodeThreadActive = FALSE;
    gAppDecodeThreadExitFlag = TRUE;
    return NULL;
}

Bool Demo_startAudioDecodeSystem (Void)
{
    char ch;
    Bool ret = FALSE;

    printf("\r\n\n\nAUDIO: Enable Decode <Y/N>: " );
    ch = Demo_getChar();
    
    if (ch == 'y' || ch == 'Y')
    {
        Int32 status;

        printf("\r\nAUDIO: Enter input file name <absolute path>: " );
        fflush(stdin);
        fgets(gDecInfo.inFile, MAX_INPUT_STR_SIZE, stdin);
        gDecInfo.inFile[ strlen(gDecInfo.inFile)-1 ] = 0;

        printf("\r\nAUDIO: Enter output file name <absolute path>: " );
        fflush(stdin);
        fgets(gDecInfo.outFile, MAX_INPUT_STR_SIZE, stdin);
        gDecInfo.outFile[ strlen(gDecInfo.outFile)-1 ] = 0;

        gDecInfo.decodeType = Demo_getIntValue("AUDIO: decode Type <0 - AAC-LC, 1 - G711>", 0, 1, 0);
#ifndef  DSP_RPE_AUDIO_ENABLE
        if (gDecInfo.decodeType == 0)
        {
            printf ("AAC-LC decode <DSP Based> not supported!!!!\n");
            return ret;
        }
#endif
        if (gDecInfo.decodeType == 0)
        {
            gDecInfo.numChannels = Demo_getIntValue("AUDIO: desired audio channels", 1, 2, 1);
        }
        gAppDecodeThreadActive = TRUE;
        status = OSA_thrCreate(&gAppDecodeThread,
                      App_decodeTaskFxn,
                      AUDIO_TSK_PRI, 
                      AUDIO_TSK_STACK_SIZE, 
                      &gDecInfo);
        if (status != 0)
        {
            printf ("AUDIO: App Decode thread create failed...\n");
            gAppDecodeThreadActive = FALSE;
            return ret;
        }
        ret = TRUE;
    }
    printf ("\r\n\n");
    return ret;
}

Bool Demo_stopAudioDecodeSystem (Bool userOpt)
{
    Bool ret = FALSE;

    if (gAppDecodeThreadActive == TRUE)
    {
        char ch = 'Y';

        if (userOpt == TRUE)
        {
            printf("\r\n\n\nAUDIO: Stop Decode <Y/N>: " );
            ch = Demo_getChar();
        }
            
        if (ch == 'y' || ch == 'Y')
        {
            gAppDecodeThreadActive = FALSE;
            while (gAppDecodeThreadExitFlag == FALSE)
            {
                printf ("**** Waiting for Decode task to exit .....\n");
                OSA_waitMsecs(1000);
            }
            OSA_thrDelete(&gAppDecodeThread);
            printf ("AUDIO:  Decode stopped....\n");
            ret = TRUE;
        }
    }
    printf ("\r\n\n");
    return ret;
}


Bool Demo_IsDecodeActive(Void)
{
    return gAppDecodeThreadActive;
} 

