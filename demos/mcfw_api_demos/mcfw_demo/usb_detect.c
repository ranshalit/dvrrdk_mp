#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include "libusb.h"
#include "usb_detect.h"
#define DEBUG_LIBUSBDETECT

int global_dev_array[DEV_OPT_NUM] = {0};
struct libusb_context* tmp_cnt;

int init_usb_detect()
{

#ifdef DEBUG_LIBUSBDETECT
	printf("\nStarting init_usb_detect\n");
#endif
	int ret_val;
	/*int i;*/
	//libusb_set_debug(NULL, 3);

  ret_val = libusb_init(NULL);
  printf("Ret_val lib_usb_init = %d \n", ret_val);
  if(ret_val)
  {

  printf("\nError in libusb_init\n");

    return ret_val;
  }
  ret_val = get_current_dev(global_dev_array);


	printf("\nEnd init_usb_detect %d\n",ret_val);


  return ret_val;
}


void exit_usb_detect()
{
#ifdef DEBUG_LIBUSBDETECT
	printf("before libusb_exit \n");
#endif

    libusb_exit(NULL);
#ifdef DEBUG_LIBUSBDETECT
    printf("after libusb_exit \n");
#endif
    return;
}

/*!
\brief print_dev_type gets DEVTYPE and prints the device type to stdout
*/

void print_dev_type(DEVTYPE val)
{
  printf("\nDevice is:");
  switch (val)
  {
    case STORAGE:
      printf("\nMass storage device");
      break;
    case NETWORK:
    	printf("\nNetwork");
    	break;
    case MOUSE:
      printf("\nMouse");
      break;
    case AHHD:
      printf("\nAHHD");
      break;
    case LVDS:
      printf("\nLVDS");
      break;
    default:
      printf("\nOTHER");
      break;
  }
}


/*!
\brief 	identify_device  will check which device is connected,
		according to its vendor+product ID for some devices
		OR usb interface class for general devices
*/
DEVTYPE identify_device(libusb_device *device, struct libusb_device_descriptor desc)
{
  struct libusb_config_descriptor *config;
  struct libusb_interface *iface;
  struct libusb_interface_descriptor *iface_desc;

  int ret_val = 0;

  if((desc.idVendor == 0x3EB) && (desc.idProduct == 0x202D))
    return AHHD;
  if((desc.idVendor == 0x451) && (desc.idProduct == 0x2046)) //added in version 1.1
    return LVDS;
  if((desc.idVendor == 0x424) && (desc.idProduct == 0x7500))
     return NETWORK;

#ifdef DEBUG_LIBUSBDETECT
  printf("desc.idVendor = 0x%X, desc.idProduct = 0x%X \n", desc.idVendor,desc.idProduct );
#endif

  ret_val = libusb_get_config_descriptor(device, 0, &config);
  if(ret_val)	//if ret_val !=0 - get usb config descriptor failed
    return OTHER;
  iface = (struct libusb_interface *) &(config)->interface[0];
  iface_desc = (struct libusb_interface_descriptor *) &(iface)->altsetting[0];

 #ifdef DEBUG_LIBUSBDETECT
  printf("iface_desc-> bInterfaceClass %d \n", iface_desc->bInterfaceClass );
#endif

  if(iface_desc->bInterfaceClass == HID_CLASS)
    return MOUSE;
  if(iface_desc->bInterfaceClass == MASS_STORGE_CLASS)
    return STORAGE;

  return OTHER;
}



/*!
\brief 	identify_device  will check which device is connected,
		according to its vendor+product ID for some devices
		OR usb interface class for general devices
		get_current_dev() will ceck which devices are connected
		It will change dev_array so that dev_array[DEV] is 1
		If the device DEV is connected
*/
int get_current_dev(int dev_array[DEV_OPT_NUM])
{
  libusb_device **devs;
  libusb_device *device;
//  libusb_device_handle *handle;
  struct libusb_device_descriptor desc;
  DEVTYPE ret_val = OTHER;
  ssize_t cnt;
  int i;
  int res = 0;

#ifdef DEBUG_LIBUSBDETECT
      printf("\nStarting get_current_dev");
#endif

  //initiallize the dev array
  for(i=0; i < DEV_OPT_NUM; i++)
    dev_array[i] = 0;
  //get the current list of connected devices
  cnt = libusb_get_device_list(NULL, &devs);
  if (cnt < 0)
  {
#ifdef DEBUG_LIBUSBDETECT
     printf("\nError in libusb_get_device_list\n");
#endif
     return -1;
  }
  //go through all devices and identify them
  for (i = 0; i < cnt; i++)
  {
    device = devs[i];
    res = libusb_get_device_descriptor(device, &desc);
    if (res)
    {
#ifdef DEBUG_LIBUSBDETECT
      printf("\nError in libusb_get_device_descriptor");
#endif
      libusb_free_device_list(devs, 1);
      return res;
    }
    ret_val = identify_device(device,desc);
    if (ret_val != OTHER)
    {
      dev_array[ret_val]++;
    }
  }
  libusb_free_device_list(devs, 1);

#ifdef DEBUG_LIBUSBDETECT
      printf("\nEnd get_current_dev");
#endif

  return 0;
}



/*!
\brief 	block_for_dev() gives an alert when there is a change in connected device
		it will return once an event was detected, with:
		event_type = 1 -> connection
		event_type = 0 -> disconnection
		device_changed will hold the type of device that was changed
		If there was a problem- the function will return -1. otherwise - 0
*/

int wait_for_usb_event(DEVTYPE* device_changed, int* event_type)
{
	libusb_device **devs;
	libusb_device *device;
	//libusb_device_handle *handle;
	struct libusb_device_descriptor desc;
	DEVTYPE curr_dev = OTHER;
	ssize_t cnt = 0 ;
	int res = 0;
	int i;
	int dev_array[DEV_OPT_NUM] = {0};

#ifdef DEBUG_LIBUSBDETECT
	printf("\nStarting wait_for_usb_event\n");
#endif

	while(1)
	{
		cnt = libusb_get_device_list(NULL, &devs);
		if (cnt < 0)
		{
#ifdef DEBUG_LIBUSBDETECT
			printf("\nError in libusb_get_device_list\n");
#endif
			return -1;
		}

		for(i=0; i<DEV_OPT_NUM; i++)
			dev_array[i] = 0;
		for (i = 0; i < cnt; i++)
		{
			device = devs[i];
			res = libusb_get_device_descriptor(device, &desc);
			if (res)
			{
#ifdef DEBUG_LIBUSBDETECT
				printf("\nError in libusb_get_device_descriptor");
#endif
				libusb_free_device_list(devs, 1);
				return res;
			}
			curr_dev = identify_device(device,desc);
			dev_array[curr_dev]++;
		}
		/*Check if something has changed.
     We don't care about last cell in the array, since it's "other"  = unrecognized devices */
		for (i = 0; i < DEV_OPT_NUM - 1; i++)
		{
			if(dev_array[i] == global_dev_array[i])
			{
				continue;
			}
			if(dev_array[i] < global_dev_array[i])
			{
				*event_type = 0; // a device was disconnected
				global_dev_array[i]--;
				*device_changed = i;
				goto exit_here;
			}
			if(dev_array[i] > global_dev_array[i])
			{
				*event_type = 1; // a device was connected
				global_dev_array[i]++;
				*device_changed = i;
				goto exit_here;
			}
		}
		sleep(4);
	}


#ifdef DEBUG_LIBUSBDETECT
	printf("\nEnd wait_for_usb_event");
#endif

exit_here:
  libusb_free_device_list(devs, 1);
  return 0;
}

