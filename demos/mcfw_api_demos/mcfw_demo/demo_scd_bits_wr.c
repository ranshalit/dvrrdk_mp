/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2011 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/



#include <demo_scd_bits_wr.h>
#include <ti_gpx_scd_tz_rgb565.h>
#include <ti_gpx_scd_oc_rgb565.h>
#include <ti_gpx_scd_td_rgb565.h>
#include <ti_gpx_scd_imd_rgb565.h>
//#include <ti_gpx_scd_ch_num_rgb565.h>

#define FILE_WRITE_STOPPED  (0)
#define FILE_WRITE_RUNNING  (1)

Scd_Ctrl gScd_ctrl;
VDIS_MOSAIC_S vdMosaicParam;
char *scdVaEventName[] = { 
                        "TRIP ZONE EVENT", 
                        "OBJECT COUNT EVENT",
                        "TAMPER DETECT EVENT",
                        "IMD EVENT OBEJECTS DETETCTED"
                    };


Int32 Scd_resetStatistics()
{
    UInt32 chId;
    Scd_ChInfo *pChInfo;

    for(chId=0; chId<VENC_CHN_MAX; chId++)
    {
        pChInfo = &gScd_ctrl.chInfo[chId];

        pChInfo->totalDataSize = 0;
        pChInfo->numFrames = 0;
//        pChInfo->maxWidth = 0;
//        pChInfo->maxHeight= 0;
    }

    gScd_ctrl.statsStartTime = OSA_getCurTimeInMsec();

    return 0;
}

Int32 Scd_printStatistics(Bool resetStats)
{
    UInt32 chId;
    Scd_ChInfo *pChInfo;
    float elaspedTime;

    elaspedTime = OSA_getCurTimeInMsec() - gScd_ctrl.statsStartTime;

    elaspedTime /= 1000.0; // in secs

    OSA_printf( "\n"
            "\n *** SCD Frame Results Received Statistics *** "
            "\n"
            "\n Elased time = %6.1f secs"
            "\n"
            "\n CH | Bitrate (Kbps) | FPS | Width (max/min) | Height (max/min)"
            "\n ----------------------------------------------------------------------------------",
            elaspedTime
            );

    for(chId=0; chId<VENC_CHN_MAX;chId++)
    {
        pChInfo = &gScd_ctrl.chInfo[chId];

        if(pChInfo->numFrames)
        {
            OSA_printf(" %2d | %14.2f | %3.1f | %5d | %6d ",
                chId,
                (pChInfo->totalDataSize*8.0/elaspedTime)/1024.0,
                pChInfo->numFrames*1.0/elaspedTime,
                pChInfo->maxWidth,
                pChInfo->maxHeight
                );
        }
    }

    printf("\n");

    if(resetStats)
        Scd_resetStatistics();

    return 0;
}

Void Scd_bitsWriteCbFxn(Ptr pPrm)
{
    OSA_semSignal(&gScd_ctrl.wrSem);
}


Int32 Scd_trackCh(UInt32 tCh, UInt32 flag)
{
    if(flag == 0)
    {
        gScd_ctrl.chInfo[tCh].enableMotionTracking = FALSE;
        gScd_ctrl.chInfo[tCh].enableGpxEvntDisplay = FALSE;
    }
    else if(flag == 1)
    {
        gScd_ctrl.chInfo[tCh].enableMotionTracking = TRUE;
        gScd_ctrl.chInfo[tCh].enableGpxEvntDisplay = FALSE;
    }
    else if(flag == 2)
    {
        gScd_ctrl.chInfo[tCh].enableMotionTracking = FALSE;
        gScd_ctrl.chInfo[tCh].enableGpxEvntDisplay = TRUE;
    }
    else
    {
        gScd_ctrl.chInfo[tCh].enableMotionTracking = TRUE;
        gScd_ctrl.chInfo[tCh].enableGpxEvntDisplay = TRUE;
    }

    gScd_ctrl.chInfo[tCh].updateGrid      = gScd_ctrl.chInfo[tCh].enableMotionTracking;
    gScd_ctrl.chInfo[tCh].updateStaticObj = TRUE;

    return 0;
}

void Scd_windowGrid(Bool flag, UInt32 numHorzBlks, Scd_ChInfo  *pChInfo)
{
   UInt32 winWidth, winHeight;
   UInt32 startX, startY;

   if(flag==TRUE)
   {
       OSA_printf(" [SCD MOTION TRACK]: WinWidth %d, WinHeight %d, StartX %d StartY %d DWidth %d DHeight %d\n",
                                             pChInfo->curWinWidth, pChInfo->curWinHeight,
                                              pChInfo->curWinStartX, pChInfo->curWinStartY,
                                              gScd_ctrl.displayWidth, gScd_ctrl.displayHeight
                                              );

       winWidth  = (pChInfo->curWinWidth  * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH * 100)/gScd_ctrl.displayWidth;
       winHeight = (pChInfo->curWinHeight * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT * 100)/gScd_ctrl.displayHeight;

       startX    = (pChInfo->curWinStartX * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH * 100)/gScd_ctrl.displayWidth;
       startY    = (pChInfo->curWinStartY * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT * 100)/gScd_ctrl.displayHeight;

#if DEMO_SCD_MOTION_TRACK_DEBUG
       OSA_printf(" [SCD MOTION TRACK]: WinWidth %d, WinHeight %d, StartX %d StartY %d\n",
                                             winWidth, winHeight, startX, startY);
#endif
       grpx_draw_grid(winWidth, winHeight, startX, startY, numHorzBlks);
       pChInfo->prevWinWidth  = pChInfo->curWinWidth; 
       pChInfo->prevWinHeight = pChInfo->curWinHeight;
       pChInfo->prevWinStartX = pChInfo->curWinStartX;
       pChInfo->prevWinStartY = pChInfo->curWinStartY;
       pChInfo->numHorzBlks   = numHorzBlks;
   }
   else
   {
#if DEMO_SCD_MOTION_TRACK_DEBUG
       OSA_printf(" [SCD MOTION TRACK]: Prev WinWidth %d, WinHeight %d, StartX %d StartY %d\n",
                                                  pChInfo->prevWinWidth, 
                                                  pChInfo->prevWinHeight, 
                                                  pChInfo->prevWinStartX, 
                                                  pChInfo->prevWinStartY);
#endif
       winWidth  = (pChInfo->prevWinWidth  * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH * 100)/gScd_ctrl.displayWidth;
       winHeight = (pChInfo->prevWinHeight * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT * 100)/gScd_ctrl.displayHeight;

       startX    = (pChInfo->prevWinStartX * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH * 100)/gScd_ctrl.displayWidth;
       startY    = (pChInfo->prevWinStartY * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT * 100)/gScd_ctrl.displayHeight;

       grpx_draw_grid_exit(winWidth, winHeight, startX, startY, pChInfo->numHorzBlks);
//       grpx_draw_grid_exit(gScd_ctrl.chInfo[chId].prevWinWidth, gScd_ctrl.chInfo[chId].prevWinHeight, gScd_ctrl.chInfo[chId].prevWinStartX,  gScd_ctrl.chInfo[chId].prevWinStartY, gScd_ctrl.chInfo[chId].numHorzBlks);
   }
}

/* To get the number of windows in case of layout switch*/
static Void Scd_updateDisplayParam()
{
    UInt32 width, height;
    VSYS_VIDEO_STANDARD_E resolution;

    resolution = Vdis_getResolution(VDIS_DEV_HDMI);

    width  = 1920;
    height = 1080;
    switch(resolution)
    {
        case VSYS_STD_1080P_60:
          width  = 1920;
          height = 1080;
          break;
        case VSYS_STD_1080P_50:
          width  = 1920;
          height = 1080;
          break;
        case VSYS_STD_720P_60:
          width  = 1280;
          height = 720;
          break;
        case VSYS_STD_XGA_60:
          width  = 1024;
          height = 768;
          break;
    case VSYS_STD_SXGA_60:
          width  = 1280;
          height = 1024;
          break;
        default:
          break;
    }
    gScd_ctrl.displayWidth  = width;
    gScd_ctrl.displayHeight = height;
#if DEMO_SCD_MOTION_TRACK_DEBUG
    OSA_printf(" [SCD VA GPX]: New DisplayWidth %d DisplayHeight %d \n", gScd_ctrl.displayWidth, gScd_ctrl.displayHeight);
#endif
}
/* To get the number of windows in case of layout switch*/
Void Scd_updateLayoutParam()
{
    UInt32 winIdx, chIdx;

    Vdis_getMosaicParams(VDIS_DEV_HDMI, &vdMosaicParam);

    for(chIdx = 0; chIdx < VCAP_CHN_MAX; chIdx++)
    {
      gScd_ctrl.chInfo[chIdx].chOnMosaic = FALSE;
      gScd_ctrl.chInfo[chIdx].updateStaticObj = TRUE;
    }
    Scd_updateDisplayParam();


    gScd_ctrl.numberOfWindows = vdMosaicParam.numberOfWindows;;

    gScd_ctrl.startChId = vdMosaicParam.chnMap[0] % gDemo_info.maxVcapChannels;;
    for(winIdx = 0; winIdx < gScd_ctrl.numberOfWindows; winIdx++)
    {
      Scd_ChInfo * pChInfo;
      chIdx = vdMosaicParam.chnMap[winIdx] % gDemo_info.maxVcapChannels;
      
      pChInfo = &gScd_ctrl.chInfo[chIdx];
      gScd_ctrl.chInfo[chIdx].chOnMosaic = TRUE;

      if(gScd_ctrl.chInfo[chIdx].enableMotionTracking)
         gScd_ctrl.chInfo[chIdx].updateGrid = TRUE;
      else
         gScd_ctrl.chInfo[chIdx].updateGrid = FALSE;

      pChInfo->curWinWidth  = vdMosaicParam.winList[winIdx].width ;
      pChInfo->curWinHeight = vdMosaicParam.winList[winIdx].height ;
      pChInfo->curWinStartX = vdMosaicParam.winList[winIdx].start_X;
      pChInfo->curWinStartY = vdMosaicParam.winList[winIdx].start_Y;

      pChInfo->scaledWinWidth  = (pChInfo->curWinWidth  * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH  * 100)/gScd_ctrl.displayWidth;
      pChInfo->scaledWinHeight = (pChInfo->curWinHeight * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT * 100)/gScd_ctrl.displayHeight; 
      pChInfo->scaledWinStartX = (pChInfo->curWinStartX * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH  * 100)/gScd_ctrl.displayWidth;
      pChInfo->scaledWinStartY = (pChInfo->curWinStartY * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT * 100)/gScd_ctrl.displayHeight;
    }
#if DEMO_SCD_MOTION_TRACK_DEBUG
    OSA_printf(" [SCD VA GPX]: Received no of windows for this Mosaic Layout are %d\n",  
                                                               gScd_ctrl.numberOfWindows);

    for(winIdx = 0; winIdx < gScd_ctrl.numberOfWindows; winIdx++)
    {
       OSA_printf(" [SCD VA GPX]: ChanNo. %d on Window %d is updated %d \n",
           vdMosaicParam.chnMap[winIdx], winIdx, gScd_ctrl.chInfo[winIdx].updateStaticObj);
       OSA_printf(" [SCD VA GPX]: Width %d Height %d StartX %d StarY %d \n", 
                                        vdMosaicParam.winList[winIdx].width, vdMosaicParam.winList[winIdx].height,
                                       vdMosaicParam.winList[winIdx].start_X, vdMosaicParam.winList[winIdx].start_Y);


    }
#endif
}
/* To get the number of windows in case of layout switch*/
Void Scd_mosaicUpdated(VDIS_MOSAIC_S * mosaicParam)
{
    gScd_ctrl.layoutUpdated = TRUE;
}

#if DEMO_SCD_VA_TXT_NUM_OSD
static Int32 Scd_VaPrepaerDispalyTextMsg(UInt8 * displayContent, UInt8 * inputBuf, UInt8 * displayBuf, UInt32 strLength)
{
    UInt32 i, j;
    for(i = 0; i < strLength; i++)
    {
        if(((UInt8)(displayContent[i]) >= (UInt8)'A') && ((UInt8)(displayContent[i]) <= (UInt8)'Z'))
        {
           OSA_printf("%c %d index % d\n", displayContent[i], (UInt8) displayContent[i], ((UInt8)displayContent[i]-65));
           memcpy(displayBuf + (MAX_SCD_VA_EVENT_GPX_CHAR_SIZE*i), inputBuf + (MAX_SCD_VA_EVENT_GPX_CHAR_SIZE*((UInt8)displayContent[i]-65)), MAX_SCD_VA_EVENT_GPX_CHAR_SIZE);
        }
        else if(((UInt8)(displayContent[i]) >= (UInt8)'a') && ((UInt8)(displayContent[i]) <= (UInt8)'z'))
        {
           OSA_printf("%d \n", (UInt8) displayContent[i]);
           memcpy(displayBuf + (MAX_SCD_VA_EVENT_GPX_CHAR_SIZE*2*i), inputBuf + (MAX_SCD_VA_EVENT_GPX_CHAR_SIZE*((UInt8)displayContent[i]-97)), MAX_SCD_VA_EVENT_GPX_CHAR_SIZE);
        }
        else if((UInt8)(displayContent[i]) == 32)
        {
           UInt8 * addr;
           addr = displayBuf + (MAX_SCD_VA_EVENT_GPX_CHAR_SIZE*i);
           for(j = 0; j< (16*24); j++)
           {
              *addr++ = 0x18;
              *addr++ = 0xbe;
           }
        }
    }
    return 0;
}

static Int32 Scd_VaPrepaerDispalyNumMsg(UInt8 displayContent, UInt8 * inputBuf, UInt8 * displayBuf)
{
    memcpy(displayBuf, inputBuf + (72*14*2*displayContent), 72*14*2);
    return 0;
}
#endif

Int32 Scd_VaProcessMetaData(UInt32 winIdx, AlgLink_ScdResult * pObjScdResult, Scd_ChInfo  *pChInfo)
{
    AlgLink_ScdVaObjMeta * pObjMetaData;
    Polygon_Points * polygonePoints;
    UInt32 i;
    UInt32 eventIdx;
    Bool drawVaGpx = FALSE;
    Bool reDrawStaticObj = FALSE;
#if DEMO_SCD_MOTION_TRACK_DEBUG
    if(pChInfo->updateStaticObj)
    {
            OSA_printf(" [SCD VA GPX]: SCD Layout Change of Ch-%d: WinId %d MaxNumWindows %d \n", pObjScdResult->chId, winIdx, gScd_ctrl.numberOfWindows);
            OSA_printf(" [SCD VA GPX]: StartX %d StartY %d WinWidth %d WinHeight %d \n", 
                  pChInfo->scaledWinStartX,pChInfo->scaledWinStartY,pChInfo->scaledWinWidth,pChInfo->scaledWinHeight);
    }
#endif
    eventIdx = DEMO_SCD_VA_EVT_TZ;
    if(pChInfo->enableGpxEvntDisplay &&
       ((pObjScdResult->mode & ALG_LINK_SCD_DETECTMODE_IMD) ||
        (pObjScdResult->mode & ALG_LINK_SCD_DETECTMODE_SMETA) ||
        (pObjScdResult->mode & ALG_LINK_SCD_DETECTMODE_TRIPZONE) ||
        (pObjScdResult->mode & ALG_LINK_SCD_DETECTMODE_COUNTER)))
    {
        drawVaGpx = TRUE;
    }
    if(pChInfo->scdVaEvent)
    {
       if(pChInfo->scdVaEvent & ALG_LINK_SCD_DETECTOR_TRIPZONE)
       {
          eventIdx = DEMO_SCD_VA_EVT_TZ;
       }
       if(pChInfo->scdVaEvent & ALG_LINK_SCD_DETECTOR_COUNTER)
       {
           eventIdx = DEMO_SCD_VA_EVT_OC;
       }
       if(pChInfo->scdVaEvent & ALG_LINK_SCD_DETECTOR_TAMPER)
       {  
            eventIdx = DEMO_SCD_VA_EVT_TD;
       }
       if(pChInfo->scdVaEvent & ALG_LINK_SCD_DETECTOR_IMD)
       {   
           eventIdx = DEMO_SCD_VA_EVT_IMD;
       }
       if(!pChInfo->enableGpxEvntDisplay || !pChInfo->chOnMosaic || pChInfo->updateStaticObj || ((pChInfo->displayCnt == 4) && pChInfo->scdVaEvent))
       {
         grpx_fb_draw_osd_exit(
                              pChInfo->eventInfoData.startX, 
                              pChInfo->eventInfoData.startY, 
                              pChInfo->eventInfoData.width, 
                              pChInfo->eventInfoData.height,
                              1, (UInt8 *)gScd_ctrl.bufInfo[eventIdx].virtAddr);
         pChInfo->scdVaEvent = 0;
         reDrawStaticObj = TRUE;
       }
    }
    if(pChInfo->prevFrameObjects)
         reDrawStaticObj = TRUE;

    pChInfo->displayCnt++;
    if(pObjScdResult->frmResult)
    {
        pChInfo->eventInfoData.startX = pChInfo->scaledWinStartX + ((50 * pChInfo->scaledWinWidth)/100) - (MAX_SCD_VA_EVENT_GPX_DISPLAY_WIDTH*100/2);;
        pChInfo->eventInfoData.startY = pChInfo->scaledWinStartY + (90 * pChInfo->scaledWinHeight)/100;;
        pChInfo->eventInfoData.width  = MAX_SCD_VA_EVENT_GPX_DISPLAY_WIDTH;
        pChInfo->eventInfoData.height = MAX_SCD_VA_EVENT_GPX_DISPLAY_HEIGHT;

        eventIdx = DEMO_SCD_VA_EVT_TZ;
        if(pObjScdResult->frmResult & ALG_LINK_SCD_DETECTOR_TRIPZONE)
        {
            OSA_printf(" SCD   : Algo TZ Event Detected in Chan %d !!!\n", pObjScdResult->chId);
            eventIdx = DEMO_SCD_VA_EVT_TZ;
            pChInfo->scdVaEvent |= ALG_LINK_SCD_DETECTOR_TRIPZONE;
        }
        if(pObjScdResult->frmResult & ALG_LINK_SCD_DETECTOR_COUNTER)
        {
            OSA_printf(" SCD   : Algo OC Event Detected in Chan %d!!!\n", pObjScdResult->chId);
            eventIdx = DEMO_SCD_VA_EVT_OC;
            pChInfo->scdVaEvent |= ALG_LINK_SCD_DETECTOR_COUNTER;
        }
        if(pObjScdResult->frmResult & ALG_LINK_SCD_DETECTOR_TAMPER)
        {
//            OSA_printf(" SCD   : Algo CTD Event Detected in Chan %d !!!\n", pObjScdResult->chId);
            eventIdx = DEMO_SCD_VA_EVT_TD;
            pChInfo->scdVaEvent |= ALG_LINK_SCD_DETECTOR_TAMPER;
        }
        if(pObjScdResult->frmResult & ALG_LINK_SCD_DETECTOR_IMD)
        {
            OSA_printf(" SCD   : Algo IMD Event Detected in Chan %d!!!\n", pObjScdResult->chId);
            eventIdx = DEMO_SCD_VA_EVT_IMD;
            pChInfo->scdVaEvent |= ALG_LINK_SCD_DETECTOR_IMD;
        }
        if(pChInfo->chOnMosaic && pChInfo->enableGpxEvntDisplay)
        {
            grpx_fb_draw_osd(
                             pChInfo->eventInfoData.startX, 
                             pChInfo->eventInfoData.startY, 
                             pChInfo->eventInfoData.width, 
                             pChInfo->eventInfoData.height, 
                             1, (UInt8 *)gScd_ctrl.bufInfo[eventIdx].virtAddr);
        }
        else
            pChInfo->scdVaEvent = 0;
        pChInfo->displayCnt = 0;
#if 0
        {
            char eventTypeDisplayBuf[MAX_SCD_VA_EVENT_GPX_DISPLAY_TEXT_BUF_SIZE];
            Scd_VaPrepaerDispalyNumMsg(pObjScdResult->chId, (UInt8 *)gScd_ctrl.numBufInfo.virtAddr, (UInt8 *)eventTypeDisplayBuf);

            grpx_fb_draw_osd(winStartX + 18600, winStartY, 
                             72, MAX_SCD_VA_EVENT_GPX_CHAR_HEIGHT, 
                             1, (UInt8 *)eventTypeDisplayBuf
                             );
            grpx_fb_draw_osd(70000, 40000, 76, 14, 1, (UInt8 *)gScd_ctrl.chTxtBufInfo.virtAddr);
        }
#endif
    }
    if(pChInfo->prevFrameObjects)
    {
#if DEMO_SCD_MOTION_TRACK_DEBUG
      OSA_printf(" [SCD VA GPX]: Erasing Previous Object; Num of Object Detected : %d \n", pChInfo->prevFrameObjects);
#endif
      for(i = 0; i < pChInfo->prevFrameObjects; i++)
      {
          if(pChInfo->dynamicObject[i].presentOnDisplay)
          {
              grpx_fb_draw_polygon_exit(&pChInfo->dynamicObject[i]);
              pChInfo->dynamicObject[i].presentOnDisplay = FALSE;
          }
      }
    }
    if(pObjScdResult->numObjects && pChInfo->chOnMosaic && drawVaGpx)
    {
#if DEMO_SCD_MOTION_TRACK_DEBUG
        OSA_printf(" [SCD VA GPX]: Num of Object Detected : %d \n", pObjScdResult->numObjects);
#endif
        for(i = 0; i < pObjScdResult->numObjects ; i++)
        {
            pObjMetaData = &pObjScdResult->objMetadata[i];
            polygonePoints = &pChInfo->dynamicObject[i];
#if DEMO_SCD_MOTION_TRACK_DEBUG
            OSA_printf(" [SCD VA GPX]: Object Id %d \n", i);
            OSA_printf(" [SCD VA GPX]: StartX is %d StartY is %d\n", pObjMetaData->objBox.startX, pObjMetaData->objBox.startY);
            OSA_printf(" [SCD VA GPX]: Width is %d Height is %d\n", pObjMetaData->objBox.width, pObjMetaData->objBox.height);
            OSA_printf(" [SCD VA GPX]: Object Centroid is %d x %d\n", pObjMetaData->objCentroid.x, pObjMetaData->objCentroid.y);
#endif
            polygonePoints->noOfPoints = 5;

            polygonePoints->pixLocation[0].x = (pObjMetaData->objBox.startX * 100) / pChInfo->maxWidth;
            polygonePoints->pixLocation[0].y = (pObjMetaData->objBox.startY * 100) / pChInfo->maxHeight;
            polygonePoints->pixLocation[1].x = ((pObjMetaData->objBox.startX + pObjMetaData->objBox.width) * 100) / pChInfo->maxWidth;
            polygonePoints->pixLocation[1].y = polygonePoints->pixLocation[0].y;
            polygonePoints->pixLocation[2].x = polygonePoints->pixLocation[1].x;
            polygonePoints->pixLocation[2].y = ((pObjMetaData->objBox.startY + pObjMetaData->objBox.height) * 100) / pChInfo->maxHeight;
            polygonePoints->pixLocation[3].x = polygonePoints->pixLocation[0].x;
            polygonePoints->pixLocation[3].y = polygonePoints->pixLocation[2].y;
            polygonePoints->pixLocation[4].x = polygonePoints->pixLocation[0].x;
            polygonePoints->pixLocation[4].y = polygonePoints->pixLocation[0].y;

            polygonePoints->winWidth   = pChInfo->scaledWinWidth; 
            polygonePoints->windHeight = pChInfo->scaledWinHeight;
            polygonePoints->startX     = pChInfo->scaledWinStartX;
            polygonePoints->startY     = pChInfo->scaledWinStartY;

            polygonePoints->presentOnDisplay = TRUE;
            grpx_fb_draw_polygon(polygonePoints);
        }
    }
    if(pChInfo->initialise && 
         (pChInfo->updateStaticObj           || 
          pObjScdResult->polygonRtUpdated    || 
             !pChInfo->chOnMosaic            ||
             !drawVaGpx))     /* in case of SWITCH LAYOUT */
    {
      for(i = 0; i < pChInfo->prevFramePolygons; i++)
      {
          if(pChInfo->staticObject[i].presentOnDisplay)
          {
#if DEMO_SCD_MOTION_TRACK_DEBUG
            OSA_printf(" [SCD VA GPX]: SCD Layout Change of Ch-%d: Erasing Previous Polygon-%d\n", pObjScdResult->chId, i);
            OSA_printf(" [SCD VA GPX]: StartX %d StartY %d WinWidth %d WinHeight %d \n", 
               pChInfo->staticObject[i].startX,
               pChInfo->staticObject[i].startY,
               pChInfo->staticObject[i].winWidth,
               pChInfo->staticObject[i].windHeight);
#endif
             grpx_fb_draw_polygon_exit(&pChInfo->staticObject[i]);
             pChInfo->staticObject[i].presentOnDisplay = FALSE;
          }
      }
    }
    /* Redraw Plogon/static zone at below conditions .*/
    /* 1: When Layout Changes     
       2: Redraw if object were drawn in previous frame.
       3. Redraw if text message display of VA event was done.
       4: Redraw only if channel is present in current Mosaic 
       */
    if((reDrawStaticObj || pChInfo->updateStaticObj) && 
                                 pChInfo->chOnMosaic && 
                                 pChInfo->initialise && 
                                 drawVaGpx           && 
                                 !pObjScdResult->polygonRtUpdated)     /* in case of SWITCH LAYOUT */
    {
        Polygon_Points * tempPolygonePnts;
        for(i = 0; i < pChInfo->prevFramePolygons; i++)
        {
            tempPolygonePnts = &pChInfo->staticObject[i];
            tempPolygonePnts->winWidth   = pChInfo->scaledWinWidth; 
            tempPolygonePnts->windHeight = pChInfo->scaledWinHeight;
            tempPolygonePnts->startX     = pChInfo->scaledWinStartX;
            tempPolygonePnts->startY     = pChInfo->scaledWinStartY;
#if DEMO_SCD_MOTION_TRACK_DEBUG
            OSA_printf(" [SCD VA GPX]: SCD Layout Change of Ch-%d: Drawing New Polygon-%d \n", pObjScdResult->chId, i);
            OSA_printf(" [SCD VA GPX]: StartX %d StartY %d WinWidth %d WinHeight %d \n", 
               tempPolygonePnts->startX,
               tempPolygonePnts->startY,
               tempPolygonePnts->winWidth,
               tempPolygonePnts->windHeight);
          }    
#endif
            tempPolygonePnts->presentOnDisplay = TRUE;
            grpx_fb_draw_polygon(tempPolygonePnts);
        }
    }

    /* Redraw Plogon/static zone at belwo conditions .*/
     /* 1: When NumOfPolgon of non-Zero
       2: Redraw if pologon param is updated.
       3: Draw only if channel is present in current Mosaic 
       */

    if(pObjScdResult->numPolygons && pObjScdResult->polygonRtUpdated)
    {
        UInt32 j;
        pChInfo->prevFramePolygons =  pObjScdResult->numPolygons;
        for(i = 0; i < pObjScdResult->numPolygons; i++)
        {
          polygonePoints = &pChInfo->staticObject[i];
          polygonePoints->noOfPoints = pObjScdResult->zoneCfg[i].noOfPoints;
          for(j = 0; j < pObjScdResult->zoneCfg[i].noOfPoints;j++)
          {
#if DEMO_SCD_MOTION_TRACK_DEBUG
              OSA_printf(" [SCD VA GPX]: Polygon-%d Point-%d (%d,%d) !!!\n", \
                       i, j, pObjScdResult->zoneCfg[i].pixLocation[j].x,pObjScdResult->zoneCfg[i].pixLocation[j].y);

#endif
              polygonePoints->pixLocation[j].x = (pObjScdResult->zoneCfg[i].pixLocation[j].x * 100) / pChInfo->maxWidth;
              polygonePoints->pixLocation[j].y = (pObjScdResult->zoneCfg[i].pixLocation[j].y * 100) / pChInfo->maxHeight;

              polygonePoints->winWidth   = pChInfo->scaledWinWidth; 
              polygonePoints->windHeight = pChInfo->scaledWinHeight;
              polygonePoints->startX     = pChInfo->scaledWinStartX;
              polygonePoints->startY     = pChInfo->scaledWinStartY;
          }
          polygonePoints->presentOnDisplay = TRUE;
#if DEMO_SCD_MOTION_TRACK_DEBUG
          OSA_printf(" [SCD VA GPX]: StartX %d StartY %d WinWidth %d WinHeight %d \n", 
               polygonePoints->startX,
               polygonePoints->startY,
               polygonePoints->winWidth,
               polygonePoints->windHeight);
#endif
           if(pChInfo->chOnMosaic && drawVaGpx)
              grpx_fb_draw_polygon(polygonePoints);
        }
    }
    pChInfo->prevFrameObjects = pObjScdResult->numObjects;
    pChInfo->updateStaticObj = FALSE;

    return 0;
}

void *Scd_bitsWriteMain(void *pPrm)
{
    Int32                      status, frameId;
    VALG_FRAMERESULTBUF_LIST_S bitsBuf;
    VALG_FRAMERESULTBUF_S      *pBuf;
    Scd_ChInfo                 *pChInfo;

#if DEMO_SCD_ENABLE_FILE_WRITE
    FILE *fp = NULL;
    UInt32 fileWriteState = FILE_WRITE_STOPPED, writeDataSize;
#endif

    VSYS_PARAMS_S sysContextInfo;
    char                  threadName[128];

    snprintf(threadName,sizeof(threadName),"%s_%x",__func__,0);
    threadName[sizeof(threadName) - 1] = 0;
    OSA_printTID(threadName);

    Vsys_getContext(&sysContextInfo);

#if DEMO_SCD_ENABLE_FILE_WRITE
    if(gScd_ctrl.fileWriteEnable)
    {
        fp = fopen(gScd_ctrl.fileWriteName, "wb");
        if(fp!=NULL)
        {
            fileWriteState = FILE_WRITE_RUNNING;
            OSA_printf(" Opened file [%s] for writing CH%d\n", gScd_ctrl.fileWriteName, gScd_ctrl.fileWriteChn);
        }
        else
        {
            OSA_printf(" ERROR: File open [%s] for writing CH%d FAILED !!!!\n", gScd_ctrl.fileWriteName, gScd_ctrl.fileWriteChn);
        }
    }
#endif
    while(!gScd_ctrl.exitWrThr)
    {
        status = OSA_semWait(&gScd_ctrl.wrSem, OSA_TIMEOUT_FOREVER);
        if(status!=OSA_SOK)
            break;
        status = Vcap_getAlgResultBuffer(&bitsBuf, TIMEOUT_NO_WAIT);

        if(status==ERROR_NONE && bitsBuf.numBufs)
        {
            for(frameId=0; frameId<bitsBuf.numBufs; frameId++)
            {
                pBuf = &bitsBuf.bitsBuf[frameId];
                if(pBuf->chnId >= VENC_CHN_MAX)
                {
                   continue;
                }
                pChInfo = &gScd_ctrl.chInfo[pBuf->chnId];

                pChInfo->totalDataSize += pBuf->filledBufSize;
                pChInfo->numFrames++;
                if(vdMosaicParam.numberOfWindows == 0)
                   Vdis_getMosaicParams(VDIS_DEV_HDMI, &vdMosaicParam);

#if DEMO_SCD_ENABLE_MOTION_TRACKING
                Scd_TrackMotionOnDisplay(pBuf);
#endif
#if DEMO_SCD_ENABLE_FILE_WRITE
                if(gScd_ctrl.fileWriteEnable);
                {
                    if(pBuf->chnId == gScd_ctrl.chId && fileWriteState == FILE_WRITE_RUNNING)
                    {
                        writeDataSize = fwrite(pBuf->bufVirtAddr, 1, pBuf->filledBufSize, fp);
                        if(writeDataSize!=pBuf->filledBufSize)
                        {
                            fileWriteState = FILE_WRITE_STOPPED;
                            fclose(fp);
                            OSA_printf(" Closing file [%s] for CH%d\n", gScd_ctrl.fileWriteName, gScd_ctrl.fileWriteChn);
                        }
                    }
                }
#endif
            }
            Vcap_releaseAlgResultBuffer(&bitsBuf);
        }
    }

    gScd_ctrl.isWrThrStopDone = TRUE;
#if DEMO_SCD_ENABLE_FILE_WRITE
    if(gScd_ctrl.fileWriteEnable)
    {
        if(fileWriteState==FILE_WRITE_RUNNING)
        {
            fclose(fp);
            OSA_printf(" Closing file [%s] for CH%d\n", gScd_ctrl.fileWriteName, gScd_ctrl.fileWriteChn);
        }
    }
#endif
    return NULL;
}

Int32 Scd_bitsWriteCreate(UInt32 demoId, UInt32 scdResolution)
{
    VCAP_CALLBACK_S callback;

    Int32 status;
    UInt32 chId;
    Scd_ChInfo *pChInfo;
    UInt32 i;
    UInt8 *curVirtAddr;

    gScd_ctrl.chIdTrack            = 0;
    /* As default layout is 4x4 widnows */
    gScd_ctrl.numberOfWindows      = SCD_MAX_CHANNELS;

    for(chId=0; chId<VENC_CHN_MAX; chId++)
    {
        pChInfo = &gScd_ctrl.chInfo[chId];
        pChInfo->chOnMosaic           = FALSE;
        pChInfo->prevFrameObjects     = 0;
        pChInfo->prevFramePolygons    = 0;
        pChInfo->scdVaEvent           = 0;
        pChInfo->updateStaticObj      = FALSE;
        pChInfo->initialise           = FALSE;
        pChInfo->prevWinIdTrack       = 0xFF;
        pChInfo->gridPresent          = FALSE;
        pChInfo->updateGrid           = FALSE;
        pChInfo->enableMotionTracking = FALSE;
        pChInfo->enableGpxEvntDisplay = FALSE;
        for(i = 0; i<DEMO_SCD_VA_MAX_POLGON; i++)
        {
           pChInfo->staticObject[i].presentOnDisplay = FALSE;
        }
        for(i = 0; i<DEMO_SCD_VA_MAX_OBJECTS; i++)
        {
           pChInfo->dynamicObject[i].presentOnDisplay = FALSE;
        }
    }

#if DEMO_SCD_ENABLE_MOTION_TRACKING
#if defined(TI_816X_BUILD)
    Demo_scdVaParamInit(demoId);
    /* Making chanOffset zero as in Ne-Prog, SCD channels start from Chan-0 */
    if(demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE)
    {
       if(Demo_getMotionTrackEnable())
       {
          UInt32 chId;

          chId = Demo_getChId(" VA Graphics Event Display Channel", gDemo_info.maxVcapChannels);

          /* adding offset to get actual SCD channel no. as SCD is enabled in
           * either primary/secondary path */
          gScd_ctrl.chInfo[chId].enableGpxEvntDisplay = TRUE;

          /* As default layout is 4x4 widnows */
          gScd_ctrl.numberOfWindows = SCD_MAX_CHANNELS;
       }
    }
#endif
#endif
#if DEMO_SCD_ENABLE_FILE_WRITE
 
    gScd_ctrl.fileWriteEnable = Demo_getFileWriteEnable();

    if(gScd_ctrl.fileWriteEnable)
    {
        char path[256];
        UInt32 chanOffset;

        chanOffset = gDemo_info.maxVcapChannels;

        /* Making chanOffset zero as in Ne-Prog, SCD channels start from Chan-0 */
        if(demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE)
        {
             chanOffset = 0;
        }

        Demo_getFileWritePath(path, "/dev/shm");

        gScd_ctrl.fileWriteChn = Demo_getChId("File Write", gDemo_info.maxVcapChannels);

        sprintf(gScd_ctrl.fileWriteName, "%s/VID_CH%02d.bin", path, gScd_ctrl.fileWriteChn);

        /* adding offset to get actual SCD channel no. as SCD is enabled in
         * either primary/secondary path */
        gScd_ctrl.chId = gScd_ctrl.fileWriteChn + chanOffset;
    }
#endif

    gScd_ctrl.exitWrThr              = FALSE;
    gScd_ctrl.isWrThrStopDone        = FALSE;
    gScd_ctrl.prevNumberOfWindows    = gScd_ctrl.numberOfWindows;

    gScd_ctrl.startChId              = 0;
    gScd_ctrl.layoutUpdated           = FALSE;

    callback.newDataAvailableCb = Scd_bitsWriteCbFxn;

////////////////////////////////////////

#if 0
    OSA_printf(" Requesting for memory for Alphabet\n\n");        
    status = Vsys_allocBuf(SCD_BUF_HEAP_SR_ID, MAX_SCD_VA_EVENT_GPX_DISPLAY_TEXT_BUF_SIZE, SCD_BUFFER_ALIGNMENT, &gScd_ctrl.chTxtBufInfo);
    OSA_assert(status==OSA_SOK);
    curVirtAddr = gScd_ctrl.chTxtBufInfo.virtAddr;
    OSA_assert(sizeof(gMCFW_gpxTiScdVaChan_76x14_rgb)<=MAX_SCD_VA_EVENT_GPX_DISPLAY_TEXT_BUF_SIZE);
    memcpy(curVirtAddr, gMCFW_gpxTiScdVaChan_76x14_rgb, sizeof(gMCFW_gpxTiScdVaChan_76x14_rgb));

    OSA_printf(" Requesting for memory for numbers\n\n");        
    status = Vsys_allocBuf(SCD_BUF_HEAP_SR_ID, MAX_SCD_VA_EVENT_GPX_DISPLAY_TEXT_BUF_SIZE, SCD_BUFFER_ALIGNMENT, &gScd_ctrl.numBufInfo);
    OSA_assert(status==OSA_SOK);
    curVirtAddr = gScd_ctrl.numBufInfo.virtAddr;

    OSA_assert(sizeof(gMCFW_gpxTiScdVaNum_72x238_rgb) <= MAX_SCD_VA_EVENT_GPX_DISPLAY_TEXT_BUF_SIZE);
    memcpy(curVirtAddr, gMCFW_gpxTiScdVaNum_72x238_rgb, sizeof(gMCFW_gpxTiScdVaNum_72x238_rgb));
#endif
    for(i = 0; i < MAX_SCD_VA_EVENT; i++)
    {
        status = Vsys_allocBuf(SCD_BUF_HEAP_SR_ID, MAX_SCD_VA_EVENT_GPX_DISPLAY_BUF_SIZE, SCD_BUFFER_ALIGNMENT, &gScd_ctrl.bufInfo[i]);
        OSA_assert(status==OSA_SOK);
        curVirtAddr = gScd_ctrl.bufInfo[i].virtAddr;
        switch(i)
        {
            case DEMO_SCD_VA_EVT_TZ:
              OSA_assert(sizeof(gMCFW_gpxTiScdVaTzRgb_186x14_rgb)<=MAX_SCD_VA_EVENT_GPX_DISPLAY_BUF_SIZE);
              memcpy(curVirtAddr, gMCFW_gpxTiScdVaTzRgb_186x14_rgb, sizeof(gMCFW_gpxTiScdVaTzRgb_186x14_rgb));
              break;
            case DEMO_SCD_VA_EVT_OC:
              OSA_assert(sizeof(gMCFW_gpxTiScdVaOcRgb_186x14_rgb)<=MAX_SCD_VA_EVENT_GPX_DISPLAY_BUF_SIZE);
              memcpy(curVirtAddr, gMCFW_gpxTiScdVaOcRgb_186x14_rgb, sizeof(gMCFW_gpxTiScdVaOcRgb_186x14_rgb));
              break;
            case DEMO_SCD_VA_EVT_TD:
              OSA_assert(sizeof(gMCFW_gpxTiScdVaTdRgb_186x14_rgb)<=MAX_SCD_VA_EVENT_GPX_DISPLAY_BUF_SIZE);
              memcpy(curVirtAddr, gMCFW_gpxTiScdVaTdRgb_186x14_rgb, sizeof(gMCFW_gpxTiScdVaTdRgb_186x14_rgb));
              break;
            case DEMO_SCD_VA_EVT_IMD:
              OSA_assert(sizeof(gMCFW_gpxTiScdVaImdRgb_186x14_rgb)<=MAX_SCD_VA_EVENT_GPX_DISPLAY_BUF_SIZE);
              memcpy(curVirtAddr, gMCFW_gpxTiScdVaImdRgb_186x14_rgb, sizeof(gMCFW_gpxTiScdVaImdRgb_186x14_rgb));
              break;
            default:
              break;
         }
    }


///////////////////////////////////////
    /* Register call back with encoder */
    Vcap_registerBitsCallback(&callback,
                         (Ptr)&gScd_ctrl);

    status = OSA_semCreate(&gScd_ctrl.wrSem, 1, 0);
    OSA_assert(status==OSA_SOK);

    status = OSA_thrCreate(
        &gScd_ctrl.wrThrHndl,
        Scd_bitsWriteMain,
        OSA_THR_PRI_DEFAULT,
        0,
        &gScd_ctrl
        );

    OSA_assert(status==OSA_SOK);

    OSA_waitMsecs(100); // allow for print to complete
    return OSA_SOK;
}

Void Scd_bitsWriteStop()
{
    gScd_ctrl.exitWrThr = TRUE;
}
Int32 Scd_bitsWriteDelete()
{
    UInt32 i;
    gScd_ctrl.exitWrThr = TRUE;

    OSA_semSignal(&gScd_ctrl.wrSem);

    OSA_printf("Motion Tracking Delete Call");

    while(!gScd_ctrl.isWrThrStopDone)
    {
        OSA_waitMsecs(10);
    }

    for(i = 0; i < MAX_SCD_VA_EVENT; i++)
    {
      if(gScd_ctrl.bufInfo[i].virtAddr != NULL)
      {
          Vsys_freeBuf(SCD_BUF_HEAP_SR_ID, gScd_ctrl.bufInfo[i].virtAddr, MAX_SCD_VA_EVENT_GPX_DISPLAY_BUF_SIZE);
      }
    }
//    Vsys_freeBuf(SCD_BUF_HEAP_SR_ID, gScd_ctrl.numBufInfo.virtAddr, MAX_SCD_VA_EVENT_GPX_DISPLAY_TEXT_BUF_SIZE);
    OSA_thrDelete(&gScd_ctrl.wrThrHndl);
    OSA_semDelete(&gScd_ctrl.wrSem);

    return OSA_SOK;
}

int  Scd_TrackMotionOnDisplay(VALG_FRAMERESULTBUF_S *pBuf)
{
    UInt32 numBlkChg;
    UInt32 monitoredBlk;
    UInt32 numHorzBlks, numVerBlks, numBlksInFrame, blkHeight;
    AlgLink_ScdResult *pChResult;
    UInt32 scdResultBuffChanId;
    Scd_ChInfo  *pChInfo;
    UInt32      blkIdx;
#if USE_FBDEV
    UInt32 winIdx = 0;


    int  width, height;
    int  startX, startY;
    Bool   gridErase;
#endif
    pChInfo = &gScd_ctrl.chInfo[pBuf->chnId];
#if USE_FBDEV
    if(pChInfo->initialise == FALSE)
    {
        pChInfo->maxWidth  = pBuf->frameWidth;
        pChInfo->maxHeight = pBuf->frameHeight;

        for(blkIdx = 0; blkIdx < ALG_LINK_SCD_MAX_BLOCKS_IN_FRAME; blkIdx++)
        {
           pChInfo->blkTag[blkIdx] = 0;
        }
         OSA_printf(" [SCD VA GPX]: Chan-Id No. %d DisplayWidth %d DisplayHeight %d\n", 
                   pBuf->chnId, gScd_ctrl.displayWidth, gScd_ctrl.displayHeight);
         Scd_updateLayoutParam();
    }

    gridErase = FALSE;
    if((pChInfo->maxWidth != pBuf->frameWidth) ||
        (pChInfo->maxHeight != pBuf->frameHeight))
    {
       gridErase = TRUE;
    }
#endif
    pChResult = (AlgLink_ScdResult *)pBuf->bufVirtAddr;

#if defined(TI_816X_BUILD)
    if(gDemo_info.usecase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC)
    {
        /* Not adding Offset  as in Ne-Prog, SCD channels start from Chan-0 */
        scdResultBuffChanId = pChResult->chId;
    }
    else
#endif
      scdResultBuffChanId = pChResult->chId - gDemo_info.maxVcapChannels;   /* Subtracting offset as SCD Channels are on Secondary path */

    monitoredBlk = 0;
    numBlkChg    = 0;

    numHorzBlks     = ((pChInfo->maxWidth + 0x1F ) & (~0x1F)) / 32;  /* Rounding to make divisible by 32 */
    if((pChInfo->maxHeight%ALG_LINK_SCD_BLK_HEIGHT_MIN) == 0)/* For Block height is divisible by 10 */
       blkHeight = ALG_LINK_SCD_BLK_HEIGHT_MIN;
    else   /* For Block height is divisible by 12 */
       blkHeight = ALG_LINK_SCD_BLK_HEIGHT;

    numVerBlks    = pChInfo->maxHeight / blkHeight;

    numBlksInFrame = numHorzBlks * numVerBlks;

#if USE_FBDEV

    if(gScd_ctrl.layoutUpdated)
    {
#if DEMO_SCD_MOTION_TRACK_DEBUG
        OSA_printf(" [SCD VA GPX]: Ch-Id %d WinWidth %d, WinHeight %d, StartX %d StartY %d DWidth %d DHeight %d\n",
                                             pChResult->chId, pChInfo->curWinWidth, pChInfo->curWinHeight,
                                              pChInfo->curWinStartX, pChInfo->curWinStartY,
                                              gScd_ctrl.displayWidth, gScd_ctrl.displayHeight
                                              );
#endif
        Scd_updateLayoutParam();
#if DEMO_SCD_MOTION_TRACK_DEBUG
        OSA_printf(" [SCD VA GPX]: Updated Ch-Id %d WinWidth %d, WinHeight %d, StartX %d StartY %d DWidth %d DHeight %d\n",
                                             pChResult->chId, pChInfo->curWinWidth, pChInfo->curWinHeight,
                                              pChInfo->curWinStartX, pChInfo->curWinStartY,
                                              gScd_ctrl.displayWidth, gScd_ctrl.displayHeight
                                              );
#endif
    }

    /* Find out where the tracking channel is displayed on current mosaic */
    for(winIdx=0;winIdx < vdMosaicParam.numberOfWindows; winIdx++)
    {
        if((vdMosaicParam.chnMap[winIdx]%gDemo_info.maxVcapChannels) ==  pChResult->chId ) //trackChId)
        {
           pChInfo->chOnMosaic = TRUE;
           break;
        }
    }
    if(winIdx >= vdMosaicParam.numberOfWindows)
       pChInfo->chOnMosaic = FALSE;

#if defined(TI_816X_BUILD)
    Scd_VaProcessMetaData(winIdx,  pChResult, pChInfo);
#endif

    if(pChInfo->enableMotionTracking)
    {
        if((pChInfo->gridPresent ==  TRUE) &&
            ((!pChInfo->chOnMosaic ||                      /*if channel to be tracked does not lie on the current mosaic,*/
             (pChInfo->updateGrid) ||      /* to disable LMD if channel is switched or different channel is to be tracked */
                gScd_ctrl.layoutUpdated) || (gridErase ==  TRUE)))  /* in case of SWITCH LAYOUT, REDRAW GRID(acc to new layout) */
        {
           OSA_printf("\n\n");
           OSA_printf(" [SCD MOTION TRACK]: Grid clean up \n");
           if(!pChInfo->chOnMosaic)
           {
               OSA_printf(" [SCD MOTION TRACK]: When Tracking channel is not in Current Mosaic Layout \n");
               OSA_printf(" [SCD MOTION TRACK]: Starting Channel ID in the Current layout %d Max Channels in the Mosaic %d \n",
                          gScd_ctrl.startChId, gScd_ctrl.numberOfWindows);
           }
           if(pChInfo->updateGrid)
           {
                 OSA_printf(" [SCD MOTION TRACK]: When Num Win changes \n");
                 OSA_printf(" [SCD MOTION TRACK]: Previous Num of Win %d Current Num of Win %d \n",
                          gScd_ctrl.prevNumberOfWindows, gScd_ctrl.numberOfWindows);
           }
           gridErase    =  TRUE;
        }
        else
        {
           gridErase    = FALSE;;
        }

        if((gridErase ==  TRUE) && (pChInfo->gridPresent == TRUE))
        {
            Scd_windowGrid(FALSE, numHorzBlks, pChInfo);        /* undraw grid acc to previous no of windows */
            OSA_printf(" [SCD MOTION TRACK]: Grid Erase Finished \n");

            for(blkIdx = 0; blkIdx < numBlksInFrame; blkIdx++)
            {
              if(pChInfo->blkTag[blkIdx] == 1 )    /* in case of channel switch,all previously drawn boxes are erased */
              {
                  OSA_printf(" [SCD MOTION TRACK]: Box clean up when Tracking channel changes \n");

                  width  = pChInfo->prevWinWidth/numHorzBlks;
                  height = pChInfo->prevWinHeight/numVerBlks;
                  startX = pChInfo->prevWinStartX + ((blkIdx%numHorzBlks) * width);
                  startY = pChInfo->prevWinStartY + ((blkIdx/numHorzBlks) * height);
                  width  = (width * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH)/gScd_ctrl.displayWidth - 4;
                  height = (height * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT)/gScd_ctrl.displayHeight - 4;
                  startX = (startX * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH)/gScd_ctrl.displayWidth + 2;
                  startY = (startY * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT)/gScd_ctrl.displayHeight + 2;

                  grpx_draw_box_exit(width,height,startX, startY);
                  pChInfo->blkTag[blkIdx] = 0;              }
            }
            pChInfo->updateGrid    = TRUE;
            pChInfo->gridPresent =  FALSE;
        }

        /* Recalculating Few variables if resolution has changed.*/
        numHorzBlks     = ((pBuf->frameWidth + 0x1F ) & (~0x1F)) / 32;  /* Rounding to make divisible by 32 */
 
        if(!pChInfo->chOnMosaic && pChInfo->updateGrid)  /*if channel to be tracked does not lie on the current mosaic,*/
        {
          pChInfo->updateGrid = FALSE;
          OSA_printf(" [SCD MOTION TRACK]: Disabling Grid Draw as Channel is not Present in the Current layout \n");
        }
//        printf("GridPresent %d updateGrid %d chId %d \n", pChInfo->gridPresent, pChInfo->updateGrid, scdResultBuffChanId);
        if(!pChInfo->gridPresent && pChInfo->updateGrid)
        {
            OSA_printf(" [SCD MOTION TRACK]: Fresh Grid Drawing \n");
            Scd_windowGrid(TRUE, numHorzBlks, pChInfo);
            pChInfo->prevWinIdTrack  = winIdx;              // update prevWinIdTrack
            pChInfo->updateGrid      =  FALSE;
            pChInfo->gridPresent     =  TRUE;
        }
     }
     else
     {
       if((pChInfo->updateGrid  ==  FALSE) && (pChInfo->gridPresent == TRUE))
       {
           OSA_printf(" [SCD MOTION TRACK]: Grid clean up when Motion Tracking is disabled \n");
           Scd_windowGrid(FALSE, numHorzBlks, pChInfo);

           for(blkIdx = 0; blkIdx < numBlksInFrame; blkIdx++)
           {
               if(pChInfo->blkTag[blkIdx] == 1 )                    /* in case of channel/layout switch,all previously drawn boxes are erased */
               {
                  OSA_printf(" [SCD MOTION TRACK]: Box clean up when Motion Tracking is disabled \n");

                  width  = pChInfo->prevWinWidth/numHorzBlks;
                  height = pChInfo->prevWinHeight/numVerBlks;
                  startX = pChInfo->prevWinStartX + ((blkIdx%numHorzBlks) * width);
                  startY = pChInfo->prevWinStartY + ((blkIdx/numHorzBlks) * height);
                  width  = (width * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH)/gScd_ctrl.displayWidth - 4;
                  height = (height * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT)/gScd_ctrl.displayHeight - 4;
                  startX = (startX * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH)/gScd_ctrl.displayWidth + 2;
                  startY = (startY * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT)/gScd_ctrl.displayHeight + 2;

                  grpx_draw_box_exit(width,height,startX, startY);
                  pChInfo->blkTag[blkIdx] = 0;
               }
           }
       }
       pChInfo->updateGrid  = FALSE;
       pChInfo->gridPresent =  FALSE;
     }
#endif
    if(gScd_ctrl.layoutUpdated)
    {

        OSA_printf(" [SCD MOTION TRACK]: WinWidth %d, WinHeight %d, StartX %d StartY %d DWidth %d DHeight %d\n",
                                             pChInfo->curWinWidth, pChInfo->curWinHeight,
                                              pChInfo->curWinStartX, pChInfo->curWinStartY,
                                              gScd_ctrl.displayWidth, gScd_ctrl.displayHeight
                                              );
        gScd_ctrl.layoutUpdated = FALSE;
    }

    /* Assigning current frame width and height to struct for drawing blocks for current frame.*/
    pChInfo->maxWidth = pBuf->frameWidth;
    pChInfo->maxHeight = pBuf->frameHeight;

  
    if((pBuf->frameHeight%ALG_LINK_SCD_BLK_HEIGHT_MIN) == 0)/* For Block height is divisible by 10 */
       blkHeight = ALG_LINK_SCD_BLK_HEIGHT_MIN;
    else   /* For Block height is divisible by 12 */
       blkHeight = ALG_LINK_SCD_BLK_HEIGHT;
   
     numVerBlks    = pBuf->frameHeight / blkHeight;
   
     numBlksInFrame = numHorzBlks * numVerBlks;
   
     /* Logic  to see how many blocks of total enabled blocks experienced change.
     * For each block, algorithm returns no. of pixels changed in the current
     * frame. This is compared against thresold determined using SCD sensitivity .
     * if changed pixels are more than the calculated thresold, block is marked as changed
     * i.e. motion is detected in the block.
     * Minimum value of thresold is 20% and then it is incrmented by 10% for
     * each sensitivity level change. Thresold can vary from 20% - 100%
     * At max sensitivity, thresold would be 20%. That means if 20% pixels
     * are changed block is marked as changed.
     * At minimu sensitivity, thresold would be 100%. That means if 100% pixels
     * are changed block is marked as changed */
     for(blkIdx = 0; blkIdx < numBlksInFrame; blkIdx++)
     {
        AlgLink_ScdblkChngConfig * blockConfig;

        blockConfig = &pChResult->blkConfig[blkIdx];
                                                                                       
        if(blockConfig->monitored == 1)
        {
            UInt32 threshold;

            monitoredBlk++;
            threshold = DEMO_SCD_MOTION_DETECTION_SENSITIVITY(ALG_LINK_SCD_BLK_WIDTH, blkHeight) +
                              (DEMO_SCD_MOTION_DETECTION_SENSITIVITY_STEP * (ALG_LINK_SCD_SENSITIVITY_MAX - blockConfig->sensitivity));

            if(pChResult->blkResult[blkIdx].numPixelsChanged > threshold)
            {
                numBlkChg++;
#if USE_FBDEV
                if((pChInfo->enableMotionTracking) && pChInfo->gridPresent)
                {
                    width       = pChInfo->curWinWidth/numHorzBlks;
                    height      = pChInfo->curWinHeight/numVerBlks;
                    startX      = pChInfo->curWinStartX + ((blkIdx%numHorzBlks) * width);
                    startY      = pChInfo->curWinStartY + ((blkIdx/numHorzBlks) * height);
                    width = (width * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH)/gScd_ctrl.displayWidth - 4;
                    height = (height * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT)/gScd_ctrl.displayHeight - 4;
                    startX = (startX * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH)/gScd_ctrl.displayWidth + 2;
                    startY = (startY * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT)/gScd_ctrl.displayHeight + 2;
                    if(pChInfo->blkTag[blkIdx] == 0)
                    {
                      grpx_draw_box(width, height, startX, startY);
                    }
                    pChInfo->blkTag[blkIdx] = 1;
                 }
#endif
            }
#if USE_FBDEV
            else
            {
                if((pChInfo->enableMotionTracking) && pChInfo->gridPresent)
                {
                   if(pChInfo->blkTag[blkIdx] == 1 )
                   {
                     width       = pChInfo->curWinWidth/numHorzBlks;
                     height      = pChInfo->curWinHeight/numVerBlks;
                     startX      = pChInfo->curWinStartX + ((blkIdx%numHorzBlks) * width);
                     startY      = pChInfo->curWinStartY + ((blkIdx/numHorzBlks) * height);
                     width = (width * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH)/gScd_ctrl.displayWidth - 4;
                     height = (height * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT)/gScd_ctrl.displayHeight - 4;
                     startX = (startX * DEMO_SCD_MOTION_TRACK_GRPX_WIDTH)/gScd_ctrl.displayWidth + 2;
                     startY = (startY * DEMO_SCD_MOTION_TRACK_GRPX_HEIGHT)/gScd_ctrl.displayHeight + 2;
                     grpx_draw_box_exit(width,height,startX, startY);
                     pChInfo->blkTag[blkIdx] = 0;
                   }

                }
            }
#endif
        }
    }
    gScd_ctrl.prevNumberOfWindows = gScd_ctrl.numberOfWindows;        // update prevWindows
#if USE_FBDEV
    pChInfo->prevWinIdTrack = winIdx;
#endif
    if((monitoredBlk > 0) && (numBlkChg > 0))
    {
        int newChNum = scdResultBuffChanId;

        if(newChNum >= Vcap_getNumChannels())
        {
            newChNum -=   Vcap_getNumChannels();
        }
        OSA_printf(" [MOTION DETECTED] %d: SCD CH <%d> CAP CH = %d \n",
                 OSA_getCurTimeInMsec(), pChResult->chId, newChNum);
    }
   if(!pChInfo->initialise)
      pChInfo->initialise = TRUE;
   return 0;
}

