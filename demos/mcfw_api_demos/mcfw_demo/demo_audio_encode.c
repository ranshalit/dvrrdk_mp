#include <demo_audio.h>

static OSA_ThrHndl     gAppEncodeThread;
static Bool            gAppEncodeThreadActive = FALSE;
static Bool            gAppEncodeThreadExitFlag = FALSE;
static Audio_EncInfo   gEncInfo;

static Void *App_encodeTaskFxn(Void * prm)
{
    FILE                  *in = NULL, *out = NULL;
    UInt8                 *inBuf = NULL;
    UInt8                 *outBuf = NULL;
    AENC_PROCESS_PARAMS_S encPrm;
    Int32                 rdIdx, to_read, readBytes;
    Int32                 frameCnt = 0, totalBytes = 0;
    Int32                 inBytes, inBufSize, outBufSize;

    AENC_CREATE_PARAMS_S  aencParams;
    Void                  *encHandle = NULL;
    Audio_EncInfo         *info = prm;
    Bool                  isSharedRegion = FALSE;
    char                  threadName[128];

    snprintf(threadName,sizeof(threadName),"%s_%x",__func__,0);
    threadName[sizeof(threadName) - 1] = 0;
    OSA_printTID(threadName);

    if (!prm)
    {
        printf ("AENC task failed <invalid param>...........\n");
        return NULL;
    }

    memset(&aencParams, 0, sizeof(aencParams));
    memset(&encPrm, 0, sizeof(encPrm));

    aencParams.bitRate = info->bitRate;
    aencParams.encoderType = (info->encodeType == 0) ? AUDIO_CODEC_TYPE_AAC_LC : AUDIO_CODEC_TYPE_G711;
    aencParams.numberOfChannels = info->numChannels;
    aencParams.sampleRate = info->sampleRate;

    encHandle = Aenc_create(&aencParams);
    if (encHandle)
    {
        printf ("AENC Create done...........\n");
    }
    else
    {
        printf ("AENC Create failed...........\n");
        printf ("\n********Encode APP Task Exitting....\n");
        gAppEncodeThreadActive = FALSE;
        gAppEncodeThreadExitFlag = TRUE;
        return NULL;
    }

    gAppEncodeThreadExitFlag = FALSE;

    if (aencParams.encoderType == AUDIO_CODEC_TYPE_AAC_LC)
    {
        inBufSize = (MAX_IN_SAMPLES * SAMPLE_LEN * aencParams.numberOfChannels);
        if (inBufSize < aencParams.minInBufSize)
            inBufSize = aencParams.minInBufSize;
    
        outBufSize = MAX_OUTPUT_BUFFER;
        if (outBufSize < aencParams.minOutBufSize)
            outBufSize = aencParams.minOutBufSize;
    }
    else
    {
        inBufSize = MAX_INPUT_BUFFER;
        outBufSize = MAX_OUTPUT_BUFFER;
    }

    inBytes = inBufSize;

    in = fopen(info->inFile, "rb");
    if (!in)
    {
        printf ("File <%s> Open Error....\n", info->inFile);
    }  
    out = fopen(info->outFile, "wb");  
    if (!out)
    {
        printf ("File <%s> Open Error....\n", info->outFile);
    }  
    if (aencParams.encoderType == AUDIO_CODEC_TYPE_AAC_LC)
        isSharedRegion = TRUE;

    inBuf = App_allocBuf(inBytes, isSharedRegion);
    outBuf = App_allocBuf(outBufSize, isSharedRegion); 

    if (!inBuf || !outBuf)
    {
        printf ("Memory Error....\n");
    }

    if (in && out && inBuf && outBuf)
    {
        printf ("\n\n=============== Starting Encode ===================\n");
        sleep(1);

        rdIdx = 0;
        to_read = inBytes;
        encPrm.outBuf.dataBuf = outBuf;

        while (gAppEncodeThreadActive == TRUE)
        {
            readBytes = fread (inBuf + rdIdx, 1, to_read, in);
            if (readBytes)
            {
                encPrm.inBuf.dataBufSize = readBytes;
                encPrm.inBuf.dataBuf = inBuf;
                encPrm.outBuf.dataBufSize = outBufSize;
                if (Aenc_process(encHandle, &encPrm) < 0)
                {
                    printf ("ENC: Encode process failed... Corrupted handled!!!! ....\n");
                    break;
                }

                if (encPrm.outBuf.dataBufSize <= 0)
                {
                    printf ("ENC: Encoder didnt generate bytes <remaining - %d>... exiting....\n", readBytes);
                    printf ("=============== Encode completed, bytes generated <%d> ================\n",
                            totalBytes);
                    break;
                }
                if (out)
                {
                    fwrite(encPrm.outBuf.dataBuf, 1, encPrm.outBuf.dataBufSize, out);
                }
                {
//                    printf ("ENC - %d, Bytes Generated - %d <total %d>, bytesConsumed - %d\n", 
//                            frameCnt,  encPrm.outBuf.dataBufSize, totalBytes, encPrm.inBuf.dataBufSize);              
                }
                frameCnt++;
                totalBytes += encPrm.outBuf.dataBufSize;
            }
            else
            {
                printf ("=============== Encode completed, bytes generated <%d> ================\n",
                        totalBytes);
                break;
            }
        }
    }
    else
    {
        printf ("\n\n=============== Encode not starting.... file / mem error ============\n");
    }
    prm = prm;
    if (in)
        fclose(in);
    if (out)
        fclose(out);
    if (inBuf)
        App_freeBuf(inBuf, inBufSize, isSharedRegion);
    if (outBuf)
        App_freeBuf(outBuf, outBufSize, isSharedRegion);

    Aenc_delete(encHandle);

    printf ("\n********Encode APP Task Exitting....\n");
    gAppEncodeThreadActive = FALSE;
    gAppEncodeThreadExitFlag = TRUE;

    return NULL;
}

Bool Demo_startAudioEncodeSystem (Void)
{
    char ch;
    Bool ret = FALSE;

    printf("\r\n\n\n AUDIO: Enable Encode <Y/N>: " );
    ch = Demo_getChar();
    
    if (ch == 'y' || ch == 'Y')
    {
        Int32 status;

        printf("\r\n AUDIO: Enter input PCM file name <absolute path>: " );
        fflush(stdin);
        fgets(gEncInfo.inFile, MAX_INPUT_STR_SIZE, stdin);
        gEncInfo.inFile[ strlen(gEncInfo.inFile)-1 ] = 0;

        printf("\r\n AUDIO: Enter output file name <absolute path>: " );
        fflush(stdin);
        fgets(gEncInfo.outFile, MAX_INPUT_STR_SIZE, stdin);
        gEncInfo.outFile[ strlen(gEncInfo.outFile)-1 ] = 0;

        gEncInfo.encodeType = Demo_getIntValue("encode Type <0 - AAC-LC, 1 - G711>", 0, 1, 0);
#ifndef  DSP_RPE_AUDIO_ENABLE
        if (gEncInfo.encodeType == 0)
        {
            printf ("AAC-LC encode <DSP Based> not supported!!!!\n");
            return ret;
        }
#endif
        if (gEncInfo.encodeType == 0)
        {
            gEncInfo.bitRate = Demo_getIntValue("AUDIO: bitrate <value as per encode requirement>", 24000, 800000, 24000);

            gEncInfo.numChannels = Demo_getIntValue("AUDIO: num of audio channels", 1, 2, 1);

            gEncInfo.sampleRate = Demo_getIntValue("AUDIO: Sample Rate", 16000, 48000, 16000);
        }

        gAppEncodeThreadActive = TRUE;
        status = OSA_thrCreate(&gAppEncodeThread,
                      App_encodeTaskFxn,
                      AUDIO_TSK_PRI, 
                      AUDIO_TSK_STACK_SIZE, 
                      &gEncInfo);
        if (status != 0)
        {
            gAppEncodeThreadActive = FALSE;
            printf ("AUDIO: App Encode thread create failed...\n");
            return ret;
        }
        ret = TRUE;
    }
    printf ("\r\n\n");
    return ret;
}


Bool Demo_stopAudioEncodeSystem (Bool userOpt)
{
    Bool ret = FALSE;

    if (gAppEncodeThreadActive == TRUE)
    {
        char ch = 'Y';

        if (userOpt == TRUE)
        {
            printf("\r\n\n\nAUDIO: Stop Encode <Y/N>: " );
            ch = Demo_getChar();
        }

        if (ch == 'y' || ch == 'Y')
        {
            gAppEncodeThreadActive = FALSE;

            while (gAppEncodeThreadExitFlag == FALSE)
            {
                printf ("**** Waiting for Encode task to exit .....\n");
                OSA_waitMsecs(1000);
            }
            OSA_thrDelete(&gAppEncodeThread);
            printf ("AUDIO:  Encode stopped....\n");
            ret = TRUE;
        }
    }
    printf ("\r\n\n");
    return ret;
}


Bool Demo_IsEncodeActive(Void)
{
    return gAppEncodeThreadActive;
}
