
#include <demo.h>
#include "iniparser.h"

Demo_ScdInfo gDemo_ScdInfo;

#define MCFW_DEMO_SCD_VA_INI_FILE_NO                                   "DMVAConfigChId"
#define MCFW_DEMO_SCD_VA_INI_FILE_CHID                                 "chId"
#define MCFW_DEMO_SCD_VA_INI_FILE_DETECTMODE                           "detectmode"
#define MCFW_DEMO_SCD_VA_INI_FILE_SENSITIVITY                          "sensitiveness"
#define MCFW_DEMO_SCD_VA_INI_FILE_OBJECT_IN_VIEW                       "objectsInView"
#define MCFW_DEMO_SCD_VA_INI_FILE_DIRECTION                            "direction"
#define MCFW_DEMO_SCD_VA_INI_FILE_DETECTIONRATE                        "detectionrate"
#define MCFW_DEMO_SCD_VA_INI_FILE_TAMPER_RESET_CNT                     "tamperResetCnt"
#define MCFW_DEMO_SCD_VA_INI_FILE_TAMPER_BG_REFRESH_INTERVAL           "tamperBGRefreshInterval"
#define MCFW_DEMO_SCD_VA_INI_FILE_ORIENTATION                          "orientation"
#define MCFW_DEMO_SCD_VA_INI_FILE_MIN_PERSON_SIZE                      "minPersonSize"
#define MCFW_DEMO_SCD_VA_INI_FILE_MAX_PERSON_SIZE                      "maxPersonSize"
#define MCFW_DEMO_SCD_VA_INI_FILE_MIN_VEHICLE_SIZE                     "minVehicleSize"
#define MCFW_DEMO_SCD_VA_INI_FILE_MAX_VEHICLE_SIZE                     "maxVehicleSize"
#define MCFW_DEMO_SCD_VA_INI_FILE_NO_OF_ZONE                           "numOfZone"
#define MCFW_DEMO_SCD_VA_INI_FILE_ZONE_ID                              "zoneId"
#define MCFW_DEMO_SCD_VA_INI_FILE_ZONE_VALID                           "zoneValid"
#define MCFW_DEMO_SCD_VA_INI_FILE_ZONE_TYPE                            "zoneType"
#define MCFW_DEMO_SCD_VA_INI_FILE_ZONE_DIR                             "zoneDir"
#define MCFW_DEMO_SCD_VA_INI_FILE_ZONE_NUM_OF_POINT                    "zoneNumPoints"
#define MCFW_DEMO_SCD_VA_INI_FILE_ZONE_XY                              "xy"

#define MCFW_DEMO_SCD_VA_MAX_FILE_NUM_IN_INI                           100
#define MCFW_DEMO_SCD_VA_MAX_RES_NUM                                   10


#define MCFW_DEMO_SCD_VA_MAX_FILE_EXTENSION_LENGTH                      (4)
                                                                        

Int32  scdTileConfig[SCD_MAX_CHANNELS][SCD_MAX_TILES];

static Int32 Demo_scdGetXYValues(char * data, UInt32 * x, UInt32 * y)
{
    char * tempArray;

    tempArray = strtok (data,"x");
    *x = atoi(tempArray);
    while (tempArray != NULL)
    {
       *y = atoi(tempArray);
       tempArray = strtok (NULL, "x");
    }
    return 0;
}
static Int32 Demo_scdGetCfgArray(dictionary *ini)
{
    UInt32 totalChnlNum,i,j,k;
    UInt32 chanIdx;
    char temp[256];
    char tempSecName[256];
    char *name;
    char xyValue[256];
    Demo_ScdInfo * scd_configInfo;
    AlgLink_ScdChVaParams *cfg;

    scd_configInfo = &gDemo_ScdInfo;
    
    totalChnlNum = iniparser_getnsec(ini);
    scd_configInfo->numVaCh = 0;
    chanIdx = 0;
    for (i = 0; i < totalChnlNum; i++)
    {
        UInt32 chId;
        scd_configInfo->numVaCh++;

        name = iniparser_getsecname(ini,i);
//        printf("Section name is %s \n", name);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_CHID);
//        printf("New section name is %s \n", temp);
        chId = iniparser_getint(ini,temp,0);;

        cfg = &gDemo_ScdInfo.chInfo[chanIdx].scdChPrm.scdVaAlgCfg;
        cfg->chId = chId;//iniparser_getint(ini,temp,0);;
        chanIdx++;

        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_DETECTMODE);
        cfg->mode = iniparser_getint(ini,temp,0);

        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_SENSITIVITY);
        cfg->sensitiveness = iniparser_getint(ini,temp,0);

        name  = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_OBJECT_IN_VIEW);
        cfg->objectInView = iniparser_getint(ini,temp,0);

        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_DIRECTION);
        cfg->direction = iniparser_getint(ini,temp,1);

        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_DETECTIONRATE);
        cfg->detectionRate = iniparser_getint(ini,temp,1);

        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_TAMPER_RESET_CNT);
        cfg->tamperResetCnt = iniparser_getint(ini,temp,10);

        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_TAMPER_BG_REFRESH_INTERVAL);
        cfg->tamperBGRefreshInterval = iniparser_getint(ini,temp,10);

        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_ORIENTATION);
        cfg->orientation = iniparser_getint(ini,temp,1);

        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_MIN_PERSON_SIZE);
        strcpy(tempSecName,iniparser_getstring(ini,temp,0));
        Demo_scdGetXYValues(tempSecName, &cfg->minPersonSize.width, &cfg->minPersonSize.height); 


        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_MAX_PERSON_SIZE);
        strcpy(tempSecName,iniparser_getstring(ini,temp,0));
        Demo_scdGetXYValues(tempSecName, &cfg->maxPersonSize.width, &cfg->maxPersonSize.height); 
        
        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_MIN_VEHICLE_SIZE);
        strcpy(tempSecName,iniparser_getstring(ini,temp,0));
        Demo_scdGetXYValues(tempSecName, &cfg->minVehicleSize.width, &cfg->minVehicleSize.height); 

        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_MAX_VEHICLE_SIZE);
        strcpy(tempSecName,iniparser_getstring(ini,temp,0));
        Demo_scdGetXYValues(tempSecName, &cfg->maxVehicleSize.width, &cfg->maxVehicleSize.height); 

        name = iniparser_getsecname(ini,i);
        sprintf(temp,"%s:%s",name,MCFW_DEMO_SCD_VA_INI_FILE_NO_OF_ZONE);
        cfg->numZone = iniparser_getint(ini,temp,1);
        i += cfg->numZone;
        
       for (j = 0; j < cfg->numZone; j++)
       {
          sprintf(tempSecName,"%s-zone0%d",name,j);
          sprintf(temp,"%s:%s",tempSecName,MCFW_DEMO_SCD_VA_INI_FILE_ZONE_ID);
          cfg->zoneCfg[j].zoneId = iniparser_getint(ini,temp,0);

          sprintf(tempSecName,"%s-zone0%d",name,j);
          sprintf(temp,"%s:%s",tempSecName,MCFW_DEMO_SCD_VA_INI_FILE_ZONE_VALID);
          cfg->zoneCfg[j].activeZone = iniparser_getint(ini,temp,0);

          sprintf(tempSecName,"%s-zone0%d",name,j);
          sprintf(temp,"%s:%s",tempSecName,MCFW_DEMO_SCD_VA_INI_FILE_ZONE_TYPE);
          cfg->zoneCfg[j].zoneType = iniparser_getint(ini,temp,0);

          sprintf(tempSecName,"%s-zone0%d",name,j);
          sprintf(temp,"%s:%s",tempSecName,MCFW_DEMO_SCD_VA_INI_FILE_ZONE_DIR);
          cfg->zoneCfg[j].zoneDirection = iniparser_getint(ini,temp,0);

          sprintf(tempSecName,"%s-zone0%d",name,j);
          sprintf(temp,"%s:%s",tempSecName,MCFW_DEMO_SCD_VA_INI_FILE_ZONE_NUM_OF_POINT);
          cfg->zoneCfg[j].noOfPoints = iniparser_getint(ini,temp,0);

          for (k = 0; k < cfg->zoneCfg[j].noOfPoints; k++)
          {
           sprintf(tempSecName,"%s-zone0%d",name,j);
           sprintf(temp,"%s:%s%d",tempSecName,MCFW_DEMO_SCD_VA_INI_FILE_ZONE_XY,k);
           strcpy(xyValue,iniparser_getstring(ini,temp,0));
           Demo_scdGetXYValues(xyValue, &cfg->zoneCfg[j].pixLocation[k].x, &cfg->zoneCfg[j].pixLocation[k].y); 
          }
       }
    }

    return 0;
}
static Int32 Demo_scdInputIniFilePath()
{
    Bool done=FALSE;
    Demo_ScdInfo * scd_configInfo;

    scd_configInfo = &gDemo_ScdInfo;

    printf(" \n");
    printf(" DMVAL Configuration Sample ini files available in ./demo_ini folder. \n");
    printf(" Enter the DMVAL Config .ini filename with full path : ");

    fflush(stdin);
    do
    {
        fflush(stdin);
        fgets(scd_configInfo->iniPath, MAX_INPUT_STR_SIZE, stdin);

        printf(" \n");

        /* remove \n from the path name */
        scd_configInfo->iniPath[ strlen(scd_configInfo->iniPath)-1 ] = 0;

        if(Demo_isFileValid(scd_configInfo->iniPath))
        {
           printf(" Path is valid \n");
           printf(" [%s] ini file to be accessed !!!\n", scd_configInfo->iniPath);
           done = TRUE;
        }
        else
        {
           printf(" Path is invalid, Using default Path \n" );
           strcpy(scd_configInfo->iniPath,"./demo_ini/dmvalConfig.ini");
           if(Demo_isFileValid(scd_configInfo->iniPath))
           {
              printf(" [%s] ini file to be accessed!!!\n", scd_configInfo->iniPath);
           }
           else
           {
               printf(" [%s] ini file could not be accessed, Not updateing VA Params!!!\n", scd_configInfo->iniPath);
               return OSA_EFAIL;
           }
           done = TRUE;
        }
    }while(!done);

    return OSA_SOK;
}
static Int32 Demo_scdRdParseIniFile(void)
{
    dictionary * ini = NULL ;
    Demo_ScdInfo * scd_configInfo;

    scd_configInfo = &gDemo_ScdInfo;

    if(Demo_isFileValid(scd_configInfo->iniPath))
       ini = iniparser_load(scd_configInfo->iniPath);
    else
       return OSA_EFAIL;

    if(ini==NULL)
    {
        printf(" \n");
        printf(" ERROR: [%s] ini file could not be accessed !!!\n", scd_configInfo->iniPath);
    }
    else
    {
        Demo_scdGetCfgArray(ini);
        iniparser_freedict(ini);
    }
    return OSA_SOK;
}

Int32 Demo_scdInit(UInt32 numCh, UInt32 startChId, UInt32 scdResolution, UInt32 videoMode, UInt32 demoId)
{

    UInt32 maxHorBlks, maxVerBlks;
    Demo_ScdInfo * scd_configInfo;
    VCAP_CHN_DYNAMIC_PARAM_S params; /* Placeholder, Just initialising, not setting params at SCD link */
    UInt32 i, j, k, chId, idx;

    scd_configInfo = &gDemo_ScdInfo;


    if(scdResolution == DEMO_SCD_RESOLUTION_CLASS_QCIF)
    {
         maxHorBlks = 6;  /* QCIF Resolution rounded to 32 pixels */
         maxVerBlks = 12; /* QCIF Resolution*/
    }
    else
    {
         maxHorBlks = 11;  /* CIF Resolution rounded to 32 pixels */
         maxVerBlks = 24;  /* CIF Resolution*/
    }
    if(gDemo_info.scdTileConfigInitFlag == FALSE)
    {
        for(chId = 0; chId < SCD_MAX_CHANNELS; chId++)
           for(idx = 0; idx < SCD_MAX_TILES; idx++)
              scdTileConfig[chId][idx] = 0;  /*disabling all the tiles */

        gDemo_info.scdTileConfigInitFlag = TRUE;
    }

    memset(&params, 0, sizeof(params));
    scd_configInfo->numCh = numCh;
    for(i = 0; i < numCh; i++)
    {
        scd_configInfo->chInfo[i].chId = i + startChId;
        scd_configInfo->chInfo[i].videoMode = videoMode;
        if(scdResolution == DEMO_SCD_RESOLUTION_CLASS_QCIF)
        {
            scd_configInfo->chInfo[i].maxWidth = 176;
            if(videoMode)
               scd_configInfo->chInfo[i].maxHeight = 144;
            else 
               scd_configInfo->chInfo[i].maxHeight = 120;

            scd_configInfo->chInfo[i].numHorzBlks = maxHorBlks;
            scd_configInfo->chInfo[i].numVertBlks = maxVerBlks;
            scd_configInfo->chInfo[i].numBlksInFrame = maxVerBlks * maxHorBlks;
        }
        else
        {
            scd_configInfo->chInfo[i].maxWidth = 352;
            if(videoMode)
               scd_configInfo->chInfo[i].maxHeight = 288;
            else 
               scd_configInfo->chInfo[i].maxHeight = 240;

            scd_configInfo->chInfo[i].numHorzBlks = maxHorBlks;
            scd_configInfo->chInfo[i].numVertBlks = maxVerBlks;
            scd_configInfo->chInfo[i].numBlksInFrame = maxVerBlks * maxHorBlks;
        }
        if(videoMode)
            scd_configInfo->chInfo[i].blkHeight = 12;
        else
            scd_configInfo->chInfo[i].blkHeight = 10;

        params.scdChBlkPrm.numValidBlock = 0;
        for(j = 0; j < maxVerBlks; j++)
        {
            for(k = 0; k < maxHorBlks; k++)
            {
                AlgLink_ScdChBlkConfig * blkConfig;
                blkConfig = &params.scdChBlkPrm.blkConfig[params.scdChBlkPrm.numValidBlock];

                blkConfig->blockId      =  k + (j * maxHorBlks)   ;
                blkConfig->sensitivity  = ALG_LINK_SCD_SENSITIVITY_MID;
                blkConfig->monitorBlock = FALSE;
                params.scdChBlkPrm.numValidBlock++;
            }
        }
    }
#if defined(TI_816X_BUILD)
    if(demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE)
    {
        UInt32 status;
        status = OSA_SOK;

        if(scd_configInfo->iniPath == NULL)
        {
            status = Demo_scdInputIniFilePath();
        }
        if(status == OSA_SOK)
        {
            status = Demo_scdRdParseIniFile();
            if(status == OSA_SOK)
            {
              for(chId = 0; chId < gDemo_ScdInfo.numVaCh; chId++)
              {
                  memcpy(&params.scdChVaPrm, &scd_configInfo->chInfo[chId].scdChPrm.scdVaAlgCfg, sizeof(AlgLink_ScdChVaParams));
                  Vcap_setDynamicParamChn(scd_configInfo->chInfo[chId].scdChPrm.scdVaAlgCfg.chId, &params, VCAP_SCDVACONFIG);
                  Vcap_setFrameRate(scd_configInfo->chInfo[chId].scdChPrm.scdVaAlgCfg.chId, 4, 30, 10);
              }
            }
        }
    }
#endif
    return 0;
}

Int32 Demo_scdUpdateParam(UInt32 chId, UInt32 width, UInt32 height)
{
    UInt32 maxHorBlks, maxVerBlks;
    Demo_ScdInfo * scd_configInfo;

    UInt32 i, chIdx;

    scd_configInfo = &gDemo_ScdInfo;

    printf(" SCD: Channel config changed. ChId is %d, Resolution %d x %d \n", chId, width, height);

    maxHorBlks = SystemUtils_align(width, ALG_LINK_SCD_BLK_WIDTH)/ALG_LINK_SCD_BLK_WIDTH;
    if(height%10)
       maxVerBlks = height/ALG_LINK_SCD_BLK_HEIGHT; 
    else
       maxVerBlks = height/ALG_LINK_SCD_BLK_HEIGHT_MIN;

    chIdx = 0;
    for(i = 0; i < scd_configInfo->numCh; i++)
    {
        if(scd_configInfo->chInfo[i].chId == chId)
        {
           chIdx = i;
           break;
        }
        if(i == scd_configInfo->numCh)
           return 0;
    }

    scd_configInfo->chInfo[chIdx].maxWidth = width;
    scd_configInfo->chInfo[chIdx].maxHeight = height;

    scd_configInfo->chInfo[chIdx].numHorzBlks = maxHorBlks;
    scd_configInfo->chInfo[chIdx].numVertBlks = maxVerBlks;
    scd_configInfo->chInfo[chIdx].numBlksInFrame = maxVerBlks * maxHorBlks;
    for(i = 0; i < SCD_MAX_TILES; i++)
        scdTileConfig[chIdx][i] = 0;  /*disabling all the tiles */

    return 0;
}

Int32 Demo_scdUpdateBlkConfigParam(UInt32 chId, UInt32 demoId)
{
    UInt32 tileId,startX, startY, endX, endY;
    UInt32 maxHorBlks, maxVerBlks, maxHorBlkPerTile, maxVerBlkPerTile;
    Int32 i, j, sensitivity, flag = 0, chIdx;
    Demo_ScdInfo * scd_configInfo;
    VCAP_CHN_DYNAMIC_PARAM_S params;

    scd_configInfo = &gDemo_ScdInfo;

     chIdx = 0;
    for(i = 0; i < scd_configInfo->numCh; i++)
    {
        if(scd_configInfo->chInfo[i].chId == chId)
        {
           chIdx = i;
           break;
        }
        if(i == scd_configInfo->numCh)
           return 0;
    }

    maxHorBlks = scd_configInfo->chInfo[chIdx].numHorzBlks;  /* QCIF Resolution rounded to 32 pixels */
    maxVerBlks = scd_configInfo->chInfo[chIdx].numVertBlks; /* QCIF Resolution*/

    maxHorBlkPerTile = SystemUtils_align(maxHorBlks, 3)/3;  /* Aligning for 3x3 Tile Grid */
    maxVerBlkPerTile = SystemUtils_align(maxVerBlks, 3)/3;  /* Aligning for 3x3 Tile Grid */

    chId = chIdx;
    printf("\n Enabled Tile Nos:\t");
    for(i = 0; i < SCD_MAX_TILES; i++)
    {
       if(scdTileConfig[chId][i] == 1)
       {
          flag = 1;
          printf("%d, ",i);
       }
    }
    if(flag == 0)
       printf("Currently zero tiles are enabled for LMD for Chan-%d\n",chId);

    printf("\n");
    tileId = Demo_getIntValue(" Tile Id/No.", 0, 8, 0);

    startX = (tileId%3)*maxHorBlkPerTile;
    startY = (tileId/3)*maxVerBlkPerTile;

    endX = startX + maxHorBlkPerTile;
    endY = startY + maxVerBlkPerTile;

    if(endX > maxHorBlks)
       endX = maxHorBlks;
    if(endY > maxVerBlks)
       endY = maxVerBlks;

    params.scdChBlkPrm.chId = chId;
    params.scdChBlkPrm.numValidBlock = 0;

    flag        = Demo_getIntValue("LMD block disable/enable flag", 0, 1, 1);

    if(flag == 1)
    {
       sensitivity = Demo_getIntValue("LMD block sensitivity", 0, 8, 8);
       scdTileConfig[chId][tileId] = 1;
    }
    else
    {
       scdTileConfig[chId][tileId] = 0;
       sensitivity = 0;
    }
    printf(" maxHorBlkPerTile %d maxHorBlks %d maxVerBlkPerTile %d maxVerBlks %d \n",
          maxHorBlkPerTile, maxHorBlks, maxVerBlkPerTile, maxVerBlks);
    printf("StartX %d EndX %d StartY %d EndY %d \n", startX, endX, startY, endY);
    for(i = startY; i < endY; i++)
    {
        for(j = startX; j < endX; j++)
        {
            AlgLink_ScdChBlkConfig * blkConfig;
            blkConfig = &params.scdChBlkPrm.blkConfig[params.scdChBlkPrm.numValidBlock];

            blkConfig->blockId      =  j + (i * maxHorBlks)   ;
            blkConfig->sensitivity  = sensitivity;
            blkConfig->monitorBlock = flag;
            params.scdChBlkPrm.numValidBlock++;
        }
    }
    printf("\nNumber of blocks in the tile to be updated %d\n\n",params.scdChBlkPrm.numValidBlock);

    Vcap_setDynamicParamChn(chId, &params, VCAP_SCDBLOCKCONFIG);

    return 0;
}

Void Demo_scdVaParamInit(UInt32 demoId)
{
    memset(&gDemo_ScdInfo, 0, sizeof(Demo_ScdInfo));
    if(demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE)
       Demo_scdInputIniFilePath();
}

Void Demo_scdUpdateVaParam()
{
    Demo_ScdInfo * scd_configInfo;
    VCAP_CHN_DYNAMIC_PARAM_S params;
    UInt32 chId, status;
    scd_configInfo = &gDemo_ScdInfo;

    status = OSA_SOK;
    memset(&params, 0, sizeof(params));

    status = Demo_scdRdParseIniFile();

    for(chId = 0; chId < scd_configInfo->numVaCh; chId++)
    {
        memcpy(&params.scdChVaPrm, &scd_configInfo->chInfo[chId].scdChPrm.scdVaAlgCfg, sizeof(AlgLink_ScdChVaParams));
        Vcap_setDynamicParamChn(scd_configInfo->chInfo[chId].scdChPrm.scdVaAlgCfg.chId, &params, VCAP_SCDVACONFIG);
    }
 
   return;
}
