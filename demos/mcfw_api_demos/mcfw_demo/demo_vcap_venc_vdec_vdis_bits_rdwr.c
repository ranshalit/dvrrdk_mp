/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2011 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

/**
    \file demo_vcap_venc_vdec_vdis_bits_rdwr.c
    \brief
*/

#include <demo_vcap_venc_vdec_vdis.h>
#include <commit_orderedarray.h>
#define ENABLE_DMA_COPY
#include <time.h>
#include <sys/stat.h>
#include <commit.h>
int testlist(char* path);


#define MAX_RETRY_CNT (1000000)


UInt32 g_splitIdx = 0;
UInt32 waitforIframe = 0;
UInt32 g_numOfSplits = 1;


/* ranran */
 Bool g_isCyclic;
 UInt64 g_filesize;
 char g_videoFilePath[256];
extern UInt32 g_alertLimit;
 UInt32 g_partitionsize;

Bool g_IsDiskLimit = FALSE;
FILE* g_currentFile;


VcapVencVdecVdis_IpcBitsCtrl gVcapVencVdecVdis_ipcBitsCtrl =
{
    .fObj.fpWrHdr  = {NULL},
    .fObj.fpWrData = {{NULL}},
    .fObj.fpWrMvData = {NULL},
    .fObj.maxFileSize    = MCFW_IPC_BITS_MAX_FILE_SIZE,
    .fObj.enableFWrite   = MCFW_IPC_BITS_ENABLE_FILE_WRITE,
    .fObj.enableLayerWrite = MCFW_IPC_BITS_ENABLE_FILE_LAYERS_WRITE,
    .fObj.fwriteEnableBitMask = MCFW_IPC_BITS_FWRITE_ENABLE_BITMASK_DEFAULT,
    .fObj.fileExtension = MCFW_IPC_BITS_DEFAULT_FILE_EXTENSION,
    .noNotifyBitsInHLOS  = MCFW_IPC_BITS_NONOTIFYMODE_BITSIN,
    .noNotifyBitsOutHLOS = MCFW_IPC_BITS_NONOTIFYMODE_BITSOUT,
    #ifdef ENABLE_DMA_COPY
    .dmaObj.useDma       = MCFW_IPC_BITS_USE_DMA_FOR_BITBUF_COPY,
    #else
    .dmaObj.useDma       = FALSE,
    #endif
};


static
Void VcapVencVdecVdis_ipcBitsGenerateFileName(char *dirPath,
                                 char *fname,
                                 UInt32 chNum,
                                 UInt32 chLayerNum,
                                 char *fsuffix,
                                 char *dstBuf,
                                 UInt32 maxLen);

static
unsigned long long VcapVencVdecVdis_ipcBitsGetPartitionFreeSpace(char *filePath);

Void VcapVencVdecVdis_ipcBitsInCbFxn (Ptr cbCtx)
{
    VcapVencVdecVdis_IpcBitsCtrl *app_ipcBitsCtrl;
    static Int printInterval;

    OSA_assert(cbCtx = &gVcapVencVdecVdis_ipcBitsCtrl);
    app_ipcBitsCtrl = cbCtx;
    OSA_semSignal(&app_ipcBitsCtrl->thrObj.bitsInNotifySem);
    #ifdef IPC_BITS_DEBUG
    if ((printInterval % MCFW_IPCBITS_INFO_PRINT_INTERVAL) == 0)
    {
        OSA_printf("MCFW_IPCBITS: Callback function:%s",__func__);
    }
    #endif
    printInterval++;

}

static Bool VcapVencVdecVdis_ipcBitsWriteWrap(FILE  * fp,
                                 UInt32 bytesToWrite,
                                 UInt32 maxFileSize)
{
    static Int printStatsIterval = 0;
    Bool wrapOccured = FALSE;

   /* ranran  printf("fp 0x%x len %ld\n", (UInt32) fp, ((ftell(fp)) + bytesToWrite)); */
    if (maxFileSize != MCFW_IPC_BITS_MAX_FILE_SIZE_INFINITY)
    {
        if (((ftell(fp)) + bytesToWrite) > maxFileSize)
        {
            if ((printStatsIterval % MCFW_IPCBITS_INFO_PRINT_INTERVAL) == 0)
                OSA_printf("MCFW_IPCBITS:File wrap @ [%ld],MaxFileSize [%d]",
                           ftell(fp),maxFileSize);
            rewind(fp);
            /*printStatsIterval++;*/
            wrapOccured = TRUE;
        }
    }
    return wrapOccured;
}


/*
* When numTemporalLayer is set to VENC_TEMPORAL_LAYERS_1 & enableSVCExtensionFlag is
* set to enabled, then the stream comes as follows IPPPPP.....PPPPPPI
* and the file dump with complete stream will be VBITS_DATA_0_LAYER_0.h264
* ****************************************************************************
*  When numTemporalLayer is set to VENC_TEMPORAL_LAYERS_2 & enableSVCExtensionFlag is
*  set to enabled, then the stream comes as follows
*
*     P1    P1    P1    P1    P1
*   I    P0    P0    P0    P0     ......
*
*  and the file dump with complete stream will be VBITS_DATA_0_LAYER_1.h264 while the
*  file VBITS_DATA_0_LAYER_0.h264 will be the subset stream of lesser frame rate.
*  There for, for any numTemporalLayer set, the complete stream file will be
*  (numTemporalLayer-1) index file
*****************************************************************************
*
*  When numTemporalLayer is set to VENC_TEMPORAL_LAYERS_4 & enableSVCExtensionFlag is
*  set to enabled, then the stream comes as follows
*
* P3  P3    P3  P3    P3  P3       P3 P3     ....
*    P2         P2          P2           P2        ....
*           P1                       P1               P1    P1   .....
*I                       P0                               P0         ....
*
*
*  and the file dump with complete stream will be
        VBITS_DATA_0_LAYER_3.h264 -> IP3P2P3P1P3P2P3P0P3P2P3P1....
*  while the  files
*      VBITS_DATA_0_LAYER_2.h264  -> IP2P1P2P0P2P1P2P0....
*      VBITS_DATA_0_LAYER_1.h264  -> IP1P0P1P0P1P0...
*      VBITS_DATA_0_LAYER_0.h264  -> IP0P0P0P0IP0....
*        will be the subset streams with decreasing order of frame rate.
*
*/

/*ranran*/
int VcapVencVdecVdis_ipcBitsCommitGetUsedDisk()
{
	return g_splitIdx*100/g_numOfSplits;

}

static Void VcapVencVdecVdis_ipcBitsCommitWriteBitsToFile (FILE  * fpHdr[MCFW_IPC_BITS_MAX_NUM_CHANNELS],
                                           FILE  * fpBuf[MCFW_IPC_BITS_MAX_NUM_CHANNELS][MCFW_IPC_BITS_MAX_NUM_CHANNELS_LAYERS],
                                           FILE  *fpMvBuf[MCFW_IPC_BITS_MAX_NUM_CHANNELS],
                                           VCODEC_BITSBUF_LIST_S *bufList,
                                           UInt32 maxFileSize,
                                           UInt32 fwriteEnableChannelBitmask,
                                           Bool enableLayerWrite,
                                           Bool wrapOccuredHdr[],
                                           Bool wrapOccuredBuf[MCFW_IPC_BITS_MAX_NUM_CHANNELS][MCFW_IPC_BITS_MAX_NUM_CHANNELS_LAYERS])
{
    Int i;
    VCODEC_BITSBUF_S *pBuf;
    size_t write_cnt;
    char path[256];
    unsigned long long length = 0, paritionFreeSize = 0, maxDeleteRetries = 0;
    Bool wrapOccured = FALSE;
    char fileNameBuffer[128];
    Int status = OSA_SOK;
    int rc = 0;

    //printf("ranran VcapVencVdecVdis_ipcBitsCommitWriteBitsToFile %s\n",gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath);
    for (i = 0; i < bufList->numBufs;i++)
    {
        UInt32 fileIdx;

        pBuf = &bufList->bitsBuf[i];
        OSA_assert(pBuf->chnId < MCFW_IPC_BITS_MAX_NUM_CHANNELS);
        fileIdx = pBuf->chnId;



      /*  OSA_assert(pBuf->frameType == VCODEC_FRAME_TYPE_I_FRAME);*/

        if ((fwriteEnableChannelBitmask & (1 << fileIdx)) || (fileIdx > 31 && (fwriteEnableChannelBitmask & (1 << (fileIdx-32)))))
        {
        	if (waitforIframe == 1)
        	{
        		/* if we on wait for I frame, and this is not I-frame, then ignore and continue waiting */
        		if (pBuf->frameType != VCODEC_FRAME_TYPE_I_FRAME)
        			continue;
        		/* this is I-frame, stop ignoring */
        		waitforIframe = 0;
        	}
        	if (g_IsDiskLimit == TRUE)
        	{
        		/* if we reached disk limit (non-cyclic case only), then ignore message */
        		return;
        	}

		    wrapOccured =	VcapVencVdecVdis_ipcBitsWriteWrap(g_currentFile,  pBuf->filledBufSize,	  gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize);

		    if (FALSE == wrapOccured)
		    {
		    	/* if we can write without reading size limit , then write */
		    	write_cnt = fwrite(pBuf->bufVirtAddr,sizeof(char),  pBuf->filledBufSize,  g_currentFile);
		    	OSA_assert(write_cnt == pBuf->filledBufSize);
		    }
		    if (TRUE == wrapOccured)
		    {
		    	/* we reached maximum file size. the next condition test seems unnecessary. should be removed */
		    	if (waitforIframe == 0)
		    	{
		    		rc = fclose(g_currentFile);
		    		if (rc != 0)
		    		{
		    			printf("fclose failed %d\n",rc);
		    		}
		    		/* first glitch */
					waitforIframe = 1;
					/* get next file index */
				    strcpy(fileNameBuffer, gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath);
				    strcat(fileNameBuffer, "/.");
					paritionFreeSize = VcapVencVdecVdis_ipcBitsGetPartitionFreeSpace(fileNameBuffer);

					if ((gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize/1024) > paritionFreeSize)
					{
						/* free space is not enough for reaching maximum size limitation (for the new file) */
						if (g_isCyclic == FALSE)
						{
							/* if non cyclic - then we can't go on writing */
							/* set error TBD */
							printf("g_IsDiskLimit is TRUE !!\n");
							g_IsDiskLimit = TRUE;
							return;
						}

						else
						{
							/* non cyclic mode, therefore, try to delete file one after the other till we can write and reach maximum file size */
							maxDeleteRetries = 0;
							while(1)
							{
								/* delete file and remove from array */
								remove_file_from_array(path, &length);
								printf("remove file %s length %lld\n",path,length);
								status = remove(path);
								if (status == -1)
								{
									printf("failed in removing file\n");
								}
								/* get next file index */
								paritionFreeSize = VcapVencVdecVdis_ipcBitsGetPartitionFreeSpace(fileNameBuffer);
								maxDeleteRetries++;
								if (maxDeleteRetries > MAX_RETRY_CNT)
								{
									printf("trying to delete more than %lld times\n",maxDeleteRetries);
									/* set error TBD */
									exit(5);
								}
								if ((gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize/1024) < paritionFreeSize)
								{
									break;
								}
							}
						}

					}

					if ((gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize/1024) > paritionFreeSize)
					{
						if (g_isCyclic == TRUE)
						{
							printf("#2 paritionFreeSize %lld \n",paritionFreeSize);
							printf("not expected after removing file\n");
							/* set error TBD */
							exit(2);
						}
					}



					time_t curr_time = time(NULL);
					VcapVencVdecVdis_ipcBitsGenerateFileName(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath,
											COMMIT_MCFW_IPC_BITS_DATA_FILE_NAME,
										   0,
										   curr_time,
										   gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileExtension,
										   fileNameBuffer,
										   sizeof(fileNameBuffer));
					g_currentFile = fopen(fileNameBuffer,"a+b");
					if (g_currentFile == NULL)
					{
						printf("not expected fopen returned null");
						exit(3);
					}
					printf("ranran #1 %s\n",fileNameBuffer);
					status =  setvbuf(g_currentFile,
									  NULL,
									  _IOFBF,
									  MCFW_IPC_BITS_FILEBUF_SIZE_DATA);
					if (status != 0)
					{
						printf("setvbuf failed %d\n",status);
						exit(4);
					}
					add_file_to_array(fileNameBuffer);
				    /* TBD need to remove the next lines and test! */
	        		if (pBuf->frameType != VCODEC_FRAME_TYPE_I_FRAME)
	        			continue;
	        		waitforIframe = 0;

		    	}

		    }
        }
    }
}

static Void VcapVencVdecVdis_ipcBitsWriteBitsToFile (FILE  * fpHdr[MCFW_IPC_BITS_MAX_NUM_CHANNELS],
                                           FILE  * fpBuf[MCFW_IPC_BITS_MAX_NUM_CHANNELS][MCFW_IPC_BITS_MAX_NUM_CHANNELS_LAYERS],
                                           FILE  *fpMvBuf[MCFW_IPC_BITS_MAX_NUM_CHANNELS],
                                           VCODEC_BITSBUF_LIST_S *bufList,
                                           UInt32 maxFileSize,
                                           UInt32 fwriteEnableChannelBitmask,
                                           Bool enableLayerWrite,
                                           Bool wrapOccuredHdr[],
                                           Bool wrapOccuredBuf[MCFW_IPC_BITS_MAX_NUM_CHANNELS][MCFW_IPC_BITS_MAX_NUM_CHANNELS_LAYERS])
{
    Int i, j;
    VCODEC_BITSBUF_S *pBuf;
    size_t write_cnt;
    static UInt32 maxMvFrameCnt;

    for (i = 0; i < bufList->numBufs;i++)
    {
        UInt32 fileIdx;

        pBuf = &bufList->bitsBuf[i];
        OSA_assert(pBuf->chnId < MCFW_IPC_BITS_MAX_NUM_CHANNELS);
        fileIdx = pBuf->chnId;
        if ((fwriteEnableChannelBitmask & (1 << fileIdx)) || (fileIdx > 31 && (fwriteEnableChannelBitmask & (1 << (fileIdx-32)))))
        {
            /* This file write will be the default case valid for all codecs*/
            if(enableLayerWrite == FALSE ||
              ((enableLayerWrite == TRUE) && (pBuf->codecType != VCODEC_TYPE_H264)))
            {
                if (FALSE == wrapOccuredBuf[fileIdx][MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX])
                {
                     wrapOccuredBuf[fileIdx][MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX] =
                        VcapVencVdecVdis_ipcBitsWriteWrap(fpBuf[fileIdx][MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX],
                                                          pBuf->filledBufSize,
                                                          maxFileSize);
                }

                if (FALSE == wrapOccuredBuf[fileIdx][MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX])
                {
                    write_cnt = fwrite(pBuf->bufVirtAddr,sizeof(char),
                                       pBuf->filledBufSize,
                                       fpBuf[fileIdx][MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX]);
                    OSA_assert(write_cnt == pBuf->filledBufSize);

                    /*The below check for max frames as 500, means write mv data only for first n frame*/
                    if(pBuf->mvDataFilledSize != 0 && maxMvFrameCnt <= 500)
                    {
                        maxMvFrameCnt++;

                        write_cnt = fwrite(pBuf->bufVirtAddr + pBuf->mvDataOffset,
                                  sizeof(char), pBuf->mvDataFilledSize,fpMvBuf[fileIdx]);

                        OSA_assert(write_cnt == pBuf->mvDataFilledSize);

                    }
                }
            }
            else if(enableLayerWrite == TRUE)
            {
                /*I-Frames should be present in all the h264 temporal layers*/
                if( pBuf->temporalId == 0 &&
                    pBuf->frameType == VCODEC_FRAME_TYPE_I_FRAME )
                {
                    for(j = 0; j < pBuf->numTemporalLayerSetInCodec ; j++)
                    {
                        if (FALSE == wrapOccuredBuf[fileIdx][j])
                        {
                            wrapOccuredBuf[fileIdx][j] =
                            VcapVencVdecVdis_ipcBitsWriteWrap(fpBuf[fileIdx][j],pBuf->filledBufSize, maxFileSize);
                        }

                    if (FALSE == wrapOccuredBuf[fileIdx][j])
                        {
                            write_cnt = fwrite(pBuf->bufVirtAddr,sizeof(char),pBuf->filledBufSize,fpBuf[fileIdx][j]);
                            OSA_assert(write_cnt == pBuf->filledBufSize);
                        }
                    }
                }

                /*P-Frames should be present in specific layers based on their temporal Id's
                               ***/
                if( pBuf->temporalId == 0 &&
                     pBuf->frameType == VCODEC_FRAME_TYPE_P_FRAME )
                {
                    for(j = 0; j < pBuf->numTemporalLayerSetInCodec ; j++)
                    {

                        if (FALSE == wrapOccuredBuf[fileIdx][j])
                        {
                            wrapOccuredBuf[fileIdx][j] =
                                VcapVencVdecVdis_ipcBitsWriteWrap(fpBuf[fileIdx][j],pBuf->filledBufSize, maxFileSize);
                        }

                        if (FALSE == wrapOccuredBuf[fileIdx][j])
                        {
                            write_cnt = fwrite(pBuf->bufVirtAddr,sizeof(char),pBuf->filledBufSize,fpBuf[fileIdx][j]);
                            OSA_assert(write_cnt == pBuf->filledBufSize);
                        }
                    }
                }

                if( pBuf->temporalId == 1 &&
                     pBuf->frameType == VCODEC_FRAME_TYPE_P_FRAME )
                {
                    for(j = 1; j < pBuf->numTemporalLayerSetInCodec ; j++)
                    {

                        if (FALSE == wrapOccuredBuf[fileIdx][j])
                        {
                            wrapOccuredBuf[fileIdx][j] =
                                VcapVencVdecVdis_ipcBitsWriteWrap(fpBuf[fileIdx][j],pBuf->filledBufSize, maxFileSize);
                        }

                        if (FALSE == wrapOccuredBuf[fileIdx][j])
                        {
                            write_cnt = fwrite(pBuf->bufVirtAddr,sizeof(char),pBuf->filledBufSize,fpBuf[fileIdx][j]);
                            OSA_assert(write_cnt == pBuf->filledBufSize);
                        }
                    }
                }

                if( pBuf->temporalId == 2 &&
                     pBuf->frameType == VCODEC_FRAME_TYPE_P_FRAME )
                {
                    for(j = 2; j < pBuf->numTemporalLayerSetInCodec ; j++)
                    {

                        if (FALSE == wrapOccuredBuf[fileIdx][j])
                        {
                            wrapOccuredBuf[fileIdx][j] =
                                VcapVencVdecVdis_ipcBitsWriteWrap(fpBuf[fileIdx][j],pBuf->filledBufSize, maxFileSize);
                        }

                        if (FALSE == wrapOccuredBuf[fileIdx][j])
                        {
                            write_cnt = fwrite(pBuf->bufVirtAddr,sizeof(char),pBuf->filledBufSize,fpBuf[fileIdx][j]);
                            OSA_assert(write_cnt == pBuf->filledBufSize);
                        }
                    }
                }
                if( pBuf->temporalId == 3 &&
                     pBuf->frameType == VCODEC_FRAME_TYPE_P_FRAME )
                {
                    for(j = 3; j < pBuf->numTemporalLayerSetInCodec ; j++)
                    {

                        if (FALSE == wrapOccuredBuf[fileIdx][j])
                        {
                            wrapOccuredBuf[fileIdx][j] =
                                VcapVencVdecVdis_ipcBitsWriteWrap(fpBuf[fileIdx][j],pBuf->filledBufSize, maxFileSize);
                        }

                        if (FALSE == wrapOccuredBuf[fileIdx][j])
                        {
                            write_cnt = fwrite(pBuf->bufVirtAddr,sizeof(char),pBuf->filledBufSize,fpBuf[fileIdx][j]);
                            OSA_assert(write_cnt == pBuf->filledBufSize);
                        }
                    }
                }

                /*The below check for max frames as 500, means write mv data only for first n frame*/
                if(pBuf->mvDataFilledSize != 0 && maxMvFrameCnt <= 500)
                {
                    maxMvFrameCnt++;

                    write_cnt = fwrite(pBuf->bufVirtAddr + pBuf->mvDataOffset,
                              sizeof(char), pBuf->mvDataFilledSize,fpMvBuf[fileIdx]);

                    OSA_assert(write_cnt == pBuf->mvDataFilledSize);

                }
            }

            if (FALSE == wrapOccuredHdr[fileIdx])
            {
                wrapOccuredHdr[fileIdx] =
                VcapVencVdecVdis_ipcBitsWriteWrap(fpHdr[fileIdx],sizeof(*pBuf),maxFileSize);
            }
            if (FALSE == wrapOccuredHdr[fileIdx])
            {
                write_cnt = fwrite(pBuf,sizeof(*pBuf),1,fpHdr[fileIdx]);
                OSA_assert(write_cnt == 1);
            }

        }
    }
}


static Void VcapVencVdecVdis_ipcBitsCopyBitBufInfo (VCODEC_BITSBUF_S *dst,
                                          const VCODEC_BITSBUF_S *src)
{
    dst->chnId = src->chnId;
    dst->codecType = src->codecType;
    dst->filledBufSize = src->filledBufSize;
    dst->bottomFieldBitBufSize = src->bottomFieldBitBufSize;
    dst->inputFileChanged = src->inputFileChanged;
    dst->frameType = src->frameType;
    dst->timestamp  = src->timestamp;
    dst->upperTimeStamp= src->upperTimeStamp;
    dst->lowerTimeStamp= src->lowerTimeStamp;
    dst->seqId         = Demo_displayGetCurSeqId();
}


static Void VcapVencVdecVdis_ipcBitsCopyBitBufDataMem2Mem(VCODEC_BITSBUF_S *dstBuf,
                                                VCODEC_BITSBUF_S *srcBuf)
{
    OSA_assert(srcBuf->filledBufSize < dstBuf->bufSize);

    if (FALSE == gVcapVencVdecVdis_ipcBitsCtrl.dmaObj.useDma)
    {
        memcpy(dstBuf->bufVirtAddr,srcBuf->bufVirtAddr,srcBuf->filledBufSize);
    }
    else
    {
        OSA_DmaCopy1D prm;

        prm.srcPhysAddr = (UInt32)srcBuf->bufPhysAddr;
        prm.dstPhysAddr = (UInt32)dstBuf->bufPhysAddr;
        prm.size        = srcBuf->filledBufSize;

        OSA_dmaCopy1D(&gVcapVencVdecVdis_ipcBitsCtrl.dmaObj.dmaChHdnl,
                      &prm,
                      1);

    }
}


static UInt32 VcapVencVdecVdis_ipcBitsGetBitBufSize (VCODEC_BITSBUF_S    *bitsBuf)
{
    UInt32 maxSize = 0;

    if (bitsBuf)
    {
        maxSize = bitsBuf->frameWidth * bitsBuf->frameHeight;

        if (gVcapVencVdecVdis_ipcBitsCtrl.thrObj.chInfo[bitsBuf->chnId].chBufSize < maxSize)
        {
            maxSize = gVcapVencVdecVdis_ipcBitsCtrl.thrObj.chInfo[bitsBuf->chnId].chBufSize;
            printf ("## CH:%d -> Actual Frame Size: %d, IPCBits Init Size: %d\n",
                    bitsBuf->chnId,
                    bitsBuf->frameWidth * bitsBuf->frameHeight,
                    gVcapVencVdecVdis_ipcBitsCtrl.thrObj.chInfo[bitsBuf->chnId].chBufSize
                        );
        }
    }
    return maxSize;
}


static Void VcapVencVdecVdis_ipcBitsGetEmptyBitBufs(VCODEC_BITSBUF_LIST_S *emptyBufList,
                        Int32 chId,
                        UInt32 chBufSize)
{
    VDEC_BUF_REQUEST_S reqInfo;

    emptyBufList->numBufs = 0;
    reqInfo.chNum = chId;
    reqInfo.bufSize = chBufSize;
    Vdec_requestBitstreamBuffer(&reqInfo, emptyBufList, 0);
}


static Void VcapVencVdecVdis_ipcBitsSendFullBitBufs(VCODEC_BITSBUF_S    *bitsBuf)
{
    VCODEC_BITSBUF_LIST_S fullBufList;

    fullBufList.numBufs = 1;
    fullBufList.bitsBuf[0] = *bitsBuf;
    if (fullBufList.numBufs)
    {
        gVcapVencVdecVdis_ipcBitsCtrl.totalDecFrames+=fullBufList.numBufs;
        Vdec_putBitstreamBuffer(&fullBufList);
    }
}

//#define DEMO_VCAP_VENC_VDEC_VDIS_REPLACE_DECODER_TIMESTAMPS (TRUE)



#define VDEC_VDIS_FRAME_DURATION_MS (33)

static Void VcapVencVdecVdis_setFrameTimeStamp(VcapVencVdecVdis_ChInfo *decChInfo,
                                               VCODEC_BITSBUF_S *pEmptyBuf)
{
#ifdef DEMO_VCAP_VENC_VDEC_VDIS_REPLACE_DECODER_TIMESTAMPS
    UInt64 curTimeStamp =
      decChInfo->numFrames * VDEC_VDIS_FRAME_DURATION_MS;

    pEmptyBuf->lowerTimeStamp = (UInt32)(curTimeStamp & 0xFFFFFFFF);
    pEmptyBuf->upperTimeStamp = (UInt32)((curTimeStamp >> 32)& 0xFFFFFFFF);
#else
    UInt64 curTimeStamp =  pEmptyBuf->upperTimeStamp;
    curTimeStamp = curTimeStamp << 32;
    curTimeStamp |= pEmptyBuf->lowerTimeStamp;
#endif /* DEMO_VCAP_VENC_VDEC_VDIS_REPLACE_DECODER_TIMESTAMPS */
    if (0 == decChInfo->numFrames)
    {
        UInt32 displayChId;

        Vdec_mapDec2DisplayChId(VDIS_DEV_HDMI,pEmptyBuf->chnId,&displayChId);
        Vdis_setFirstVidPTS(VDIS_DEV_HDMI,displayChId,curTimeStamp);
        Vdec_mapDec2DisplayChId(VDIS_DEV_DVO2,pEmptyBuf->chnId,&displayChId);
        Vdis_setFirstVidPTS(VDIS_DEV_DVO2,displayChId,curTimeStamp);
        Vdec_mapDec2DisplayChId(VDIS_DEV_SD,pEmptyBuf->chnId,&displayChId);
        Vdis_setFirstVidPTS(VDIS_DEV_SD,displayChId,curTimeStamp);
    }
    pEmptyBuf->timestamp = (UInt32)Avsync_getWallTime();
    decChInfo->numFrames += 1;
}

static Void VcapVencVdecVdis_ipcBitsProcessFullBufs(VcapVencVdecVdis_IpcBitsCtrlThrObj *thrObj,
                                       VcapVencVdecVdis_IpcBitsCtrlFileObj *fObj,
                                       VcapVencVdecVdis_ChInfo decChInfo[])
{
    VCODEC_BITSBUF_LIST_S fullBufList;
    VCODEC_BITSBUF_S *pFullBuf;
    VCODEC_BITSBUF_S *pEmptyBuf;
    Int i;
    VCODEC_BITSBUF_LIST_S emptyBufList;
    static Int printStatsInterval = 0;
    static Int32  waitCountPerCh[VCODEC_BITSBUF_MAX] = { 0 };

    #ifdef IPC_BITS_DEBUG
    if ((printStatsInterval % MCFW_IPCBITS_INFO_PRINT_INTERVAL) == 0)
    {
        OSA_printf("MCFW_IPCBITS:%s:INFO: periodic print..",__func__);
    }
    #endif
    printStatsInterval++;

    Venc_getBitstreamBuffer(&fullBufList, 0);

    for (i = 0; i < fullBufList.numBufs; i++)
    {
        VcapVencVdecVdis_updateStatistics(&fullBufList.bitsBuf[i]);

        if (fullBufList.bitsBuf[i].chnId < gDemo_info.maxVdecChannels)
        {
            pFullBuf = &fullBufList.bitsBuf[i];
GET_BUF:    
            VcapVencVdecVdis_ipcBitsGetEmptyBitBufs(&emptyBufList, 
                        pFullBuf->chnId,
                        VcapVencVdecVdis_ipcBitsGetBitBufSize(pFullBuf));
//               gVcapVencVdecVdis_ipcBitsCtrl.thrObj.chInfo[pFullBuf->chnId].chBufSize);
            if (emptyBufList.numBufs && emptyBufList.bitsBuf[0].bufVirtAddr)
            {
                waitCountPerCh[fullBufList.bitsBuf[i].chnId] = 0;

                pEmptyBuf = &emptyBufList.bitsBuf[0];

                if (pFullBuf->filledBufSize <= pEmptyBuf->bufSize)
                {
                    VcapVencVdecVdis_ipcBitsCopyBitBufInfo(pEmptyBuf,pFullBuf);
                    VcapVencVdecVdis_ipcBitsCopyBitBufDataMem2Mem(pEmptyBuf,pFullBuf);
                    VcapVencVdecVdis_setFrameTimeStamp(&(decChInfo[pEmptyBuf->chnId]),pEmptyBuf);
                }
                else
                {
                    OSA_printf("MCFW_IPCBITS: Encode FilledSize [%d] is greater than IPCBits Empty BufSize [%d]",
                               pFullBuf->filledBufSize, pEmptyBuf->bufSize);
                }
                VcapVencVdecVdis_ipcBitsSendFullBitBufs(pEmptyBuf);
            }
            else
            {
                waitCountPerCh[fullBufList.bitsBuf[i].chnId]++;
                if (waitCountPerCh[fullBufList.bitsBuf[i].chnId] < MCFW_IPCBITS_RECVFXN_MAX_WAIT_COUNT_PER_CHANNEL)
                {
                    OSA_waitMsecs(MCFW_IPCBITS_RECVFXN_PERIOD_MS);
                    goto GET_BUF;
                }
                else
                {
                    gVcapVencVdecVdis_ipcBitsCtrl.decBufMissCount[fullBufList.bitsBuf[i].chnId]++;
                    continue;
                }
            }
        }
    }
    if (fObj->enableFWrite)
    {
    	/* ranran */
        if (gDemo_info.usecase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
        {
			VcapVencVdecVdis_ipcBitsCommitWriteBitsToFile(fObj->fpWrHdr,
										  fObj->fpWrData,
										  fObj->fpWrMvData,
										  &fullBufList,
										  fObj->maxFileSize,
										  fObj->fwriteEnableBitMask,
										  fObj->enableLayerWrite,
										  fObj->wrapOccuredHdr,
										  fObj->wrapOccuredBuf);
        }
        else
        {
        	VcapVencVdecVdis_ipcBitsWriteBitsToFile(fObj->fpWrHdr,
                                      fObj->fpWrData,
                                      fObj->fpWrMvData,
                                      &fullBufList,
                                      fObj->maxFileSize,
                                      fObj->fwriteEnableBitMask,
                                      fObj->enableLayerWrite,
                                      fObj->wrapOccuredHdr,
                                      fObj->wrapOccuredBuf);
        }
    }
    Venc_releaseBitstreamBuffer(&fullBufList);
}

static Void *VcapVencVdecVdis_ipcBitsRecvFxn(Void * prm)
{
    VcapVencVdecVdis_IpcBitsCtrl *ipcBitsCtrl = (VcapVencVdecVdis_IpcBitsCtrl *) prm;
    VcapVencVdecVdis_IpcBitsCtrlThrObj *thrObj = &ipcBitsCtrl->thrObj;
    VcapVencVdecVdis_IpcBitsCtrlFileObj *fObj =  &ipcBitsCtrl->fObj;
    UInt32 printStatsInterval = OSA_getCurTimeInMsec();
    UInt32 elapsedTime;

    OSA_printTID(__func__);
    while (FALSE == thrObj->exitBitsInThread)
    {
        OSA_semWait(&thrObj->bitsInNotifySem,OSA_TIMEOUT_FOREVER);
        if(thrObj->exitBitsInThread)
            break;
        VcapVencVdecVdis_ipcBitsProcessFullBufs(thrObj,
                                      fObj,
                                      ipcBitsCtrl->decInfo);

        elapsedTime = OSA_getCurTimeInMsec() - printStatsInterval;

        if (elapsedTime >= 10000)
        {
            #if 1
            VcapVencVdecVdis_printAvgStatistics(elapsedTime, TRUE);
            #endif

            printStatsInterval = OSA_getCurTimeInMsec();
        }
    }
    thrObj->exitBitsInThreadDone = TRUE;
    return NULL;
}


static Void VcapVencVdecVdis_ipcBitsInitThrObj(VcapVencVdecVdis_IpcBitsCtrlThrObj *thrObj)
{

    OSA_semCreate(&thrObj->bitsInNotifySem,
                  MCFW_IPCBITS_MAX_PENDING_RECV_SEM_COUNT,0);
    thrObj->exitBitsInThread = FALSE;
    thrObj->exitBitsInThreadDone = FALSE;

    OSA_thrCreate(&thrObj->thrHandleBitsIn,
                  VcapVencVdecVdis_ipcBitsRecvFxn,
                  MCFW_IPCBITS_RECVFXN_TSK_PRI,
                  MCFW_IPCBITS_RECVFXN_TSK_STACK_SIZE,
                  &gVcapVencVdecVdis_ipcBitsCtrl);

}

static Void VcapVencVdecVdis_ipcBitsDeInitThrObj(VcapVencVdecVdis_IpcBitsCtrlThrObj *thrObj)
{
    thrObj->exitBitsInThread = TRUE;
    OSA_semSignal(&thrObj->bitsInNotifySem);

    while(thrObj->exitBitsInThreadDone == FALSE)
    {
        OSA_waitMsecs(10);
    }

    OSA_thrDelete(&thrObj->thrHandleBitsIn);
    OSA_semDelete(&thrObj->bitsInNotifySem);
}


static
Void VcapVencVdecVdis_ipcBitsGenerateFileName(char *dirPath,
                                    char *fname,
                                    UInt32 chNum,
                                    UInt32 chLayerNum,
                                    char *fsuffix,
                                    char *dstBuf,
                                    UInt32 maxLen)
{
#ifdef DEMO_PHASE_2
	snprintf(dstBuf,
		             (maxLen - 2),
		             "%s/%s_%d_%d.%s",
		             dirPath,
		             "sample",
		             0,
		             0,
		             fsuffix);

	dstBuf[(maxLen - 1)] = 0;
#else
	snprintf(dstBuf,
	             (maxLen - 2),
	             "%s/%s_%d_%d.%s",
	             dirPath,
	             fname,
	             chNum,
	             chLayerNum,
	             fsuffix);
    if(gVcapVencVdecVdis_ipcBitsCtrl.fObj.enableLayerWrite == TRUE)
    {
        snprintf(dstBuf,
             (maxLen - 2),
             "%s/%s_%d_LAYER_%d.%s",
             dirPath,
             fname,
             chNum,
             chLayerNum,
             fsuffix);
    }
    else if(gVcapVencVdecVdis_ipcBitsCtrl.fObj.enableSplitWrite == TRUE)
    {
        snprintf(dstBuf,
             (maxLen - 2),
             "%s/%s_%d_%d.%s",
             dirPath,
             fname,
             chNum,
             chLayerNum,
             fsuffix);
    }
    else
    {
        snprintf(dstBuf,
             (maxLen - 2),
             "%s/%s_%d.%s",
             dirPath,
             fname,
             chNum,
             fsuffix);
    }
    dstBuf[(maxLen - 1)] = 0;
#endif
}


Void VcapVencVdecVdis_ipcBitsInitSetBitsInNoNotifyMode(Bool noNotifyMode)
{
    gVcapVencVdecVdis_ipcBitsCtrl.noNotifyBitsInHLOS = noNotifyMode;
}

Void VcapVencVdecVdis_ipcBitsInitSetBitsOutNoNotifyMode(Bool noNotifyMode)
{
    gVcapVencVdecVdis_ipcBitsCtrl.noNotifyBitsOutHLOS = noNotifyMode;
}

static
UInt32 VcapVencVdecVdis_ipcBitsGetNumEnabledChannels()
{
    UInt32 numEnabledChannels = 0, numCh = 0;
    Int i;
    /* ranran */
    if (gDemo_info.usecase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
    	numCh  = MCFW_IPC_BITS_MAX_NUM_CHANNELS_NEW_RANRAN;
    else
    	numCh  = MCFW_IPC_BITS_MAX_NUM_CHANNELS;

#if (MCFW_IPC_BITS_MAX_NUM_CHANNELS > 64)
    #error "Bitmask for channel fwrite enable supports max 32 channels.Change it"
#endif

    if (FALSE == gVcapVencVdecVdis_ipcBitsCtrl.fObj.enableFWrite)
    {
        numEnabledChannels = 0;
    }
    else
    {
        for (i = 0; i < numCh; i++)
        {
            if ((1 << i) & gVcapVencVdecVdis_ipcBitsCtrl.fObj.fwriteEnableBitMask)
            {
                numEnabledChannels++;
            }

        }
    }
    return (numEnabledChannels);
}

static
unsigned long long VcapVencVdecVdis_ipcBitsGetPartitionFreeSpace(char *filePath)
{
    struct statvfs statBuf;
    int status;
    unsigned long long freeSize;



    status = statvfs(filePath,&statBuf);
    if (status == 0)
    {
        freeSize = statBuf.f_bsize * statBuf.f_bfree;
        freeSize = (float)freeSize *
                     ((float)MCFW_IPC_BITS_MAX_PARTITION_USAGE_PERCENT/100);

    }
    else
    {
        freeSize = 0;

    }
    /* ranran modify so that partition size is in Kbytes (can't store SDD size in unsigned long long */
    freeSize = freeSize/1024;
    OSA_printf("Partition Free Size for Path[%s] = [0x%llX]",
               filePath,freeSize);
    /* ranran added */
    if (g_partitionsize != 0)
	{
    	return g_partitionsize;
	}
    printf("ranran freeSize 0x%llX block_size=0x%lx free_block=0x%lx\n",freeSize,statBuf.f_bsize, statBuf.f_bfree);
    return (freeSize);
}

static
UInt64 VcapVencVdecVdis_ipcBitsGetMaxFileSizePerChannel(char *filePath)
{
    UInt32 maxFileSizePerChannel = 0;
    unsigned long long paritionFreeSize;

    paritionFreeSize = VcapVencVdecVdis_ipcBitsGetPartitionFreeSpace(filePath);
    printf("1  num_enabled_ch %d \n",VcapVencVdecVdis_ipcBitsGetNumEnabledChannels());
    printf("2  paritionFreeSize %lld \n",paritionFreeSize);
    printf("3  paritionFreeSize %lld \n",paritionFreeSize);
/* ranran */
    if (gDemo_info.usecase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
    {
    	return g_filesize;
    }


    if (0 == paritionFreeSize)
    {
        OSA_printf("Not able to determine partition size.Using default size:%d",
                   MCFW_IPC_BITS_MAX_FILE_SIZE);
        maxFileSizePerChannel = MCFW_IPC_BITS_MAX_FILE_SIZE;
    }
    else
    {
        unsigned long maxFileSizeActual;
        maxFileSizeActual = paritionFreeSize/
                            VcapVencVdecVdis_ipcBitsGetNumEnabledChannels();
        if (maxFileSizeActual > MCFW_IPC_BITS_MAX_SINGLE_FILE_SIZE)
        {
            OSA_printf("Limiting File Size to max[0x%X],actual[0x%lX]",
                       MCFW_IPC_BITS_MAX_SINGLE_FILE_SIZE,
                       maxFileSizeActual);
            maxFileSizeActual = MCFW_IPC_BITS_MAX_SINGLE_FILE_SIZE;
        }
        maxFileSizePerChannel = maxFileSizeActual;
        /* Each channel will have a buffer and a header. So divide by 2 */
        maxFileSizePerChannel /= 2;
        OSA_printf("Max File size per channel:0x%X",maxFileSizePerChannel);
    }
    return (maxFileSizePerChannel);
}


/*ranran */
static
Int   VcapVencVdecVdis_ipcBitsOpenPartFileHandles()
{
    Int status = OSA_SOK;
   /* Int splitIndex = 0 ;*/
    unsigned long long paritionFreeSize = 0;
    char fileNameBuffer[128];
    char path[256];
    unsigned long long length = 0;
    unsigned long long maxDeleteRetries = 0;

    printf("ranran #3 VcapVencVdecVdis_ipcBitsOpenPartFileHandles\n");
    init_ordered_array(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath);

    printf("ranran #4 VcapVencVdecVdis_ipcBitsOpenPartFileHandles\n");

    strcpy(fileNameBuffer, gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath);
    strcat(fileNameBuffer, "/.");

	gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize = g_filesize;
	paritionFreeSize = VcapVencVdecVdis_ipcBitsGetPartitionFreeSpace(fileNameBuffer);

	/* TBD */
	if ((gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize/1024) > paritionFreeSize)
	{
	    printf("ranran #5 VcapVencVdecVdis_ipcBitsOpenPartFileHandles\n");

		/* add all files to list */
		/* must remove file */
		/* delete file and remove from array */
		if (g_isCyclic == FALSE)
		{
			g_IsDiskLimit = TRUE;
			printf("no space left");
			return -1;
		}
		else
		{
			maxDeleteRetries = 0;
			while(1)
			{
				remove_file_from_array(path, &length);
				printf("remove file %s length %lld\n",path,length);
				status = remove(path);
				if (status < 0)
				{
					printf("failed in removing file %s\n", path);
				}
				/* get next file index */
				paritionFreeSize = VcapVencVdecVdis_ipcBitsGetPartitionFreeSpace(fileNameBuffer);
				maxDeleteRetries++;
				if (maxDeleteRetries > MAX_RETRY_CNT)
				{
					printf("trying to delete more than %lld times\n",paritionFreeSize);
					exit(1);
				}
				if ((gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize/1024) < paritionFreeSize)
				{
					break;
				}
			}
		}

	}
	else
	{

	}
    printf("ranran #6 VcapVencVdecVdis_ipcBitsOpenPartFileHandles\n");

	printf("partition size %lld  maxsize %lld file %s\n",paritionFreeSize,gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize,fileNameBuffer);
	time_t curr_time = time(NULL);
	VcapVencVdecVdis_ipcBitsGenerateFileName(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath,
							COMMIT_MCFW_IPC_BITS_DATA_FILE_NAME,
						   0,
						   curr_time,
						   gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileExtension,
						   fileNameBuffer,
						   sizeof(fileNameBuffer));
#ifdef DEMO_PHASE_2
	g_currentFile = fopen(fileNameBuffer,"wb");
#else
	g_currentFile = fopen(fileNameBuffer,"a+b");
#endif
	if (g_currentFile == NULL)
	{
		printf("not expected fopen returned null");
		exit(3);
	}
	printf("ranran #2 %s\n",fileNameBuffer);
	status =  setvbuf(g_currentFile,
					  NULL,
					  _IOFBF,
					  MCFW_IPC_BITS_FILEBUF_SIZE_DATA);
	add_file_to_array(fileNameBuffer);
#if 0
	g_numOfSplits = paritionFreeSize/gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize;
	printf("ranran num of splits %d partition %lld file_size %d\n",g_numOfSplits, paritionFreeSize, gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize);

	OSA_assert(g_numOfSplits <= MCFW_IPC_BITS_MAX_NUM_CHANNELS_LAYERS);
    for(splitIndex=0; splitIndex < g_numOfSplits; splitIndex++)
    {

		 /* NOTE:
				   * Based on enableLayerWrite flag which is specific to h264 bit-stream when disabled will do
				   * default file write[MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX is 0],
				   * all other codecs also fall in this case and for when the flag is enabled
				   * will create multiple file handles per temporal layer */
		VcapVencVdecVdis_ipcBitsGenerateFileName(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath,
								   MCFW_IPC_BITS_DATA_FILE_NAME,
								   i,
								   splitIndex,
								   gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileExtension,
								   fileNameBuffer,
								   sizeof(fileNameBuffer));

		gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][splitIndex]
												= fopen(fileNameBuffer,"a+b");
		printf("ranran name %s ch %d split %d fd %d\n",fileNameBuffer, i,splitIndex,(Int) gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][splitIndex]);
		OSA_assert(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][splitIndex] != NULL);
		status =  setvbuf(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][splitIndex],
						  NULL,
						  _IOFBF,
						  MCFW_IPC_BITS_FILEBUF_SIZE_DATA);
		OSA_assert(status != -1);
		gVcapVencVdecVdis_ipcBitsCtrl.fObj.wrapOccuredBuf[i][splitIndex]
												= FALSE;

    }
#endif
/* old

 	fd = fileno(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[fileIdx][g_splitIdx]);
	ftruncate(fd, 0);
*/
    return status;
}



static
Int   VcapVencVdecVdis_ipcBitsOpenFileHandles()
{
    Int status = OSA_SOK;
    Int i,j, numLayers;
    char fileNameHdr[128];
    char fileNameBuffer[128];



    /*ranran*/
    if (gDemo_info.usecase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
    	numLayers = MCFW_IPC_BITS_MAX_NUM_CHANNELS_LAYERS;
    else
    	numLayers = MCFW_IPC_BITS_MAX_NUM_CHANNELS_LAYERS_OLD_RANRAN;
    printf("numLayers %d",numLayers);

    for (i = 0; i < MCFW_IPC_BITS_MAX_NUM_CHANNELS; i++)
    {

        VcapVencVdecVdis_ipcBitsGenerateFileName(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath,
                                       MCFW_IPC_BITS_HDR_FILE_NAME,
                                       i,
                                       0,
                                       MCFW_IPC_BITS_DEFAULT_FILE_EXTENSION,
                                       fileNameHdr,
                                       sizeof(fileNameHdr));
        gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrHdr[i] = fopen(fileNameHdr,"wb");

        OSA_assert(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrHdr[i] != NULL);

        if (0 == i)
        {

            gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize =
                VcapVencVdecVdis_ipcBitsGetMaxFileSizePerChannel(fileNameHdr);
        	printf("set maxfile size 1 %lld\n",gVcapVencVdecVdis_ipcBitsCtrl.fObj.maxFileSize);
        }

        status =  setvbuf(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrHdr[i],
                          NULL,
                          _IOFBF,
                          MCFW_IPC_BITS_FILEBUF_SIZE_HDR);
        OSA_assert(status != -1);
        gVcapVencVdecVdis_ipcBitsCtrl.fObj.wrapOccuredHdr[i] = FALSE;

        if(gVcapVencVdecVdis_ipcBitsCtrl.fObj.enableLayerWrite == TRUE)
        {
            for (j = 0; j < numLayers; j++)
            {
                VcapVencVdecVdis_ipcBitsGenerateFileName(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath,
                                           MCFW_IPC_BITS_DATA_FILE_NAME,
                                           i,
                                           j,
                                           gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileExtension,
                                           fileNameBuffer,
                                           sizeof(fileNameBuffer));

                gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][j] = fopen(fileNameBuffer,"wb");
                OSA_assert(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][j] != NULL);
                status =  setvbuf(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][j],
                                  NULL,
                                  _IOFBF,
                                  MCFW_IPC_BITS_FILEBUF_SIZE_DATA);
                OSA_assert(status != -1);
                gVcapVencVdecVdis_ipcBitsCtrl.fObj.wrapOccuredBuf[i][j] = FALSE;
            }
        }
        else
        {
             /* NOTE:
                       * Based on enableLayerWrite flag which is specific to h264 bit-stream when disabled will do
                       * default file write[MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX is 0],
                       * all other codecs also fall in this case and for when the flag is enabled
                       * will create multiple file handles per temporal layer */
            VcapVencVdecVdis_ipcBitsGenerateFileName(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath,
                                       MCFW_IPC_BITS_DATA_FILE_NAME,
                                       i,
                                       0,
                                       gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileExtension,
                                       fileNameBuffer,
                                       sizeof(fileNameBuffer));

            gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX]
                                                    = fopen(fileNameBuffer,"wb");
            OSA_assert(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX] != NULL);
            status =  setvbuf(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX],
                              NULL,
                              _IOFBF,
                              MCFW_IPC_BITS_FILEBUF_SIZE_DATA);
            OSA_assert(status != -1);
            gVcapVencVdecVdis_ipcBitsCtrl.fObj.wrapOccuredBuf[i][MCFW_IPC_BITS_DATA_FILE_DEFAULTLAYER_INDEX]
                                                    = FALSE;

        }

        VcapVencVdecVdis_ipcBitsGenerateFileName(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath,
                                       MCFW_IPC_BITS_DATA_FILE_NAME,
                                       i,
                                       0,
                                       gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileExtension,
                                       fileNameBuffer,
                                       sizeof(fileNameBuffer));

        strncat(fileNameBuffer,".mv",3);
        gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrMvData[i] = fopen(fileNameBuffer,"wb");
        OSA_assert(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrMvData[i] != NULL);
        status =  setvbuf(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrMvData[i],
                          NULL,
                          _IOFBF,
                          MCFW_IPC_BITS_FILEBUF_SIZE_DATA);
        OSA_assert(status != -1);

    }
    return status;
}


static
Int   VcapVencVdecVdis_ipcBitsInitFileHandles()
{
    Int i,j, numCh, numLayers;
    printf("VcapVencVdecVdis_ipcBitsInitFileHandles\n");
    /* ranran */
    if (gDemo_info.usecase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
    {

    	numCh  = MCFW_IPC_BITS_MAX_NUM_CHANNELS_NEW_RANRAN;
    	numLayers = MCFW_IPC_BITS_MAX_NUM_CHANNELS_LAYERS;
    }
    else
    {
    	numCh  = MCFW_IPC_BITS_MAX_NUM_CHANNELS;
    	numLayers = MCFW_IPC_BITS_MAX_NUM_CHANNELS_LAYERS_OLD_RANRAN;
    }

    for (i = 0; i < numCh; i++)
    {
        if (gDemo_info.usecase != VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
        {
        	MCFW_IPC_BITS_INIT_FILEHANDLE (gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrHdr[i]);
        }
        for (j = 0; j < numLayers; j++)
        {
            MCFW_IPC_BITS_INIT_FILEHANDLE (gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrData[i][j]);
        }
        if (gDemo_info.usecase != VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
        {
        	MCFW_IPC_BITS_INIT_FILEHANDLE (gVcapVencVdecVdis_ipcBitsCtrl.fObj.fpWrMvData[i]);
        }
    }

    return OSA_SOK;
}


Int32 VcapVencVdecVdis_ipcBitsFileWritePath (char path[])
{
    strcpy (path, gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath);
    return 0;
}

static Int32 VcapVencVdecVdis_ipcBitsInitFObj()
{
    static Bool fileDirInputDone = FALSE;

    VcapVencVdecVdis_ipcBitsInitFileHandles();
    if (gDemo_info.usecase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
    {
    	fileDirInputDone = TRUE;
        strcpy (gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath, g_videoFilePath);
    }
    else
    {
    	strcpy (gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath, MCFW_IPC_BITS_FILE_STORE_DIR);
    }

    if (!fileDirInputDone)
    {

        Demo_getFileWritePath(
                gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileDirPath,
                MCFW_IPC_BITS_FILE_STORE_DIR
                );

        fileDirInputDone = TRUE;
    }
    /* ranran */
    if (gDemo_info.usecase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
    {
    	VcapVencVdecVdis_ipcBitsOpenPartFileHandles();
    }
    else
    {
    	VcapVencVdecVdis_ipcBitsOpenFileHandles();
    }
    return OSA_SOK;
}


static Int32 VcapVencVdecVdis_ipcBitsInitDmaObj()
{
    Int32 status = OSA_SOK;

    if (gVcapVencVdecVdis_ipcBitsCtrl.dmaObj.useDma)
    {
        status = OSA_dmaInit();
        OSA_assert(status == OSA_SOK);

        status =
        OSA_dmaOpen(&gVcapVencVdecVdis_ipcBitsCtrl.dmaObj.dmaChHdnl,
                    OSA_DMA_MODE_NORMAL,
                    MCFW_IPC_BITS_DMA_MAX_TRANSFERS);
        OSA_assert(status == OSA_SOK);

    }
    if (OSA_SOK != status)
    {
        OSA_printf("DEMO_MCFW_IPC_BITS_CTRL:Disabling DMA as channel open failed[%d]",
                   status);
        gVcapVencVdecVdis_ipcBitsCtrl.dmaObj.useDma = FALSE;
    }
    return status;
}

static Int32 VcapVencVdecVdis_ipcBitsDeInitDmaObj()
{
    Int32 status = OSA_SOK;

    if (gVcapVencVdecVdis_ipcBitsCtrl.dmaObj.useDma)
    {

        status =
        OSA_dmaClose(&gVcapVencVdecVdis_ipcBitsCtrl.dmaObj.dmaChHdnl);
        OSA_assert(status == OSA_SOK);

        status = OSA_dmaExit();
        OSA_assert(status == OSA_SOK);
    }
    return status;
}

Int32 VcapVencVdecVdis_ipcBitsSetFileWriteMask (UInt32 fileWritemask)
{
    gVcapVencVdecVdis_ipcBitsCtrl.fObj.fwriteEnableBitMask = fileWritemask;
    return 0;
}

Int32 VcapVencVdecVdis_ipcBitsSetFileExtension (char *fileExt)
{
    strcpy(gVcapVencVdecVdis_ipcBitsCtrl.fObj.fileExtension, fileExt);
    return 0;
}


Int32 VcapVencVdecVdis_ipcBitsInit(VcapVencVdecVdis_chBufInfo chInfo[],
                                         Bool enableFWrite,
                                         Bool enableLayerWrite,
                                         Bool enableSplitWrite)

{
    Int32 i;
    VENC_CALLBACK_S callback;

    VcapVencVdecVdis_resetAvgStatistics();
    VcapVencVdecVdis_resetStatistics();

    gVcapVencVdecVdis_ipcBitsCtrl.fObj.enableFWrite = enableFWrite;
    gVcapVencVdecVdis_ipcBitsCtrl.fObj.enableLayerWrite = enableLayerWrite;
    gVcapVencVdecVdis_ipcBitsCtrl.fObj.enableSplitWrite = enableSplitWrite;
    callback.newDataAvailableCb = VcapVencVdecVdis_ipcBitsInCbFxn;
    /* Register call back with encoder */
    Venc_registerCallback(&callback,
                         (Ptr)&gVcapVencVdecVdis_ipcBitsCtrl);

    if (gVcapVencVdecVdis_ipcBitsCtrl.fObj.enableFWrite == TRUE)
    {
        VcapVencVdecVdis_ipcBitsInitFObj();
    }
    VcapVencVdecVdis_ipcBitsInitDmaObj();

    for (i=0; i<MCFW_IPCBITS_MAX_CHANNELS; i++)
    {
        gVcapVencVdecVdis_ipcBitsCtrl.thrObj.chInfo[i] = chInfo[i];
//        printf ("#IPCBITS - [%dx%d, size: %d]\n",
//               chInfo[i].width, chInfo[i].height, chInfo[i].chBufSize);
    }

    memcpy(gVcapVencVdecVdis_ipcBitsCtrl.thrObj.chInfo, chInfo, sizeof(gVcapVencVdecVdis_ipcBitsCtrl.thrObj.chInfo));
    VcapVencVdecVdis_ipcBitsInitThrObj(&gVcapVencVdecVdis_ipcBitsCtrl.thrObj);
    memset (&gVcapVencVdecVdis_ipcBitsCtrl.decInfo,0,sizeof(gVcapVencVdecVdis_ipcBitsCtrl.decInfo));
    return OSA_SOK;
}

Void VcapVencVdecVdis_ipcBitsStop(void)
{
    gVcapVencVdecVdis_ipcBitsCtrl.thrObj.exitBitsInThread = TRUE;
}

Int32 VcapVencVdecVdis_ipcBitsExit()
{
    OSA_printf("Entered:%s...",__func__);
    if (gVcapVencVdecVdis_ipcBitsCtrl.fObj.enableFWrite == TRUE){
        VcapVencVdecVdis_ipcBitsInitFileHandles();
        if (gDemo_info.usecase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
        {
        	fclose (g_currentFile);
        }
    }
    VcapVencVdecVdis_ipcBitsDeInitDmaObj();
    VcapVencVdecVdis_ipcBitsDeInitThrObj(&gVcapVencVdecVdis_ipcBitsCtrl.thrObj);
    OSA_printf("Leaving:%s...",__func__);
    return OSA_SOK;
}


Int32 VcapVencVdecVdis_resetStatistics()
{
    UInt32 chId;
    VcapVencVdecVdis_ChInfo *pChInfo;

    for(chId=0; chId<VENC_CHN_MAX; chId++)
    {
        pChInfo = &gVcapVencVdecVdis_ipcBitsCtrl.chInfo[chId];

        pChInfo->totalDataSize = 0;
        pChInfo->numKeyFrames = 0;
        pChInfo->numFrames = 0;
        pChInfo->maxWidth = 0;
        pChInfo->minWidth = 0xFFF;
        pChInfo->maxHeight= 0;
        pChInfo->minHeight= 0xFFF;
        pChInfo->maxLatency= 0;
        pChInfo->minLatency= 0xFFF;

    }

    gVcapVencVdecVdis_ipcBitsCtrl.statsStartTime = OSA_getCurTimeInMsec();

    return 0;
}

Int32 VcapVencVdecVdis_resetAvgStatistics()
{
    gVcapVencVdecVdis_ipcBitsCtrl.totalEncFrames = 0;
    gVcapVencVdecVdis_ipcBitsCtrl.totalDecFrames = 0;
    memset(gVcapVencVdecVdis_ipcBitsCtrl.decBufMissCount, 0,
            sizeof(gVcapVencVdecVdis_ipcBitsCtrl.decBufMissCount));

    return 0;
}

Int32 VcapVencVdecVdis_printAvgStatistics(UInt32 elapsedTime, Bool resetStats)
{
    UInt32 totalFrames, i;

    totalFrames = gVcapVencVdecVdis_ipcBitsCtrl.totalEncFrames+gVcapVencVdecVdis_ipcBitsCtrl.totalDecFrames;

    /* show total average encode FPS */
    printf( " ## AVERAGE: ENCODE [%6.1f] FPS, DECODE [%6.1f] FPS, ENC+DEC [%6.1f] FPS ... in %4.1f secs, \n",
            gVcapVencVdecVdis_ipcBitsCtrl.totalEncFrames*1000.0/elapsedTime,
            gVcapVencVdecVdis_ipcBitsCtrl.totalDecFrames*1000.0/elapsedTime,
            totalFrames*1000.0/elapsedTime,
            elapsedTime/1000.0
            );

    i = 0;

#if     0
    printf ( " ## DECBUF_MISS_CNT: \t");
    for (i=0; i<Venc_getPrimaryChannels(); i++)
    {
        printf ( " [%6d] \t",
            gVcapVencVdecVdis_ipcBitsCtrl.decBufMissCount[i]);
    }
    printf ("\n");
#endif

    if(resetStats)
        VcapVencVdecVdis_resetAvgStatistics();

    return 0;
}

Int32 VcapVencVdecVdis_printStatistics(Bool resetStats, Bool allChs)
{
    UInt32 chId;
    VcapVencVdecVdis_ChInfo *pChInfo;
    float elapsedTime;

    elapsedTime = OSA_getCurTimeInMsec() - gVcapVencVdecVdis_ipcBitsCtrl.statsStartTime;

    elapsedTime /= 1000.0; // in secs

    if(allChs)
    {
        printf( "\n"
            "\n *** Encode Bitstream Received Statistics *** "
            "\n"
            "\n Elased time = %6.1f secs"
            "\n"
            "\n CH | Bitrate (Kbps) | FPS | Key-frame FPS | Width (max/min) | Height (max/min) | Latency (max/min)"
            "\n --------------------------------------------------------------------------------------------------",
            elapsedTime
            );

        for(chId=0; chId<VENC_CHN_MAX;chId++)
        {
            pChInfo = &gVcapVencVdecVdis_ipcBitsCtrl.chInfo[chId];

            if(pChInfo->numFrames)
            {
                printf("\n %2d | %14.2f | %3.1f | %13.1f | %5d / %6d | %6d / %6d  | %6d / %6d",
                    chId,
                    (pChInfo->totalDataSize*8.0/elapsedTime)/1024.0,
                    pChInfo->numFrames*1.0/elapsedTime,
                    pChInfo->numKeyFrames*1.0/elapsedTime,
                    pChInfo->maxWidth,
                    pChInfo->minWidth,
                    pChInfo->maxHeight,
                    pChInfo->minHeight,
                    pChInfo->maxLatency,
                    pChInfo->minLatency

                );
            }
        }

        printf("\n");

        if(resetStats)
            VcapVencVdecVdis_resetStatistics();
    }
    else
    {
    }

    return 0;
}

Int32 VcapVencVdecVdis_updateStatistics(VCODEC_BITSBUF_S *pBuf)
{
    VcapVencVdecVdis_ChInfo *pChInfo;
    UInt32 latency;

    if(pBuf->chnId<VENC_CHN_MAX)
    {
        pChInfo = &gVcapVencVdecVdis_ipcBitsCtrl.chInfo[pBuf->chnId];

        pChInfo->totalDataSize += pBuf->filledBufSize;
        pChInfo->numFrames++;
        gVcapVencVdecVdis_ipcBitsCtrl.totalEncFrames++;
        if(pBuf->frameType==VCODEC_FRAME_TYPE_I_FRAME)
        {
            pChInfo->numKeyFrames++;
        }

        latency = Avsync_getWallTime() - (((UInt64)pBuf->upperTimeStamp << 32) |  (UInt64)pBuf->lowerTimeStamp);

        if(latency > pChInfo->maxLatency)
            pChInfo->maxLatency = latency;

        if(latency < pChInfo->minLatency)
            pChInfo->minLatency = latency;

        if(pBuf->frameWidth > pChInfo->maxWidth)
            pChInfo->maxWidth = pBuf->frameWidth;

        if(pBuf->frameWidth < pChInfo->minWidth)
            pChInfo->minWidth = pBuf->frameWidth;

        if(pBuf->frameHeight > pChInfo->maxHeight)
            pChInfo->maxHeight = pBuf->frameHeight;

        if(pBuf->frameHeight < pChInfo->minHeight)
            pChInfo->minHeight = pBuf->frameHeight;

    }
    return 0;
}
