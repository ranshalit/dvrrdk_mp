/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2011 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/



#ifndef _DEMO_AUDIO_H_
#define _DEMO_AUDIO_H_


#include <demo.h>
#include <alsa/asoundlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef  DSP_RPE_AUDIO_ENABLE
#include <osa_thr.h>
#include <ti/xdais/dm/iaudio.h>
#endif

#include <osa_thr.h>
#include <osa_mbx.h>

/* Use this macro if ALSA Playback needs to be done using mmap mode 
  * In this demo app (live playback), there is no advantage with mmap mode
  * as there is still a data copy from capture buffer. MMAP mode will be useful
  * in scenarios (like hdd playback) where one level of data copy can be avoided
  * by accessing mmaped driver buffers 
  */
// #define DEMO_USE_MMAP_MODE_FOR_AUDIO_PLAYBACK

// #define     USE_DEFAULT_CONFIG_FOR_AUDIO    /* Dont get any user input */
#ifdef  USE_DEFAULT_CONFIG_FOR_AUDIO
#define AUDIO_DEFAULT_FILE_WRITE_PATH         "/dev/shm"
#define AUDIO_DEFAULT_NUM_ENCODE_CHANNELS     4
#define AUDIO_DEFAULT_PLAYBACK_CHANNEL        0
#define AUDIO_DEFAULT_PLAYBACK_DEVICE         AUDIO_PLAYBACK_DEVICE_ENUM_HDMI
#endif

/****************************************************/
#define AUDIO_BITS_FWRITE_BUFFER_SIZE   (256*1024)

#ifdef  DSP_RPE_AUDIO_ENABLE
#define ENCODE_TYPE                         AUDIO_CODEC_TYPE_AAC_LC     
#define ENCODE_BUFFER_FROM_SHARED_REGION  TRUE
#else
#define ENCODE_TYPE                         AUDIO_CODEC_TYPE_G711
#define ENCODE_BUFFER_FROM_SHARED_REGION  FALSE
#endif

#define ENCODE_BITRATE                  24000   /* value really depends on codec guidelines */   
#define AUDIO_TSK_PRI                   17
#define AUDIO_TSK_STACK_SIZE           (10*1024)

#define MAX_IN_SAMPLES                  (1024)
#define SAMPLE_LEN                      2

#define MAX_INPUT_BUFFER                (4*1024)
#define MAX_OUTPUT_BUFFER               (4*1024)

#define AUDIO_MAX_PLAYBACK_BUF_SIZE     (120*1024)  /* Max hd read for ~4 secs worth of data assuming 16KHz */

#define AUDIO_PLAYBACK_DEVICE_AIC       "plughw:0,1"
#define AUDIO_PLAYBACK_DEVICE_HDMI      "plughw:1,0"

enum
{
    AUDIO_PLAYBACK_DEVICE_ENUM_AIC = 0,
    AUDIO_PLAYBACK_DEVICE_ENUM_HDMI,
} AUDIO_PLAYBACK_DEVICE;

#define AUDIO_CHANNELS_SUPPORTED        1       //< Only mono

#define AUDIO_BUFFERING_BEFORE_PLAY     2

#define AUDIO_ERROR(...) \
  fprintf(stderr, " ERROR  (%s|%s|%d): ", __FILE__, __func__, __LINE__); \
  fprintf(stderr, __VA_ARGS__);

#define     AUD_DEVICE_PRINT_ERROR_AND_RETURN(str, err, hdl)        \
        fprintf (stderr, "AUDIO >> " str, snd_strerror (err));  \
        snd_pcm_close (hdl);    \
        return  -1;


typedef struct  _AudioStats
{
    Int32   errorCnt;
    Int32   lastError;
    UInt32  samplesPlayed;
    Int32   insideAlsaWrite;
    UInt32  curPlaybackBuf;

} AudioStats;

Void *App_allocBuf(Int32 bufSize, Bool fromSharedRegion);
Void App_freeBuf (Void *buf, Int32 bufSize, Bool fromSharedRegion);
Bool Demo_SuspendAudioPlayBackOnHdmi(void);
Bool Demo_ResumeAudioPlayBackOnHdmi(void);
Bool Demo_postNewAudioData(void);
#endif /* _DEMO_AUDIO_H_ */
