#include <demo_audio.h>
#include <osa_que.h>
#include <demo_vcap_venc_vdec_vdis.h>

#define WARN_CAPT   "\r\n Demo Audio Capture Warning : "
#define INFO_CAPT   "\r\n Demo Audio Capture Info : "
#define ERR_CAPT    "\r\n Demo Audio Capture ERROR : "

#define WARN_PLAY   "\r\n Demo Audio Playback Warning : "
#define INFO_PLAY   "\r\n Demo Audio Playback Info : "
#define ERR_PLAY    "\r\n Demo Audio Playback ERROR : "

/* Local defines */

#define DEMO_SUSPEND_AUDIO_PLAYBACK (1)
#define DEMO_RESUME_AUDIO_PLAYBACK  (2)

#define AUDIO_FWRITE_ONLY_ENCODED_CHANNELS    /* Write only encoded channels data to file */
#define AUDIO_MAX_CH_BUFFERS                50	 /* This the maximum queuing possible. The internal buffers are much smaller than this */

typedef struct
{
    ACAP_GET_DATA_PARAMS_S   dataPrm;
    Int32                    chNum;
} APP_AUDIO_PLAYBACK_DATA;

static UInt8                    *gEncodeBuf[ACAP_CHANNELS_MAX];
static UInt16                   *gCaptureBuf[ACAP_CHANNELS_MAX];

static OSA_ThrHndl              gAppCaptureThread;
static OSA_ThrHndl              gAppPlaybackThread;

static OSA_QueHndl              gChAudioPlaybackBufQue;
static OSA_QueHndl              gChAudioPlaybackEmptyBufQue;
static APP_AUDIO_PLAYBACK_DATA  gChAudioDataParams[AUDIO_MAX_CH_BUFFERS];

static Bool                     gAppCaptureThreadActive = FALSE;
static Bool                     gAppPlaybackThreadActive = FALSE;

static Bool                     gAppCaptureThreadExitFlag = FALSE;
static Bool                     gAppPlaybackThreadExitFlag = FALSE;

static Audio_CapInfo            gCapInfo;

static snd_pcm_t                *play_handle = NULL;
static snd_pcm_uframes_t        period_size;
static Int32                    size;
static Int32                    playAudioFlag = 0;
static AudioStats               playStats;

static OSA_SemHndl      gStartPlayNotifySem;
static OSA_SemHndl      gStartCaptureNotifySem;
static UInt32           gSamplingHz;
static Bool             gIsTvConnected = FALSE;
static OSA_MbxHndl      gPlaybackMbx;
static OSA_MbxHndl      gPlaybackSuspendMbx;
static OSA_MbxHndl      gPlaybackResumeMbx;
static OSA_MbxHndl      *gPlaybackMbxHndl = NULL;


Void *App_allocBuf(Int32 bufSize, Bool fromSharedRegion)
{
    if (fromSharedRegion == TRUE)
        return Audio_allocateSharedRegionBuf(bufSize);
    else
        return malloc(bufSize);
}

Void App_freeBuf (Void *buf, Int32 bufSize, Bool fromSharedRegion)
{
    if (fromSharedRegion == TRUE)
        Audio_freeSharedRegionBuf(buf, bufSize);
    else
        free(buf);
}

static Int32 App_printChStats ()
{
    Int32 chNum = 0;

    if (gCapInfo.enablePlayback == TRUE)
        chNum = gCapInfo.playbackChNum;

    Acap_printChStats(chNum);

    if (gCapInfo.enablePlayback == TRUE)
    {
        printf (INFO_PLAY "samples played: %d, ErrCnt: %d, LastErr: %s, InsideAlsa: %d, lastBuf: %X\n", 
                playStats.samplesPlayed, 
                playStats.errorCnt,
                snd_strerror(playStats.lastError),
                playStats.insideAlsaWrite,
                (int) playStats.curPlaybackBuf);
    }
    printf (INFO_CAPT"CH bufs rd: %d, wr: %d\n", 
             OSA_queGetQueuedCount(&gChAudioPlaybackEmptyBufQue),
             OSA_queGetQueuedCount(&gChAudioPlaybackBufQue));
    return 0;
}

static Int32 Audio_initRenderDevice (Int32 channels, Uint32 sample_rate, UInt32 playbackDevId)
{
    Int32 rc;
    snd_pcm_hw_params_t *params;
    Uint32 val, rate;
    Int32 dir, ret;
    Int32 resample;

    if(playbackDevId == AUDIO_PLAYBACK_DEVICE_ENUM_AIC)
    {
        /* Open AIC device for playback. */
        rc = snd_pcm_open(&play_handle, AUDIO_PLAYBACK_DEVICE_AIC,
                         SND_PCM_STREAM_PLAYBACK,
                                0);
        
    }
    else
    {
        /* Open HDMI device for playback. */
        rc = snd_pcm_open(&play_handle, AUDIO_PLAYBACK_DEVICE_HDMI,
                         SND_PCM_STREAM_PLAYBACK,
                                0);        
    }


    if (rc < 0)
    {
        printf(ERR_PLAY " Unable to open pcm device: %s", snd_strerror(rc));
        return -1;
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(play_handle, params);

    /* Set the desired hardware parameters. */
    /* Interleaved mode */
#ifndef DEMO_USE_MMAP_MODE_FOR_AUDIO_PLAYBACK
    snd_pcm_hw_params_set_access(play_handle, params,
                   SND_PCM_ACCESS_RW_INTERLEAVED);
#else
    {
        snd_pcm_access_mask_t *mask = alloca(snd_pcm_access_mask_sizeof());

        snd_pcm_access_mask_none(mask);
        snd_pcm_access_mask_set(mask, SND_PCM_ACCESS_MMAP_INTERLEAVED);
        // snd_pcm_access_mask_set(mask, SND_PCM_ACCESS_MMAP_COMPLEX);
        rc = snd_pcm_hw_params_set_access_mask(play_handle, params, mask);

        if (rc < 0) 
        {
            printf("\n AUDIO >> MMAP Access type not available....\n");
        } 
        else
        {
            printf("\n AUDIO >> MMAP Access type available....\n");
        }

    }
#endif

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(play_handle, params,
                           SND_PCM_FORMAT_S16_LE);

    rc = snd_pcm_hw_params_set_channels(play_handle, params, channels);
    if (rc < 0)
    {
        printf(ERR_PLAY " Channels count (%i) not available for playbacks: %s", channels, snd_strerror(rc));
    }

    dir = 0;
    ret = snd_pcm_hw_params_set_rate_near(play_handle, params,
                               &sample_rate, &dir);
    if (ret != 0)
    {
        printf(WARN_PLAY " The rate %d Hz is not supported by your hardware.\n ==> Using %d Hz instead.", sample_rate, ret);
    }
    else
    {
        printf(INFO_PLAY " The rate %d Hz set for play back hardware, ret - %d", sample_rate, ret);
    }

    period_size = 1024;  

    if(playbackDevId == AUDIO_PLAYBACK_DEVICE_ENUM_HDMI) // Works for hdmi 
    {
        snd_pcm_uframes_t bufferSizeMax = (2*1024);

        resample = 1;
        snd_pcm_hw_params_set_rate_resample(play_handle, params, resample);

        rc = snd_pcm_hw_params_set_period_size_near(play_handle,
                                  params, &period_size, &dir);
        if (rc < 0)
        {
            printf(ERR_PLAY " Unable to set period size %li err: %s", period_size, snd_strerror(rc));
            return -1;
        }

        if ((rc = snd_pcm_hw_params_set_buffer_size (play_handle, params, bufferSizeMax)) < 0)
        {
            printf("\n\nAUDIO >>  cannot set buffer size (%lu)\n", bufferSizeMax);
        }

    }
    else if(playbackDevId == AUDIO_PLAYBACK_DEVICE_ENUM_AIC)// Works for aic 
    {
        snd_pcm_uframes_t bufferSizeMax = (4*1024);

        rc = snd_pcm_hw_params_set_period_size_near(play_handle,
                                  params, &period_size, &dir);
        if (rc < 0)
        {
            printf(ERR_PLAY " Unable to set period size %li err: %s", period_size, snd_strerror(rc));
            return -1;
        }

        if ((rc = snd_pcm_hw_params_set_buffer_size (play_handle, params, bufferSizeMax)) < 0)
        {
            printf(ERR_PLAY " cannot set buffer size (%lu)", bufferSizeMax);
        }

    }

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(play_handle, params);
    if (rc < 0)
    {
        AUD_DEVICE_PRINT_ERROR_AND_RETURN("\n Unable to set hw parameters: (%s)", rc, play_handle);
        play_handle = NULL;
        return -1;
    }

    /* Use a buffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(params, &period_size,
                                    &dir);

    size = period_size * 2; /* 2 bytes/sample, 1 channel */
    snd_pcm_hw_params_get_period_time(params, &val, &dir);
    snd_pcm_hw_params_get_rate(params, &rate, &dir);
    printf(INFO_PLAY " Audio Playback Device Opened,"
           " period size: %lu, period time: %d, rate: %d", 
           period_size, val, rate);
    return 0;
}


Int32 Audio_deInitRenderDevice (void)
{
    if (play_handle)
    {
        snd_pcm_close(play_handle);
        play_handle = NULL;
        printf(INFO_PLAY " Playback Device Closed");
    }
    return 0;
}

#ifdef  DEMO_USE_MMAP_MODE_FOR_AUDIO_PLAYBACK
static Int32 xrun_recovery(snd_pcm_t *handle, Int32 err, Int8 *errStr)
{
    printf (ERR_PLAY "(%s %d) -> %s: stream recovery -> err: %s\n", 
            __FUNCTION__, __LINE__, errStr, snd_strerror(err));

    playStats.errorCnt++;
    playStats.lastError = err;

    if (err == -EPIPE) 
    {    
        /* under-run */
        err = snd_pcm_prepare(handle);
        if (err < 0)
        {
            printf(ERR_PLAY "(%s %d) Can't recovery from underrun, prepare failed: %s\n", __FUNCTION__, __LINE__, snd_strerror(err));
        }
        return 0;
    } 
    else if (err == -ESTRPIPE) 
    {
        while ((err = snd_pcm_resume(handle)) == -EAGAIN)
        {
            usleep(20);       /* wait until the suspend flag is released */
        }

        if (err < 0) 
        {
            err = snd_pcm_prepare(handle);
            if (err < 0)
            {
                printf(ERR_PLAY "(%s %d) Can't recovery from suspend, prepare failed: %s\n", __FUNCTION__, __LINE__, snd_strerror(err));
            }
        }
        return 0;
    }
    return err;
}
#endif

Int32   Audio_playBuffer(UInt8 *buffer, Int32 numBytes)
{
    if (play_handle == NULL)
    {
        printf(ERR_PLAY " Device play_handle is NULL");
        printf(ERR_PLAY " Skipping to play this buffer");
        return 0;
    }

#ifdef DEMO_USE_MMAP_MODE_FOR_AUDIO_PLAYBACK
    {
#define SAMPLE_LEN      2
        Int32 samples_played = 0;
        const snd_pcm_channel_area_t *my_areas;
        snd_pcm_uframes_t offset, avail;
        snd_pcm_sframes_t commitres;
        Int32 err;
        UInt8 *ptr;
        static Int32 first = 1;
        Int32 numSamples = numBytes / SAMPLE_LEN;

        while (numSamples > 0)
        {
            avail = snd_pcm_avail_update(play_handle);
            if (avail < period_size) 
            {
                avail = 0;
            }


            if (avail)
            {
//                printf (ERR_PLAY "(%s %d) snd_pcm_avail (PLAYBACK) -> avail: %li, numSamples: %d\n", 
//                        __FUNCTION__, __LINE__, avail, numSamples);

                if (avail > numSamples)
                    avail = numSamples;
                
                err = snd_pcm_mmap_begin(play_handle, &my_areas, &offset, &avail);
                if (err < 0) 
                {
                    if ((err = xrun_recovery(play_handle, err, "mmap_begin ->")) < 0) 
                    {
                        printf(ERR_PLAY "(%s %d) MMAP begin avail error: %s\n", 
                                        __FUNCTION__, __LINE__, snd_strerror(err));
                    }
                    first = 1;
                }

//                printf (ERR_PLAY "(%s %d) avail %li, offset %li areas[0].add %X areas[0].first %d\n",
//                    __FUNCTION__, __LINE__, avail, offset, (unsigned int)my_areas[0].addr, my_areas[0].first);


                ptr = my_areas[0].addr + (offset * SAMPLE_LEN);
                if (ptr)
                {
                    memcpy(ptr, buffer, (avail*SAMPLE_LEN));
                }

                commitres = snd_pcm_mmap_commit(play_handle, offset, avail);
                if (commitres < 0 || (snd_pcm_uframes_t)commitres != avail) 
                {
                    printf(ERR_PLAY "(%s %d) mmap_commit for offset %li, avail %li, commitres %li\n", 
                                __FUNCTION__, __LINE__, offset, avail, commitres);
                    if ((err = xrun_recovery(play_handle, commitres >= 0 ? -EPIPE : commitres, "commit ->")) < 0) 
                    {
                            printf("(%s %d) MMAP commit error: %s\n", __FUNCTION__, __LINE__, snd_strerror(err));
                            exit(EXIT_FAILURE);
                    }
                    first = 1;
                }
                numSamples -= avail;
                numBytes -= (avail * SAMPLE_LEN);
                samples_played += avail;
            }
            else
            {
                usleep(20);
            }
            if (first == 1)
            {
                err = snd_pcm_start(play_handle);
                if (err < 0) 
                {
                    printf(ERR_PLAY "(%s %d) Start error: %s\n", __FUNCTION__, __LINE__, snd_strerror(err));
                }
                first = 0;
            }
        }
        return samples_played;
    }
#else
    {
        Int32 samples_played = 0;
        Int32 rc;

        while ((numBytes >= size) && playAudioFlag)
        {
            playStats.insideAlsaWrite = 1;
            rc = snd_pcm_writei(play_handle, buffer + samples_played, period_size);
            playStats.insideAlsaWrite = 0;
            samples_played += size;
            numBytes -= size;
            if (rc == -EPIPE)
            {
                /* EPIPE means underrun */
                printf(ERR_PLAY " Underrun occurred");
                snd_pcm_prepare(play_handle);
                playStats.errorCnt++;
                playStats.lastError = rc;
                break;
            }
            else if (rc < 0)
            {
                printf(ERR_PLAY " Could not write (writei returned error %s)",
                    snd_strerror(rc));
                playStats.errorCnt++;
                playStats.lastError = rc;
                break;
            }
            else if (rc != (Int32)period_size)
            {
                printf(ERR_PLAY " short write, wrote %d frames", rc);
                playStats.errorCnt++;
                playStats.lastError = rc;
            }
        }
        return samples_played;
    }
#endif
}

static void waitToExit(void)
{
    while ( gAppPlaybackThreadActive == TRUE)
        sleep(1);
}

static void App_updateChTime(UInt32 *pTimeTotal, UInt32 *pTimeLast)
{
    if (pTimeLast && pTimeTotal)
    {
        UInt32 timeElapsed = OSA_getCurTimeInMsec() - (*pTimeLast);
        (*pTimeTotal) += timeElapsed;
        *pTimeLast = OSA_getCurTimeInMsec();
    }
}

static void App_initChTime(UInt32 timeLast[], Int32 maxChannels)
{
    Int32 i;

    timeLast[0] = OSA_getCurTimeInMsec();
    for (i=1; i<maxChannels; i++)
        timeLast[i] = timeLast[0];
}

static Void *App_playbackTaskFxn(Void * prm)
{
    Int32 status;
    VDIS_SCREEN_INFO_S *pSInfo;
    OSA_MbxHndl  *pMbxHndl = (OSA_MbxHndl *)prm;
    OSA_MsgHndl *pMsg;
    APP_AUDIO_PLAYBACK_DATA *pPlaybackPrm;
    UInt32 timeTotal;
    UInt32 timeLast;
    UInt32 playbackTime = 0;
    char                  threadName[128];

    snprintf(threadName,sizeof(threadName),"%s_%x",__func__,0);
    threadName[sizeof(threadName) - 1] = 0;
    OSA_printTID(threadName);

    gAppPlaybackThreadExitFlag = FALSE;
    memset(&playStats, 0, sizeof(playStats));
    timeTotal = timeLast = 0;
    App_initChTime(&timeLast, 1);

    if (pMbxHndl == NULL)
    {
        printf(ERR_PLAY " MBX is NULL Exiting...");
        gAppPlaybackThreadExitFlag = TRUE;
        return 0;
    }

    if (gCapInfo.enablePlayback == 1)
    {
        OSA_semWait(&gStartPlayNotifySem, OSA_TIMEOUT_FOREVER);
        if (Audio_initRenderDevice(AUDIO_CHANNELS_SUPPORTED, gSamplingHz, gCapInfo.playbackDevId) < 0)
        {
            printf(ERR_PLAY " Audio playback device init failed...");
            while ( gAppPlaybackThreadActive == TRUE)
                sleep(1);
        }
        else
        {
            playAudioFlag = 1;
        }
    }
    
    /* Clear the message box - drop all messages */
    pMsg = NULL;
    status = OSA_mbxCheckMsg(pMbxHndl, &pMsg);
    while (status == OSA_SOK)
    {
        OSA_mbxAckOrFreeMsg(pMsg, OSA_SOK);
    }

    while ( gAppPlaybackThreadActive == TRUE)
    {
        if (gCapInfo.playbackDevId == AUDIO_PLAYBACK_DEVICE_ENUM_HDMI) /* HDMI Specific check only */
        {
            /* 
                         1. Check if change in connection status - 
                         1.1. If disconnected - deinitialize audio playback
                         1.2. If connected - check if streaming, HDMI and connected
                         1.2.1 Initialize audio player if not initialized
                         1.2.2 Prep to play
                         1.2. If DVI stop - deInitAudio player
                         3. If disconnected stop - deInitAudio player
                         */
            if (gIsTvConnected != Demo_isSinkDetected())
            {
                playAudioFlag = 0;
                /* Change in conncetion status - check for plug in / plug out */
                gIsTvConnected = Demo_isSinkDetected();
                if (gIsTvConnected == TRUE)
                {
                    /* Connected */
                    /* Check if TV can support audio */
                    pSInfo = NULL;
                    /* Check if it is TV and we can play out */
                    if (Demo_getSinkDetails(&pSInfo) != 0)
                    {
                        printf(ERR_PLAY " Audio could not get TV details...");
                        printf (ERR_PLAY" Audio playback is stopped...");
                        waitToExit();
                    }
                    if (pSInfo == NULL)
                    {
                        printf(ERR_PLAY " Audio could not get TV details...");
                        printf (ERR_PLAY" Audio playback is stopped...");
                        waitToExit();
                    }
                    if ((pSInfo->isHdmi == TRUE) && 
                        (pSInfo->isSinkConnected == TRUE) && 
                        (pSInfo->isStreaming == TRUE))
                    {
                        if (gCapInfo.enablePlayback == TRUE)
                        {
                            playAudioFlag = 1;
                        }
                    }
                    else
                    {
                        /* Error */
                        printf(WARN_PLAY " Audio playback stopped due to");
                        if (pSInfo->isHdmi == FALSE)
                        {
                            printf (" Sink is DVI\n");
                        } 
                        else if (pSInfo->isStreaming == TRUE)
                        {
                            printf (" Video not streaming \n");
                            printf (" Should re-start demo !!! \n");
                            waitToExit();
                        }
                        else
                        {
                            printf(WARN_PLAY " TV disconnected - Waiting for re-connect");
                            gIsTvConnected = FALSE;
                        }
                    }
                    if ((play_handle == NULL) && (playAudioFlag == 1))
                    {
                        if (Audio_initRenderDevice(AUDIO_CHANNELS_SUPPORTED, 
                                gSamplingHz, gCapInfo.playbackDevId) < 0)
                        {
                            playAudioFlag = 0;
                            printf(ERR_PLAY " Audio playback device init failed...");
                            waitToExit();
                        }
                    }
                }
                else
                {
                    /* Disconnected */
                    playAudioFlag = 0;
                    if (play_handle != NULL)
                    {
                        Audio_deInitRenderDevice();
                    }
                
                }
            }
            /* No change in conncetion - no issues */
        }
        /* Check for messages - if so act */
        status = OSA_mbxCheckMsg(pMbxHndl, &pMsg);
        if (status == OSA_SOK)
        {
            switch (OSA_msgGetCmd(pMsg))
            {
                case DEMO_SUSPEND_AUDIO_PLAYBACK:
                    playAudioFlag = 0;
                    if (play_handle != NULL)
                    {
                        Audio_deInitRenderDevice();
                    }
                    else
                    {
                        printf(WARN_PLAY " Playback is already suspended");
                    }
                break;
                
                case DEMO_RESUME_AUDIO_PLAYBACK:
                    if (play_handle != NULL)
                    {
                        printf(WARN_PLAY " Audio play back in progress...");
                        printf (" Suspend audio play back first.");
                        break;
                    }
                    if (pSInfo->isHdmi == FALSE)
                    {
                        playAudioFlag = 0;
                        printf(WARN_PLAY " Connected sink is DVI");
                        printf(WARN_PLAY " Cannot play audio on DVI....");
                        break;
                    } 
                    if (Audio_initRenderDevice(AUDIO_CHANNELS_SUPPORTED, 
                                gSamplingHz, gCapInfo.playbackDevId) < 0)
                    {
                        playAudioFlag = 0;
                        printf (ERR_PLAY" Audio playback device init failed...");
                        printf (ERR_PLAY" Could not resume audio playback");
                        waitToExit();
                    }
                    playAudioFlag = 1;
                break;
                default:
                    printf (WARN_PLAY" Warning playback thread received");
                    printf (" unhandled command. Ignoring the same");
                break;
            }
            OSA_mbxAckOrFreeMsg(pMsg, OSA_SOK);
        }
        if (playAudioFlag)
        {            
        	pPlaybackPrm = NULL;
			status = OSA_queGet(&gChAudioPlaybackBufQue, (Int32 *)(&pPlaybackPrm), OSA_TIMEOUT_NONE);
            if (pPlaybackPrm) 
            {
                if (pPlaybackPrm->dataPrm.captureDataBuf)
                {
                    playStats.curPlaybackBuf = (UInt32) pPlaybackPrm->dataPrm.captureDataBuf;
                    playStats.samplesPlayed += Audio_playBuffer(pPlaybackPrm->dataPrm.captureDataBuf, pPlaybackPrm->dataPrm.captureDataSize);
    				Acap_setConsumedData(pPlaybackPrm->chNum, &pPlaybackPrm->dataPrm);
                }
                /* return the structure */
                status = OSA_quePut(&gChAudioPlaybackEmptyBufQue, (Int32)pPlaybackPrm, OSA_TIMEOUT_NONE);
                OSA_assert(status == OSA_SOK);
            }
            else
            {
                usleep(20);
            }
        }
        else
        {
            usleep(20);
        }
        App_updateChTime(&timeTotal, &timeLast);
        playbackTime += (timeTotal / 1000);
        if (timeTotal > 10000)
        {
            if (playStats.samplesPlayed)
                printf (INFO_PLAY "(PlaybackTsk - %d secs) samples played: %d, ErrCnt: %d, LastErr: %s, InsideAlsa: %d, lastBuf: %X\n", 
                        playbackTime,
                        playStats.samplesPlayed, 
                        playStats.errorCnt,
                        snd_strerror(playStats.lastError),
                        playStats.insideAlsaWrite,
                        (int) playStats.curPlaybackBuf
                    );  
            /* reset stats */
            timeTotal = 0;
            timeLast = OSA_getCurTimeInMsec();
        }
    }
    Audio_deInitRenderDevice();
    printf (INFO_PLAY " Exiting audio play task............");
    gAppPlaybackThreadExitFlag = TRUE;
    return 0;
}

static Void *App_captureTaskFxn(Void * prm)
{
    ACAP_PARAMS_S acapParams;
    ACAP_GET_DATA_PARAMS_S dataPrm;
    APP_AUDIO_PLAYBACK_DATA *pPlaybackPrm;
    FILE *fp[ACAP_CHANNELS_MAX];
    UInt32 bytesWritten[ACAP_CHANNELS_MAX];
    UInt32 samplesCaptured[ACAP_CHANNELS_MAX];
    UInt32 timeTotal[ACAP_CHANNELS_MAX];
    UInt32 timeLast[ACAP_CHANNELS_MAX];
    UInt8 i;
    Int8  file[250];
    UInt8 *buf = NULL;
    Int32 bytesAvailable;
    Int8  notifyPlayback = 1, bufferedNo = 0;
	Int32 status;
    char                  threadName[128];

    snprintf(threadName,sizeof(threadName),"%s_%x",__func__,0);
    threadName[sizeof(threadName) - 1] = 0;
    OSA_printTID(threadName);

    gAppCaptureThreadExitFlag = FALSE;

    OSA_semWait(&gStartCaptureNotifySem, OSA_TIMEOUT_FOREVER);

    Acap_params_init(&acapParams);

    memset(gCaptureBuf, 0, sizeof(gCaptureBuf));
    memset(gEncodeBuf, 0, sizeof(gEncodeBuf));
    memset(&dataPrm, 0, sizeof(dataPrm));
    memset(&bytesWritten, 0, sizeof(bytesWritten));
    memset(&samplesCaptured, 0, sizeof(samplesCaptured));

    for(i=0; i<ACAP_CHANNELS_MAX; i++)
    {
        fp[i] = NULL;
    }

    acapParams.enableTVP5158 = TRUE;
    acapParams.numChannels = gDemo_info.maxVcapChannels;

    printf(INFO_CAPT " Capture Buf Size %d, Enc Buffer Size %d", 
            acapParams.chPrm[0].captureBuf.dataBufSize*acapParams.sampleLen,
            acapParams.chPrm[0].encodeBuf.dataBufSize);

    for(i=0; i<acapParams.numChannels; i++)
    {
        gCaptureBuf[i] = NULL;
        gEncodeBuf[i] = NULL;

        timeTotal[i] = timeLast[i] = 0;
        samplesCaptured[i] = 0;

        if (i < gCapInfo.numEncodeChannels)
        {
            acapParams.chPrm[i].enableEncode = TRUE;
        }
        if (acapParams.chPrm[i].enableEncode == TRUE)
        {
            acapParams.chPrm[i].encodeParam.bitRate = ENCODE_BITRATE;
            acapParams.chPrm[i].encodeParam.encoderType = ENCODE_TYPE;

            /* Allocate in shared region for DSP Encode */
            acapParams.chPrm[i].captureBuf.dataBuf = 
                                    gCaptureBuf[i] = 
                                    App_allocBuf(acapParams.chPrm[i].captureBuf.dataBufSize*acapParams.sampleLen, ENCODE_BUFFER_FROM_SHARED_REGION);
            if (gCaptureBuf[i] == NULL)
            {
                printf (ERR_CAPT " Capture buffer allocation failed...");
                return NULL;
            }

            acapParams.chPrm[i].encodeBuf.dataBuf = gEncodeBuf[i] = App_allocBuf(acapParams.chPrm[i].encodeBuf.dataBufSize, ENCODE_BUFFER_FROM_SHARED_REGION);
            if (gEncodeBuf[i] == NULL)
            {
                printf (ERR_CAPT " Encode buffer allocation failed...");
                return NULL;
            }
            /* numberOfChannels & sampleRate will be assigned internally */
        }
        else if (i < ACAP_CHANNELS_MAX)
        {
            acapParams.chPrm[i].enableEncode = FALSE;
            acapParams.chPrm[i].captureBuf.dataBuf = 
                            gCaptureBuf[i] = 
                            App_allocBuf(acapParams.chPrm[i].captureBuf.dataBufSize*acapParams.sampleLen, FALSE);
        }
    }

    if (Acap_init(&acapParams) != 0)
    {
        printf (ERR_CAPT " Capture start failed ");
        goto CAP_EXIT;
    }

    Acap_start();

    gSamplingHz = acapParams.sampleRate;

    App_initChTime(timeLast, acapParams.numChannels);

    for(i=0; i<acapParams.numChannels; i++)
    {
        fp[i] = NULL;

        if (acapParams.chPrm[i].enableEncode == TRUE)
        {
            if (ENCODE_TYPE == AUDIO_CODEC_TYPE_AAC_LC)
                sprintf (file, "%s/ch%02d.aac", gCapInfo.outFilePath, i);
            else
                sprintf (file, "%s/ch%02d.ulaw", gCapInfo.outFilePath,i);
        }
        else
        {
            sprintf (file, "%s/ch%02d.pcm", gCapInfo.outFilePath, i);
        }
        fp[i] = fopen (file, "wb");
        if (fp[i] == NULL)
        {
            printf (ERR_CAPT " fopen failed for %s !!!!", file);
        }    
        bytesWritten[i] = 0;

        if (fp[i])
            setvbuf(fp[i],
                  NULL,
                  _IOFBF,
                  AUDIO_BITS_FWRITE_BUFFER_SIZE);
    }
   
    while (gAppCaptureThreadActive == TRUE)
    {
        if (gAppCaptureThreadActive == FALSE)
            break;

        for (i=0; i<acapParams.numChannels; i++)
        {
            if (gAppCaptureThreadActive == FALSE)
                break;

            Acap_getData(i, &dataPrm);

            if ((dataPrm.captureDataSize == 0) && (dataPrm.encodeDataSize == 0))
            {
                usleep(100);
                continue;
            }
            else
            {
                samplesCaptured[i] += dataPrm.captureDataSize;
                App_updateChTime(&timeTotal[i], &timeLast[i]);

                /******* send for playback  ***********/
                if (
                     (gCapInfo.enablePlayback == TRUE) && 
                     (gCapInfo.playbackChNum == i) 
                    )
                {
                    bufferedNo++;
                    if ((notifyPlayback == 1) && (bufferedNo >= AUDIO_BUFFERING_BEFORE_PLAY))
                    {
                        notifyPlayback = 0;
                        OSA_semSignal(&gStartPlayNotifySem);
                        printf (INFO_CAPT " Notify done");
                    }

                    pPlaybackPrm = NULL;                    
                    status = OSA_queGet(&gChAudioPlaybackEmptyBufQue, (Int32 *)(&pPlaybackPrm), OSA_TIMEOUT_NONE);
                    OSA_assert(status == OSA_SOK);
                    if (pPlaybackPrm)
                    {
                        pPlaybackPrm->dataPrm.captureDataBuf = dataPrm.captureDataBuf;
                        pPlaybackPrm->dataPrm.captureDataSize = dataPrm.captureDataSize*acapParams.sampleLen;
                        pPlaybackPrm->chNum = i;
                        status = OSA_quePut(&gChAudioPlaybackBufQue, (Int32)pPlaybackPrm, OSA_TIMEOUT_NONE);
                        OSA_assert(status == OSA_SOK);
                    }
                }

                /********* Write to file - currently writing only encoded channels. all channels can be written with a good buffering mechanism **************/
                if (acapParams.chPrm[i].enableEncode == TRUE)
                {
                    bytesAvailable = dataPrm.encodeDataSize;
                    buf = dataPrm.encodeDataBuf;
                }
                else
                {
                    bytesAvailable = dataPrm.captureDataSize*acapParams.sampleLen;
                    buf = dataPrm.captureDataBuf;
                }

                if (fp[i] && bytesAvailable)
                {
                    /* Write only encoded channels to avoid data miss */
#ifdef   AUDIO_FWRITE_ONLY_ENCODED_CHANNELS
                    if (acapParams.chPrm[i].enableEncode == TRUE) 
#endif
                    {
                        bytesWritten[i] += fwrite(buf, bytesAvailable, 1, fp[i]);
                    }
                }
                /******************************/

                if (timeTotal[i] > 10000)
                {
                    if (i == 0) /* print sample rate only for ch 0 */
                    {
                        App_printChStats();
                        printf (INFO_CAPT " Ch %d, Total Time: %d, Samples: %d, Sample Rate: %d KHz\n",
                            i, timeTotal[i], samplesCaptured[i], (samplesCaptured[i] / timeTotal[i]));
                    }
                    /* reset stats */
                    timeTotal[i] = samplesCaptured[i] = 0;
                    timeLast[i] = OSA_getCurTimeInMsec();
                }

                if ((gCapInfo.enablePlayback == FALSE) 
                    ||
                    (
                     (gCapInfo.enablePlayback == TRUE) && 
                     (gCapInfo.playbackChNum != i) 
                    ))
                {
					/* Return buffer only for non playback channels. Playback ch data will be returned after playing */
					Acap_setConsumedData(i, &dataPrm);	
                }	
            }
        }
    }

CAP_EXIT:
    gAppCaptureThreadActive = FALSE;
    for(i=0; i<acapParams.numChannels; i++)
    {
        if (fp[i])  
            fclose(fp[i]);
        if (acapParams.chPrm[i].enableEncode == TRUE)
        {
            if (gEncodeBuf[i])
                App_freeBuf(gEncodeBuf[i], acapParams.chPrm[i].encodeBuf.dataBufSize, ENCODE_BUFFER_FROM_SHARED_REGION);
            if (gCaptureBuf[i])
                App_freeBuf(gCaptureBuf[i], (acapParams.chPrm[i].captureBuf.dataBufSize*acapParams.sampleLen), ENCODE_BUFFER_FROM_SHARED_REGION);
        }
        else
        {
            if (gCaptureBuf[i])
                App_freeBuf(gCaptureBuf[i], (acapParams.chPrm[i].captureBuf.dataBufSize*acapParams.sampleLen), FALSE);
        }
    }
    printf (INFO_CAPT " Capture APP Task Exitting...\n");
    gAppCaptureThreadExitFlag = TRUE;
    return NULL;
}

Void Demo_initAudioSystem(Void)
{
    Audio_systemInit();
}

Void Demo_deInitAudioSystem(Void)
{
    Audio_systemDeInit();
}

Bool Demo_changePlaybackChannel (Void)
{
    /* CAUTION: A more clean up way to be done to ensure that channel buffers queued up are 
         * given back to capture so that buffers of a specific channel dont get into the queue of the new
         * playback channel. Right now, playback channel change is done in async way
         */
    if (gCapInfo.enablePlayback == 1)
    {
        char ch;
        ch = Demo_getIntValue("AUDIO: Enter Playback Ch Number - ", 0, gDemo_info.maxVcapChannels - 1, 0);
        gCapInfo.playbackChNum = ch;
    }
    else
    {
        printf (WARN_PLAY " Playback not active... Stop & Start capture demo again in playback mode....");
    }
    return TRUE;
}

Bool Demo_signalCaptureThread (void)
{
    if (Demo_IsCaptureActive())
        OSA_semSignal(&gStartCaptureNotifySem);

    return TRUE;
}

Bool Demo_startAudioCaptureSystem (Void)
{
    Bool ret = FALSE;
    char ch;

    Int32 status;

    gPlaybackMbxHndl = NULL;

#ifndef    USE_DEFAULT_CONFIG_FOR_AUDIO
    gCapInfo.numEncodeChannels = Demo_getIntValue("AUDIO: Enter number of channels to be encoded - ", 0, gDemo_info.maxVcapChannels, 4);
    if (gCapInfo.numEncodeChannels)
        printf ("\r 0 to %d channels will be encoded...\n", gCapInfo.numEncodeChannels - 1);
    VcapVencVdecVdis_ipcBitsFileWritePath(gCapInfo.outFilePath);

    printf("AUDIO: Do you enable playback <Y/N>: ");
    ch = Demo_getChar();
    if (ch == 'y' || ch == 'Y')
    {
        gCapInfo.enablePlayback = 1;
        gCapInfo.playbackEncodedFlag = 0;
        gCapInfo.playbackChNum = 0;
        if (gCapInfo.playbackChNum < gCapInfo.numEncodeChannels)
        {
            gCapInfo.playbackEncodedFlag = 1;
        }
        gCapInfo.playbackDevId = Demo_getIntValue("Playback Device 0-AIC3x 1-HDMI Out", AUDIO_PLAYBACK_DEVICE_ENUM_AIC, AUDIO_PLAYBACK_DEVICE_ENUM_HDMI, AUDIO_PLAYBACK_DEVICE_ENUM_HDMI);
        printf("\r\nAUDIO: Playback enabled for Ch %d on %s device\n",
                gCapInfo.playbackChNum, 
                gCapInfo.playbackDevId == AUDIO_PLAYBACK_DEVICE_ENUM_AIC ? "AIC" : "HDMI"
                );
    }
    else
    {
        gCapInfo.enablePlayback = 0;
    }
#else
    ch = 0;
    strcpy (gCapInfo.outFilePath, AUDIO_DEFAULT_FILE_WRITE_PATH);
    gCapInfo.numEncodeChannels = AUDIO_DEFAULT_NUM_ENCODE_CHANNELS;
    gCapInfo.enablePlayback = 1;
    gCapInfo.playbackEncodedFlag = 1;
    gCapInfo.playbackChNum = AUDIO_DEFAULT_PLAYBACK_CHANNEL;
    gCapInfo.playbackDevId = AUDIO_DEFAULT_PLAYBACK_DEVICE;
#endif
    status = OSA_semCreate(&gStartCaptureNotifySem,
                    1, 
                    0);
    if (status != 0)
    {
        printf (ERR_CAPT " App playback thread create failed...");
        return ret;
    }

    status = OSA_thrCreate(&gAppCaptureThread,
                  App_captureTaskFxn,
                  OSA_THR_PRI_MAX, 
                  AUDIO_TSK_STACK_SIZE, 
                  NULL);

    if (status != 0)
    {
        printf (ERR_CAPT " App capture thread create failed...");
        return ret;
    }
    gAppCaptureThreadActive = TRUE;
    ret = TRUE;


    if (gCapInfo.enablePlayback == 1)
    {
        status = OSA_semCreate(&gStartPlayNotifySem,
                        1, 
                        0);
        if (status != OSA_SOK)
        {
            gCapInfo.enablePlayback = FALSE;
        }
        else
        {
            gAppPlaybackThreadActive = TRUE;
            gPlaybackMbxHndl = &gPlaybackMbx;
            /* Create message box used to suspend / resume audio */
            status = OSA_mbxCreate(gPlaybackMbxHndl);
            status |= OSA_mbxCreate(&gPlaybackSuspendMbx);
            status |= OSA_mbxCreate(&gPlaybackResumeMbx);
            if (status != OSA_SOK)
            {
                printf (ERR_CAPT " App playback MBX create failed...");
            }
            else
            {
                status = OSA_thrCreate(&gAppPlaybackThread,
                              App_playbackTaskFxn,
                              OSA_THR_PRI_MAX, // AUDIO_TSK_PRI, 
                              AUDIO_TSK_STACK_SIZE, 
                              (void *)gPlaybackMbxHndl);
				if (status == OSA_SOK)
				{
                    Int32 bufNo;

					status = OSA_queCreate(&gChAudioPlaybackBufQue, AUDIO_MAX_CH_BUFFERS);
					OSA_assert(status == OSA_SOK);
					status = OSA_queCreate(&gChAudioPlaybackEmptyBufQue, AUDIO_MAX_CH_BUFFERS);
					OSA_assert(status == OSA_SOK);
            
                    /* Insert the empty data params */
                    for(bufNo=0; bufNo<AUDIO_MAX_CH_BUFFERS; bufNo++)
                        status = OSA_quePut(&gChAudioPlaybackEmptyBufQue, (Int32)&gChAudioDataParams[bufNo], OSA_TIMEOUT_NONE);
                    OSA_assert(status == OSA_SOK);
				}
            }
        }
    }
    if (status != 0)
    {
        gAppPlaybackThreadActive = FALSE;
        printf (ERR_CAPT " App playback thread create failed...");
    }
    printf ("\r\n\n");
    return ret;
}

Bool Demo_stopAudioCaptureSystem (Bool userOpt)
{
    Bool ret = FALSE;

    if (gAppCaptureThreadActive == TRUE)
    {
        char ch = 'Y';

        if (userOpt == TRUE)
        {
            printf("\r\n\n\nAUDIO: Stop Capture[Encode] <Y/N>: " );
            ch = Demo_getChar();
        }

        if (ch == 'y' || ch == 'Y')
        {
            Audio_printChStats(gCapInfo.playbackChNum);
 
            /* Stop playback first to clear up capture buffers */
            if (gCapInfo.enablePlayback == TRUE)
            {
                gAppPlaybackThreadActive = FALSE;
                playAudioFlag = 0;

                /* write some data to unlock the pipe */
                OSA_semSignal(&gStartPlayNotifySem);

                while (gAppPlaybackThreadExitFlag == FALSE)
                {
                    printf (INFO_PLAY " Waiting for playback task to exit");
                    OSA_waitMsecs(1000);
                }

                OSA_mbxDelete(gPlaybackMbxHndl);
                OSA_mbxDelete(&gPlaybackSuspendMbx);
                OSA_mbxDelete(&gPlaybackResumeMbx);
                OSA_thrDelete(&gAppPlaybackThread);
                OSA_semDelete(&gStartPlayNotifySem);

                printf (INFO_PLAY " Playback stopped");
            }

            gAppCaptureThreadActive = FALSE;
            playAudioFlag = 0;
            OSA_semSignal(&gStartCaptureNotifySem);

            /* 
                        * Stop Capture to unblock the capture thread waiting on Acap_getData.
                        * This is mandatory as sometimes Acap_getData() might not get unblocked 
                        * if no data is ever made available from driver. 
                        */            
            Acap_stop();

            while (gAppCaptureThreadExitFlag == FALSE)
            {
                printf (INFO_CAPT " Waiting for capture task to exit");
                OSA_waitMsecs(1000);
            }
            
            printf (INFO_CAPT " Deleting capture thread....");
            OSA_thrDelete(&gAppCaptureThread);
            printf (INFO_CAPT " Capture stopped");
            ret = TRUE;
            OSA_semDelete(&gStartCaptureNotifySem);
            if (gCapInfo.enablePlayback == TRUE)
            {
                OSA_queDelete(&gChAudioPlaybackBufQue);
                OSA_queDelete(&gChAudioPlaybackEmptyBufQue);
            }
            /* Deinit capture after app capture thread exitted */
            Acap_deInit();
        }
    }
    printf ("\r\n\n");
    return ret;
}


Bool Demo_IsCaptureActive(Void)
{
    return gAppCaptureThreadActive;
}

Bool Demo_SuspendAudioPlayBackOnHdmi(void)
{
    Int status;

    if (gPlaybackMbxHndl == NULL)
    {
        return FALSE;
    }

    /* Play back should be active and should be via HDMI only */
    if ((gAppPlaybackThreadActive == TRUE) && 
        (gCapInfo.enablePlayback == TRUE) && 
        (gCapInfo.playbackDevId == AUDIO_PLAYBACK_DEVICE_ENUM_HDMI))
    {
        printf (INFO_PLAY " Stopping play back on HDMI...");
        status = OSA_mbxSendMsg(gPlaybackMbxHndl, &gPlaybackSuspendMbx,  
                        DEMO_SUSPEND_AUDIO_PLAYBACK, NULL, OSA_MBX_WAIT_ACK);
        if (status != OSA_SOK)
        {
            printf (ERR_PLAY " Could not send SUSPEND command");
        }
        else
        {
            printf("[Done] \n");
        }
    }
    else
    {
        printf (ERR_PLAY " Could not send SUSPEND command");
    }

    return TRUE;
}


Bool Demo_ResumeAudioPlayBackOnHdmi(void)
{
    Int status;

    if (gPlaybackMbxHndl == NULL)
    {
        return FALSE;
    }

    /* Play back should be active and should be via HDMI only */
    if ((gAppPlaybackThreadActive == TRUE) && 
        (gCapInfo.enablePlayback == TRUE) && 
        (gCapInfo.playbackDevId == AUDIO_PLAYBACK_DEVICE_ENUM_HDMI))
    {
        printf (INFO_PLAY " Re-Starting play back on HDMI...");
        status = OSA_mbxSendMsg(gPlaybackMbxHndl, &gPlaybackResumeMbx,  
                            DEMO_RESUME_AUDIO_PLAYBACK, NULL, OSA_MBX_WAIT_ACK);
        if (status != OSA_SOK)
        {
            printf (ERR_PLAY " Could not send RESUME command");
        }
        else
        {
            printf("[Done] \n");
        }
    }
    else
    {
        printf (ERR_PLAY " Could not send RESUME command");
    }

    return TRUE;
}
