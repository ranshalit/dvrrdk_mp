
#include <demo.h>
#include <demo_scd_bits_wr.h>
#include <demo_audio.h>
#include <limits.h>

char gDemo_displaySettingsMenu[] = {
    "\r\n ====================="
    "\r\n Display Settings Menu"
    "\r\n ====================="
    "\r\n"
    "\r\n 1: Disable channel"
    "\r\n 2: Enable  channel"
    "\r\n 3: Switch Layout"
    "\r\n 4: Switch Channels"
    "\r\n 5: Change resolution"
    "\r\n 6: Switch Queue(ONLY FOR SD Display)"
    "\r\n 8: Switch SDTV channel (ONLY for progressive demo)"
    "\r\n 9: 2x digital zoom in top left"
    "\r\n a: 2x digital zoom in center"
    "\r\n b: Avsync Pause "
    "\r\n c: Avsync Timescale Play "
    "\r\n d: Avsync Step Fwd "
    "\r\n e: Avsync Normal Play "
    "\r\n f: Avsync Seek Play "
    "\r\n g: Avsync Reset Player timer "
    "\r\n h: Avsync Set Player State Play "
    "\r\n i: Avsync Scan Play "
    "\r\n j: SwMS flush frames "
    "\r\n k: Run Grpx fbdev demo"
    "\r\n l: Change Brightness/Contrast/Saturation/Hue"
    "\r\n m: Print Detailed Timings Info"
    "\r\n n: Enable HMP crop (ONLY for decode->display demo)"
    "\r\n o: Disable HMP crop (ONLY for decode->display demo)"
    "\r\n q: Display current channel map (ONLY for decode->display demo)"
    "\r\n r: Change channel map (ONLY for decode->display demo)"
#ifdef TI_816X_BUILD
    "\r\n s: Change EDE setting"
    "\r\n u: Change CPROC setting"
#endif
    "\r\n t: Get Video Time Stamp Info"
    "\r\n"
    "\r\n p: Previous Menu"
    "\r\n"
    "\r\n Enter Choice: "
};

char gDemo_displayLayoutMenu[] = {
    "\r\n ====================="
    "\r\n Select Display Layout"
    "\r\n ====================="
    "\r\n"
    "\r\n 1: 1x1 CH"
    "\r\n 2: 2x2 CH"
    "\r\n 3: 3x3 CH"
    "\r\n 4: 4x4 CH"
    "\r\n 5: 2x2 CH + 4CH"
    "\r\n 6: 1CH + 5CH"
    "\r\n 7: 1CH + 7CH"
    "\r\n 8: 1CH + 2CH PIP "
};

char gDemo_displayLayoutMenuDecDemoOnly[] = {
    "\r\n 9: 4x5 CH"
#ifdef TI_816X_BUILD
    "\r\n a: 5x5 CH"
    "\r\n b: 5x6 CH"
    "\r\n c: 6x6 CH"
#endif
};

char gDemo_displayLayoutMenuEnd[] = {
    "\r\n"
    "\r\n Enter Choice: "
};

char gDemo_displayMenu[] = {
    "\r\n ====================="
    "\r\n Select Display"
    "\r\n ====================="
    "\r\n"
    "\r\n 1: ON-Chip HDMI"
    "\r\n 2: VGA / HDCOMP "
    "\r\n 3: OFF-Chip HDMI"
    "\r\n 4: SD "
    "\r\n"
    "\r\n Enter Choice: "

};

char gDemo_ResolutionMenu[] = {
    "\r\n ====================="
    "\r\n Select Display"
    "\r\n ====================="
    "\r\n"
    "\r\n 1: 1080P60"
    "\r\n 2: 720P60"
    "\r\n 3: XGA"
    "\r\n 4: SXGA"
    "\r\n 5: NTSC"
    "\r\n 6: PAL"
    "\r\n 7: 1080P50"
    "\r\n 8: 640X480P"
    "\r\n 9: 800X480P (ranran)"
    "\r\n"
    "\r\n Enter Choice: "

};
char gDemo_displayHDDemoChanMenu[] = {
    "\r\n ********                 Channel Mapping                 *******"
    "\r\n Channel 0 - Physical Channel 0   Channel 1 - Physical Channel  4"
    "\r\n Channel 2 - Physical Channel 8   Channel 3 - Physical Channel 12"
    "\r\n"
};

char gDemo_dispColorControlMenu[] = {
    "\r\n ====================="
    "\r\n Select Control"
    "\r\n ====================="
    "\r\n"
    "\r\n 1: Brightness"
    "\r\n 2: Contrast"
    "\r\n 3: Saturation"
    "\r\n 4: Hue"
};

static int layoutId = DEMO_LAYOUT_MODE_7CH_1CH; // Have this static to use layoutId in Demo_displayChangeFpsForLayout() when channel remap alone happens

/*
  * This API is to reset the BIG LIVE channel(s) in a old layout to 30 / 25 fps
  */
Void Demo_displayResetFps(VDIS_MOSAIC_S *vdMosaicParam, UInt32 layoutId)
    {
       /* Set outputFPS for 814x usecases generating 60fps for channels shown as bigger window in some layouts */
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
        Int32 currentFrameRate = Demo_swMsGetOutputFPS(vdMosaicParam);
        if (currentFrameRate == 50)
            currentFrameRate = 25;

        if (currentFrameRate == 60)
            currentFrameRate = 30;

        switch (layoutId)
        {
#if defined(TI_8107_BUILD)
            /* Stream ID 0 refers to live channel for Vcap_setFrameRate */
            case DEMO_LAYOUT_MODE_4CH:
                printf ("4CH Layout, Resetting FPS of CH%d %d %d %d to %d/%dfps\n",
                        vdMosaicParam->chnMap[0],
                        vdMosaicParam->chnMap[1],
                        vdMosaicParam->chnMap[2],
                        vdMosaicParam->chnMap[3],
                        currentFrameRate, currentFrameRate
                    );
                Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[1], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[2], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[3], 0, currentFrameRate, currentFrameRate);
                break;
#else
            /* Stream ID 0 refers to live channel for Vcap_setFrameRate */
            case DEMO_LAYOUT_MODE_4CH:
                printf ("4CH Layout, Resetting FPS of CH%d %d %d %d to %d/%dfps\n",
                        vdMosaicParam->chnMap[0],
                        vdMosaicParam->chnMap[1],
                        vdMosaicParam->chnMap[2],
                        vdMosaicParam->chnMap[3],
                        currentFrameRate*2, currentFrameRate
                    );
                Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate*2, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[1], 0, currentFrameRate*2, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[2], 0, currentFrameRate*2, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[3], 0, currentFrameRate*2, currentFrameRate);
                break;
#endif

            case DEMO_LAYOUT_MODE_1CH:
            case DEMO_LAYOUT_MODE_6CH:
            case DEMO_LAYOUT_MODE_7CH_1CH:
                printf ("1CH Layout, Resetting FPS of CH%d to %d/%dfps\n",
                        vdMosaicParam->chnMap[0],
                        currentFrameRate*2, currentFrameRate
                    );

                Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate*2, currentFrameRate);
                break;
        }
#else
    Int32 i=0;
    for (i=0; i<Vcap_getNumChannels(); i++)
        Vcap_setFrameRate(i, 0, 60, 60);
#endif
    }


/*
  * This API is to set have the LIVE channel(s) shown in bigger window <in some layouts> to be rendered
  *  at 60 / 50 fps for some 814x usecase
  */
Void Demo_displayChangeFpsForLayout (VDIS_MOSAIC_S *vdMosaicParam, UInt32 layoutId)
{
   /* Set outputFPS for 814x usecases generating 60fps for channels shown as bigger window in some layouts */
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
    VSYS_PARAMS_S sysInfo;
    Int32 i, currentFrameRate = Demo_swMsGetOutputFPS(vdMosaicParam);

    Vsys_getContext(&sysInfo);

    if (currentFrameRate == 50)
        currentFrameRate = 25;

    if (currentFrameRate == 60)
        currentFrameRate = 30;

    switch (layoutId)
    {
        /* Stream ID 0 refers to live channel for Vcap_setFrameRate */
        case DEMO_LAYOUT_MODE_4CH:
            printf ("4CH Layout, Setting FPS of CH%d %d %d %d to %d/%dfps\n",
                    vdMosaicParam->chnMap[0],
                    vdMosaicParam->chnMap[1],
                    vdMosaicParam->chnMap[2],
                    vdMosaicParam->chnMap[3],
                    currentFrameRate*2, currentFrameRate*2
                );
            /* Reset all capture channels fps */
            for (i=0; i<Vcap_getNumChannels(); i++)
                Vcap_setFrameRate(i, 0, currentFrameRate*2, currentFrameRate);
#ifdef TI_814X_BUILD
            Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate*2, currentFrameRate*2);
            Vcap_setFrameRate(vdMosaicParam->chnMap[1], 0, currentFrameRate*2, currentFrameRate*2);
            Vcap_setFrameRate(vdMosaicParam->chnMap[2], 0, currentFrameRate*2, currentFrameRate*2);
            Vcap_setFrameRate(vdMosaicParam->chnMap[3], 0, currentFrameRate*2, currentFrameRate*2);
            Demo_swMsSetOutputFPS(vdMosaicParam, currentFrameRate*2);
#else /* else of #ifdef TI_814X_BUILD */
            if(sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH)
            {
                Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate*2, currentFrameRate*2);
                Vcap_setFrameRate(vdMosaicParam->chnMap[1], 0, currentFrameRate*2, currentFrameRate*2);
                Vcap_setFrameRate(vdMosaicParam->chnMap[2], 0, currentFrameRate*2, currentFrameRate*2);
                Vcap_setFrameRate(vdMosaicParam->chnMap[3], 0, currentFrameRate*2, currentFrameRate*2);
                Demo_swMsSetOutputFPS(vdMosaicParam, currentFrameRate*2);
            }
            else if ((sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) &&
                     (TRUE == sysInfo.enableOptBwMode))
            {
                /* SWMS output fps is always set to 30fps for progressive demo, so no need to
                   multiply currentFrameRate by 2 */
                Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[1], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[2], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[3], 0, currentFrameRate, currentFrameRate);
                Demo_swMsSetOutputFPS(vdMosaicParam, currentFrameRate);
            }
            else
            {
                Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate*2, currentFrameRate*2);
                Vcap_setFrameRate(vdMosaicParam->chnMap[1], 0, currentFrameRate*2, currentFrameRate*2);
                Vcap_setFrameRate(vdMosaicParam->chnMap[2], 0, currentFrameRate*2, currentFrameRate*2);
                Vcap_setFrameRate(vdMosaicParam->chnMap[3], 0, currentFrameRate*2, currentFrameRate*2);
                Demo_swMsSetOutputFPS(vdMosaicParam, currentFrameRate*2);
            }
#endif /* Endif of #ifdef TI_814X_BUILD */
            break;

#ifdef TI_814X_BUILD
        case DEMO_LAYOUT_MODE_1CH:
        case DEMO_LAYOUT_MODE_6CH:
        case DEMO_LAYOUT_MODE_7CH_1CH:

            printf ("1CH Layout, Setting FPS of CH%d to %d/%dfps\n",
                    vdMosaicParam->chnMap[0],
                    currentFrameRate*2, currentFrameRate*2
                );

            /* Reset all capture channels fps */
            for (i=0; i<Vcap_getNumChannels(); i++)
                Vcap_setFrameRate(i, 0, currentFrameRate*2, currentFrameRate);
            Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate*2, currentFrameRate*2);
            Demo_swMsSetOutputFPS(vdMosaicParam, currentFrameRate*2);
            break;
#else   // TI_8107_BUILD
        /* DM8107 supports 60 fps S/W mosaic only for 1x1 layout */
        case DEMO_LAYOUT_MODE_1CH:
            if ((sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) &&
                (TRUE == sysInfo.enableOptBwMode))
            {
                /* Reset all capture channels fps */
                for (i = 0; i < Vcap_getNumChannels(); i++)
                {
                    Vcap_setFrameRate(i, 0, currentFrameRate, currentFrameRate);
                }
                /* SWMS output fps is always set to 30fps for progressive demo, so no need to
                   multiply currentFrameRate by 2 */
                Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[1], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[2], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[3], 0, currentFrameRate, currentFrameRate);
                Demo_swMsSetOutputFPS(vdMosaicParam, currentFrameRate);
            }
            else
            {
                printf("1CH Layout, Setting FPS of CH%d to %d/%dfps\n",
                        vdMosaicParam->chnMap[0],
                        currentFrameRate * 2,
                        currentFrameRate * 2);

                /* Reset all capture channels fps */
                for (i = 0; i < Vcap_getNumChannels(); i++)
                {
                    Vcap_setFrameRate(i, 0, currentFrameRate * 2, currentFrameRate);
                }
                Vcap_setFrameRate(
                        vdMosaicParam->chnMap[0],
                        0,
                        currentFrameRate * 2,
                        currentFrameRate * 2);
                Demo_swMsSetOutputFPS(vdMosaicParam, currentFrameRate * 2);
            }
            break;
        case DEMO_LAYOUT_MODE_6CH:
        case DEMO_LAYOUT_MODE_7CH_1CH:
            if ((sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) &&
                (TRUE == sysInfo.enableOptBwMode))
            {
                /* Reset all capture channels fps */
                for (i = 0; i < Vcap_getNumChannels(); i++)
                {
                    Vcap_setFrameRate(i, 0, currentFrameRate, currentFrameRate);
                }
                /* SWMS output fps is always set to 30fps for progressive demo, so no need to
                   multiply currentFrameRate by 2 */
                Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[1], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[2], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[3], 0, currentFrameRate, currentFrameRate);
                Demo_swMsSetOutputFPS(vdMosaicParam, currentFrameRate);
            }
            else
            {
                printf("1CH Layout, Setting FPS of CH%d to %d/%dfps\n",
                        vdMosaicParam->chnMap[0],
                        currentFrameRate * 2,
                        currentFrameRate);

                /* Reset all capture channels fps */
                for (i = 0; i < Vcap_getNumChannels(); i++)
                {
                    Vcap_setFrameRate(i, 0, currentFrameRate * 2, currentFrameRate);
                }
                Vcap_setFrameRate(
                        vdMosaicParam->chnMap[0],
                        0,
                        currentFrameRate * 2,
                        currentFrameRate);
                Demo_swMsSetOutputFPS(vdMosaicParam, currentFrameRate);
            }
            break;
#endif

        default:
#ifdef TI_8107_BUILD
            if ((sysInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) &&
                (TRUE == sysInfo.enableOptBwMode))
            {
                /* swMS fps would be have been modified during layout generation; change only Capture frame rate */
                for (i=0; i<Vcap_getNumChannels(); i++)
                    Vcap_setFrameRate(i, 0, currentFrameRate, currentFrameRate);

                /* SWMS output fps is always set to 30fps for progressive demo, so no need to
                   multiply currentFrameRate by 2 */
                Vcap_setFrameRate(vdMosaicParam->chnMap[0], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[1], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[2], 0, currentFrameRate, currentFrameRate);
                Vcap_setFrameRate(vdMosaicParam->chnMap[3], 0, currentFrameRate, currentFrameRate);
                Demo_swMsSetOutputFPS(vdMosaicParam, currentFrameRate);
            }
            else
            {
                printf ("NORMAL Layout, Setting FPS of all channels to %d/%dfps\n",
                        currentFrameRate*2, currentFrameRate
                    );
                /* swMS fps would be have been modified during layout generation; change only Capture frame rate */
                for (i=0; i<Vcap_getNumChannels(); i++)
                    Vcap_setFrameRate(i, 0, currentFrameRate*2, currentFrameRate);
            }
#else
            printf ("NORMAL Layout, Setting FPS of all channels to %d/%dfps\n",
                    currentFrameRate*2, currentFrameRate
                );
            /* swMS fps would be have been modified during layout generation; change only Capture frame rate */
            for (i=0; i<Vcap_getNumChannels(); i++)
                Vcap_setFrameRate(i, 0, currentFrameRate*2, currentFrameRate);
#endif

    }


#else /* TI816x */
    Int32 i;

    for (i=0; i<vdMosaicParam->numberOfWindows; i++)
    {
        VSYS_PARAMS_S sysContextInfo;
        Vsys_getContext(&sysContextInfo);
        if(sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC)
        {
            if((vdMosaicParam->chnMap[i] < Vcap_getNumChannels()) && (vdMosaicParam->useLowCostScaling[i] == FALSE))
                Vcap_setFrameRate(vdMosaicParam->chnMap[i],0,30,30);
        }
        else
            if(vdMosaicParam->chnMap[i] < Vcap_getNumChannels())
                Vcap_setFrameRate(vdMosaicParam->chnMap[i],0,60,30);

    }
#endif
}


int Demo_displaySwitchChn(int devId, int startChId)
{
    UInt32 chMap[VDIS_MOSAIC_WIN_MAX];
    int i;

    for(i=0;i<VDIS_MOSAIC_WIN_MAX;i++)
    {
         if (i < gDemo_info.maxVdisChannels)
            chMap[i] = (i+startChId)%gDemo_info.maxVdisChannels;
         else
            chMap[i] = DEMO_SW_MS_INVALID_ID;

    }
    Vdis_setMosaicChn(devId, chMap);

    /* wait for the info prints to complete */
    OSA_waitMsecs(100);

    return 0;
}

int Demo_displaySwitchQueue(int devId, int queueId)
{
    Vdis_switchActiveQueue(devId,queueId);
    return 0;
}

Void MultiCh_carDVRSwitchDisplayCh(Int displayChId);
int Demo_displaySwitchCarDVRCh(int displayChNum)
{
    MultiCh_carDVRSwitchDisplayCh(displayChNum);
    return 0;
}

int Demo_displaySwitchSDChan(int devId, int chId)
{
    Vdis_switchSDTVChId(devId, chId);
    return 0;
}
int Demo_displaySwitchChannel(int devId, int chId)
{
    Vdis_switchActiveChannel(devId,chId);
    return 0;
}

int Demo_displayChnEnable(int chId, Bool enable)
{
    if(chId >= gDemo_info.maxVdisChannels)
    {
        return -1;
    }

    if(enable)
    {
        Vdis_enableChn(VDIS_DEV_HDMI,chId);
        Vdis_enableChn(VDIS_DEV_HDCOMP,chId);
        Vdis_enableChn(VDIS_DEV_SD,chId);
    }
    else
    {
        Vdis_disableChn(VDIS_DEV_HDMI,chId);
        Vdis_disableChn(VDIS_DEV_HDCOMP,chId);
        Vdis_disableChn(VDIS_DEV_SD,chId);
    }

    /* wait for the info prints to complete */
    OSA_waitMsecs(100);

    return 0;
}

int Demo_displayGetLayoutId(int demoId)
{
    char ch;
    int layoutId = DEMO_LAYOUT_MODE_4CH_4CH;
    Bool done = FALSE;

    while(!done)
    {
        printf(gDemo_displayLayoutMenu);

        if (demoId == DEMO_VDEC_VDIS)
            printf(gDemo_displayLayoutMenuDecDemoOnly);

        printf(gDemo_displayLayoutMenuEnd);

        ch = Demo_getChar();

        done = TRUE;

        switch(ch)
        {
            case '1':
                layoutId = DEMO_LAYOUT_MODE_1CH;
                break;
            case '2':
                layoutId = DEMO_LAYOUT_MODE_4CH;
                break;
            case '3':
                layoutId = DEMO_LAYOUT_MODE_9CH;
                break;
            case '4':
                layoutId = DEMO_LAYOUT_MODE_16CH;
                break;
            case '5':
                layoutId = DEMO_LAYOUT_MODE_4CH_4CH;
                break;
            case '6':
                layoutId = DEMO_LAYOUT_MODE_6CH;
                break;
            case '7':
                layoutId = DEMO_LAYOUT_MODE_7CH_1CH;
                break;
            case '8':
                layoutId = DEMO_LAYOUT_MODE_2CH_PIP;
                break;
            case '9':
                layoutId = DEMO_LAYOUT_MODE_20CH_4X5;
                break;
#if !defined(TI_814X_BUILD) && !defined(TI_8107_BUILD)
            case 'a':
                layoutId = DEMO_LAYOUT_MODE_25CH_5X5;
                break;
            case 'b':
                layoutId = DEMO_LAYOUT_MODE_30CH_5X6;
                break;
            case 'c':
                layoutId = DEMO_LAYOUT_MODE_36CH_6X6;
                break;
#endif
            default:
                done = FALSE;
                break;
        }
    }

    return layoutId;
}

int Demo_displaySetResolution(UInt32 displayId, UInt32 resolution)
{
    VDIS_MOSAIC_S vdisMosaicParams;
    VSYS_PARAMS_S sysContextInfo;
#if USE_FBDEV
    UInt32 outWidth, outHeight;
#endif

    if (Vdis_isSupportedDisplay(displayId))
    {
            Vsys_getContext(&sysContextInfo);

            #if USE_FBDEV
            grpx_stop(displayId);
            #endif

            #if defined(TI_8107_BUILD)
            if (((sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) ||
                 (sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH)) &&
                ((VDIS_DEV_HDMI == displayId) || (VDIS_DEV_SD == displayId) || (VDIS_DEV_HDCOMP == displayId)))
            {
                Vcap_stopWrbkCapt();
            }
            #endif
            #if defined(TI_816X_BUILD)
            if (sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_HD_SD_5CH)
            {
                Vcap_stopWrbkCapt();
            }
            #endif

            Vdis_stopDrv(displayId);
            memset(&vdisMosaicParams, 0, sizeof(VDIS_MOSAIC_S));
            /* Update the resolution that we are changing into */
            Vdis_setResolution(displayId, resolution);

            /* Start with default layout */
            Demo_swMsGenerateLayout(displayId, 0, gDemo_info.maxVdisChannels,
                    layoutId,
                    &vdisMosaicParams, FALSE,
                    gDemo_info.Type,
                    Vdis_getSwMsLayoutResolution(displayId));
            if (sysContextInfo.systemUseCase != VSYS_USECASE_MULTICHN_HD_VCAP_VENC
                    &&
               sysContextInfo.systemUseCase != VSYS_USECASE_MULTICHN_HD_SD_VCAP_VENC
            )
            {
                Demo_displayResetFps(&vdisMosaicParams,layoutId);

                Demo_displayChangeFpsForLayout(&vdisMosaicParams,layoutId);
            }
            #if defined(TI_8107_BUILD)
            if (!((sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC)
                   &&
                ((VDIS_DEV_HDMI == displayId) || (VDIS_DEV_HDCOMP == displayId) ||
                    (VDIS_DEV_SD == displayId))))
            {
                Vdis_setMosaicParams(displayId, &vdisMosaicParams);
            }
            #else
            Vdis_setMosaicParams(displayId, &vdisMosaicParams);
            #endif

            Vdis_startDrv(displayId);

            #if USE_FBDEV
            Demo_swMsGetOutSize(resolution, &outWidth, &outHeight);

            grpx_scale(displayId, 0, 0, outWidth, outHeight);

            grpx_start(displayId);
            #endif

            #if defined(TI_8107_BUILD)
            if (((sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) ||
                 (sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH)) &&
                ((VDIS_DEV_HDMI == displayId) || (VDIS_DEV_SD == displayId) || (VDIS_DEV_HDCOMP == displayId)))
            {
                UInt32 inW, inH, outW, outH;

                Demo_swMsGetOutSize(Vdis_getResolution(VDIS_DEV_HDMI), &inW, &inH);
                Demo_swMsGetOutSize(Vdis_getResolution(VDIS_DEV_SD), &outW, &outH);

                Vcap_setWrbkCaptScParams(inW, inH, outW, outH);

                Vcap_startWrbkCapt();
            }
            /* Get the Demo Id */
            /* Set the Scalar Parameters as per resolution */
            /* Start Write back Driver */
            #endif
            #if defined(TI_816X_BUILD)
            if (sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_HD_SD_5CH)
            {
                UInt32 inW, inH, outW, outH;

                Demo_swMsGetOutSize(Vdis_getResolution(VDIS_DEV_HDMI), &inW, &inH);
                Demo_swMsGetOutSize(Vdis_getResolution(VDIS_DEV_HDMI), &outW, &outH);

                Vcap_setWrbkCaptScParams(inW, inH, outW, outH);

                Vcap_startWrbkCapt();
            }
            #endif

    }
    return 0;
}

static
Void Demo_displayUpdateSeqId()
{
    gDemo_info.curDisplaySeqId++;
}

UInt32 Demo_displayGetCurSeqId()
{
    return (gDemo_info.curDisplaySeqId);
}

Bool Demo_isLayoutPossible(Int32 demoId, Int32 layoutId)
{
    if (demoId == DEMO_HD_SD_5CH)
    {
		/* HD SD use case in interlace mode uses DEI driver for which there is restriction of 
                 max windows (channels) supported (at vpss side). Hence restricting layouts.
               */
        if ((layoutId == DEMO_LAYOUT_MODE_1CH)
            || (layoutId == DEMO_LAYOUT_MODE_4CH)
            || (layoutId == DEMO_LAYOUT_MODE_9CH)
            )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    return TRUE;
}

int Demo_displaySettings(int demoId)
{
    Bool done = FALSE;
    char ch;
    static VDIS_MOSAIC_S vdMosaicParam;
    UInt32 chId, startChId, displayId, resolution, displayChId;
    int prevLayoutId;
    Bool validRes = FALSE;
    static Int32 queueNo; // Should be static to retain current queue
    VSYS_PARAMS_S sysContextInfo;
    VDIS_DEV devId;
    Bool forceLowCostScale = FALSE;
#if defined(TI_816X_BUILD)
    int edeStrength;
    int cprocCfg;
#endif

    Vsys_getContext(&sysContextInfo);

#if defined(TI_816X_BUILD)
    if ((demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE)  ||
            (demoId == DEMO_VCAP_VENC_VDIS))
        layoutId = DEMO_LAYOUT_MODE_16CH;
#endif

    if ((demoId == DEMO_VCAP_VENC_VDEC_VDIS_INTERLACED) ||
        (demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE_8CH))
        forceLowCostScale = TRUE;

    if(gDemo_info.maxVdisChannels<=0)
    {
        printf(" \n");
        printf(" WARNING: Display NOT enabled, this menu is NOT valid !!!\n");
        return -1;
    }

    while(!done)
    {
        printf(gDemo_displaySettingsMenu);

        ch = Demo_getChar();

        switch(ch)
        {
            case '1':
                chId = Demo_getChId("DISPLAY", gDemo_info.maxVdisChannels);

                Demo_displayChnEnable(chId, FALSE);
                break;

            case '2':
                chId = Demo_getChId("DISPLAY", gDemo_info.maxVdisChannels);

                Demo_displayChnEnable(chId, TRUE);
                break;

            case '3':
               prevLayoutId = layoutId;

               {
                    layoutId = Demo_displayGetLayoutId(demoId);
                    if (Demo_isLayoutPossible(demoId, layoutId) == FALSE)
                    {
                        printf("\nDEMO: Layout not ALLOWED/POSSIBLE for this demo\n");
                        break;
                    }

                    /************* For devID = VDIS_DEV_HDMI *************/
                    devId = VDIS_DEV_HDMI;
                    startChId = 0;

                    #if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                    forceLowCostScale = FALSE;
                    #endif

                    Demo_swMsGenerateLayout(
                                devId,
                                startChId,
                                gDemo_info.maxVdisChannels,
                                layoutId,
                                &vdMosaicParam,
                                forceLowCostScale,
                                gDemo_info.Type,
                                Vdis_getSwMsLayoutResolution(devId)
                            );
                    #ifdef TI_816X_BUILD
                    if ((demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE)  ||
                        (demoId == DEMO_VCAP_VENC_VDIS))
                    {
                        Demo_displayResetFps(&vdMosaicParam,layoutId);
                        Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                    }
                    #endif

                    #if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                    Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                    #endif
                    Vdis_setMosaicParams(devId,&vdMosaicParam);
#if defined (TI_816X_BUILD) || defined (TI_814X_BUILD)
#if DEMO_SCD_ENABLE_MOTION_TRACKING
#if USE_FBDEV
                {
                   Bool drawGrid = TRUE;
#if defined (TI_816X_BUILD)
                   if(sysContextInfo.systemUseCase != VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC)
                   {
                      drawGrid = FALSE;
                   }
#endif

                   if(sysContextInfo.enableScd == TRUE && drawGrid)
                   {
                     Scd_mosaicUpdated();
//                     Scd_trackLayout(vdMosaicParam.numberOfWindows, vdMosaicParam.chnMap[0]); /* To pass the number of windows on display in current layout  */
                   }
                 }
#endif
#endif
#endif
                    /************* For devID = VDIS_DEV_HDCOMP *************/
                    #ifdef TI_816X_BUILD
                    if (Vdis_isSupportedDisplay(VDIS_DEV_HDCOMP))
                    {
                        devId = VDIS_DEV_HDCOMP;
                        startChId = 0;

                        /* if the number of channels being display are more than that can fit in 4x4 then
                            make the other channels appear on the second HD Display.
                            Otherwise show same channels on the other HD Display
                        */
                        if(gDemo_info.maxVdisChannels>VDIS_MOSAIC_WIN_MAX)
                            startChId = VDIS_MOSAIC_WIN_MAX;

                        Demo_swMsGenerateLayout(
                            devId,
                            startChId,
                            gDemo_info.maxVdisChannels,
                            layoutId,
                            &vdMosaicParam,
                            forceLowCostScale,
                            gDemo_info.Type,
                            Vdis_getSwMsLayoutResolution(devId)
                        );

                        if ((demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE) ||
                            (demoId == DEMO_VCAP_VENC_VDIS) )
                        {
                            Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                        }

                        Vdis_setMosaicParams(devId,&vdMosaicParam);
                    }
                    else
                    {
                        printf("No HDCOMP present. Usecase id[%d]\n",demoId);
                    }
                    #endif


                    /************* For devID = VDIS_DEV_SD *************/
                    devId = VDIS_DEV_SD;
                    startChId = 0;

                    #if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                    if (layoutId == DEMO_LAYOUT_MODE_4CH)
                        forceLowCostScale = TRUE;
                    #endif
                    Demo_swMsGenerateLayout(
                        devId,
                        startChId,
                        gDemo_info.maxVdisChannels,
                        layoutId,
                        &vdMosaicParam,
                        forceLowCostScale,
                        gDemo_info.Type,
                        Vdis_getSwMsLayoutResolution(devId)
                    );

                    #if defined(TI_814X_BUILD)
                    Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                    #endif

                    #if defined(TI_8107_BUILD)
                    if (demoId != DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE)
                    {
                        Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                    }
                    #endif

                    #ifdef TI_816X_BUILD
                    if ((demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE)  ||
                        (demoId == DEMO_VCAP_VENC_VDIS))
                    {
                        Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                    }
                    #endif

                    #if defined(TI_8107_BUILD)
                    if (demoId != DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE)
                    {
                        Vdis_setMosaicParams(devId,&vdMosaicParam);
                    }
                    #else
                    Vdis_setMosaicParams(devId,&vdMosaicParam);
                    #endif

                    /* wait for the info prints to complete */
                    OSA_waitMsecs(500);
                }
                break;

            case '4':

                {
                    #if defined(TI_814X_BUILD)
                        chId = Demo_getChId("CH ID", gDemo_info.maxVdisChannels);

                        if (Vdis_getMosaicParams(VDIS_DEV_HDMI,&vdMosaicParam) >= 0)
                        {
                            Demo_displayResetFps(&vdMosaicParam,layoutId);
                        }
                        Demo_displaySwitchChn(VDIS_DEV_HDMI, chId);

                        if (Vdis_getMosaicParams(VDIS_DEV_HDMI,&vdMosaicParam) >= 0)
                        {
                            Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                        }

                        /* wait for the info prints to complete */
                        OSA_waitMsecs(500);

                        chId = Demo_getChId("DISPLAY (SDTV)", gDemo_info.maxVdisChannels);

                        if (Vdis_getMosaicParams(VDIS_DEV_SD,&vdMosaicParam) >= 0)
                        {
                            Demo_displayResetFps(&vdMosaicParam,layoutId);
                        }

                        Demo_displaySwitchChn(VDIS_DEV_SD, chId);

                        if (Vdis_getMosaicParams(VDIS_DEV_SD,&vdMosaicParam) >= 0)
                        {
                            Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                        }

                        /* wait for the info prints to complete */
                        OSA_waitMsecs(500);

                    #elif defined(TI_8107_BUILD)
                        chId = Demo_getChId("DISPLAY (HDMI)", gDemo_info.maxVdisChannels);

                        if (Vdis_getMosaicParams(VDIS_DEV_HDMI,&vdMosaicParam) >= 0)
                        {
                            Demo_displayResetFps(&vdMosaicParam,layoutId);
                        }
                        Demo_displaySwitchChn(VDIS_DEV_HDMI, chId);

                        if (Vdis_getMosaicParams(VDIS_DEV_HDMI,&vdMosaicParam) >= 0)
                        {
                            Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                        }

                        /* wait for the info prints to complete */
                        OSA_waitMsecs(500);

                        if ((sysContextInfo.systemUseCase != VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) &&
                            (sysContextInfo.systemUseCase != VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH))
                        {
                            chId = Demo_getChId("DISPLAY (SDTV)", gDemo_info.maxVdisChannels);

                            if (Vdis_getMosaicParams(VDIS_DEV_SD,&vdMosaicParam) >= 0)
                            {
                                Demo_displayResetFps(&vdMosaicParam,layoutId);
                            }

                            Demo_displaySwitchChn(VDIS_DEV_SD, chId);

                            if (Vdis_getMosaicParams(VDIS_DEV_SD,&vdMosaicParam) >= 0)
                            {
                                Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                            }

                            /* wait for the info prints to complete */
                            OSA_waitMsecs(500);
                        }
                    #else
                        chId = Demo_getChId("DISPLAY (HDMI/SDTV)", gDemo_info.maxVdisChannels);

                        Demo_displaySwitchChn(VDIS_DEV_HDMI, chId);

                        if ((demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE) ||
                             (demoId == DEMO_VCAP_VENC_VDIS))
                        {
                            Vdis_getMosaicParams(VDIS_DEV_HDMI,&vdMosaicParam);

                            Demo_displayResetFps(&vdMosaicParam,layoutId);

                            Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                        }
                        /* wait for the info prints to complete */
                        OSA_waitMsecs(500);
                    #endif

                    #if defined (TI_816X_BUILD) || defined (TI_814X_BUILD)
                        #if DEMO_SCD_ENABLE_MOTION_TRACKING
                            #if USE_FBDEV
                            {
                                VSYS_PARAMS_S sysContextInfo;
                                Bool drawGrid = TRUE;

                                Vsys_getContext(&sysContextInfo);

                                #if defined (TI_816X_BUILD)
                                if(sysContextInfo.systemUseCase != VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC)
                                {
                                    drawGrid = FALSE;
                                }
                                #endif

                               if(sysContextInfo.enableScd == TRUE && drawGrid)
                               {
                                   Scd_mosaicUpdated();
                               }
                            }
                            #endif
                        #endif
                    #endif

                    #if defined (TI_816X_BUILD)
                    /* For TI814x/TI8107, this has been already taken care */
                    /* SDTV if enabled follows HDMI */
                    Demo_displaySwitchChn(VDIS_DEV_SD, chId);
                    #endif

                    if ((demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE) ||
                        (demoId == DEMO_VCAP_VENC_VDIS))
                    {
                        Vdis_getMosaicParams(VDIS_DEV_HDMI,&vdMosaicParam);

                        Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                    }
                    /* wait for the info prints to complete */
                    OSA_waitMsecs(500);

                    if (sysContextInfo.systemUseCase != VSYS_USECASE_MULTICHN_HD_VCAP_VENC
                            &&
                        sysContextInfo.systemUseCase != VSYS_USECASE_MULTICHN_HD_SD_VCAP_VENC
                    )
                    {
                        chId = Demo_getChId("DISPLAY (HDCOMP/DVO2)", gDemo_info.maxVdisChannels);

                        Demo_displaySwitchChn(VDIS_DEV_HDCOMP, chId);

                        if ((demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE) ||
                             (demoId == DEMO_VCAP_VENC_VDIS))
                        {
                            Vdis_getMosaicParams(VDIS_DEV_HDMI,&vdMosaicParam);

                            Demo_displayChangeFpsForLayout(&vdMosaicParam,layoutId);
                        }
                        OSA_waitMsecs(500);
                    }
                }
                /* wait for the info prints to complete */
                OSA_waitMsecs(500);

                break;
            case '5':

                {
                    printf(gDemo_displayMenu);
                    displayId  = Demo_getIntValue("Display Id", 1, 4, 1);
                    displayId -= 1;
                    #if defined(TI_814X_BUILD)
                    if (displayId == VDIS_DEV_HDCOMP) {
                        printf(" NOTE: This is not supported in this demo !!! \n");
                    }
                    else {
                    #endif
                    #if defined(TI_8107_BUILD)
                    if (displayId == VDIS_DEV_DVO2) {
                        printf(" NOTE: This is not supported in this demo !!! \n");
                    }
                    else {
                    #endif
                        if ((
                                sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_VCAP_VENC ||
                                sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_HD_VCAP_VENC ||
                                sysContextInfo.systemUseCase == VSYS_USECASE_MULTICHN_HD_SD_VCAP_VENC
                            )
                                &&
                            (displayId == VDIS_DEV_HDCOMP || displayId == VDIS_DEV_DVO2)
                           )
                        {
                            printf(" NOTE: This is not supported in this demo !!! \n");
                            OSA_waitMsecs(100);
                            break;
                        }
                        printf(gDemo_ResolutionMenu);
                        resolution = Demo_getIntValue("Display Id", 1, 9, 1);
                        switch(resolution) {
                            case 1:
                                if (displayId != VDIS_DEV_SD)
                                {
                                    resolution = VSYS_STD_1080P_60;
                                    validRes   = TRUE;
                                }
                                else
                                {
                                    printf("\n Resolution Not supported !!\n");
                                }
                            break;
                            case 2:
                                if (displayId != VDIS_DEV_SD)
                                {
                                    resolution = VSYS_STD_720P_60;
                                    validRes   = TRUE;
                                }
                                else
                                {
                                    printf("\n Resolution Not supported !!\n");
                                }
                            break;
                            case 3:
                                if (displayId != VDIS_DEV_SD)
                                {
                                    resolution = VSYS_STD_XGA_60;
                                    validRes   = TRUE;
                                }
                                else
                                {
                                    printf("\n Resolution Not supported !!\n");
                                }
                            break;
                            case 4:
                                if (displayId != VDIS_DEV_SD)
                                {
                                    resolution = VSYS_STD_SXGA_60;
                                    validRes   = TRUE;
                                }
                                else
                                {
                                    printf("\n Resolution Not supported !!\n");
                                }
                            break;
                            case 5:
                                if (displayId == VDIS_DEV_SD)
                                {
                                    resolution = VSYS_STD_NTSC;
                                    validRes   = TRUE;
                                }
                                else
                                {
                                    printf("\n Resolution Not supported !!\n");
                                }
                            break;
                            case 6:
                                if (displayId == VDIS_DEV_SD)
                                {
                                    resolution = VSYS_STD_PAL;
                                    validRes   = TRUE;
                                }
                                else
                                {
                                    printf("\n Resolution Not supported !!\n");
                                }
                            break;
                            case 7:
                                if (displayId != VDIS_DEV_SD)
                                {
                                    resolution = VSYS_STD_1080P_50;
                                    validRes   = TRUE;
                                }
                                else
                                {
                                    printf("\n Resolution Not supported !!\n");
                                }
                            break;
                            case 8:
                                /* In case of 8107, we use writeback path from HDMI and 
                                display it on SD Venc. So switching to VGA 60 would cause upscaling 
                                This is not supported. */
#ifdef TI_8107_BUILD
                                if ((sysContextInfo.systemUseCase != 
                                     VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) 
                                     && 
                                     (sysContextInfo.systemUseCase != VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH))
#endif
                                {
                                    if (displayId != VDIS_DEV_SD)
                                    {
                                        resolution = VSYS_STD_VGA_60;
                                        validRes   = TRUE;
                                    }
                                    else
                                    {
                                        printf("\n Resolution Not supported !!\n");
                                    }
                                }
                            break;

                            case 9:
                            	//ranran
                                if (displayId != VDIS_DEV_SD)
                                {
                                	printf("resolution is custom!!\n");
                                    resolution = VSYS_STD_CUSTOM;
                                    validRes   = TRUE;
                                }
                                else
                                {
                                    printf("\n Resolution Not supported !!\n");
                                }
                            break;
                            default:
                            	printf("resolution is default!!!!\n");
                                resolution = VSYS_STD_1080P_60;

                        }

                        if (validRes)
                        {
#if defined (TI_814X_BUILD)
                            if ((displayId == VDIS_DEV_HDMI)
                                ||
                                (displayId == VDIS_DEV_DVO2))
                            {
                                Demo_SuspendAudioPlayBackOnHdmi();
                            }
#endif
#if defined (TI_8107_BUILD)
                            if ((displayId == VDIS_DEV_HDMI)
                                ||
                                (displayId == VDIS_DEV_HDCOMP))
                            {
                                Demo_SuspendAudioPlayBackOnHdmi();
                            }
#endif
#if defined (TI_816X_BUILD)
                            if (displayId == VDIS_DEV_HDMI)
                            {
                                Demo_SuspendAudioPlayBackOnHdmi();
                            }
#endif

                            Demo_displaySetResolution(displayId, resolution);
#if defined (TI_816X_BUILD) || defined (TI_814X_BUILD)
#if DEMO_SCD_ENABLE_MOTION_TRACKING
#if USE_FBDEV
                            {
                                VSYS_PARAMS_S sysContextInfo;
                                Bool drawGrid = TRUE;

                                Vsys_getContext(&sysContextInfo);

                                #if defined (TI_816X_BUILD)
                                if(sysContextInfo.systemUseCase != VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC)
                                {
                                    drawGrid = FALSE;
                                }
                                #endif

                               if(sysContextInfo.enableScd == TRUE && drawGrid)
                               {
                                   Scd_mosaicUpdated();
                               }
                           }
#endif
#endif
#endif
#if defined (TI_814X_BUILD)
                            if ((displayId == VDIS_DEV_HDMI)
                                ||
                                (displayId == VDIS_DEV_DVO2))
                            {
                                Demo_ResumeAudioPlayBackOnHdmi();
                            }
#endif
#if defined (TI_8107_BUILD)
                            if ((displayId == VDIS_DEV_HDMI)
                                ||
                                (displayId == VDIS_DEV_HDCOMP))
                            {
                                Demo_ResumeAudioPlayBackOnHdmi();
                            }
#endif
#if defined (TI_816X_BUILD)
                            if (displayId == VDIS_DEV_HDMI)
                            {
                                Demo_ResumeAudioPlayBackOnHdmi();
                            }
#endif
                        }
                        validRes = FALSE;
                    #if defined(TI_814X_BUILD) || defined (TI_8107_BUILD)
                    }
                    #endif
                }
                break;
            case '6':
                {
                    queueNo = Demo_getIntValue("DISPLAY ACTIVE QUEUE(0/1) (only for SD)",
                                            0,
                                            1,
                                            0);
                    if (queueNo == 0)
                    {
                        /* Queue 0 has only 1 channel. Reset to Ch 0 while switching to Queue 0*/
                        printf ("Resetting to Ch 0\n");
                        Demo_displaySwitchSDChan(VDIS_DEV_SD,0);
                    }
                    Demo_displaySwitchQueue(VDIS_DEV_SD, queueNo);
                }
                break;
            case '8':
                if (demoId == DEMO_VCAP_VENC_VDEC_VDIS_PROGRESSIVE)
                {
                    Int32 chNo;

                    chNo = Demo_getIntValue("SDTV channel:",
                                             0,
                                             gDemo_info.maxVcapChannels-1,
                                             0);
                    #if 0
                    if (queueNo == 0)
                    {
                        chNo = 0;
                    }
                    #endif
                    Demo_displaySwitchSDChan(VDIS_DEV_SD, chNo);
                }
                if (demoId == DEMO_CARDVR_4CH)
                {
                    Int32 chNo;

                    chNo = Demo_getIntValue("SDTV channel:",
                                             0,
                                             2,
                                             0);
                    Demo_displaySwitchCarDVRCh(chNo);
                }
                #ifdef TI_816X_BUILD
                if (demoId == DEMO_HD_SD_5CH)
                {
                    UInt32 switchToInterlaced;
                    extern Void MultiCh_hdsdidvrSwitchInterlacedProgressiveSwitch(Bool switchToInterlaced);

                    switchToInterlaced = Demo_getIntValue("Switch To Interlaced (0 - FALSE/  1 - TRUE)",
                                             0,
                                             1,
                                             0);
                    MultiCh_hdsdidvrSwitchInterlacedProgressiveSwitch(switchToInterlaced);
                }
                #endif
                break;

            case '9':
            case 'a':
                {
                    WINDOW_S chnlInfo, winCrop;
                    UInt32 winId = 0;

                    devId = VDIS_DEV_HDMI;

                    Vdis_getChnlInfoFromWinId(devId, winId,&chnlInfo);
                    winCrop.width = chnlInfo.width/2;
                    winCrop.height = chnlInfo.height/2;

                    if (ch == '9')
                    {
                        winCrop.start_X = chnlInfo.start_X;
                        winCrop.start_Y = chnlInfo.start_Y;
                    }
                    else
                    {
                        winCrop.start_X = chnlInfo.start_X + chnlInfo.width/4;
                        winCrop.start_Y = chnlInfo.start_Y + chnlInfo.height/4;
                    }

                    Vdis_SetCropParam(devId, winId,winCrop);
                }
                break;
            case 'b':
                devId = VDIS_DEV_HDMI;
                chId = Demo_getChId("DECODE", gDemo_info.maxVdecChannels + 1);
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_pauseMosaic(devId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_pauseChn(devId, displayChId);
                }
                devId = VDIS_DEV_HDCOMP;
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_pauseMosaic(devId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_pauseChn(devId, displayChId);
                }
                devId = VDIS_DEV_SD;
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_pauseMosaic(devId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_pauseChn(devId, displayChId);
                }

                break;
            case 'c':
            {
                UInt32 timescale,seqId;

                devId = VDIS_DEV_HDMI;
                chId = Demo_getChId("DECODE", gDemo_info.maxVdecChannels + 1);
                timescale = Demo_getIntValue("Playback Speed x1000",50,2000,1000);
                seqId = Demo_displayGetCurSeqId();
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_mosaicSetPlaybackSpeed(devId,timescale,seqId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_setPlaybackSpeed(devId,displayChId,timescale,seqId);
                }
                devId = VDIS_DEV_HDCOMP;
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_mosaicSetPlaybackSpeed(devId,timescale,seqId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_setPlaybackSpeed(devId,displayChId,timescale,seqId);
                }
                devId = VDIS_DEV_SD;
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_mosaicSetPlaybackSpeed(devId,timescale,seqId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_setPlaybackSpeed(devId,displayChId,timescale,seqId);
                }

                break;
            }
            case 'd':
                devId = VDIS_DEV_HDMI;

                chId = Demo_getChId("DECODE", gDemo_info.maxVdecChannels + 1);
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_stepMosaic(devId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_stepChn(devId,displayChId);
                }

                devId = VDIS_DEV_HDCOMP;
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_stepMosaic(devId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_stepChn(devId,displayChId);
                }
                devId = VDIS_DEV_SD;
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_stepMosaic(devId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_stepChn(devId,displayChId);
                }

                break;
            case 'e':
                devId = VDIS_DEV_HDMI;
                chId = Demo_getChId("DECODE", gDemo_info.maxVdecChannels + 1);
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_resumeMosaic(devId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_resumeChn(devId,displayChId);
                }
                devId = VDIS_DEV_HDCOMP;
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_resumeMosaic(devId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_resumeChn(devId,displayChId);
                }
                devId = VDIS_DEV_SD;
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_resumeMosaic(devId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_resumeChn(devId,displayChId);
                }
                break;
            case 'f':
            {
                UInt32 seekVidPTS,seqId;

                devId = VDIS_DEV_HDMI;
                chId = Demo_getChId("DECODE", gDemo_info.maxVdecChannels + 1);
                if (chId == gDemo_info.maxVdecChannels)
                {
                    displayChId = VDIS_CHN_ALL;
                }
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                }
                seekVidPTS = Demo_getIntValue("Seek PTS",0,INT_MAX,0);
                if (VDIS_CHN_ALL == displayChId)
                {
                    Demo_displayUpdateSeqId();
                }
                seqId = Demo_displayGetCurSeqId();
                Vdis_seek(devId,displayChId,AVSYNC_INVALID_PTS,seekVidPTS,seqId);
                devId = VDIS_DEV_HDCOMP;
                if (chId == gDemo_info.maxVdecChannels)
                {
                    displayChId = VDIS_CHN_ALL;
                }
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                }
                Vdis_seek(devId,displayChId,AVSYNC_INVALID_PTS,seekVidPTS,seqId);
                devId = VDIS_DEV_SD;
                if (chId == gDemo_info.maxVdecChannels)
                {
                    displayChId = VDIS_CHN_ALL;
                }
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                }
                Vdis_seek(devId,displayChId,AVSYNC_INVALID_PTS,seekVidPTS,seqId);

                break;
            }
            case 'g':
            {
                devId = VDIS_DEV_HDMI;
                chId = Demo_getChId("DECODE", gDemo_info.maxVdecChannels + 1);
                if (chId == gDemo_info.maxVdecChannels)
                {
                    Vdis_resetMosaicPlayerTime(devId);
                }
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_resetChPlayerTime(devId,displayChId);
                }
                devId = VDIS_DEV_HDCOMP;
                if (chId == gDemo_info.maxVdecChannels)
                {
                    Vdis_resetMosaicPlayerTime(devId);
                }
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_resetChPlayerTime(devId,displayChId);
                }
                devId = VDIS_DEV_SD;
                if (chId == gDemo_info.maxVdecChannels)
                {
                    Vdis_resetMosaicPlayerTime(devId);
                }
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_resetChPlayerTime(devId,displayChId);
                }

                break;
            }
            case 'h':
            {
                UInt32 seqId;

                devId = VDIS_DEV_HDMI;
                chId = Demo_getChId("DECODE", gDemo_info.maxVdecChannels + 1);
                seqId = Demo_displayGetCurSeqId();
                if (chId == gDemo_info.maxVdecChannels)
                {
                    Demo_displayUpdateSeqId();
                    seqId = Demo_displayGetCurSeqId();
                    Vdis_setMosaicPlayerStatePlay(devId,seqId);
                }
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_setChPlayerStatePlay(devId,displayChId,seqId);
                }
                devId = VDIS_DEV_HDCOMP;
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_setMosaicPlayerStatePlay(devId,seqId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_setChPlayerStatePlay(devId,displayChId,seqId);
                }
                devId = VDIS_DEV_SD;
                if (chId == gDemo_info.maxVdecChannels)
                    Vdis_setMosaicPlayerStatePlay(devId,seqId);
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_setChPlayerStatePlay(devId,displayChId,seqId);
                }

                break;
            }
            case 'i':
            {
                UInt32 frameDisplayDuration;
                UInt32 seqId;

                devId = VDIS_DEV_HDMI;
                chId = Demo_getChId("DECODE", gDemo_info.maxVdecChannels + 1);
                frameDisplayDuration = Demo_getIntValue("Scan Frame Display Duration",100,500,300);
                seqId = Demo_displayGetCurSeqId();
                if (chId == gDemo_info.maxVdecChannels)
                {
                    Demo_displayUpdateSeqId();
                    seqId = Demo_displayGetCurSeqId();
                    Vdis_scanMosaic(devId,frameDisplayDuration,seqId);
                }
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_scanCh(devId,displayChId,frameDisplayDuration,seqId);
                }
                devId = VDIS_DEV_HDCOMP;
                if (chId == gDemo_info.maxVdecChannels)
                {
                    Vdis_scanMosaic(devId,frameDisplayDuration,seqId);
                }
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_scanCh(devId,displayChId,frameDisplayDuration,seqId);
                }
                devId = VDIS_DEV_SD;
                if (chId == gDemo_info.maxVdecChannels)
                {
                    Vdis_scanMosaic(devId,frameDisplayDuration,seqId);
                }
                else
                {
                    Vdec_mapDec2DisplayChId(devId,chId,&displayChId);
                    Vdis_scanCh(devId,displayChId,frameDisplayDuration,seqId);
                }

                break;
            }
            case 'j':
            {
                UInt32 holdLastFrame;

                devId = VDIS_DEV_HDMI;
                chId = Demo_getChId("DISPLAY", gDemo_info.maxVdisChannels + 1);
                if (chId == gDemo_info.maxVdisChannels)
                {
                    chId = VDIS_CHN_ALL;
                }
                holdLastFrame = Demo_getIntValue("SWMS flush : Hold Last Frame (0 - FALSE/1 - TRUE)",0,1,1);
                Vdis_flushSwMs(devId,chId,holdLastFrame);
                break;
            }
            case 'k':
                //grpx_demo();
                break;

            case 'l':
            {
                UInt32 ctrlVal, controlId;

                printf(gDemo_displayMenu);
                displayId = Demo_getIntValue("Display Id", 1, 4, 1);
                displayId -= 1;

#if defined(TI_814X_BUILD)
                if (displayId == VDIS_DEV_HDCOMP) {
                    printf(" NOTE: This is not supported in this demo !!! \n");
                } else {
#endif
                    printf(gDemo_dispColorControlMenu);
                    controlId = Demo_getIntValue("Control Id", 1, 4, 1);

                    if (4 == controlId)
                        ctrlVal = Demo_getIntValue("Control Value", 1, 61, 31);
                    else
                        ctrlVal = Demo_getIntValue("Control Value", 1, 256, 128);

                    controlId -= 1u;
                    Vdis_setColorControl(displayId, controlId, ctrlVal);

#if defined(TI_814X_BUILD)
                }
#endif
                break;
            }

            case 'm':
            {
                printf(gDemo_displayMenu);
                displayId = Demo_getIntValue("Display Id", 1, 4, 1);
                displayId -= 1;

                if (displayId != VDIS_DEV_HDMI) {
                    printf("\nSupported for HDMI interface only !!\n");
                } else {
                    Vdis_printScreenDetailedTimings(displayId);
                }
            }
            break;


            case 'n':
            if (demoId == VSYS_USECASE_MULTICHN_VDEC_VDIS)
            {
                int chId;
                VDIS_MPSCLR_SET_CROP_INFO_S cropParams;
                devId = VDIS_DEV_HDMI;

                chId = Demo_getChId("DISPLAY", gDemo_info.maxVdisChannels);
                cropParams.cropStartX = Demo_getIntValue("Start X offset", 0, 4096, 0);
                cropParams.cropStartY = Demo_getIntValue("Start Y offset", 0, 4096, 0);
                cropParams.cropWidth = Demo_getIntValue("Active video width", 64, 4096, 64);
                cropParams.cropHeight = Demo_getIntValue("Active video height", 64, 4096, 64);

                Vdis_MpsclrSetFwdCrop(devId, chId, &cropParams, TRUE);
            }
            break;
            case 'o':
            if (demoId == VSYS_USECASE_MULTICHN_VDEC_VDIS)
            {
                int chId;
                VDIS_MPSCLR_SET_CROP_INFO_S cropParams;
                devId = VDIS_DEV_HDMI;

                chId = Demo_getChId("DISPLAY", gDemo_info.maxVdisChannels);

                Vdis_MpsclrSetFwdCrop(devId, chId, &cropParams, FALSE);
            }
            break;

            case 'q':
            if (demoId == VSYS_USECASE_MULTICHN_VDEC_VDIS)
            {
                devId = VDIS_DEV_HDMI;
                Vdis_printChn2SwMsInstMap(devId);
            }
            break;

            case 'r':
            if (demoId == VSYS_USECASE_MULTICHN_VDEC_VDIS)
            {
                VDIS_SWMS_SET_CHAN_INST_MAP_INFO_S mapParam, mapParamsDvo2;
                UInt32 chCnt, scCnt, i, j;
                printf (" SWMS Demo : Distribute video channels to be processed"
                        " by each instance of SC equally. \n");
                printf ("\n");
                printf (" SWMS Demo : e.g. If you have 9 channels of video \n");
                printf (" SWMS Demo : SC instance 1 is processing 0 - 7 "
                        " channels & instance 2 is processing 8&9 channels\n");
                printf (" SWMS Demo : Enter 2 for number of scalar \n");
                printf (" SWMS Demo : Enter 9 for number of video streams \n");
                printf (" SWMS Demo : SWMS will be configured to process \n "
                            "Channels 0, 2, 4, 6, & 8 by SC1 \n "
                            "Channels 1, 3, 5, 7, by SC2 \n");

                scCnt = Demo_getIntValue("the number of scalar used ",
                        1, SYSTEM_SW_MS_MAX_INST, 2);

                chCnt = Demo_getIntValue("the number of video streams",
                        1, 32, 5);

                mapParam.numChannels = chCnt;

                /* SC instance to be used */
                j = 0;
                /* IMPORTANT
                    We should start with the first channel that is assigned to
                    this instance of SWMS - in this demo, we start with 0.
                    */
                for (i = 0; i < chCnt; i++)
                {
                    mapParam.channelMap[i] = i;
                    mapParam.scInstMap[i] = j++;
                    if (j == scCnt)
                        j = 0;
                }
                devId = VDIS_DEV_HDMI;
                Vdis_setChn2SwMsInstMap(devId, &mapParam);

                mapParamsDvo2.numChannels = chCnt;
                /* SC instances to be used */
                j = 0;
                /* IMPORTANT
                    We should start with the first channel that is assigned to
                    this instance of SWMS - in this demo, we start with 0.
                    */
                for (i = 0; i < chCnt; i++)
                {
                    mapParamsDvo2.channelMap[i] = i;
                    mapParamsDvo2.scInstMap[i] = j++;
                    if (j == scCnt)
                        j = 0;
                }
                devId = VDIS_DEV_DVO2;
                Vdis_setChn2SwMsInstMap(devId, &mapParamsDvo2);
            }
                break;
            case 't':
            {
                Int32 ch;
                Bool validFlag;
                UInt64 firstPTS = 0, currPTS = 0;

                ch = Demo_getChId("Enter the channel number: ", gDemo_info.maxVdisChannels);
                Demo_displayGetChTimeInfo(ch, &validFlag, &firstPTS, &currPTS);
                printf ("\nChannel %d %s, first PTS: %llu last PTS: %llu\n",
                    ch,
                    validFlag == TRUE ? "stared" : "not started",
                    firstPTS,
                    currPTS
                    );
            }
                break;
            case 'p':
                done = TRUE;
                break;
#ifdef TI_816X_BUILD
            case 's':
                edeStrength = Demo_getIntValue("EDE strength", 
                                0, 12, 6);
                Vdis_setEdeStrength(edeStrength);
                break;
            case 'u':
                printf("CPROC color temperature setting:\n\
                        \t0  : neutral\n\
                        \t1  : color temerature 3077K\n\
                        \t2  : color temerature 3333K\n\
                        \t3  : color temerature 3636K\n\
                        \t4  : color temerature 4000K\n\
                        \t5  : color temerature 4444K\n\
                        \t6  : color temerature 5000K\n\
                        \t7  : color temerature 5333K\n\
                        \t8  : color temerature 5714K\n\
                        \t9  : color temerature 6052K\n\
                        \t10 : color temerature 6342K\n\
                        \t11 : color temerature 6667K\n\
                        \t12 : color temerature 7009K\n\
                        \t13 : color temerature 7328K\n\
                        \t14 : color temerature 7682K\n\
                        \t15 : color temerature 8000K\n\
                        \t16 : color temerature 10000K\n\
                        \t17 : color temerature 11111K\n");
                cprocCfg = Demo_getIntValue("CPROC color temperature", 0, 17, 0);
                Vdis_setCprocConfig(cprocCfg);
                break;
#endif
            }
    }

    return 0;

}

Int32 Demo_displayGetChTimeInfo (Int32 chNum, Bool *displayStarted, UInt64 *firstPTS, UInt64 *currPTS)
{
    VDIS_MOSAIC_S   mosaicParams;
    Int32 id;
    Int32 vdevId;
    CHANNEL_TIME_INFO chTimeInfo;

    for (vdevId = VDIS_DEV_HDMI; vdevId <VDIS_DEV_SD; vdevId++)
    {
        Vdis_getMosaicParams(vdevId, &mosaicParams);
        for (id=0; id<VDIS_MOSAIC_WIN_MAX; id++)
        {
            if ((mosaicParams.chnMap[id] != DEMO_SW_MS_INVALID_ID)
                && (mosaicParams.chnMap[id] == chNum))
            {
                break;
            }
        }

        if (id < VDIS_MOSAIC_WIN_MAX)
            break;
    }

    if (id == VDIS_MOSAIC_WIN_MAX)
    {
        *displayStarted = FALSE;
    }
    else
    {
        chTimeInfo.chId = chNum;
        Vdis_getChTimeInfo(vdevId, &chTimeInfo);
        *displayStarted = chTimeInfo.displayStarted;
        *firstPTS = chTimeInfo.firstPTS;
        *currPTS = chTimeInfo.currPTS;
    }
    return 0;
}



