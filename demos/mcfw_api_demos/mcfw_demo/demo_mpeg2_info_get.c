/*
 *******************************************************************************
 * Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 *******************************************************************************
 */

/**
 *******************************************************************************
 * @file <demo_mpeg2_info_get.c>
 *
 * @brief  This file has the implementataion for the generation of frame size
 *         information
 *
 *
 * @author: Satish Arora
 *
 * @version 0.0 (Aug 2013) : Base version  [Satish Arora] - reusing chunking logic used in AVTE [Hari]
 ******************************************************************************
 */
/******************************************************************************
 *  INCLUDE FILES
 ******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include <sys/time.h>

#define AVCLOG(X,Y,Z) {} // { printf Z; printf ("\n"); }

/* In lieu of avc.h */
typedef uint32_t AVC_Err;

/* Common errorcodes */
#define AVC_SUCCESS                 (0)
#define AVC_ERR_NOT_SUPPORTED       (1)
#define AVC_ERR_MALLOC_FAILED       (2)
#define AVC_ERR_INTERNAL_ERROR      (3)
#define AVC_ERR_INVALID_ARGS        (4)
#define AVC_ERR_NO_OUTPUT_CHANNEL   (5)
#define AVC_ERR_WRONG_STATE         (6)
#define AVC_ERR_INSUFFICIENT_INPUT  (7)


#define CHECKPOINT 

uint32_t chunkCnt = 1;




typedef struct
{
	uint8_t *ptr;
	int32_t bufsize, bufused;
} AVChunk_Buf;

/* Various start codes for MPEG-2/1 */
#define MPEG2V_PICTURE_START_CODE 0x100
#define MPEG2V_SEQ_START_CODE     0x1B3
#define MPEG2V_GOP_START_CODE     0x1B8

#define MPEG2V_WORKING_WORD_INIT  0xFFFFFFFF

enum {
	MPEG2V_ST_LOOKING_FOR_SEQ_SC, /**< Initial state at start, look for seq-start code */
	MPEG2V_ST_LOOKING_FOR_PICT_SC,/**< Looking for picture-start-code to start a frame */
	MPEG2V_ST_INSIDE_PICT_SC,     /**< Pict-SC found, now look for other SCs to end a frame */
	MPEG2V_ST_STREAM_ERR,         /**< When some discontinuity was detected in the stream */
	MPEG2V_ST_HOLDING_SC          /**< Intermediate state, when a new frame is detected */
};

/* MPEG2 video chunking */
typedef struct
{
	uint32_t workingWord;
	uint8_t state;
} Mpeg2Vid_ChunkingCtx;





void Mpeg2Vid_chunkingCtxInit(Mpeg2Vid_ChunkingCtx *c)
{
	c->workingWord = MPEG2V_WORKING_WORD_INIT;
	c->state = MPEG2V_ST_LOOKING_FOR_SEQ_SC;
}

AVC_Err Mpeg2Vid_doChunking(Mpeg2Vid_ChunkingCtx *c, 
	AVChunk_Buf *opBufs, uint32_t numOutBufs, AVChunk_Buf *inBuf, void *attrs)
{
	int i = 0,j,frame_end,sc_found,tmp,bytes;
	uint32_t w;
	uint8_t *inp;

	inp = &inBuf->ptr[inBuf->bufused];
	bytes = inBuf->bufsize - inBuf->bufused;

BACK:
	if (c->state == MPEG2V_ST_INSIDE_PICT_SC)
	{
		tmp = i;
		frame_end = sc_found = 0;
		while (i < bytes)
		{
			c->workingWord <<= 8;
			c->workingWord |= inp[i++];
			if (c->workingWord == MPEG2V_PICTURE_START_CODE ||
					c->workingWord == MPEG2V_SEQ_START_CODE ||
					c->workingWord == MPEG2V_GOP_START_CODE)
			{
				frame_end = sc_found = 1;
				break;
			}
		}
		j = i - tmp;

		/* minus 4 to remove next header that is already copied */
		if (sc_found) j -= 4;

		if (j > (int32_t)(opBufs->bufsize - opBufs->bufused))
		{
		/* output buffer is full, end the frame right here
		 */
			AVCLOG(AVC_MOD_MAIN, AVC_LVL_TRACE_ALL, ("memcpy(%p,%d,%p,%d,%d)", 
				opBufs->ptr, opBufs->bufused, inp, tmp, opBufs->bufsize - opBufs->bufused));
			memcpy(&opBufs->ptr[opBufs->bufused],&inp[tmp],opBufs->bufsize - opBufs->bufused);
			opBufs->bufused = opBufs->bufsize;
			c->state = MPEG2V_ST_LOOKING_FOR_PICT_SC;
			frame_end = 1;
		}
		else if (j > 0)
		{
			AVCLOG(AVC_MOD_MAIN, AVC_LVL_TRACE_ALL, ("memcpy(%p,%d,%p,%d,%d)", 
				opBufs->ptr, opBufs->bufused, inp, tmp, j));
			memcpy(&opBufs->ptr[opBufs->bufused],&inp[tmp],j);
			opBufs->bufused += j;
		}
		else
		{
			opBufs->bufused += j;
		}
		
		if (frame_end)
		{
			if (sc_found) c->state = MPEG2V_ST_HOLDING_SC;
			inBuf->bufused += i;
			return AVC_SUCCESS;
		}
	}
	if (c->state == MPEG2V_ST_LOOKING_FOR_PICT_SC)
	{
		tmp = i;
		sc_found = 0;
		while (i < bytes)
		{
			c->workingWord <<= 8;
			c->workingWord |= inp[i++];
			if (c->workingWord == MPEG2V_PICTURE_START_CODE)
			{
				sc_found = 1;
				break;
			}
		}
		j = i - tmp;

		if (j > (opBufs->bufsize - opBufs->bufused))
		{
		/* output buffer is full, discard this data, go back to looking for seq start code
		 */
			opBufs->bufused = 0;
			c->state = MPEG2V_ST_LOOKING_FOR_SEQ_SC;
		}
		else if (j > 0)
		{
			AVCLOG(AVC_MOD_MAIN, AVC_LVL_TRACE_ALL, ("memcpy(%p,%d,%p,%d,%d)", 
				opBufs->ptr, opBufs->bufused, inp, tmp, j));
			memcpy(&opBufs->ptr[opBufs->bufused],&inp[tmp],j);
			opBufs->bufused += j;
		}
		
		if (sc_found)
		{
			/* Set the attribute at rioWptr */
			c->state = MPEG2V_ST_INSIDE_PICT_SC;
		}
	}
	if (c->state == MPEG2V_ST_STREAM_ERR)
	{
		while (i < bytes)
		{
			c->workingWord <<= 8;
			c->workingWord |= inp[i++];

			if (c->workingWord == MPEG2V_PICTURE_START_CODE ||
					c->workingWord == MPEG2V_SEQ_START_CODE ||
					c->workingWord == MPEG2V_GOP_START_CODE)
			{
				c->state = MPEG2V_ST_HOLDING_SC;
				break;
			}
		}
	}
	if (c->state == MPEG2V_ST_LOOKING_FOR_SEQ_SC)
	{
		while (i < bytes)
		{
			c->workingWord <<= 8;
			c->workingWord |= inp[i++];

			if (c->workingWord == MPEG2V_SEQ_START_CODE)
			{
				c->state = MPEG2V_ST_HOLDING_SC;
				break;
			}
		}
	}
	if (c->state == MPEG2V_ST_HOLDING_SC)
	{
		w = c->workingWord;
		opBufs->bufused = 0;
		if (opBufs->bufsize < 4)
		{
			/* Means output buffer does not have space for 4 bytes, bad error
			 */
			AVCLOG(AVC_MOD_MAIN, AVC_LVL_CRITICAL_ERROR, ("Bad error"));
		}
		/* Copy these 4 bytes into output */
		for (j=0; j<4;j++,w<<=8) 
		{
			opBufs->ptr[opBufs->bufused+j] = ((w >> 24) & 0xFF);
		}
		/* Copying frame start code done, now proceed to next state
		 */
		opBufs->bufused += j;
		c->state = (c->workingWord == MPEG2V_PICTURE_START_CODE)?
			MPEG2V_ST_INSIDE_PICT_SC:MPEG2V_ST_LOOKING_FOR_PICT_SC;

		c->workingWord = MPEG2V_WORKING_WORD_INIT;
	}

	if (i < bytes) goto BACK;

	inBuf->bufused += i;
	return AVC_ERR_INSUFFICIENT_INPUT;
}


#define READSIZE (4*1024*1024)
#define CHUNKSIZE (4*1024*1024)



/**
 ******************************************************************************
 *  @fn   Demo_generateMpeg2HdrFile
 *  @brief Function to generate hdr file with frame size info
 *
 *  @param[in]  char *filename : Input bitstream Filename 
 *  
 *  @return[out]  : None
 ******************************************************************************
 */
void Demo_generateMpeg2HdrFile(char *filename)
{
    FILE *file ,*file2;

    char Framesizefilename[1024];


    int frameSize = 0;
/*
       int tempframesize = 0;
	int retValue  = 0;

	int stream[100048];
	int *Strptr;
	int new_nal = 1;

	unsigned int i         = 0;
	unsigned int Total     = 0;
	unsigned int bufferlen = 100000 ;

*/

	uint8_t *readBuf, *chunkBuf;
	Mpeg2Vid_ChunkingCtx ctx;
	AVChunk_Buf inBuf, outBuf;
	uint32_t bytes;

	

    file = fopen(filename, "rb" );

    bzero(Framesizefilename,1024);
    strcat(Framesizefilename, filename);
    strcat(Framesizefilename,".hdr\0");

    if(file == NULL)
    {
        printf("Error: can't open Input file.\n");
        exit(1);
    }
    else
    {
        printf(" Input file [%s] opened successfully !!!\n", filename);
    }

    if((file2 = fopen(Framesizefilename, "w+")) == NULL)
    {
        printf("Error: Cannot open Output file.\n");
        exit(1);
    }
    else
    {
        printf(" Output file [%s] opened successfully !!!\n", Framesizefilename);
    }






	
	readBuf = (uint8_t*)malloc(READSIZE);
	if (!readBuf) exit(1);

	chunkBuf = (uint8_t*)malloc(CHUNKSIZE);
	if (!chunkBuf) exit(1);



	Mpeg2Vid_chunkingCtxInit(&ctx);
	
	outBuf.ptr = chunkBuf;
	outBuf.bufsize = CHUNKSIZE;
	outBuf.bufused = 0;

	frameSize = 0;

	while (!feof(file))
	{
		bytes = fread(readBuf, 1, READSIZE, file);

		inBuf.ptr = readBuf;

		inBuf.bufsize = bytes ;  //((bytes-tmp)>184)?184:(bytes-tmp);
		inBuf.bufused = 0;

		while (inBuf.bufsize != inBuf.bufused)
		{
			if (AVC_SUCCESS == Mpeg2Vid_doChunking(&ctx, &outBuf, 1, &inBuf, NULL))
			{
				// printf("%d     %d\n", chunkCnt++, outBuf.bufused);
				frameSize += inBuf.bufused;
				
				fprintf(file2,"%d\n",frameSize - 4); //outBuf.bufused);
				//outBuf.bufused = 0;
				frameSize = 4;

				inBuf.ptr += inBuf.bufused;

				inBuf.bufsize -= inBuf.bufused ; 
				inBuf.bufused = 0;
			}
			else frameSize += inBuf.bufused;
				
			
		}


		//diff = (((tend.tv_sec - tstart.tv_sec)*1000000)+tend.tv_usec)-tstart.tv_usec;
		//printf("Time taken for %d bytes (microsecs): %d  per kbyte: %d\n",bytes,diff,(diff*1024)/(bytes));
	}

	free(chunkBuf);
	free(readBuf);

    fclose(file);
    fclose(file2);




#if 0

	x = getc( file );

    while( x != EOF )
    {
        stream[i++] = x;
        if (i >= bufferlen)
        {
            if ((retValue = H264VID_getFrameSize( stream ,bufferlen, new_nal)) == 0)
            {
                framesize += bufferlen;
                i = 0;
                new_nal = 0;
            }
            else
            {
                new_nal    = 1;
                framesize += retValue;
                Total     += framesize;

                //                printf("frame size( %d ) = %d \tTotal= 0x%x\n",
                //                      index++, framesize, Total);
                fprintf(file2,"%d\n",framesize);

                for (j=retValue, k=0; j < bufferlen; j++, k++)
                {
                    stream[k] = stream[j];
                }

                i         = k;
                framesize = 0;
            }
        }

        x = getc( file );
    }

    bufferlen = i;
    Strptr    = stream;
    tempframesize = framesize;
    while ((framesize = H264VID_getFrameSize( Strptr ,bufferlen, new_nal)) != 0)
    {
	Total += (framesize + tempframesize);
        //       printf ("frame size( %d ) = %d \tTotal= 0x%x\n", index++, framesize, Total);
        fprintf(file2,"%d\n",(framesize + tempframesize));

        Strptr    += framesize;
        bufferlen -= framesize;
        tempframesize = 0;
    }
    bufferlen += tempframesize;
    Total += bufferlen;
    //    printf ("frame size( %d ) = %d \tTotal= 0x%x\n", index++, bufferlen, Total);
    fprintf(file2,"%d\n",bufferlen);

    fclose(file);
    fclose(file2);

    //    printf("File Closed\n");

#endif

	
}




