/*
 * commit_client.c
 *
 *  Created on: Jul 7, 2014
 *      Author: ubuntu
 */


/************tcpclient.c************************/
/* Header files needed to use the sockets API. */
/* File contains Macro, Data Type and */
/* Structure definitions along with Function */
/* prototypes. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <commit.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <commit_msg.h>
#include <signal.h>
#include <dirent.h>
#include <demo.h>
#include <demo_vcap_venc_vdec_vdis.h>
#include <time.h>
#include <sys/stat.h>
#include "usb_detect.h"
#include <libudev.h>

#define MAX_BUF_LEGTH 	256

#define THR_HANDLE_IDLE 	0
#define THR_HANDLE_NEW_MSG 	1
#define THR_EXIT	 		2


/* Server's port number */
#define SERVPORT 4200

#define READ_LENGTH_STATE 	0
#define READ_OPCODE_STATE 	1
#define READ_MESSAGE_STATE 	2

#define PRINT_DEBUG(format, args...) printf(format , ## args)
//#define PRINT_DEBUG(format, args...)
/* Big endian */

#if 0
#define DATA_HTONS(val) htons(val)
#define DATA_HTONL(val) htonl(val)
#define DATA_NTOHS(val) ntohs(val)
#define DATA_NTOHL(val) ntohl(val)
#else
/* Little endian */
#define DATA_HTONS(val) (val)
#define DATA_HTONL(val) (val)
#define DATA_NTOHS(val) (val)
#define DATA_NTOHL(val) (val)
#endif

typedef struct _thr_msgInfo
{
	unsigned short opcode;
	char buf[MAX_BUF_LEGTH];
	int length;
}thr_msgInfo;

static int socket_id = 0;
static UInt32 g_capacity = 0;
static int IsUsbConnected = 0;

keep_alive keep_alive_msg;
gsdu_process_status gsdu_process_status_msg;
gsdu_general_status gsdu_general_status_msg;
gsdu_version gsdu_version_msg;
gsdu_error_table gsdu_error_table_msg;
gsdu_single_fault gsdu_single_fault_msg;
int g_thr_cnt = 0;
int g_comm_cnt = 0;

static thr_msgInfo thrMsgInfo = {0};
static int thr_handler_trigger = 0;
static int s_comm_status = 0;

char g_ssdFilePath[256] = {0};
char g_usbFilePath[256] = {0};

int HandleInfoStatusMsg(hiu_status_info_msg* msg, int length);
int udev_test (void);
int test_usb(void);
int test_usb2(void);
int write_sock(int sock_id, const void * buff, int length);
int HandleMsg(unsigned short opcode, char* msg, int length);
void *handler_thr_entry(void *cmd);
int HandleProcessCmd(hiu_process_cmd* msg, int length);
int IsDevConnected(char* device);
int HandleInfoReqCmd(hiu_info_req* msg, int length);
static
unsigned long long getFreeSpace();

/* Pass in 1 parameter which is either the */
/* address or host name of the server, or */
/* set the server name in the #define SERVER ... */
int startClient()
{
	/* Variable and structure definitions. */
	int rc, i, flags, state = READ_LENGTH_STATE;
	unsigned short msg_size;
	unsigned short msg_opcode;
	struct sockaddr_in serveraddr;
	char buffer[MAX_BUF_LEGTH];
	char server[255];
	int wait_for_keep_alive = 0;
	int totalcnt = 0;
	struct hostent *hostp;

	s_comm_status = 0;

	keep_alive_msg.length = DATA_HTONS(2);
	keep_alive_msg.opcode = DATA_HTONS(GSDU_KEEP_ALIVE_OPCODE);

	gsdu_process_status_msg.length = DATA_HTONS(4);
	gsdu_process_status_msg.opcode = DATA_HTONS(GSDU_PROCESS_STATUS);

	gsdu_general_status_msg.length = DATA_HTONS(7);
	gsdu_general_status_msg.opcode = DATA_HTONS(GSDU_GENERAL_STATUS);

	gsdu_version_msg.length = DATA_HTONS(4);
	gsdu_version_msg.opcode = DATA_HTONS(GSDU_SOFTWARE_VERSION);
	gsdu_version_msg.major_number = MAJOR_VERSION;
	gsdu_version_msg.minor_number = MINOR_VERSION;

	gsdu_error_table_msg.length = DATA_HTONS(4);
	gsdu_error_table_msg.opcode = DATA_HTONS(GSDU_SYSTEM_ERROR);

	gsdu_single_fault_msg.length = DATA_HTONS(5);
	gsdu_single_fault_msg.opcode = DATA_HTONS(GSDU_SINGLE_ERROR);

	/* this variable is our reference to the second thread */
	pthread_t handler_thread_id;
	if(pthread_create(&handler_thread_id, NULL, handler_thr_entry, &thr_handler_trigger))
	{
		fprintf(stderr, "Error creating thread\n");
		return 0;
	}

	/* ranran TBD !! */
	/* GetFifoAppComm();*/
	/*InitControlTable();*/
	signal(SIGPIPE, SIG_IGN);
start_connect:
	s_comm_status = 0;
    printf("start_connect\n");
	wait_for_keep_alive = 0;
	/* The socket() function returns a socket */
	/* descriptor representing an endpoint. */
	/* The statement also identifies that the */
	/* INET (Internet Protocol) address family */
	/* with the TCP transport (SOCK_STREAM) */
	/* will be used for this socket. */
	/******************************************/
	/* get a socket descriptor */
	if((socket_id = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("Client-socket() error");
		goto start_connect;
	}
	else
		printf("Client-socket() OK socket_id %d ranran\n",socket_id);
	/* ranran added - no blocking */
	/*fcntl(socket_id, F_SETFL, O_NONBLOCK);*/
	/*If the server hostname is supplied*/

	strcpy(server, SERVER_IP);

	memset(&serveraddr, 0x00, sizeof(struct sockaddr_in));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(SERVPORT);

	if((serveraddr.sin_addr.s_addr = inet_addr(server)) == (unsigned long)INADDR_NONE)
	{

		/* When passing the host name of the server as a */
		/* parameter to this program, use the gethostbyname() */
		/* function to retrieve the address of the host server. */
		/***************************************************/
		/* get host address */
		hostp = gethostbyname(server);
		if(hostp == (struct hostent *)NULL)
		{
			printf("HOST NOT FOUND --> ");
			/* h_errno is usually defined */
			/* in netdb.h */
			printf("h_errno = %d\n",h_errno);
			printf("---This is a client program---\n");
			rc = close(socket_id);
			if (rc < 0)
			{
				printf("error closing socket %d #3", rc);
			}
			goto start_connect;
		}
		memcpy(&serveraddr.sin_addr, hostp->h_addr, sizeof(serveraddr.sin_addr));
	}

	/* After the socket descriptor is received, the */
	/* connect() function is used to establish a */
	/* connection to the server. */
	/***********************************************/
	/* connect() to server. */
	printf("before connect\n");
	if((rc = connect(socket_id, (struct sockaddr *)&serveraddr, sizeof(serveraddr))) < 0)
	{
		printf("Client-connect() error %d ip=%s port=0x%x\n",rc,SERVER_IP,SERVPORT);
		rc = close(socket_id);
		if (rc < 0)
		{
			printf("error closing socket %d #4", rc);
		}
		goto start_connect;
	}
	else
	{
		printf("Connection established...\n");
	}
	printf("before fcntl\n");
	flags = fcntl(socket_id, F_GETFL);
	if (flags == -1)
	{
		printf("fcntl failed #1\n");
	}
	else
	{
		printf("fcntl #1 success\n");
	}
	flags = fcntl(socket_id, F_SETFL, flags | O_NONBLOCK);
	if (flags == -1)
	{
		printf("fcntl failed #2\n");
	}
	else
	{
		printf("fcntl #2 success\n");
	}
	s_comm_status = 1;

	/* Send string to the server using */
	/* the write_sock() function. */
	/*********************************************/
	/* write_sock() some string to the server. */
#if 0
	printf("Sending some string to the  %s...\n", server);
	rc = write_sock(socket_id, data, sizeof(data));

	if(rc < 0)
	{
		perror("Client-write() error");
		rc = getsockopt(socket_id, SOL_SOCKET, SO_ERROR, &temp, &length);
		if(rc == 0)
		{
			/* Print out the asynchronously received error. */
			errno = temp;
			perror("SO_ERROR was %d",rc);
		}
		rc = close(socket_id);
		if (rc < 0)
		{
			printf("error closing socket %d #5", rc);
		}
		goto start_connect;
	}
	else
	{
		printf("Client-write() is OK\n");
		printf("String successfully sent lol!\n");
		printf("Waiting the %s to echo back...\n", server);
	}
#endif
	state = READ_LENGTH_STATE;
	while(1)
	{

		totalcnt = 0;

		if (state == READ_OPCODE_STATE)
		{
			PRINT_DEBUG("7\n");
			goto read_opcode;
		}
		else if (state == READ_MESSAGE_STATE)
		{
			PRINT_DEBUG("8\n");
			goto read_message;
		}
		/* read length  */


		while(totalcnt < 2)
		{
			PRINT_DEBUG("9\n");
			rc = read(socket_id, &buffer[totalcnt], 2-totalcnt);
			PRINT_DEBUG("9.1\n");
			if (rc <= 0)
			{
				PRINT_DEBUG("9.2\n");
				goto inc_keep_alive;
			}
			if(rc > 0)
			{
				PRINT_DEBUG("9.3\n");
				totalcnt += rc;
			}
		}
		PRINT_DEBUG("9.4\n");
		state = READ_OPCODE_STATE;
		totalcnt = 0;
		memcpy(&msg_size,&buffer[0],2 );
		msg_size = DATA_NTOHS(msg_size);
		if ((msg_size < 2 )||(msg_size > 32  ))
		{
			printf("unexpected message size %d\n", msg_size);
			state = READ_LENGTH_STATE;
			PRINT_DEBUG("9.5\n");
			goto inc_keep_alive;
		}
		PRINT_DEBUG("9.6\n");
		/* read opcode */
read_opcode:
		while(totalcnt < 2)
		{
			PRINT_DEBUG("1\n");
			rc = read(socket_id, &buffer[totalcnt+2], 2-totalcnt);
			PRINT_DEBUG("2\n");
			if (rc <= 0)
			{
				PRINT_DEBUG("3\n");
				goto inc_keep_alive;
			}
			if(rc > 0)
			{
				PRINT_DEBUG("3.1\n");
				totalcnt += rc;
			}
		}
		PRINT_DEBUG("9.8\n");
		state = READ_MESSAGE_STATE;
		totalcnt = 0;
		memcpy(&msg_opcode,&buffer[2],2 );
		msg_opcode = DATA_NTOHS(msg_opcode);
		/* read message */
read_message:
		while(totalcnt < msg_size-2)
		{
			PRINT_DEBUG("4\n");
			/* Wait for the server to echo the */
			/* string by using the read() function. */
			/***************************************/
			/* Read data from the server. */
			rc = read(socket_id, &buffer[totalcnt+4], msg_size-2-totalcnt);
			PRINT_DEBUG("4\n");
			if (rc <= 0)
			{
				PRINT_DEBUG("5\n");
				goto inc_keep_alive;
			}
			if(rc > 0)
			{
				PRINT_DEBUG("6\n");
				totalcnt += rc;
			}
		}
		printf(">>msg: ");
		for(i=0;i<(msg_size+2);i++)
			printf("0x%x ",buffer[i]);
		printf("\n");

		state = READ_LENGTH_STATE;
		if (msg_opcode == HIU_KEEP_ALIVE_OPCODE)
		{
			printf(">>keep alive \n");
			wait_for_keep_alive = 0;
		}
		else
		{
			/* for now can only do one job at a time. no more ! */
			/* handle message in another thread */
			if (thr_handler_trigger == THR_HANDLE_NEW_MSG)
			{
				printf("unexpected. not finished previous handling\n");
			}
			else
			{


				memcpy(thrMsgInfo.buf, buffer, (msg_size+2));
				thrMsgInfo.length =  (msg_size+2);
				thrMsgInfo.opcode = msg_opcode;
				printf("change trigger #1\n");
				thr_handler_trigger = THR_HANDLE_NEW_MSG;

			}

		}
inc_keep_alive:
		if (g_comm_cnt++ == 10)
		{
			printf("send keep alive\n");
			g_comm_cnt = 0;
			rc = write_sock(socket_id, &keep_alive_msg, sizeof(keep_alive_msg));
			PRINT_DEBUG("after keep alive\n");
			if(rc < sizeof(keep_alive_msg))
			{
				printf("not expected failed in write_sock keep_alive %d\n", rc);
			}
			/* check if keep alive is not received for too long */
			wait_for_keep_alive++;
			if (wait_for_keep_alive == MAX_KEEPALIVE_TIMEOUT)
			{
				printf("not received alive for too long. restart...\n");
				state = READ_LENGTH_STATE;
				rc = close(socket_id);
				if (rc < 0)
				{
					printf("error closing socket %d #6", rc);
				}
				goto start_connect;
			}
			printf("alive counter %d\n",wait_for_keep_alive);
		}
		OSA_waitMsecs(100);
		/*sleep(1);*/


	}
	/* wait for the second thread to finish */
	if(pthread_join(handler_thread_id, NULL))
	{

		fprintf(stderr, "Error joining thread\n");
		return 0;

	}
	printf("exiting...\n");
	/* When the data has been read, close() */
	/* the socket descriptor. */
	/****************************************/
	/* Close socket descriptor from client side. */
	rc = close(socket_id);
	if (rc < 0)
	{
		printf("error closing socket %d #2", rc);
	}
	exit(0);
	return 0;
}


/* this function is run by the second thread */
void *handler_thr_entry(void *cmd)
{
	printf("handler_thr_entry thread created\n");
	int rc = 0;
/*	int ret = 0;*/
	while(1)
	{

		if((thr_handler_trigger) == THR_EXIT)
		{
			printf("handler_thr_entry exit\n");
			break;
		}
		else if((thr_handler_trigger) == THR_HANDLE_NEW_MSG)
		{

			/* handle task and wait */
			rc = HandleMsg(thrMsgInfo.opcode, thrMsgInfo.buf, thrMsgInfo.length);
			if(rc < 0)
			{
				printf("error in HandleMsg %d\n",rc);
			}


			printf("change trigger #2\n");
			thr_handler_trigger = THR_HANDLE_IDLE;
		}
		/* send general status */
		if (g_thr_cnt++ == 10)
		{

			g_thr_cnt = 0;
			g_capacity = getFreeSpace();
			gsdu_general_status_msg.capacity = DATA_HTONL(g_capacity);
			gsdu_general_status_msg.usb_connect_status = (IsDevConnected(COMMIT_USB_PATH_DEFAULT) == 0) ? 1:0;
			PRINT_DEBUG("send general status\n");
			rc = write_sock(socket_id, &gsdu_general_status_msg, sizeof(gsdu_general_status_msg));
			PRINT_DEBUG("after general status\n");
			if(rc < sizeof(gsdu_general_status_msg))
			{
				printf("writing through socket failed #4 %d\n", rc);
			}
		}
		OSA_waitMsecs(100);
		/*sleep(1);*/
	}
	/* the function must return something - NULL will do */
	return NULL;

}



int HandleMsg(unsigned short opcode, char* msg, int length)
{
	printf("HandleMsg\n");
	int rc = 0;
	switch(opcode)
	{
		case HIU_PROCESS_CMD:
			printf("TBD HIU_PROCESS_CMD \n");
			rc = HandleProcessCmd((hiu_process_cmd *) msg, length);
			if (rc < 0)
			{
				printf("HandleProcessCmd failed %d\n",rc);
			}
			break;

		case HIU_INFO_REQ_CMD:

			printf("TBD HIU_INFO_REQ_CMD \n");
			rc = HandleInfoReqCmd((hiu_info_req *) msg, length);
			if (rc < 0)
			{
				printf("HandleProcessCmd failed %d\n",rc);
			}

			break;

		case HIU_HIU_INFO_STATUS:

			printf("TBD HIU_HIU_INFO_STATUS \n");
			rc = HandleInfoStatusMsg((hiu_status_info_msg *) msg, length);
			if (rc < 0)
			{
				printf("HandleInfoStatusMsg failed %d\n",rc);
			}
			break;
/*
		case 5:
			remove_error(olderr--);
			printf("TBD test_5 not done yet\n");
			break;
*/
		default:
			printf("unexpected message opcode 0x%x",opcode);
			break;

	}
	return rc;
}
/*
typedef struct _hiu_process_msg
{
	unsigned short length;
	unsigned short opcode;
	unsigned char weapon_status;
	unsigned char safet_switch_state;
	unsigned char arm_switch_state;
	unsigned char override_switch_state;
	unsigned char selected_switch_state;
	unsigned char weapon_system_mode;
	unsigned char selected_ammuniation;
	unsigned char selected_fire_type;
	unsigned char selected_camera;
	unsigned char selected_fov;
	unsigned char tod_day;
	unsigned char tod_month;
	unsigned char tod_year;
	unsigned char tod_hour;
	unsigned char tod_min;

}hiu_status_info_msg;*/
int HandleInfoStatusMsg(hiu_status_info_msg* msg, int length)
{

	printf("HandleInfoStatusMsg\n");

	printf("weapon %d\n", msg->weapon_status);
	printf("safety_sw %d\n", msg->safet_switch_state);
	printf("arm_sw %d\n",msg->arm_switch_state);
	printf("ovrd_sw %d\n", msg->override_switch_state);
	printf("sel_sw %d\n", msg->selected_switch_state);
	printf("weapon_mode %d\n", msg->weapon_system_mode);
	printf("ammu %d\n", msg->selected_ammuniation);
	printf("fire_type %d\n", msg->selected_fire_type);
	printf("camera %d\n", msg->selected_camera);
	printf("fov %d\n", msg->selected_fov);
	printf("day %d\n", msg->tod_day);
	printf("month %d\n", msg->tod_month);
	printf("year %d\n", msg->tod_year);
	printf("hour %d\n", msg->tod_hour);
	printf("min %d\n",msg->tod_min);
	return 0;
}

int HandleInfoReqCmd(hiu_info_req* msg, int length)
{
	int rc = 0, res = 0 , i = 0;
	printf("HandleInfoReqCmd\n");

	if (msg->info_req == INFO_REQ_ERROR_TABLE)
	{
		res = fill_error_table(gsdu_error_table_msg.error_table);
		printf("number of errors %d\n",res);
		for(i=0; i < res; i++)
		{
			gsdu_error_table_msg.error_table[i] = DATA_HTONS(gsdu_error_table_msg.error_table[i]);
		}
		gsdu_error_table_msg.number_errors = DATA_HTONS(res);
		gsdu_error_table_msg.length = DATA_HTONS(res*2+4);
		/* length + 2 because through socket we send the length field too (complete message )*/
		rc = write_sock(socket_id, &gsdu_error_table_msg, res*2+6);
		if(rc < (res*2+6))
		{
			printf("writing through socket failed #5.0 %d\n", rc);
		}
	}
	else if(msg->info_req == INFO_REQ_VERSION)
	{
		rc = write_sock(socket_id, &gsdu_version_msg, sizeof(gsdu_version_msg));
		if(rc < sizeof(gsdu_version_msg))
		{
			printf("writing through socket failed #4 %d\n", rc);
		}
	}
	else
	{
		/* ignore message */
		printf("ignore message , wrong value 0x%x\n",msg->info_req);
	}
	return 0;
}

int HandleProcessCmd(hiu_process_cmd* msg, int length)
{

	int rc = 0;
	char stringCmd[256];

	printf("HandleProcessCmd\n");

	/* TBD remove next line */

	gsdu_process_status_msg.process_state = STATUS_INPROCESS;
	gsdu_process_status_msg.process_type = msg->process_cmd;
	printf("<< gsdu_process_status_msg type %d state %d \n",gsdu_process_status_msg.process_type,gsdu_process_status_msg.process_state);
	rc = write_sock(socket_id, &gsdu_process_status_msg, sizeof(gsdu_process_status_msg));
	if(rc < sizeof(gsdu_process_status_msg))
	{
		printf("writing through socket failed #1 %d\n", rc);
	}
	else
		printf("write_sock success !!\n");


	/* no need ntohs */
	switch(msg->process_cmd)
	{
		case RECORD_PROCESS_RECORD:
			printf("RECORD_PROCESS_START\n");
			if (msg->process_type == PROCESS_ENTER)
			{
				rc = Demo_commitStart();
				if (rc < 0)
				{
					printf("Demo_commitStart failed %d\n",rc);
				}
			}
			else
			{
				rc = Demo_commitStop();
				if (rc < 0)
				{
					printf("Demo_commitStop failed %d\n",rc);
				}
			}
			break;

		case RECORD_PROCESS_DELETE:
			if (msg->process_type == PROCESS_ENTER)
			{
				printf("RECORD_PROCESS_DELETE enter\n");
				strcpy(stringCmd, "rm -fr ");
				strcat(stringCmd, COMMIT_VIDEO_PATH_DEFAULT);
				strcat(stringCmd, "/*");
				rc = system(stringCmd);
				if (WIFEXITED(rc))
				{
				    rc = WEXITSTATUS(rc);
				    printf("Exit with status: %d\n", rc);
				}
				else {
				    /* Killed by a signal. */
					rc = -1;
				}
				if (rc != 0)
				{
					printf("system failed %d %s #1",rc,stringCmd);
				}
			}
			else
			{
				printf("RECORD_PROCESS_DELETE exit\n");
				rc = -1;
			}
			break;

		case RECORD_PROCESS_DELETE_FILE:
			printf("RECORD_PROCESS_DELETE_FILE not supported\n");
			rc = -1;
			break;

		case RECORD_PROCESS_COPY:
			if (msg->process_type == PROCESS_ENTER)
			{
				printf("RECORD_PROCESS_COPY enter\n");
				if (IsUsbConnected == 0)
				{
					printf("usb not connected. ignore\n");
					rc = -1;
				}
				else
				{
					strcpy(stringCmd, "cp ");
					strcat(stringCmd, COMMIT_VIDEO_PATH_DEFAULT);
					strcat(stringCmd, "/*");
					strcat(stringCmd, " ");
					strcat(stringCmd, COMMIT_USB_PATH_DEFAULT);
					rc = system(stringCmd);
					if (WIFEXITED(rc))
					{
						rc = WEXITSTATUS(rc);
						printf("Exit with status: %d\n", rc);
					} else {
						/* Killed by a signal. */
						rc = -1;
					}
					if (rc != 0)
					{
						printf("system failed %d %s #1",rc,stringCmd);
					}
				}
			}
			else
			{
				printf("RECORD_PROCESS_COPY exit\n");
				rc = -1;
			}
			break;

			/* mount is currently missing */
		case 5:
			if (msg->process_type == RECORD_PROCESS_UMOUNT)
			{
				printf("RECORD_PROCESS_COPY enter\n");
				if (IsUsbConnected == 0)
				{
					printf("usb not connected. ignore\n");
					rc = -1;
				}
				else
				{
					strcpy(stringCmd, "umount ");
					strcat(stringCmd, COMMIT_USB_PATH_DEFAULT);
					rc = system(stringCmd);
					if (WIFEXITED(rc))
					{
						rc = WEXITSTATUS(rc);
						printf("Exit with status: %d\n", rc);
					} else {
						/* Killed by a signal. */
						rc = -1;
					}
					if (rc != 0)
					{
						printf("system failed %d %s #1",rc,stringCmd);
					}
					else
					{
						printf("remove mount point\n");
						strcpy(stringCmd, "rm -fr ");
						strcat(stringCmd, COMMIT_USB_PATH_DEFAULT);
						rc = system(stringCmd);
						if (WIFEXITED(rc))
						{
						    rc = WEXITSTATUS(rc);
						    printf("Exit with status: %d\n", rc);
						}
						else {
						    /* Killed by a signal. */
							rc = -1;
						}
					}
				}
			}
			else
			{
				printf("RECORD_PROCESS_COPY exit\n");
				rc = -1;
			}
			break;

		default:
			printf("process command unfamiliar. ignore 0x%x\n",msg->process_cmd);
			rc = -1;
			break;
	}

	/* send result process */
	gsdu_process_status_msg.process_state = (rc == 0) ? STATUS_END_OK : STATUS_END_FAIL;
	printf("<< gsdu_process_status_msg type %d state %d \n",gsdu_process_status_msg.process_type,gsdu_process_status_msg.process_state);
	rc = write_sock(socket_id, &gsdu_process_status_msg, sizeof(gsdu_process_status_msg));
	if(rc < sizeof(gsdu_process_status_msg))
	{
		printf("writing through socket failed #3 %d\n", rc);
	}

	return rc;
}

int IsDevConnected(char* device)
{
	DIR           *d;

	d = opendir(device);
	if (d)
	{
		IsUsbConnected = 1;
		printf("usb  connected...\n");
		closedir(d);
		return 0;
	}
	else
	{
		IsUsbConnected = 0;
		printf("usb not connected !\n");
		return -1;
	}

	return 0;
}

int SendSingleError(unsigned short err, unsigned char exist)
{
	int rc = 0;
	printf("send SendSingleError err %d exist %d\n",err,exist);
	gsdu_single_fault_msg.fault_number = DATA_HTONS(err);
	gsdu_single_fault_msg.error_status = exist;
	rc = write_sock(socket_id, &gsdu_single_fault_msg, sizeof(gsdu_single_fault_msg));
	if(rc < sizeof(gsdu_single_fault_msg))
	{
		printf("writing through socket failed #5.1 %d\n",rc);
		return -1;
	}
	return 0;
}


static
unsigned long long getFreeSpace()
{
    struct statvfs statBuf;
    int rc;
    unsigned long long freeSize = 0;
    char mystr[256];

    strcpy(mystr,COMMIT_VIDEO_PATH_DEFAULT);
    strcat(mystr, "/.");
    rc = statvfs(mystr,&statBuf);
    if (rc == 0)
    {
    	freeSize = statBuf.f_bfree*MCFW_IPC_BITS_MAX_PARTITION_USAGE_PERCENT/statBuf.f_blocks;
    }
    else
    {
    	printf("statvfs failed %d\n", rc);
    	freeSize = 0;
    }
    return freeSize;
}




int write_sock(int sock_id, const void * buff, int length)
{
	int i =0;
	if (s_comm_status == 0)
	{
		printf("not trying to write to socket now\n");
		return -1;
	}
	printf("soc_id %d<< ",socket_id);
	for (i=0; i< length; i++)
	{
		printf("0x%x ", ((unsigned char*)buff)[i]);
	}
	printf("\n");
	return write(sock_id,buff,length);
}


int test_usb(void)
{
  int event_type;
  DEVTYPE device_changed;

  int res = 0;
  res = init_usb_detect();
  if(res)
  {
    printf("\nError in initializing libusbdetect, res is %d, trying again...\n", res);
    return res;
  }

 // for(i=0; i < 5; i++)
  while(1)
  {
    res = wait_for_usb_event(&device_changed, &event_type);
    if(!res)
    {
      print_dev_type(device_changed);
      if(event_type)
        printf("\nConnected\n");
      else
        printf("\nDisconnected\n");
    }
    else
    {
      printf("\nError in current iteration of wait_for_usb_event, res is %d, trying again...\n", res);
      continue;
    }
  }

//  exit_usb_detect;
  return 0;
}




int udev_test (void)
{
	struct udev *udev;
	struct udev_enumerate *enumerate;
	struct udev_list_entry *devices, *dev_list_entry;
	struct udev_device *dev;

	printf("udev_test start\n");
	/* Create the udev object */
	udev = udev_new();
	if (!udev) {
		printf("Can't create udev\n");
		exit(1);
	}

	/* Create a list of the devices in the 'hidraw' subsystem. */
	enumerate = udev_enumerate_new(udev);
	udev_enumerate_add_match_subsystem(enumerate, "hidraw");
	udev_enumerate_scan_devices(enumerate);
	devices = udev_enumerate_get_list_entry(enumerate);
	/* For each item enumerated, print out its information.
	   udev_list_entry_foreach is a macro which expands to
	   a loop. The loop will be executed for each member in
	   devices, setting dev_list_entry to a list entry
	   which contains the device's path in /sys. */
	printf("before listing\n");
	udev_list_entry_foreach(dev_list_entry, devices) {
		const char *path;
		printf("in listing\n");
		/* Get the filename of the /sys entry for the device
		   and create a udev_device object (dev) representing it */
		path = udev_list_entry_get_name(dev_list_entry);
		dev = udev_device_new_from_syspath(udev, path);

		/* usb_device_get_devnode() returns the path to the device node
		   itself in /dev. */
		printf("Device Node Path: %s\n", udev_device_get_devnode(dev));

		/* The device pointed to by dev contains information about
		   the hidraw device. In order to get information about the
		   USB device, get the parent device with the
		   subsystem/devtype pair of "usb"/"usb_device". This will
		   be several levels up the tree, but the function will find
		   it.*/
		dev = udev_device_get_parent_with_subsystem_devtype(
		       dev,
		       "usb",
		       "usb_device");
		if (!dev) {
			printf("Unable to find parent usb device.");
			exit(1);
		}

		/* From here, we can call get_sysattr_value() for each file
		   in the device's /sys entry. The strings passed into these
		   functions (idProduct, idVendor, serial, etc.) correspond
		   directly to the files in the directory which represents
		   the USB device. Note that USB strings are Unicode, UCS2
		   encoded, but the strings returned from
		   udev_device_get_sysattr_value() are UTF-8 encoded. */
		printf("  VID/PID: %s %s\n",
		        udev_device_get_sysattr_value(dev,"idVendor"),
		        udev_device_get_sysattr_value(dev, "idProduct"));
		printf("  %s\n  %s\n",
		        udev_device_get_sysattr_value(dev,"manufacturer"),
		        udev_device_get_sysattr_value(dev,"product"));
		printf("  serial: %s\n",
		         udev_device_get_sysattr_value(dev, "serial"));
		udev_device_unref(dev);
	}
	/* Free the enumerator object */
	udev_enumerate_unref(enumerate);

	udev_unref(udev);

	return 0;
}

