/*
 * commit.c
 *
 *  Created on: Jul 8, 2014
 *      Author: ubuntu
 */

#include <commit.h>
#include <commit_client.h>
errorElement g_error_table[MAX_NUM_ERRORS];


int add_error(UInt16 err)
{
	int i = 0 , rc = -1, free_index = -1;
	printf("add_error %d\n", err);

	for(i=0; i <MAX_NUM_ERRORS; i++)
	{
		if (g_error_table[i].is_exist == 1)
		{
			if (g_error_table[i].error_number == err)
				return 0;
		}
		else
			free_index = i;
	}

	if (free_index < 0)
	{
		printf("not enough space in error table!\n");
		return free_index;
	}

	g_error_table[free_index].error_number = err;
	g_error_table[free_index].is_exist = 1;

	rc = SendSingleError(err , 1);
	if (rc != 0)
	{
		printf("SendSingleError failure %d\n",rc);
	}
	return rc;

}

int remove_error(UInt16 err)
{
	int i = 0 , rc = -1;
	printf("remove_error %d\n", err);

	for(i=0; i < MAX_NUM_ERRORS; i++)
	{
		if (g_error_table[i].error_number == err)
		{
			g_error_table[i].is_exist = 0;
			rc = 0;
			break;
		}
	}
	if (rc == 0)
	{
		return SendSingleError(err , 0);
	}

	return -1;
}

int fill_error_table(unsigned short* table)
{
	int index = 0 , i = 0;
	printf("fill_error_table\n");
	for(i=0; i <MAX_NUM_ERRORS; i++)
	{
		if (g_error_table[i].is_exist == 1)
		{
			table[index] = g_error_table[i].error_number;
			index++;
		}
	}


	return index;
}
