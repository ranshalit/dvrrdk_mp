/*
 * commit.h
 *
 *  Created on: Jul 8, 2014
 *      Author: ubuntu
 */

#ifndef COMMIT_H_
#define COMMIT_H_

#include <ti_media_std.h>

#define MAJOR_VERSION	0x1
#define MINOR_VERSION	0x2
//ranran
//#define COMMIT_VIDEO_PATH_DEFAULT 		"/home/root"
#define COMMIT_VIDEO_PATH_DEFAULT 		"/media/sda1"
#define COMMIT_USB_PATH_DEFAULT 		"/media/sdb1"

#define COMMIT_SSD_DEVICE_PATH_DEFAULT 	"/dev/sda1"
#define COMMIT_USB_DEVICE_PATH_DEFAULT 	"/dev/sdb1"


#define COMMIT_CONFIGURE_PATH_DEFAULT 	"/var/ftp/video_conf.ini"
/* max file is in Kbytes , for exmaple if it is (1024*1024) it means 1G byte*/
#define COMMIT_MAX_FILE_SIZE_DEFAULT 	(1024*1024)
#define FTP_SERVER  "192.168.1.13"
#define SERVER_IP   "192.168.1.4"

#define MAX_NUM_ERRORS	32


typedef struct _errorElement
{
	UInt16 error_number;
	UInt8 is_exist;
}errorElement;


int add_error(UInt16 err);
int remove_error(UInt16 err);
int fill_error_table(unsigned short* table);


#endif /* COMMIT_H_ */
