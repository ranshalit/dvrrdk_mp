
. ./parseEnv.sh

.  ./env_${ENV_DDR_MEM}_${ENV_LINUX_MEM}.sh

killall remote_debug_client.out 2> /dev/null

rm -rf $1

cat /dev/null > $1

./bin/remote_debug_client.out ${REMOTE_DEBUG_ADDR}  | tee -a $1 &

watch -n 300 -t ./rotate_logs.sh $1 $2
