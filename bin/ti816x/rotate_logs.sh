# Usage:  rotate_logs log_fully_qualified_name max_size_in_bytes
# Example:
#         rotate_logs /dev/sda1/m3log.txt 1048576    # Rotate "messages" files if size exceeds 1,048,576.

rotate_logs() {
  #set -x
  local LOGFL_FQNAME="$1"
  local LOGFL_DIR=$(dirname ${LOGFL_FQNAME})
  local LOGFL_NAME=$(basename ${LOGFL_FQNAME})
  # ensure we have a base and it is not null
  if [ $# -lt 2 -o -z "$1" ]; then
    echo "Usage:  $0 log_fully_qualified_name max_size_in_bytes" >&2
    echo "Example:" >&2
    echo -e "\t$0 /dev/sda1/m3log.txt 1048576\t# Rotate \"/dev/sda1/m3log.txt\" files if size exceeds 1,048,576" >&2
    return 1
  fi
  # see if we were given a max number and make sure it is a number
  if [ $# -ge 2 ]; then
    if ! echo "$2" | egrep -q '^[0-9]+$'; then
      echo "Maximum size, \"$2\", must be a number." >&2
      return 1
    fi
    local max=$2
  fi
  local i
  local n
  # Find if file greater than max log size
  local LOGFL_LARGE=$( find  ${LOGFL_DIR} -name ${LOGFL_NAME} \
                -size  +${max}c \
                -exec  echo YES \; )
  LOGFL_LARGE=${LOGFL_LARGE:="NO"}
  if [ ${LOGFL_LARGE} = "YES" ]; then
		cp -pf "$LOGFL_FQNAME" "$LOGFL_FQNAME".0
		cat /dev/null > ${LOGFL_FQNAME}
  fi
}

rotate_logs $*

