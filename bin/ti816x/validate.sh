#!/bin/sh
#Script validates kernel bootargs.
#Incase of failure returns 0 else 1
parse_linux_base_address() {
    LINUX_PHYMEM_START_LIST=$(cat /proc/iomem |sed -n 's/^\s*\([0-9a-fA-F]*\).*System RAM.*$/0x\1/p')
    set $LINUX_PHYMEM_START_LIST
    echo $1
    return 0
}

UIMAGE_LINUX_START_ADDR=$(parse_linux_base_address)

if test "`grep -i mem=${RDK_LINUX_MEM} /proc/cmdline`" = ""
then
  echo "*** This RDK version requires mem=${RDK_LINUX_MEM}M in bootargs ***"
  exit 0
else
  echo "*** Bootargs Validated for mem param ***"
fi

if test "`grep -i notifyk.vpssm3_sva=${NOTIFYK_VPSSM3_SVA_ADDR} /proc/cmdline`" = ""
then
  echo "*** This RDK version requires notifyk.vpssm3_sva=${NOTIFYK_VPSSM3_SVA_ADDR} in bootargs ***"
  exit 0
else
  echo "*** Bootargs Validated for notifyk.vpssm3 params ***"
fi

if test "${UIMAGE_LINUX_START_ADDR}" != "${RDK_LINUX_BASE_ADDR}"
then
  echo "*** This RDK version requires LINUX_BASE_ADDRESS=${RDK_LINUX_BASE_ADDR}.Recompile uImage ***"
  RDK_LINUX_LOAD_ADDR=$(echo ${RDK_LINUX_BASE_ADDR} | sed s/./8/7)
  echo "Cmd to rebuild uImage@${RDK_LINUX_BASE_ADDR}:mkimage -A arm -O linux -T kernel -C none -a ${RDK_LINUX_LOAD_ADDR} -e ${RDK_LINUX_LOAD_ADDR} -n 'Linux-2.6.37' -d arch/arm/boot/zImage uImage"
  echo "Refer http://processors.wiki.ti.com/index.php/DM816x_AM389x_PSP_User_Guide#Auto_detection_of_Kernel_Load_Address_and_Run_Time_RAM_Base_Determination"
  exit 0
else
  echo "*** Kernel Base address validated ***"
fi

exit 1
