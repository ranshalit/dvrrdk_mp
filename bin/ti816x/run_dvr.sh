#! /bin/sh

sysctl -w net.core.rmem_max=33554432
sysctl -w net.core.wmem_max=33554432

export LD_LIBRARY_PATH=$PWD/firmware
export QT_QWS_FONTDIR=/usr/lib/fonts

dvrapp_dir="./bin"
mkdir -p $dvrapp_dir/dvrapp_cfg

./init.sh n
./load.sh

. ./parseEnv.sh
.  ./env_${ENV_DDR_MEM}_${ENV_LINUX_MEM}.sh

# 'eth1' use eth1 interface and do DHCP before moving ahead
# 'eth0' use eth0 interface and do DHCP before moving ahead

eth_if="eth0"

cd scripts/

if [ "$eth_if" == "eth1" ]; then ./eth_offload_load.sh;	fi
if [ "$eth_if" == "eth0" ]; then ./eth_org_load.sh;	fi

if [ "$eth_if" == "eth1" ]; then ifconfig eth0 down;	fi

cd -

./bin/sys_pri.out --dmm-pri-set A8 0

$dvrapp_dir/dvrmain --$eth_if -qws hdmi0 hdmi1 &

echo 3 > /proc/sys/vm/drop_caches

./drop_cache.sh &
