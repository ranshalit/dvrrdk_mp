

if ifconfig | sed 's/[ \t].*//;/^$/d' | grep eth0; then
	echo "#"
	echo "# eth0 interface is ACTIVE, skipping DHCP, only setting coalescing !!!"
	echo "#"
	ethtool -C eth0 rx-usecs 500
	exit 0
fi

ifconfig eth0 up
udhcpc -i eth0
ethtool -C eth0 rx-usecs 500