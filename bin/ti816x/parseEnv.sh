parse_cmdline() {

	cmdline=$(cat /proc/cmdline)

	
	if [ $1 == "mem" ] || [ $1 == "ddr_mem" ] || [ $1 == "notifyk.vpssm3_sva" ]
		then
			target_var=$1
	else
		return 1			
	fi

	for item in $cmdline
	do
		case $item in
			$target_var=*)
				echo ${item//$target_var=}
				return 0
			;;
		esac
	done

	return 1
}

ENV_LINUX_MEM=$(parse_cmdline mem)
ENV_DDR_MEM=$(parse_cmdline ddr_mem)
ENV_NOTIFYK_VPSSM3_SVA_ADDR=$(parse_cmdline notifyk.vpssm3_sva)


export ENV_LINUX_MEM
export ENV_DDR_MEM
export ENV_NOTIFYK_VPSSM3_SVA_ADDR
