
if ifconfig | sed 's/[ \t].*//;/^$/d' | grep eth1; then
	echo "#"
	echo "# eth1 interface is ACTIVE, cannot load it dynamically !!!"
	echo "#"
	exit 1
fi

insmod ../kermod/eth_offload.ko gro_enable=1 gso_enable=0 shared_mem_phys_addr=${ETH_OFFLOAD_ADDR}

ifconfig eth1 up
udhcpc -i eth1