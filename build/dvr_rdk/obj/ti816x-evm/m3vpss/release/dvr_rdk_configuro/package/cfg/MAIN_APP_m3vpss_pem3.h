/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-y25
 */

#include <xdc/std.h>

#define XDC_CFG_TILER_MEM_BASE_ADDR 0xa0000000

#define XDC_CFG_TILER_MEM_8BIT_SIZE 0x8000000

#define XDC_CFG_TILER_MEM_16BIT_SIZE 0x6000000

#define XDC_CFG_TILER_MEM_32BIT_SIZE 0x0

extern int xdc_runtime_Startup__EXECFXN__C;

extern int xdc_runtime_Startup__RESETFXN__C;

#ifndef ti_sysbios_knl_Task__include
#ifndef __nested__
#define __nested__
#include <ti/sysbios/knl/Task.h>
#undef __nested__
#else
#include <ti/sysbios/knl/Task.h>
#endif
#endif

extern ti_sysbios_knl_Task_Struct TSK_idle;

