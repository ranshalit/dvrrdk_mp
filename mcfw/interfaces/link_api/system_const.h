/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/
/**
    \ingroup LINK_API
    \defgroup SYSTEM_CONST_API System constants API

    Enlists system wide constants and enums

    @{
*/

/**
    \file system_const.h
    \brief Common system constants
*/

#ifndef _SYSTEM_CONST_H_
#define _SYSTEM_CONST_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Define's */

/* @{ */

/**
    \brief Buffer alignment

    SUPPORTED in ALL platforms
*/
#define SYSTEM_BUFFER_ALIGNMENT         (16)

/**
    \brief Maximum output queues

    SUPPORTED in ALL platforms
*/
#define SYSTEM_MAX_OUT_QUE              (6u)

/**
    \brief Maximum channels per output queue

    SUPPORTED in ALL platforms
*/
#define SYSTEM_MAX_CH_PER_OUT_QUE       (64u)

/**
    \brief Maximum planes

    SUPPORTED in ALL platforms
*/
#define SYSTEM_MAX_PLANES               (3)

/**
    \brief Capture instance - VIP0 - Port A

    SUPPORTED in ALL platforms
*/
#define SYSTEM_CAPTURE_INST_VIP0_PORTA (0u)

/**
    \brief Capture instance - VIP0 - Port B

    SUPPORTED in ALL platforms
*/
#define SYSTEM_CAPTURE_INST_VIP0_PORTB (1u)

/**
    \brief Capture instance - VIP1 - Port A

    SUPPORTED in ALL platforms
*/
#define SYSTEM_CAPTURE_INST_VIP1_PORTA (2u)

/**
    \brief Capture instance - VIP1 - Port B

    SUPPORTED in ALL platforms
*/
#define SYSTEM_CAPTURE_INST_VIP1_PORTB (3u)

/**
    \brief Capture instance - MAX instances

    SUPPORTED in ALL platforms
*/
#define SYSTEM_CAPTURE_INST_MAX        (4u)

/**
    \brief Write back capture instance from SC5

    SUPPORTED on TI8107 platform
*/
#define SYSTEM_CAPTURE_INST_SC5_WB2     (4u)

/**
    \brief Maximum Write Capture Instance
*/
#define SYSTEM_WB_CAPTURE_INST_MAX     (1u)


/**
    \brief Driver ID base for video decoder driver class.

    SUPPORTED in ALL platforms
*/
#define SYSTEM_DEVICE_VID_DEC_DRV_BASE       (0x00000400u)

/**
    \brief TVP5158 video decoder driver ID used at the time of SYSTEM_create()

    SUPPORTED in Platforms with TVP5158
*/
#define SYSTEM_DEVICE_VID_DEC_TVP5158_DRV    (SYSTEM_DEVICE_VID_DEC_DRV_BASE + 0x0000u)

/**
    \brief Default display sequence Id

    SUPPORTED in ALL platforms
*/
#define SYSTEM_DISPLAY_SEQID_DEFAULT         (0)

/* @} */

/* Data structure's */

/**
    \brief Video Data format.

    SUPPORTED in ALL platforms
 */
typedef enum
{
    SYSTEM_DF_YUV422I_UYVY = 0x0000,
    /**< YUV 422 Interleaved format - UYVY. */
    SYSTEM_DF_YUV422I_YUYV,
    /**< YUV 422 Interleaved format - YUYV. */
    SYSTEM_DF_YUV422I_YVYU,
    /**< YUV 422 Interleaved format - YVYU. */
    SYSTEM_DF_YUV422I_VYUY,
    /**< YUV 422 Interleaved format - VYUY. */
    SYSTEM_DF_YUV422SP_UV,
    /**< YUV 422 Semi-Planar - Y separate, UV interleaved. */
    SYSTEM_DF_YUV422SP_VU,
    /**< YUV 422 Semi-Planar - Y separate, VU interleaved. */
    SYSTEM_DF_YUV422P,
    /**< YUV 422 Planar - Y, U and V separate. */
    SYSTEM_DF_YUV420SP_UV,
    /**< YUV 420 Semi-Planar - Y separate, UV interleaved. */
    SYSTEM_DF_YUV420SP_VU,
    /**< YUV 420 Semi-Planar - Y separate, VU interleaved. */
    SYSTEM_DF_YUV420P,
    /**< YUV 420 Planar - Y, U and V separate. */
    SYSTEM_DF_YUV444P,
    /**< YUV 444 Planar - Y, U and V separate. */
    SYSTEM_DF_YUV444I,
    /**< YUV 444 interleaved - YUVYUV... */
    SYSTEM_DF_RGB16_565 = 0x1000,
    /**< RGB565 16-bit - 5-bits R, 6-bits G, 5-bits B. */
    SYSTEM_DF_ARGB16_1555,
    /**< ARGB1555 16-bit - 5-bits R, 5-bits G, 5-bits B, 1-bit Alpha (MSB). */
    SYSTEM_DF_RGBA16_5551,
    /**< RGBA5551 16-bit - 5-bits R, 5-bits G, 5-bits B, 1-bit Alpha (LSB). */
    SYSTEM_DF_ARGB16_4444,
    /**< ARGB4444 16-bit - 4-bits R, 4-bits G, 4-bits B, 4-bit Alpha (MSB). */
    SYSTEM_DF_RGBA16_4444,
    /**< RGBA4444 16-bit - 4-bits R, 4-bits G, 4-bits B, 4-bit Alpha (LSB). */
    SYSTEM_DF_ARGB24_6666,
    /**< ARGB6666 24-bit - 6-bits R, 6-bits G, 6-bits B, 6-bit Alpha (MSB). */
    SYSTEM_DF_RGBA24_6666,
    /**< RGBA6666 24-bit - 6-bits R, 6-bits G, 6-bits B, 6-bit Alpha (LSB). */
    SYSTEM_DF_RGB24_888,
    /**< RGB24 24-bit - 8-bits R, 8-bits G, 8-bits B. */
    SYSTEM_DF_ARGB32_8888,
    /**< ARGB32 32-bit - 8-bits R, 8-bits G, 8-bits B, 8-bit Alpha (MSB). */
    SYSTEM_DF_RGBA32_8888,
    /**< RGBA32 32-bit - 8-bits R, 8-bits G, 8-bits B, 8-bit Alpha (LSB). */
    SYSTEM_DF_BGR16_565,
    /**< BGR565 16-bit -   5-bits B, 6-bits G, 5-bits R. */
    SYSTEM_DF_ABGR16_1555,
    /**< ABGR1555 16-bit - 5-bits B, 5-bits G, 5-bits R, 1-bit Alpha (MSB). */
    SYSTEM_DF_ABGR16_4444,
    /**< ABGR4444 16-bit - 4-bits B, 4-bits G, 4-bits R, 4-bit Alpha (MSB). */
    SYSTEM_DF_BGRA16_5551,
    /**< BGRA5551 16-bit - 5-bits B, 5-bits G, 5-bits R, 1-bit Alpha (LSB). */
    SYSTEM_DF_BGRA16_4444,
    /**< BGRA4444 16-bit - 4-bits B, 4-bits G, 4-bits R, 4-bit Alpha (LSB). */
    SYSTEM_DF_ABGR24_6666,
    /**< ABGR6666 24-bit - 6-bits B, 6-bits G, 6-bits R, 6-bit Alpha (MSB). */
    SYSTEM_DF_BGR24_888,
    /**< BGR888 24-bit - 8-bits B, 8-bits G, 8-bits R. */
    SYSTEM_DF_ABGR32_8888,
    /**< ABGR8888 32-bit - 8-bits B, 8-bits G, 8-bits R, 8-bit Alpha (MSB). */
    SYSTEM_DF_BGRA24_6666,
    /**< BGRA6666 24-bit - 6-bits B, 6-bits G, 6-bits R, 6-bit Alpha (LSB). */
    SYSTEM_DF_BGRA32_8888,
    /**< BGRA8888 32-bit - 8-bits B, 8-bits G, 8-bits R, 8-bit Alpha (LSB). */
    SYSTEM_DF_BITMAP8 = 0x2000,
    /**< BITMAP 8bpp. */
    SYSTEM_DF_BITMAP4_LOWER,
    /**< BITMAP 4bpp lower address in CLUT. */
    SYSTEM_DF_BITMAP4_UPPER,
    /**< BITMAP 4bpp upper address in CLUT. */
    SYSTEM_DF_BITMAP2_OFFSET0,
    /**< BITMAP 2bpp offset 0 in CLUT. */
    SYSTEM_DF_BITMAP2_OFFSET1,
    /**< BITMAP 2bpp offset 1 in CLUT. */
    SYSTEM_DF_BITMAP2_OFFSET2,
    /**< BITMAP 2bpp offset 2 in CLUT. */
    SYSTEM_DF_BITMAP2_OFFSET3,
    /**< BITMAP 2bpp offset 3 in CLUT. */
    SYSTEM_DF_BITMAP1_OFFSET0,
    /**< BITMAP 1bpp offset 0 in CLUT. */
    SYSTEM_DF_BITMAP1_OFFSET1,
    /**< BITMAP 1bpp offset 1 in CLUT. */
    SYSTEM_DF_BITMAP1_OFFSET2,
    /**< BITMAP 1bpp offset 2 in CLUT. */
    SYSTEM_DF_BITMAP1_OFFSET3,
    /**< BITMAP 1bpp offset 3 in CLUT. */
    SYSTEM_DF_BITMAP1_OFFSET4,
    /**< BITMAP 1bpp offset 4 in CLUT. */
    SYSTEM_DF_BITMAP1_OFFSET5,
    /**< BITMAP 1bpp offset 5 in CLUT. */
    SYSTEM_DF_BITMAP1_OFFSET6,
    /**< BITMAP 1bpp offset 6 in CLUT. */
    SYSTEM_DF_BITMAP1_OFFSET7,
    /**< BITMAP 1bpp offset 7 in CLUT. */
    SYSTEM_DF_BITMAP8_BGRA32,
    /**< BITMAP 8bpp BGRA32. */
    SYSTEM_DF_BITMAP4_BGRA32_LOWER,
    /**< BITMAP 4bpp BGRA32 lower address in CLUT. */
    SYSTEM_DF_BITMAP4_BGRA32_UPPER,
    /**< BITMAP 4bpp BGRA32 upper address in CLUT. */
    SYSTEM_DF_BITMAP2_BGRA32_OFFSET0,
    /**< BITMAP 2bpp BGRA32 offset 0 in CLUT. */
    SYSTEM_DF_BITMAP2_BGRA32_OFFSET1,
    /**< BITMAP 2bpp BGRA32 offset 1 in CLUT. */
    SYSTEM_DF_BITMAP2_BGRA32_OFFSET2,
    /**< BITMAP 2bpp BGRA32 offset 2 in CLUT. */
    SYSTEM_DF_BITMAP2_BGRA32_OFFSET3,
    /**< BITMAP 2bpp BGRA32 offset 3 in CLUT. */
    SYSTEM_DF_BITMAP1_BGRA32_OFFSET0,
    /**< BITMAP 1bpp BGRA32 offset 0 in CLUT. */
    SYSTEM_DF_BITMAP1_BGRA32_OFFSET1,
    /**< BITMAP 1bpp BGRA32 offset 1 in CLUT. */
    SYSTEM_DF_BITMAP1_BGRA32_OFFSET2,
    /**< BITMAP 1bpp BGRA32 offset 2 in CLUT. */
    SYSTEM_DF_BITMAP1_BGRA32_OFFSET3,
    /**< BITMAP 1bpp BGRA32 offset 3 in CLUT. */
    SYSTEM_DF_BITMAP1_BGRA32_OFFSET4,
    /**< BITMAP 1bpp BGRA32 offset 4 in CLUT. */
    SYSTEM_DF_BITMAP1_BGRA32_OFFSET5,
    /**< BITMAP 1bpp BGRA32 offset 5 in CLUT. */
    SYSTEM_DF_BITMAP1_BGRA32_OFFSET6,
    /**< BITMAP 1bpp BGRA32 offset 6 in CLUT. */
    SYSTEM_DF_BITMAP1_BGRA32_OFFSET7,
    /**< BITMAP 1bpp BGRA32 offset 7 in CLUT. */
    SYSTEM_DF_BAYER_RAW = 0x3000,
    /**< Bayer pattern. */
    SYSTEM_DF_RAW_VBI,
    /**< Raw VBI data. */
    SYSTEM_DF_RAW,
    /**< Raw data - Format not interpreted. */
    SYSTEM_DF_MISC,
    /**< For future purpose. */
    SYSTEM_DF_INVALID
    /**< Invalid data format. Could be used to initialize variables. */
}System_VideoDataFormat;


/**
    \brief Video Scan Format.

    SUPPORTED in ALL platforms
 */
typedef enum
{
    SYSTEM_SF_INTERLACED = 0,
    /**< Interlaced mode. */
    SYSTEM_SF_PROGRESSIVE,
    /**< Progressive mode. */
    SYSTEM_SF_MAX
    /**< Should be the last value of this enumeration.
         Will be used by driver for validating the input parameters. */
} System_VideoScanFormat;

/**
    \brief Enum for buffer memory type.

    SUPPORTED in ALL platforms
 */
typedef enum
{
    SYSTEM_MT_NONTILEDMEM = 0,
    /**< 1D non-tiled memory. */
    SYSTEM_MT_TILEDMEM,
    /**< 2D tiled memory. */
    SYSTEM_MT_MAX
    /**< Should be the last value of this enumeration.
         Will be used by driver for validating the input parameters. */
} System_MemoryType;

/**
    \brief Video standards..

    SUPPORTED in ALL platforms
 */
typedef enum
{
    SYSTEM_STD_NTSC = 0u,
    /**< 720x480 30FPS interlaced NTSC standard. */
    SYSTEM_STD_PAL,
    /**< 720x576 30FPS interlaced PAL standard. */

    SYSTEM_STD_480I,
    /**< 720x480 30FPS interlaced SD standard. */
    SYSTEM_STD_576I,
    /**< 720x576 30FPS interlaced SD standard. */

    SYSTEM_STD_CIF,
    /**< Interlaced, 360x120 per field NTSC, 360x144 per field PAL. */
    SYSTEM_STD_HALF_D1,
    /**< Interlaced, 360x240 per field NTSC, 360x288 per field PAL. */
    SYSTEM_STD_D1,
    /**< Interlaced, 720x240 per field NTSC, 720x288 per field PAL. */

    SYSTEM_STD_480P,
    /**< 720x480 60FPS progressive ED standard. */
    SYSTEM_STD_576P,
    /**< 720x576 60FPS progressive ED standard. */

    SYSTEM_STD_720P_60,
    /**< 1280x720 60FPS progressive HD standard. */
    SYSTEM_STD_720P_50,
    /**< 1280x720 50FPS progressive HD standard. */

    SYSTEM_STD_1080I_60,
    /**< 1920x1080 30FPS interlaced HD standard. */
    SYSTEM_STD_1080I_50,
    /**< 1920x1080 50FPS interlaced HD standard. */

    SYSTEM_STD_1080P_60,
    /**< 1920x1080 60FPS progressive HD standard. */
    SYSTEM_STD_1080P_50,
    /**< 1920x1080 50FPS progressive HD standard. */

    SYSTEM_STD_1080P_24,
    /**< 1920x1080 24FPS progressive HD standard. */
    SYSTEM_STD_1080P_30,
    /**< 1920x1080 30FPS progressive HD standard. */

    /* Vesa standards from here Please add all SMTPE and CEA standard enums
       above this only. this is to ensure proxy Oses compatibility
     */
#ifdef HDVPSS_VER_01_00_01_36
    SYSTEM_STD_VGA_60,
#else
    SYSTEM_STD_VGA_60 = 0x100,
#endif
    /**< 640x480 60FPS VESA standard. */
    SYSTEM_STD_VGA_72,
    /**< 640x480 72FPS VESA standard. */
    SYSTEM_STD_VGA_75,
    /**< 640x480 75FPS VESA standard. */
    SYSTEM_STD_VGA_85,
    /**< 640x480 85FPS VESA standard. */

#ifndef HDVPSS_VER_01_00_01_36
    SYSTEM_STD_WVGA_60,
    /**< 800x480 60PFS WVGA */
#endif

    SYSTEM_STD_SVGA_60,
    /**< 800x600 60FPS VESA standard. */
    SYSTEM_STD_SVGA_72,
    /**< 800x600 72FPS VESA standard. */
    SYSTEM_STD_SVGA_75,
    /**< 800x600 75FPS VESA standard. */
    SYSTEM_STD_SVGA_85,
    /**< 800x600 85FPS VESA standard. */

#ifndef HDVPSS_VER_01_00_01_36
    SYSTEM_STD_WSVGA_70,
    /**< 1024x600 70FPS standard. */
#endif

    SYSTEM_STD_XGA_60,
    /**< 1024x768 60FPS VESA standard. */
    SYSTEM_STD_XGA_70,
    /**< 1024x768 72FPS VESA standard. */
    SYSTEM_STD_XGA_75,
    /**< 1024x768 75FPS VESA standard. */
    SYSTEM_STD_XGA_85,
    /**< 1024x768 85FPS VESA standard. */

#ifndef HDVPSS_VER_01_00_01_36
    SYSTEM_STD_1368_768_60,
    /**< 1368x768 60 PFS VESA>*/
    SYSTEM_STD_1366_768_60,
    /**< 1366x768 60 PFS VESA>*/
    SYSTEM_STD_1360_768_60,
    /**< 1360x768 60 PFS VESA>*/
#endif

    SYSTEM_STD_WXGA_60,
    /**< 1280x768 60FPS VESA standard. */
    SYSTEM_STD_WXGA_75,
    /**< 1280x768 75FPS VESA standard. */
    SYSTEM_STD_WXGA_85,
    /**< 1280x768 85FPS VESA standard. */

#ifndef HDVPSS_VER_01_00_01_36
    SYSTEM_STD_1440_900_60,
    /**< 1440x900 60 PFS VESA>*/
#endif

    SYSTEM_STD_SXGA_60,
    /**< 1280x1024 60FPS VESA standard. */
    SYSTEM_STD_SXGA_75,
    /**< 1280x1024 75FPS VESA standard. */
    SYSTEM_STD_SXGA_85,
    /**< 1280x1024 85FPS VESA standard. */

#ifndef HDVPSS_VER_01_00_01_36
    SYSTEM_STD_WSXGAP_60,
    /**< 1680x1050 60 PFS VESA>*/
#endif

    SYSTEM_STD_SXGAP_60,
    /**< 1400x1050 60FPS VESA standard. */
    SYSTEM_STD_SXGAP_75,
    /**< 1400x1050 75FPS VESA standard. */

    SYSTEM_STD_UXGA_60,
    /**< 1600x1200 60FPS VESA standard. */

    /* Multi channel standards from here Please add all VESA standards enums
       above this only. this is to ensure proxy Oses compatibility */
#ifdef HDVPSS_VER_01_00_01_36
    SYSTEM_STD_MUX_2CH_D1,
#else
    SYSTEM_STD_MUX_2CH_D1 = 0x200,
#endif
    /**< Interlaced, 2Ch D1, NTSC or PAL. */

    SYSTEM_STD_MUX_2CH_HALF_D1,
    /**< Interlaced, 2ch half D1, NTSC or PAL. */
    SYSTEM_STD_MUX_2CH_CIF,
    /**< Interlaced, 2ch CIF, NTSC or PAL. */
    SYSTEM_STD_MUX_4CH_D1,
    /**< Interlaced, 4Ch D1, NTSC or PAL. */
    SYSTEM_STD_MUX_4CH_CIF,
    /**< Interlaced, 4Ch CIF, NTSC or PAL. */
    SYSTEM_STD_MUX_4CH_HALF_D1,
    /**< Interlaced, 4Ch Half-D1, NTSC or PAL. */
    SYSTEM_STD_MUX_8CH_CIF,
    /**< Interlaced, 8Ch CIF, NTSC or PAL. */
    SYSTEM_STD_MUX_8CH_HALF_D1,
    /**< Interlaced, 8Ch Half-D1, NTSC or PAL. */

    SYSTEM_STD_MUX_4CH_960H,
    /**< Interlaced ,4Ch 960H , NTSC or PAL */

    /* Auto detect and Custom standards Please add all multi channel standard
       enums above this only. this is to ensure proxy Oses compatibility */

#ifdef HDVPSS_VER_01_00_01_36
    SYSTEM_STD_AUTO_DETECT,
#else
    SYSTEM_STD_AUTO_DETECT = 0x300,
#endif
    /**< Auto-detect standard. Used in capture mode. */
    SYSTEM_STD_CUSTOM,
    /**< Custom standard used when connecting to external LCD etc...
         The video timing is provided by the application.
         Used in display mode. */
    SYSTEM_STD_INVALID = 0xFFFF
    /**< Invalid standard used for initializations and error checks*/


} SYSTEM_Standard;

/**
    \brief CPU revision ID.

    SUPPORTED in ALL platforms
 */
typedef enum
{
    SYSTEM_PLATFORM_CPU_REV_1_0,
    /**< CPU revision 1.0. */
    SYSTEM_PLATFORM_CPU_REV_1_1,
    /**< CPU revision 1.1. */
    SYSTEM_PLATFORM_CPU_REV_2_0,
    /**< CPU revision 2.0. */
    SYSTEM_PLATFORM_CPU_REV_2_1,
    /**< CPU revision 2.1. */
    SYSTEM_PLATFORM_CPU_REV_UNKNOWN,
    /**< Unknown/unsupported CPU revision. */
    SYSTEM_PLATFORM_CPU_REV_MAX
    /**< Max CPU revision. */
} System_PlatformCpuRev;



/**
    \brief Buffer type - Video frame or Video bitstream

    SUPPORTED in ALL platforms
 */
typedef enum  {

    SYSTEM_BUF_TYPE_VIDFRAME,
    SYSTEM_BUF_TYPE_VIDBITSTREAM,
    SYSTEM_BUF_TYPE_MAX

} System_BufType;



/**
    \brief Enum for VENCs present in the system

    SUPPORTED in ALL platforms
 */
typedef enum
{
    SYSTEM_DC_VENC_HDMI = 0,
    /**< Enable the Module */
    SYSTEM_DC_VENC_HDCOMP = 1,
    /**< Enable the Module */
    SYSTEM_DC_VENC_DVO2 = 2,
    /**< Enable the Module */
    SYSTEM_DC_VENC_SD = 3,
    /**< Enable the Module */
    SYSTEM_DC_MAX_VENC = 4
    /**< Enable the Module */
} System_VencTypes;

/**
    \brief Enum for the output clock modules
    Caution: Do not change the enum values.

    SUPPORTED in ALL platforms
 */
typedef enum
{
    SYSTEM_VPLL_OUTPUT_VENC_RF = 0,
    /**< Pixel clock frequency for the RF,
         Note: SD Pixel frequency will RF Clock Freq/4.
         This is Video0 PLL for DM385 */
    SYSTEM_VPLL_OUTPUT_VENC_D,
    /**< VencD output clock.
         This is Video1 PLL for DM385 */
    SYSTEM_VPLL_OUTPUT_VENC_A,
    /**< VencA output clock. For the TI814X, this is for DVO1.
         This is HDMI PLL for DM385 */
    SYSTEM_VPLL_OUTPUT_HDMI,
    /**< HDMI output clock, this is used for HDMI display TI814X only. */
    SYSTEM_VPLL_OUTPUT_MAX_VENC
    /**< This should be last Enum. */
} System_VPllOutputClk;


/**
    \brief Enums for HDCOMP DAC sync output selection.

    SUPPORTED ONLY in TI816X and TI810X platforms
 */
typedef enum
{
    SYSTEM_HDCOMP_SYNC_SRC_DVO1 = 0,
    /**< HDCOMP(DAC) HSYNC/VSYNC analog outputs are selected instead of
         VOUT1_HSYNC/VSYNC (DVO1). */
    SYSTEM_HDCOMP_SYNC_SRC_DVO2 = 1
    /**< HDCOMP(DAC) HSYNC/VSYNC analog outputs are selected instead of
         VOUT0_AVID/FID (DVO2). */
} System_HdCompSyncSource;

/**
 *  \brief Configuration parameter specific to Discrete Sync mode
 */
typedef enum
{
    SYSTEM_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_EMBEDDED_SYNC = 0,
    /**< Single Channel non multiplexed mode */
    SYSTEM_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_LINE_MUX_EMBEDDED_SYNC,
    /**< Multi-channel line-multiplexed mode */
    SYSTEM_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_PIXEL_MUX_EMBEDDED_SYNC,
    /**< Multi-channel pixel muxed */
    SYSTEM_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_DISCRETE_SYNC_HSYNC_VBLK,
    /**< Single Channel non multiplexed discrete sync mode with HSYNC and
        VBLK as control signals. */
    SYSTEM_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_DISCRETE_SYNC_HSYNC_VSYNC,
    /**< Single Channel non multiplexed discrete sync mode with HSYNC and
        VSYNC as control signals. */
    SYSTEM_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_DISCRETE_SYNC_ACTVID_VBLK,
    /**< Single Channel non multiplexed discrete sync mode with ACTVID and
        VBLK as control signals. */
    SYSTEM_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_DISCRETE_SYNC_ACTVID_VSYNC,
    /**< Single Channel non multiplexed discrete sync mode with ACTVID and
        VBLK as control signals. */
    SYSTEM_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_LINE_MUX_SPLIT_LINE_EMBEDDED_SYNC,
    /**< Multi-channel line-multiplexed mode - split line mode */
    SYSTEM_CAPT_VIDEO_CAPTURE_MODE_MAX
    /**< Maximum modes */
} System_CaptVideoCaptureMode;

/**
 * \brief Video interface mode
 */
typedef enum
{
    SYSTEM_CAPT_VIDEO_IF_MODE_8BIT = 0,
    /**< Embedded sync mode:  8bit - BT656 standard  */
    SYSTEM_CAPT_VIDEO_IF_MODE_16BIT,
    /**< Embedded sync mode:  16bit - BT1120 standard  */
    SYSTEM_CAPT_VIDEO_IF_MODE_24BIT,
    /**< Embedded sync mode:  24bit */
    SYSTEM_CAPT_VIDEO_IF_MODE_MAX
    /**< Maximum modes */
} System_CaptVideoIfMode;

/**
 *  \brief Enum for Control Channel Selection
 *  It describes channels numbers from extract control code and Vertical
 *  Ancillary Data
 */
typedef enum
{
     SYSTEM_VIP_CTRL_CHAN_SEL_7_0 = 0,
     /**< Use data[7:0] to extract control codes and Vertical Ancillary Data */

     SYSTEM_VIP_CTRL_CHAN_SEL_15_8,
     /**< Use data[15:8] to extract control codes and Vertical Ancillary Data */

     SYSTEM_VIP_CTRL_CHAN_SEL_23_16,
     /**< Use data[23:16] to extract control codes and Vertical Ancillary Data
      */

     SYSTEM_VIP_CTRL_CHAN_DONT_CARE = -1
     /**< Value is dont care */

} System_VipCtrlChanSel;

/**
 *  \brief It is used only for Discrete Sync
 */
typedef enum
{
    SYSTEM_VIP_LINE_CAPTURE_STYLE_HSYNC = 0,
    /**< Use HSYNC style linecapture */

    SYSTEM_VIP_LINE_CAPTURE_STYLE_ACTVID,
    /**< Use ACTVID style line capture */

    SYSTEM_VIP_LINE_CAPTURE_STYLE_DONT_CARE = -1
    /**< Value is dont care */

} System_VipLineCaptureStyle;

/**
 *  \brief It is only used for Discrete Sync
 */
typedef enum
{
    SYSTEM_VIP_FID_DETECT_MODE_PIN = 0,
    /**< Take FID from pin */

    SYSTEM_VIP_FID_DETECT_MODE_VSYNC,
    /**< FID is determined by VSYNC skew */

    SYSTEM_VIP_FID_DETECT_MODE_DONT_CARE = -1
    /**< Value is dont care */

} System_VipFidDetectMode;

/**
 *  \brief VIP Polarity
 */
typedef enum
{
    SYSTEM_VIP_POLARITY_LOW = 0,
    /**< low Polarity */

    SYSTEM_VIP_POLARITY_HIGH,
    /**< high Polarity */

    SYSTEM_VIP_POLARITY_DONT_CARE = -1
    /**< Value is dont care */

} System_VipPolarity;

/**
 *  \brief Pixel clock edge polarity
 */
typedef enum
{
    SYSTEM_VIP_PIX_CLK_EDGE_POL_RISING = 0,
    /**< Rising Edge is active PIXCLK edge */

    SYSTEM_VIP_PIX_CLK_EDGE_POL_FALLING,
    /**< Falling Edge is active PIXCLK edge */

    SYSTEM_VIP_PIX_CLK_EDGE_POL_DONT_CARE = -1
    /**< Value is dont care */

} System_VipPixClkEdgePol;

/**
 *  \brief VIP Field ID
 */
typedef enum
{
    SYSTEM_VIP_FID_EVEN = 0,
    /**< Even field id */

    SYSTEM_VIP_FID_ODD,
    /**< Odd field id */

    SYSTEM_VIP_FID_DONT_CARE = -1
    /**< Value is dont care */

} System_VipFid;

/**
 *  \brief It gives the imformation in 8b interface mode from where to extract
 *  Vertical Ancillary data
 */
typedef enum
{
    SYSTEM_VIP_ANC_CH_SEL_8B_LUMA_SIDE = 0,
    /**< Extract 8b Mode Vertical Ancillary Data from Luma Sites */

    SYSTEM_VIP_ANC_CH_SEL_8B_CHROMA_SIDE,
    /**< Extract 8b Mode Vertical Ancillary Data from Chroma Sites */

    SYSTEM_VIP_ANC_CH_SEL_DONT_CARE = -1
    /**< Value is dont care */

} System_VipAncChSel8b;

/**
 *  \brief In embedded sync for 2x/4x mux mode there are two way to extract
 *  soruce number, one is from least significant nibble of the XV/fvh codeword
 *  and other is least significant nibble of a horizontal blanking pixel value
 */
typedef enum
{
    SYSTEM_VIP_SRC_NUM_POS_LS_NIBBLE_OF_CODEWORD = 0,
    /**< srcnum is in the least significant nibble of the XV/fvh codeword */

    SYSTEM_VIP_SRC_NUM_POS_LS_NIBBLE_OF_HOR_BLNK_PIX,
    /**< srcnum is in the least significant nibble of a horizontal blanking
     * pixelvalue
     */

    SYSTEM_VIP_SRC_NUM_POS_DONT_CARE = -1
    /**< Value is dont care */

} System_VipSrcNumPos;

/**
 *  \brief Enum for specifying per-defined CSC co-effs
 */
typedef enum
{
    SYSTEM_CSC_MODE_SDTV_VIDEO_R2Y = 0,
    /**< Select coefficient for SDTV Video */
    SYSTEM_CSC_MODE_SDTV_VIDEO_Y2R,
    /**< Select coefficient for SDTV Video */
    SYSTEM_CSC_MODE_SDTV_GRAPHICS_R2Y,
    /**< Select coefficient for SDTV Graphics */
    SYSTEM_CSC_MODE_SDTV_GRAPHICS_Y2R,
    /**< Select coefficient for SDTV Graphics */
    SYSTEM_CSC_MODE_HDTV_VIDEO_R2Y,
    /**< Select coefficient for HDTV Video */
    SYSTEM_CSC_MODE_HDTV_VIDEO_Y2R,
    /**< Select coefficient for HDTV Video */
    SYSTEM_CSC_MODE_HDTV_GRAPHICS_R2Y,
    /**< Select coefficient for HDTV Graphics */
    SYSTEM_CSC_MODE_HDTV_GRAPHICS_Y2R,
    /**< Select coefficient for HDTV Graphics */
    SYSTEM_CSC_MODE_MAX,
    /**< Should be the last value of this enumeration.
         Will be used by driver for validating the input parameters. */
    SYSTEM_CSC_MODE_NONE = 0xFFFFu
    /**< Used when coefficients are provided */
} System_CscMode;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

/* @} */

