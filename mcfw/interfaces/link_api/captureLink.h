/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

/**
    \ingroup LINK_API
    \defgroup CAPTURE_LINK_API Capture Link API

    Capture Link can be used to instantiate capture upto
    SYSTEM_CAPTURE_INST_MAX Video input port instances.

    Each instance can have upto two outputs.

    The frames from these capture outputs can be put in upto
    four output queues.

    Each output queue can inturn to be connected to a link like
    Display or DEI or NSF.

    @{
*/

/**
    \file captureLink.h
    \brief Capture Link API
*/

#ifndef _CAPTURE_LINK_H_
#define _CAPTURE_LINK_H_

#ifdef __cplusplus
extern "C" {
#endif


/* Include's    */
#include <mcfw/interfaces/link_api/system.h>

/* Define's */

/* @{ */

/** \brief Max outputs per VIP instance */
#define CAPTURE_LINK_MAX_OUTPUT_PER_INST  (2)

/** \brief Max output queues in the capture link */
#define CAPTURE_LINK_MAX_OUT_QUE          (4)

/** \brief Max Channels per output queue */
#define CAPTURE_LINK_MAX_CH_PER_OUT_QUE   (16)

/** \brief Indicates number of output buffers to be set to default
 *         value by the capture link
 */
#define CAPTURE_LINK_NUM_BUFS_PER_CH_DEFAULT (0)

/** \brief Max blind area supported for each channel */
#define CAPTURE_LINK_MAX_BLIND_AREA_PER_CHNL (16)

/** \brief NO odd fields will be skipped */
#define CAPTURE_LINK_ODD_FIELD_SKIP_NONE   (0)

/** \brief All odd fields will be skipped */
#define CAPTURE_LINK_ODD_FIELD_SKIP_ALL    (1)

/** \brief 1/2 odd fields will be skipped */
#define CAPTURE_LINK_ODD_FIELD_SKIP_1_2    (2)

/** \brief 1/4 odd fields will be skipped */
#define CAPTURE_LINK_ODD_FIELD_SKIP_1_4    (3)


/* @} */

/* Control Command's    */

/**
    \ingroup LINK_API_CMD
    \addtogroup CAPTURE_LINK_API_CMD  CAPTURE Link Control Commands

    @{
*/

/**
    \brief Link CMD: Detect capture video source format

    This command can make the capture link wait until video
    source is detect on all the expected input video sources

    \param UInt32 timeout  [IN] BIOS_WAIT_FOREVER or BIOS_NO_WAIT
*/
#define CAPTURE_LINK_CMD_DETECT_VIDEO            (0x1000)

/**
    \brief Link CMD: force reset VIP port

    This command will force reset the VIP port, this will cause
    the both portA and portB on the VIP.

    VIP instance is determined based on overflow check using
    IOCTL_VPS_CAPT_CHECK_OVERFLOW

    \param None
*/
#define CAPTURE_LINK_CMD_FORCE_RESET             (0x1001)

/**
    \brief Print detaild capture link information

    This is meant to be used by driver developer for internal debugging purposes.

    \param NONE
*/
#define CAPTURE_LINK_CMD_PRINT_ADV_STATISTICS    (0x1002)

/**
    \brief Print buffer information of the capture link

    Print buffer count be queued and dequeued.

    \param NONE
*/
#define CAPTURE_LINK_CMD_PRINT_BUFFER_STATISTICS (0x100B)

/**
    \brief add one extra buffer pool for given channel

    Add one extra frames pool that used for capture for a given channel when frames from the normal
    buffer pool are exhausted. Now this is only used when SD spot display to avoid the SD display
    dequeue effect the normal operation of the other link.

    \param CaptureLink_ExtraFramesChId *pPrm [IN] Given queue id and channel id
*/
#define CAPTURE_LINK_CMD_SET_EXTRA_FRAMES_CH_ID  (0x100C)

/**
    \brief set blind area

    Set blind are information, this will create a black area on the captured frames,
    this will effect both preview path and encoder path.

    \param CaptureLink_BlindInfo *blindAreaInfo [IN] blind area info , channel ID, number of
                                                    blind are, blind rectangle information.
*/
#define CAPTURE_LINK_CMD_CONFIGURE_BLIND_AREA    (0x100D)

/**
    \brief skip the odd fields

    This API will skip the odd fields from the capture link.
    Only applicable for interlaced input source.

    \param CaptureLink_SkipOddFields *pPrm [IN] channel ID in which queID and the skip mode.
    the skip mode could be
    CAPTURE_LINK_ODD_FIELD_SKIP_NONE to CAPTURE_LINK_ODD_FIELD_SKIP_1_4
*/
#define CAPTURE_LINK_CMD_SKIP_ODD_FIELDS   (0x100E)

/**
    \brief set the capture resolution

    This API will enable dynamic resolution change in the capture link.

    \param CaptureLink_SetResolution *pPrm [IN] queId, chId and the picture size.

*/
#define CAPTURE_LINK_CMD_SET_RESOLUTION   (0x100F)


/**
    \brief Start/Stop Write back capture driver

    This command will start/stop write back capture driver. When the settings in the
    main display path is changed, Write back capture driver should be restarted.
    This Command is used for restarting write back driver.
    This command should not be used for actual capture.

    \param CaptureLink_CtrlWrbkCapt *pPrm [IN] queId, chId and flag to
           enable/disable driver.

*/
#define CAPTURE_LINK_CMD_ENABLE_WRBK_CAPT   (0x1010)


/**
    \brief Set the Scalar Parameters.

    This command is used to set the scalar parameters. This is
    typically used for the scalar available in the write back capture.

    \param CaptureLink_ScParams *pPrm [IN] queId, chId and scaling params.

*/
#define CAPTURE_LINK_CMD_SET_SC_PARAMS   (0x1011)


/* @} */

/**
    \brief Params for CAPTURE_LINK_CMD_ENABLE_WRBK_CAPT
*/
typedef struct
{
    UInt32 queId;
    /**< Output Que ID */
    UInt32 chId;
    /**< Output CH ID */
    UInt32 enable;
    /**< Flag used to enable Write back Capture */
} CaptureLink_CtrlWrbkCapt;

/**
    \brief Params for CAPTURE_LINK_CMD_SET_SC_PARAMS
*/
typedef struct
{
    UInt32 queId;
    /**< Output Que ID */
    UInt32 chId;
    /**< Output CH ID */
    UInt32 scEnable;
    /**< Flag to enable/disable Scaler */
    UInt32 inWidth;
    /**< Scalar Input Width */
    UInt32 inHeight;
    /**< Scalar Input Height */
    UInt32 outWidth;
    /**< Scalar Input Width */
    UInt32 outHeight;
    /**< Scalar Input Height */
} CaptureLink_ScParams;


/**
    \brief Params for CAPTURE_LINK_CMD_SET_RESOLUTION
*/
typedef struct
{
    UInt32 queId;
    /**< Output Que ID */
    UInt32 chId;
    /**< Output CH ID */
    UInt32 width;
    /**< Picture width of the current captured video */
    UInt32 height;
    /**< Picture width of the current captured video */
} CaptureLink_SetResolution;

/**
    \brief Parameters used to specify or change at run-time
        the channel for which extra buffers are needed

    This is applicable only when CaptureLink_CreateParams.numExtraBuf > 0
*/
typedef struct
{
    UInt32 queId;
    /**< Queue for which the extra channel buffers are needed */

    UInt32 chId;
    /**< Channel in the queue for whom extra channel buffers are needed */

} CaptureLink_ExtraFramesChId;

/**
    \brief Capture Color Specific Parameters
*/

typedef struct
{
    UInt32 brightness;
    /**< Brightness: 0-255 */

    UInt32 contrast;
    /**< contrast: 0-255 */

    UInt32 satauration;
    /**< satauration: 0-255 */

    UInt32 hue;
    /**< hue: 0-255 */

    UInt32 chId;
    /**< chId: 0-15 */

}CaptureLink_ColorParams;


/**
    \brief Capture output parameters
*/
typedef struct
{
    UInt32              dataFormat;
    /**< output data format, YUV422, YUV420, RGB, see System_VideoDataFormat */

    UInt32              scEnable;
    /**< TRUE: enable scaling, FALSE: disable scaling */

    UInt32              scOutWidth;
    /**< Scaler output width */

    UInt32              scOutHeight;
    /**< Scaler output height */

    UInt32              outQueId;
    /**< Link output que ID to which this VIP instance output frames are put */

} CaptureLink_OutParams;


typedef struct {

     UInt32 clipActive;
     /**< FALSE: Do not clip active Data
      *   TRUE : Clip active Data
      */

     UInt32 clipBlank;
     /**< FALSE: Do not clip blanking Data
      *   TRUE : Clip blanking Data
      */

     UInt32 ctrlChanSel;
     /**< For valid values see #System_VipCtrlChanSel,
        only valid for embedded sync mode and 16-bit/24-bit video interface mode
    */

     UInt32 ancChSel8b;
     /**< For valid values see #System_VipAncChSel8b */

     UInt32 pixClkEdgePol;
     /**< For valid values see #System_VipPixClkEdgePol */

     UInt32 invertFidPol;
     /**< FALSE: Keep FID as found, TRUE: Invert Value of FID */

     UInt32 embSyncErrCorrEnable;
     /**< TRUE: Error Correction enable, FALSE: disabled */

     UInt32 embSyncSrcNumPos;
     /**< For valid values see #System_VipSrcNumPos */

     UInt32 embSyncIsMaxChan3Bits;
     /**< FALSE: Use all 5 bits of the field for 32 total sources,
      *   TRUE : Use only bits 2:0 of this field to support
      *          the TVP5158's 8 sources, with the two upper
      *          most bits reserved
      */

     UInt32 disSyncFidSkewPostCnt;
     /**< Post count value when using vsync skew in FID determination */

     UInt32 disSyncFidSkewPreCnt;
     /**< Pre count value when using vsync skew in FID determination */

     UInt32 disSyncLineCaptureStyle;
     /**< For valid values see #System_VipLineCaptureStyle */

     UInt32 disSyncFidDetectMode;
     /**< For valid values see #System_VipFidDetectMode */

     UInt32 disSyncActvidPol;
     /**< For valid values see #System_VipPolarity */

     UInt32 disSyncVsyncPol;
     /**< For valid values see #System_VipPolarity */

     UInt32 disSyncHsyncPol;
     /**< For valid values see #System_VipPolarity */

     UInt32 cscBypass;
     /**< Flag to indicate whether CSC to be bypassed or not.

        Only applicable for RGB24-bit input
     */

     UInt32 cscMode;
     /**< Used to select one of pre-calculated coefficient sets. Used only
          For valid values see #System_CscMode.

          Only applicable for RGB24-bit input
    */

    UInt32  cscMulCoeff[3][3];
    /**< Multiplication coefficients in the format A0, B0, C0 in the first row,
         A1, B1, C1 in the second row and A2, B2, C2 in the third row.

         Only applicable for RGB24-bit input.

         Only used when CaptureLink_VipInstAdvancedParams.cscMode = SYSTEM_CSC_MODE_NONE
     */
    UInt32 cscAddCoeff[3];
    /**< Addition Coefficients.

         Only applicable for RGB24-bit input.

         Only used when CaptureLink_VipInstAdvancedParams.cscMode = SYSTEM_CSC_MODE_NONE
    */

} CaptureLink_VipInstAdvancedParams;

/**
    \brief VIP instance information
*/
typedef struct
{
    UInt32                        vipInstId;
    /**< VIP capture driver instance ID, see SYSTEM_CAPTURE_INST_VIPx_PORTy */

    UInt32                        videoCaptureMode;
    /**< Video capture mode. For valid values see #System_CaptVideoCaptureMode */

    UInt32                        videoIfMode;
    /**< Video interface mode. For valid values see #System_CaptVideoIfMode */

    UInt32                        inScanFormat;
    /**< Input source scan format - interlaced or progressive.
         For valid values see #System_VideoScanFormat. */

    UInt32                        videoDecoderId;
    /**< NOT USED, kept for legacy reason's */

    UInt32                        inDataFormat;
    /**< Input source data format, RGB or YUV422, see System_VideoDataFormat */

    UInt32                        standard;
    /**< Required video standard, see System_VideoStandard */

    UInt32                        numOutput;
    /**< Number of outputs per VIP */

    CaptureLink_OutParams         outParams[CAPTURE_LINK_MAX_OUTPUT_PER_INST];
    /**< Information about each output */

    UInt32                        numChPerOutput;
    /**< Number of individual channels per outputs */

    UInt32                        enableTimestampInInterrupt;
    /**< TRUE/FALSE Enables/disables time stamping of captured frames. When
            enabled, uses interrupt to time stamp frames. When disabled, uses
            approximation to time stamp frames. */

    UInt32                        numBufsPerCh;
    /**< Number of output buffers per channel in capture,
         This will be used only if overrideNumBufsInInstPrms flag is TRUE */

    UInt32                        muxModeStartChId;
    /**< Start channel ID in pixel or line mux mode. Used to add an offset to
         start channel mapping. This will be used when the decoder start channel
         ID is other than 0.
         For example some decoder's channel 0 CHID starts from 4 instead of 0.
         This is valid only in multi-channel mode and is ignored in single
         channel or other modes. */

    UInt32                        useAdvancedParams;
    /**< TRUE: CaptureLink_VipInstParams.advancedParams used and applied to this VIP Inst
        FALSE: CaptureLink_VipInstParams.advancedParams ignored
    */

    CaptureLink_VipInstAdvancedParams   advancedParams;
    /**< Advanced capture instance related parameters */

} CaptureLink_VipInstParams;

/**
    \brief windows of the blind area
*/
typedef struct
{
    UInt32 startX;
    /**< in pixels, MUST be multiple of 2 of YUV422I and multiple of 4 for YUV420SP */
    UInt32 startY;
    /**< in lines, MUST be multiple of 2 for YUV420SP  */
    UInt32 width;
    /**< in pixels, MUST be multiple of 2 of YUV422I and multiple of 4 for YUV420SP */
    UInt32 height;
    /**< in lines, MUST be multiple of 2 for YUV420SP  */
    UInt32 fillColorYUYV;
    /**< Color in YUYV format .*/
    UInt32 enableWin;
    /**< TRUE: Draw this blind area, FALSE: Do not draw this area */
} CaptureLink_BlindWin;

/**
    \brief information of the blind area
*/
typedef struct
{
    UInt32                  channelId;
    /**< channel id for the blind area */
    UInt32                  queId;
    /**< queue id of the blind area */
    UInt32                  numBlindArea;
    /**< number of valid blind area, 0 means disable bland area of this channel */

    CaptureLink_BlindWin    win[CAPTURE_LINK_MAX_BLIND_AREA_PER_CHNL];
    /**< window settings of blind are */
} CaptureLink_BlindInfo;

/**
    \brief Params for CAPTURE_LINK_CMD_SKIP_ODD_FIELDS
*/
typedef struct
{
    UInt32 queId;
    /**< Output Que ID */
    UInt32 skipOddFieldsChBitMask;
    /**< Channels for which odd fields should be skipped

        bit0 = CH0
        bit1 = CH1
        ...
        bitN = CHN
    */
    UInt32 oddFieldSkipRatio;
    /**< skip mode, it should be CAPTURE_LINK_ODD_FIELD_SKIP_XXXX */
} CaptureLink_SkipOddFields;

/**
    \brief Capture Link create parameters
*/
typedef struct
{
    Bool                      isPalMode;
    /**< pal mode based on usecase */

    UInt16                    numVipInst;
    /**< Number of VIP instances in this link */

    CaptureLink_VipInstParams vipInst[SYSTEM_CAPTURE_INST_MAX];
    /**< VIP instance information */

    System_LinkOutQueParams   outQueParams[CAPTURE_LINK_MAX_OUT_QUE];
    /**< Output queue information */

    UInt32                    tilerEnable;
    /**< TRUE/FALSE: Enable/disable tiler */


    UInt32                    fakeHdMode;
    /**< Capture in D1 but tells link size is 1080p, useful to test HD data flow on DVR HW or VS EVM */

    UInt32                    enableSdCrop;
    /**< Applicable only for D1/CIF capture input, crops 720 to 704 and 360 to 352 at video decoder.
        */
    UInt32                    doCropInCapture;
    /**< Applicable only for D1/CIF capture input, crops 720 to 704 and 360 to 352 at video decoder.
         Input is still 720/360. Crop is done in capture link by adjusting capture frame buffer pointer
         */
    UInt32                    numBufsPerCh;
    /**< Number of output buffers per channel in capture */

    UInt32                    numExtraBufs;
    /**<
        Number of extra buffers allocated and used by capture across channels.
        Buffers are allocated using WxH of CH0

        This is useful in some cases where the next link may hold the buffers
        of a given channel for a longer duration.

        In such cases the extra buffers will used for capture. This will ensure
        capture does not drop frames due to next link holding onto the
        buffer for a longer time

        Example, when capture is connect to SD display link directly

        This should be used typically when all CHs have the same properties like WxH
    */

    UInt32                    maxBlindAreasPerCh;
    /**< max blind area for one channel */

    UInt32                    overrideNumBufsInInstPrms;
    /** 0: use numBufsPerCh parameter from the create time parameter ie same
            number of buffers are allocated for instances
        1: use numBufsPerCh parameter from CaptureLink_VipInstParams */

} CaptureLink_CreateParams;

/**
    \brief Audio Codec parameters
*/
typedef struct
{
    UInt32 deviceNum;
  /**< Device number for which to apply the audio parameters */

    UInt32 numAudioChannels;
  /**< Number of audio channels */
    UInt32 samplingHz;
  /**< Audio sampling rate in Hz, Valid values: 8000, 16000 */

    UInt32 masterModeEnable;
  /**< TRUE: Master mode of operation, FALSE: Slave mode of operation */

    UInt32 dspModeEnable;
  /**< TRUE: DSP data format mode, FALSE: I2S data format mode */

    UInt32 ulawEnable;
  /**< TRUE: 8-bit ulaw data format mode, FALSE: 16-bit PCM data format mode */

    UInt32 cascadeStage;
  /**< Cascade stage number, Valid values: 0..3 */

    UInt32 audioVolume;
  /**< Audio volume, Valid values: 0..8. Refer to TVP5158 datasheet for details
   */

    UInt32 tdmChannelNum;
    /**< Number of TDM channels: 0: 2CH, 1: 4CH, 2: 8CH, 3: 12CH 4: 16CH */

} Capture_AudioModeParams;

/**
    \brief Capture link register and init

    - Creates link task
    - Registers as a link with the system API

    \return FVID2_SOK on success
*/
Int32 CaptureLink_init();

/**
    \brief Capture link de-register and de-init

    - Deletes link task
    - De-registers as a link with the system API

    \return FVID2_SOK on success
*/
Int32 CaptureLink_deInit();

/**
    \brief Set defaults for in link channel information

    \param pPrm [OUT] Default information
*/
static inline void CaptureLink_CreateParams_Init(CaptureLink_CreateParams *pPrm)
{
    UInt32 i;
    memset(pPrm, 0, sizeof(*pPrm));

    pPrm->numVipInst = 0;
    pPrm->tilerEnable = FALSE;
    pPrm->fakeHdMode = FALSE;
    pPrm->enableSdCrop = TRUE;
    pPrm->doCropInCapture = TRUE;
    pPrm->numBufsPerCh = CAPTURE_LINK_NUM_BUFS_PER_CH_DEFAULT;
    pPrm->numExtraBufs = 0;
    pPrm->overrideNumBufsInInstPrms = 0;

    for (i=0; i<SYSTEM_CAPTURE_INST_MAX; i++)
    {
        pPrm->vipInst[i].numChPerOutput                         = 0;
        pPrm->vipInst[i].enableTimestampInInterrupt             = TRUE;
        pPrm->vipInst[i].videoCaptureMode                       = SYSTEM_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_PIXEL_MUX_EMBEDDED_SYNC;
        pPrm->vipInst[i].videoIfMode                            = SYSTEM_CAPT_VIDEO_IF_MODE_8BIT;
        pPrm->vipInst[i].inScanFormat                           = SYSTEM_SF_INTERLACED;
        pPrm->vipInst[i].muxModeStartChId                       = 0;
        pPrm->vipInst[i].useAdvancedParams                      = FALSE;
        pPrm->vipInst[i].advancedParams.clipActive              = FALSE;
        pPrm->vipInst[i].advancedParams.clipBlank               = FALSE;
        pPrm->vipInst[i].advancedParams.ctrlChanSel             = SYSTEM_VIP_CTRL_CHAN_SEL_7_0;
        pPrm->vipInst[i].advancedParams.ancChSel8b              = SYSTEM_VIP_ANC_CH_SEL_8B_LUMA_SIDE;
        pPrm->vipInst[i].advancedParams.pixClkEdgePol           = SYSTEM_VIP_PIX_CLK_EDGE_POL_RISING;
        pPrm->vipInst[i].advancedParams.invertFidPol            = FALSE;
        pPrm->vipInst[i].advancedParams.embSyncErrCorrEnable    = FALSE;
        pPrm->vipInst[i].advancedParams.embSyncSrcNumPos        = SYSTEM_VIP_SRC_NUM_POS_LS_NIBBLE_OF_CODEWORD;
        pPrm->vipInst[i].advancedParams.embSyncIsMaxChan3Bits   = FALSE;
        pPrm->vipInst[i].advancedParams.disSyncFidSkewPostCnt   = 0;
        pPrm->vipInst[i].advancedParams.disSyncFidSkewPreCnt    = 0;
        pPrm->vipInst[i].advancedParams.disSyncLineCaptureStyle = SYSTEM_VIP_LINE_CAPTURE_STYLE_ACTVID;
        pPrm->vipInst[i].advancedParams.disSyncFidDetectMode    = SYSTEM_VIP_FID_DETECT_MODE_PIN;
        pPrm->vipInst[i].advancedParams.disSyncActvidPol        = SYSTEM_VIP_POLARITY_HIGH;
        pPrm->vipInst[i].advancedParams.disSyncVsyncPol         = SYSTEM_VIP_POLARITY_LOW;
        pPrm->vipInst[i].advancedParams.disSyncHsyncPol         = SYSTEM_VIP_POLARITY_LOW;
        pPrm->vipInst[i].advancedParams.cscBypass               = TRUE;
        pPrm->vipInst[i].advancedParams.cscMode                 = SYSTEM_CSC_MODE_HDTV_GRAPHICS_R2Y;
    }
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif


/*@}*/
