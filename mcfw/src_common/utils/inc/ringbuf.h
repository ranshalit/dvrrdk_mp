/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2013 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#ifndef _OSA_RINGBUF_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _OSA_RINGBUF_H_


#include <osa_mutex.h>

#define RINGBUF_DATA_ALIGN_SIZE     32
#define RINGBUF_DATA_ALIGN(val, alignment) ((val + alignment -1)/alignment*alignment)
#define MIN_TERMINATION_SIZE(x,y) ((x)*(y))

#define     RING_BUF_MAX_STRING_LEN      200

#if     1
#define RINGBUF_OK      OSA_SOK
#define RINGBUF_FAIL    OSA_EFAIL
#else
#define RINGBUF_OK       FVID2_SOK 
#define RINGBUF_FAIL    
#endif

#define RINGBUF_ISERROR(status)    ((status == RINGBUF_OK) ? FALSE : TRUE)


typedef struct  _RingBufferLock
{
#ifdef    __ti__    /* BIOS Builds */
        
#else
    pthread_mutex_t mutex;
#endif
} RingBufferLock;

typedef struct _RingBufferHandle
{
	void 	        *baseAddr;          /**< Base address of the ring buffer */
	UInt32	        totalSize;          /**< Total size of ring buffer */
	UInt32	        usableSize;         /**< Total usable size of ring buffer */
    Int32           alignment;          /**< Alignment for each buffer request */
	UInt32 	        wrAcquirePtr;       /**< Pointer acquired by writer */  
	UInt32 	        wrAcquireLen;       /**< Size acquired by writer */  
	UInt32 	        wrReleasePtr;       /**< Pointer released by writer */  
	UInt32 	        wrReleaseLen;       /**< Size released by writer - this marks the filled data length */  
	UInt32 	        rdAcquirePtr;       /**< Pointer acquired by reader */  
	UInt32 	        rdAcquireLen;       /**< Size acquired by reader */  
	UInt32 	        rdReleasePtr;       /**< Pointer released by reader */  
	UInt32 	        rdReleaseLen;       /**< Size released by reader - this marks the empty length */  
	Int32	        writeAcquireCount;  /**< Streams written */
	Int32	        writeReleaseCount;  /**< Streams released after write */
	Int32	        writeCancelCount ;  /**< Streams cancelled (not written) */
	Int32	        readAcquireCount;   /**< Streams read */
	Int32	        readReleaseCount;   /**< Streams released after read */
	Int32	        readCancelCount ;  /**< Streams cancelled (not read) */
    Bool            initDone;           /**< internal book keeping info */
    RingBufferLock  lock;                /**< internal book keeping info */
    Int8            ringBufId;           /**< Identifier - Internal use */
} RingBufferHandle_t ;

Int32 RingBufferInit(RingBufferHandle_t *ringBufferHndl, Void *pBaseAddr, UInt32 size);
Bool RingBufferPrintInfo(Int32 lineNo, Int8 *func, RingBufferHandle_t *ringBufferHndl);
Bool RingBufferWriterReserve(RingBufferHandle_t *ringBufferHndl, UInt32 reqLength);
Void* RingBufferWriterAcquire(RingBufferHandle_t *ringBufferHndl, UInt32 reqLength);
Int32 RingBufferWriterRelease(RingBufferHandle_t *ringBufferHndl, UInt32 writeLength);
Int32 RingBufferWriterCancel(RingBufferHandle_t *ringBufferHndl);
Void* RingBufferReaderAcquire(RingBufferHandle_t *ringBufferHndl, UInt32 *reqLength);
Int32 RingBufferReaderRelease(RingBufferHandle_t *ringBufferHndl, UInt32 readLength);
Int32 RingBufferReaderCancel(RingBufferHandle_t *ringBufferHndl);
Int32 RingBufferDeInit(RingBufferHandle_t *ringBufferHndl);
Void  RingBufferLogPrint (Void);
Void RingBufferSetErrorPrintFlag (Bool enableFlag);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _OSA_RINGBUF_H_ */
