/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#define OSA_DEBUG_MODE

#include <osa_debug.h>
#include <ringbuf.h>

// #define RING_BUF_PRINT_TRACE  
#ifdef RING_BUF_PRINT_TRACE
#define RING_BUF_TRACE_PRINT(...)  do { printf("\n\r [RingBuf] " __VA_ARGS__); fflush(stdout); } while(0)
#else
#define RING_BUF_TRACE_PRINT(...)
#endif

#define RING_BUF_PRINT_INFO
#ifdef RING_BUF_PRINT_INFO
#define RING_BUF_INFO_PRINT(...)  do { printf("\n\r [RingBuf] " __VA_ARGS__); fflush(stdout); } while(0)
#else
#define RING_BUF_INFO_PRINT(...)
#endif


typedef enum _RINGBUF_CTL
{
    RING_BUF_READER,
    RING_BUF_WRITER
} RINGBUF_CTL;


#define     RING_BUF_PRINT   
#define     RING_BUF_STR_MAX_LEN    100

#define     RING_BUF_ERROR_ALREADY_ACQUIRED         "Acquire already done"
#define     RING_BUF_ERROR_NEED_TO_ACQUIRE          "Acquire first!!!"
#define     RING_BUF_ERROR_DATA_FULL                "Data full"
#define     RING_BUF_ERROR_DATA_EMPTY               "Data empty"
#define     RING_BUF_ERROR_RING_BUF_SIZE_REDUCED   "Buffer Size Reduced"
#define     RING_BUF_ERROR_RING_BUF_SIZE_RESTORED  "Buffer Size Restored"


#define     RING_BUF_DEBUG_INFO              __LINE__, (Int8 *)__FUNCTION__

/* Indexing of created ring buffers */
static Int32            ringBufNum = 0;

static
Void    RingBufferPrintError (Int32 lineNo, Int8 *func, RINGBUF_CTL ctl , Int8  *errStr)
{
    if (ctl == RING_BUF_READER)
        OSA_printf("RingBuf(%d, %s) - Reader: %s", lineNo, func, errStr);
    else
        OSA_printf("RingBuf(%d, %s) - Writer: %s", lineNo, func, errStr);
}

#ifdef    __ti__    /* BIOS Builds */
static
Int32 RingBufferCreateLock (RingBufferLock *lock)
{
    Int32 status = RINGBUF_OK;
    return status;    
}

static
Int32 RingBufferDeleteLock (RingBufferLock *lock)
{
    Int32 status = RINGBUF_OK;
    return status;    
}

static
Void    RingBufferAcquireLock (RingBufferLock *lock)
{
}

static
Void    RingBufferReleaseLock (RingBufferLock *lock)
{
}
#else
static
Int32 RingBufferCreateLock (RingBufferLock *lock)
{
    Int32 status = RINGBUF_OK;
    pthread_mutexattr_t mutex_attr;

    status |= pthread_mutexattr_init(&mutex_attr);
    status |= pthread_mutex_init(&lock->mutex, &mutex_attr);
    return status;    
}

static
Int32 RingBufferDeleteLock (RingBufferLock *lock)
{
    Int32 status = RINGBUF_OK;

    pthread_mutex_destroy(&lock->mutex);  
    return status;    
}

static
Void    RingBufferAcquireLock (RingBufferLock *lock)
{
    pthread_mutex_lock(&lock->mutex);
}

static
Void    RingBufferReleaseLock (RingBufferLock *lock)
{
    pthread_mutex_unlock(&lock->mutex);
}
#endif


/**
    \brief     Initialize Ring Buffer
    
    \param  ringBufferHndl    [IN,OUT]  Ring buffer handle, data structure
    \param  pBaseAddr          [IN]         Base address of the ring buffer provided by caller

    \return   RINGBUF_OK on success
*/
Int32 RingBufferInit(RingBufferHandle_t *ringBufferHndl, Void *pBaseAddr, UInt32 size)
{
    Int32 status = RINGBUF_OK;

    memset(ringBufferHndl, 0, sizeof(*ringBufferHndl));
    ringBufferHndl->initDone = FALSE;

	ringBufferHndl->baseAddr	 = pBaseAddr;
	ringBufferHndl->usableSize = ringBufferHndl->totalSize   = size;
    ringBufferHndl->rdReleaseLen = size;   
    ringBufferHndl->ringBufId = ringBufNum;
    ringBufNum++;

    status |= RingBufferCreateLock(&ringBufferHndl->lock);
    UTILS_assert(status == RINGBUF_OK);
    ringBufferHndl->initDone = (status == RINGBUF_OK) ? TRUE : FALSE;
    OSA_printf ("\r\n******* RingBuf %d created ********\n", ringBufferHndl->ringBufId);
    return status;
}

/**
    \brief     Delete Ring Buffer
    
    \param  ringBufferHndl    [IN]  Ring buffer handle to be terminated

    \return   RINGBUF_OK on success
*/
Int32 RingBufferDeInit(RingBufferHandle_t *ringBufferHndl)
{
    if (ringBufferHndl->initDone == TRUE)
    {
        RingBufferDeleteLock(&ringBufferHndl->lock);  
        OSA_printf ("******* RingBuf %d deleted ********\n", ringBufferHndl->ringBufId);
        memset(ringBufferHndl, 0, sizeof(*ringBufferHndl));
        ringBufNum--;
    }
    return RINGBUF_OK;
}


/**
    \brief     Print ring buffer information
    
    \param  lineNo                [IN]  Line number passed by caller - for debug purpose
    \param  func                   [IN]  Function calling this function - for debug purpose
    \param  ringBufferHndl    [IN]  Ring buffer handle 

    \return   RINGBUF_OK on success
*/
Bool RingBufferPrintInfo(Int32 lineNo, Int8 *func, RingBufferHandle_t *ringBufferHndl)
{
     RING_BUF_INFO_PRINT ("\n********** RingBuf - %d Info (%d, %s) ***********\n", ringBufferHndl->ringBufId, lineNo, func);
     RING_BUF_INFO_PRINT ("initDone   :%s\t", (ringBufferHndl->initDone == TRUE) ? "TRUE" : "FALSE");
     RING_BUF_INFO_PRINT ("base       :0x%08X\t", (UInt32) ringBufferHndl->baseAddr);
     RING_BUF_INFO_PRINT ("totalSize  :%d\t", ringBufferHndl->totalSize);
     RING_BUF_INFO_PRINT ("availSize  :%d\n", ringBufferHndl->usableSize);
     RING_BUF_INFO_PRINT ("Writer -> acqPtr[%d] acqSize[%d] relPtr[%d] relSize[%d]\t", 
                ringBufferHndl->wrAcquirePtr, ringBufferHndl->wrAcquireLen, ringBufferHndl->wrReleasePtr, ringBufferHndl->wrReleaseLen);
     RING_BUF_INFO_PRINT ("Reader -> acqPtr[%d] acqSize[%d] relPtr[%d] relSize[%d]\t", 
                ringBufferHndl->rdAcquirePtr, ringBufferHndl->rdAcquireLen, ringBufferHndl->rdReleasePtr, ringBufferHndl->rdReleaseLen);
     RING_BUF_INFO_PRINT ("Count   -> writer [%d, %d, %d] reader [%d, %d, %d]\n", 
                 ringBufferHndl->writeAcquireCount, ringBufferHndl->writeReleaseCount, ringBufferHndl->writeCancelCount,
                 ringBufferHndl->readAcquireCount, ringBufferHndl->readReleaseCount, ringBufferHndl->readCancelCount);
	 return TRUE;
}


/**
    \brief     Get available size (doesnt care about discontinous availability)
    Available writeSize is determined by the reader rel ptr & writer acq pointer 
    Available readSize is  determined by the writer rel ptr & reader acq pointer 
    This API ignores the wrap around case. 

    \param  acqPtr                [IN]  Offset acquired by reader or writer 
    \param  relPtr                 [IN]  Offset released by reader or writer 
    \param  totalSize            [IN]  Total size of the ring buffer

    \return  Returns the contiguous available size
*/
static inline
Int32   RingBufferAvailableSize(Int32 acqPtr, Int32 relPtr, Int32 totalSize)
{
    Int32 avail;

    if (relPtr > acqPtr)
    {
        avail = relPtr - acqPtr;
    }
    else
    {
        avail = totalSize - acqPtr;
    }
    return avail;
}


/**
    \brief     Get available size irrespective of the wrap around condition
    Available writeSize is determined by the reader rel ptr & writer acq pointer 
    Available readSize is  determined by the writer rel ptr & reader acq pointer 

    \param  acqPtr                [IN]  Offset acquired by reader or writer 
    \param  relPtr                 [IN]  Offset released by reader or writer 
    \param  totalSize            [IN]  Total size of the ring buffer

    \return   Returns the total available size 
*/
static inline
Int32   RingBufferAvailableSizeWrap(Int32 acqPtr, Int32 relPtr)
{
    Int32 avail = 0;

    if (relPtr <= acqPtr)
    {
        avail = relPtr;
    }
    return avail;
}

/**
    \brief     Increment a reader or writer pointer 
 
    \param  ptr                [IN, OUT]  pointer to increment
    \param  size               [IN]          size of increment
    \param  totalSize        [IN]          Max allowed pointer size

    \return   None
*/
static inline
Void   RingBufferIncrementPtr(UInt32 *ptr, UInt32 size, UInt32 totalSize)
{
    Int32 newPtr = *ptr + size;

    /* Wrap Around case */
    if (newPtr >= totalSize)
    {
        *ptr = 0;
    }
    else
    {
        *ptr = newPtr;
    }
}


/**
    \brief     Decrement a reader or writer pointer.
    This API is generally invoked when a pointer increment has happened already
    via acquire API and now this moved pointer should be decremented based on 
    released size. 
    General assumption is that pointer is >= size.
    If pointer is less that size, this means that a wrap around has occurred. 
    In this case, move back the pointer to its original position and then decrement.
 
    \param  ptr                [IN, OUT]  pointer to decrement
    \param  size               [IN]          size of decrement
    \param  totalSize        [IN]          Max allowed pointer size

    \return   None
*/
static inline
Void   RingBufferDecrementPtr(UInt32 *ptr, UInt32 size, UInt32 totalSize)
{
    if (*ptr < size)
    {
        /* This means that a wrap around has occurred earlier; so move from tail */
        *ptr = *ptr + totalSize;
    }
    *ptr = *ptr - size;

}


/**
    \brief    Reset ring buffer offsets on detection of emptiness
    If buffer is empty, all reader / writer acquire / release pointers can be reset.
    This is just for simplicity sake. 
 
    \param  ringBufferHndl                [IN, OUT]  Ring buffer context

    \return   None
*/
static inline
Bool RingBufferHandleEmpty(RingBufferHandle_t *ringBufferHndl)
{
    ringBufferHndl->wrAcquirePtr = 0;
    ringBufferHndl->rdAcquirePtr = 0;
    ringBufferHndl->wrReleasePtr = 0;
    ringBufferHndl->rdReleasePtr = 0;

    ringBufferHndl->rdReleaseLen = ringBufferHndl->totalSize;
    return TRUE;
}


/**
    \brief    Check if ring buffer is empty -ie.,  writer has written nothing 
     or  reader has read everything written.
 
    \param  ringBufferHndl                [IN]  Ring buffer context

    \return   None
*/
static inline
Bool RingBufferIsEmpty(RingBufferHandle_t *ringBufferHndl)
{
    if ((ringBufferHndl->wrAcquireLen == 0)
        && (ringBufferHndl->rdAcquireLen == 0)
        && (ringBufferHndl->rdReleaseLen == ringBufferHndl->usableSize))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}



/**
    \brief    Acquire buffer pointer for writing data.
     Available writer size is determined by rdReleaseLen which is intially the complete
     ring buffer length. Each acquire should be followed by release or cancel API.
     Multiple Acquire calls not possible. 
     If the writer pointer reaches towards the end with some space left out at the end
     as well as the top, it ignores the bottom most space as a continous buffer is required
     by application. In such scenario, the effective total length of ring buffer will be reduced
     by the unusable tail section. This size will be restored on empty scenario.
    
     This API will return valid pointer only if exact requested length is available. 
     This is added as a limitation just to keep implementation simple. There is no
     requirement to have a generic implementation of ring buffer. 

    \param  ringBufferHndl                [IN]  Ring buffer context
    \param  reqLength                      [IN]  Required Buffer size 

    \return   Returns a valid buffer pointer on SUCCESS else NULL pointer
*/
Void* RingBufferWriterAcquire(RingBufferHandle_t *ringBufferHndl, UInt32 reqLength)
{
    Void *ptr = NULL;
    Int32 avail, unusedAvail = 0;
    Int32 status = RINGBUF_OK;

    RING_BUF_TRACE_PRINT("%s %d> %d", __FUNCTION__, __LINE__, reqLength);
    RingBufferAcquireLock(&ringBufferHndl->lock);

    if (ringBufferHndl->wrAcquireLen)
    {
        RingBufferPrintError(RING_BUF_DEBUG_INFO, RING_BUF_WRITER, RING_BUF_ERROR_ALREADY_ACQUIRED);
        status |= RINGBUF_FAIL;
    }
    /* rdReleaseLen is added to check if read data is full, in this case writer cannot acquire anymore */
    if (ringBufferHndl->rdReleaseLen == 0) 
    {
//        RingBufferPrintError(RING_BUF_DEBUG_INFO, RING_BUF_READER, RING_BUF_ERROR_DATA_FULL);
        status |= RINGBUF_FAIL;
    }

    if (status == RINGBUF_OK)
    {   
        /* Adjust all pointers if empty */
        if (RingBufferIsEmpty(ringBufferHndl) == TRUE)
        {
            RingBufferHandleEmpty(ringBufferHndl);
        }

        avail = RingBufferAvailableSize(ringBufferHndl->wrAcquirePtr, ringBufferHndl->rdReleasePtr, ringBufferHndl->usableSize);
        if (avail < reqLength)
        {
            /* Check if available from top - wrap around case */
            unusedAvail = avail; /* This tail side buffer is non-usable for writer */
            avail = RingBufferAvailableSizeWrap(ringBufferHndl->wrAcquirePtr, ringBufferHndl->rdReleasePtr);
        }

        if (avail >= reqLength)
        {

            if (unusedAvail)
            {
                /* The tail side has become unusable, so reduce usable ring buffer size temporarily. 
                                * NOTE - THIS UNUSED BUFFER AT THE END CAN BE REUSED ONLY WHEN 
                                * ALL DATA IS READ (EMPTY) or ALMOST EMPTY (All data is read, writer has 
                                * acquired, but not released yet 
                                * i.e - when (wrAcqPtr + rdReleaseLen) > usableRingBufSize.
                                */
                ringBufferHndl->rdReleaseLen -= unusedAvail;
                ringBufferHndl->usableSize -= unusedAvail;

                /* Reset write related pointers */
                ringBufferHndl->wrAcquirePtr = ringBufferHndl->wrReleasePtr = 0;
                
                /* If read pointers moved to the end (because we have reduced total size now), reset them as well */
                if (ringBufferHndl->rdReleasePtr >= ringBufferHndl->usableSize)
                    ringBufferHndl->rdReleasePtr = 0;
                if (ringBufferHndl->rdAcquirePtr >= ringBufferHndl->usableSize)
                    ringBufferHndl->rdAcquirePtr = 0; 
                
//                RingBufferPrintError(RING_BUF_DEBUG_INFO, RING_BUF_WRITER, RING_BUF_ERROR_RING_BUF_SIZE_REDUCED);
            }
            ptr = (Void*) (ringBufferHndl->baseAddr + ringBufferHndl->wrAcquirePtr);
            
            ringBufferHndl->wrAcquireLen += reqLength;
            ringBufferHndl->rdReleaseLen -= reqLength;
            RingBufferIncrementPtr(&ringBufferHndl->wrAcquirePtr, reqLength, ringBufferHndl->usableSize);
            ringBufferHndl->writeAcquireCount++;

        }
    }
    
    RingBufferReleaseLock(&ringBufferHndl->lock);
	return ptr;
}


/**
    \brief    Release buffer pointer after writing data.
     Released buffer length should be less than or equal to acquired length.
     After writer release, this data is available for reader. 

    \param  ringBufferHndl                [IN]  Ring buffer context
    \param  reqLength                      [IN]  Released buffer size

    \return   Returns a valid buffer pointer on SUCCESS else NULL pointer
*/
Int32 RingBufferWriterRelease(RingBufferHandle_t *ringBufferHndl, UInt32 writeLength)
{
    Int32 status = RINGBUF_OK;

    RING_BUF_TRACE_PRINT("%s %d> %d", __FUNCTION__, __LINE__, writeLength);
    RingBufferAcquireLock(&ringBufferHndl->lock);

    if (writeLength > ringBufferHndl->wrAcquireLen)
    {
        RingBufferPrintError(RING_BUF_DEBUG_INFO, RING_BUF_WRITER, RING_BUF_ERROR_NEED_TO_ACQUIRE);
        status |= RINGBUF_FAIL;
    }
    else
    {
        ringBufferHndl->wrAcquireLen -= writeLength;
        ringBufferHndl->wrReleaseLen += writeLength;
        RingBufferIncrementPtr(&ringBufferHndl->wrReleasePtr, writeLength, ringBufferHndl->usableSize);
        ringBufferHndl->writeReleaseCount++;
    }

    RingBufferReleaseLock(&ringBufferHndl->lock);
    return status;
}



/**
    \brief    Cancel the unused buffer which was acquired earlier for write. 
    After acquire, some space will be used for writing data and the unused 
    space should be cancelled so that its available for next acquire. 
 
    This acquire, release, cancel mechanisms ensure that each frame occupies 
    exactly required space and not a fixed length thus allowing variable sized 
    frames in the system optimizing the buffer usage. 

    \param  ringBufferHndl                [IN]  Ring buffer context

    \return   Returns RINGBUF_OK on SUCCESS
*/
Int32 RingBufferWriterCancel(RingBufferHandle_t *ringBufferHndl)
{
    Int32 status = RINGBUF_OK;

    RING_BUF_TRACE_PRINT("%s %d> ", __FUNCTION__, __LINE__);
    RingBufferAcquireLock(&ringBufferHndl->lock);
    if (ringBufferHndl->wrAcquireLen)
    {
        RingBufferHandle_t prevState;

        prevState = *ringBufferHndl;
        RingBufferDecrementPtr(&ringBufferHndl->wrAcquirePtr, ringBufferHndl->wrAcquireLen, ringBufferHndl->usableSize);
        
        if (ringBufferHndl->wrAcquirePtr != ringBufferHndl->wrReleasePtr)
        {
            /*********** Enable this debug code for now till thorough testing ************/
            OSA_printf ("\n----------------------------------\n");
            OSA_printf ("%s %d ---- Error, wrAcqPtr[%d] != wrRelPtr[%d] \n",
                __FUNCTION__, __LINE__, ringBufferHndl->wrAcquirePtr, ringBufferHndl->wrReleasePtr);
            OSA_printf ("Prev State----------- :\n");
            RingBufferPrintInfo(__LINE__, (Int8 *)__FUNCTION__, &prevState);
            OSA_printf ("Curr State----------- :\n");
            RingBufferPrintInfo(__LINE__, (Int8 *)__FUNCTION__, ringBufferHndl);
            OSA_printf ("\n----------------------------------\n");

            OSA_assert(ringBufferHndl->wrAcquirePtr == ringBufferHndl->wrReleasePtr);
            /******************************************************************/
        }

        /* Include this size in reader released data so that its available for next writer acquire */
        ringBufferHndl->rdReleaseLen += ringBufferHndl->wrAcquireLen;
        ringBufferHndl->wrAcquireLen = 0;
        ringBufferHndl->writeCancelCount++;
    }
    RingBufferReleaseLock(&ringBufferHndl->lock);
    return status;
}


/**
    \brief    Acquire buffer pointer for reading data.
     Available reader size is determined by wrReleaseLen which is intially 0.
     Each acquire should be followed by release or cancel API.
     Multiple Acquire calls not possible. 
     If the reader pointer reaches towards the end and the effective total length of ring buffer 
     was REDUCED earlier due to discontinuities, it will be restored.
    
     This API will return valid pointer only if exact requested length is available. 

    \param  ringBufferHndl                [IN]  Ring buffer context
    \param  reqLength                      [IN]  Required Buffer size 

    \return   Returns a valid buffer pointer on SUCCESS else NULL pointer
*/
Void* RingBufferReaderAcquire(RingBufferHandle_t *ringBufferHndl, UInt32 *reqLength)
{
    Void *ptr = NULL;
    Int32 avail;
    Int32 status = RINGBUF_OK;

    RING_BUF_TRACE_PRINT("%s %d> %d", __FUNCTION__, __LINE__, *reqLength);

    RingBufferAcquireLock(&ringBufferHndl->lock);

    if (ringBufferHndl->rdAcquireLen)
    {
        RingBufferPrintError(RING_BUF_DEBUG_INFO, RING_BUF_READER, RING_BUF_ERROR_ALREADY_ACQUIRED);
        status |= RINGBUF_FAIL;
    }
    /* wrReleaseLen is added to check if some data is written, if not reader cannot acquire */
    if (ringBufferHndl->wrReleaseLen == 0) 
    {
        RingBufferPrintError(RING_BUF_DEBUG_INFO, RING_BUF_READER, RING_BUF_ERROR_DATA_EMPTY);
        status |= RINGBUF_FAIL;
    }

    if (status == RINGBUF_OK)
    {
        avail = RingBufferAvailableSize(ringBufferHndl->rdAcquirePtr, ringBufferHndl->wrReleasePtr, ringBufferHndl->usableSize);
        if (avail >= *reqLength)
        {
            ptr = (Void*) (ringBufferHndl->baseAddr + ringBufferHndl->rdAcquirePtr);

            RingBufferIncrementPtr(&ringBufferHndl->rdAcquirePtr, *reqLength, ringBufferHndl->usableSize);
            ringBufferHndl->rdAcquireLen += *reqLength;
            ringBufferHndl->wrReleaseLen -= *reqLength;
        
            ringBufferHndl->readAcquireCount++;
        }
        else
        {
            *reqLength = 0;
        }    
    }
    
    RingBufferReleaseLock(&ringBufferHndl->lock);
    return ptr;
}


/**
    \brief    Release buffer pointer after reading data.
     Released buffer length should be less than or equal to acquired length.
     After reader release, this data is available for writer. 

    \param  ringBufferHndl                [IN]  Ring buffer context
    \param  reqLength                      [IN]  Released buffer size

    \return   Returns RINGBUF_OK on SUCCESS 
*/
Int32 RingBufferReaderRelease(RingBufferHandle_t *ringBufferHndl, UInt32 readLength)
{
    Int32 status = RINGBUF_OK;

    RING_BUF_TRACE_PRINT("%s %d> %d", __FUNCTION__, __LINE__, readLength);

    RingBufferAcquireLock(&ringBufferHndl->lock);

    if (readLength > ringBufferHndl->rdAcquireLen)
    {
        RingBufferPrintError(RING_BUF_DEBUG_INFO, RING_BUF_READER, RING_BUF_ERROR_NEED_TO_ACQUIRE);
        status |= RINGBUF_FAIL;
    }
    else
    {
        ringBufferHndl->rdAcquireLen -= readLength;
        ringBufferHndl->rdReleaseLen += readLength;
        RingBufferIncrementPtr(&ringBufferHndl->rdReleasePtr, readLength, ringBufferHndl->usableSize);

        ringBufferHndl->readReleaseCount++;

        /* If ring buffer is empty, this is when the unused buffer left out can be attached again */
        if ((ringBufferHndl->totalSize > ringBufferHndl->usableSize)
            && 
            ((ringBufferHndl->wrAcquirePtr + ringBufferHndl->rdReleaseLen) >= ringBufferHndl->usableSize)
            )
        {
            Int32 unusedSize = ringBufferHndl->totalSize - ringBufferHndl->usableSize;

            ringBufferHndl->usableSize = ringBufferHndl->totalSize;
            /* Increment empty length to make this available for writer */
            ringBufferHndl->rdReleaseLen += unusedSize;
            
//            RingBufferPrintError(RING_BUF_DEBUG_INFO, RING_BUF_READER, RING_BUF_ERROR_RING_BUF_SIZE_RESTORED);
        }
    }

    RingBufferReleaseLock(&ringBufferHndl->lock);
    return status;
}


/**
    \brief    Cancel the unused buffer which was acquired earlier for reading. 
 
    This is not used in current system as reader consumes complete frame. 

    \param  ringBufferHndl                [IN]  Ring buffer context

    \return   Returns RINGBUF_OK on SUCCESS 
*/
Int32 RingBufferReaderCancel(RingBufferHandle_t *ringBufferHndl)
{
    Int32 status = RINGBUF_OK;

    RING_BUF_TRACE_PRINT("%s %d> ", __FUNCTION__, __LINE__);

    RingBufferAcquireLock(&ringBufferHndl->lock);
    if (ringBufferHndl->rdAcquireLen)
    {
        RingBufferDecrementPtr(&ringBufferHndl->rdAcquirePtr, ringBufferHndl->rdAcquireLen, ringBufferHndl->usableSize);
        
        /* Include this size in writer released data so that its available for next reader acquire */
        ringBufferHndl->wrReleaseLen += ringBufferHndl->rdAcquireLen;
        ringBufferHndl->rdAcquireLen = 0;

        ringBufferHndl->readCancelCount++;
    }
    RingBufferReleaseLock(&ringBufferHndl->lock);
    return status;
}


