/*
 *  ======== genaddrinfoscript.xs ========
 *  Javascript that generates a unix shell script
 *  containining address info from the config.bld
 *  This file must be loaded from config.bld
 *  using xdc.loadCapsule();
 */



function Value2HexString(Value)
{
    return ("0x" + java.lang.Long.toHexString(Value));
}


function Value2IntString(Value)
{
    return (java.lang.Long.toString(Value));
}


function IntType(Value)
{
    return (Value>>>0);
}

function Size2MB(Size)
{
    return (IntType(Size)/MB);
}

function GetQuotedString(Value)
{
    return ("\""+Value+"\"");
}

function GetShellExportString(EnvVariable,Value)
{
    return ("export " + EnvVariable+"="+GetQuotedString(Value));
}

function GenAddrFile()
{
    print ("Generation of Shell script in progress...");
    /* Create the value of the environmental variables to be set */
    var RDK_LINUX_MEM_STR           = Size2MB(LINUX_SIZE) + "M";
    var NOTIFYK_VPSSM3_SVA_ADDR_STR = Value2HexString(NOTIFY_SHARED_ADDR);
    var REMOTE_DEBUG_ADDR_STR       = Value2HexString(REMOTE_DEBUG_ADDR);
    var ETH_OFFLOAD_ADDR_STR        = Value2HexString(ETH_OFFLOAD_ADDR);
    var HDVPSS_SHARED_MEM_STR       = Value2HexString(HDVPSS_SHARED_ADDR);
    /* HDVPSS_SHARED_SIZE should be a decimal integer not hexadecimal */
    var HDVPSS_SHARED_SIZE_STR      = Value2IntString(HDVPSS_SHARED_SIZE);
    var RDK_TOTAL_MEM_STR           = Size2MB(TOTAL_MEM_SIZE);
    var RDK_LINUX_BASE_ADDR_STR     = Value2HexString(LINUX_ADDR);

    var filename = "./env"+"_"+RDK_TOTAL_MEM_STR+"M_"+Size2MB(LINUX_SIZE)+"M.sh";

    print ("generation ",filename);

    /* Create the env.sh file */
    var File = xdc.useModule('xdc.services.io.File');
    var fd = File.open(filename,"w");
    fd.writeLine("#!/bin/sh                                                                    ");
    fd.writeLine("#RDK_LINUX_MEM:                                                              ");
    fd.writeLine("#The amount of memory allocated to linux.                                    ");
    fd.writeLine("#The kernel bootargs mem= parameter should match this value.                 ");
    fd.writeLine(GetShellExportString("RDK_LINUX_MEM",RDK_LINUX_MEM_STR));
    fd.writeLine("#The start address of kernel NOTIFY_MEM                                      ");
    fd.writeLine("");
    fd.writeLine("#The kernel bootargs notifyk.vpssm3_sva= parameter should match this value.  ");
    fd.writeLine(GetShellExportString("NOTIFYK_VPSSM3_SVA_ADDR",NOTIFYK_VPSSM3_SVA_ADDR_STR));
    fd.writeLine("");
    fd.writeLine("#The start address of REMOTE_DEBUG_ADDR section                              ");
    fd.writeLine("#The address of REMOTE_DEBUG_MEM in the slave executables should match this  ");
    fd.writeLine(GetShellExportString("REMOTE_DEBUG_ADDR",REMOTE_DEBUG_ADDR_STR));
    fd.writeLine("");
    fd.writeLine("#The start address of ETH_OFFLOAD section                              ");
    fd.writeLine(GetShellExportString("ETH_OFFLOAD_ADDR",ETH_OFFLOAD_ADDR_STR));
    fd.writeLine("");
    fd.writeLine("#The start address of HDVPSS_SHARED_MEM section                              ");
    fd.writeLine("#The address of HDVPSS_SHARED_MEM in the slave executables should match this ");
    fd.writeLine(GetShellExportString("HDVPSS_SHARED_MEM",HDVPSS_SHARED_MEM_STR));
    fd.writeLine("");
    fd.writeLine("#The size of HDVPSS_SHARED_MEM section                              ");
    fd.writeLine(GetShellExportString("HDVPSS_SHARED_SIZE",HDVPSS_SHARED_SIZE_STR));
    fd.writeLine("");
    fd.writeLine("#Kernel base address (physical address)                             ");
    fd.writeLine(GetShellExportString("RDK_LINUX_BASE_ADDR",RDK_LINUX_BASE_ADDR_STR));
    fd.writeLine("");
    

    fd.close();
    print ("Generation of Shell script in completed...");
}


function drawLine (term_char,h_char,len)
{
    let i = 0;
    let print_str = new Array(term_char);

    for (i = 0; i < len; i++)
    {
        print_str.push(h_char);
    }
    print_str.push(term_char);
    return (print_str.join(""));
}

String.prototype.replaceAt=function(index, str) {
      return this.substr(0, index) + str + this.substr(index+str.length);
}

function drawCenteredText(textBox,textString)
{

    return (textBox.replaceAt((textBox.length/2) - (textString.length / 2),textString));

}

function pad(number, length) {
    var str = '' + number;
    while (str.length < length) str = '0' + str;
    return str.toUpperCase();
}

function dec2hexStr(number) { 
    return "0x" + pad(Number(IntType(number)).toString(16),8); 
}

function printBox (size, comment,isFinal)
{
    let width = 20;
    let height = 4;
    let size_str;
    let textBoxString;
    let prefixString;
    let prefixStringDefault = "                    ";
    let startAddress;
    let addressString;

    if( typeof printBox.total == 'undefined' ) {
        printBox.total = 0;
    }

     
    startAddress = DDR3_ADDR + printBox.total;
    addressString = dec2hexStr(startAddress);
    prefixString = prefixStringDefault.replaceAt((prefixStringDefault.length/2 - addressString.length/2),addressString);
    if (size >= MB)
    {
        size_str = (size / MB) + " MB";
    }
    else
    {
        size_str = (size / KB) + " KB";
    }
    print(prefixString + drawLine('+','-',width));
    print(prefixStringDefault + drawLine('|',' ',width));
    textBoxString = drawCenteredText(drawLine('|',' ',width),size_str);
    print(prefixStringDefault + textBoxString + " " + comment);
    print(prefixStringDefault + drawLine('|',' ',width));
    if (isFinal == true)
    {
        startAddress += size;
        addressString = dec2hexStr(startAddress - 1);
        prefixString = prefixStringDefault.replaceAt((prefixStringDefault.length/2 - addressString.length/2),addressString);

        print(prefixString + drawLine('+','-',width));
    }
    printBox.total  += size;
}

function printBoxFinal (size, comment)
{
    printBox (size, comment,true);
    
}
