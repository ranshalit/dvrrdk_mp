/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#define ti_sysbios_family_c64p_Cache__nolocalnames
#define ti_sysbios_hal_Cache__nolocalnames
#include "scdLink_priv.h"
#include "scd_ti.h"
#include "ti/sdo/fc/dskt2/dskt2.h"
#include "ti/sdo/fc/rman/rman.h"
#include <mcfw/interfaces/common_def/ti_vsys_common_def.h>
#include <mcfw/src_bios6/utils/utils_mem.h>
#include <ti/sysbios/family/c64p/Cache.h>

static UInt8 gScratchId = 1;
static Int32 AlgLink_ScdalgSetChScdPrm(AlgLink_ScdChPrm    *pScdChPrm,
                            AlgLink_ScdChParams * params);

#define UTILS_SCD_OUTBUF_SIZE()   (sizeof(AlgLink_ScdResult))
#define ALG_LINK_SCD_MAX_DMACOPY_FRAMES_PER_CH       (2)

#define UTILS_PRINTF_MAX_ARG1_LEN (500)
char utils_strArg1[UTILS_PRINTF_MAX_ARG1_LEN];

int utils_fflush(FILE * fp)
{ 
       return 0;
}

Int32 utils_printf(const char *format, ...)
{
    va_list va;
    int len;
    va_start(va, format);
    len = vsnprintf(utils_strArg1, UTILS_PRINTF_MAX_ARG1_LEN, format, va);

    utils_strArg1[len] = '\n';
    utils_strArg1[len] = '\0';
    Vps_printf(" %d: SCD   : VA :%s \n", Utils_getCurTimeInMsec(), utils_strArg1);
    va_end(va);
    return 0 ;
}


HPTimeStamp AlgLink_ScdGetFrameTS(AlgLink_ScdChObj * pChObj,UInt32 timeStampMsec)
{    
#if 1
    UInt32 interFrameInterval;
 
    interFrameInterval = (1000000/(pChObj->frameSkipCtx.outputFrameRate));

    pChObj->frameTS.clock.tv_usec += interFrameInterval;
//    Vps_printf("InterFrameInterval  %d O/p Framerate %d\n", interFrameInterval, pChObj->frameSkipCtx.outputFrameRate); 
//    Vps_printf("Sec %d uSec %d\n", pAlgObj->frameTS.clock.tv_sec, pAlgObj->frameTS.clock.tv_usec); 
    if(pChObj->frameTS.clock.tv_usec >= (interFrameInterval * (pChObj->frameSkipCtx.outputFrameRate)))
    {
       pChObj->frameTS.clock.tv_sec ++;
       pChObj->frameTS.clock.tv_usec = 0;
    } 
//    Vps_printf("Sec %d uSec %d\n", pAlgObj->frameTS.clock.tv_sec, pAlgObj->frameTS.clock.tv_usec);
#else  
    pChObj->frameTS.clock.tv_sec  = (timeStampMsec/1000);
    pChObj->frameTS.clock.tv_usec = (timeStampMsec % 1000) * 1000;
#endif 
   
    return (pChObj->frameTS);
} 


Int32 AlgLink_ScdVAInitParams(AlgLink_ScdChPrm *pChPrm) //(AlgLink_ScdObj * pObj)
{
    pChPrm->chDMVLParam.size         = sizeof(DMVALparams);
    pChPrm->chDMVLParam.imgType      = DMVAL_IMG_LUMA;
    pChPrm->chDMVLParam.maxHeight    = pChPrm->height;
    pChPrm->chDMVLParam.maxWidth     = pChPrm->width;
    pChPrm->chDMVLParam.detectMode   = DMVAL_DETECTMODE_TAMPER;

    pChPrm->chDMVLParam.edmaBaseAddr = NULL;
    pChPrm->chDMVLParam.edmaChId     = -1;
    pChPrm->chDMVLParam.edmaQId      = -1;

    pChPrm->sensitiveness = DMVAL_SENSITIVITY_LEVEL_1;
    pChPrm->maxNoObj      = ALG_LINK_SCD_VA_MAX_OBJECTS;
    pChPrm->direction     = DMVAL_DIRECTION_UNSPECIFIED;

    pChPrm->objectInView  = 0x1;  /* Enabling only People in view */

    pChPrm->minPersonSize[0]  = (ALG_LINK_VA_MINPERSONWIDTH * pChPrm->width)/100;
    pChPrm->minVehicleSize[0] = (ALG_LINK_VA_MINVEHICLEWIDTH * pChPrm->width)/100;
    pChPrm->maxPersonSize[0]  = (ALG_LINK_VA_MAXPERSONWIDTH * pChPrm->width)/100;
    pChPrm->maxVehicleSize[0] = (ALG_LINK_VA_MAXVEHICLEWIDTH * pChPrm->width)/100;
    pChPrm->minPersonSize[1]  = (ALG_LINK_VA_MINPERSONHEIGHT * pChPrm->height)/100;
    pChPrm->minVehicleSize[1] = (ALG_LINK_VA_MINVEHICLEHEIGHT * pChPrm->height)/100;
    pChPrm->maxPersonSize[1]  = (ALG_LINK_VA_MAXPERSONHEIGHT * pChPrm->height)/100;
    pChPrm->maxVehicleSize[1] = (ALG_LINK_VA_MAXVEHICLEHEIGHT * pChPrm->height)/100;

    Vps_printf("MinPersonWidth-1 %d MaxPersonWidth-1 %d \n", pChPrm->minPersonSize[0], pChPrm->maxPersonSize[0]);
    Vps_printf("MinPersonWidth-2 %d MaxPersonWidth-2 %d \n", pChPrm->minPersonSize[1], pChPrm->maxPersonSize[1]);
    Vps_printf("MinVehicleWidth-1 %d MaxVehicleWidth-1 %d \n", pChPrm->minVehicleSize[0], pChPrm->maxVehicleSize[0]);
    Vps_printf("MinVehicleWidth-2 %d MaxVehicleWidth-2 %d \n", pChPrm->minVehicleSize[1], pChPrm->maxVehicleSize[1]);

    pChPrm->numPolygons = 2;

    /* First Polygon */
    pChPrm->polygon[0].size      = sizeof(DMVALpolygon);
    pChPrm->polygon[0].polygonID = 1;
    pChPrm->polygon[0].valid     = 1;
    pChPrm->polygon[0].type      = DMVAL_IMD;//DMVAL_TZ_1A;
    pChPrm->polygon[0].numPoints = 5;
    pChPrm->polygon[0].dir       = DMVAL_DIRECTION_UNSPECIFIED;

    pChPrm->polygon[0].pt[0].x   = (10 * pChPrm->width)/100;
    pChPrm->polygon[0].pt[0].y   = (10 * pChPrm->height)/100;
    pChPrm->polygon[0].pt[1].x   = (50 * pChPrm->width)/100;
    pChPrm->polygon[0].pt[1].y   = (10 * pChPrm->height)/100;
    pChPrm->polygon[0].pt[2].x   = (50 * pChPrm->width)/100;
    pChPrm->polygon[0].pt[2].y   = (90 * pChPrm->height)/100;
    pChPrm->polygon[0].pt[3].x   = (10 * pChPrm->width)/100;
    pChPrm->polygon[0].pt[3].y   = (90 * pChPrm->height)/100;
    pChPrm->polygon[0].pt[4].x   = pChPrm->polygon[0].pt[0].x;
    pChPrm->polygon[0].pt[4].y   = pChPrm->polygon[0].pt[0].y;

    /* Second Polygon */
    pChPrm->polygon[1].size      = sizeof(DMVALpolygon);
    pChPrm->polygon[1].polygonID = 2;
    pChPrm->polygon[1].valid     = 1;
    pChPrm->polygon[1].type      = DMVAL_IMD;//DMVAL_TZ_1B;
    pChPrm->polygon[1].numPoints = 5;
    pChPrm->polygon[1].dir       = DMVAL_DIRECTION_UNSPECIFIED;

    pChPrm->polygon[1].pt[0].x   = (50 * pChPrm->width)/100;
    pChPrm->polygon[1].pt[0].y   = (10 * pChPrm->height)/100;
    pChPrm->polygon[1].pt[1].x   = (90 * pChPrm->width)/100;
    pChPrm->polygon[1].pt[1].y   = (10 * pChPrm->height)/100;
    pChPrm->polygon[1].pt[2].x   = (90 * pChPrm->width)/100;
    pChPrm->polygon[1].pt[2].y   = (90 * pChPrm->height)/100;
    pChPrm->polygon[1].pt[3].x   = (50 * pChPrm->width)/100;
    pChPrm->polygon[1].pt[3].y   = (90 * pChPrm->height)/100;
    pChPrm->polygon[1].pt[4].x   = pChPrm->polygon[1].pt[0].x;
    pChPrm->polygon[1].pt[4].y   = pChPrm->polygon[1].pt[0].y;

    pChPrm->polygonRtUpdated = TRUE;
#ifdef SYSTEM_DEBUG_SCD
    Vps_printf(" %d: SCD   : VA ALG Init Done of Channel - %d!!!\n", Utils_getCurTimeInMsec(),pChPrm->chId);
#endif 

    return FVID2_SOK;
}
Int32 AlgLink_ScdResetParam(AlgLink_ScdObj * pObj)
{
    AlgLink_ScdChPrm    *pChPrm; 
    AlgLink_ScdChObj    *pChObj;
    Int32       scdChId;

    UInt32      numHorzBlks, numVertBlks;
    UInt32      i, x, y;

    for(scdChId = 0;  scdChId<pObj->createArgs.numValidChForSCD; scdChId++)
    {
        pChObj    = &pObj->chObj[scdChId];
        pChPrm    = &pObj->chParams[scdChId];

        numHorzBlks    = pObj->createArgs.maxWidth / 32;
        numVertBlks    = pObj->createArgs.maxHeight / 12;

        i = 0;
        for(y = 0; y < numVertBlks; y++)
        {
            for(x = 0; x < numHorzBlks; x++)
            {
                pChPrm->blkConfig[i].sensitivity = (SCD_Sensitivity) ALG_LINK_SCD_SENSITIVITY_MID;
                pChPrm->blkConfig[i].monitored   = 0;
                i++;
            }
        }

#ifdef SYSTEM_DEBUG_SCD
       Vps_printf(" %d: SCD   : Block Config Reset Done of Channel - %d!!!\n", Utils_getCurTimeInMsec(),pChObj->chId);
#endif
    }
    return FVID2_SOK;
}

static Int32 AlgLink_ScdCreateOutObj(AlgLink_ScdObj * pObj)
{
    AlgLink_ScdOutObj *pOutObj;

    Int32 status;
    UInt32 bufIdx;
    Int i,j,queueId;
    UInt32 totalBufCnt;

    for(queueId = 0; queueId < ALG_LINK_MAX_OUT_QUE; queueId++)
    {

        pOutObj = &pObj->outObj[queueId];

        pObj->outObj[queueId].numAllocPools = 1;

        pOutObj->buf_size[0] = UTILS_SCD_OUTBUF_SIZE();
        pOutObj->buf_size[0] = 
          VpsUtils_align(pOutObj->buf_size[0], 
                         SharedRegion_getCacheLineSize(SYSTEM_IPC_SR_CACHED));

        status = Utils_bitbufCreate(&pOutObj->bufOutQue, TRUE, FALSE,
                                    pObj->outObj[queueId].numAllocPools);
        UTILS_assert(status == FVID2_SOK);

        totalBufCnt = 0;
        for (i = 0; i < pOutObj->numAllocPools; i++)
        {
            pOutObj->outNumBufs[i] = (pObj->createArgs.numValidChForSCD * pObj->createArgs.numBufPerCh);

            for (j = 0; j < pObj->createArgs.numValidChForSCD; j++)
            {
                pOutObj->ch2poolMap[j] =  i;
            }

            status = Utils_memBitBufAlloc(&(pOutObj->outBufs[totalBufCnt]),
                                          pOutObj->buf_size[i],
                                          pOutObj->outNumBufs[i]);
            UTILS_assert(status == FVID2_SOK);

            for (bufIdx = 0; bufIdx < pOutObj->outNumBufs[i]; bufIdx++)
            {
                UTILS_assert((bufIdx + totalBufCnt) < ALG_LINK_SCD_MAX_OUT_FRAMES);
                pOutObj->outBufs[bufIdx + totalBufCnt].allocPoolID = i;
                pOutObj->outBufs[bufIdx + totalBufCnt].doNotDisplay =
                    FALSE;
                status =
                    Utils_bitbufPutEmptyBuf(&pOutObj->bufOutQue,
                                            &pOutObj->outBufs[bufIdx +
                                                              totalBufCnt]);
                UTILS_assert(status == FVID2_SOK);
            }
            /* align size to minimum required frame buffer alignment */
            totalBufCnt += pOutObj->outNumBufs[i];
        }
    }

    return (status);
}

Int32 AlgLink_ScdAlgChCreate(AlgLink_ScdObj * pObj)
{
    Int32               status, scdChId;
    AlgLink_ScdChParams *pChLinkPrm;
    AlgLink_ScdChPrm    *pChPrm;
    AlgLink_ScdChObj    *pChObj;
    FVID2_Frame         *pFrame;
    UInt32               frameId;

    for(scdChId = 0;  scdChId<pObj->createArgs.numValidChForSCD; scdChId++)
    {
        pChPrm     = &pObj->chParams[scdChId];
        pChLinkPrm = &pObj->createArgs.chDefaultParams[scdChId];
        pChObj     = &pObj->chObj[scdChId];

        pChObj->frameSkipCtx.firstTime       = TRUE;

        if((pChLinkPrm->inputFrameRate == 0) || (pChLinkPrm->outputFrameRate == 0))
        {
            pChObj->frameSkipCtx.inputFrameRate  = pObj->createArgs.inputFrameRate;
            pChObj->frameSkipCtx.outputFrameRate = pObj->createArgs.outputFrameRate;
        }
        else
        {
            pChObj->frameSkipCtx.inputFrameRate  = pChLinkPrm->inputFrameRate;
            pChObj->frameSkipCtx.outputFrameRate = pChLinkPrm->outputFrameRate;
        }

        pChObj->scdChStat                    = ALG_LINK_SCD_DETECTOR_UNAVAILABLE;
        pChObj->chId                         = pChLinkPrm->chId;

        status = AlgLink_ScdalgSetChScdPrm(pChPrm, pChLinkPrm);

        if(pObj->createArgs.thresold2WaitAfterFrmAlert > ALG_LINK_TAMPER_ALERT_MAX_THRESOLD)
        {
            Vps_printf("%d: SCD    : Chan ID %d: Value of thresold2WaitAfterFrmAlert is OutOfRange \
                             Limiting to Max Supported.\n",
                   Utils_getCurTimeInMsec(), pChPrm->chId);
            pObj->createArgs.thresold2WaitAfterFrmAlert = ALG_LINK_TAMPER_ALERT_MAX_THRESOLD;
        }
        if(pObj->createArgs.thresold2WaitB4FrmAlert > ALG_LINK_TAMPER_ALERT_MAX_THRESOLD)
        {
            Vps_printf("%d: SCD    : Chan ID %d: Value of thresold2WaitB4FrmAlert is OutOfRange \
                             Limiting to Max Supported.\n",
                   Utils_getCurTimeInMsec(), pChPrm->chId);
            pObj->createArgs.thresold2WaitB4FrmAlert = ALG_LINK_TAMPER_ALERT_MAX_THRESOLD;
        }
        pChPrm->thresold2WaitAfterFrmAlert   = pObj->createArgs.thresold2WaitAfterFrmAlert;
        pChPrm->thresold2WaitB4FrmAlert      = pObj->createArgs.thresold2WaitB4FrmAlert;

        pChPrm->tamperBGRefreshInterval        = pChLinkPrm->scdVaAlgCfg.tamperBGRefreshInterval;
        pChPrm->rtPrmUpdate                  = FALSE;
        pChPrm->algReset                     = FALSE;
        UTILS_assert(status==0);

        pChPrm->width  = pObj->inQueInfo->chInfo[pChPrm->chId].width +
                                pObj->inQueInfo->chInfo[pChPrm->chId].startX;
        pChPrm->height = pObj->inQueInfo->chInfo[pChPrm->chId].height +
                                pObj->inQueInfo->chInfo[pChPrm->chId].startY;
        pChPrm->stride = pObj->inQueInfo->chInfo[pChPrm->chId].pitch[0];

#ifdef SYSTEM_DEBUG_SCD
        Vps_printf(" %d: SCD    : Chan ID %d: Resolution %d x %d, In FPS = %d, Out FPS = %d!!!\n",
                   Utils_getCurTimeInMsec(),
                    pChPrm->chId,
                    pChPrm->width,
                    pChPrm->height,
                    pChObj->frameSkipCtx.inputFrameRate,
                    pChObj->frameSkipCtx.outputFrameRate
            );
#endif

         status = Utils_queCreate(&pChObj->scdProcessObj.freeQ,
                                  pObj->createArgs.numBufPerCh,
                                  pChObj->scdProcessObj.freeQMem,
                                  UTILS_QUE_FLAG_NO_BLOCK_QUE
                                     );

         UTILS_assert(status==FVID2_SOK);

         pChObj->scdProcessObj.processFrameSize = pChPrm->stride * pChPrm->height;

         pChObj->scdProcessObj.processFrameSize = \
                                        VpsUtils_align(pChObj->scdProcessObj.processFrameSize, 
                                        SharedRegion_getCacheLineSize(SYSTEM_IPC_SR_CACHED));

         for(frameId=0; frameId<ALG_LINK_SCD_MAX_OUT_FRAMES_PER_CH; frameId++)
         {
             pChObj->scdProcessObj.perFrameCfg[frameId].bufId = 0;
         }
         /* alloc channel process buffer memory */

         for(frameId=0; frameId<ALG_LINK_SCD_MAX_DMACOPY_FRAMES_PER_CH; frameId++)
         {
             pFrame = &pChObj->scdProcessObj.processFrames[frameId];
             pFrame->appData = &pChObj->scdProcessObj.processFrameInfo[frameId];
             pFrame->addr[0][0] =
                     Utils_memAlloc(
                         pChObj->scdProcessObj.processFrameSize,
                         SharedRegion_getCacheLineSize(SYSTEM_IPC_SR_CACHED)
                     );
             pChObj->scdProcessObj.perFrameCfg[frameId].bufId = (frameId + 1);
             pFrame->perFrameCfg = (AlgLink_perFrameConfig*)&pChObj->scdProcessObj.perFrameCfg[frameId];


             UTILS_assert(pFrame->addr[0][0]!=NULL);

             status = Utils_quePut(&pChObj->scdProcessObj.freeQ, pFrame, BIOS_NO_WAIT);

             UTILS_assert(status==FVID2_SOK);
         }

    }
    return 0;
}

Int32 AlgLink_ScdVACreate(AlgLink_ScdObj * pObj)
{
    AlgLink_ScdChPrm    *pChPrm; 
    AlgLink_ScdChObj    *pChObj;
    UInt32 status;
    UInt32 libStatus;
    DMVALhandle *pDmvaHndl;
    Int32       scdChId;
    UInt32      sensitivity, tamperBGRefreshInterval;
    UInt32      cacheMarbit;
    Int16 vehiclesInFOV = 0,peopleInFOV = 0;
    UInt32 i;
    UInt32 mode;
    
    for(scdChId = 0;  scdChId<pObj->createArgs.numValidChForSCD; scdChId++)
    {
       pChObj    = &pObj->chObj[scdChId];
       pChPrm    = &pObj->chParams[scdChId];
       pDmvaHndl = &pObj->dmvalHndl[scdChId];

       pDmvaHndl->size     = sizeof(DMVALhandle);
       pChObj->frameTS.clock.tv_sec  = 0;
       pChObj->frameTS.clock.tv_usec = 0; 
       pChObj->output.size = sizeof(DMVALout);
       pChObj->objectCount = 0;

       status = AlgLink_ScdVAInitParams(pChPrm);
       UTILS_assert(status == FVID2_SOK);
       if(scdChId < pObj->createArgs.maxNumVaChan)
       {
          /* Enabling all the VA modes for only few channels */
          pChPrm->chDMVLParam.detectMode = DMVAL_DETECTMODE_TAMPER | DMVAL_DETECTMODE_IMD  |
                                           DMVAL_DETECTMODE_SMETA  | DMVAL_DETECTMODE_TRIP |
                                           DMVAL_DETECTMODE_COUNT;
       }

       sensitivity = pObj->createArgs.chDefaultParams[scdChId].frmSensitivity;

       if(sensitivity > DMVAL_SENSITIVITY_LEVEL_8)
           pChPrm->sensitiveness = DMVAL_SENSITIVITY_LEVEL_8;
       else if(sensitivity < DMVAL_SENSITIVITY_LEVEL_1)
          pChPrm->sensitiveness = DMVAL_SENSITIVITY_LEVEL_1;
       else
          pChPrm->sensitiveness = (DMVALsensitivity) sensitivity;

       pChPrm->detectMode = pObj->createArgs.chDefaultParams[scdChId].mode;
       pChPrm->tamperResetCnt  = ALG_LINK_TAMPER_RESET_COUNT_SCALER_CONSTANT;// * pChObj->frameSkipCtx.outputFrameRate; 
       pChPrm->verboseLevel = (Int16) pObj->createArgs.chDefaultParams[scdChId].scdVaAlgCfg.verboseLevel;
       pChPrm->chVaPrmUpdate = FALSE;

       memset(pDmvaHndl,0x00,sizeof(DMVALhandle));

       libStatus = DMVAL_getMemReq(pDmvaHndl,&pChPrm->chDMVLParam);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf(" %d: SCD   : VA Create Call Status 0x%X!!!\n", 
                 Utils_getCurTimeInMsec(),libStatus);
       }
       UTILS_assert(libStatus == DMVAL_NO_ERROR);   
       pChObj->algMemSize = pDmvaHndl->memTab[0].size;   
           /* ALG mem alloc */
       pChObj->algMemSize = VpsUtils_align(pChObj->algMemSize, 
                                        SharedRegion_getCacheLineSize(SYSTEM_IPC_SR_CACHED));
       
       pChObj->algMemPtr  = Utils_memAlloc(pChObj->algMemSize, SharedRegion_getCacheLineSize(SYSTEM_IPC_SR_CACHED));
        UTILS_assert(pChObj->algMemPtr != NULL);

       cacheMarbit = ti_sysbios_family_c64p_Cache_getMar(pChObj->algMemPtr);
#ifdef SYSTEM_DEBUG_SCD
       Vps_printf(" %d: SCD   : VA MEMALLOC %d bytes for DMVAL internal memory at addr 0x%X!!!\n", 
                       Utils_getCurTimeInMsec(),pChObj->algMemSize,pChObj->algMemPtr);

       if(cacheMarbit != ti_sysbios_family_c64p_Cache_Mar_ENABLE)
       {
          Vps_printf(" %d: SCD   : VA Internal memory allocated at Non-Cached!!!\n",  \
                       Utils_getCurTimeInMsec());
       }
#endif
       UTILS_assert(cacheMarbit == ti_sysbios_family_c64p_Cache_Mar_ENABLE);

#ifdef SYSTEM_DEBUG_SCD
       Vps_printf(" %d: VA   : MEMALLOC %d bytes for DMVAL memory at addr 0x%X!!!\n", 
               Utils_getCurTimeInMsec(),pChObj->algMemSize,pChObj->algMemPtr);
#endif 

       pDmvaHndl->memTab[0].base = pChObj->algMemPtr;
       libStatus = DMVAL_create(pDmvaHndl,&pChPrm->chDMVLParam);
       if(libStatus != DMVAL_NO_ERROR)
       {
          Vps_printf(" %d: SCD   : VA Create Call Status 0x%x!!!\n", 
                 Utils_getCurTimeInMsec(),libStatus);
       }

       UTILS_assert(libStatus == DMVAL_NO_ERROR);  
 
       /* DMVAL init is done only for one algo */
       mode = pObj->createArgs.chDefaultParams[scdChId].mode;                    
       pChPrm->chDMVLParam.detectMode = (DMVALdetectMode)(((mode & 0x3C) >> 1) | (mode & 0x1));

       /* DMVAL init is done only for one algo */
       Vps_printf("%d: SCD   : VA Detect Mode is 0x%x \n", Utils_getCurTimeInMsec(), pChPrm->chDMVLParam.detectMode);
       libStatus = DMVAL_initModule(pDmvaHndl, pChPrm->chDMVLParam.detectMode);
       if(libStatus != DMVAL_NO_ERROR)
       {
          Vps_printf(" %d: SCD   : VA Create Call Status 0x%x!!!\n", 
                 Utils_getCurTimeInMsec(),libStatus);
       }

       UTILS_assert(libStatus == DMVAL_NO_ERROR);  

       vehiclesInFOV = (pChPrm->objectInView & 0x2);
       peopleInFOV   = (pChPrm->objectInView & 0x1);
       
       pChPrm->detectionRate = ALG_LINK_VA_DETECTIONRATE;
       pChPrm->tzMaxSpatialOverlap  = 10;     // Default value from VA test bench
       pChPrm->tzMinTemporalOverlap = 1000;   // Default value from VA test bench

       /* DMVAL set params */

       for(i = 0; i < pChPrm->numPolygons; i++)
       {
          if(pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_TRIP)
          {
             Vps_printf(" %d: SCD   : VA Create Call Setting TRIP Zone Config!!!\n", 
                   Utils_getCurTimeInMsec());

             pChPrm->polygon[0].type = DMVAL_TZ_1A;
             pChPrm->polygon[1].type = DMVAL_TZ_1B;
          }
          else if(pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_COUNT)
          {
             Vps_printf(" %d: SCD   : VA Create Call Setting OC Config!!!\n", 
                   Utils_getCurTimeInMsec());

             pChPrm->polygon[0].type = DMVAL_OC_1A;
             pChPrm->polygon[1].type = DMVAL_OC_1B;
          }
          else
          {
             Vps_printf(" %d: SCD   : VA Create Call Setting IMD  Config!!!\n", 
                   Utils_getCurTimeInMsec());

             pChPrm->polygon[0].type = DMVAL_IMD;
             pChPrm->polygon[1].type = DMVAL_IMD;
          }          

          libStatus = DMVAL_setROI(pDmvaHndl,&(pChPrm->polygon[i]));
          if(libStatus != DMVAL_NO_ERROR)
          {
             Vps_printf("WARNING:%s:Error 0x%x while setting ROI-%d\n",__func__, libStatus, i);
          }
       }
       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_DBG_VERBOSE_LEVEL,&(pChPrm->verboseLevel),1);  
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_DBG_VERBOSE_LEVEL\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(
                          pDmvaHndl,DMVAL_PARAM_TAMPERSENSITIVITY,
                          (Int16*)&(pChPrm->sensitiveness),
                          1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPERSENSITIVITY\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(
                           pDmvaHndl,DMVAL_PARAM_SENSITIVITY,
                          (Int16*)&(pChPrm->sensitiveness),
                          1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_SENSITIVITY\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(
                          pDmvaHndl,DMVAL_PARAM_TAMPER_BCK_RESET_INTERVAL, //DMVAL_PARAM_TAMPERCOUNTER,
                          (Int16*)&(pChPrm->tamperResetCnt),
                          1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPER_BCK_RESET_INTERVAL\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(
                          pDmvaHndl,DMVAL_PARAM_TAMPER_BLOCKUP_TH, 
                          (Int16*)&(pChPrm->thresold2WaitB4FrmAlert),
                          1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPER_BLOCKUP_TH\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(
                          pDmvaHndl,DMVAL_PARAM_TAMPER_BLOCKDOWN_TH, 
                          (Int16*)&(pChPrm->thresold2WaitAfterFrmAlert),
                          1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPER_BLOCKDOWN_TH\n",__func__,libStatus);
       }

       tamperBGRefreshInterval = (Int16)pChPrm->tamperBGRefreshInterval;// * pChObj->frameSkipCtx.outputFrameRate;
       libStatus = DMVAL_setParameter(
                          pDmvaHndl,DMVAL_PARAM_TAMPER_BACKGROUND_INTERVAL, //DMVAL_PARAM_TAMPER_BACKGROUND_FRM_INTERVAL, 
                          (Int16*)&tamperBGRefreshInterval, 
                          1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPER_BACKGROUND_INTERVAL\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_MINPERSONSIZE,pChPrm->minPersonSize,2);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_MINPERSONSIZE\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_MAXPERSONSIZE,pChPrm->maxPersonSize,2);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_MAXPERSONSIZE\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_MINVEHICLESIZE,pChPrm->minVehicleSize,2);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_MINVEHICLESIZE\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_MAXVEHICLESIZE,pChPrm->maxVehicleSize,2);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_MAXVEHICLESIZE\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_INVIEW_VEHICLES,&vehiclesInFOV,1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_INVIEW_VEHICLES\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_INVIEW_PEOPLE,&peopleInFOV,1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_INVIEW_PEOPLE\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_OCDIRECTION,(Int16*)&(pChPrm->direction),1);
       if(libStatus != DMVAL_NO_ERROR)
       {
           Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_OCDIRECTION\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_TZDIRECTION,(Int16*)&(pChPrm->direction),1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TZDIRECTION\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_DETECTIONRATE,&(pChPrm->detectionRate),1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_DETECTIONRATE\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_OVERLAPPED_EVENTS_AREA_RATIO,&(pChPrm->tzMaxSpatialOverlap),1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_OVERLAPPED_EVENTS_AREA_RATIO\n",__func__,libStatus);
       }

       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_OVERLAPPED_EVENTS_INTERVAL,&(pChPrm->tzMinTemporalOverlap),1);
       if(libStatus != DMVAL_NO_ERROR)
       {
         Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_OVERLAPPED_EVENTS_INTERVAL\n",__func__,libStatus);
       }

       pChPrm->orientation = 1;
       libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_OCORIENTATION,&(pChPrm->orientation),1);
       if(libStatus != DMVAL_NO_ERROR)
       {
           Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_OCORIENTATION\n",__func__,libStatus);
       }


#ifdef SYSTEM_DEBUG_SCD
       Vps_printf(" %d: SCD   : VA ALG Create Done of Channel - %d!!!\n", Utils_getCurTimeInMsec(),pChObj->chId);
#endif
       if(!pObj->createArgs.chDefaultParams[scdChId].useDefaultVaAlgCfg)
          AlgLink_ScdalgSetChVaParam(pObj, &pObj->createArgs.chDefaultParams[scdChId].scdVaAlgCfg);
    }
    AlgLink_ScdPrintVaChConfig(pObj);
    return FVID2_SOK;
} 

Int32 AlgLink_ScdLMDCreate(AlgLink_ScdObj * pObj)
{

    Int32               chId;
    SCD_createPrm       algCreatePrm;
    SCD_chPrm           chDefaultParams[ALG_LINK_SCD_MAX_CH];
    IALG_Fxns           *algFxns = (IALG_Fxns *)&SCD_TI;

    if((pObj->createArgs.maxWidth > ALG_LINK_SCD_MAX_FRAME_WIDTH) || 
        (pObj->createArgs.maxHeight > ALG_LINK_SCD_MAX_FRAME_HEIGHT))
    {
          Vps_printf(" %d: SCD    : Error!!! Input resolution WxH %d x %d is more than Max WxH = %d x %d!!!\n",
           Utils_getCurTimeInMsec(),
            pObj->createArgs.maxWidth,
            pObj->createArgs.maxHeight,
            ALG_LINK_SCD_MAX_FRAME_WIDTH,
            ALG_LINK_SCD_MAX_FRAME_HEIGHT);

          return FVID2_EFAIL;
    }
    
    algCreatePrm.maxWidth    = pObj->createArgs.maxWidth;
    algCreatePrm.maxHeight   = pObj->createArgs.maxHeight;
    algCreatePrm.maxStride   = pObj->createArgs.maxStride;
    algCreatePrm.maxChannels = pObj->createArgs.numValidChForSCD;
    algCreatePrm.numSecs2WaitB4Init        = pObj->createArgs.numSecs2WaitB4Init;
    algCreatePrm.numSecs2WaitB4FrmAlert    = pObj->createArgs.numSecs2WaitB4FrmAlert;
    algCreatePrm.numSecs2WaitAfterFrmAlert = pObj->createArgs.numSecs2WaitAfterFrmAlert;
    algCreatePrm.fps                       = (SCD_Fps) pObj->createArgs.outputFrameRate;
    algCreatePrm.chDefaultParams           = (SCD_chPrm *)&chDefaultParams[0];

    for(chId=0; chId < algCreatePrm.maxChannels; chId++)
    {
        SCD_chPrm      * chl = &(algCreatePrm.chDefaultParams[chId]);
        AlgLink_ScdChParams  * chCreatePrm = &(pObj->createArgs.chDefaultParams[chId]);
        if ((chCreatePrm->mode == SCD_DETECTMODE_MONITOR_BLOCKS) ||
             (chCreatePrm->mode == SCD_DETECTMODE_MONITOR_BLOCKS_FRAME))
        {
          chl->blkConfig = (SCD_blkChngConfig *)chCreatePrm->blkConfig;
        }
        else
        {
          chl->blkConfig = NULL;
        }

        // The remaining parameter values filled in here do not really matter as
        // they will be over-written by calls to SCD_TI_setPrms. We'll fill in
        // just a few
        chl->chId     = chCreatePrm->chId;
        chl->mode     = (SCD_Mode)(chCreatePrm->mode & 0x2);
        chl->width    = pObj->createArgs.maxWidth;
        chl->height   = pObj->createArgs.maxHeight;
        chl->stride   = pObj->createArgs.maxStride;
        chl->curFrame = NULL;

        chl->frmSensitivity     = (SCD_Sensitivity)chCreatePrm->frmSensitivity;
        chl->frmIgnoreLightsON  = chCreatePrm->frmIgnoreLightsON;
        chl->frmIgnoreLightsOFF = chCreatePrm->frmIgnoreLightsOFF;
        chl->frmEdgeThreshold   = chCreatePrm->frmEdgeThreshold;
        
        if( pObj->inQueInfo->chInfo[chId].dataFormat != FVID2_DF_YUV420SP_UV)
        {
#ifdef SYSTEM_DEBUG_SCD
           Vps_printf(" %d: SCD    : Channel No. %d Create ERROR - Input Format Not Supported !!!\n",
                 Utils_getCurTimeInMsec(), chCreatePrm->chId);
#endif
             return FVID2_EFAIL;
        }

    }

    /* Create algorithm instance and get algo handle  */
    pObj->algHndl = DSKT2_createAlg((Int)gScratchId,
            (IALG_Fxns *)algFxns, NULL,(IALG_Params *)&algCreatePrm);

    if(pObj->algHndl == NULL)
    {
#ifdef SYSTEM_DEBUG_SCD
        Vps_printf(" %d: SCD    : Create ERROR !!!\n",
               Utils_getCurTimeInMsec());
#endif

        return FVID2_EFAIL;
    }

    return FVID2_SOK;
}


Int32 AlgLink_ScdalgCreate(AlgLink_ScdObj * pObj)
{
    Int32       status;

#ifdef SYSTEM_DEBUG_SCD
    Vps_printf(" %d: SCD    : Create in progress !!!\n", Utils_getCurTimeInMsec());
#endif

    pObj->totalFrameCount = 0;

    if(pObj->createArgs.numBufPerCh == 0)
        pObj->createArgs.numBufPerCh = ALG_LINK_SCD_MAX_OUT_FRAMES_PER_CH;

    if(pObj->createArgs.numBufPerCh > ALG_LINK_SCD_MAX_OUT_FRAMES_PER_CH)
    {
        Vps_printf("\n SCDLINK: WARNING: User is asking for %d buffers per CH. But max allowed is %d. \n"
            " Over riding user requested with max allowed \n\n",
            pObj->createArgs.numBufPerCh, ALG_LINK_SCD_MAX_OUT_FRAMES_PER_CH
            );

        pObj->createArgs.numBufPerCh = ALG_LINK_SCD_MAX_OUT_FRAMES_PER_CH;

    }

#ifdef SYSTEM_DEBUG_SCD
    Vps_printf(" %d: SCD    : Max WxH = %d x %d, Max CH = %d, FPS = %d !!!\n",
           Utils_getCurTimeInMsec(),
            pObj->createArgs.maxWidth,
            pObj->createArgs.maxHeight,
            pObj->createArgs.numValidChForSCD,
            pObj->createArgs.outputFrameRate
        );
#endif

    if( pObj->createArgs.numValidChForSCD > pObj->inQueInfo->numCh)
    {
#ifdef SYSTEM_DEBUG_SCD
        Vps_printf(" %d: SCD    : Create ERROR - SCD channels < InQueue Channels !!!\n",
               Utils_getCurTimeInMsec());
#endif

        return FVID2_EFAIL;
    }

    status = AlgLink_ScdLMDCreate(pObj);
    UTILS_assert(status == FVID2_SOK);

    AlgLink_ScdAlgChCreate(pObj);


    status = AlgLink_ScdVACreate(pObj);
    UTILS_assert(status == FVID2_SOK);
 
    AlgLink_ScdCreateOutObj(pObj);

    AlgLink_ScdresetStatistics(pObj);

    status = Utils_dmaCreateChDSP(&pObj->dmaCh, UTILS_DMA_DEFAULT_EVENT_Q, 1, FALSE);

    UTILS_assert(status==FVID2_SOK);

    status = Utils_queCreate(&pObj->processQ,
                             pObj->createArgs.numBufPerCh * pObj->createArgs.numValidChForSCD,
                             pObj->processQMem,
                             UTILS_QUE_FLAG_BLOCK_QUE_GET
                            );
    UTILS_assert(status==FVID2_SOK);

    AlgLink_scdAlgProcessTskSendCmd(pObj, SYSTEM_CMD_START);

#ifdef SYSTEM_DEBUG_SCD
    Vps_printf(" %d: SCD    : Create Done !!!\n",
               Utils_getCurTimeInMsec());
#endif

 return FVID2_SOK;
}

Int32 AlgLink_ScdAlgChDelete(AlgLink_ScdObj * pObj)
{
    AlgLink_ScdChObj *pChObj;
    FVID2_Frame *pFrame;
    UInt32 frameId;
    UInt32 chId;
    Int32 status;

    for(chId=0; chId<pObj->createArgs.numValidChForSCD; chId++)
    {
        pChObj = &pObj->chObj[chId];

        status = Utils_queDelete(&pChObj->scdProcessObj.freeQ);

        UTILS_assert(status==FVID2_SOK);

        /* free chann process buffer memory */

        for(frameId=0; frameId<ALG_LINK_SCD_MAX_DMACOPY_FRAMES_PER_CH; frameId++)
        {
            pFrame = &pChObj->scdProcessObj.processFrames[frameId];

            status = Utils_memFree(
                        pFrame->addr[0][0], pChObj->scdProcessObj.processFrameSize
                            );

            UTILS_assert(status==FVID2_SOK);
        }
    }

    return FVID2_SOK;
}

 
Int32 AlgLink_ScdVADelete(AlgLink_ScdObj * pObj)
{
    AlgLink_ScdChObj *pChObj;
    UInt32 chId;

    /* Unless process task is stopped, don't free up memory 
     * It would avoid Alg call with junk data as this task and process task are 
     * independent.*/
    while(pObj->processTskState != ALG_LINK_PROCESS_TASK_STATE_STOPPED);

    for(chId=0; chId<pObj->createArgs.numValidChForSCD; chId++)
    {
        pChObj = &pObj->chObj[chId];

        /* Free ALG mem */
        Utils_memFree(pChObj->algMemPtr,pChObj->algMemSize);
    }
    return FVID2_SOK;
}

Int32 AlgLink_ScdLMDDelete(AlgLink_ScdObj * pObj)
{
    Int32 scratchId = gScratchId;
    UInt32 outId, status;

    if(pObj->algHndl == NULL)
        return FVID2_EFAIL;

    DSKT2_freeAlg(scratchId, (IALG_Handle)pObj->algHndl);

    for (outId = 0; outId < ALG_LINK_MAX_OUT_QUE; outId++)
    {
        Int32 i,bitbuf_index;
        AlgLink_ScdOutObj *pOutObj;

        pOutObj = &pObj->outObj[outId];

        status = Utils_bitbufDelete(&pOutObj->bufOutQue);
        UTILS_assert(status == FVID2_SOK);

        bitbuf_index = 0;
        for (i = 0; i < pOutObj->numAllocPools; i++)
        {
            UTILS_assert((pOutObj->outBufs[bitbuf_index].bufSize ==
                          pOutObj->buf_size[i]));
            status = Utils_memBitBufFree(&pOutObj->outBufs[bitbuf_index],
                                pOutObj->outNumBufs[i]);
            UTILS_assert(status == FVID2_SOK);
            bitbuf_index += pOutObj->outNumBufs[i];
        }
    }
   UTILS_assert(status==FVID2_SOK);

   return FVID2_SOK;
}

Int32 AlgLink_ScdalgDelete(AlgLink_ScdObj * pObj)
{
    Int32 status = FVID2_SOK;

#ifdef SYSTEM_DEBUG_SCD
    Vps_printf(" %d: SCD    : Delete in progress !!!\n",
               Utils_getCurTimeInMsec());
#endif

    AlgLink_scdAlgProcessTskSendCmd(pObj, SYSTEM_CMD_STOP);

    status = Utils_dmaDeleteChDSP(&pObj->dmaCh);
    UTILS_assert(status==FVID2_SOK);

    /* Delete SCD VA Handle and Free VA memory*/
    status = AlgLink_ScdVADelete(pObj);
    UTILS_assert(status==FVID2_SOK);

    /* Deleting process queue after process task delete as it might be in use */
    status = Utils_queDelete(&pObj->processQ);
    UTILS_assert(status==FVID2_SOK);

    /* Delete SCD LMD Handle and Free Out Queue buffer used by LMD */
    status = AlgLink_ScdLMDDelete(pObj);
    UTILS_assert(status==FVID2_SOK);

    /* Delete SCD Channels Object and Release memory held by FreeQ etc.*/
    status = AlgLink_ScdAlgChDelete(pObj);
    UTILS_assert(status==FVID2_SOK);

#ifdef SYSTEM_DEBUG_SCD
    Vps_printf(" %d: SCD    : Delete Done !!!\n",
               Utils_getCurTimeInMsec());
#endif

    return status;
}

Int32 AlgLink_ScdalgProcess(AlgLink_ScdObj *pScdAlgLinkObj, UInt32 chId, AlgLink_ScdResult * scdResultBuff)
{
    SCD_Status      chanStatus;
    SCD_Result      scdResult;
    UInt32          chanID;
    SCD_chPrm       chanParam;
    AlgLink_ScdChPrm * chPrm;
    UInt32         blkIdx;

    chanID = pScdAlgLinkObj->chParams[chId].chId;
    chPrm = &(pScdAlgLinkObj->chParams[chId]);

    chanParam.chId                 = chPrm->chId;
    chanParam.mode                 = (SCD_Mode)(chPrm->mode & 0x2);
    chanParam.width                = chPrm->width;
    chanParam.height               = chPrm->height;
    chanParam.stride               = chPrm->width;//chPrm->stride;
    chanParam.curFrame             = chPrm->curFrame;
    chanParam.frmSensitivity       = (SCD_Sensitivity)chPrm->frmSensitivity;
    chanParam.frmIgnoreLightsON    = chPrm->frmIgnoreLightsON;
    chanParam.frmIgnoreLightsOFF   = chPrm->frmIgnoreLightsOFF;
    chanParam.frmEdgeThreshold     = chPrm->frmEdgeThreshold;

    if(chPrm->chBlkConfigUpdate == TRUE)
    {
        chanParam.blkConfig = (SCD_blkChngConfig *) chPrm->blkConfig;
    }
    else
    {
        chanParam.blkConfig = NULL;
    }

    chanStatus = SCD_TI_setPrms(pScdAlgLinkObj->algHndl, &chanParam, chanID);

    if(chanStatus != SCD_NO_ERROR)
    {
#ifdef SYSTEM_DEBUG_SCD
        Vps_printf(" %d: SCD    : ERROR: Alg Set Params (chanID = %d) - 0x%08X !!!\n",
                  Utils_getCurTimeInMsec(), chanID, chanStatus);
#endif
        return FVID2_EFAIL;
    }


    scdResult.frmResult = SCD_DETECTOR_NO_TAMPER;
    scdResult.blkResult = (SCD_blkChngMeta *)(scdResultBuff->blkResult);

    chanStatus = SCD_TI_process(pScdAlgLinkObj->algHndl, chanID, &scdResult);

    if(chanStatus != SCD_NO_ERROR)
    {
#ifdef SYSTEM_DEBUG_SCD
        Vps_printf(" %d: SCD    : ERROR: Alg Process (chanID = %d) !!!\n",
                  Utils_getCurTimeInMsec(), chanID );
#endif

        return FVID2_EFAIL;
    }

//    scdResultBuff->frmResult |= (AlgLink_ScdOutput) scdResult.frmResult;
    scdResultBuff->chId = chanID;


    /* Motion detect event notification. */
     UInt32 numBlkChg;
     UInt32 monitoredBlk;
     UInt32 numHorzBlks, numVertBlks, numBlksInFrame, blkHeight;

     monitoredBlk = 0;
     numBlkChg = 0;
     numHorzBlks     = ((pScdAlgLinkObj->chParams[chId].width + 0x1F ) & (~0x1F)) / 32;  /* Rounding to make divisible by 32 */
     if((pScdAlgLinkObj->chParams[chId].height%ALG_LINK_SCD_BLK_HEIGHT_MIN) == 0)        /* For Block height is divisible by 10 */
        blkHeight = ALG_LINK_SCD_BLK_HEIGHT_MIN;
     else   /* For Block height is divisible by 12 */
        blkHeight = ALG_LINK_SCD_BLK_HEIGHT;
     numVertBlks    = pScdAlgLinkObj->chParams[chId].height / blkHeight;

     numBlksInFrame = numHorzBlks * numVertBlks;

     /* Logic  to see how many blocks of total enabled blocks experienced change.
      * For each block, algorithm returns no. of pixels changed in the current
      * frame. This is compared against thresold determined using SCD sensitivity .
      * if changed pixels are more than the calculated thresold, block is marked as changed
      * i.e. motion is detected in the block.
      * Minimum value of thresold is 20% and then it is incrmented by 10% for
      * each sensitivity level change. Thresold can vary from 20% - 100%
      * At max sensitivity, thresold would be 20%. That means if 20% pixels
      * are changed block is marked as changed.
      * At minimu sensitivity, thresold would be 100%. That means if 100% pixels
      * are changed block is marked as changed */

     memcpy(&scdResultBuff->blkConfig, &pScdAlgLinkObj->chParams[chId].blkConfig, (sizeof(SCD_blkChngConfig) * numBlksInFrame));
     for(blkIdx = 0; blkIdx < numBlksInFrame; blkIdx++)
     {
         SCD_blkChngConfig * blockConfig;

         blockConfig = &pScdAlgLinkObj->chParams[chId].blkConfig[blkIdx];
//         scdResultBuff->blkConfig[blkIdx].monitored   = blockConfig->monitored;
//         scdResultBuff->blkConfig[blkIdx].sensitivity = blockConfig->sensitivity;
         if(blockConfig->monitored == 1)
         {
             UInt32 threshold;

             monitoredBlk++;
             threshold = MOTION_DETECTION_SENSITIVITY(ALG_LINK_SCD_BLK_WIDTH, blkHeight) +
                               (MOTION_DETECTION_SENSITIVITY_STEP * (SCD_SENSITIVITY_MAX - blockConfig->sensitivity));

             if(scdResultBuff->blkResult[blkIdx].numPixelsChanged > threshold)
             {
                 numBlkChg++;
             }
         }
 
         /* If Motion detection mode ( **_MONITOR_BLOCK) is disabled of block is 
          * not monitor then set the values in the Result buff to zero to avoid 
          * false trigger/alarm. */

         if(((pScdAlgLinkObj->chParams[chId].mode & SCD_DETECTMODE_MONITOR_BLOCKS) == FALSE) || 
              (blockConfig->monitored == 0))
         {
              scdResultBuff->blkResult[blkIdx].numPixelsChanged  = 0;
              scdResultBuff->blkResult[blkIdx].numFrmsBlkChanged = 0;
         }
     }

     /* Logic  to notify A8-host about motion detection.
      * Currently, if atleast 1 block is detected as changed A8 is notified.
      * User can use commented logic to chnage this behavior. 
      * if((monitoredBlk > 0) && (numBlkChg > (NUM_BLOCK_MOTION_DETECTION_THRESOLD(monitoredBlk)))) 
      */

    if(pScdAlgLinkObj->createArgs.enableMotionNotify)
    {
        if((monitoredBlk > 0) && (numBlkChg > 0))
        {
#ifdef SYSTEM_DEBUG_SCD_RT
            Vps_printf(" %d: SCD    : Motion Detected (chanID = %d),  !!!\n",
                   Utils_getCurTimeInMsec(), chanID );
#endif
            System_linkControl(SYSTEM_LINK_ID_HOST, VSYS_EVENT_MOTION_DETECT, scdResultBuff, sizeof(AlgLink_ScdResult), TRUE);
        }
    }

    return FVID2_SOK;
}

Int32 AlgLink_ScdalgProcessData(AlgLink_ScdObj * pObj)
{
    UInt32 curTime, i;
    FVID2_Frame *pFrame;
    Int32 chIdx, status;
    Bitstream_Buf *pOutBuf;
    Bitstream_BufList outBitBufList;
    System_FrameInfo *pInFrameInfo;
    Bool doFrameDrop;
    AlgLink_ScdChObj *pChObj;
    AlgLink_ScdChPrm    *pChPrm; 
    Utils_BitBufHndl *bufOutQue;
    UInt32 libStatus;
    DMVALimage inImage;
    Bool frameProcessedTAMPER,frameProcessedLMD; 
    UInt32 frameId;
    DMVALout *pOutput;
    AlgLink_ScdResult * scdOutput;
    bufOutQue = &pObj->outObj[0].bufOutQue;
    outBitBufList.numBufs = 0;
    outBitBufList.appData = NULL;
    frameProcessedTAMPER = FALSE;
    frameProcessedLMD    = FALSE;
    status = FVID2_EFAIL;

    pFrame = NULL;

    Utils_queGet(&pObj->processQ, (Ptr*)&pFrame, 1, BIOS_WAIT_FOREVER);

    if(pFrame==NULL)
        return FVID2_EFAIL;
    do
    {
        for(chIdx = 0; chIdx < pObj->createArgs.numValidChForSCD; chIdx++)
        {
          if(pFrame->channelNum == pObj->chParams[chIdx].chId)
          {
            break;
          }
        }

        pChObj = &pObj->chObj[chIdx]; 
        pChPrm    = &pObj->chParams[chIdx];

        ti_sysbios_family_c64p_Cache_inv(pFrame->addr[0][0], pChObj->scdProcessObj.processFrameSize, 
                                        ti_sysbios_family_c64p_Cache_Type_ALL,TRUE);
      
        pInFrameInfo = (System_FrameInfo *) pFrame->appData;

        doFrameDrop = FALSE;
        pOutBuf = NULL;
        pObj->totalFrameCount++; 

        status = Utils_bitbufGetEmptyBuf(bufOutQue,
                                         &pOutBuf,
                                         0, //pObj->outObj.ch2poolMap[chIdx], /*Need to change later.*/
                                         BIOS_NO_WAIT);

        if(pChPrm->chVaPrmUpdate)
        {
           AlgLink_ScdalgSetChVaParam(pObj, &pChPrm->newVaParams);
           pChPrm->chVaPrmUpdate = FALSE;
        }
           
        if(!((status == FVID2_SOK) && (pOutBuf)))
        {
           doFrameDrop = TRUE;
        }

        if(doFrameDrop == FALSE)
        {
            scdOutput = (AlgLink_ScdResult *) pOutBuf->addr;
            if( (pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_TAMPER) || 
                 (pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_IMD) || 
                  (pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_TRIP) || 
                   (pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_COUNT) ||
                   (pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_SMETA))
            {
                pOutBuf->lowerTimeStamp = (UInt32)(pInFrameInfo->ts64 & 0xFFFFFFFF);
                pOutBuf->upperTimeStamp = (UInt32)(pInFrameInfo->ts64 >> 32);

                 /***************************
                    VA ALG APPLY HERE
                 ***************************/      
                 DMVALhandle *pDmvaHndl = &pObj->dmvalHndl[chIdx];

                 inImage.size        = sizeof(DMVALimage);
                 inImage.pixelDepth  = DMVAL_PIXEL_U08;
                 inImage.width       = pChPrm->width;    
                 inImage.height      = pChPrm->height;
                 inImage.imageStride = pChPrm->width;//pChPrm->stride;
                 inImage.imageData   = pFrame->addr[0][0];
                 inImage.inputId     = ((AlgLink_perFrameConfig*)pFrame->perFrameCfg)->bufId; 
                 inImage.pixelDepth  = DMVAL_PIXEL_U08;
                 inImage.type        = DMVAL_IMG_LUMA;
                 inImage.horzOffset  = 0;
                 inImage.vertOffset  = 0;
                 inImage.focusValue  = ALG_LINK_DEFAULT_FOCUSVALUE;
                 inImage.timeStamp   = AlgLink_ScdGetFrameTS(pChObj,pFrame->timeStamp);

                 curTime = Utils_getCurTimeInMsec(); 
     

                 libStatus = DMVAL_NO_ERROR;
                    
//                Vps_printf(" %d: SCD   : Ch-Id %d Frame Id is %d, pFrameAdd 0x%x!!!\n", 
//                        Utils_getCurTimeInMsec(),pChPrm->chId, inImage.inputId, pFrame->addr[0][0]);
//                Vps_printf("Time Stamp in Sec is %d in uSec is %d\n", inImage.timeStamp.clock.tv_sec, inImage.timeStamp.clock.tv_usec);
                 libStatus = DMVAL_process(pDmvaHndl,&inImage, &pChObj->output);
                 if(libStatus != DMVAL_NO_ERROR)
                 {
                    Vps_printf(" %d: SCD   : VA Create Call Status 0x%x!!!\n", 
                           Utils_getCurTimeInMsec(),libStatus);
                 }

                 UTILS_assert(libStatus == DMVAL_NO_ERROR); 

                 pChObj->inFrameProcessTime += (Utils_getCurTimeInMsec() - curTime);
                 frameProcessedTAMPER = TRUE;

                 pOutput = &pChObj->output;  

                 if(pOutput->modeResult & DMVAL_DETECTOR_COUNTER)
                 {
                      pChObj->objectCount += pOutput->numObjects;
                 }

                  if(pOutput->modeResult & DMVAL_TAMPER_BLUR)
                      pOutput->modeResult = DMVAL_DETECTOR_NONE;

                  scdOutput->mode        = pChPrm->detectMode;
                  scdOutput->frmResult   = pOutput->modeResult;
                  scdOutput->numObjects  = pOutput->numObjects;
                  scdOutput->objectCount = pChObj->objectCount;
                  if(scdOutput->numObjects)
                  {
                    for(i = 0; i < scdOutput->numObjects; i++)
                    {
                        scdOutput->objMetadata[i].objBox.width  = pOutput->objMetadata[i].objBox.width;
                        scdOutput->objMetadata[i].objBox.height = pOutput->objMetadata[i].objBox.height;
                        scdOutput->objMetadata[i].objBox.startX = pOutput->objMetadata[i].objBox.xmin;
                        scdOutput->objMetadata[i].objBox.startY = pOutput->objMetadata[i].objBox.ymin;
                        scdOutput->objMetadata[i].objCentroid.x = pOutput->objMetadata[i].objCentroid.x;
                        scdOutput->objMetadata[i].objCentroid.y = pOutput->objMetadata[i].objCentroid.y;
                        scdOutput->objMetadata[i].zoneId  = pOutput->objMetadata[i].roiID;
                    }
                  }
                  scdOutput->polygonRtUpdated = pChPrm->polygonRtUpdated;
                  if(pObj->createArgs.enableVaEvntNotify)
                  {
                      AlgLink_ScdVAProcessMetaData(pObj, chIdx);
                  }
                  if(pChPrm->polygonRtUpdated)
                  {
                    Vps_printf(" %d: SCD   : VA Polygon param updated \n", Utils_getCurTimeInMsec());
                    scdOutput->numPolygons = pChPrm->numPolygons;
                    for(i = 0; i < pChPrm->numPolygons; i++)
                    {
                        UInt32 loopCnt;

                        scdOutput->zoneCfg[i].zoneId        = pChPrm->polygon[i].polygonID;
                        scdOutput->zoneCfg[i].activeZone    = pChPrm->polygon[i].valid;
                        scdOutput->zoneCfg[i].zoneType      = pChPrm->polygon[i].type;
                        scdOutput->zoneCfg[i].noOfPoints    = pChPrm->polygon[i].numPoints;
                        scdOutput->zoneCfg[i].zoneDirection = pChPrm->polygon[i].dir;
                        scdOutput->zoneCfg[i].zoneType      = pChPrm->polygon[i].type;

                        for(loopCnt = 0; loopCnt < scdOutput->zoneCfg[i].noOfPoints; loopCnt++)
                        {
                            scdOutput->zoneCfg[i].pixLocation[loopCnt].x = pChPrm->polygon[i].pt[loopCnt].x;
                            scdOutput->zoneCfg[i].pixLocation[loopCnt].y = pChPrm->polygon[i].pt[loopCnt].y;
                        }
                    }
                    pChPrm->polygonRtUpdated = FALSE;
                  }
                  if(pObj->createArgs.enableTamperNotify)
                  {

                       /* Tamper detect event notification. */
                     if((pOutput->modeResult & DMVAL_TAMPER_SCENECHANGE ) || (pOutput->modeResult & DMVAL_DETECTOR_TAMPER))
                     {
                        if((pChObj->scdChStat == ALG_LINK_SCD_DETECTOR_UNAVAILABLE) ||
                           (pChObj->scdChStat == ALG_LINK_SCD_DETECTOR_NO_CHANGE))
                        {
                           AlgLink_ScdChStatus pChStatus;
                           pChStatus.frmResult = (UInt32) ALG_LINK_SCD_DETECTOR_CHANGE;
                           pChStatus.chId      = pChPrm->chId;

                           System_linkControl(SYSTEM_LINK_ID_HOST, 
                                              VSYS_EVENT_TAMPER_DETECT, 
                                              &pChStatus, 
                                              sizeof(AlgLink_ScdChStatus), 
                                            TRUE);
                        }
                     }
                     else
                     {
                        if(pChObj->scdChStat == ALG_LINK_SCD_DETECTOR_CHANGE)
                        {
                           AlgLink_ScdChStatus pChStatus;
                           pChStatus.frmResult = (UInt32) ALG_LINK_SCD_DETECTOR_NO_CHANGE;
                           pChStatus.chId      = pChPrm->chId;

                           System_linkControl(SYSTEM_LINK_ID_HOST, 
                                              VSYS_EVENT_TAMPER_DETECT, 
                                              &pChStatus, 
                                              sizeof(AlgLink_ScdChStatus), 
                                            TRUE);
                        }
                     }

                  }
                  else
                  {
   #ifdef SYSTEM_DEBUG_SCD_RT
                     Vps_printf(" %d [TAMPER Result %d] : SCD CH ID %d !!!\n",
                            Utils_getCurTimeInMsec(), pOutput->modeResult, pChPrm->chId );
   #endif                        
                     if((pOutput->modeResult & DMVAL_TAMPER_SCENECHANGE ) || (pOutput->modeResult & DMVAL_DETECTOR_TAMPER))
                     {
                        if((pChObj->scdChStat == ALG_LINK_SCD_DETECTOR_UNAVAILABLE) ||
                           (pChObj->scdChStat == ALG_LINK_SCD_DETECTOR_NO_CHANGE))
                        {
//                             Vps_printf(" %d [TAMPER DETECTED] : SCD CH ID %d !!!\n",
//                                    Utils_getCurTimeInMsec(), pChPrm->chId
//                                    );
                        }
                     }
                     else
                     {
                        if(pChObj->scdChStat == ALG_LINK_SCD_DETECTOR_CHANGE)
                        {
//                             Vps_printf(" %d [TAMPER REMOVED] : SCD CH ID %d !!!\n",
//                                    Utils_getCurTimeInMsec(), pChPrm->chId
//                                    );
                        }
                     }
                  }

                  pChObj->scdChStat       = ALG_LINK_SCD_DETECTOR_NO_CHANGE;
                  if(pOutput->modeResult == DMVAL_TAMPER_NONE)
                  {
                      pChObj->scdChStat = ALG_LINK_SCD_DETECTOR_NO_CHANGE;
                  }
                  else if((pOutput->modeResult & DMVAL_TAMPER_SCENECHANGE) || (pOutput->modeResult & DMVAL_DETECTOR_TAMPER))
                  {
                      pChObj->scdChStat = ALG_LINK_SCD_DETECTOR_CHANGE;
                  }

            }
            else
            {
                pChObj->scdChStat       = ALG_LINK_SCD_DETECTOR_UNAVAILABLE;           
            }

            pObj->chParams[chIdx].curFrame = pFrame->addr[0][0];
            if(pChPrm->algReset || pChPrm->rtPrmUpdate)
            {
                AlgLink_ScdResetParam(pObj);
                pChPrm->algReset    = FALSE;
                pChPrm->rtPrmUpdate = FALSE;
            }

            pOutBuf->lowerTimeStamp = (UInt32)(pInFrameInfo->ts64 & 0xFFFFFFFF);
            pOutBuf->upperTimeStamp = (UInt32)(pInFrameInfo->ts64 >> 32);
            pOutBuf->channelNum = pFrame->channelNum;
            pOutBuf->fillLength = sizeof(AlgLink_ScdResult);
            pOutBuf->frameWidth = pChPrm->width;
            pOutBuf->frameHeight = pChPrm->height;

            curTime = Utils_getCurTimeInMsec();

             /***************************
               SCD LMD ALG PROCESS CALL
             ***************************/      

            AlgLink_ScdalgProcess(pObj, chIdx, (AlgLink_ScdResult *) pOutBuf->addr);
            pChObj->inFrameProcessTime += (Utils_getCurTimeInMsec() - curTime);

            frameProcessedLMD = TRUE;
            pObj->chParams[chIdx].chBlkConfigUpdate = FALSE;

            outBitBufList.bufs[outBitBufList.numBufs] = pOutBuf;
            outBitBufList.numBufs++;
        }



        if(frameProcessedTAMPER || frameProcessedLMD)
        {
            pChObj->inFrameProcessCount++;
            /* Cache write Back if result in o/p is updated */
            ti_sysbios_family_c64p_Cache_wb(pOutBuf->addr, sizeof(AlgLink_ScdResult), 
                                            ti_sysbios_family_c64p_Cache_Type_ALL,TRUE);

        }
        else
            pChObj->inFrameProcessSkipCount++;

        for(frameId = 0;frameId < DMVAL_MAX_BUFFER_LOCK;frameId ++)
        {
             /* The array ends with Id equal to 0 */
           if(pChObj->output.freeBufId[frameId] == 0)
           {
               break;
           }
           pFrame = &pChObj->scdProcessObj.processFrames[pChObj->output.freeBufId[frameId] - 1];
//           Vps_printf(" %d: SCD   : Frame No. %d Frame Id is %d, pFrameAdd 0x%x!!!\n", 
//                    Utils_getCurTimeInMsec(),frameId, pChObj->output.freeBufId[frameId], pFrame->addr[0][0]);
           /* Put the frames back which are marked as free by DMVAL_process */
//           status = Utils_bufPutEmptyFrame(&pObj->outFrameQue,pEmptyFrame);
           status = Utils_quePut(&pChObj->scdProcessObj.freeQ, pFrame, BIOS_NO_WAIT);
           UTILS_assert(status == FVID2_SOK);
        }

        if((outBitBufList.numBufs == pObj->createArgs.numValidChForSCD) ||
            (pObj->processTskState == ALG_LINK_PROCESS_TASK_STATE_STOPPING))
        {
            break;
        }

        pFrame = NULL;
        status = Utils_queGet(&pObj->processQ, (Ptr*)&pFrame, 1, BIOS_NO_WAIT);

        if (UTILS_ISERROR(status)) {
            break;
        }
    }while(pFrame != NULL);

    if (outBitBufList.numBufs)
    {
        status = Utils_bitbufPutFull(bufOutQue,
                                     &outBitBufList);
        UTILS_assert(status == FVID2_SOK);
        status = FVID2_SOK;
    }
    return status;
}

Int32 AlgLink_ScdAlgCopyFrame(AlgLink_ScdObj * pObj, FVID2_Frame *pFrame)
{
    FVID2_Frame *pEmptyFrame;
    AlgLink_ScdChPrm * pChPrm;
    AlgLink_ScdChObj * pChObj;
    Utils_DmaCopy2D dmaPrm;
    Int32 status, chIdx;

    pEmptyFrame = NULL;

    /* Search through ID list to get the proper channel no. */
    for(chIdx = 0; chIdx < pObj->createArgs.numValidChForSCD; chIdx++)
    {
      if(pFrame->channelNum == pObj->createArgs.chDefaultParams[chIdx].chId)
      {
        break;
      }
    }

    pChPrm = &pObj->chParams[chIdx];
    pChObj = &pObj->chObj[chIdx];;

    AlgLink_ScdAlgRtPrmUpdate(pObj, pChPrm, pFrame);

    Utils_queGet(&pChObj->scdProcessObj.freeQ, (Ptr*)&pEmptyFrame, 1, BIOS_NO_WAIT);

    if(pEmptyFrame==NULL)
    {
        /* no free available for SCD, so skipping this frame */
        pChObj->inFrameUserSkipCount++;
        return FVID2_SOK;
    }
    ti_sysbios_family_c64p_Cache_inv(pEmptyFrame->addr[0][0], pChObj->scdProcessObj.processFrameSize, 
                                    ti_sysbios_family_c64p_Cache_Type_ALL,TRUE);

    memcpy(pEmptyFrame->appData, pFrame->appData, sizeof(System_FrameInfo));    
    /* found a free frame, do DMA copy of Y data to this frame */

    pEmptyFrame->timeStamp = pFrame->timeStamp;

    dmaPrm.destAddr[0]  = pEmptyFrame->addr[0][0];
    dmaPrm.destPitch[0] = pChPrm->width;//pChPrm->stride;

    dmaPrm.srcAddr[0]  = pFrame->addr[0][0];
    dmaPrm.srcPitch[0] = pChPrm->stride;

    /* need to transfer only Y data, so set data format as 422I and set width = actual width / 2 */
    dmaPrm.dataFormat = FVID2_DF_YUV422I_YUYV;
    dmaPrm.srcStartX  = 0;
    dmaPrm.srcStartY  = 0;
    dmaPrm.destStartX = 0;
    dmaPrm.destStartY = 0;
    dmaPrm.width      = pChPrm->width/2;
    dmaPrm.height     = pChPrm->height;

    status = Utils_dmaCopy2D(&pObj->dmaCh, &dmaPrm, 1);
    UTILS_assert(status==FVID2_SOK);

    pEmptyFrame->channelNum = pFrame->channelNum;

    status = Utils_quePut(&pObj->processQ, pEmptyFrame, BIOS_NO_WAIT);

    if(status!=FVID2_SOK)
    {
        /* cannot submit frame now process queue is full, release frame to free Q */
        pChObj->inFrameUserSkipCount++;

        status = Utils_quePut(&pChObj->scdProcessObj.freeQ, pEmptyFrame, BIOS_NO_WAIT);

        /* this assert should never occur */
        UTILS_assert(status==FVID2_SOK);

        return FVID2_SOK;
    }

    return FVID2_SOK;
}

Int32 AlgLink_ScdAlgSubmitFrames(AlgLink_ScdObj * pObj, FVID2_FrameList *pFrameList)
{
    Int32 status = FVID2_SOK;
    UInt32 frameId, chIdx;
    FVID2_Frame *pFrame;
    AlgLink_ScdChObj *pChObj;
    Bool skipFrame;
    Int32 frameFound;
    
    for(frameId=0; frameId<pFrameList->numFrames; frameId++)
    {
        pFrame = pFrameList->frames[frameId];

        if(pFrame==NULL)
            continue;

        frameFound = 0;

        /* Search through ID list to get the proper channel no. */
        for(chIdx = 0; chIdx < pObj->createArgs.numValidChForSCD; chIdx++)
        {
            if(pFrame->channelNum == pObj->createArgs.chDefaultParams[chIdx].chId)
            {
              frameFound = 1;
              break;
            }
        }

        /* If no corresponding channel is enabled, discard/return the frame */
        if(frameFound == 0)
            continue;

        UTILS_assert(chIdx < UTILS_ARRAYSIZE(pObj->chObj));

        pChObj = &pObj->chObj[chIdx];
        pChObj->inFrameRecvCount++;
      
        /* Frame rate control at input of SCD link */
        skipFrame = Utils_doSkipFrame(&pChObj->frameSkipCtx);
        if( (skipFrame) || (pObj->createArgs.chDefaultParams[chIdx].mode == 0))
        {
            pChObj->inFrameUserSkipCount++;
            continue;
        }

        AlgLink_ScdAlgCopyFrame(pObj, pFrame);
    }

    return status;
}


Int32 AlgLink_ScdalgSetChblkUpdate(AlgLink_ScdObj * pObj,
                            AlgLink_ScdChblkUpdate * params)
{
    AlgLink_ScdChPrm *pScdChPrm;
    Int32 channelUpdated, idx;

    channelUpdated = FVID2_EFAIL;
    // Search through ID list to get the proper channel no.
    for(idx = 0; idx < pObj->createArgs.numValidChForSCD; idx++)
    {
        pScdChPrm = &pObj->chParams[idx];
        if(params->chId == pScdChPrm->chId)
        {
            pScdChPrm->chBlkConfigUpdate = TRUE;
            for(idx = 0; idx < params->numValidBlock; idx++)
            {
                SCD_blkChngConfig * blkConfig;
                blkConfig = (SCD_blkChngConfig *) ((UInt32)(pScdChPrm->blkConfig) + \
                               (sizeof(SCD_blkChngConfig) * params->blkConfig[idx].blockId));

                blkConfig->monitored = params->blkConfig[idx].monitorBlock;
                blkConfig->sensitivity = (SCD_Sensitivity) params->blkConfig[idx].sensitivity;

                channelUpdated = FVID2_SOK;
            }
            break;
        }
    }

    return (channelUpdated);
}

Int32 AlgLink_ScdalgUpdateChVaParam(AlgLink_ScdObj * pObj,
                            AlgLink_ScdChVaParams * params)
{
    AlgLink_ScdChPrm *pScdChPrm;
    Int32 status = FVID2_EFAIL;
    UInt32 idx;
    // Search through ID list to get the proper channel no.
    for(idx = 0; idx < pObj->createArgs.numValidChForSCD; idx++)
    {
        pScdChPrm = &pObj->chParams[idx];
        if(params->chId == pScdChPrm->chId)
        {
          Vps_printf("\n %d: SCD   : ########## Link Call to Update VA Params of Ch-%d########## \n", Utils_getCurTimeInMsec(), params->chId);
          memcpy(&pScdChPrm->newVaParams, params, sizeof(AlgLink_ScdChVaParams));

          pScdChPrm->chVaPrmUpdate = TRUE;
          status = FVID2_SOK;
          break;
        }
    }
    if(status == FVID2_EFAIL)
       Vps_printf(" %d: SCD  : VA ALG VA Param Update: Invalid Channel No. - %d!!!\n", \
                                 Utils_getCurTimeInMsec(), params->chId);
   return status;
}
Int32 AlgLink_ScdalgSetChVaParam(AlgLink_ScdObj * pObj,
                            AlgLink_ScdChVaParams * params)
{
    AlgLink_ScdChPrm *pScdChPrm;
    AlgLink_ScdChObj    *pScdChObj;
    Int32 idx, i;
    Int32 status = FVID2_EFAIL;
    UInt32 sensitivity, libStatus;
    DMVALhandle *pDmvaHndl;
    Bool algoUpdate;

    Vps_printf("\n %d: SCD   : ##########  Process Task Call to Update VA Params ########## \n", Utils_getCurTimeInMsec());
    // Search through ID list to get the proper channel no.
    for(idx = 0; idx < pObj->createArgs.numValidChForSCD; idx++)
    {
        Bool nonTamperAlgo = FALSE;
        pScdChPrm = &pObj->chParams[idx];
        pScdChObj = &pObj->chObj[idx];
        algoUpdate = FALSE;
        if(params->chId == pScdChPrm->chId)
        {
          memcpy(&pScdChPrm->newVaParams, params, sizeof(AlgLink_ScdChVaParams));
          UInt32 vaMode;
          UInt32 numAlgoEnabled =0;

          vaMode = params->mode;
          vaMode = (((vaMode & 0x3C) >> 1) | (vaMode & 0x1));

          if(vaMode & DMVAL_DETECTMODE_IMD)
          {
            nonTamperAlgo = TRUE;
            numAlgoEnabled++;
          }
          if (vaMode & DMVAL_DETECTMODE_TRIP)
          {
            nonTamperAlgo = TRUE;
            numAlgoEnabled++;
          }
          if (vaMode & DMVAL_DETECTMODE_COUNT)
          {
            nonTamperAlgo = TRUE;
            numAlgoEnabled++;
          }
          if (vaMode & DMVAL_DETECTMODE_SMETA)
          {
            nonTamperAlgo = TRUE;
            numAlgoEnabled++;
          }

          if(numAlgoEnabled > 1)
          {
              Vps_printf(" %d: #######################  WARNING  ######################\n", Utils_getCurTimeInMsec()); 
              Vps_printf(" %d: SCD   : More Than 1 (%d) VA Detection Modes are Enabled.\n", Utils_getCurTimeInMsec(), numAlgoEnabled); 
              Vps_printf(" %d: SCD   : Not Updating VA Params. Exiting. \n", Utils_getCurTimeInMsec());
              Vps_printf(" %d: #######################  WARNING  ######################\n", Utils_getCurTimeInMsec()); 
              break;
          }

          Vps_printf(" %d: SCD   : ########## Updating VA Params of Ch - %d ########## \n", Utils_getCurTimeInMsec(), pScdChPrm->chId);
          pDmvaHndl   = &pObj->dmvalHndl[idx];

          /* Check if detect mode has changed, if YES, reset algorithm */
          /* VA Tamper mode update */
          if(pScdChPrm->detectMode != vaMode)
          {
              Vps_printf(" %d: SCD   : VA Detection Mode has changed From Mode %d to New Mode %d\n", 
                           Utils_getCurTimeInMsec(), pScdChPrm->detectMode, params->mode);
              pScdChPrm->detectMode = params->mode;
              pScdChPrm->chDMVLParam.detectMode = (DMVALdetectMode)vaMode;
              pScdChObj->objectCount = 0;
              /* DMVAL init with new detect mode */
              libStatus = DMVAL_initModule(pDmvaHndl, pScdChPrm->chDMVLParam.detectMode);
              UTILS_assert(libStatus == DMVAL_NO_ERROR);
              algoUpdate = TRUE;
          }
          
          /* Setting Sensitivity */
          if(algoUpdate || (pScdChPrm->sensitiveness != params->sensitiveness))
          {
              sensitivity = params->sensitiveness;
              if(sensitivity > DMVAL_SENSITIVITY_LEVEL_8)
                  pScdChPrm->sensitiveness = DMVAL_SENSITIVITY_LEVEL_8;
              else if(sensitivity < DMVAL_SENSITIVITY_LEVEL_1)
                 pScdChPrm->sensitiveness = DMVAL_SENSITIVITY_LEVEL_1;
              else
                 pScdChPrm->sensitiveness = (DMVALsensitivity) sensitivity;

              /* Setting Tamper Sensitivity */
              if(pScdChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_TAMPER)
              {
                 libStatus = DMVAL_setParameter(pDmvaHndl, DMVAL_PARAM_TAMPERSENSITIVITY, (Int16*)&(pScdChPrm->sensitiveness), 1);
                  if(libStatus != DMVAL_NO_ERROR)
                  {
                     Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPERSENSITIVITY\n",__func__,libStatus);
                  }
              }
              /********************************************************************/
              libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_SENSITIVITY,(Int16*)&(pScdChPrm->sensitiveness),1);
              if(libStatus != DMVAL_NO_ERROR)
              {
                 Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_SENSITIVITY\n",__func__,libStatus);
              }
          }
          /********************************************************************/
          /* Setting BackGround Reset rate */
          if(algoUpdate || ((pScdChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_TAMPER) && (pScdChPrm->tamperResetCnt != params->tamperResetCnt)))
          {
             pScdChPrm->tamperResetCnt  = params->tamperResetCnt;

             libStatus = DMVAL_setParameter(
                                pDmvaHndl,DMVAL_PARAM_TAMPER_BCK_RESET_INTERVAL, //DMVAL_PARAM_TAMPERCOUNTER,
                                (Int16*)&(pScdChPrm->tamperResetCnt),
                                1);
             if(libStatus != DMVAL_NO_ERROR)
             {
               Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPER_BCK_RESET_INTERVAL\n",__func__,libStatus);
             }
          }
          /********************************************************************/

          /* Setting BackGround Refresh rate */
          if(algoUpdate || ((pScdChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_TAMPER) && (pScdChPrm->tamperBGRefreshInterval != params->tamperBGRefreshInterval)))
          {
               UInt32  tamperBGRefreshInterval;

               tamperBGRefreshInterval = (Int16)pScdChPrm->tamperBGRefreshInterval;// * pChObj->frameSkipCtx.outputFrameRate;
               libStatus = DMVAL_setParameter(
                                  pDmvaHndl,DMVAL_PARAM_TAMPER_BACKGROUND_INTERVAL, //DMVAL_PARAM_TAMPER_BACKGROUND_FRM_INTERVAL, 
                                  (Int16*)&tamperBGRefreshInterval, 
                                  1);
               if(libStatus != DMVAL_NO_ERROR)
               {
                 Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPER_BACKGROUND_INTERVAL\n",__func__,libStatus);
               }
          }

          /********************************************************************/
          /* Param Setting of Non-Tamper modes*/
          if(nonTamperAlgo)
          {
              /********************************************************************/
              /* Setting Detection Rate */
              if(algoUpdate || (pScdChPrm->detectionRate != params->detectionRate))
              {
                 Vps_printf(" %d: SCD   : VA DetectionRate has changed from %d to New value %d\n", Utils_getCurTimeInMsec(), pScdChPrm->detectionRate, params->detectionRate);
                 pScdChPrm->detectionRate = params->detectionRate;
                 libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_DETECTIONRATE,&(pScdChPrm->detectionRate),1);
                 if(libStatus != DMVAL_NO_ERROR)
                 {
                   Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_DETECTIONRATE\n",__func__,libStatus);
                 }
              }
              /********************************************************************/
              /* Setting Objects in View */
              if(algoUpdate || (pScdChPrm->objectInView != params->objectInView))
              {
                Int16 vehiclesInFOV, peopleInFOV;

                peopleInFOV   = ((params->objectInView & 0x1) == 0x1);
                vehiclesInFOV = ((params->objectInView & 0x2) == 0x2);
                pScdChPrm->objectInView = params->objectInView;

                Vps_printf(" %d: SCD   : VehiclesInFOV        = %d !!!\n", Utils_getCurTimeInMsec(), vehiclesInFOV);
                Vps_printf(" %d: SCD   : PeopleInFOV          = %d !!!\n", Utils_getCurTimeInMsec(), peopleInFOV);

                libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_INVIEW_PEOPLE,&peopleInFOV,1);
                if(libStatus != DMVAL_NO_ERROR)
                {
                    Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_INVIEW_PEOPLE\n",__func__,libStatus);
                }

                libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_INVIEW_VEHICLES,&vehiclesInFOV,1);
                if(libStatus != DMVAL_NO_ERROR)
                {
                    Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_INVIEW_VEHICLES\n",__func__,libStatus);
                }
              }
              /********************************************************************/

              /* Setting orientation */
              if(algoUpdate && ((pScdChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_COUNT) && (pScdChPrm->orientation != params->orientation)))
              {
                 Vps_printf(" %d: SCD   : VA OC Orientation has changed From %d to New value %d\n", Utils_getCurTimeInMsec(), pScdChPrm->orientation, params->orientation);
                 pScdChPrm->orientation = params->orientation;
                 libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_OCORIENTATION,&(pScdChPrm->orientation),1);
                 if(libStatus != DMVAL_NO_ERROR)
                 {
                     Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_OCORIENTATION\n",__func__,libStatus);
                 }
              }
              /********************************************************************/

              /* Setting direction */
              if((pScdChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_COUNT) && (algoUpdate || (pScdChPrm->direction != params->direction)))
              {
                 Vps_printf(" %d: SCD   : VA OC Direction has changed from %d to New value %d\n", Utils_getCurTimeInMsec(), pScdChPrm->direction, params->direction);
                 pScdChPrm->direction = params->direction;
                 libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_OCDIRECTION,(Int16*)&(pScdChPrm->direction),1);
                 if(libStatus != DMVAL_NO_ERROR)
                 {
                     Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_OCDIRECTION\n",__func__,libStatus);
                 }
              }
              if((pScdChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_TRIP) && (algoUpdate || (pScdChPrm->direction != params->direction)))
              {
                 Vps_printf(" %d: SCD   : VA TZ Direction has changed from %d to New value %d\n", Utils_getCurTimeInMsec(), pScdChPrm->direction, params->direction);
                 pScdChPrm->direction = params->direction;
                 libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_TZDIRECTION,(Int16*)&(pScdChPrm->direction),1);
                 if(libStatus != DMVAL_NO_ERROR)
                 {
                   Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TZDIRECTION\n",__func__,libStatus);
                 }

              }
              /********************************************************************/

              /* Setting Min Person Size */
              if(algoUpdate || ((pScdChPrm->minPersonSize[0] != params->minPersonSize.width) ||
                   (pScdChPrm->minPersonSize[1] != params->minPersonSize.height)))
              {
                 Vps_printf(" %d: SCD   : VA MinPerson Size has changed From %dx%d to New value %dx%d\n", 
                           Utils_getCurTimeInMsec(), pScdChPrm->minPersonSize[0], pScdChPrm->minPersonSize[1],
                                                        params->minPersonSize.width, params->minPersonSize.height);
                 pScdChPrm->minPersonSize[0] = params->minPersonSize.width;
                 pScdChPrm->minPersonSize[1] = params->minPersonSize.height;
                 libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_MINPERSONSIZE,pScdChPrm->minPersonSize,2);
                 if(libStatus != DMVAL_NO_ERROR)
                 {
                     Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_MINPERSONSIZE\n",__func__,libStatus);
                 }
              }
              /********************************************************************/

              /* Setting Max Person Size */
              if(algoUpdate || ((pScdChPrm->maxPersonSize[0] != params->maxPersonSize.width) ||
                   (pScdChPrm->maxPersonSize[1] != params->maxPersonSize.height)))
              {
                 Vps_printf(" %d: SCD   : VA MaxPerson Size has changed From %dx%d to New value %dx%d\n", 
                           Utils_getCurTimeInMsec(), pScdChPrm->maxPersonSize[0], pScdChPrm->maxPersonSize[1],
                                                        params->maxPersonSize.width, params->maxPersonSize.height);

                 pScdChPrm->maxPersonSize[0] = params->maxPersonSize.width;
                 pScdChPrm->maxPersonSize[1] = params->maxPersonSize.height;
                 libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_MAXPERSONSIZE,pScdChPrm->maxPersonSize,2);
                 if(libStatus != DMVAL_NO_ERROR)
                 {
                     Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_MAXPERSONSIZE\n",__func__,libStatus);
                 }
              }
              /********************************************************************/

              /* Setting Min Vehicle Size */
              if(algoUpdate || ((pScdChPrm->minVehicleSize[0] != params->minVehicleSize.width) ||
                   (pScdChPrm->minVehicleSize[1] != params->minVehicleSize.height)))
              {
                 Vps_printf(" %d: SCD   : VA MaxVehicle Size has changed From %dx%d to New value %dx%d\n", 
                           Utils_getCurTimeInMsec(), pScdChPrm->minVehicleSize[0], pScdChPrm->minVehicleSize[1],
                                                        params->minVehicleSize.width, params->minVehicleSize.height);

                 pScdChPrm->minVehicleSize[0] = params->minVehicleSize.width;
                 pScdChPrm->minVehicleSize[1] = params->minVehicleSize.height;
                 libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_MINVEHICLESIZE,pScdChPrm->minVehicleSize,2);
                 if(libStatus != DMVAL_NO_ERROR)
                 {
                     Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_MINVEHICLESIZE\n",__func__,libStatus);
                 }
              }
              /********************************************************************/

              /* Setting Max Vehicle Size */
              if(algoUpdate || ((pScdChPrm->maxVehicleSize[0] != params->maxVehicleSize.width) ||
                   (pScdChPrm->maxVehicleSize[1] != params->maxVehicleSize.height)))
              {
                 Vps_printf(" %d: SCD   : VA MaxVehicle Size has changed From %dx%d to New value %dx%d\n", 
                           Utils_getCurTimeInMsec(), pScdChPrm->maxVehicleSize[0], pScdChPrm->maxVehicleSize[1],
                                                        params->maxVehicleSize.width, params->maxVehicleSize.height);

                 pScdChPrm->maxVehicleSize[0] = params->maxVehicleSize.width;
                 pScdChPrm->maxVehicleSize[1] = params->maxVehicleSize.height;
                 libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_MAXVEHICLESIZE,pScdChPrm->maxVehicleSize,2);
                 if(libStatus != DMVAL_NO_ERROR)
                 {
                     Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_MAXVEHICLESIZE\n",__func__,libStatus);
                 }
              }
              /********************************************************************/

              /* Setting Polygon/Zone Params */
              pScdChPrm->numPolygons = params->numZone;

              if(pScdChPrm->chDMVLParam.detectMode)
              {
                  for(i = 0; i < pScdChPrm->numPolygons; i++)
                  {
                     UInt32 loopCnt;
                     pScdChPrm->polygon[i].size      = sizeof(DMVALpolygon);
                     pScdChPrm->polygon[i].polygonID = params->zoneCfg[i].zoneId;
                     pScdChPrm->polygon[i].valid     = params->zoneCfg[i].activeZone;
                     pScdChPrm->polygon[i].type      = params->zoneCfg[i].zoneType;
                     pScdChPrm->polygon[i].numPoints = params->zoneCfg[i].noOfPoints;
                     pScdChPrm->polygon[i].dir       = params->zoneCfg[i].zoneDirection;

                     for(loopCnt = 0; loopCnt < pScdChPrm->polygon[i].numPoints; loopCnt++)
                     {
                         pScdChPrm->polygon[i].pt[loopCnt].x = params->zoneCfg[i].pixLocation[loopCnt].x;
                         pScdChPrm->polygon[i].pt[loopCnt].y = params->zoneCfg[i].pixLocation[loopCnt].y;
                     }
                     if(pScdChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_TRIP)
                     {
                        Vps_printf(" %d: SCD   : VA Create Call Setting TRIP Zone Config!!!\n", 
                              Utils_getCurTimeInMsec());

                        pScdChPrm->polygon[0].type = DMVAL_TZ_1A;
                        pScdChPrm->polygon[1].type = DMVAL_TZ_1B;
                     }
                     else if(pScdChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_COUNT)
                     {
                        Vps_printf(" %d: SCD   : VA Create Call Setting OC Config!!!\n", 
                              Utils_getCurTimeInMsec());
                        pScdChPrm->polygon[0].type = DMVAL_OC_1A;
                        pScdChPrm->polygon[1].type = DMVAL_OC_1B;
                     }
                     else
                     {
                        Vps_printf(" %d: SCD   : VA Create Call Setting IMD  Config!!!\n", 
                              Utils_getCurTimeInMsec());

                        pScdChPrm->polygon[0].type = DMVAL_IMD;
                        pScdChPrm->polygon[1].type = DMVAL_IMD;
                     }          

                     libStatus = DMVAL_setROI(pDmvaHndl,&(pScdChPrm->polygon[i]));
                     if(libStatus != DMVAL_NO_ERROR)
                     {
                        Vps_printf("WARNING:%s:Error 0x%x while setting ROI-%d\n",__func__, libStatus, i);
                     }
                  }
               }
           }/* Param Setting of Non-Tamper modes*/
          /********************************************************************/
          pScdChPrm->polygonRtUpdated = TRUE;
          status = FVID2_SOK;
          break;
        }
    }
    if(status == FVID2_EFAIL)
        Vps_printf(" %d: SCD  : VA ALG VA Param Update: Invalid Channel No. - %d!!!\n", \
                                     Utils_getCurTimeInMsec(), params->chId);

    return status;
}
Int32 AlgLink_ScdalgSetChScdSensitivity(AlgLink_ScdObj * pObj,
                            AlgLink_ScdChParams * params)
{
    AlgLink_ScdChPrm *pScdChPrm;
    Int32 channelUpdated, idx;
    UInt32 sensitivity;
    UInt32 libStatus;
    DMVALhandle *pDmvaHndl;

    channelUpdated = FVID2_EFAIL;
    // Search through ID list to get the proper channel no.
    for(idx = 0; idx < pObj->createArgs.numValidChForSCD; idx++)
    {
      pScdChPrm = &pObj->chParams[idx];
      if(params->chId == pScdChPrm->chId)
      {
         /* VA Tamper Sensitivity update */
        pDmvaHndl   = &pObj->dmvalHndl[idx];
        sensitivity = params->frmSensitivity;

        if(sensitivity > DMVAL_SENSITIVITY_LEVEL_8)
            pScdChPrm->sensitiveness = DMVAL_SENSITIVITY_LEVEL_8;
        else if(sensitivity < DMVAL_SENSITIVITY_LEVEL_1)
           pScdChPrm->sensitiveness = DMVAL_SENSITIVITY_LEVEL_1;
        else
           pScdChPrm->sensitiveness = (DMVALsensitivity) sensitivity;

        libStatus = DMVAL_setParameter(pDmvaHndl,DMVAL_PARAM_TAMPERSENSITIVITY,(Int16*)&(pScdChPrm->sensitiveness),1);
        if(libStatus != DMVAL_NO_ERROR)
        {
          Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPERSENSITIVITY\n",__func__,libStatus);
        }

         /* SCD LMD Sensitivity update */
        pScdChPrm->frmSensitivity  = (SCD_Sensitivity) params->frmSensitivity;

        channelUpdated = FVID2_SOK;
        break;
      }
    }

    return (channelUpdated);
}

Int32 AlgLink_ScdalgSetChScdMode(AlgLink_ScdObj * pObj,
                            AlgLink_ScdChParams * params)
{
    AlgLink_ScdChPrm *pScdChPrm;
    Int32 channelUpdated, idx;
    UInt32 libStatus;

    channelUpdated = FVID2_EFAIL;
    // Search through ID list to get the proper channel no.
    for(idx = 0; idx < pObj->createArgs.numValidChForSCD; idx++)
    {
      pScdChPrm = &pObj->chParams[idx];
      if(params->chId == pScdChPrm->chId)
      {
         /* SCD LMD mode update */
        pScdChPrm->mode       = (SCD_Mode) (params->mode & 0x2);

         /* SCD Tamper mode update */
         /* #####                                    Keep all other modes as is ### Updating only tamper mode */
        if((pScdChPrm->detectMode & 0x1) != (params->mode & 0x1))
        {
           pScdChPrm->detectMode = (pScdChPrm->detectMode & 0x3C) | (params->mode & 0x1);
           pScdChPrm->chDMVLParam.detectMode =  (DMVALdetectMode)(((pScdChPrm->detectMode & 0x3C) >> 1) | (pScdChPrm->detectMode & 0x1));
           libStatus = DMVAL_initModule(&pObj->dmvalHndl[idx], pScdChPrm->chDMVLParam.detectMode);
           UTILS_assert(libStatus == DMVAL_NO_ERROR);
        }
        channelUpdated = FVID2_SOK;
        break;
      }
    }

    return (channelUpdated);
}

Int32 AlgLink_ScdalgSetChScdIgnoreLightsOn(AlgLink_ScdObj * pObj,
                            AlgLink_ScdChParams * params)
{
    AlgLink_ScdChPrm *pScdChPrm;
    Int32 channelUpdated, idx;

    channelUpdated = FVID2_EFAIL;
    // Search through ID list to get the proper channel no.
    for(idx = 0; idx < pObj->createArgs.numValidChForSCD; idx++)
    {
      pScdChPrm = &pObj->chParams[idx];
      if(params->chId == pScdChPrm->chId)
      {
        pScdChPrm->frmIgnoreLightsON = params->frmIgnoreLightsON;
        channelUpdated = FVID2_SOK;
        break;
      }
    }

    return (channelUpdated);
}

Int32 AlgLink_ScdalgSetChScdIgnoreLightsOff(AlgLink_ScdObj * pObj,
                            AlgLink_ScdChParams * params)
{
    AlgLink_ScdChPrm *pScdChPrm;
    Int32 channelUpdated, idx;

    channelUpdated = FVID2_EFAIL;
    // Search through ID list to get the proper channel no.
    for(idx = 0; idx < pObj->createArgs.numValidChForSCD; idx++)
    {
      pScdChPrm = &pObj->chParams[idx];
      if(params->chId == pScdChPrm->chId)
      {
        pScdChPrm->frmIgnoreLightsOFF = params->frmIgnoreLightsOFF;
        channelUpdated = FVID2_SOK;
        break;
      }
    }
    return (channelUpdated);
}

static Int32 AlgLink_ScdalgSetChScdPrm(AlgLink_ScdChPrm    *pScdChPrm,
                            AlgLink_ScdChParams * params)
{
    Int32 blkIdx;

    pScdChPrm->chId               = params->chId;
    pScdChPrm->chBlkConfigUpdate  = FALSE;

    pScdChPrm->mode               = (SCD_Mode) params->mode;
    pScdChPrm->frmSensitivity     = (SCD_Sensitivity) params->frmSensitivity;
    pScdChPrm->frmIgnoreLightsOFF = params->frmIgnoreLightsOFF;
    pScdChPrm->frmIgnoreLightsON  = params->frmIgnoreLightsON;

    for(blkIdx = 0; blkIdx < ALG_LINK_SCD_MAX_BLOCKS_IN_FRAME; blkIdx++)
    {
        pScdChPrm->blkConfig[blkIdx].monitored   = params->blkConfig[blkIdx].monitored;
        pScdChPrm->blkConfig[blkIdx].sensitivity = (SCD_Sensitivity) params->blkConfig[blkIdx].sensitivity;
    }

    return 0;
}

Int32 AlgLink_ScdVAChReset(AlgLink_ScdObj * pObj, UInt32 chId)
{
    AlgLink_ScdChPrm    *pChPrm; 
    AlgLink_ScdChObj    *pChObj;
    UInt32 libStatus;
    DMVALhandle *pDmvaHndl;
    Int32       scdChId;
    UInt32      sensitivity, tamperBGRefreshInterval;

    for(scdChId = 0;  scdChId<pObj->createArgs.numValidChForSCD; scdChId++)
    {
        pChObj    = &pObj->chObj[scdChId];
        if(chId == pChObj->chId)
        {
            pChPrm    = &pObj->chParams[scdChId];
            pDmvaHndl = &pObj->dmvalHndl[scdChId];

            pChObj->frameTS.clock.tv_sec  = 0;
            pChObj->frameTS.clock.tv_usec = 0; 

            sensitivity = pObj->createArgs.chDefaultParams[scdChId].frmSensitivity;

            if(sensitivity > DMVAL_SENSITIVITY_LEVEL_8)
                pChPrm->sensitiveness = DMVAL_SENSITIVITY_LEVEL_8;
            else if(sensitivity < DMVAL_SENSITIVITY_LEVEL_1)
               pChPrm->sensitiveness = DMVAL_SENSITIVITY_LEVEL_1;
            else
               pChPrm->sensitiveness = (DMVALsensitivity) sensitivity;

            pChPrm->detectMode = (DMVALdetectMode) (pObj->createArgs.chDefaultParams[scdChId].mode & 0x1);

            pChPrm->tamperResetCnt  = ALG_LINK_TAMPER_RESET_COUNT_SCALER_CONSTANT;// * pChObj->frameSkipCtx.outputFrameRate; 

            memset(pDmvaHndl,0x00,sizeof(DMVALhandle));


            libStatus = DMVAL_initModule(pDmvaHndl, pChPrm->chDMVLParam.detectMode);
            UTILS_assert(libStatus == DMVAL_NO_ERROR);   

            libStatus = DMVAL_setParameter(
                               pDmvaHndl,DMVAL_PARAM_TAMPERSENSITIVITY,
                               (Int16*)&(pChPrm->sensitiveness),
                               1);
            if(libStatus != DMVAL_NO_ERROR)
            {
              Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPERSENSITIVITY\n",__func__,libStatus);
            }

            libStatus = DMVAL_setParameter(
                               pDmvaHndl,DMVAL_PARAM_SENSITIVITY,
                               (Int16*)&(pChPrm->sensitiveness),
                               1);
            if(libStatus != DMVAL_NO_ERROR)
            {
               Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_SENSITIVITY\n",__func__,libStatus);
            }

            libStatus = DMVAL_setParameter(
                               pDmvaHndl,DMVAL_PARAM_TAMPER_BCK_RESET_INTERVAL, //DMVAL_PARAM_TAMPERCOUNTER,
                               (Int16*)&(pChPrm->tamperResetCnt),
                               1);
            if(libStatus != DMVAL_NO_ERROR)
            {
               Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPER_BCK_RESET_INTERVAL\n",__func__,libStatus);
            }

            libStatus = DMVAL_setParameter(
                            pDmvaHndl,DMVAL_PARAM_TAMPER_BLOCKUP_TH, 
                               (Int16*)pChPrm->thresold2WaitB4FrmAlert,
                               1);
            if(libStatus != DMVAL_NO_ERROR)
            {
              Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPER_BLOCKUP_TH\n",__func__,libStatus);
            }

            libStatus = DMVAL_setParameter(
                               pDmvaHndl,DMVAL_PARAM_TAMPER_BLOCKDOWN_TH, 
                               (Int16*)&pChPrm->thresold2WaitAfterFrmAlert,
                               1);
            if(libStatus != DMVAL_NO_ERROR)
            {
              Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPER_BLOCKDOWN_TH\n",__func__,libStatus);
            }

            tamperBGRefreshInterval = (Int16)pChPrm->tamperBGRefreshInterval;// * pChObj->frameSkipCtx.outputFrameRate;
            libStatus = DMVAL_setParameter(
                               pDmvaHndl, DMVAL_PARAM_TAMPER_BACKGROUND_INTERVAL, //DMVAL_PARAM_TAMPER_BACKGROUND_FRM_INTERVAL, 
                               (Int16*)&tamperBGRefreshInterval, 
                               1);
            if(libStatus != DMVAL_NO_ERROR)
            {
              Vps_printf("WARNING:%s:Error 0x%x while setting DMVAL_PARAM_TAMPER_BACKGROUND_INTERVAL\n",__func__,libStatus);
            }

#ifdef SYSTEM_DEBUG_SCD
           Vps_printf(" %d: SCD   : VA ALG Reset Done of Channel - %d!!!\n", Utils_getCurTimeInMsec(),pChObj->chId);
#endif
           break;
       }
    }
    if(scdChId == pObj->createArgs.numValidChForSCD)
    {
#ifdef SYSTEM_DEBUG_SCD
        Vps_printf(" %d: SCD  : VA ALG Reset: Invalid Channel No. - %d!!!\n", Utils_getCurTimeInMsec(),chId);
#endif
    }
    return FVID2_SOK;
} 

Int32 AlgLink_ScdresetStatistics(AlgLink_ScdObj *pObj)
{
    UInt32 chId;
    AlgLink_ScdChObj *pChObj;
    UInt32 oldIntState;

    oldIntState = Hwi_disable();

    for (chId = 0; chId < pObj->createArgs.numValidChForSCD; chId++)
    {
        pChObj = &pObj->chObj[chId];

        pChObj->inFrameRecvCount = 0;
        pChObj->inFrameUserSkipCount    = 0;


        pChObj->inFrameProcessCount     = 0;
        pChObj->inFrameProcessTime      = 0;
        pChObj->inFrameProcessSkipCount = 0;

    }
    Hwi_restore(oldIntState);
    pObj->totalFrameCount = 0;

    pObj->statsStartTime = Utils_getCurTimeInMsec();



    return 0;
}


Int32 AlgLink_scdAlgGetAllChFrameStatus(AlgLink_ScdObj * pObj, AlgLink_ScdAllChFrameStatus *pPrm)
{
    AlgLink_ScdChObj *pChObj;
    AlgLink_ScdCreateParams *pCreateArgs;
    UInt32 chId;

    pCreateArgs = &pObj->createArgs;

    pPrm->numCh = pCreateArgs->numValidChForSCD;

    for(chId=0; chId<pCreateArgs->numValidChForSCD; chId++)
    {
       pPrm->chanFrameResult[chId].frmResult = (UInt32)ALG_LINK_SCD_DETECTOR_UNAVAILABLE;
    }

    for(chId=0; chId<pCreateArgs->numValidChForSCD; chId++)
    {
        pChObj = &pObj->chObj[chId];

        pPrm->chanFrameResult[chId].chId      = pChObj->chId;
        pPrm->chanFrameResult[chId].frmResult = pChObj->scdChStat;
    }

    return FVID2_SOK;

}

Int32 AlgLink_ScdalgGetChResetChannel(AlgLink_ScdObj * pObj,
                            AlgLink_ScdChCtrl * params)
{
    SCD_Status      chanStatus;

    chanStatus = SCD_TI_resetChannel(
                                    pObj->algHndl,
                                    params->chId);

    if(chanStatus != SCD_NO_ERROR)
    {
#ifdef SYSTEM_DEBUG_SCD
        Vps_printf(" %d: SCD    : ERROR: Alg Process (chanID = %d) !!!\n",
                  Utils_getCurTimeInMsec(), params->chId );
#endif

        return FVID2_EFAIL;
    }
    else
    {
        Vps_printf(" %d: SCD    : chanID = %d Reset Done !!!\n",
                  Utils_getCurTimeInMsec(), params->chId);
    }

    AlgLink_ScdVAChReset(pObj, params->chId);

    return 0;
}

Int32 AlgLink_ScdPrintVaChConfig(AlgLink_ScdObj *pObj)
{
    UInt32 i, j;
    UInt32 chIdx;
    AlgLink_ScdChPrm *pChPrm;

    for(chIdx = 0; chIdx < pObj->createArgs.numValidChForSCD; chIdx++)
    {
      Bool nonTamperAlgo;
      pChPrm = &pObj->chParams[chIdx];
      Vps_printf("\n %d: SCD   : ------------------------------ !!!\n", Utils_getCurTimeInMsec());
      Vps_printf(" %d: SCD   :      VA Parameters of Ch-%d    !!!\n", Utils_getCurTimeInMsec(),pChPrm->chId);
      Vps_printf(" %d: SCD   : ------------------------------ !!!\n", Utils_getCurTimeInMsec());
      Vps_printf(" %d: SCD   : detectMode                 = %d !!!\n", Utils_getCurTimeInMsec(),pChPrm->detectMode);
      nonTamperAlgo = FALSE;
      if(pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_TRIP)
      {
         nonTamperAlgo = TRUE;
         Vps_printf(" %d: SCD   : VA TRIP Zone  Mode Enabled!!!\n", 
               Utils_getCurTimeInMsec());
      }
      else if(pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_COUNT)
      {
         nonTamperAlgo = TRUE;
         Vps_printf(" %d: SCD   : VA OC Mode Enabled!!!\n", 
               Utils_getCurTimeInMsec());
      }
      else if(pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_IMD)
      {
         nonTamperAlgo = TRUE;
         Vps_printf(" %d: SCD   : VA IMD Mode Enabled!!!\n", 
               Utils_getCurTimeInMsec());
      }
      else if(pChPrm->chDMVLParam.detectMode & DMVAL_DETECTMODE_SMETA)
      {
         nonTamperAlgo = TRUE;
         Vps_printf(" %d: SCD   : VA Stream MetaData (SMD) Mode Enabled!!!\n", 
               Utils_getCurTimeInMsec());
      }      

      Vps_printf(" %d: SCD   : sensitiveness              = %d !!!\n", Utils_getCurTimeInMsec(),pChPrm->sensitiveness);
      Vps_printf(" %d: SCD   : Tamper Reset Interval      = %d !!!\n", Utils_getCurTimeInMsec(),pChPrm->tamperResetCnt);
      Vps_printf(" %d: SCD   : Tamper BG Refresh Interval = %d !!!\n", Utils_getCurTimeInMsec(),pChPrm->tamperBGRefreshInterval);
      if(nonTamperAlgo)
      {
          Vps_printf(" %d: SCD   : Detection Interval         = %d !!!\n", Utils_getCurTimeInMsec(),pChPrm->detectionRate);
          Vps_printf(" %d: SCD   : Min Person Size      = (%d,%d) !!!\n", Utils_getCurTimeInMsec(),
                     pChPrm->minPersonSize[0],pChPrm->minPersonSize[1]);
          Vps_printf(" %d: SCD   : Max Person Size      = (%d,%d) !!!\n", Utils_getCurTimeInMsec(),
                     pChPrm->maxPersonSize[0],pChPrm->maxPersonSize[1]);
          Vps_printf(" %d: SCD   : Min Vehicle Size     = (%d,%d) !!!\n", Utils_getCurTimeInMsec(),
                     pChPrm->minVehicleSize[0],pChPrm->minVehicleSize[1]);
          Vps_printf(" %d: SCD   : Max Vehicle Size     = (%d,%d) !!!\n", Utils_getCurTimeInMsec(),
                     pChPrm->maxVehicleSize[0],pChPrm->maxVehicleSize[1]);
          Vps_printf(" %d: SCD   : VehiclesInFOV        = %d !!!\n", Utils_getCurTimeInMsec(),((pChPrm->objectInView & 0x2) == 0x2));
          Vps_printf(" %d: SCD   : PeopleInFOV          = %d !!!\n", Utils_getCurTimeInMsec(),((pChPrm->objectInView & 0x1) == 0x1));
          Vps_printf(" %d: SCD   : Direction            = %d !!!\n", Utils_getCurTimeInMsec(),pChPrm->direction);
          Vps_printf(" %d: SCD   : tzMaxSpatialOverlap  = %d !!!\n", Utils_getCurTimeInMsec(),pChPrm->tzMaxSpatialOverlap);
          Vps_printf(" %d: SCD   : tzMinTemporalOverlap = %d !!!\n", Utils_getCurTimeInMsec(),pChPrm->tzMinTemporalOverlap);

          Vps_printf(" %d: SCD   : numPolygons        = %d !!!\n", Utils_getCurTimeInMsec(),pChPrm->numPolygons);
          for(j = 0; j < pChPrm->numPolygons; j++)
          {
              Vps_printf(" %d: SCD   : ZONE-%d params      - Id = %d,valid = %d,type = %d,numPoints = %d !!!\n", Utils_getCurTimeInMsec(),
                         pChPrm->polygon[j].polygonID,pChPrm->polygon[j].valid,pChPrm->polygon[j].type,
                 j, pChPrm->polygon[j].numPoints);
              for(i = 0;i < pChPrm->polygon[j].numPoints;i++)
              {
                       Vps_printf(" %d: SCD   :  (%d,%d) !!!\n", Utils_getCurTimeInMsec(),
                           pChPrm->polygon[j].pt[i].x,pChPrm->polygon[j].pt[i].y);
              }
          }
      }
    }
    return 0;
}

Int32 AlgLink_ScdprintStatistics (AlgLink_ScdObj *pObj, Bool resetAfterPrint)
{
    UInt32 chId;
    AlgLink_ScdChObj *pChObj;
    UInt32 elaspedTime;

    elaspedTime = Utils_getCurTimeInMsec() - pObj->statsStartTime; // in msecs
    elaspedTime /= 1000; // convert to secs

    Vps_printf( " \n"
            " *** SCD Statistics *** \n"
            " \n"
            " Elasped Time           : %d secs\n"
            " Total Fields Processed : %d \n"
            " Total Fields FPS       : %d FPS\n"
            " \n"
            " \n"
            " CH  | In Recv In Process User Skip Process Skip In Process Time \n"
            " Num | FPS     FPS        FPS       FPS          per frame (msec)\n"
            " ----------------------------------------------------------------\n",
            elaspedTime,
            pObj->totalFrameCount,
            pObj->totalFrameCount / (elaspedTime)
                    );

    for (chId = 0; chId < pObj->createArgs.numValidChForSCD; chId++)
    {
        pChObj = &pObj->chObj[chId];

        Vps_printf( " %3d | %7d %10d %8d %11d %14d\n",
            pChObj->chId,
            pChObj->inFrameRecvCount/elaspedTime,
            pChObj->inFrameProcessCount/elaspedTime,
            pChObj->inFrameUserSkipCount/elaspedTime,
            pChObj->inFrameProcessSkipCount/elaspedTime,
            (pChObj->inFrameProcessTime  )/(pChObj->inFrameProcessCount)
            );
    }

    Vps_printf( " \n");

    if(resetAfterPrint)
    {
        AlgLink_ScdresetStatistics(pObj);
    }
    return FVID2_SOK;
}

Int32 AlgLink_ScdAlgRtPrmUpdate(AlgLink_ScdObj * pObj, AlgLink_ScdChPrm *pChPrm, FVID2_Frame *pFrame)
{
    System_FrameInfo *pFrameInfo;
    System_LinkChInfo *pChInfo;


    pFrameInfo = (System_FrameInfo *)pFrame->appData;

    if(pFrameInfo==NULL)
        return FVID2_EFAIL;

    if(pFrameInfo->rtChInfoUpdate==FALSE)
        return FVID2_SOK;

    pChInfo = &pFrameInfo->rtChInfo;

    if(pChInfo->width != pChPrm->width
        ||
       pChInfo->height != pChPrm->height
    )
    {
        pChPrm->rtPrmUpdate           = TRUE;
        pChPrm->algReset              = TRUE;
    }

    pChPrm->width                 = SystemUtils_floor(pChInfo->width, ALG_LINK_SCD_WIDTH_ALIGN);
    pChPrm->height                = pChInfo->height;
    pChPrm->stride                = pChInfo->pitch[0];

    if(pChPrm->width > pObj->createArgs.maxWidth)
    {
        Vps_printf(" %d: SCD  : Warning!!! RTParam Updated Width %d is Greater than maxWidth %d \n", 
                     Utils_getCurTimeInMsec(), pChPrm->width, pObj->createArgs.maxWidth);
        Vps_printf(" %d: SCD  : Warning!!! Limiting to maxWidth %d \n", 
                     Utils_getCurTimeInMsec(), pObj->createArgs.maxWidth);
        pChPrm->width = pObj->createArgs.maxWidth;
    }
    if(pChPrm->height > pObj->createArgs.maxHeight)
    {
        Vps_printf(" %d: SCD  : Warning!!! RTParam Updated Height %d is Greater than maxHeight %d \n", 
                     Utils_getCurTimeInMsec(), pChPrm->height, pObj->createArgs.maxHeight);
        Vps_printf(" %d: SCD  : Warning!!! Limiting to maxHeight %d \n", 
                     Utils_getCurTimeInMsec(), pObj->createArgs.maxHeight);
        pChPrm->height = pObj->createArgs.maxHeight;
    }
    return FVID2_SOK;
}


Int32 AlgLink_ScdVAProcessMetaData(AlgLink_ScdObj * pObj, UInt32 chIdx)
{
    AlgLink_ScdChObj *pChObj;
    AlgLink_ScdChPrm    *pChPrm; 
    DMVALout *pOutput;
    UInt32 numObj,i;

    pChObj = &pObj->chObj[chIdx]; 
    pChPrm = &pObj->chParams[chIdx];

    pOutput = &pChObj->output;

    if(pOutput->numObjects > pChPrm->maxNoObj)
    {
         numObj = pChPrm->maxNoObj;
    }
    else
    {
         numObj = pOutput->numObjects;
    }

    if(pOutput->modeResult & DMVAL_DETECTOR_TRIPZONE)
    {
        Vps_printf(" %d: SCD    : TZ Event Detected in Chan %d !!!\n", 
          Utils_getCurTimeInMsec(), chIdx);
    }
    if(pOutput->modeResult & DMVAL_DETECTOR_COUNTER)
    {
        Vps_printf(" %d: SCD    : OC Event Detected in Chan %d!!!\n", 
           Utils_getCurTimeInMsec(), chIdx);
    }
    if(pOutput->modeResult & DMVAL_DETECTOR_TAMPER)
    {
        Vps_printf(" %d: SCD    : CTD Event Detected in Chan %d !!!\n", 
            Utils_getCurTimeInMsec(), chIdx);
    }
    if(pOutput->modeResult & DMVAL_DETECTOR_IMD)
    {
        Vps_printf(" %d: SCD    : IMD Event Detected in Chan %d!!!\n", 
           Utils_getCurTimeInMsec(), chIdx);
    }
    if(numObj)
    {
        Vps_printf(" %d: SCD    :  %d Objects Detected in Chan %d!!!\n", \
           Utils_getCurTimeInMsec(), numObj);
    }
    for(i = 0; i < numObj; i++)
    {
        Vps_printf(" %d: SCD    : Object Id %d \n", 
                Utils_getCurTimeInMsec(), i);
        Vps_printf(" %d: SCD    : StartX is %d StartY is %d\n", 
                Utils_getCurTimeInMsec(),  pOutput->objMetadata[i].objBox.xmin, pOutput->objMetadata[i].objBox.ymin);
        Vps_printf(" %d: SCD    : Width is %d Height is %d\n", 
                Utils_getCurTimeInMsec(), pOutput->objMetadata[i].objBox.width, pOutput->objMetadata[i].objBox.height);
        Vps_printf(" %d: SCD    : Object Centroid is %d x %d\n", 
                Utils_getCurTimeInMsec(), pOutput->objMetadata[i].objCentroid.x, pOutput->objMetadata[i].objCentroid.y);
    }

    return numObj;
}
