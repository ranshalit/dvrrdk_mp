/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#ifndef _DEC_LINK_MPEG2_PRIV_H_
#define _DEC_LINK_MPEG2_PRIV_H_

#include <mcfw/interfaces/link_api/decLink.h>
#include <impeg2vdec.h>
#include "decLink_algIf.h"

typedef struct DecLink_MPEG2Obj {
    IMPEG2VDEC_Handle algHandle;
    Int8 versionInfo[DEC_LINK_MPEG2_VERSION_STRING_MAX_LEN];
    Int linkID;
    Int channelID;
    Int scratchID;
    UInt32 ivaChID;
    IMPEG2VDEC_DynamicParams dynamicParams;
    IMPEG2VDEC_Status status;
    IMPEG2VDEC_Params staticParams;
    IMPEG2VDEC_InArgs inArgs;
    IMPEG2VDEC_OutArgs outArgs;
    XDM2_BufDesc inBufs;
    XDM2_BufDesc outBufs;
    UInt32 memUsed[UTILS_MEM_MAXHEAPS];
} DecLink_MPEG2Obj;

Int DecLinkMPEG2_algCreate(DecLink_MPEG2Obj * hObj,
                           DecLink_AlgCreateParams * algCreateParams,
                           DecLink_AlgDynamicParams * algDynamicParams,
                           Int linkID, Int channelID, Int scratchGroupID,
                           FVID2_Format *pFormat, UInt32 numFrames,
                           IRES_ResourceDescriptor resDesc[]);
Void DecLinkMPEG2_algDelete(DecLink_MPEG2Obj * hObj);

#endif                                                     /* _DEC_LINK_MPEG2_PRIV_H_  */




