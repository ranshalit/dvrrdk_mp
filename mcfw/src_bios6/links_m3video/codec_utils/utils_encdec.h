/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#ifndef _UTILS_ENCDEC_H_
#define _UTILS_ENCDEC_H_

#include <ti/xdais/xdas.h>
#include <ti/xdais/dm/xdm.h>
#include <ti/xdais/dm/ivideo.h>
#include <mcfw/interfaces/link_api/encLink.h>
#include <mcfw/interfaces/link_api/decLink.h>
#include <mcfw/interfaces/link_api/systemLink_m3video.h>
#include <mcfw/src_bios6/utils/utils_trace.h>
#include <mcfw/src_bios6/links_common/system/system_priv_common.h>

/* =============================================================================
 * All success and failure codes for the module
 * ========================================================================== */

/** @brief Operation successful. */
#define UTILS_ENCDEC_S_SUCCESS               (0)

/** @brief General Failure */
#define UTILS_ENCDEC_E_FAIL                  (-1)

/** @brief Unknow coding type */
#define UTILS_ENCDEC_E_UNKNOWNCODINGTFORMAT  (-2)

/** @brief Internal error: unknown resolution class */
#define UTILS_ENCDEC_E_INT_UNKNOWNRESOLUTIONCLASS    (-64)

#define UTILS_ENCDEC_ACTIVITY_LOG_LENGTH (64)

#ifdef TI_816X_BUILD
#    define  NUM_HDVICP_RESOURCES                    (3)
#else
#    if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
#        define  NUM_HDVICP_RESOURCES                (1)
#    else
#        error "Unknow Device.."
#    endif
#endif

/** @brief Max array for buffer tracking */
#define   UTILS_MAX_BUFFER_TRACK_LIST       (SYSTEM_LINK_FRAMES_PER_CH * 3)

/** @enum EncDec_ResolutionClass
 *  @brief Enumeration of different resolution class.
 */
typedef enum EncDec_ResolutionClass {
    UTILS_ENCDEC_RESOLUTION_CLASS_FIRST = 0,
    UTILS_ENCDEC_RESOLUTION_CLASS_16MP = UTILS_ENCDEC_RESOLUTION_CLASS_FIRST,
    UTILS_ENCDEC_RESOLUTION_CLASS_9MP,
    UTILS_ENCDEC_RESOLUTION_CLASS_5MP,
    UTILS_ENCDEC_RESOLUTION_CLASS_4MP,
    UTILS_ENCDEC_RESOLUTION_CLASS_1080P,
    UTILS_ENCDEC_RESOLUTION_CLASS_720P,
    UTILS_ENCDEC_RESOLUTION_CLASS_D1,
    UTILS_ENCDEC_RESOLUTION_CLASS_CIF,
    UTILS_ENCDEC_RESOLUTION_CLASS_LAST = UTILS_ENCDEC_RESOLUTION_CLASS_CIF,
    UTILS_ENCDEC_RESOLUTION_CLASS_COUNT =
        (UTILS_ENCDEC_RESOLUTION_CLASS_LAST + 1)
} EncDec_ResolutionClass;


typedef enum EncDec_AlgorithmType {
    UTILS_ALGTYPE_NONE = 0,
    UTILS_ALGTYPE_H264_ENC,
    UTILS_ALGTYPE_H264_DEC,
    UTILS_ALGTYPE_MJPEG_ENC,
    UTILS_ALGTYPE_MJPEG_DEC,
    UTILS_ALGTYPE_MPEG4_DEC,
    UTILS_ALGTYPE_MPEG2_DEC,
    UTILS_ALGTYPE_LAST = UTILS_ALGTYPE_MPEG2_DEC,
    UTILS_ALGTYPE_COUNT =
        (UTILS_ALGTYPE_LAST + 1)
} EncDec_AlgorithmType;


typedef struct EncDec_AlgorithmActivityLog {
    EncDec_AlgorithmType algType[UTILS_ENCDEC_ACTIVITY_LOG_LENGTH];
    UInt32 writeIdx;
} EncDec_AlgorithmActivityLog;

#define UTILS_ENCDEC_RESOLUTION_CLASS_16MP_WIDTH                        (4*1024)
#define UTILS_ENCDEC_RESOLUTION_CLASS_16MP_HEIGHT                       (4*1024)

#define UTILS_ENCDEC_RESOLUTION_CLASS_9MP_WIDTH                         (3*1024)
#define UTILS_ENCDEC_RESOLUTION_CLASS_9MP_HEIGHT                        (3*1024)

#define UTILS_ENCDEC_RESOLUTION_CLASS_5MP_WIDTH                           (2592)
#define UTILS_ENCDEC_RESOLUTION_CLASS_5MP_HEIGHT                        (2*1024)

#define UTILS_ENCDEC_RESOLUTION_CLASS_4MP_WIDTH                         (2*1024)
#define UTILS_ENCDEC_RESOLUTION_CLASS_4MP_HEIGHT                        (2*1024)

#define UTILS_ENCDEC_RESOLUTION_CLASS_1080P_WIDTH                         (1920)
#define UTILS_ENCDEC_RESOLUTION_CLASS_1080P_HEIGHT                        (1080)

#define UTILS_ENCDEC_RESOLUTION_CLASS_720P_WIDTH                          (1280)
#define UTILS_ENCDEC_RESOLUTION_CLASS_720P_HEIGHT                          (720)

#define UTILS_ENCDEC_RESOLUTION_CLASS_D1_WIDTH                             (720)
#define UTILS_ENCDEC_RESOLUTION_CLASS_D1_HEIGHT                            (576)

#define UTILS_ENCDEC_RESOLUTION_CLASS_CIF_WIDTH                            (368)
#define UTILS_ENCDEC_RESOLUTION_CLASS_CIF_HEIGHT                           (288)


typedef Void (*Utils_encdecIVAMapChangeNotifyCb)(Ptr ctx,SystemVideo_Ivahd2ChMap_Tbl* tbl);

typedef struct Utils_encdecIVAMapChangeNotifyCbInfo {
    Utils_encdecIVAMapChangeNotifyCb       fxns;
    Ptr                                   ctx;
} Utils_encdecIVAMapChangeNotifyCbInfo;

typedef struct BuffersTrackInfo {
    UInt32      bufferId[UTILS_MAX_BUFFER_TRACK_LIST];
    UInt32      buffersCnt;
} Utils_encdecBuffersTrackInfo;


/**
    \brief Lock  one buffer

    \param pBufInfo        [IN] Buffer tracking structure
    \param buf              [IN] Buffer to add

    \return TRUE always
*/
static inline Int32 Utils_encdecLockBuffer(Utils_encdecBuffersTrackInfo *pBufInfo, UInt32 buf)
{
    if (pBufInfo->buffersCnt < UTILS_MAX_BUFFER_TRACK_LIST)
    {
        pBufInfo->bufferId[pBufInfo->buffersCnt] = buf;
        pBufInfo->buffersCnt++;    
    }
    return XDM_EOK;
}

/**
    \brief Unlock one buffer

    \param pBufInfo        [IN] Buffer tracking structure
    \param buf              [IN] Buffer to remove

    \return TRUE always
*/
static inline Int32 Utils_encdecUnlockBuffer(Utils_encdecBuffersTrackInfo *pBufInfo, UInt32 buf)
{
    Int32 i;
    
    UTILS_assert(pBufInfo->buffersCnt > 0);
    if (pBufInfo->buffersCnt)
    {
        for (i=0; i<pBufInfo->buffersCnt; i++)
        {
            if (pBufInfo->bufferId[i] == buf)
            {
                pBufInfo->buffersCnt--;
                break;
            }
        }
        if (i < pBufInfo->buffersCnt)
        {
            memmove(&pBufInfo->bufferId[i], &pBufInfo->bufferId[i+1], (sizeof(UInt32) * (pBufInfo->buffersCnt - i)));
        }
        else if (i > pBufInfo->buffersCnt)
        {
            Vps_printf ("ENC: Buffer Tracking failure.... buf %d not part of the track log\n",
                        buf);
        }
    }
    return XDM_EOK;
}

/**
    \brief Unlock all buffers

    \param pBufInfo        [IN] Buffer tracking structure

    \return TRUE always
*/
static inline Int32 Utils_encdecUnlockAllBuffers(Utils_encdecBuffersTrackInfo *pBufInfo)
{
    pBufInfo->buffersCnt = 0;
    return XDM_EOK;
}

/**
    \brief Check if coding type is H264

    This checks the IVIDEO format to see if it is H264 codec
    \param format        [IN] IVIDEO_Format enum

    \return TRUE if codec type is H264
*/
static inline Bool Utils_encdecIsH264(IVIDEO_Format format)
{
    Bool isH264;

    switch (format)
    {
        case IVIDEO_H264BP:
        case IVIDEO_H264MP:
        case IVIDEO_H264HP:
            isH264 = TRUE;
            break;
        default:
            isH264 = FALSE;
            break;
    }
    return isH264;
}
/**
    \brief Check if coding type is MPEG4

    This checks the IVIDEO format to see if it is MPEG4 codec
    \param format        [IN] IVIDEO_Format enum

    \return TRUE if codec type is MPEG4
*/
static inline Bool Utils_encdecIsMPEG4(IVIDEO_Format format)
{
    Bool isMPEG4;

    switch (format)
    {
        case IVIDEO_MPEG4SP:
        case IVIDEO_MPEG4ASP:
            isMPEG4 = TRUE;
            break;
        default:
            isMPEG4 = FALSE;
            break;
    }
    return isMPEG4;
}

/**
    \brief Check if coding type is MPEG2

    This checks the IVIDEO format to see if it is MPEG2 codec
    \param format        [IN] IVIDEO_Format enum

    \return TRUE if codec type is MPEG4
*/
static inline Bool Utils_encdecIsMPEG2(IVIDEO_Format format)
{
    Bool isMPEG2;

    switch (format)
    {
        case IVIDEO_MPEG2SP:
        case IVIDEO_MPEG2MP:
		case IVIDEO_MPEG2HP:
            isMPEG2 = TRUE;
            break;
        default:
            isMPEG2 = FALSE;
            break;
    }
    return isMPEG2;
}


/**
    \brief Check if coding type is MJPEG

    This checks the IVIDEO format to see if it is MJPEG codec
    \param format        [IN] IVIDEO_Format enum

    \return TRUE if codec type is MJPEG
*/
static inline Bool Utils_encdecIsJPEG(IVIDEO_Format format)
{
    Bool isjpeg;

    switch (format)
    {
        case IVIDEO_MJPEG:
            isjpeg = TRUE;
            break;
        default:
            isjpeg = FALSE;
            break;
    }
    return isjpeg;
}


static inline UInt32 Utils_encdecMapFVID2XDMContentType(UInt32 scanFormat)
{
    return ((scanFormat == FVID2_SF_INTERLACED) ? IVIDEO_INTERLACED :
            IVIDEO_PROGRESSIVE);
}

static inline UInt32 Utils_encdecMapFVID2XDMChromaFormat(UInt32 chromaFormat)
{
    UInt32 xdmChromaFormat;

    UTILS_assert((chromaFormat == FVID2_DF_YUV420SP_UV) ||
                 (chromaFormat == FVID2_DF_YUV420SP_VU) ||
                 (chromaFormat == FVID2_DF_YUV422I_YUYV) ||
                 (chromaFormat == FVID2_DF_YUV422I_UYVY));
    if ((chromaFormat == FVID2_DF_YUV420SP_UV)
        ||
        (chromaFormat == FVID2_DF_YUV420SP_VU))
    {
        xdmChromaFormat = XDM_YUV_420SP;
    }
    else
    {
        xdmChromaFormat = XDM_YUV_422IBE;
    }
    return xdmChromaFormat;
}

static inline UInt32 Utils_encdecMapXDMContentType2FVID2FID(UInt32 contentType)
{
    FVID2_Fid fid = FVID2_FID_MAX;

    switch (contentType)
    {
        case IVIDEO_INTERLACED_TOPFIELD:
            fid = FVID2_FID_TOP;
            break;
        case IVIDEO_INTERLACED_BOTTOMFIELD:
            fid = FVID2_FID_BOTTOM;
            break;
        default:
            // fid = FVID2_FID_FRAME;
            /* For progressive frame driver expects fid to be set to
             * FVID2_FID_TOP */
            fid = FVID2_FID_TOP;
            break;
    }
    return fid;
}

static inline IVIDEO_ContentType Utils_encdecMapFVID2FID2XDMContentType(FVID2_Fid fid)
{
    IVIDEO_ContentType contentType = IVIDEO_PROGRESSIVE;

    switch (fid)
    {
        case FVID2_FID_TOP:
            contentType = IVIDEO_INTERLACED_TOPFIELD;
            break;
        case FVID2_FID_BOTTOM:
            contentType = IVIDEO_INTERLACED_BOTTOMFIELD;
            break;
        case FVID2_FID_FRAME:
            contentType = IVIDEO_PROGRESSIVE_FRAME;
            break;
        default:
            contentType = IVIDEO_PROGRESSIVE_FRAME;
            break;
    }
    return contentType;
}

static UInt32 Enclink_h264GetFrameType (UInt32 frameType)
{
    UInt32 type = 0;

    switch (frameType)
    {
        case IVIDEO_I_FRAME:
        case IVIDEO_IDR_FRAME:
        case IVIDEO_II_FRAME:
        case IVIDEO_MBAFF_I_FRAME:
        case IVIDEO_MBAFF_IDR_FRAME:
            type = VCODEC_FRAME_TYPE_I_FRAME;
            break;

        case IVIDEO_IP_FRAME:
        case IVIDEO_PI_FRAME:
        case IVIDEO_P_FRAME:
        case IVIDEO_PP_FRAME:
        case IVIDEO_PB_FRAME:
            type = VCODEC_FRAME_TYPE_P_FRAME;
            break;

        case IVIDEO_IB_FRAME:
        case IVIDEO_BI_FRAME:
        case IVIDEO_B_FRAME:
        case IVIDEO_BB_FRAME:
        case IVIDEO_BP_FRAME:
            type = VCODEC_FRAME_TYPE_B_FRAME;
            break;
    }
    return type;
}


static inline Bool Utils_encdecIsGopStart(UInt32 frameType, UInt32 contentType)
{
    Bool isGopStart = FALSE;

    switch (frameType)
    {
        case IVIDEO_I_FRAME:
        case IVIDEO_IDR_FRAME:
        case IVIDEO_II_FRAME:
        case IVIDEO_MBAFF_I_FRAME:
        case IVIDEO_MBAFF_IDR_FRAME:
            isGopStart = TRUE;
            break;
        case IVIDEO_IP_FRAME:
        case IVIDEO_IB_FRAME:
            if (contentType == IVIDEO_INTERLACED_TOPFIELD)
            {
                isGopStart = TRUE;
            }
            break;
        case IVIDEO_PI_FRAME:
        case IVIDEO_BI_FRAME:
            if (contentType == IVIDEO_INTERLACED_BOTTOMFIELD)
            {
                isGopStart = TRUE;
            }
            break;
        default:
            isGopStart = FALSE;
            break;
    }
    return isGopStart;
}

/** @def   UTILS_ENCDEC_BITBUF_SCALING_FACTOR
 *  @brief Define that controls the size of bitbuf in realtion to its resoltuion
 */
#define UTILS_ENCDEC_BITBUF_SCALING_FACTOR                         (2)

/** @enum UTILS_ENCDEC_GET_BITBUF_SIZE
 *  @brief Macro that returns max size of encoded bitbuffer for a given resolution
 */

#define UTILS_ENCDEC_GET_BITBUF_SIZE(width,height,bitrate,framerate)          \
                    (((width) * (height))/2)

#define UTILS_ENCDEC_H264PADX                                              (32)
#define UTILS_ENCDEC_H264PADY                                              (24)

/** @enum UTILS_ENCDEC_GET_PADDED_WIDTH
 *  @brief Macro that padded width for given width */
#define UTILS_ENCDEC_GET_PADDED_WIDTH(width)                                   \
                  (((width) + (2 * UTILS_ENCDEC_H264PADX) + 127) & 0xFFFFFF80)

/** @enum UTILS_ENCDEC_GET_PADDED_HEIGHT
 *  @brief Macro that padded height for given height */
#define UTILS_ENCDEC_GET_PADDED_HEIGHT(height)                                 \
                                        ((height) + (4 * UTILS_ENCDEC_H264PADY))

Int Utils_encdecGetCodecLevel(UInt32 codingFormat,
                              UInt32 maxWidth,
                              UInt32 maxHeight,
                              UInt32 maxFrameRate,
                              UInt32 maxBitRate, Int32 * pLevel,
                              Bool isEnc);

Int Utils_encdecInit();

Int Utils_encdecDeInit();

Int Utils_encdecGetEncoderIVAID(UInt32 chId);

Int Utils_encdecGetDecoderIVAID(UInt32 chId);

Int Utils_encdecRegisterIVAMapChangeNotifyCb(Utils_encdecIVAMapChangeNotifyCbInfo *cbInfo);

Int Utils_encdecUnRegisterIVAMapChangeNotifyCb(Utils_encdecIVAMapChangeNotifyCbInfo *cbInfo);

Int Utils_encdecSetCh2IvahdMap(SystemVideo_Ivahd2ChMap_Tbl* Tbl);

Int Utils_encdecGetCh2IvahdMap(SystemVideo_Ivahd2ChMap_Tbl* tbl);

Void Utils_encdecInitGPIOForIVAVoltageScaling(
    UInt32             iva,
    UInt32             gpio_bank,
    UInt32             gpio_num,
    UInt32             gpio_value_for_iva_high_voltage,
    VSYS_IVA_VOLTAGE_E iva_init_voltage);

Void Utils_encdecSetGPIOForIVAVoltageScaling(UInt32 iva);

Void Utils_encdecSetIvaFreqVoltage(
        UInt32             iva,
        UInt32             freq,
        VSYS_IVA_VOLTAGE_E voltage);

Bool Utils_encdecGetIvaFreqVoltageReq(
        UInt32              iva,
        UInt32             *freq,
        Bool               *voltage_change,
        VSYS_IVA_VOLTAGE_E *voltage);

Void Utils_encdecResetIvaFreqVoltageReq(UInt32 iva);
#endif
