/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <mcfw/src_bios6/utils/utils_tiler.h>
#include <mcfw/src_bios6/utils/utils_tiler_allocator.h>
#include "utils_tiler_priv.h"
#include <mcfw/interfaces/link_api/system_debug.h>
#include <ti/sysbios/heaps/HeapMem.h>
#include "tiler_alg_binpack.h"

#ifdef  KB
#undef  KB
#endif

#ifdef  MB
#undef  MB
#endif

#define KB                                                             (1024u)
#define MB                                                             (KB*KB)

#define UTILS_TILER_DMM_PAT_VIEW_MAP_BASE_REGADDR    (0x4E000460u)
#define UTILS_TILER_DMM_PAT_VIEW_MAP__0_REGADDR      (0x4E000440u)
#define UTILS_TILER_DMM_PAT_VIEW_MAP__1_REGADDR      (0x4E000444u)
#define UTILS_TILER_DMM_PAT_VIEW_MAP__2_REGADDR      (0x4E000448u)
#define UTILS_TILER_DMM_PAT_VIEW_MAP__3_REGADDR      (0x4E00044Cu)

#define UTILS_TILER_DMM_PAT_VIEW_BASEPHYADDR         (0x80000000u)

#define UTILS_TILER_DMM_PAT_VIEW_MASK              (0x80000000u)
#define UTILS_TILER_DMM_PAT_VIEW_MAP_OFFSET_MASK   (0x78000000u)
#define UTILS_TILER_DMM_PAT_VIEW_MAP_OFFSET_SHIFT  (27u)
#define UTILS_TILER_DMM_PAT_TWO_NIBBLE_SHIFT       (8u)

/**************************************************************************\
* Register Overlay Structure
\**************************************************************************/

typedef struct {

    UInt32 enableLog;
    UInt32 initDone;
    UInt32 allocatorDisabled;
    HeapMem_Struct rawMemHeapObj;
    HeapMem_Handle rawMemHeap;

} Utils_TilerObj;

extern UInt8 *gUtils_tilerPhysMem;

Utils_TilerObj gUtils_tilerObj = {.initDone = FALSE ,
                                  #ifdef SYSTEM_USE_TILER
                                    .allocatorDisabled = FALSE
                                  #else
                                    .allocatorDisabled = TRUE
                                  #endif
                                  };

static Void  utils_tiler_create_non_tiled_heap(Utils_TilerObj *pObj,
                                               Ptr    heapAddress,
                                               UInt32 heapSize)
{
    HeapMem_Params params;

    UTILS_assert(pObj->rawMemHeap == NULL);
    HeapMem_Params_init(&params);
    params.buf  = heapAddress;
    params.size = heapSize;
    HeapMem_construct(&pObj->rawMemHeapObj,
                      &params);
    pObj->rawMemHeap = HeapMem_handle(&pObj->rawMemHeapObj);
    UTILS_assert(pObj->rawMemHeap != NULL);
}

static Void  utils_tiler_delete_non_tiled_heap(Utils_TilerObj *pObj)
{
    UTILS_assert(pObj->rawMemHeap ==
                 HeapMem_handle(&pObj->rawMemHeapObj));
    HeapMem_destruct(&pObj->rawMemHeapObj);
    pObj->rawMemHeap = NULL;
}

#ifdef SYSTEM_USE_TILER
int g_DbgPatViewMapVal = 0;
static Int32 utils_tiler_pat_init(Void)
{
    UInt32 v0, v1, v2, v3;
    UInt32 patViewMapVal;
    UInt32 tile8PhyAddr;
    UInt32 tile16PhyAddr;
    UInt32 tile32PhyAddr;
    UInt32 tilePGPhyAddr;
    Int32 status = 0;
    volatile UInt32 *patViewMapBaseReg =
        (volatile UInt32 *) UTILS_TILER_DMM_PAT_VIEW_MAP_BASE_REGADDR;
    volatile UInt32 *patViewMap0Reg =
        (volatile UInt32 *) UTILS_TILER_DMM_PAT_VIEW_MAP__0_REGADDR;
    volatile UInt32 *patViewMap1Reg =
        (volatile UInt32 *) UTILS_TILER_DMM_PAT_VIEW_MAP__1_REGADDR;
    volatile UInt32 *patViewMap2Reg =
        (volatile UInt32 *) UTILS_TILER_DMM_PAT_VIEW_MAP__2_REGADDR;
    volatile UInt32 *patViewMap3Reg =
        (volatile UInt32 *) UTILS_TILER_DMM_PAT_VIEW_MAP__3_REGADDR;

    *patViewMapBaseReg = UTILS_TILER_DMM_PAT_VIEW_BASEPHYADDR;

    if ((NULL != gUtils_tilerPhysMem)
        &&
        (UTILS_TILER_PHYS_MEM_SIZE != 0))
    {
        tile8PhyAddr = (UInt32) (&gUtils_tilerPhysMem[0]);
        /* Assert that physical memory is 12*MB aligned */
        UTILS_assert((tile8PhyAddr & ((128 * MB) - 1)) == 0);
        UTILS_assert(tile8PhyAddr >= UTILS_TILER_DMM_PAT_VIEW_BASEPHYADDR);
        /* Tiler containers physical size should be MB aligned */
        UTILS_assert((UTILS_TILER_CNT_8BIT_SIZE % MB) == 0);
         UTILS_assert((UTILS_TILER_CNT_16BIT_SIZE % MB) == 0);
        UTILS_assert((UTILS_TILER_CNT_32BIT_SIZE % MB) == 0);
        v0 = (tile8PhyAddr - UTILS_TILER_DMM_PAT_VIEW_BASEPHYADDR);
        v0 /= UTILS_TILER_CONTAINER_MAXSIZE;
        v0 &= 0xFF;

        if (UTILS_TILER_PHYS_MEM_SIZE >  (1 * UTILS_TILER_CONTAINER_MAXSIZE))
        {
            tile16PhyAddr = (UInt32)
            (&gUtils_tilerPhysMem[UTILS_TILER_CONTAINER_MAXSIZE]);
        }
        else
        {
            tile16PhyAddr = tile8PhyAddr;
        }


        UTILS_assert(tile16PhyAddr >= UTILS_TILER_DMM_PAT_VIEW_BASEPHYADDR);
        v1 = (tile16PhyAddr - UTILS_TILER_DMM_PAT_VIEW_BASEPHYADDR);
        v1 /= UTILS_TILER_CONTAINER_MAXSIZE;
        v1 &= 0xFF;

        if (UTILS_TILER_PHYS_MEM_SIZE >  (2 * UTILS_TILER_CONTAINER_MAXSIZE))
        {
            tile32PhyAddr = (UInt32)
                (&gUtils_tilerPhysMem[2 * UTILS_TILER_CONTAINER_MAXSIZE]);
        }
        else
        {
            tile32PhyAddr = tile16PhyAddr;
        }


        UTILS_assert(tile32PhyAddr >= UTILS_TILER_DMM_PAT_VIEW_BASEPHYADDR);
        v2 = (tile32PhyAddr - UTILS_TILER_DMM_PAT_VIEW_BASEPHYADDR);
        v2 /= UTILS_TILER_CONTAINER_MAXSIZE;
        v2 &= 0xFF;

        if (UTILS_TILER_PHYS_MEM_SIZE >  (3 * UTILS_TILER_CONTAINER_MAXSIZE))
        {
            tilePGPhyAddr = (UInt32)
                (&gUtils_tilerPhysMem[3 * UTILS_TILER_CONTAINER_MAXSIZE]);
        }
        else
        {
            tilePGPhyAddr = tile32PhyAddr;
        }

        UTILS_assert(tilePGPhyAddr >= UTILS_TILER_DMM_PAT_VIEW_BASEPHYADDR);
        v3 = (tilePGPhyAddr - UTILS_TILER_DMM_PAT_VIEW_BASEPHYADDR);
        v3 /= UTILS_TILER_CONTAINER_MAXSIZE;
        v3 &= 0xFF;

        patViewMapVal = (v0 | (v1 << 8) | (v2 << 16) | (v3 << 24));

        /* Store value for debug purpose */
        g_DbgPatViewMapVal = patViewMapVal;

        *patViewMap0Reg = patViewMapVal;
        *patViewMap1Reg = patViewMapVal;
        *patViewMap2Reg = patViewMapVal;
        *patViewMap3Reg = patViewMapVal;
        status = 0;
    }
    else
    {
        status = -1;
    }
    return status;
}
#endif

Int32 Utils_tilerAllocatorInit()
{

    if (gUtils_tilerObj.initDone == FALSE)
    {
        Int32 status = -1;

        #ifdef SYSTEM_USE_TILER
            status = utils_tiler_pat_init();
        #endif
        if (0 == status)
        {
            Utils_tilerAllocatorDebugLogEnable(FALSE);

            Utils_tilerAllocatorBinPackInit();

            Utils_tilerAllocatorFreeAll();
            gUtils_tilerObj.allocatorDisabled = FALSE;
        }
        else
        {
            gUtils_tilerObj.allocatorDisabled = TRUE;
            if (UTILS_TILER_PHYS_MEM_SIZE != 0)
            {
                utils_tiler_create_non_tiled_heap(&gUtils_tilerObj,
                                                       gUtils_tilerPhysMem,
                                                       UTILS_TILER_PHYS_MEM_SIZE);
            }
        }
        gUtils_tilerObj.initDone = TRUE;
    }
    return 0;
}

Int32 Utils_tilerAllocatorDeInit()
{

    Utils_tilerAllocatorBinPackDeInit();
    gUtils_tilerObj.initDone = FALSE;
    return 0;
}

Int32 Utils_tilerAllocatorDebugLogEnable(UInt32 enable)
{
    gUtils_tilerObj.enableLog = enable;

    return 0;
}




#ifdef SYSTEM_USE_TILER
UInt32 Utils_tilerAllocatorAlloc(UInt32 cntMode, UInt32 width, UInt32 height)
{
    UInt32 tilerAddr;

    if (gUtils_tilerObj.initDone == FALSE)
    {
        Utils_tilerAllocatorInit();
    }
    UTILS_assert(gUtils_tilerObj.initDone == TRUE);

    if (FALSE == gUtils_tilerObj.allocatorDisabled)
    {
        tilerAddr = Utils_tilerAllocatorBinPackAlloc(cntMode,width,height);
        UTILS_assert(tilerAddr != UTILS_TILER_INVALID_ADDR);
    }
    else
    {
        tilerAddr = UTILS_TILER_INVALID_ADDR;
    }
    return tilerAddr;
}
#else
UInt32 Utils_tilerAllocatorAlloc(UInt32 cntMode, UInt32 width, UInt32 height)
{
    (void)cntMode;
    (void)width;
    (void)height;

    return UTILS_TILER_INVALID_ADDR;
}

#endif


Int32 Utils_tilerAllocatorFreeAll()
{

    return Utils_tilerAllocatorBinPackFreeAll();
}

Int32 Utils_tilerAllocatorFree(UInt32 tileAddr)
{
    UTILS_assert(gUtils_tilerObj.initDone == TRUE);
    return Utils_tilerAllocatorBinPackFree(tileAddr);
}

Int32 Utils_tilerAllocatorDisable()
{
    Int32 status = 0;
    TilerAllocStats_t stats;

    UTILS_assert(gUtils_tilerObj.initDone == TRUE);
    Utils_tilerAllocatorBinPackGetStats(TILER_ALLOC_CONTAINER_INDEX_8BIT,&stats);
    if (stats.numUsedBuckets != 0)
    {
        status = -1;
    }
    Utils_tilerAllocatorBinPackGetStats(TILER_ALLOC_CONTAINER_INDEX_16BIT,&stats);
    if (stats.numUsedBuckets != 0)
    {
        status = -1;
    }

    if (0 == status)
    {
        gUtils_tilerObj.allocatorDisabled = TRUE;
        Utils_tilerAllocatorBinPackDeInit();
        utils_tiler_create_non_tiled_heap(&gUtils_tilerObj,
                                          gUtils_tilerPhysMem,
                                          UTILS_TILER_PHYS_MEM_SIZE);

    }
    return status;
}


Int32 Utils_tilerAllocatorEnable()
{
    UTILS_assert(gUtils_tilerObj.initDone == TRUE);
    if (TRUE == gUtils_tilerObj.allocatorDisabled)
    {
        gUtils_tilerObj.allocatorDisabled = FALSE;
        utils_tiler_delete_non_tiled_heap(&gUtils_tilerObj);
        Utils_tilerAllocatorBinPackInit();
        Utils_tilerAllocatorFreeAll();

    }
    return 0;
}

UInt32 Utils_tilerAllocatorIsDisabled()
{
    return gUtils_tilerObj.allocatorDisabled;

}

Ptr Utils_tilerAllocatorAllocRaw(UInt32 size, UInt32 align)
{
    Ptr addr = NULL;
    Error_Block ebObj;
    Error_Block *eb = &ebObj;

    if (TRUE == gUtils_tilerObj.allocatorDisabled)
    {
        Error_init(eb);

        /* allocate memory */
        UTILS_assert(gUtils_tilerObj.rawMemHeap != NULL);
        addr = Memory_alloc(HeapMem_Handle_upCast(gUtils_tilerObj.rawMemHeap),
                            size,
                            align,
                            eb);
    }
    return addr;
}

Void Utils_tilerAllocatorFreeRaw(Ptr addr,UInt32 size)
{
    Error_Block ebObj;
    Error_Block *eb = &ebObj;

    UTILS_assert(TRUE == gUtils_tilerObj.allocatorDisabled);
    Error_init(eb);

    /* allocate memory */
    UTILS_assert(gUtils_tilerObj.rawMemHeap != NULL);
    /* Check address to be freed falls in tiler physical memoru
     * address range
     */
    UTILS_assert((addr >= gUtils_tilerPhysMem)
                 &&
                 ((((UInt8 *)addr) + size)
                  <=
                  (gUtils_tilerPhysMem + UTILS_TILER_PHYS_MEM_SIZE)));
    Memory_free(HeapMem_Handle_upCast(gUtils_tilerObj.rawMemHeap), addr, size);
}

UInt32 Utils_tilerAllocatorGetFreeSizeRaw()
{
    UInt32 size = 0;
    Memory_Stats stats;

    if (TRUE == gUtils_tilerObj.allocatorDisabled)
    {
        Memory_getStats(HeapMem_Handle_upCast(gUtils_tilerObj.rawMemHeap), &stats);

        size = stats.totalFreeSize;
    }

    return ((UInt32) (size));
}


static
void utils_tiler_print_stats(UInt32 cntMode,TilerAllocStats_t *stats)
{
    Int i;
    char *TilerContainerIndex2NameStr[] =
    { "8BIT",
      "16BIT"
    };

    char *TilerResolution2NameStr[] =
    {
        "16MP",
        "9MP",
        "5MP",
        "4MP",
        "1080P",
        "720P",
        "D1",
        "CIF"
    };

    UTILS_assert(cntMode < UTILS_ARRAYSIZE(TilerContainerIndex2NameStr));
    Vps_printf("\n");
    Vps_printf("\r\nTILER_STATS: CNT :%s",TilerContainerIndex2NameStr[cntMode]);
    Vps_printf("\r\nTILER_STATS: CNT RESOLUTION:    %d x %d",stats->containerWidth,stats->containerHeight);
    Vps_printf("\r\nTILER_STATS: BUCKET RESOLUTION: %d x %d",stats->bucketWidth,stats->bucketHeight);
    Vps_printf("\r\nTILER_STATS: NUM FREE BUCKETS:  %d",stats->numFreeBuckets);
    Vps_printf("\r\nTILER_STATS: NUM USED BUCKETS:  %d",stats->numUsedBuckets);
    Vps_printf("\r\nTILER_STATS: TOTAL FREE AREA:   %d (%d %%)",stats->totalAreaFreeBuckets,((UInt64)stats->totalAreaFreeBuckets * 100)/(stats->containerWidth * stats->containerHeight));
    Vps_printf("\r\nTILER_STATS: TOTAL USED AREA:   %d (%d %%)",stats->totalAreaUsedBuckets,((UInt64)stats->totalAreaUsedBuckets * 100)/(stats->containerWidth * stats->containerHeight));
    if (stats->numUsedBuckets)
    {
        Vps_printf("\r\nTILER_STATS: TOTAL WASTE AREA:  %d (%d %%)",stats->totalAreaWasted,((UInt64)stats->totalAreaWasted  * 100)/(stats->containerWidth * stats->containerHeight));
        Vps_printf("\r\nTILER_STATS: MAX WIDTH RECT:    %d x %d",stats->maxRect[TILER_ALLOC_STATS_MAX_WIDTH_RECT_INDEX].width,stats->maxRect[TILER_ALLOC_STATS_MAX_WIDTH_RECT_INDEX].height);
        Vps_printf("\r\nTILER_STATS: MAX HEIGHT RECT:    %d x %d",stats->maxRect[TILER_ALLOC_STATS_MAX_HEIGHT_RECT_INDEX].width,stats->maxRect[TILER_ALLOC_STATS_MAX_HEIGHT_RECT_INDEX].height);
    }
    for (i = 0; i < UTILS_ARRAYSIZE(stats->resAllocCount);i++)
    {
        if (stats->resAllocCount[i])
        {
            UTILS_assert(i < UTILS_ARRAYSIZE(TilerResolution2NameStr));
            Vps_printf("\r\nTILER_STATS: RES ALLOC COUNT [%s] :  %d",TilerResolution2NameStr[i],stats->resAllocCount[i]);
        }
    }
    Vps_printf("\n");
}

UInt32 Utils_tilerAllocatorGetFreeSize(UInt32 cntMode)
{
    TilerAllocStats_t stats;

    stats.totalAreaFreeBuckets = 0;

    if (TRUE == gUtils_tilerObj.allocatorDisabled)
        return 0;

    if (cntMode < TILER_ALLOC_NUM_CONTAINERS)
    {
        Utils_tilerAllocatorBinPackGetStats(cntMode,&stats);
        utils_tiler_print_stats(cntMode,&stats);
    }
    return stats.totalAreaFreeBuckets;
}

void Utils_tilerAllocatorSetBucketGeometry(UInt32 bucketWidth,UInt32 bucketHeight,EncDec_ResolutionClass minRes)
{
    Utils_tilerAllocatorBinPackSetBucketGeometry(bucketWidth,bucketHeight,minRes);
}

void Utils_tilerAllocatorSetSingleBucketGeometry(EncDec_ResolutionClass minRes)
{
    Utils_tilerAllocatorBinPackSetSingleBucketGeometry(minRes);
}
