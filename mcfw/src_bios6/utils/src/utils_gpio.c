#include <mcfw/src_bios6/utils/utils_gpio.h>


#define GPIO0_BASE_ADDR (0x48032000)
#define GPIO1_BASE_ADDR (0x4804C000)

#define GPIO_OE         (0x134) // GPIO Output Enable
#define GPIO_DATAIN     (0x138) // GPIO Data Input
#define GPIO_DATAOUT    (0x13C) // GPIO Data Output



void Utils_gpioInit(
        UInt32 gpioBank, 
        UInt32 gpioNum, 
        Bool   isOutput, 
        UInt32 initValue)
{
    UInt32 addr;
    UInt32 base_addr = (GPIO_BANK_0 == gpioBank) ? GPIO0_BASE_ADDR : GPIO1_BASE_ADDR;

    UTILS_assert(gpioBank <= 1 && gpioNum < 32 && initValue <= 1);

    if (isOutput)
    { 
        addr = base_addr + GPIO_OE;
        *(volatile UInt32*)addr &= ~(1 << gpioNum); // set GPIO for output

        addr = base_addr + GPIO_DATAOUT;
        if (initValue)
        {
            *(volatile UInt32*)addr |= (1 << gpioNum);
        }
        else
        {
            *(volatile UInt32*)addr &= ~(1 << gpioNum);
        }
    }
    else
    {
        addr = base_addr + GPIO_OE;
        *(volatile UInt32*)addr |= (1 << gpioNum);    // set GPIO for input
    }
}



void Utils_gpioSet(UInt32 gpioBank, UInt32 gpioNum, UInt32 gpioVal)
{
    UInt32 addr;
    UInt32 base_addr = (GPIO_BANK_0 == gpioBank) ? GPIO0_BASE_ADDR : GPIO1_BASE_ADDR;
    
    UTILS_assert(gpioBank <= 1 && gpioNum < 32 && gpioVal <= 1);

    addr = base_addr + GPIO_OE;
    if ((*(volatile UInt32*)addr) & (1 << gpioNum))
    {
        Vps_printf(
                "GPIO%d[%d] is not set for output.\n
                Set GPIO for output by Utils_gpioInit() before calling %s\n", 
                gpioBank, gpioNum, __func__);
        UTILS_assert(0);
    }

    addr = base_addr + GPIO_DATAOUT;
    if (gpioVal)
    {
        *(volatile UInt32*)addr |= (1 << gpioNum);
    }
    else
    {
        *(volatile UInt32*)addr &= ~(1 << gpioNum);
    }
}



UInt32 Utils_gpioGet(UInt32 gpioBank, UInt32 gpioNum)
{
    UInt32 addr;
    UInt32 base_addr = (GPIO_BANK_0 == gpioBank) ? GPIO0_BASE_ADDR : GPIO1_BASE_ADDR;
    
    UTILS_assert(gpioBank <= 1 && gpioNum < 32);

    addr = base_addr + GPIO_OE;
    if (!((*(volatile UInt32*)addr) | (1 << gpioNum)))
    {
        Vps_printf(
                "GPIO%d[%d] is not set for input.\n
                Set GPIO for input by Utils_gpioInit() before calling %s\n", 
                gpioBank, gpioNum, __func__);
        UTILS_assert(0);
    }

    addr = base_addr + GPIO_DATAIN;
    return (*(volatile UInt32*)addr & (1 << gpioNum));
}


