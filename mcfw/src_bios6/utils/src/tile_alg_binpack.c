#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#ifdef _WIN32
#include "tile_alg_mcfw_stubs.h"
#else
#include <mcfw/src_bios6/utils/utils_tiler.h>
#include <mcfw/src_bios6/utils/utils_tiler_allocator.h>
#include <mcfw/src_bios6/links_m3video/codec_utils/utils_encdec.h>
#include <mcfw/src_bios6/utils/utils_que.h>
#include "utils_tiler_priv.h"
#endif
#include "tiler_alg_binpack.h"
#include "tiler_alg_binpack_priv.h"

#ifdef MIN
#undef MIN
#endif

#define MIN(a,b)                                                        ((a) < (b) ? (a) : (b))

/* Forward declaration */
static TileAllocConfig_t * tiler_alloc_cfg_get();
static
void  tiler_alloc_bucket_get_free_mem_stats (TileAllocBucket_t *bucket);
static 
void tiler_alloc_add_to_freednodes(TileAllocBucket_t *bucket,TileAllocNode_t *node);
static
void tiler_alloc_link_node_to_tree(TileAllocBucket_t *bucket,TileAllocNode_t *node);
static
Bool tiler_alloc_is_dummy_head_node(TileAllocNode_t *node);
static
void tiler_alloc_reset_bucket(TileAllocBucket_t *bucket,Bool forceFreeAll);
static
void tiler_alloc_free_bucket_resources(TileAllocBucket_t *bucket);


#define ENCDEC_RESCLASS_CREATE_INFO_TBL_ENTRY(resId) {UTILS_ENCDEC_RESOLUTION_CLASS_##resId,TILER_ALLOC_RESOLUTION_CLASS_##resId##_WIDTH,TILER_ALLOC_RESOLUTION_CLASS_##resId##_HEIGHT}
TilerAlloc_ResolutionClassInfo_s TilerAlloc_ResolutionClassInfo[UTILS_ENCDEC_RESOLUTION_CLASS_COUNT] = 
{
    ENCDEC_RESCLASS_CREATE_INFO_TBL_ENTRY(CIF),
    ENCDEC_RESCLASS_CREATE_INFO_TBL_ENTRY(D1),
    ENCDEC_RESCLASS_CREATE_INFO_TBL_ENTRY(720P),
    ENCDEC_RESCLASS_CREATE_INFO_TBL_ENTRY(1080P),
    ENCDEC_RESCLASS_CREATE_INFO_TBL_ENTRY(4MP),
    ENCDEC_RESCLASS_CREATE_INFO_TBL_ENTRY(5MP),
    ENCDEC_RESCLASS_CREATE_INFO_TBL_ENTRY(9MP),
    ENCDEC_RESCLASS_CREATE_INFO_TBL_ENTRY(16MP)
};


#define TILER_ALLOC_GET_CONTAINER_ADJUSTED_HEIGHT(cntMode,height) (((cntMode) == TILER_ALLOC_CONTAINER_INDEX_16BIT) ? ((height)/2) : (height))
#define TILER_ALLOC_IS_LEAF_NODE(node)          (((node->right == NULL) && (node->down == NULL)) ? TRUE : FALSE)
#define TILER_ALLOC_IS_SINGLE_CHILD_NODE(node) (((node->right == NULL) || (node->down == NULL)) ? TRUE : FALSE)

static
EncDec_ResolutionClass tiler_alloc_get_resolution_class(UInt32 width, UInt32 height,UInt32 cntMode)
{
    EncDec_ResolutionClass resClass = UTILS_ENCDEC_RESOLUTION_CLASS_16MP;
    Int i;
    UInt32 prevResWidth ,prevResHeight;
    

    UTILS_assert((width <= TILER_ALLOC_RESOLUTION_CLASS_MAX_WIDTH)
                 && (height <= TILER_ALLOC_GET_CONTAINER_ADJUSTED_HEIGHT(cntMode,TILER_ALLOC_RESOLUTION_CLASS_MAX_HEIGHT)));
    prevResWidth = prevResHeight = 0;
    for (i = 0; i < UTILS_ARRAYSIZE(TilerAlloc_ResolutionClassInfo);i++)
    {
        /* Ensure resClassInfo is sorted in ascending order */
        UTILS_assert((TilerAlloc_ResolutionClassInfo[i].width > prevResWidth)
                     &&
                     (TilerAlloc_ResolutionClassInfo[i].height > prevResHeight));
        if ((width <= TilerAlloc_ResolutionClassInfo[i].width) 
            &&
            (height <= TILER_ALLOC_GET_CONTAINER_ADJUSTED_HEIGHT(cntMode,TilerAlloc_ResolutionClassInfo[i].height)))
        {
            resClass = TilerAlloc_ResolutionClassInfo[i].resClass;
            break;
        }
        prevResWidth = TilerAlloc_ResolutionClassInfo[i].width;
        prevResHeight = TilerAlloc_ResolutionClassInfo[i].height;
    }
    return resClass;
}

static
Int tiler_alloc_get_resolution_class_info(UInt32 cntMode,
                                      EncDec_ResolutionClass resClass,
                                      UInt32 * pWidth, UInt32 * pHeight)
{
    Int i;
    Int status = 0;

    for (i = 0; i < UTILS_ARRAYSIZE(TilerAlloc_ResolutionClassInfo);i++)
    {
        if (resClass == TilerAlloc_ResolutionClassInfo[i].resClass)
        {
            *pWidth = TilerAlloc_ResolutionClassInfo[i].width;
            *pHeight = TILER_ALLOC_GET_CONTAINER_ADJUSTED_HEIGHT(cntMode,TilerAlloc_ResolutionClassInfo[i].height);
            break;
        }
    }
    if (i == UTILS_ARRAYSIZE(TilerAlloc_ResolutionClassInfo))
    {
        *pWidth = *pHeight = 0;
        status = UTILS_ENCDEC_E_INT_UNKNOWNRESOLUTIONCLASS;
        Vps_printf("Unknown resoltuion class:%d", resClass);
    }
    return status;
}




typedef Bool (*TilerAllocNodeTreeTraverseVisitFxn_t)(TileAllocNode_t *visitNode,Ptr visitPrms);


static
Utils_QueHandle * tiler_alloc_get_node_tree_queue(TileAllocNode_t *root)
{
    Int i,cntIndex;
    Utils_QueHandle *traverseQue = NULL;
    TileAllocConfig_t *cfg = tiler_alloc_cfg_get();

    for (cntIndex = 0; cntIndex < UTILS_ARRAYSIZE(cfg->TileAllocBucket);cntIndex++)
    {
        for (i = 0; i < UTILS_ARRAYSIZE(cfg->TileAllocBucket[cntIndex]);i++)
        {
            if (cfg->TileAllocBucket[cntIndex][i].head == root)
            {
                traverseQue = &cfg->TileAllocBucket[cntIndex][i].nodeTreeTraverseQ;
                break;
            }
        }
        if (i < UTILS_ARRAYSIZE(cfg->TileAllocBucket[cntIndex]))
        {
            break;     
        }
    }
    return traverseQue;
}

static
void tiler_alloc_validate_node_connection(TileAllocNode_t *node)
{
    UTILS_assert(node != NULL);
    if (node->parent)
    {
        UTILS_assert((node->parent->down == node) || (node->parent->right == node));
    }
    if (node->right)
    {
        UTILS_assert(node->right->parent == node);
    }
    if (node->down)
    {
        UTILS_assert(node->down->parent == node);
    }
}

static
void tiler_alloc_traverse_node_tree(TileAllocNode_t *root ,
                                    TilerAllocNodeTreeTraverseVisitFxn_t visitFxn,
                                    Ptr visitPrm)
{
    Int32 status;
    TileAllocNode_t *node;
    Bool abortTraversal = FALSE;
    Utils_QueHandle *traverseQue = tiler_alloc_get_node_tree_queue(root);


    UTILS_assert((traverseQue != NULL) 
                 && 
                 Utils_queIsEmpty(traverseQue) == TRUE);
    status = Utils_quePut(traverseQue,root,0);
    UTILS_assert(status == 0);
    while (!Utils_queIsEmpty(traverseQue))
    {
        status = Utils_queGet(traverseQue,(Ptr *)&node,1,0);
        UTILS_assert((status == 0) && (node != NULL));
        tiler_alloc_validate_node_connection(node);
        if (node != root)
        {
            UTILS_assert(tiler_alloc_is_dummy_head_node(node) == FALSE);
        }
        abortTraversal = visitFxn(node,visitPrm);
        if (abortTraversal)
        {
            break;
        }
        if (node->right != NULL)
        {
          status = Utils_quePut(traverseQue,node->right,0);
          UTILS_assert(status == 0);
        }
        if (node->down != NULL)
        {
          status = Utils_quePut(traverseQue,node->down,0);
          UTILS_assert(status == 0);
        }
    }
    if (abortTraversal)
    {
         /* Empty the traverse queue before exiting the traverse function */
         while (!Utils_queIsEmpty(traverseQue))
         {
             Utils_queGet(traverseQue,(Ptr *)&node,1,0);
         }
    }
    UTILS_assert(Utils_queIsEmpty(traverseQue) == TRUE);
}

static
void tiler_alloc_node_obj_tbl_init(TileAllocNodeTable_t *freeNodeTbl)
{
    Int i;

    freeNodeTbl->headIdx = 0;
    for (i = 0 ; i < UTILS_ARRAYSIZE(freeNodeTbl->entry) - 1;i++)
    {
        freeNodeTbl->entry[i].nextIdx = (i + 1);
    }
    freeNodeTbl->entry[UTILS_ARRAYSIZE(freeNodeTbl->entry) - 1].nextIdx = TILER_ALLOC_INVALID_IDX; 
    freeNodeTbl->numAllocNodes = 0;
    freeNodeTbl->initDone = TRUE;
}

static
void tiler_alloc_node_obj_tbl_deinit(TileAllocNodeTable_t *freeNodeTbl)
{
    UTILS_assert(freeNodeTbl->numAllocNodes == 0);
    freeNodeTbl->initDone = FALSE;

}

static
void tiler_alloc_init_node_obj(TileAllocNode_t *freeNode)
{
    memset(freeNode,0,sizeof(*freeNode));
    freeNode->isUsed = FALSE;
}

static
 TileAllocNodeTable_t * tiler_alloc_cfg_get_node_obj_tbl()
{
    TileAllocConfig_t *cfg = tiler_alloc_cfg_get();
    return &cfg->TileAllocNodeTable;
}


static
TileAllocNode_t * tiler_alloc_node_obj_alloc()
{
    TileAllocNode_t *freeNode = NULL;
    TileAllocNodeTable_t *freeNodeTbl = tiler_alloc_cfg_get_node_obj_tbl();

    UTILS_assert(freeNodeTbl->initDone == TRUE);
    if (freeNodeTbl->headIdx < UTILS_ARRAYSIZE(freeNodeTbl->entry))
    {
        freeNode = &freeNodeTbl->entry[freeNodeTbl->headIdx].node;
        tiler_alloc_init_node_obj(freeNode);
        freeNodeTbl->headIdx = freeNodeTbl->entry[freeNodeTbl->headIdx].nextIdx;
        freeNodeTbl->numAllocNodes++;
    }
    return freeNode;
}

static
void tiler_alloc_node_obj_free(TileAllocNode_t *freeNode)
{
    UInt32 freeNodeIdx = TILER_ALLOC_INVALID_IDX;
    struct TileAllocNodeTableEntry_t *freeNodeEntry;
    TileAllocNodeTable_t *freeNodeTbl = tiler_alloc_cfg_get_node_obj_tbl();

    UTILS_assert(freeNodeTbl->initDone == TRUE);
    UTILS_assert(freeNodeTbl != NULL);
    UTILS_COMPILETIME_ASSERT(offsetof(struct TileAllocNodeTableEntry_t,node) == 0);
    freeNodeEntry = (struct TileAllocNodeTableEntry_t *)freeNode;
    UTILS_assert(UTILS_ARRAYISVALIDENTRY(freeNodeEntry,freeNodeTbl->entry) == TRUE);
    freeNodeIdx = UTILS_ARRAYINDEX(freeNodeEntry,freeNodeTbl->entry);
    freeNodeEntry->nextIdx = freeNodeTbl->headIdx;
    freeNodeTbl->headIdx   = freeNodeIdx;
    UTILS_assert(freeNodeTbl->numAllocNodes > 0);
    freeNodeTbl->numAllocNodes--;
}

typedef struct TilerAllocFindNodeFxnPrm_s
{
    UInt32 width;
    UInt32 height;
    TileAllocNode_t *allocNode;
} TilerAllocFindNodeFxnPrm_s;

static
Bool tiler_alloc_find_free_node_in_tree_visit_fxn(TileAllocNode_t *root,void *prm)
{
    TilerAllocFindNodeFxnPrm_s *nodePrms = (TilerAllocFindNodeFxnPrm_s *)prm;

    if (!root->isUsed) 
    {
        if ((nodePrms->width <= root->width) && (nodePrms->height <= root->height))
        {
            if ((nodePrms->allocNode == NULL) || (nodePrms->allocNode->startY >= root->startY))
            {
                if ((nodePrms->allocNode == NULL) || (nodePrms->allocNode->startY > root->startY))
                {
                    nodePrms->allocNode = root;
                }
                else
                {
                    UTILS_assert(nodePrms->allocNode->startY == root->startY);
                    if (nodePrms->allocNode->startX < root->startX)
                    {
                        nodePrms->allocNode = root;
                    }
                }
            }
        }
    }
    return FALSE;
}

static
TileAllocNode_t * tiler_alloc_find_free_node_in_tree(TileAllocNode_t *root, UInt32 width, UInt32 height) 
{
    TilerAllocFindNodeFxnPrm_s findAllocNodePrms;

    findAllocNodePrms.width = width;
    findAllocNodePrms.height = height;
    findAllocNodePrms.allocNode = NULL;
    tiler_alloc_traverse_node_tree(root,&tiler_alloc_find_free_node_in_tree_visit_fxn,&findAllocNodePrms);
    return findAllocNodePrms.allocNode;
}

static
void tiler_alloc_set_node_geometry(TileAllocNode_t *node,UInt32 startX,UInt32 startY,UInt32 width, UInt32 height)
{
    node->startX = startX;
    node->startY = startY;
    node->width  = width;
    node->height = height;
}

static
Bool tiler_alloc_do_split_node(UInt32 width, UInt32 height,UInt32 minWidth,UInt32 minHeight)
{
    if ((width >= minWidth)
        &&
        (height >= minHeight))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }

}

static
void tiler_alloc_update_bucket_free_stats(TileAllocBucket_t *bucket,TileAllocNode_t *node)
{
    if (bucket->freeStats.maxWidthFreeNode == NULL)
    {
        bucket->freeStats.maxWidthFreeNode = node;
    }
    if (bucket->freeStats.maxHeightFreeNode == NULL)
    {
        bucket->freeStats.maxHeightFreeNode = node;
    }
    UTILS_assert(bucket->freeStats.maxHeightFreeNode != NULL);
    if (bucket->freeStats.maxHeightFreeNode->height < node->height)
    {
        bucket->freeStats.maxHeightFreeNode = node;
    }
    else
    {
        if (bucket->freeStats.maxHeightFreeNode->height == node->height)
        {
            if ((node->height * node->width) > 
                (bucket->freeStats.maxHeightFreeNode->height * bucket->freeStats.maxHeightFreeNode->width))
            {
                bucket->freeStats.maxHeightFreeNode = node;
            }

        } 
    }
    UTILS_assert(bucket->freeStats.maxWidthFreeNode != NULL);
    if (bucket->freeStats.maxWidthFreeNode->width < node->width)
    {
        bucket->freeStats.maxWidthFreeNode = node;
    }
    else
    {
        if (bucket->freeStats.maxWidthFreeNode->width == node->width)
        {
            if ((node->height * node->width) > 
                (bucket->freeStats.maxWidthFreeNode->height * bucket->freeStats.maxWidthFreeNode->width))
            {
                bucket->freeStats.maxWidthFreeNode = node;
            }

        } 
    }

}

static
TileAllocNode_t *  tiler_alloc_split_node (TileAllocBucket_t *bucket,TileAllocNode_t *node, UInt32 width, UInt32 height)
{
    EncDec_ResolutionClass resClass;
    UInt32 resClassWidth,resClassHeight;
    Int32 status = 0;
    UInt32 minWidth,minHeight;
    TileAllocConfig_t *tileAllocCfg;
    UInt32 rightNodeHeight;
    TileAllocNode_t *prevChild = NULL;
    Bool bottomSplit = FALSE;
    Bool sideSplit = FALSE;

    tileAllocCfg = tiler_alloc_cfg_get();
    tiler_alloc_get_resolution_class_info(bucket->cntMode,tileAllocCfg->minAllocResolution,&minWidth,&minHeight);
    UTILS_assert(tiler_alloc_is_dummy_head_node(node) == FALSE);
    if (height < minHeight)
    {
        height = minHeight;
    }
    UTILS_assert((node->width >= width) && (node->height >= height));
    UTILS_assert((bucket->bucketWidth >= node->width) && (bucket->bucketHeight >= node->height));
    UTILS_assert(node->isUsed == FALSE);
    node->isUsed = TRUE;
    bucket->numAllocatedNodes++;
    if (tiler_alloc_do_split_node(node->width,node->height - height,minWidth,minHeight))
    {
        prevChild = node->down;
        node->down  = tiler_alloc_node_obj_alloc();
        UTILS_assert(node->down != NULL);
        bucket->numNodesInTree++;
        node->down->parent = node;
        tiler_alloc_set_node_geometry(node->down, node->startX,     node->startY + height, node->width,     node->height - height );
        if (prevChild)
        {
            UTILS_assert((node->down->down == NULL) && (prevChild->parent == node));
            node->down->down = prevChild;
            prevChild->parent = node->down;
        }
        bottomSplit = TRUE;
    }
    if (bottomSplit)
    {
        rightNodeHeight = height;
    }
    else
    {
        rightNodeHeight = node->height;
    }
    if(tiler_alloc_do_split_node(node->width - width, rightNodeHeight,minWidth,minHeight))
    {
        prevChild = node->right;
        node->right = tiler_alloc_node_obj_alloc();
        UTILS_assert(node->right != NULL);
        bucket->numNodesInTree++;
        tiler_alloc_set_node_geometry( node->right, node->startX + width, node->startY, node->width - width, rightNodeHeight);
        node->right->parent = node;
        if (prevChild)
        {
            UTILS_assert((node->right->right == NULL) && (prevChild->parent == node));
            node->right->right = prevChild;
            prevChild->parent = node->right;
        }
        sideSplit = TRUE;
    }
    if (bottomSplit)
    {
        node->height -= node->down->height;
    }
    if (sideSplit)
    {
        node->width -= node->right->width;
    }
    resClass = tiler_alloc_get_resolution_class(width,height,bucket->cntMode);
    status = tiler_alloc_get_resolution_class_info(bucket->cntMode,resClass, &resClassWidth, &resClassHeight);
    UTILS_assert((status == 0) && (width <= node->width) && (height <= node->height));
    node->resClass = resClass;
    if (0 == bucket->resAllocCount[resClass]) /* First node allocated from this resolution */
    {
        /* New resolution class allocated in the bucket */
        UTILS_assert(bucket->numAllocResolutions < UTILS_ARRAYSIZE(bucket->allocResolution));
        bucket->allocResolution[bucket->numAllocResolutions] = resClass;
        bucket->numAllocResolutions++;
    }
    bucket->resAllocCount[resClass] += 1;
    if ((node == bucket->freeStats.maxHeightFreeNode) ||
        (node == bucket->freeStats.maxWidthFreeNode))
    {
        tiler_alloc_bucket_get_free_mem_stats(bucket);
    }
    else
    {
        bucket->freeStats.totalFreeArea -= node->width * node->height;
    }
    bucket->freeStats.totalWasteArea += ((node->width * node->height) - (width * height));
    return node;
}


typedef struct TilerAllocLocateNodeInBucketFxnPrm_s
{
    UInt32 startX;
    UInt32 startY;
    TileAllocNode_t *locatedNode;
} TilerAllocLocateNodeInBucketFxnPrm_s;

static
Bool tiler_alloc_locate_node_visit_fxn(TileAllocNode_t *root,void *prm)
{
    TilerAllocLocateNodeInBucketFxnPrm_s *nodePrms = (TilerAllocLocateNodeInBucketFxnPrm_s *)prm;
    Bool nodeFound = FALSE;

    if ((nodePrms->startX == root->startX) && (nodePrms->startY == root->startY))
    {
        nodeFound = TRUE;
        nodePrms->locatedNode = root;
    }
    return nodeFound;
}

static
TileAllocNode_t *  tiler_alloc_locate_node_in_bucket (TileAllocBucket_t *bucket,UInt32 startX,UInt32 startY) 
{
    TileAllocNode_t *head = bucket->head;
    TilerAllocLocateNodeInBucketFxnPrm_s locateNodePrms;

    UTILS_assert((startX >= bucket->bucketStartX) && (startY >= bucket->bucketStartY));
    locateNodePrms.startX = startX;
    locateNodePrms.startY = startY;
    locateNodePrms.locatedNode = NULL;
    tiler_alloc_traverse_node_tree(head,&tiler_alloc_locate_node_visit_fxn,&locateNodePrms);
    return locateNodePrms.locatedNode                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ;

}

static
Bool tiler_alloc_bucket_stats_visit_fxn(TileAllocNode_t *root,void *prm)
{
    TilerAllocBucketStatsFxnPrm_s *nodePrms = (TilerAllocBucketStatsFxnPrm_s *)prm;
    Bool abortTreeTraversal = FALSE;

    if(!root->isUsed)
    {
        if (nodePrms->maxHeightFreeNode == NULL)
        {
            nodePrms->maxHeightFreeNode = root;
        }
        if (nodePrms->maxWidthFreeNode == NULL)
        {
            nodePrms->maxWidthFreeNode = root;
        }
        UTILS_assert(nodePrms->maxHeightFreeNode != NULL);
        if (nodePrms->maxHeightFreeNode->height < root->height)
        {
            nodePrms->maxHeightFreeNode = root;
        }
        UTILS_assert(nodePrms->maxWidthFreeNode != NULL);
        if (nodePrms->maxWidthFreeNode->width < root->width)
        {
            nodePrms->maxWidthFreeNode = root;
        }
        nodePrms->totalFreeArea += root->width * root->height;
    }
    return abortTreeTraversal;
}

static
void  tiler_alloc_bucket_get_free_mem_stats (TileAllocBucket_t *bucket) 
{
    TileAllocNode_t *head = bucket->head;
    UInt i;

    bucket->freeStats.totalFreeArea = 0;
    bucket->freeStats.maxWidthFreeNode = NULL;
    bucket->freeStats.maxHeightFreeNode = NULL;
    tiler_alloc_traverse_node_tree(head,&tiler_alloc_bucket_stats_visit_fxn,&bucket->freeStats);
    for (i = 0; i < bucket->freedNodes.numNodes;i++)
    {
        tiler_alloc_update_bucket_free_stats(bucket,bucket->freedNodes.nodes[i]);
        bucket->freeStats.totalFreeArea += bucket->freedNodes.nodes[i]->width * bucket->freedNodes.nodes[i]->height;
    }
}

static
void tiler_alloc_init_dummy_head_node(TileAllocNode_t *dummyHead)
{
    dummyHead->isUsed = TRUE;
    dummyHead->width   = 0;
    dummyHead->height  = 0;
    dummyHead->startX  = ~0u;
    dummyHead->startY  = ~0u;
}

static
Bool tiler_alloc_is_dummy_head_node(TileAllocNode_t *node)
{
    if (((node->width == node->height) && (node->width == 0))
        &&
        ((node->startX == node->startY) && (node->startX == ~0u)))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static
void tiler_alloc_link_parent_child(TileAllocNode_t *node,TileAllocNode_t *child)
{
    if (node->parent->right == node)
    {                               
        node->parent->right = child;
    }                               
    else                            
    {                               
        UTILS_assert(node->parent->down == node);
        node->parent->down = child;
    }
    if (child)
    {         
        child->parent = node->parent;
    }
}

typedef struct TilerAllocTreeValidatePrm_s
{
    TileAllocNode_t *head;
    UInt32           numNodesInTree;
} TilerAllocTreeValidatePrm_s;

static
Bool tiler_alloc_validate_tree_visit_fxn(TileAllocNode_t *root,void *prm)
{
    TilerAllocTreeValidatePrm_s *validatePrm = (TilerAllocTreeValidatePrm_s *)prm;
    UTILS_assert(validatePrm != NULL); 

    if (tiler_alloc_is_dummy_head_node(root))
    {
        UTILS_assert(root->parent == NULL);
        UTILS_assert(validatePrm->head == root);
    }
    tiler_alloc_validate_node_connection(root);
    validatePrm->numNodesInTree++;
    return FALSE;
}


static
void tiler_alloc_unlink_node_from_tree(TileAllocBucket_t *bucket,TileAllocNode_t *node)
{
    TileAllocNode_t *nodeToReconnect;
    TileAllocNode_t *child;
 
    UTILS_assert(node != NULL);
    if (node != bucket->head)
    {
        TilerAllocTreeValidatePrm_s validatePrm;
        UInt32 preUnLinkCount;

        validatePrm.head = bucket->head;
        validatePrm.numNodesInTree = 0;
        tiler_alloc_traverse_node_tree(bucket->head,&tiler_alloc_validate_tree_visit_fxn,&validatePrm);
        preUnLinkCount = validatePrm.numNodesInTree;

        UTILS_assert(node->parent != NULL);
        UTILS_assert(tiler_alloc_is_dummy_head_node(node) == FALSE);
        UTILS_assert((tiler_alloc_is_dummy_head_node(node->parent) == FALSE) || (node->parent == bucket->head));

        if (TILER_ALLOC_IS_LEAF_NODE(node)) 
        {
            tiler_alloc_link_parent_child(node,NULL);
        }
        else
        {
            if (TILER_ALLOC_IS_SINGLE_CHILD_NODE(node))
            {
                child = (node->right != NULL) ?  node->right : node->down;
                tiler_alloc_link_parent_child(node,child);
            }
            else
            {
                UTILS_assert((node->right != NULL) && (node->down != NULL));
                if (node->parent->right == node)
                {
                    tiler_alloc_link_parent_child(node,node->right);
                    nodeToReconnect =  node->down;
                }
                else
                {
                    tiler_alloc_link_parent_child(node,node->down);
                    nodeToReconnect =  node->right;
                }
                tiler_alloc_link_node_to_tree(bucket,nodeToReconnect);
            }
        }
        UTILS_assert(bucket->numNodesInTree > 0);
        bucket->numNodesInTree--;
        validatePrm.head = bucket->head;
        validatePrm.numNodesInTree = 0;
        tiler_alloc_traverse_node_tree(bucket->head,&tiler_alloc_validate_tree_visit_fxn,&validatePrm);
        UTILS_assert(validatePrm.numNodesInTree == (preUnLinkCount - 1));
    }
    else
    {
        TileAllocNode_t *newHead;

        UTILS_assert(node->parent == NULL);
        newHead = tiler_alloc_node_obj_alloc();
        UTILS_assert(newHead != NULL);
        /* Copy node info */
        *newHead = *node;
        tiler_alloc_init_dummy_head_node(newHead);
        if (newHead->right)
        {
            newHead->right->parent = newHead;
        }
        if (newHead->down)
        {
            newHead->down->parent = newHead;
        }
        bucket->head = newHead;
    }
    node->down = NULL;
    node->right = NULL;
    node->parent = NULL; 
}

static
Bool tiler_alloc_locate_leafnode_visit_fxn(TileAllocNode_t *root,void *prm)
{
    Bool nodeFound = FALSE;

    if (TILER_ALLOC_IS_LEAF_NODE(root))
    {
        TileAllocNode_t **leafNodePtr = ( TileAllocNode_t **)prm;

        nodeFound = TRUE;
        *leafNodePtr = root;
    }
    return nodeFound;
}

static
void tiler_alloc_link_node_to_tree(TileAllocBucket_t *bucket,TileAllocNode_t *node)
{
    TileAllocNode_t *head = bucket->head;
    TileAllocNode_t *leafNode = NULL;

    tiler_alloc_traverse_node_tree(head,&tiler_alloc_locate_leafnode_visit_fxn,&leafNode);
    UTILS_assert((leafNode != NULL) && (TILER_ALLOC_IS_LEAF_NODE(leafNode)));
    leafNode->right = node;
    node->parent = leafNode;
}

static
void  tiler_alloc_free_node(TileAllocBucket_t *bucket,TileAllocNode_t *node)
{
    UInt32 i;

    node->isUsed = FALSE;
    tiler_alloc_unlink_node_from_tree(bucket,node);
    UTILS_assert(bucket->numAllocatedNodes > 0);
    bucket->numAllocatedNodes--;
    UTILS_assert(bucket->resAllocCount[node->resClass] > 0);
    bucket->resAllocCount[node->resClass] -= 1;
    if (bucket->resAllocCount[node->resClass] == 0)
    {
        for (i = 0; i < bucket->numAllocResolutions; i++)
        {

            if (bucket->allocResolution[i] == node->resClass)
            {
                break;
            }
        }
        UTILS_assert(bucket->numAllocResolutions > 0);
        for (/* i remains unchanged */; i < (bucket->numAllocResolutions - 1); i++)
        {
            bucket->allocResolution[i] = bucket->allocResolution[i + 1];
        }
        /* No more nodes of this resolution in this bucket */
        bucket->numAllocResolutions--;
    }
    tiler_alloc_update_bucket_free_stats(bucket,node);
    bucket->freeStats.totalFreeArea += node->width * node->height;
    tiler_alloc_add_to_freednodes(bucket,node);
}

static
void tiler_alloc_reset_freed_nodes(TilerAllocBucketFreedNodeTbl *freedNodes)
{
    UInt i;

    for (i = 0; i < freedNodes->numNodes; i++)
    {
        tiler_alloc_node_obj_free(freedNodes->nodes[i]);
    }
    freedNodes->numNodes = 0;
}

static 
void tiler_alloc_add_to_freednodes(TileAllocBucket_t *bucket,TileAllocNode_t *node)
{
    UTILS_assert(bucket->freedNodes.numNodes < UTILS_ARRAYSIZE(bucket->freedNodes.nodes));
    bucket->freedNodes.nodes[bucket->freedNodes.numNodes] = node;
    bucket->freedNodes.numNodes++;
}

static 
void tiler_alloc_remove_from_freednodes(TileAllocBucket_t *bucket,TileAllocNode_t *node)
{
    UInt32 i;

    UTILS_assert(bucket->freedNodes.numNodes < UTILS_ARRAYSIZE(bucket->freedNodes.nodes));
    for (i = 0; i < bucket->freedNodes.numNodes; i++)
    {
        if (bucket->freedNodes.nodes[i] == node)
        {
            break;
        }
    }
    UTILS_assert((i < bucket->freedNodes.numNodes) && (bucket->freedNodes.numNodes > 0));
    for (/* i remains unchanged */; i < bucket->freedNodes.numNodes; i++)
    {
        bucket->freedNodes.nodes[i] =  bucket->freedNodes.nodes[i + 1];
    }
    bucket->freedNodes.numNodes--;
}

static
TileAllocNode_t * tiler_alloc_find_node_in_freednodes(TileAllocBucket_t *bucket, UInt32 width, UInt32 height)
{
    UInt32 i;
    TileAllocNode_t *allocNode = NULL;

    UTILS_assert(bucket->freedNodes.numNodes < UTILS_ARRAYSIZE(bucket->freedNodes.nodes));
    for (i = 0; i < bucket->freedNodes.numNodes; i++)
    {
       if ((width <= bucket->freedNodes.nodes[i]->width)
           &&
           (height <= bucket->freedNodes.nodes[i]->height))
       {
           break;
       }
    }
    if (i < bucket->freedNodes.numNodes)
    {
        allocNode = bucket->freedNodes.nodes[i];
    }
    return allocNode;
}


static
TileAllocNode_t * tiler_alloc_frame(TileAllocBucket_t *bucket, UInt32 width, UInt32 height)
{
    TileAllocNode_t *allocNode;

    /* Find a node in the freednodes list */
    allocNode = tiler_alloc_find_node_in_freednodes(bucket, width, height);
    if (NULL != allocNode)
    {
        UTILS_assert(allocNode->isUsed == FALSE);
        tiler_alloc_remove_from_freednodes(bucket,allocNode);
        tiler_alloc_link_node_to_tree(bucket,allocNode);
        bucket->numNodesInTree++;
        allocNode = tiler_alloc_split_node(bucket,allocNode,width,height);
    }
    else
    {
        allocNode = tiler_alloc_find_free_node_in_tree(bucket->head,width,height);
        if (allocNode)
        {
            UTILS_assert(allocNode->isUsed == FALSE);
            allocNode = tiler_alloc_split_node(bucket,allocNode,width,height);

        }
   }
   return allocNode;
}

static
void tiler_alloc_merge_freed_nodes(TileAllocBucket_t *bucket)
{
    TileAllocNode_t *nodeToFree = NULL;
    Bool mergeDone = FALSE;
    size_t i,j;

    do 
    {
        mergeDone = FALSE;

        for(i = 0; i < bucket->freedNodes.numNodes; ++i)
        {
            for(j = i+1; j < bucket->freedNodes.numNodes; ++j)
            {
                if ((bucket->freedNodes.nodes[i]->width == bucket->freedNodes.nodes[j]->width) && 
                    (bucket->freedNodes.nodes[i]->startX == bucket->freedNodes.nodes[j]->startX))
                {
                    if (bucket->freedNodes.nodes[i]->startY == bucket->freedNodes.nodes[j]->startY + bucket->freedNodes.nodes[j]->height)
                    {
                        bucket->freedNodes.nodes[i]->startY -= bucket->freedNodes.nodes[j]->height;
                        bucket->freedNodes.nodes[i]->height += bucket->freedNodes.nodes[j]->height;
                        tiler_alloc_update_bucket_free_stats(bucket, bucket->freedNodes.nodes[i]);
                        nodeToFree = bucket->freedNodes.nodes[j];
                        tiler_alloc_remove_from_freednodes(bucket,nodeToFree);
                        tiler_alloc_node_obj_free(nodeToFree);
                        --j;
                        mergeDone = TRUE;
                    }
                    else if (bucket->freedNodes.nodes[i]->startY + bucket->freedNodes.nodes[i]->height == bucket->freedNodes.nodes[j]->startY)
                    {
                        bucket->freedNodes.nodes[i]->height += bucket->freedNodes.nodes[j]->height;
                        tiler_alloc_update_bucket_free_stats(bucket, bucket->freedNodes.nodes[i]);
                        nodeToFree = bucket->freedNodes.nodes[j];
                        tiler_alloc_remove_from_freednodes(bucket,nodeToFree);
                        tiler_alloc_node_obj_free(nodeToFree);
                        --j;
                        mergeDone = TRUE;
                    }
                }
                else if ((bucket->freedNodes.nodes[i]->height == bucket->freedNodes.nodes[j]->height) && (bucket->freedNodes.nodes[i]->startY == bucket->freedNodes.nodes[j]->startY))
                {
                    if (bucket->freedNodes.nodes[i]->startX == bucket->freedNodes.nodes[j]->startX + bucket->freedNodes.nodes[j]->width)
                    {
                        bucket->freedNodes.nodes[i]->startX -= bucket->freedNodes.nodes[j]->width;
                        bucket->freedNodes.nodes[i]->width += bucket->freedNodes.nodes[j]->width;
                        tiler_alloc_update_bucket_free_stats(bucket, bucket->freedNodes.nodes[i]);
                        nodeToFree = bucket->freedNodes.nodes[j];
                        tiler_alloc_remove_from_freednodes(bucket,nodeToFree);
                        tiler_alloc_node_obj_free(nodeToFree);
                        --j;
                        mergeDone = TRUE;
                    }
                    else 
                    if (bucket->freedNodes.nodes[i]->startX + bucket->freedNodes.nodes[i]->width == bucket->freedNodes.nodes[j]->startX)
                    {
                        bucket->freedNodes.nodes[i]->width += bucket->freedNodes.nodes[j]->width;
                        tiler_alloc_update_bucket_free_stats(bucket, bucket->freedNodes.nodes[i]);
                        nodeToFree = bucket->freedNodes.nodes[j];
                        tiler_alloc_remove_from_freednodes(bucket,nodeToFree);
                        tiler_alloc_node_obj_free(nodeToFree);
                        --j;
                        mergeDone = TRUE;
                    }
                }
            }
        }
    } while (mergeDone);
}

typedef TileAllocBucket_t (TileAllocBucketAry)[TILER_ALLOC_MAX_NUM_BUCKETS];

static
 TileAllocBucketAry * tiler_alloc_cfg_get_bucket_array(UInt32 cntMode)
{
    TileAllocConfig_t *cfg = tiler_alloc_cfg_get();
    UTILS_assert(cntMode < UTILS_ARRAYSIZE(cfg->TileAllocBucket));
    return &cfg->TileAllocBucket[cntMode];
}

static
void tiler_alloc_reset_bucket_freestats(TilerAllocBucketStatsFxnPrm_s *freeStats)
{
    freeStats->totalFreeArea = 0;
    freeStats->maxHeightFreeNode = NULL;
    freeStats->maxWidthFreeNode  = NULL;
    freeStats->totalWasteArea = 0;
}


static
void tiler_alloc_init_bucket(TileAllocBucket_t *bucket,UInt32 cntMode)
{
    Int32 status;

    memset(bucket,0,sizeof(*bucket));
    bucket->isValid  = FALSE;
    bucket->head = NULL;
    bucket->numAllocResolutions = 0;
    bucket->numAllocatedNodes   = 0;
    tiler_alloc_reset_bucket_freestats(&bucket->freeStats);
    bucket->freedNodes.numNodes = 0;
    bucket->cntMode = cntMode;
    bucket->numNodesInTree = 0;
    status = 
    Utils_queCreate(&bucket->nodeTreeTraverseQ,
                    UTILS_ARRAYSIZE(bucket->nodeTreeTraverseQMem),
                    bucket->nodeTreeTraverseQMem,0);
    UTILS_assert(status == 0);		
}

static
void tiler_alloc_init_bucket_geometry(TileAllocBucket_t *bucket,UInt32 startX, UInt32 startY,UInt32 width,UInt32 height)
{
    UTILS_assert(bucket->isValid == FALSE);
    UTILS_assert((startX & (TILER_ALLOC_WIDTH_ALIGN - 1)) == 0);
    UTILS_assert((startY & (TILER_ALLOC_HEIGHT_ALIGN - 1)) == 0);
    bucket->bucketStartX = startX;
    bucket->bucketStartY = startY;
    bucket->bucketWidth  = width;
    bucket->bucketHeight = height;
    UTILS_assert(bucket->head == NULL);
    bucket->head = tiler_alloc_node_obj_alloc();
    bucket->numNodesInTree++;
    UTILS_assert(bucket->numNodesInTree == 1);
    UTILS_assert(bucket->head != NULL);
    tiler_alloc_set_node_geometry(bucket->head,startX,startY,width,height);
    bucket->head->isUsed = FALSE;
    //bucket->head->resClass = tiler_alloc_get_resolution_class(bucket->head->width,bucket->head->height,bucket->cntMode);
    tiler_alloc_bucket_get_free_mem_stats(bucket);
    bucket->isValid  = TRUE;
}

static
void tiler_alloc_free_bucket_resources(TileAllocBucket_t *bucket)
{
    if (bucket->isValid)
    {
        UTILS_assert(bucket->numAllocatedNodes == 0);
        UTILS_assert(bucket->head != NULL);
        tiler_alloc_reset_bucket(bucket,FALSE);
        tiler_alloc_node_obj_free(bucket->head); 
        bucket->numNodesInTree--;
        UTILS_assert(bucket->numNodesInTree == 0);
        bucket->head = NULL;
        bucket->isValid = FALSE;
    }

}
static
void tiler_alloc_deinit_bucket(TileAllocBucket_t *bucket)
{
    Int32 status;

    tiler_alloc_free_bucket_resources(bucket);
    bucket->numAllocResolutions = 0;
    status = 
    Utils_queDelete(&bucket->nodeTreeTraverseQ);
    UTILS_assert(status == 0);		
    memset(bucket,0,sizeof(*bucket));
}

static
Bool tiler_alloc_bucket_is_geometry_change_allowed(UInt32 cntMode)
{
    TileAllocBucketAry * TileAllocBucket = tiler_alloc_cfg_get_bucket_array(cntMode);
    Int i;
    Bool changeAllowed = TRUE;

    for (i = 0; i < UTILS_ARRAYSIZE(*TileAllocBucket);i++)
    {
        if ((*TileAllocBucket)[i].isValid)
        {
            if (((*TileAllocBucket)[i].numAllocatedNodes != 0) || ((*TileAllocBucket)[i].freedNodes.numNodes != 0))
            {
                changeAllowed = FALSE;
                break;
            }
        }
    }
    return changeAllowed;
}

typedef struct TilerAllocFreeNodeFxnPrm_s 
{
    Bool forceFree;
    TileAllocBucket_t *bucket;
} TilerAllocFreeNodeFxnPrm_t;

static
Bool tiler_alloc_free_all_node_in_tree_visit_fxn(TileAllocNode_t *root,void *prm)
{
    TilerAllocFreeNodeFxnPrm_t *visitPrm = prm;
    TileAllocBucket_t *bucket = visitPrm->bucket;

    UTILS_assert((root->isUsed == FALSE) || tiler_alloc_is_dummy_head_node(root) || (visitPrm->forceFree));
    tiler_alloc_node_obj_free(root);
    bucket->numNodesInTree--;
    return FALSE;
}

static
void tiler_alloc_free_all_node_in_tree(TileAllocBucket_t *bucket,Bool forceFreeAll) 
{
    TilerAllocFreeNodeFxnPrm_t freeNodePrm;

    freeNodePrm.bucket = bucket;
    freeNodePrm.forceFree = forceFreeAll;
    tiler_alloc_traverse_node_tree(bucket->head,&tiler_alloc_free_all_node_in_tree_visit_fxn,&freeNodePrm);
}


static
void tiler_alloc_reset_bucket(TileAllocBucket_t *bucket,Bool forceFreeAll)
{
    if (forceFreeAll == TRUE)
    {
        bucket->numAllocatedNodes = 0;
        bucket->numAllocResolutions = 0;
        memset(bucket->resAllocCount,0,sizeof(bucket->resAllocCount));
        memset(bucket->allocResolution ,0,sizeof(bucket->allocResolution));
    }
    else
    {
        UTILS_assert((bucket->numAllocatedNodes == 0) && (bucket->numAllocResolutions == 0));
    }
    UTILS_assert(bucket->head != NULL);
    tiler_alloc_reset_freed_nodes(&bucket->freedNodes);
    tiler_alloc_reset_bucket_freestats(&bucket->freeStats);
    tiler_alloc_free_all_node_in_tree(bucket,forceFreeAll);
    bucket->head = NULL;
    UTILS_assert(bucket->numNodesInTree == 0);
    bucket->isValid = FALSE;
    tiler_alloc_init_bucket_geometry(bucket,bucket->bucketStartX, bucket->bucketStartY,bucket->bucketWidth,bucket->bucketHeight);
}

static
UInt32 tiler_alloc_get_partition_dimension(UInt32 totalDim,UInt32 minDim,UInt32 baseDim,UInt32 curStartPoint)
{
    UInt32 partion_dim;

    if (totalDim >= (curStartPoint + baseDim))
    {
        if (totalDim - (curStartPoint + baseDim) > minDim)
        {
            partion_dim = baseDim;
        }
        else
        {
            partion_dim = baseDim + (totalDim - (curStartPoint + baseDim));
        }
    }
    else
    {
        UTILS_assert(curStartPoint <= totalDim);
        partion_dim = totalDim - curStartPoint; 
        UTILS_assert(partion_dim >= minDim);
    }
    return partion_dim;
}

static 
void tiler_alloc_partition_tiler_container_into_buckets(UInt32 cntMode,UInt32 containerWidth,UInt32 containerHeight,UInt32 bucketWidth,UInt32 bucketHeight,UInt32 minWidth,UInt32 minHeight)
{
    UInt32 startY,startX;
    UInt32 bucketNum;
    UInt32 partitionWidth,partitionHeight;
    TileAllocBucketAry * TileAllocBucket = tiler_alloc_cfg_get_bucket_array(cntMode);

    UTILS_assert((bucketWidth <= containerWidth) && (bucketHeight <= containerHeight));
    UTILS_assert(tiler_alloc_bucket_is_geometry_change_allowed(cntMode) == TRUE);
    startY = 0;
    if (TILER_ALLOC_CONTAINER_INDEX_8BIT == cntMode)
    {
        /* Work around to ensure tilerAddr 0 is not allocated.THis is because HDPSS drivers
         * treat 0 as NULL buffer and dont handle this buffer correctly
         */
        startY = TILER_ALLOC_NULL_ADDRESS_WORKAROUND_8BIT_STARTY;
    }
    bucketNum = 0;
    while(startY < containerHeight)
    {
        startX = 0;
        partitionHeight = VpsUtils_align((tiler_alloc_get_partition_dimension(containerHeight,minHeight,bucketHeight,startY)),TILER_ALLOC_HEIGHT_ALIGN);
        while(startX < containerWidth)
        {
            partitionWidth =  VpsUtils_align((tiler_alloc_get_partition_dimension(containerWidth,minWidth,bucketWidth,startX)),TILER_ALLOC_WIDTH_ALIGN);
            UTILS_assert(bucketNum < UTILS_ARRAYSIZE(*TileAllocBucket));
            if ((*TileAllocBucket)[bucketNum].head != NULL)
            {
                tiler_alloc_free_all_node_in_tree(&((*TileAllocBucket)[bucketNum]),FALSE);
                (*TileAllocBucket)[bucketNum].head = NULL;
                UTILS_assert((*TileAllocBucket)[bucketNum].numNodesInTree == 0);
            }
            (*TileAllocBucket)[bucketNum].isValid = FALSE;
            tiler_alloc_init_bucket_geometry(&(*TileAllocBucket)[bucketNum],startX,startY,partitionWidth,partitionHeight);
            bucketNum++;
            startX += partitionWidth;
        }
        UTILS_assert(startX == containerWidth);
        startY += partitionHeight;
    }
    for( /* bucketNum remains unchanged */ ; bucketNum < UTILS_ARRAYSIZE(*TileAllocBucket); bucketNum++)
    {
        if ((*TileAllocBucket)[bucketNum].isValid)
        {
            tiler_alloc_free_bucket_resources(&((*TileAllocBucket)[bucketNum]));
        }
        (*TileAllocBucket)[bucketNum].isValid = FALSE;
    }
    UTILS_assert(startY == containerHeight);
}


static
TileAllocBucket_t * tiler_alloc_map_coordinates2bucket(UInt32 cntMode,UInt32 startX,UInt32 startY)
{
    Int i;
    TileAllocBucket_t *matchBucket = NULL;
    TileAllocBucketAry * TileAllocBucket = tiler_alloc_cfg_get_bucket_array(cntMode);

    for (i = 0; i < UTILS_ARRAYSIZE(*TileAllocBucket);i++)
    {
       if ((*TileAllocBucket)[i].isValid)
       {
           if ((startX >= (*TileAllocBucket)[i].bucketStartX)
               &&
               (startX <  ((*TileAllocBucket)[i].bucketStartX + (*TileAllocBucket)[i].bucketWidth))
               &&
               (startY >= (*TileAllocBucket)[i].bucketStartY)
               &&
               (startY <  ((*TileAllocBucket)[i].bucketStartY + (*TileAllocBucket)[i].bucketHeight)))
           {
               matchBucket = &(*TileAllocBucket)[i];
               break;
           }
       }
    }
    return matchBucket;
}

static
Bool  tiler_alloc_check_alloc_possible_in_bucket(TileAllocBucket_t *allocBucket ,
                                                 UInt32 allocWidth,
                                                 UInt32 allocHeight)
{
    Bool allocPossible = FALSE;

    if (allocBucket->freeStats.maxWidthFreeNode)
    {
        if ((allocBucket->freeStats.maxWidthFreeNode->width >= allocWidth)
            &&
            (allocBucket->freeStats.maxWidthFreeNode->height >= allocHeight))
        {
            allocPossible = TRUE;
        }
    }
    if (allocBucket->freeStats.maxHeightFreeNode)
    {
        if ((allocBucket->freeStats.maxHeightFreeNode->width >= allocWidth)
            &&
            (allocBucket->freeStats.maxHeightFreeNode->height >= allocHeight))
        {
            allocPossible = TRUE;
        }
    }
    return allocPossible;

}

static
Bool tiler_alloc_free_all_unused_nodes_in_tree_visit_fxn(TileAllocNode_t *root,void *prm)
{
    TileAllocBucket_t *bucket = prm;

    if((root->isUsed == FALSE) && (root != bucket->head))
    {
        tiler_alloc_add_to_freednodes(bucket,root);
    }
    return FALSE;
}


static
void tiler_alloc_harvest_free_nodes(TileAllocBucket_t *bucket)
{
    UInt32 freeIndexPreHarvest = bucket->freedNodes.numNodes;
    UInt i;

    tiler_alloc_traverse_node_tree(bucket->head,&tiler_alloc_free_all_unused_nodes_in_tree_visit_fxn,bucket);

    for(i = freeIndexPreHarvest; i < bucket->freedNodes.numNodes;i++)
    {
        tiler_alloc_unlink_node_from_tree(bucket,bucket->freedNodes.nodes[i]);
    }
    tiler_alloc_merge_freed_nodes(bucket);
}

/*
 * \fn     tiler_alloc_find_bucket_to_alloc
 * \brief  Find a bucket to allocate the frame from
 *
 * Find best match bucket to allocate
 * - First alloc from bucket where same resolution has already been alloced 
 * - Second alloc from a fresh bucket
 * - Third alloc from a bucket where different resolution was alloced.
 */
static
TileAllocBucket_t * tiler_alloc_find_bucket_to_alloc(UInt32 cntMode,UInt32 allocWidth,UInt32 allocHeight)
{
    UInt i,j;
    TileAllocBucket_t *allocBucket = NULL;
    TileAllocBucket_t *possibleFit = NULL;
    EncDec_ResolutionClass resClass = tiler_alloc_get_resolution_class(allocWidth,allocHeight,cntMode);
    TileAllocBucketAry *TileAllocBucket = NULL;

    TileAllocBucket = tiler_alloc_cfg_get_bucket_array(cntMode);

    /* First try allocation from a bucket already having the resolution */
    for (i = 0; i < UTILS_ARRAYSIZE((*TileAllocBucket));i++)
    {
       if ((*TileAllocBucket)[i].isValid)
       {
           for (j = 0; j < (*TileAllocBucket)[i].numAllocResolutions; j++)
           {
               if (tiler_alloc_check_alloc_possible_in_bucket(&(*TileAllocBucket)[i],
                                                              allocWidth,
                                                              allocHeight))
               {
                   possibleFit = &(*TileAllocBucket)[i];
                   if((*TileAllocBucket)[i].allocResolution[j] == resClass)
                   {
                       allocBucket = &((*TileAllocBucket)[i]);
                       break;
                   }
               }
           }
       }
       if (allocBucket)
           break;
    }
    if (!allocBucket)
    {
        /* Next try allocation from a bucket which is not allocated */
        for (i = 0; i < UTILS_ARRAYSIZE(*TileAllocBucket);i++)
        {
           if ((*TileAllocBucket)[i].isValid)
           {
               if (((*TileAllocBucket)[i].numAllocatedNodes == 0)
                   &&
                   ((*TileAllocBucket)[i].bucketWidth >= allocWidth)
                   &&
                   ((*TileAllocBucket)[i].bucketHeight >= allocHeight))
               {
                   allocBucket = &((*TileAllocBucket)[i]);
                   break;
               }
           }
        }
    }
    if (!allocBucket && !possibleFit)
    {
        for (i = 0; i < UTILS_ARRAYSIZE((*TileAllocBucket));i++)
        {
           if ((*TileAllocBucket)[i].isValid && ((*TileAllocBucket)[i].freeStats.totalFreeArea >= (allocWidth * allocHeight)))
           {
               tiler_alloc_harvest_free_nodes(&((*TileAllocBucket)[i]));
               if (tiler_alloc_check_alloc_possible_in_bucket(&(*TileAllocBucket)[i],
                                                              allocWidth,
                                                              allocHeight))
               {
                   possibleFit = &(*TileAllocBucket)[i];
               }
           }
        }
    }
    if (!allocBucket)
    {
        /* Finally try allocation from a bucket which has free space to accomodate this resolution class */
        allocBucket = possibleFit;
    }
    return allocBucket;
}

static
void utils_tiler_init_container_offset(TileAllocConfig_t *tileAllocCfg)
{
    tileAllocCfg->startOffset[TILER_ALLOC_CONTAINER_INDEX_8BIT] = 0;
    if (UTILS_TILER_PHYS_MEM_SIZE > 1 * UTILS_TILER_CONTAINER_MAXSIZE)
    {
        tileAllocCfg->startOffset[TILER_ALLOC_CONTAINER_INDEX_16BIT] = 0;
    }
    else
    {
        tileAllocCfg->startOffset[TILER_ALLOC_CONTAINER_INDEX_16BIT] =
            tileAllocCfg->startOffset[TILER_ALLOC_CONTAINER_INDEX_8BIT] +
            UTILS_TILER_CNT_8BIT_SIZE;
    }
}

static
void  tiler_alloc_init_tile_config_default_params(TileAllocConfig_t *tileAllocCfg)
{
    tileAllocCfg->minAllocResolution = UTILS_ENCDEC_RESOLUTION_CLASS_CIF;
    tileAllocCfg->containerWidth[TILER_ALLOC_CONTAINER_INDEX_8BIT] = TILE8_MAX_WIDTH;
    tileAllocCfg->containerWidth[TILER_ALLOC_CONTAINER_INDEX_16BIT] = TILE16_MAX_WIDTH;
    tileAllocCfg->containerHeight[TILER_ALLOC_CONTAINER_INDEX_8BIT] = UTILS_TILER_MAX_HEIGHT_8BIT;
    tileAllocCfg->containerHeight[TILER_ALLOC_CONTAINER_INDEX_16BIT] = UTILS_TILER_MAX_HEIGHT_16BIT;
    utils_tiler_init_container_offset(tileAllocCfg);
    tileAllocCfg->allocatorDisabled = FALSE;
    tileAllocCfg->enableLog = FALSE;
}
static 
void  tiler_alloc_init_tile_config(TileAllocConfig_t *tileAllocCfg)
{
    Int i,cntIndex;

    memset(tileAllocCfg,0,sizeof(*tileAllocCfg));
    for (cntIndex = 0; cntIndex < UTILS_ARRAYSIZE(tileAllocCfg->TileAllocBucket);cntIndex++)
    {
        for (i = 0; i < UTILS_ARRAYSIZE(tileAllocCfg->TileAllocBucket[cntIndex]);i++)
        {
            tiler_alloc_init_bucket(&tileAllocCfg->TileAllocBucket[cntIndex][i],cntIndex);
        }
    }
    tiler_alloc_node_obj_tbl_init(&tileAllocCfg->TileAllocNodeTable);
    tiler_alloc_init_tile_config_default_params(tileAllocCfg);
    tileAllocCfg->forceFreeAllDone = FALSE;
    tileAllocCfg->initDone = TRUE;
}

static 
void  tiler_alloc_deinit_tile_config(TileAllocConfig_t *tileAllocCfg)
{
    Int i,cntIndex;
    
    UTILS_assert(tileAllocCfg->initDone);
    for (cntIndex = 0; cntIndex < UTILS_ARRAYSIZE(tileAllocCfg->TileAllocBucket);cntIndex++)
    {
        for (i = 0; i < UTILS_ARRAYSIZE(tileAllocCfg->TileAllocBucket[cntIndex]);i++)
        {
            tiler_alloc_deinit_bucket(&tileAllocCfg->TileAllocBucket[cntIndex][i]);
        }
    }
    tiler_alloc_node_obj_tbl_deinit(&tileAllocCfg->TileAllocNodeTable);
    tileAllocCfg->initDone = FALSE;
}


Int32 Utils_tilerAllocatorBinPackInit()
{
    Int32 status = 0;
    TileAllocConfig_t *tileAllocCfg = tiler_alloc_cfg_get();

    UTILS_COMPILETIME_ASSERT(TILER_ALLOC_CONTAINER_INDEX_8BIT == UTILS_TILER_CNT_8BIT);
    UTILS_COMPILETIME_ASSERT(TILER_ALLOC_CONTAINER_INDEX_16BIT == UTILS_TILER_CNT_16BIT);
    tiler_alloc_init_tile_config(tileAllocCfg);
    Utils_tilerAllocatorBinPackSetSingleBucketGeometry(tileAllocCfg->minAllocResolution);
    return status;
}

void Utils_tilerAllocatorBinPackSetBucketGeometry(UInt32 bucketWidth,UInt32 bucketHeight,EncDec_ResolutionClass minRes)
{
    TileAllocConfig_t *tileAllocCfg = tiler_alloc_cfg_get();
    UInt32 minWidth,minHeight;
    UInt32 cntIndex;

    UTILS_assert((0 == UTILS_ENCDEC_RESOLUTION_CLASS_FIRST) && (minRes <= UTILS_ENCDEC_RESOLUTION_CLASS_LAST));
    UTILS_assert(tileAllocCfg->initDone == TRUE);
    for (cntIndex = 0; cntIndex < TILER_ALLOC_NUM_CONTAINERS; cntIndex++)
    {
        tiler_alloc_get_resolution_class_info(cntIndex,minRes,&minWidth,&minHeight);
        tiler_alloc_partition_tiler_container_into_buckets(cntIndex,
                                                           tileAllocCfg->containerWidth[cntIndex],
                                                           tileAllocCfg->containerHeight[cntIndex],
                                                           bucketWidth,
                                                           TILER_ALLOC_GET_CONTAINER_ADJUSTED_HEIGHT(cntIndex,bucketHeight),
                                                           minWidth,
                                                           minHeight); 
    }
    tileAllocCfg->minAllocResolution = minRes;    
}

void Utils_tilerAllocatorBinPackSetSingleBucketGeometry(EncDec_ResolutionClass minRes)
{
    TileAllocConfig_t *tileAllocCfg = tiler_alloc_cfg_get();
    UInt32 minWidth,minHeight;
    UInt32 cntIndex;

    UTILS_assert((0 == UTILS_ENCDEC_RESOLUTION_CLASS_FIRST) && (minRes <= UTILS_ENCDEC_RESOLUTION_CLASS_LAST));
    UTILS_assert(tileAllocCfg->initDone == TRUE);
    for (cntIndex = 0; cntIndex < TILER_ALLOC_NUM_CONTAINERS; cntIndex++)
    {
        tiler_alloc_get_resolution_class_info(cntIndex,minRes,&minWidth,&minHeight);
        tiler_alloc_partition_tiler_container_into_buckets(cntIndex,
                                                           tileAllocCfg->containerWidth[cntIndex],
                                                           tileAllocCfg->containerHeight[cntIndex],
                                                           tileAllocCfg->containerWidth[cntIndex],
                                                           tileAllocCfg->containerHeight[cntIndex],
                                                           minWidth,
                                                           minHeight); 
    }
    tileAllocCfg->minAllocResolution = minRes;    
}


static
UInt32 Utils_tilerAllocatorGetAddr(UInt32 cntMode, UInt32 startX, UInt32 startY)
{
    UInt32 tilerAddr, maxPitch,  offset;
    TileAllocConfig_t *tileAllocCfg = tiler_alloc_cfg_get();

    UTILS_assert(TRUE == tileAllocCfg->initDone);

    Utils_tilerGetMaxPitch(cntMode, &maxPitch);
    UTILS_assert(cntMode < UTILS_ARRAYSIZE(tileAllocCfg->startOffset));
    offset = tileAllocCfg->startOffset[cntMode];

    tilerAddr = offset + (startY * maxPitch + startX);
    tilerAddr = UTILS_TILER_PUT_CNT_MODE(tilerAddr, cntMode);

    if (tileAllocCfg->enableLog)
    {
        Vps_printf(" [TILER] Tiler Addr = 0x%08x, mode = %d, x,y = %d,%d\n",
                   tilerAddr, cntMode, startX, startY);
    }

    return tilerAddr;
}

static
void Utils_tilerAllocatorMapTileAddr2Coordinates(UInt32 tilerAddr,UInt32 *cntMode,UInt32 *startX,UInt32 *startY)
{
    UInt32  maxPitch, offset;
    TileAllocConfig_t *tileAllocCfg = tiler_alloc_cfg_get();

    UTILS_assert(tilerAddr != UTILS_TILER_INVALID_ADDR);
   *cntMode = UTILS_TILER_GET_CNT_MODE(tilerAddr);
    UTILS_assert(*cntMode < UTILS_ARRAYSIZE(tileAllocCfg->startOffset));
    offset = tileAllocCfg->startOffset[*cntMode];
    Utils_tilerGetMaxPitch(*cntMode, &maxPitch);
    tilerAddr = UTILS_TILER_CLEAR_CNT_MODE(tilerAddr);
    *startX = (tilerAddr - offset) % maxPitch;
    *startY = (tilerAddr - offset) / maxPitch;
    UTILS_assert(Utils_tilerAllocatorGetAddr(*cntMode,*startX,*startY) == UTILS_TILER_PUT_CNT_MODE(tilerAddr,*cntMode));
    if (tileAllocCfg->enableLog)
    {
        Vps_printf(" [TILER] Tiler Addr = 0x%08x, mode = %d, x,y = %d,%d\n",
                   UTILS_TILER_PUT_CNT_MODE(tilerAddr,*cntMode), *cntMode, *startX, *startY);
    }

}

static
void tiler_alloc_validate_tree(TileAllocBucket_t *bucket)
{
    TilerAllocTreeValidatePrm_s validatePrm;

    validatePrm.head = bucket->head;
    validatePrm.numNodesInTree = 0;
    tiler_alloc_traverse_node_tree(bucket->head,&tiler_alloc_validate_tree_visit_fxn,&validatePrm);
    UTILS_assert(validatePrm.numNodesInTree == bucket->numNodesInTree);
}

UInt32 Utils_tilerAllocatorBinPackAlloc(UInt32 cntMode, UInt32 width, UInt32 height)
{
    TileAllocBucket_t *bucket = NULL;
    TileAllocNode_t *allocNode = NULL;
    TileAllocConfig_t *tileAllocCfg = tiler_alloc_cfg_get();
    UInt32 tilerAddr = UTILS_TILER_INVALID_ADDR;

    UTILS_assert(tileAllocCfg->initDone == TRUE);
    UTILS_assert((cntMode == UTILS_TILER_CNT_8BIT) || (cntMode == UTILS_TILER_CNT_16BIT));

    if (FALSE == tileAllocCfg->allocatorDisabled)
    {
       
        tileAllocCfg->forceFreeAllDone = FALSE; /* Reset force free on alloc */
        width = VpsUtils_align(width, TILER_ALLOC_WIDTH_ALIGN);
        height = VpsUtils_align(height, TILER_ALLOC_HEIGHT_ALIGN);

        bucket = tiler_alloc_find_bucket_to_alloc(cntMode,width,height);
        if (bucket)
        {
            allocNode = tiler_alloc_frame(bucket,width,height);
            if (allocNode)
            {
                UTILS_assert((allocNode->startX & (TILER_ALLOC_WIDTH_ALIGN - 1)) == 0);
                UTILS_assert((allocNode->startY & (TILER_ALLOC_HEIGHT_ALIGN - 1)) == 0);
                tilerAddr = Utils_tilerAllocatorGetAddr(cntMode, allocNode->startX, allocNode->startY);
                UTILS_assert(tilerAddr != UTILS_TILER_INVALID_ADDR);
            }
            tiler_alloc_validate_tree(bucket);
        }
    }
    else
    {
        tilerAddr = UTILS_TILER_INVALID_ADDR;
    }
    UTILS_assert(tilerAddr != UTILS_TILER_INVALID_ADDR);
    //Vps_printf("TILER_ALLOC:CNT%d,width:%d,height:%d,Addr:%x\n",cntMode, width, height,tilerAddr);
    return tilerAddr;
}

Int32 Utils_tilerAllocatorBinPackFree(UInt32 tileAddr)
{
    TileAllocConfig_t *tileAllocCfg = tiler_alloc_cfg_get();
    UInt32 cntMode, startX,startY;
    TileAllocBucket_t *bucket = NULL;
    TileAllocNode_t *allocNode = NULL;

    UTILS_assert(tileAllocCfg->initDone == TRUE);
    UTILS_assert (FALSE == tileAllocCfg->allocatorDisabled);
    if (tileAllocCfg->forceFreeAllDone == FALSE)
    {
        Utils_tilerAllocatorMapTileAddr2Coordinates(tileAddr,&cntMode, &startX,&startY);
        bucket = tiler_alloc_map_coordinates2bucket(cntMode,startX,startY);
        UTILS_assert(bucket != NULL);
        allocNode = tiler_alloc_locate_node_in_bucket(bucket,startX,startY);
        UTILS_assert(allocNode != NULL);
        UTILS_assert(allocNode->isUsed == TRUE);
        UTILS_assert(tiler_alloc_is_dummy_head_node(allocNode) == FALSE);
        tiler_alloc_free_node(bucket,allocNode);
        tiler_alloc_merge_freed_nodes(bucket);
        tiler_alloc_bucket_get_free_mem_stats(bucket);
        tiler_alloc_validate_tree(bucket);
    }

    if (bucket && (bucket->numAllocatedNodes == 0))
    {
        UTILS_assert(bucket->freeStats.totalFreeArea == (bucket->bucketWidth * bucket->bucketHeight));
        tiler_alloc_reset_bucket(bucket,FALSE);
    }
    return 0;
}

Int32 Utils_tilerAllocatorBinPackGetStats(UInt32 cntMode,TilerAllocStats_t *stats)
{
    TileAllocConfig_t *tileAllocCfg = tiler_alloc_cfg_get();
    TileAllocBucketAry * TileAllocBucket = tiler_alloc_cfg_get_bucket_array(cntMode);
    UInt i,resIdx;
    
    UTILS_assert(stats != NULL);
    memset(stats,0,sizeof(*stats));
    UTILS_assert(cntMode < UTILS_ARRAYSIZE(tileAllocCfg->containerWidth));
    stats->containerWidth  = tileAllocCfg->containerWidth[cntMode];
    stats->containerHeight = tileAllocCfg->containerHeight[cntMode];

    for (i = 0; i < UTILS_ARRAYSIZE(*TileAllocBucket);i++)
    {
        if ((*TileAllocBucket)[i].isValid)
        {
            stats->bucketWidth = (*TileAllocBucket)[i].bucketWidth;
            stats->bucketHeight = (*TileAllocBucket)[i].bucketHeight;
            stats->totalAreaFreeBuckets += (*TileAllocBucket)[i].freeStats.totalFreeArea; 
            if ((*TileAllocBucket)[i].numAllocatedNodes == 0)
            {
                stats->numFreeBuckets++;
            }
            else
            {
                stats->numUsedBuckets++;
                stats->totalAreaUsedBuckets += 
                    (*TileAllocBucket)[i].bucketWidth * (*TileAllocBucket)[i].bucketHeight - (*TileAllocBucket)[i].freeStats.totalFreeArea;
                stats->totalAreaWasted +=   (*TileAllocBucket)[i].freeStats.totalWasteArea;
                if ((*TileAllocBucket)[i].freeStats.maxWidthFreeNode)
                {
                    if (stats->maxRect[TILER_ALLOC_STATS_MAX_WIDTH_RECT_INDEX].width < (*TileAllocBucket)[i].freeStats.maxWidthFreeNode->width)
                    {
                        stats->maxRect[TILER_ALLOC_STATS_MAX_WIDTH_RECT_INDEX].width = (*TileAllocBucket)[i].freeStats.maxWidthFreeNode->width;
                        stats->maxRect[TILER_ALLOC_STATS_MAX_WIDTH_RECT_INDEX].height = (*TileAllocBucket)[i].freeStats.maxWidthFreeNode->height;
                    }
                }
                if ((*TileAllocBucket)[i].freeStats.maxHeightFreeNode)
                {
                    if (stats->maxRect[TILER_ALLOC_STATS_MAX_HEIGHT_RECT_INDEX].height < (*TileAllocBucket)[i].freeStats.maxHeightFreeNode->height)
                    {
                        stats->maxRect[TILER_ALLOC_STATS_MAX_HEIGHT_RECT_INDEX].width = (*TileAllocBucket)[i].freeStats.maxHeightFreeNode->width;
                        stats->maxRect[TILER_ALLOC_STATS_MAX_HEIGHT_RECT_INDEX].height = (*TileAllocBucket)[i].freeStats.maxHeightFreeNode->height;
                    }
                }
                UTILS_COMPILETIME_ASSERT(UTILS_ARRAYSIZE(stats->resAllocCount) == UTILS_ARRAYSIZE((*TileAllocBucket)[0].resAllocCount));
                for (resIdx = 0;  resIdx < UTILS_ARRAYSIZE(stats->resAllocCount); resIdx++)
                {
                    stats->resAllocCount[resIdx] += (*TileAllocBucket)[i].resAllocCount[resIdx];
                }
            }
        }
    }
    return 0;
}


Int32 Utils_tilerAllocatorBinPackDeInit()
{
    TileAllocConfig_t *tileAllocCfg = tiler_alloc_cfg_get();

    tiler_alloc_deinit_tile_config(tileAllocCfg);
    return 0;
}

Int32 Utils_tilerAllocatorBinPackFreeAll()
{
    Int i,cntIndex;
    TileAllocConfig_t *tileAllocCfg = tiler_alloc_cfg_get();

    if (FALSE == tileAllocCfg->forceFreeAllDone)
    {
        for (cntIndex = 0; cntIndex < UTILS_ARRAYSIZE(tileAllocCfg->TileAllocBucket);cntIndex++)
        {
            for (i = 0; i < UTILS_ARRAYSIZE(tileAllocCfg->TileAllocBucket[cntIndex]);i++)
            {
                if (tileAllocCfg->TileAllocBucket[cntIndex][i].isValid)
                {
                    tiler_alloc_reset_bucket(&tileAllocCfg->TileAllocBucket[cntIndex][i],TRUE);
                }
            }
        }
        tileAllocCfg->forceFreeAllDone = TRUE;
    }
    return 0;
}


TileAllocConfig_t    TileAllocConfig;

static
TileAllocConfig_t * tiler_alloc_cfg_get()
{
    return &TileAllocConfig;
}


