/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include "utils_dma_bios6.h"



//#define SYSTEM_UTILS_DMA_PARAM_CHECK

#define UTILS_DMA_M3_VIRT_2_PHYS_OFFSET     (0x80000000-0x20000000)

#define UTILS_DMA_FAST_FILL_MAX_PIXELS_PER_LINE   (2048)

Utils_DmaObj gUtils_dmaObj;

/* +2 for YUV422 2bytes per pixel, +2 for Y/C for YUV420SP data format */
#pragma DATA_ALIGN(gUtils_dmaFastFillBuf, 128)
static UInt8 gUtils_dmaFastFillBuf[UTILS_DMA_FAST_FILL_COLOR_MAX][UTILS_DMA_FAST_FILL_MAX_PIXELS_PER_LINE*(2+2)];

static inline UInt32 Utils_dmaVirtToPhys(UInt32 addr)
{
    UInt32 msbNibble;

    msbNibble = (addr >> 28);

    if(msbNibble >= 2 && msbNibble <= 3)
        addr+=UTILS_DMA_M3_VIRT_2_PHYS_OFFSET;

    return addr;
}

Int32 Utils_dmaFastFillSetColor(UInt32 colorIndex, UInt32 fillColorYUYV)
{
    volatile UInt8 *pAddr;
    UInt32 fillValue, i;

    if(colorIndex >= UTILS_DMA_FAST_FILL_COLOR_MAX )
    {
        Vps_rprintf(" UTILS: DMA: Utils_dmaFastFillSetColor(): WARNING: Color Index out of range, ignoring !!!\n");
        return -1;
    }

    pAddr = gUtils_dmaFastFillBuf[colorIndex];
    fillValue = fillColorYUYV;

    /* memset for YUV422 data format */
    for(i=0; i<(UTILS_DMA_FAST_FILL_MAX_PIXELS_PER_LINE*2)/sizeof(UInt32); i++)
    {
        *(volatile UInt32*)pAddr = fillValue;
        pAddr += sizeof(UInt32);
    }

    /* memset Y color for YUV420SP */
    {
        /* set fill color for Y-plane */
        UInt8 y0 = (fillColorYUYV & 0x000000FF);
        UInt8 y1 = (((fillColorYUYV & 0x00FF0000) >> 16) & 0xFF);

        fillValue = (y0<<0) | (y1<<8) | (y0<<16) | (y1<<24);
    }

    for(i=0; i<(UTILS_DMA_FAST_FILL_MAX_PIXELS_PER_LINE)/sizeof(UInt32); i++)
    {
        *(volatile UInt32*)pAddr = fillValue;
        pAddr += sizeof(UInt32);
    }


    /* memset C color for YUV420SP */
    {
        /* set fill color for C-plane */
        UInt8 c0 = (((fillColorYUYV & 0x0000FF00) >> 8)  & 0xFF);
        UInt8 c1 = (((fillColorYUYV & 0xFF000000) >> 24) & 0xFF);

        fillValue = (c0<<0) | (c1<<8) | (c0<<16) | (c1<<24);
    }

    for(i=0; i<(UTILS_DMA_FAST_FILL_MAX_PIXELS_PER_LINE)/sizeof(UInt32); i++)
    {
        *(volatile UInt32*)pAddr = fillValue;
        pAddr += sizeof(UInt32);
    }


    Cache_wbInv(gUtils_dmaFastFillBuf[colorIndex], UTILS_DMA_FAST_FILL_MAX_PIXELS_PER_LINE*(2+2), Cache_Type_ALL, TRUE);

    return 0;
}

UInt32 Utils_dmaFastFillGetLineAddr(UInt32 dataFormat, UInt32 colorPlane, UInt32 colorIndex)
{
    UInt8 *pAddr;

    if(colorIndex >= UTILS_DMA_FAST_FILL_COLOR_MAX )
    {
        Vps_rprintf(" UTILS: DMA: Utils_dmaFastFillGetLineAddr(): WARNING: Color Index out of range, using index 0 !!!\n");
        colorIndex = 0;
    }

    pAddr = gUtils_dmaFastFillBuf[colorIndex];

    if(dataFormat==FVID2_DF_YUV420SP_UV)
    {
        /* Y-plane */
        if(colorPlane==0)
            pAddr += UTILS_DMA_FAST_FILL_MAX_PIXELS_PER_LINE*2;
        else
            pAddr += UTILS_DMA_FAST_FILL_MAX_PIXELS_PER_LINE*(2+1);
    }

    return Utils_dmaVirtToPhys((UInt32)pAddr);
}


void Utils_dmaCallback(unsigned int tcc, EDMA3_RM_TccStatus status, void *appData)
{
    (void)tcc;
    Utils_DmaChObj *pObj = (Utils_DmaChObj *)appData;

    switch (status)
    {
        case EDMA3_RM_XFER_COMPLETE:
            Semaphore_post(pObj->semComplete);
            break;
        case EDMA3_RM_E_CC_DMA_EVT_MISS:

            break;
        case EDMA3_RM_E_CC_QDMA_EVT_MISS:

            break;
        default:
            break;
    }
}


Int32 Utils_dmaInit()
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;

    memset(&gUtils_dmaObj, 0, sizeof(gUtils_dmaObj));

    gUtils_dmaObj.hEdma = Utils_edma3init(gUtils_dmaObj.edma3InstanceId, &edma3Result);
    if (!gUtils_dmaObj.hEdma)
    {
        Vps_printf(
            " UTILS: DMA: Utils_dmaInit() ... FAILED (%d) \n",
                edma3Result
            );
    }

    Utils_dmaFastFillSetColor(UTILS_DMA_FAST_FILL_COLOR_BLACK, UTILS_DMA_GENERATE_FILL_PATTERN(0x30, 0x80, 0x80));
    Utils_dmaFastFillSetColor(UTILS_DMA_FAST_FILL_COLOR_GREY , UTILS_DMA_GENERATE_FILL_PATTERN(0x80, 0x80, 0x80));
    Utils_dmaFastFillSetColor(UTILS_DMA_FAST_FILL_COLOR_BLUE , UTILS_DMA_GENERATE_FILL_PATTERN(0x68, 0xD5, 0x5A));
    Utils_dmaFastFillSetColor(UTILS_DMA_FAST_FILL_COLOR_WHITE, UTILS_DMA_GENERATE_FILL_PATTERN(0xFF, 0x80, 0x80));

    return edma3Result;
}

Int32 Utils_dmaDeInit()
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;

    edma3Result = Utils_edma3deinit(gUtils_dmaObj.edma3InstanceId, gUtils_dmaObj.hEdma);
    if (edma3Result != EDMA3_DRV_SOK)
    {
        Vps_printf(
            " UTILS: DMA: Utils_dmaDeInit() ... FAILED (%d) \n",
                edma3Result
            );
    }

    return edma3Result;
}

Int32 Utils_dmaCreateSemaphores(Utils_DmaChObj *pObj)
{
    Semaphore_Params semParams;

    Semaphore_Params_init(&semParams);

    semParams.mode = Semaphore_Mode_BINARY;

    pObj->semComplete = Semaphore_create(0u, &semParams, NULL);
    UTILS_assert(pObj->semComplete != NULL);

    Semaphore_Params_init(&semParams);

    semParams.mode = Semaphore_Mode_BINARY;

    pObj->semLock = Semaphore_create(1u, &semParams, NULL);
    UTILS_assert(pObj->semLock != NULL);

    return 0;
}

Int32 Utils_dmaAllocDmaCh(Utils_DmaChObj *pObj)
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;
    EDMA3_RM_TccCallback tccCb;

    pObj->iTcc        = EDMA3_DRV_TCC_ANY;
    pObj->iChannel    = EDMA3_DRV_DMA_CHANNEL_ANY;

    if(pObj->enableIntCb)
        tccCb = &Utils_dmaCallback;
    else
        tccCb = NULL;

    edma3Result = EDMA3_DRV_requestChannel (gUtils_dmaObj.hEdma, &pObj->iChannel, &pObj->iTcc,
                                   (EDMA3_RM_EventQueue)pObj->eventQ,
                                      tccCb, (void *)pObj);

    if (edma3Result == EDMA3_DRV_SOK)
    {
        Vps_printf(" UTILS: DMA: Allocated CH (TCC) = %d (%d)\n", pObj->iChannel, pObj->iTcc);

        edma3Result = EDMA3_DRV_clearErrorBits (gUtils_dmaObj.hEdma,pObj->iChannel);
    }

    return edma3Result;
}

Int32 Utils_dmaAllocParam(Utils_DmaChObj *pObj)
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;
    UInt32 paramId, paramPhyAddr, iChannel, iTcc;
    Utils_DmaTxObj  *pTxObj;

    /* allocate PaRAM entries */
    for(paramId=0; paramId<pObj->maxTransfers; paramId++)
    {
        pTxObj = &pObj->pTxObj[paramId];

        pTxObj->pBlankPixelPhysAddr = (Utils_DmaBlankPixel*)Utils_dmaVirtToPhys((UInt32)&pTxObj->blankPixel);

        edma3Result = EDMA3_DRV_SOK;

        if(paramId==0)
        {
            pTxObj->iParam = pObj->iChannel;
        }
        else
        {
            iChannel = EDMA3_DRV_LINK_CHANNEL;
            iTcc     = EDMA3_DRV_TCC_ANY;

            edma3Result = EDMA3_DRV_requestChannel (
                                    gUtils_dmaObj.hEdma,
                                    &iChannel,
                                    &iTcc,
                                    (EDMA3_RM_EventQueue)pObj->eventQ,
                                    NULL,
                                    NULL
                                  );

            pTxObj->iParam = iChannel;
        }

        if (edma3Result == EDMA3_DRV_SOK)
        {
            edma3Result = EDMA3_DRV_getPaRAMPhyAddr(gUtils_dmaObj.hEdma, pTxObj->iParam, &paramPhyAddr);

            pTxObj->pParamSet = (EDMA3_DRV_PaRAMRegs*)paramPhyAddr;
        }

        pTxObj->pParamSet->destAddr   = 0;
        pTxObj->pParamSet->srcAddr    = 0;
        pTxObj->pParamSet->srcBIdx    = 0;
        pTxObj->pParamSet->destBIdx   = 0;
        pTxObj->pParamSet->srcCIdx    = 0;
        pTxObj->pParamSet->destCIdx   = 0;
        pTxObj->pParamSet->aCnt       = 0;
        pTxObj->pParamSet->bCnt       = 0;
        pTxObj->pParamSet->cCnt       = 1;
        pTxObj->pParamSet->bCntReload = 0;
        pTxObj->pParamSet->opt        = 0;
        pTxObj->pParamSet->linkAddr   = 0xFFFF;

        if (edma3Result == EDMA3_DRV_SOK)
        {
            Vps_printf(" UTILS: DMA: %d of %d: Allocated PaRAM = %d (0x%08X)\n", paramId, pObj->maxTransfers, pTxObj->iParam, pTxObj->pParamSet);
        }
        else
        {
            Vps_printf(" UTILS: DMA: %d of %d: ERROR in PaRAM allocation\n", paramId, pObj->maxTransfers);
            break;
        }
    }

    return edma3Result;
}

Int32 Utils_dmaFreeDmaChParam(Utils_DmaChObj *pObj)
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;
    UInt32 paramId;

    for(paramId=0; paramId<pObj->maxTransfers; paramId++)
    {
        edma3Result = EDMA3_DRV_freeChannel(gUtils_dmaObj.hEdma, pObj->pTxObj[paramId].iParam);
    }

    return edma3Result;
}

Int32 Utils_dmaCreateCh(Utils_DmaChObj *pObj, UInt32 eventQ, UInt32 maxTransfers, Bool enableIntCb)
{
    UInt32 memSize;
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;

    memset(pObj, 0, sizeof(*pObj));

    if(maxTransfers<1)
        maxTransfers = 1;

    pObj->maxTransfers= maxTransfers;
    pObj->eventQ      = eventQ;
    pObj->enableIntCb = enableIntCb;

    /* alloc and clear PaRAM data structure's */
    memSize       = sizeof(Utils_DmaTxObj)*pObj->maxTransfers;

    pObj->pTxObj  = malloc(memSize);
    UTILS_assert(pObj->pTxObj != NULL);

    memset(pObj->pTxObj, 0, memSize);

    if (edma3Result == EDMA3_DRV_SOK)
        edma3Result |= Utils_dmaCreateSemaphores(pObj);

    if (edma3Result == EDMA3_DRV_SOK)
        edma3Result |= Utils_dmaAllocDmaCh(pObj);

    if (edma3Result == EDMA3_DRV_SOK)
        edma3Result |= Utils_dmaAllocParam(pObj);

    return edma3Result;
}

Int32 Utils_dmaDeleteCh(Utils_DmaChObj *pObj)
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;

    edma3Result = Utils_dmaFreeDmaChParam(pObj);

    free(pObj->pTxObj);
    pObj->pTxObj = NULL;

    Semaphore_delete(&pObj->semComplete);
    Semaphore_delete(&pObj->semLock);

    Vps_printf(" UTILS: DMA: Free'ed CH (TCC) = %d (%d)\n", pObj->iChannel, pObj->iTcc);

    return edma3Result;
}

Int32 Utils_dmaPollWaitComplete(Utils_DmaChObj *pObj)
{
    uint16_t tccStatus;

    EDMA3_DRV_waitAndClearTcc(gUtils_dmaObj.hEdma,pObj->iTcc);
    EDMA3_DRV_checkAndClearTcc(gUtils_dmaObj.hEdma,pObj->iTcc, &tccStatus);
    EDMA3_DRV_clearErrorBits (gUtils_dmaObj.hEdma,pObj->iChannel);

    return 0;
}


Int32 Utils_dmaTriggerAndWait(Utils_DmaChObj *pObj)
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;
    uint16_t tccStatus;

    edma3Result |= EDMA3_DRV_checkAndClearTcc(gUtils_dmaObj.hEdma,pObj->iTcc, &tccStatus);
    edma3Result |= EDMA3_DRV_clearErrorBits (gUtils_dmaObj.hEdma,pObj->iChannel);
    edma3Result |= EDMA3_DRV_enableTransfer (gUtils_dmaObj.hEdma,pObj->iChannel,
                                           EDMA3_DRV_TRIG_MODE_MANUAL);

    if (edma3Result != EDMA3_DRV_SOK)
    {
        Vps_printf(
            " UTILS: DMA: Utils_dmaTrigger() ... FAILED (CH=%d) \n",
                pObj->iChannel, edma3Result
            );
    }

    if (edma3Result == EDMA3_DRV_SOK)
    {
        if(pObj->enableIntCb)
        {
            Semaphore_pend(pObj->semComplete, BIOS_WAIT_FOREVER);
        }
        else
        {
            Utils_dmaPollWaitComplete(pObj);
        }
    }

    return edma3Result;
}

Int32 Utils_dmaParamSetCheck(EDMA3_DRV_PaRAMRegs *pParamSet)
{
    if(pParamSet->aCnt == 0
        ||
       pParamSet->bCnt == 0
        ||
       pParamSet->cCnt == 0
        ||
       pParamSet->destAddr == NULL
        ||
       pParamSet->srcAddr == NULL
        ||
       pParamSet->bCntReload == 0
    )
    {
        return FVID2_EFAIL;
    }

    return FVID2_SOK;
}


Int32 Utils_dmaLinkDma(Utils_DmaChObj *pObj, UInt32 numTx)
{
    UInt32 i;
    EDMA3_DRV_PaRAMRegs *pParamSet;
    UInt32 opt;

    /* link the EDMAs */
    for(i=0; i<numTx; i++)
    {
        pParamSet   = pObj->pTxObj[i].pParamSet;

        opt  =
                 (EDMA3_CCRL_OPT_ITCCHEN_ENABLE << EDMA3_CCRL_OPT_ITCCHEN_SHIFT)
               | ((pObj->iTcc << EDMA3_CCRL_OPT_TCC_SHIFT) & EDMA3_CCRL_OPT_TCC_MASK)
               | (EDMA3_CCRL_OPT_SYNCDIM_ABSYNC << EDMA3_CCRL_OPT_SYNCDIM_SHIFT)
                  ;

        if( i== (numTx-1) )
        {
            /* last DMA */

            /* enable interrupt for last Transfer ONLY */
            opt |= (EDMA3_CCRL_OPT_TCINTEN_ENABLE << EDMA3_CCRL_OPT_TCINTEN_SHIFT);

            /* do not link DMA */
            pParamSet->linkAddr = 0xFFFF;
        }
        else
        {
            /* enable chaining after transfer complete */
            opt |= (EDMA3_CCRL_OPT_TCCHEN_ENABLE << EDMA3_CCRL_OPT_TCCHEN_SHIFT);

            /* link current DMA to next DMA */
            pParamSet->linkAddr = (UInt32)pObj->pTxObj[i+1].pParamSet;
        }

        pParamSet->opt = opt;
    }

    return 0;
}

Int32 Utils_dmaRun(Utils_DmaChObj *pObj, UInt32 *numTx)
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;

    if(*numTx==0)
        return edma3Result;

    Utils_dmaLinkDma(pObj, *numTx);

    edma3Result = Utils_dmaTriggerAndWait(pObj);

    *numTx = 0;

    return edma3Result;
}

Int32 Utils_dmaFill2D(Utils_DmaChObj *pObj, Utils_DmaFill2D *pInfo, UInt32 numTransfers)
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;
    EDMA3_DRV_PaRAMRegs *pParamSet;
    UInt32 bpp; /* bytes per pixel */
    Utils_DmaBlankPixel *pBlankPixel;
    UInt32 i, numTx;

    Semaphore_pend(pObj->semLock, BIOS_WAIT_FOREVER);

    numTx = 0;

    for(i=0; i<numTransfers; i++)
    {
        pParamSet   = pObj->pTxObj[numTx].pParamSet;
        pBlankPixel = pObj->pTxObj[numTx].pBlankPixelPhysAddr;

        bpp = 2;
        if(pInfo->dataFormat==FVID2_DF_YUV420SP_UV)
            bpp = 1;

        pParamSet->destAddr   = (UInt32)pInfo->destAddr[0]
                              + pInfo->destPitch[0]*pInfo->startY
                              + pInfo->startX * bpp;
        pParamSet->srcAddr    = (uint32_t)&(pBlankPixel->pixel);

        if(pInfo->dataFormat==FVID2_DF_YUV420SP_UV)
        {
            /* set fill color for Y-plane */
            UInt8 y0 = (pInfo->fillColorYUYV & 0x000000FF);
            UInt8 y1 = (((pInfo->fillColorYUYV & 0x00FF0000) >> 16) & 0xFF);

            pBlankPixel->pixel = (y0<<0) | (y1<<8) | (y0<<16) | (y1<<24);
        }
        else
        {
            /* set the fill color */
            pBlankPixel->pixel = pInfo->fillColorYUYV;
        }

        pParamSet->srcBIdx    = 0;
        pParamSet->destBIdx   = UTILS_DMA_BYTES_TO_WRITE;
        pParamSet->srcCIdx    = 0;
        pParamSet->destCIdx   = pInfo->destPitch[0];

        pParamSet->aCnt       = UTILS_DMA_BYTES_TO_WRITE;
        pParamSet->bCnt       = (pInfo->width*bpp)/UTILS_DMA_BYTES_TO_WRITE;
        pParamSet->cCnt       = pInfo->height;
        /* If cCnt is zero DMA xfer doesnt happen.So set cCnt to 1 if cCnt happens to be zero */
        if (0 == pParamSet->cCnt)
            pParamSet->cCnt = 1;

        pParamSet->bCntReload = pParamSet->bCnt;

        #ifdef SYSTEM_UTILS_DMA_PARAM_CHECK
        UTILS_assert(
            Utils_dmaParamSetCheck(pParamSet) == FVID2_SOK
        );
        #endif

        if(pInfo->dataFormat==FVID2_DF_YUV420SP_UV)
        {
            numTx++;

            if(numTx>=pObj->maxTransfers)
            {
                edma3Result |= Utils_dmaRun(pObj, &numTx);
            }

            /* setup PaRAM for UV plane */
            pParamSet   = pObj->pTxObj[numTx].pParamSet;
            pBlankPixel = pObj->pTxObj[numTx].pBlankPixelPhysAddr;

            bpp = 1;

            /* for chroma plane when input or output is tiled we need to set pitch as -32KB
                since other wise pitch value will overflow the 16-bit register in EDMA.

                Hence all for all Chroma TX's we set pitch as -pitch and DMA from last line
                to first line
            */

            pParamSet->destAddr   = (UInt32)pInfo->destAddr[1]
                                  + pInfo->destPitch[1]*((pInfo->startY+pInfo->height)/2 - 1)
                                  + pInfo->startX * bpp;
            pParamSet->srcAddr    = (uint32_t)&(pBlankPixel->pixel);

            {
                /* set fill color for C-plane */
                UInt8 c0 = (((pInfo->fillColorYUYV & 0x0000FF00) >> 8)  & 0xFF);
                UInt8 c1 = (((pInfo->fillColorYUYV & 0xFF000000) >> 24) & 0xFF);

                pBlankPixel->pixel = (c0<<0) | (c1<<8) | (c0<<16) | (c1<<24);
            }

            pParamSet->srcBIdx    = 0;
            pParamSet->destBIdx   = UTILS_DMA_BYTES_TO_WRITE;
            pParamSet->srcCIdx    = 0;
            pParamSet->destCIdx   = -pInfo->destPitch[1];

            pParamSet->aCnt       = UTILS_DMA_BYTES_TO_WRITE;
            pParamSet->bCnt       = (pInfo->width*bpp)/UTILS_DMA_BYTES_TO_WRITE;
            pParamSet->cCnt       = pInfo->height/2;
            /* If cCnt is zero DMA xfer doesnt happen.So set cCnt to 1 if cCnt happens to be zero */
            if (0 == pParamSet->cCnt)
                pParamSet->cCnt = 1;

            pParamSet->bCntReload = pParamSet->bCnt;

            #ifdef SYSTEM_UTILS_DMA_PARAM_CHECK
            UTILS_assert(
                Utils_dmaParamSetCheck(pParamSet) == FVID2_SOK
            );
            #endif
        }

        /* goto next tranfer information */
        pInfo++;
        numTx++;

        if(numTx>=pObj->maxTransfers)
        {
            edma3Result |= Utils_dmaRun(pObj, &numTx);
        }
    }

    if(numTx)
    {
        edma3Result |= Utils_dmaRun(pObj, &numTx);
    }

    Semaphore_post(pObj->semLock);

    return edma3Result;
}

Int32 Utils_dmaFastFill2D(Utils_DmaChObj *pObj, Utils_DmaFill2D *pInfo, UInt32 numTransfers)
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;
    EDMA3_DRV_PaRAMRegs *pParamSet;
    UInt32 bpp; /* bytes per pixel */
    UInt32 i, numTx;

    Semaphore_pend(pObj->semLock, BIOS_WAIT_FOREVER);

    numTx = 0;

    for(i=0; i<numTransfers; i++)
    {
        pParamSet   = pObj->pTxObj[numTx].pParamSet;

        bpp = 2;
        if(pInfo->dataFormat==FVID2_DF_YUV420SP_UV)
            bpp = 1;

        pParamSet->destAddr   = (UInt32)pInfo->destAddr[0]
                              + pInfo->destPitch[0]*pInfo->startY
                              + pInfo->startX * bpp;
        pParamSet->srcAddr    = (uint32_t)Utils_dmaFastFillGetLineAddr(pInfo->dataFormat, 0, pInfo->fastFillColorIndex);

        pParamSet->destBIdx   = pInfo->destPitch[0];

        pParamSet->aCnt       = pInfo->width*bpp;
        pParamSet->bCnt       = pInfo->height;
        pParamSet->cCnt       = 1;

        #ifdef SYSTEM_UTILS_DMA_PARAM_CHECK
        UTILS_assert(
            Utils_dmaParamSetCheck(pParamSet) == FVID2_SOK
        );
        #endif

        if(pInfo->dataFormat==FVID2_DF_YUV420SP_UV)
        {
            numTx++;

            if(numTx>=pObj->maxTransfers)
            {
                edma3Result |= Utils_dmaRun(pObj, &numTx);
            }

            /* setup PaRAM for UV plane */
            pParamSet   = pObj->pTxObj[numTx].pParamSet;

            bpp = 1;

            /* for chroma plane when input or output is tiled we need to set pitch as -32KB
                since other wise pitch value will overflow the 16-bit register in EDMA.

                Hence all for all Chroma TX's we set pitch as -pitch and DMA from last line
                to first line
            */

            pParamSet->destAddr   = (UInt32)pInfo->destAddr[1]
                                  + pInfo->destPitch[1]*((pInfo->startY+pInfo->height)/2 - 1)
                                  + pInfo->startX * bpp;
            pParamSet->srcAddr    = (uint32_t)Utils_dmaFastFillGetLineAddr(pInfo->dataFormat, 1, pInfo->fastFillColorIndex);

            pParamSet->destBIdx   = -pInfo->destPitch[1];

            pParamSet->aCnt       = (pInfo->width*bpp);
            pParamSet->bCnt       = pInfo->height/2;
            pParamSet->cCnt       = 1;

            #ifdef SYSTEM_UTILS_DMA_PARAM_CHECK
            UTILS_assert(
                Utils_dmaParamSetCheck(pParamSet) == FVID2_SOK
            );
            #endif
        }

        /* goto next tranfer information */
        pInfo++;
        numTx++;

        if(numTx>=pObj->maxTransfers)
        {
            edma3Result |= Utils_dmaRun(pObj, &numTx);
        }
    }

    if(numTx)
    {
        edma3Result |= Utils_dmaRun(pObj, &numTx);
    }

    Semaphore_post(pObj->semLock);

    return edma3Result;
}

Int32 Utils_dmaCopy2D(Utils_DmaChObj *pObj, Utils_DmaCopy2D *pInfo, UInt32 numTransfers)
{
    EDMA3_DRV_Result edma3Result = EDMA3_DRV_SOK;
    EDMA3_DRV_PaRAMRegs *pParamSet;
    UInt32 bpp; /* bytes per pixel */
    UInt32 i, numTx;

    Semaphore_pend(pObj->semLock, BIOS_WAIT_FOREVER);

    numTx = 0;

    for(i=0; i<numTransfers; i++)
    {
        pParamSet   = pObj->pTxObj[numTx].pParamSet;

        bpp = 2;
        if(pInfo->dataFormat==FVID2_DF_YUV420SP_UV)
            bpp = 1;

        pParamSet->destAddr   = (UInt32)pInfo->destAddr[0]
                              + pInfo->destPitch[0]*pInfo->destStartY
                              + pInfo->destStartX * bpp;

        pParamSet->srcAddr   = (UInt32)pInfo->srcAddr[0]
                              + pInfo->srcPitch[0]*pInfo->srcStartY
                              + pInfo->srcStartX * bpp;

        pParamSet->srcBIdx    = pInfo->srcPitch[0];
        pParamSet->destBIdx   = pInfo->destPitch[0];

        pParamSet->aCnt       = (pInfo->width*bpp);
        pParamSet->bCnt       = pInfo->height;
        pParamSet->cCnt       = 1;

        #ifdef SYSTEM_UTILS_DMA_PARAM_CHECK
        UTILS_assert(
            Utils_dmaParamSetCheck(pParamSet) == FVID2_SOK
        );
        #endif

        if(pInfo->dataFormat==FVID2_DF_YUV420SP_UV)
        {
            numTx++;

            if(numTx>=pObj->maxTransfers)
            {
                edma3Result |= Utils_dmaRun(pObj, &numTx);
            }

            /* setup PaRAM for UV plane */
            pParamSet   = pObj->pTxObj[numTx].pParamSet;

            bpp = 1;

            /* for chroma plane when input or output is tiled we need to set pitch as -32KB
                since other wise pitch value will overflow the 16-bit register in EDMA.

                Hence all for all Chroma TX's we set pitch as -pitch and DMA from last line
                to first line
            */

            pParamSet->destAddr   = (UInt32)pInfo->destAddr[1]
                                  + pInfo->destPitch[1]*((pInfo->destStartY+pInfo->height)/2-1)
                                  + pInfo->destStartX * bpp;

            pParamSet->srcAddr   = (UInt32)pInfo->srcAddr[1]
                                  + pInfo->srcPitch[1]*((pInfo->srcStartY+pInfo->height)/2-1)
                                  + pInfo->srcStartX * bpp;

            pParamSet->srcBIdx    = -pInfo->srcPitch[1];
            pParamSet->destBIdx   = -pInfo->destPitch[1];

            pParamSet->aCnt       = (pInfo->width*bpp);
            pParamSet->bCnt       = pInfo->height/2;
            if (pParamSet->bCnt == 0) {
                pParamSet->bCnt = 1;
            }
            pParamSet->cCnt       = 1;

            #ifdef SYSTEM_UTILS_DMA_PARAM_CHECK
            UTILS_assert(
                Utils_dmaParamSetCheck(pParamSet) == FVID2_SOK
            );
            #endif
        }

        /* goto next tranfer information */
        pInfo++;
        numTx++;

        if(numTx>=pObj->maxTransfers)
        {
            edma3Result |= Utils_dmaRun(pObj, &numTx);
        }
    }

    if(numTx)
    {
        edma3Result |= Utils_dmaRun(pObj, &numTx);
    }

    Semaphore_post(pObj->semLock);

    return edma3Result;
}


