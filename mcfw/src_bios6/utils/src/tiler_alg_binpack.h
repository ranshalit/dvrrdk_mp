#ifndef __TILE_ALG_BINPACK_H__
#define __TILE_ALG_BINPACK_H__


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#ifdef _WIN32
#include "tile_alg_mcfw_stubs.h"
#endif

#define TILER_ALLOC_RESOLUTION_CLASS_16MP_WIDTH                        UTILS_ENCDEC_GET_PADDED_WIDTH(4u*1024u)
#define TILER_ALLOC_RESOLUTION_CLASS_16MP_HEIGHT                       UTILS_ENCDEC_GET_PADDED_HEIGHT(4u*1024u)

#define TILER_ALLOC_RESOLUTION_CLASS_9MP_WIDTH                         UTILS_ENCDEC_GET_PADDED_WIDTH(3u*1024u)
#define TILER_ALLOC_RESOLUTION_CLASS_9MP_HEIGHT                        UTILS_ENCDEC_GET_PADDED_HEIGHT(3u*1024u)

#define TILER_ALLOC_RESOLUTION_CLASS_5MP_WIDTH                         UTILS_ENCDEC_GET_PADDED_WIDTH(2560u)
#define TILER_ALLOC_RESOLUTION_CLASS_5MP_HEIGHT                        UTILS_ENCDEC_GET_PADDED_HEIGHT(1920u)

#define TILER_ALLOC_RESOLUTION_CLASS_4MP_WIDTH                         UTILS_ENCDEC_GET_PADDED_WIDTH(2u*1024u)
#define TILER_ALLOC_RESOLUTION_CLASS_4MP_HEIGHT                        UTILS_ENCDEC_GET_PADDED_HEIGHT(2u*1024u)

#define TILER_ALLOC_RESOLUTION_CLASS_1080P_WIDTH                       UTILS_ENCDEC_GET_PADDED_WIDTH(1920u)
#define TILER_ALLOC_RESOLUTION_CLASS_1080P_HEIGHT                      UTILS_ENCDEC_GET_PADDED_HEIGHT(1080u)

#define TILER_ALLOC_RESOLUTION_CLASS_720P_WIDTH                        UTILS_ENCDEC_GET_PADDED_WIDTH(1280u)
#define TILER_ALLOC_RESOLUTION_CLASS_720P_HEIGHT                       UTILS_ENCDEC_GET_PADDED_HEIGHT(720u)

#define TILER_ALLOC_RESOLUTION_CLASS_D1_WIDTH                          UTILS_ENCDEC_GET_PADDED_WIDTH(720u)
#define TILER_ALLOC_RESOLUTION_CLASS_D1_HEIGHT                         UTILS_ENCDEC_GET_PADDED_HEIGHT(576u)

#define TILER_ALLOC_RESOLUTION_CLASS_CIF_WIDTH                         UTILS_ENCDEC_GET_PADDED_WIDTH(368u)
#define TILER_ALLOC_RESOLUTION_CLASS_CIF_HEIGHT                        UTILS_ENCDEC_GET_PADDED_HEIGHT(288u)

#define TILER_ALLOC_RESOLUTION_CLASS_MAX_WIDTH                         (TILER_ALLOC_RESOLUTION_CLASS_16MP_WIDTH)
#define TILER_ALLOC_RESOLUTION_CLASS_MAX_HEIGHT                        (TILER_ALLOC_RESOLUTION_CLASS_16MP_HEIGHT)

#define TILER_ALLOC_RESOLUTION_CLASS_MIN_WIDTH                         (TILER_ALLOC_RESOLUTION_CLASS_CIF_WIDTH)
#define TILER_ALLOC_RESOLUTION_CLASS_MIN_HEIGHT                        (TILER_ALLOC_RESOLUTION_CLASS_CIF_HEIGHT)

typedef enum TilerAllocContainerIndex_e 
{
   TILER_ALLOC_NUM_CONTAINERS              =                       2u,
   TILER_ALLOC_CONTAINER_INDEX_8BIT        =                       0u,
   TILER_ALLOC_CONTAINER_INDEX_16BIT       =                       1u,
} TilerAllocContainerIndex_e;

#define TILE8_MAX_WIDTH                                                (16u*KB)
#define TILE8_MAX_HEIGHT                                               (8u*KB)

#define TILE16_MAX_WIDTH                                               (32u*KB)
#define TILE16_MAX_HEIGHT                                              (4u*KB)

#define TILE16_WIDTH_DIVISION_FACTOR                                    (1)
#define TILE16_HEIGHT_DIVISION_FACTOR                                   (2)

#define TILER_ALLOC_BUCKET_WIDTH_DEFAULT                                (TILER_ALLOC_RESOLUTION_CLASS_1080P_WIDTH)
#define TILER_ALLOC_BUCKET_HEIGHT_DEFAULT                               (TILER_ALLOC_RESOLUTION_CLASS_720P_HEIGHT * 2)

typedef struct TilerAlloc_ResolutionClassInfo_s
{
    EncDec_ResolutionClass resClass;
    UInt32 width;
    UInt32 height;
} TilerAlloc_ResolutionClassInfo_s;


#define TILER_ALLOC_STATS_MAX_WIDTH_RECT_INDEX  (0)
#define TILER_ALLOC_STATS_MAX_HEIGHT_RECT_INDEX (1)
#define TILER_ALLOC_STATS_MAX_RECT_COUNT        (2)

typedef struct TilerAllocStats_t {
    UInt32 containerWidth;
    UInt32 containerHeight;
    UInt32 bucketWidth;
    UInt32 bucketHeight;
    UInt32 numFreeBuckets;
    UInt32 numUsedBuckets;
    UInt32 totalAreaFreeBuckets;
    UInt32 totalAreaUsedBuckets;
    UInt32 totalAreaWasted;
    struct TilerAllocStatsRect_s {
        UInt32 width;
        UInt32 height;
    } maxRect[TILER_ALLOC_STATS_MAX_RECT_COUNT];
    UInt32 resAllocCount[UTILS_ENCDEC_RESOLUTION_CLASS_COUNT];
} TilerAllocStats_t;


Int32 Utils_tilerAllocatorBinPackInit();
Int32 Utils_tilerAllocatorBinPackDeInit();
void Utils_tilerAllocatorBinPackSetBucketGeometry(UInt32 bucketWidth,UInt32 bucketHeight,EncDec_ResolutionClass minRes);
Int32 Utils_tilerAllocatorBinPackFree(UInt32 tileAddr);
UInt32 Utils_tilerAllocatorBinPackAlloc(UInt32 cntMode, UInt32 width, UInt32 height);
Int32 Utils_tilerAllocatorBinPackGetStats(UInt32 cntMode,TilerAllocStats_t *stats);
void Utils_tilerAllocatorBinPackSetSingleBucketGeometry(EncDec_ResolutionClass minRes);
Int32 Utils_tilerAllocatorBinPackFreeAll();

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
