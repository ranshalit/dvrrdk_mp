#ifndef __TILE_ALG_BINPACK_PRIV_H__
#define __TILE_ALG_BINPACK_PRIV_H__


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#ifdef _WIN32
#include "tile_alg_mcfw_stubs.h"
#endif

#include "tiler_alg_binpack.h"

#ifdef MAX
#undef MAX
#endif

#define MAX(a,b)                                                       ((a) > (b) ? (a) : (b))

#ifdef MIN
#undef MIN
#endif

#define MIN(a,b)                                                       ((a) < (b) ? (a) : (b))



#define TILER_ALLOC_BUCKET_WIDTH_MIN_8BIT                               (TILER_ALLOC_BUCKET_WIDTH_DEFAULT)
#define TILER_ALLOC_BUCKET_HEIGHT_MIN_8BIT                              (TILER_ALLOC_BUCKET_HEIGHT_DEFAULT)

#define TILER_ALLOC_BUCKET_WIDTH_MIN_16BIT                              ((TILER_ALLOC_BUCKET_WIDTH_MIN_8BIT)/TILE16_WIDTH_DIVISION_FACTOR)
#define TILER_ALLOC_BUCKET_HEIGHT_MIN_16BIT                             ((TILER_ALLOC_BUCKET_HEIGHT_MIN_8BIT)/TILE16_HEIGHT_DIVISION_FACTOR)

#define TILER_ALLOC_BUCKET_WIDTH_MAX_8BIT                               (TILE8_MAX_WIDTH)
#define TILER_ALLOC_BUCKET_HEIGHT_MAX_8BIT                              (TILE8_MAX_HEIGHT)

#define TILER_ALLOC_BUCKET_WIDTH_MAX_16BIT                              (TILE16_MAX_WIDTH)
#define TILER_ALLOC_BUCKET_HEIGHT_MAX_16BIT                             (TILE16_MAX_HEIGHT)


#define TILER_ALLOC_BUCKET_MAX_NUM_COLS_8BIT                            ((TILE8_MAX_WIDTH/TILER_ALLOC_BUCKET_WIDTH_MIN_8BIT) + 1)
#define TILER_ALLOC_BUCKET_MAX_NUM_ROWS_8BIT                            ((TILE8_MAX_HEIGHT/TILER_ALLOC_BUCKET_HEIGHT_MIN_8BIT) + 1)
#define TILER_ALLOC_BUCKET_MAX_NUM_COLS_16BIT                           ((TILE16_MAX_WIDTH/TILER_ALLOC_BUCKET_WIDTH_MIN_16BIT) + 1)
#define TILER_ALLOC_BUCKET_MAX_NUM_ROWS_16BIT                           ((TILE16_MAX_HEIGHT/TILER_ALLOC_BUCKET_HEIGHT_MIN_16BIT) + 1)
#define TILER_ALLOC_MAX_NUM_BUCKETS_8BIT                                (TILER_ALLOC_BUCKET_MAX_NUM_COLS_8BIT * TILER_ALLOC_BUCKET_MAX_NUM_ROWS_8BIT)
#define TILER_ALLOC_MAX_NUM_BUCKETS_16BIT                               (TILER_ALLOC_BUCKET_MAX_NUM_COLS_16BIT * TILER_ALLOC_BUCKET_MAX_NUM_ROWS_16BIT)
#define TILER_ALLOC_MAX_NUM_BUCKETS                                     (MAX(TILER_ALLOC_MAX_NUM_BUCKETS_8BIT,TILER_ALLOC_MAX_NUM_BUCKETS_16BIT))

#define TILER_ALLOC_NODES_MAX_NUM_COLS_IN_BUCKET_8BIT                   ((TILER_ALLOC_BUCKET_WIDTH_MIN_8BIT/TILER_ALLOC_RESOLUTION_CLASS_MIN_WIDTH) + 1)
#define TILER_ALLOC_NODES_MAX_NUM_ROWS_IN_BUCKET_8BIT                   ((TILER_ALLOC_BUCKET_HEIGHT_MIN_8BIT/TILER_ALLOC_RESOLUTION_CLASS_MIN_HEIGHT) + 1)
#define TILER_ALLOC_NODES_MAX_NUM_NODES_IN_BUCKET_8BIT                  (TILER_ALLOC_NODES_MAX_NUM_COLS_IN_BUCKET_8BIT * TILER_ALLOC_NODES_MAX_NUM_ROWS_IN_BUCKET_8BIT)

#define TILER_ALLOC_NODES_MAX_NUM_COLS_IN_BUCKET_16BIT                  ((TILER_ALLOC_BUCKET_WIDTH_MIN_16BIT/(TILER_ALLOC_RESOLUTION_CLASS_MIN_WIDTH/TILE16_WIDTH_DIVISION_FACTOR)) + 1)
#define TILER_ALLOC_NODES_MAX_NUM_ROWS_IN_BUCKET_16BIT                  ((TILER_ALLOC_BUCKET_HEIGHT_MIN_16BIT/(TILER_ALLOC_RESOLUTION_CLASS_MIN_HEIGHT/TILE16_HEIGHT_DIVISION_FACTOR)) + 1)
#define TILER_ALLOC_NODES_MAX_NUM_NODES_IN_BUCKET_16BIT                 (TILER_ALLOC_NODES_MAX_NUM_COLS_IN_BUCKET_16BIT * TILER_ALLOC_NODES_MAX_NUM_ROWS_IN_BUCKET_16BIT)

#define TILER_ALLOC_NODES_MAX_NUM_NODES_IN_BUCKET                       (128)

#define TILER_ALLOC_NODES_MAX_NUM_NODES_8BIT                            (TILER_ALLOC_NODES_MAX_NUM_NODES_IN_BUCKET_8BIT * TILER_ALLOC_MAX_NUM_BUCKETS_8BIT)
#define TILER_ALLOC_NODES_MAX_NUM_NODES_16BIT                           (TILER_ALLOC_NODES_MAX_NUM_NODES_IN_BUCKET_16BIT * TILER_ALLOC_MAX_NUM_BUCKETS_16BIT)
#define TILER_ALLOC_NODES_MAX_NUM_NODES                                 (TILER_ALLOC_NODES_MAX_NUM_NODES_8BIT + TILER_ALLOC_NODES_MAX_NUM_NODES_16BIT)

#define TILER_ALLOC_INVALID_IDX                                         (~0u)

#define TILER_ALLOC_WIDTH_ALIGN                                         ((((VPS_BUFFER_ALIGNMENT * 2) + 7) / 8) * 8)
#define TILER_ALLOC_HEIGHT_ALIGN                                        (4)

#define TILER_ALLOC_NULL_ADDRESS_WORKAROUND_8BIT_STARTY                 (TILER_ALLOC_HEIGHT_ALIGN)

typedef struct TileAllocNode_t {
    Bool   isUsed;
    UInt32 startX;
    UInt32 startY;
    UInt32 width;
    UInt32 height;
    struct TileAllocNode_t *right;
    struct TileAllocNode_t *down;
    struct TileAllocNode_t *parent;
    EncDec_ResolutionClass resClass;
} TileAllocNode_t;

typedef struct TilerAllocBucketStatsFxnPrm_s
{
    UInt32 totalFreeArea;
    UInt32 totalWasteArea;
    TileAllocNode_t *maxWidthFreeNode;
    TileAllocNode_t *maxHeightFreeNode;
} TilerAllocBucketStatsFxnPrm_s;

typedef struct TilerAllocBucketFreedNodeTbl_s
{
   UInt32            numNodes;
   TileAllocNode_t  *nodes[TILER_ALLOC_NODES_MAX_NUM_NODES_IN_BUCKET]; 
} TilerAllocBucketFreedNodeTbl;

typedef struct TileAllocBucket_t {
    Bool             isValid;
    TileAllocNode_t *head;
    UInt32           bucketStartX;
    UInt32           bucketStartY;
    UInt32           bucketWidth;
    UInt32           bucketHeight;
    UInt32           numAllocResolutions;
    EncDec_ResolutionClass allocResolution[UTILS_ENCDEC_RESOLUTION_CLASS_COUNT];
    UInt32           resAllocCount[UTILS_ENCDEC_RESOLUTION_CLASS_COUNT];
    Utils_QueHandle  nodeTreeTraverseQ;
    Ptr              nodeTreeTraverseQMem[TILER_ALLOC_NODES_MAX_NUM_NODES_IN_BUCKET];
    TilerAllocBucketFreedNodeTbl freedNodes;
    UInt32           numAllocatedNodes;
    TilerAllocBucketStatsFxnPrm_s freeStats;
    UInt32           cntMode;
    UInt32           numNodesInTree;
} TileAllocBucket_t;

typedef struct TileAllocNodeTableEntry_t {
    TileAllocNode_t    node;
    UInt32             nextIdx;
} TileAllocNodeTableEntry_t;

typedef struct TileAllocNodeTable_t 
{
    Bool    initDone;
    UInt32  headIdx;
    UInt32  numAllocNodes;
    TileAllocNodeTableEntry_t entry[TILER_ALLOC_NODES_MAX_NUM_NODES];
} TileAllocNodeTable_t;

typedef struct TileAllocConfig_t {
   Bool                   initDone;
   Bool                   allocatorDisabled;
   Bool                   enableLog;
   EncDec_ResolutionClass minAllocResolution;
   Bool                   forceFreeAllDone;
   UInt32             startOffset[TILER_ALLOC_NUM_CONTAINERS];
   UInt32             containerWidth[TILER_ALLOC_NUM_CONTAINERS];
   UInt32             containerHeight[TILER_ALLOC_NUM_CONTAINERS];
   TileAllocBucket_t  TileAllocBucket[TILER_ALLOC_NUM_CONTAINERS][TILER_ALLOC_MAX_NUM_BUCKETS];
   TileAllocNodeTable_t TileAllocNodeTable;
} TileAllocConfig_t;


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

