#ifndef _UTILS_GPIO_H
#define _UTILS_GPIO_H

#include <mcfw/src_bios6/utils/utils.h>

#define GPIO_BANK_0 (0)
#define GPIO_BANK_1 (1)

#define GPIO_LOW    (0)
#define GPIO_HIGH   (1)

void Utils_gpioInit(
        UInt32 gpioBank, 
        UInt32 gpioNum, 
        Bool   isOutput, 
        UInt32 initValue);
void Utils_gpioSet(UInt32 gpioBank, UInt32 gpioNum, UInt32 value);
UInt32 Utils_gpioGet(UInt32 gpioBank, UInt32 gpioNum);

#endif  // _UTILS_GPIO_H

