/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/


/** ========================================================================== */
/** Include Files */
/** ========================================================================== */

#include "system_priv_m3vpss.h"
#include <mcfw/interfaces/common_def/ti_vsys_common_def.h>


Int32 System_displayUnderflowPrint(Bool runTimePrint, Bool clearAll)
{
    if (runTimePrint)
    {
        Vps_rprintf("\n %u: DISPLAY: UNDERFLOW COUNT: HDMI(BP0) %u, HDDAC(BP0) %d, DVO2(BP1) %u, SDDAC(SEC1) %u \n",
            Utils_getCurTimeInMsec(), gSystem_objVpss.displayUnderflowCount[0],
            gSystem_objVpss.displayUnderflowCount[1],
            gSystem_objVpss.displayUnderflowCount[2],
            gSystem_objVpss.displayUnderflowCount[3]);
    }
    else
    {
        Vps_printf("\n %u: DISPLAY: UNDERFLOW COUNT: HDMI(BP0) %d, HDDAC(BP0) %d, DVO2(BP1) %d, SDDAC(SEC1) %d \n",
            Utils_getCurTimeInMsec(), gSystem_objVpss.displayUnderflowCount[0],
            gSystem_objVpss.displayUnderflowCount[1],
            gSystem_objVpss.displayUnderflowCount[2],
            gSystem_objVpss.displayUnderflowCount[3]);
    }

    if (clearAll)
    {
        System_displayUnderflowCheck(TRUE);
    }

    return FVID2_SOK;
}

Int32 VpsHal_vpsClkcModuleCountUnderFlow(Bool clearAll,
                                         UInt32 * vencUnderflowCounter);

Int32 System_displayUnderflowCheck(Bool clearAll)
{
    if (clearAll)
    {
        gSystem_objVpss.displayUnderflowCount[0] = 0;
        gSystem_objVpss.displayUnderflowCount[1] = 0;
        gSystem_objVpss.displayUnderflowCount[2] = 0;
        gSystem_objVpss.displayUnderflowCount[3] = 0;
    }
    VpsHal_vpsClkcModuleCountUnderFlow(clearAll,
                                       gSystem_objVpss.displayUnderflowCount);

    return FVID2_SOK;
}

/**
 *  System_dispSetPixClk
 *  Configure Pixel Clock.
 */
Int32 System_dispSetPixClk()
{
    Int32 retVal = FVID2_SOK;

    gSystem_objVpss.systemDrvHandle = FVID2_create(FVID2_VPS_VID_SYSTEM_DRV,
                                                   0, NULL, NULL, NULL);
    if (NULL == gSystem_objVpss.systemDrvHandle)
    {
        Vps_printf("%s: Error %d @ line %d\n", __FUNCTION__, retVal, __LINE__);
        return (FVID2_EFAIL);
    }

    gSystem_objVpss.vpllCfg[SYSTEM_VPLL_OUTPUT_VENC_D].outputVenc = VPS_SYSTEM_VPLL_OUTPUT_VENC_D;
    retVal = FVID2_control(gSystem_objVpss.systemDrvHandle,
                           IOCTL_VPS_VID_SYSTEM_SET_VIDEO_PLL,
                           &(gSystem_objVpss.vpllCfg[SYSTEM_VPLL_OUTPUT_VENC_D]), NULL);
    if (FVID2_SOK != retVal)
    {
        Vps_printf("%s: Error %d @ line %d\n", __FUNCTION__, retVal, __LINE__);
        return (retVal);
    }

#if defined(TI_816X_BUILD)
    gSystem_objVpss.vpllCfg[SYSTEM_VPLL_OUTPUT_VENC_A].outputVenc = VPS_SYSTEM_VPLL_OUTPUT_VENC_A;
    retVal = FVID2_control(gSystem_objVpss.systemDrvHandle,
                           IOCTL_VPS_VID_SYSTEM_SET_VIDEO_PLL,
                           &(gSystem_objVpss.vpllCfg[SYSTEM_VPLL_OUTPUT_VENC_A]), NULL);
    if (FVID2_SOK != retVal)
    {
        Vps_printf("%s: Error %d @ line %d\n", __FUNCTION__, retVal, __LINE__);
        return (retVal);
    }
#endif

    FVID2_delete(gSystem_objVpss.systemDrvHandle, NULL);

    return (FVID2_SOK);
}

/**
 *  System_dispCheckStopList
 *  Frees the VPDMA lists that are busy.
 */
Int32 System_dispCheckStopList()
{
    volatile UInt32 status = 0;
    volatile UInt32 retryCount = 60;

    /*[BOOTSCREEN] Entry: This block of code is to check the VPDMA lists,
        and wait until VPDMA list becomes NOT busy.
        This is required since a VPDMA list is started during Boot Time Splash Screen
    */
    if((*(UInt32 *)VPS_MODULE_CLK) == 2)
    {
        /* VPSS is powered ON */
        do
        {
            status = *(volatile UInt32 *)VPDMA_LIST_STAT_SYNC & 0x00FF0000;

            if( status == 0 )
            {
                /* all lists are free */
                break;
            }

            Vps_printf(" %d: SYSTEM: VPDMA is Busy (0x%08x) !!!\n",
                        Utils_getCurTimeInMsec(), status);
            Task_sleep(1000);
        }while(retryCount--);

        status = *(volatile UInt32 *)VPDMA_LIST_STAT_SYNC & 0x00FF0000;
        if( status == 0 )
        {
            Vps_printf(" %d: SYSTEM: All VPDMA Free !!! \n",
                    Utils_getCurTimeInMsec());
        }
        else
        {
            Vps_printf(" %d: SYSTEM: VPDMA is Busy (0x%08x) !!! ---> ERROR !!!!\n",
                        Utils_getCurTimeInMsec(), status);
        }
    }
    /*[BOOTSCREEN] Exit */

    return FVID2_SOK;
}
