/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include "swMsLink_priv.h"
#include "mcfw/src_bios6/links_m3vpss/system/system_priv_m3vpss.h"
#include <mcfw/interfaces/common_def/ti_vsys_common_def.h>

static Bool SwMsLink_drvIsOverlapLayout(SwMsLink_Obj *pObj);
static
Int32 SwMsLink_freeFrame(System_LinkInQueParams * pInQueParams,
                         FVID2_FrameList * frameList, FVID2_Frame * pFrame);
static Int32 SwMsLink_creatChanToInstMap(SwMsLink_Obj * pObj);
static Int32 SwMsLink_updateWinIdToScInstLChMap(SwMsLink_Obj * pObj);

Bool  waitingOnDriverCbSWMS[SYSTEM_SW_MS_MAX_INST] = {FALSE};
#define SWMSLINK_DEBUG_BLANK_OUTPUT_BUFFER                              (FALSE)

/*
This function is to fill blank data for a blank input frame
When a CH input frame is not available or a CH is disabled from display
then this blank frame is used as input

The same blank frame is used for all channels.

This initial color of the blank frame is filled inside this function
using CPU memcopy.

This works ONLY on YUV422I input format.
So that the CPU load during blank frame fill
and DDR BW during blank frame read is not high the blank frame
size should be small typically < 400x300

Asserts are put in the function to check these conditions.
*/
static
Int32 SwMsLink_fillBlankDataPattern(FVID2_Format * pFormat,
                               FVID2_Frame * pFrame)
{
    /* using memcpy to make blank frame buffer */
    UInt32 w, h, fillColor32;
    UInt32 *pAddr;

    if(pFormat->height*pFormat->width > 400*300)
    {
        Vps_printf(" %d: SWMS: Blank buffer size of %d x %d is too big !!!\n",
                Utils_getCurTimeInMsec(),
                pFormat->width,
                pFormat->height
            );
        UTILS_assert(0);
    }

    fillColor32 = UTILS_DMA_GENERATE_FILL_PATTERN(0x00, 0x80, 0x80);

    if (SYSTEM_DF_YUV422I_YUYV == pFormat->dataFormat)
    {
        for(h=0; h<pFormat->height; h++)
        {
            pAddr = (UInt32*)((UInt32)pFrame->addr[0][0] + h*pFormat->pitch[0]);
            for(w=0; w<pFormat->width; w+=2) /* +2 since data format is YUV422I */
            {
               *pAddr = fillColor32;

               pAddr++;
            }
        }
    }
    else if ((SYSTEM_DF_YUV420SP_UV == pFormat->dataFormat) ||
             (SYSTEM_DF_YUV420SP_VU == pFormat->dataFormat))
    {
        UInt16 value, *addr;
        for(h=0; h<pFormat->height; h++)
        {
            addr = (UInt16*)((UInt32)pFrame->addr[0][0] + h*pFormat->pitch[0]);
            value = (fillColor32 & 0xFF) | ((fillColor32 & 0xFF0000) >> 8);

            for(w=0; w<pFormat->width; w+=2) /* +2 since data format is YUV422I */
            {
                *addr = value;
                addr ++;
            }
        }
        for(h=0; h<pFormat->height/2; h++)
        {
            addr = (UInt16*)((UInt32)pFrame->addr[0][1] + h*pFormat->pitch[0]);
            value = ((fillColor32 & 0xFF00) >> 8) | ((fillColor32 & 0xFF000000) >> 16);

            for(w=0; w<pFormat->width; w+=2) /* +2 since data format is YUV422I */
            {
                *addr = value;
                addr ++;
            }
        }
    }
    else /* YUV422 SP Format */
    {
        UInt16 value, *addr;
        for(h=0; h<pFormat->height; h++)
        {
            addr = (UInt16*)((UInt32)pFrame->addr[0][0] + h*pFormat->pitch[0]);
            value = (fillColor32 & 0xFF) | ((fillColor32 & 0xFF0000) >> 8);

            for(w=0; w<pFormat->width; w+=2) /* +2 since data format is YUV422I */
            {
                *addr = value;
                addr ++;
            }
        }
        for(h=0; h<pFormat->height; h++)
        {
            addr = (UInt16*)((UInt32)pFrame->addr[0][1] + h*pFormat->pitch[0]);
            value = ((fillColor32 & 0xFF00) >> 8) | ((fillColor32 & 0xFF000000) >> 16);

            for(w=0; w<pFormat->width; w+=2) /* +2 since data format is YUV422I */
            {
                *addr = value;
                addr ++;
            }
        }
    }


    return FVID2_SOK;
}

static
Void  SwMsLink_drvSetBlankOutputFlag(SwMsLink_Obj * pObj)
{
    Int frameId;

    for (frameId = 0; frameId < UTILS_ARRAYSIZE(pObj->outFrameInfo);
         frameId++)
    {
        pObj->outFrameInfo[frameId].swMsBlankOutBuf =
            SWMS_LINK_DO_OUTBUF_BLANKING;
    }
}

static
Int32 SwMsLink_drvDmaCreate(SwMsLink_Obj * pObj)
{
    Int32 status;

    status = Utils_dmaCreateCh(&pObj->dmaObj,
                               UTILS_DMA_DEFAULT_EVENT_Q,
                               1, TRUE);
    UTILS_assert(status==FVID2_SOK);

    if(pObj->createArgs.enableLayoutGridDraw == TRUE)
    {
        status = Utils_dmaCreateCh(&pObj->gridDmaObj,
                                   UTILS_DMA_DEFAULT_EVENT_Q,
                                   SWMS_LINK_DMA_GRID_MAX_TRANSFERS, TRUE);
        UTILS_assert(status==FVID2_SOK);
    }

    return status;
}

static
Int32 SwMsLink_drvDmaDelete(SwMsLink_Obj * pObj)
{
    Int32 status;

    status = Utils_dmaDeleteCh(&pObj->dmaObj);
    UTILS_assert(status==FVID2_SOK);

    if(pObj->createArgs.enableLayoutGridDraw == TRUE)
    {
        status = Utils_dmaDeleteCh(&pObj->gridDmaObj);
        UTILS_assert(status==FVID2_SOK);
    }

    return status;
}

Int32 SwMsLink_drvDoDma(SwMsLink_Obj * pObj, FVID2_Frame *pFrame)
{
    Int32 status = 0,i;
    Utils_DmaFill2D lineInfo[SWMS_LINK_DMA_GRID_MAX_TRANSFERS];

    UInt16 thickness, numTx;

    UInt32 pitch;

    thickness = 4;

    pitch = pObj->outFrameFormat.pitch[0];

    numTx=0;

    for(i=0;i<pObj->layoutParams.numWin;i++)
    {
        /*Horizontal Top line*/
        lineInfo[numTx].destAddr[0] = pFrame->addr[0][0];
        lineInfo[numTx].destPitch[0] = pitch;
        if (pObj->createArgs.outDataFormat == SYSTEM_DF_YUV420SP_UV)
        {
            lineInfo[numTx].destAddr[1] = pFrame->addr[0][1];
            lineInfo[numTx].destPitch[1] = pObj->outFrameFormat.pitch[1];
        }
        lineInfo[numTx].dataFormat  = pObj->createArgs.outDataFormat;
        lineInfo[numTx].width = pObj->layoutParams.winInfo[i].width;
        lineInfo[numTx].height = thickness;
        lineInfo[numTx].fillColorYUYV =
            UTILS_DMA_GENERATE_FILL_PATTERN(SW_MS_GRID_FILL_PIXEL_LUMA ,
                                            SW_MS_GRID_FILL_PIXEL_CHROMA,
                                            SW_MS_GRID_FILL_PIXEL_CHROMA);

        lineInfo[numTx].fastFillColorIndex = UTILS_DMA_FAST_FILL_COLOR_WHITE;

        lineInfo[numTx].startX = pObj->layoutParams.winInfo[i].startX;
        lineInfo[numTx].startY = pObj->layoutParams.winInfo[i].startY;
        numTx++;

        /*Horizontal Bottom line*/
        lineInfo[numTx].destAddr[0] = pFrame->addr[0][0];
        lineInfo[numTx].destPitch[0] = pitch;
        lineInfo[numTx].dataFormat  = pObj->createArgs.outDataFormat;
        if (pObj->createArgs.outDataFormat == SYSTEM_DF_YUV420SP_UV)
        {
            lineInfo[numTx].destAddr[1] = pFrame->addr[0][1];
            lineInfo[numTx].destPitch[1] = pObj->outFrameFormat.pitch[1];
        }
        lineInfo[numTx].width = pObj->layoutParams.winInfo[i].width;
        lineInfo[numTx].height = thickness;
        lineInfo[numTx].fillColorYUYV =
            UTILS_DMA_GENERATE_FILL_PATTERN(SW_MS_GRID_FILL_PIXEL_LUMA ,
                                            SW_MS_GRID_FILL_PIXEL_CHROMA,
                                            SW_MS_GRID_FILL_PIXEL_CHROMA);

        lineInfo[numTx].fastFillColorIndex = UTILS_DMA_FAST_FILL_COLOR_WHITE;

        lineInfo[numTx].startX = pObj->layoutParams.winInfo[i].startX;
        lineInfo[numTx].startY = pObj->layoutParams.winInfo[i].startY +
        pObj->layoutParams.winInfo[i].height - thickness;
        numTx++;

        /*Vertical Left side*/
        lineInfo[numTx].destAddr[0] = pFrame->addr[0][0];
        lineInfo[numTx].destPitch[0] = pitch;
        lineInfo[numTx].dataFormat  = pObj->createArgs.outDataFormat;
        if (pObj->createArgs.outDataFormat == SYSTEM_DF_YUV420SP_UV)
        {
            lineInfo[numTx].destAddr[1] = pFrame->addr[0][1];
            lineInfo[numTx].destPitch[1] = pObj->outFrameFormat.pitch[1];
        }
        lineInfo[numTx].width = thickness;
        lineInfo[numTx].height = pObj->layoutParams.winInfo[i].height;
        lineInfo[numTx].fillColorYUYV =
            UTILS_DMA_GENERATE_FILL_PATTERN(SW_MS_GRID_FILL_PIXEL_LUMA ,
                                            SW_MS_GRID_FILL_PIXEL_CHROMA,
                                            SW_MS_GRID_FILL_PIXEL_CHROMA);

        lineInfo[numTx].fastFillColorIndex = UTILS_DMA_FAST_FILL_COLOR_WHITE;

        lineInfo[numTx].startX = pObj->layoutParams.winInfo[i].startX +
        pObj->layoutParams.winInfo[i].width - thickness;
        lineInfo[numTx].startY = pObj->layoutParams.winInfo[i].startY;
        numTx++;

        /*Vertical right side*/
        lineInfo[numTx].destAddr[0] = pFrame->addr[0][0];
        lineInfo[numTx].destPitch[0] = pitch;
        lineInfo[numTx].dataFormat  = pObj->createArgs.outDataFormat;
        if (pObj->createArgs.outDataFormat == SYSTEM_DF_YUV420SP_UV)
        {
            lineInfo[numTx].destAddr[1] = pFrame->addr[0][1];
            lineInfo[numTx].destPitch[1] = pObj->outFrameFormat.pitch[1];
        }
        lineInfo[numTx].width = thickness;
        lineInfo[numTx].height =  pObj->layoutParams.winInfo[i].height;
        lineInfo[numTx].fillColorYUYV =
            UTILS_DMA_GENERATE_FILL_PATTERN(SW_MS_GRID_FILL_PIXEL_LUMA ,
                                            SW_MS_GRID_FILL_PIXEL_CHROMA,
                                            SW_MS_GRID_FILL_PIXEL_CHROMA);

        lineInfo[numTx].fastFillColorIndex = UTILS_DMA_FAST_FILL_COLOR_WHITE;

        lineInfo[numTx].startX = pObj->layoutParams.winInfo[i].startX;
        lineInfo[numTx].startY = pObj->layoutParams.winInfo[i].startY;
        numTx++;

        UTILS_assert(numTx<=SWMS_LINK_DMA_GRID_MAX_TRANSFERS);

        status = Utils_dmaFastFill2D(&pObj->gridDmaObj, lineInfo, numTx);
        numTx = 0;
    }
    return status;
}

static
Int32 SwMsLink_drvCheckBlankOutputBuffer(SwMsLink_Obj * pObj, FVID2_Frame *pFrame)
{
    Int32 status = 0;
    System_FrameInfo *frameInfo = pFrame->appData;

    if ((NULL != frameInfo)
        &&
        (frameInfo->swMsBlankOutBuf))
    {
        Utils_DmaFill2D blankFrameInfo[SWMS_LINK_DMA_MAX_TRANSFERS];
        UInt16  numTx;
        UInt32 pitch;

        pitch = pObj->outFrameFormat.pitch[0];

        numTx=0;

        blankFrameInfo[numTx].destAddr[0] = pFrame->addr[0][0];
        blankFrameInfo[numTx].destPitch[0] = pitch;
        blankFrameInfo[numTx].dataFormat  = pObj->createArgs.outDataFormat;
        if (pObj->createArgs.outDataFormat == SYSTEM_DF_YUV420SP_UV)
        {
            blankFrameInfo[numTx].destAddr[1] = pFrame->addr[0][1];
            blankFrameInfo[numTx].destPitch[1] = pObj->outFrameFormat.pitch[1];
        }
        blankFrameInfo[numTx].width = pObj->outFrameFormat.width;
        blankFrameInfo[numTx].height = pObj->outFrameFormat.height;
        blankFrameInfo[numTx].fillColorYUYV =
                UTILS_DMA_GENERATE_FILL_PATTERN(SW_MS_BLANK_FRAME_PIXEL_LUMA ,
                                                SW_MS_BLANK_FRAME_PIXEL_CHROMA,
                                                SW_MS_BLANK_FRAME_PIXEL_CHROMA);

        blankFrameInfo[numTx].fastFillColorIndex = SW_MS_BLANK_FRAME_FAST_FILL_PIXEL_COLOR;

        blankFrameInfo[numTx].startX = 0;
        blankFrameInfo[numTx].startY = 0;
        #if SWMSLINK_DEBUG_BLANK_OUTPUT_BUFFER
            Vps_printf("%d:SWMS: Start Blanking of output buffer: "
                       "BufAddr:[%p] Width:[%d] Height:[%d] Pitch:[%d] Pixel:[0x%X / %d]",
                       Utils_getCurTimeInMsec(),
                       blankFrameInfo[numTx].destAddr[0],
                       blankFrameInfo[numTx].width,
                       blankFrameInfo[numTx].height,
                       blankFrameInfo[numTx].destPitch[0],
                       blankFrameInfo[numTx].fillColorYUYV,
                       blankFrameInfo[numTx].fastFillColorIndex
                    );
        #endif
        numTx++;

        UTILS_assert(numTx<=SWMS_LINK_DMA_MAX_TRANSFERS);

        status = Utils_dmaFastFill2D(&pObj->dmaObj, blankFrameInfo, numTx);

        if (UTILS_ISERROR(status))
        {
            Vps_printf("SWMS: Utils_dmaFill2D for output buffer failed!!");
        }
        else
        {
            #if SWMSLINK_DEBUG_BLANK_OUTPUT_BUFFER
                Vps_printf("%d:SWMS: End Blanking of output buffer: "
                       "BufAddr:[%p] ",
                       Utils_getCurTimeInMsec(),
                       blankFrameInfo[0].destAddr[0]);
            #endif
        }
        frameInfo->swMsBlankOutBuf = FALSE;
    }

    return status;
}

static Bool SwMsLink_drvIsVipOutputUsed(UInt32 drvInst)
{
    Bool  result = FALSE;

#ifdef TI_816X_BUILD
    if ((VPS_M2M_INST_MAIN_DEIH_SC3_VIP0 == drvInst) ||
        (VPS_M2M_INST_AUX_DEI_SC4_VIP1 == drvInst) ||
        (VPS_M2M_INST_MAIN_DEIH_SC1_SC3_WB0_VIP0 == drvInst) ||
        (VPS_M2M_INST_AUX_DEI_SC2_SC4_WB1_VIP1 == drvInst))
    {
        result = TRUE;
    }
#else
    if ((VPS_M2M_INST_MAIN_DEI_SC3_VIP0 == drvInst) ||
        (VPS_M2M_INST_MAIN_DEI_SC1_SC3_WB0_VIP0 == drvInst) ||
        (VPS_M2M_INST_AUX_SC4_VIP1 == drvInst) ||
        (VPS_M2M_INST_AUX_SC2_SC4_WB1_VIP1 == drvInst))
    {
        result = TRUE;
    }
#endif

    return (result);
}

static Bool SwMsLink_drvIsVipScOnlyOutput(UInt32 drvInst)
{
    Bool outputInVipSc = FALSE;

#ifdef TI_816X_BUILD
    if ((VPS_M2M_INST_MAIN_DEIH_SC3_VIP0 == drvInst) ||
        (VPS_M2M_INST_AUX_DEI_SC4_VIP1 == drvInst))
    {
        outputInVipSc = TRUE;
    }
#else
    if ((VPS_M2M_INST_MAIN_DEI_SC3_VIP0 == drvInst) ||
        (VPS_M2M_INST_AUX_SC4_VIP1 == drvInst))
    {
        outputInVipSc = TRUE;
    }
#endif

    return (outputInVipSc);
}

static Int32 SwMsLink_drvCreateDupObj(SwMsLink_Obj * pObj)
{
    Int32 status;
    Int i;

    memset(&pObj->dupObj, 0, sizeof(pObj->dupObj));
    status = Utils_queCreate(&pObj->dupObj.dupQue,
                             UTILS_ARRAYSIZE(pObj->dupObj.dupQueMem),
                             pObj->dupObj.dupQueMem,
                             UTILS_QUE_FLAG_BLOCK_QUE_GET);
    UTILS_assertError(!UTILS_ISERROR(status),
                      status,
                      SWMS_LINK_E_DUPOBJ_CREATE_FAILED, pObj->linkId, -1);
    if (!UTILS_ISERROR(status))
    {
        for (i = 0; i < SWMS_LINK_MAX_DUP_FRAMES; i++)
        {
            pObj->dupObj.frameInfo[i].pVdecOrgFrame = NULL;
            pObj->dupObj.frameInfo[i].vdecRefCount = 0;
            pObj->dupObj.dupFrameMem[i].appData = &(pObj->dupObj.frameInfo[i]);
            status = Utils_quePut(&pObj->dupObj.dupQue,
                                  &pObj->dupObj.dupFrameMem[i], BIOS_NO_WAIT);
            UTILS_assert(!UTILS_ISERROR(status));
        }
    }
    return status;
}

static Int32 SwMsLink_drvDeleteDupObj(SwMsLink_Obj * pObj)
{
    Int32 status;

    UTILS_assertError((Utils_queIsFull(&pObj->dupObj.dupQue) == TRUE),
                      status,
                      SWMS_LINK_E_DUPOBJ_DELETE_FAILED, pObj->linkId, -1);
    status = Utils_queDelete(&pObj->dupObj.dupQue);
    UTILS_assertError(!UTILS_ISERROR(status),
                      status,
                      SWMS_LINK_E_DUPOBJ_DELETE_FAILED, pObj->linkId, -1);
    return status;
}

static Int32 SwMsLink_drv_init_outframe(SwMsLink_Obj * pObj,
                                        FVID2_Frame * pFrame)
{
    System_FrameInfo *pFrameInfo;

    pFrameInfo = (System_FrameInfo *) pFrame->appData;
    UTILS_assert((pFrameInfo != NULL)
                 &&
                 UTILS_ARRAYISVALIDENTRY(pFrameInfo, pObj->outFrameInfo));
    pFrameInfo->vdecRefCount = 1;
    pFrameInfo->pVdecOrgFrame = NULL;
    return FVID2_SOK;
}

static Int32 SwMsLink_dupFrame(SwMsLink_Obj * pObj, FVID2_Frame * pOrgFrame,
                               FVID2_Frame ** ppDupFrame)
{
    Int status = FVID2_SOK;
    FVID2_Frame *pFrame;
    System_FrameInfo *pFrameInfo, *pOrgFrameInfo;

    status =
        Utils_queGet(&pObj->dupObj.dupQue, (Ptr *) & pFrame, 1, BIOS_NO_WAIT);
    UTILS_assert(status == FVID2_SOK);
    UTILS_assert(pFrame != NULL);
    pFrameInfo = (System_FrameInfo *) pFrame->appData;
    UTILS_assert(pFrameInfo != NULL);
    while (((System_FrameInfo *) pOrgFrame->appData)->pVdecOrgFrame != NULL)
    {
        pOrgFrame = ((System_FrameInfo *) pOrgFrame->appData)->pVdecOrgFrame;
    }
    pOrgFrameInfo = pOrgFrame->appData;
    memcpy(pFrame, pOrgFrame, sizeof(*pOrgFrame));
    pOrgFrameInfo = pOrgFrame->appData;
    memcpy(pFrameInfo, pOrgFrameInfo, sizeof(*pOrgFrameInfo));

    pFrame->appData = pFrameInfo;
    pFrameInfo->pVdecOrgFrame = pOrgFrame;
    UTILS_assert(pOrgFrameInfo->vdecRefCount <= SWMS_LINK_MAX_DUP_PER_FRAME);
    pOrgFrameInfo->vdecRefCount++;
    *ppDupFrame = pFrame;

    return status;
}

Int32 SwMsLink_drvFreeProcessedFrames(SwMsLink_Obj * pObj,
                                      FVID2_FrameList * freeFrameList)
{
    Int i, status = FVID2_SOK;
    FVID2_Frame *freeFrame;
    FVID2_Frame *origFrame;
    System_FrameInfo *freeFrameInfo;
    UInt cookie;
    UInt32 origFrameIndex;

    cookie = Hwi_disable();

    for (i = 0; i < freeFrameList->numFrames; i++)
    {
        freeFrame = freeFrameList->frames[i];
        UTILS_assert(freeFrame != NULL);
        freeFrameInfo = freeFrame->appData;
        UTILS_assert(freeFrameInfo != NULL);
        if (freeFrameInfo->pVdecOrgFrame)
        {
            UTILS_assert(UTILS_ARRAYISVALIDENTRY(freeFrame,
                                                 pObj->dupObj.dupFrameMem));
            origFrame = freeFrameInfo->pVdecOrgFrame;
            status = Utils_quePut(&pObj->dupObj.dupQue,
                                  freeFrame, BIOS_NO_WAIT);
            UTILS_assert(!UTILS_ISERROR(status));
            freeFrame = origFrame;
            origFrameIndex = UTILS_ARRAYINDEX(freeFrame,pObj->outFrames);
            UTILS_assert(origFrameIndex < UTILS_ARRAYSIZE(pObj->outFrames));
            UTILS_COMPILETIME_ASSERT(UTILS_ARRAYSIZE(pObj->outFrames) ==
                                     UTILS_ARRAYSIZE(pObj->outFrameInfo));
            freeFrameInfo = &pObj->outFrameInfo[origFrameIndex];
        }
        UTILS_assert((freeFrameInfo->pVdecOrgFrame == NULL)
                     && (freeFrameInfo->vdecRefCount > 0));
        freeFrameInfo->vdecRefCount--;
        if (freeFrameInfo->vdecRefCount == 0)
        {
            status = Utils_bufPutEmptyFrame(&pObj->bufOutQue, freeFrame);
            UTILS_assert(!UTILS_ISERROR(status));
        }
    }

    Hwi_restore(cookie);

    return status;
}

Int32 SwMsLink_drvResetStatistics(SwMsLink_Obj * pObj)
{
    UInt32 winId;

    SwMsLink_OutWinObj *pWinObj;

    for(winId=0; winId<SYSTEM_SW_MS_MAX_WIN; winId++)
    {
        pWinObj = &pObj->winObj[winId];

        pWinObj->framesRecvCount = 0;
        pWinObj->framesInvalidChCount = 0;
        pWinObj->framesRejectCount = 0;
        pWinObj->framesQueRejectCount = 0;
        pWinObj->framesQueuedCount = 0;
        pWinObj->framesRepeatCount = 0;
        pWinObj->framesAccEventCount = 0;
        pWinObj->framesAccMax = 0;
        pWinObj->framesAccMin = 0xFF;
        pWinObj->framesDroppedCount = 0;
        pWinObj->framesUsedCount = 0;
        pWinObj->framesFidInvalidCount = 0;

        pWinObj->minLatency = 0xFFFF;
        pWinObj->maxLatency = 0;
        pWinObj->framesOutBufCopyCount = 0;
        pWinObj->framesInBufCopyCount = 0;
    }
    pObj->framesOutReqCount = 0;
    pObj->framesOutDropCount = 0;
    pObj->framesOutRejectCount = 0;
    pObj->framesOutCount = 0;
    pObj->prevDoScalingTime  = 0;
    pObj->scalingInterval    = 0;
    pObj->scalingIntervalMin = SW_MS_SCALING_INTERVAL_INVALID;
    pObj->scalingIntervalMax = 0;
    pObj->statsStartTime = Utils_getCurTimeInMsec();

    return 0;
}

Int32 SwMsLink_drvResetAvgStatistics(SwMsLink_Obj * pObj)
{
    UInt32 winId;

    pObj->avgStats.framesOutCount = 0;
    for (winId = 0; winId < SYSTEM_SW_MS_MAX_WIN; winId++)
    {
        pObj->avgStats.framesUsedCount[winId] = 0;
    }

    pObj->avgStats.statsStartTime = Utils_getCurTimeInMsec();

    return 0;
}

Int32 SwMsLink_drvPrintAvgStatistics(SwMsLink_Obj * pObj)
{
    UInt32 elaspedTime;

    elaspedTime = Utils_getCurTimeInMsec() - pObj->avgStats.statsStartTime; // in msecs

    if(elaspedTime < SYSTEM_RT_STATS_LOG_INTERVAL*1000)
        return 0;

    #ifdef SYSTEM_PRINT_RT_AVG_STATS_LOG
    {
        UInt32 winId, outputFPS, totalWinFPS;

        outputFPS = (pObj->avgStats.framesOutCount*10000/elaspedTime);

        totalWinFPS = 0;
        for (winId = 0; winId < pObj->layoutParams.numWin; winId++)
        {
            totalWinFPS += pObj->avgStats.framesUsedCount[winId];
        }

        totalWinFPS = (totalWinFPS*10000)/elaspedTime;

        Vps_rprintf(" %-8s: Output  FPS: %d.%d fps , Total Window FPS: %d.%d fps ... in %d.%d secs\n",
            pObj->name,
            outputFPS/10,
            outputFPS%10,
            totalWinFPS/10,
            totalWinFPS%10,
            elaspedTime/1000,
            (elaspedTime%1000)/100
            );
    }
    #endif

    SwMsLink_drvResetAvgStatistics(pObj);


    return 0;
}


Int32 SwMsLink_drvPrintDriverStatistics(SwMsLink_Obj * pObj)
{
    Int32 i;

    Vps_printf( " \n"
            " *** [%s] Mosaic Driver Statistics *** \n",
            pObj->name
            );
    for (i = 0; i < pObj->createArgs.numSwMsInst; i++)
    {
        Vps_printf(
                "%d) *** Drv Inst [%d], handle %X, %s  %s  %s\n",
                i,
                pObj->DrvObj[i].drvInstId,
                pObj->DrvObj[i].fvidHandle,
                (pObj->DrvObj[i].isDeiDrv) ? "DEI_DRV" : "NON_DEI",
                (pObj->DrvObj[i].bypassDei == TRUE) ? "bypassDei" : " ",
                (pObj->DrvObj[i].forceBypassDei== TRUE) ? "forceBypassDei" : " "
            );
        if (pObj->DrvObj[i].isDeiDrv)
        {
            Vps_printf( 
            "DualOut Inst [%d], %s  %s  numVipStrms %d *** \n",
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId,
                (SwMsLink_drvIsVipOutputUsed(pObj->DrvObj[i].drvInstId) == TRUE)
                        ? "VipOutputUsed" : " ",
                (SwMsLink_drvIsVipScOnlyOutput(pObj->DrvObj[i].drvInstId) == TRUE)
                    ? "VipScOnlyOutput" : " ",
                pObj->DrvObj[i].cfg.dei.deiCreateParams.numVipOutStreams
            );
        }
    }
    return 0;
}


Int32 SwMsLink_drvPrintStatistics(SwMsLink_Obj * pObj, Bool resetAfterPrint)
{
    UInt32 winId;
    SwMsLink_OutWinObj *pWinObj;
    UInt32 elaspedTime;

    elaspedTime = Utils_getCurTimeInMsec() - pObj->statsStartTime; // in msecs
    elaspedTime /= 1000; // convert to secs

    SwMsLink_drvPrintDriverStatistics(pObj);

    Vps_printf( " \n"
            " *** [%s] Mosaic Statistics *** \n"
            " \n"
            " Elasped Time: %d secs\n"
            " \n"
            " Output Request FPS   : %d fps (%d frames) \n"
            " Output Actual  FPS   : %d fps (%d frames) \n"
            " Output Drop    FPS   : %d fps (%d frames) \n"
            " Output Reject  FPS   : %d fps (%d frames) \n"
            " Scaling Internal     : %d ms \n"
            " Scaling Internal min : %d ms \n"
            " Scaling Internal max : %d ms \n"
            " \n"
            " Win | Window Repeat Drop Recv Que  FID Invlid Acc Event          Invalid   Que Reject Reject Latency   OutBufCopy InBufCopy\n"
            " Num | FPS    FPS    FPS  FPS  FPS  FPS        Count (Max/Min)    CH Frames Frames     Frames Min / Max FPS        FPS      \n"
            " ---------------------------------------------------------------------------------------------------------------------------\n",
            pObj->name,
            elaspedTime,
            pObj->framesOutReqCount/elaspedTime,
            pObj->framesOutReqCount,
            pObj->framesOutCount/elaspedTime,
            pObj->framesOutCount,
            pObj->framesOutDropCount/elaspedTime,
            pObj->framesOutDropCount,
            pObj->framesOutRejectCount/elaspedTime,
            pObj->framesOutRejectCount,
            pObj->scalingInterval/pObj->framesOutReqCount,
            pObj->scalingIntervalMin,
            pObj->scalingIntervalMax
            );


    for (winId = 0; winId < pObj->layoutParams.numWin; winId++)
    {
        pWinObj = &pObj->winObj[winId];

        Vps_printf( " %3d | %6d %6d %4d %4d %4d %10d %8d (%3d/%3d) %9d %10d %6d %3d / %3d %10d %9d \n",
            winId,
            #if 1
            pWinObj->framesUsedCount/elaspedTime,
            pWinObj->framesRepeatCount/elaspedTime,
            pWinObj->framesDroppedCount/elaspedTime,
            pWinObj->framesRecvCount/elaspedTime,
            pWinObj->framesQueuedCount/elaspedTime,
            pWinObj->framesFidInvalidCount/elaspedTime,
            #else
            pWinObj->framesUsedCount,
            pWinObj->framesRepeatCount,
            pWinObj->framesDroppedCount,
            pWinObj->framesRecvCount,
            pWinObj->framesQueuedCount,
            pWinObj->framesFidInvalidCount,
            #endif
            pWinObj->framesAccEventCount,
            pWinObj->framesAccMax,
            pWinObj->framesAccMin,
            pWinObj->framesInvalidChCount,
            pWinObj->framesQueRejectCount,
            pWinObj->framesRejectCount/elaspedTime,
            pWinObj->minLatency,
            pWinObj->maxLatency,
            pWinObj->framesOutBufCopyCount/elaspedTime,
            pWinObj->framesInBufCopyCount/elaspedTime
            );
    }

    Vps_printf( " \n");

    SwMsLink_drvPrintLayoutParams(pObj);

    Vps_printf( " \n");

    if(resetAfterPrint)
    {
        SwMsLink_drvResetStatistics(pObj);
    }

    return 0;
}

Int32 SwMsLink_drvModifyFramePointer(SwMsLink_Obj * pObj, SwMsLink_DrvObj *pDrvObj,
                                         Bool addOffset)
{
    Int32 offset[2],frameId, bytesPerPixel,windId;
    System_LinkChInfo *rtChannelInfo;
    System_FrameInfo *frameInfo;

    for(frameId = 0; frameId< pDrvObj->outFrameList.numFrames; frameId++)
    {
        if(pDrvObj->inFrameList.frames[frameId]==NULL)
        {
            /* SWMS: WARNING: NULL FVID2_Frame found */
            continue;
        }
        frameInfo = (System_FrameInfo *)
                    pDrvObj->inFrameList.frames[frameId]->appData;
        if (frameInfo == NULL)
        {
            /* Blank frames will not have appdata and do not require further
                processing */
            continue;
        }
        UTILS_assert (frameInfo->swmsOrgChannelNum < SYSTEM_SW_MS_MAX_CH_ID);

        windId = pObj->layoutParams.ch2WinMap[frameInfo->swmsOrgChannelNum];

        if(windId >= SYSTEM_SW_MS_MAX_WIN)
        {
            Vps_rprintf(" ### SWMS: WARNING: "
                "Frame %d of %d: Illegal Window ID %d found !!!\n",
                 frameId, pDrvObj->inFrameList.numFrames, windId);
            continue;
        }

        if (pObj->layoutParams.winInfo[windId].channelNum <
            pObj->inQueInfo.numCh)
        {
            rtChannelInfo = &pObj->
                 rtChannelInfo[pObj->layoutParams.winInfo[windId].channelNum];

            if(rtChannelInfo->dataFormat == SYSTEM_DF_YUV422I_YUYV)
                bytesPerPixel = 2;
            else
                bytesPerPixel = 1;

            offset[0] = rtChannelInfo->pitch[0]*rtChannelInfo->startY    +
                                       rtChannelInfo->startX*bytesPerPixel;
            offset[1] = rtChannelInfo->pitch[1]*rtChannelInfo->startY/2  +
                                       rtChannelInfo->startX*bytesPerPixel;

            if(addOffset == FALSE)
            {
                offset[0] = -offset[0];
                offset[1] = -offset[1];

                /* Retore original channel number */
                pDrvObj->inFrameList.frames[frameId]->channelNum =
                    frameInfo->swmsOrgChannelNum;
            }
            else
            {
                /* Convert the input channel number to
                    SC instance logical channel number */
                pDrvObj->inFrameList.frames[frameId]->channelNum = 
                                     pDrvObj->win2ScLChMap[windId];
            }

            #ifndef SWMS_ENABLE_MODIFY_FRAME_POINTER_ALWAYS
            if ((rtChannelInfo->width + rtChannelInfo->startX) > SW_MS_MAX_WIDTH_SUPPORTED)
            #endif
            {
                /* when appData is NULL frame is blank frame in this case dont update address */
               if(pDrvObj->inFrameList.frames[frameId]->appData!=NULL)
               {
                    pDrvObj->inFrameList.frames[frameId]->addr[0][0] =
                      (Ptr) ((Int32)pDrvObj->inFrameList.frames[frameId]->addr[0][0] + offset[0]);
                   pDrvObj->inFrameList.frames[frameId]->addr[0][1] =
                        (Ptr) ((Int32)pDrvObj->inFrameList.frames[frameId]->addr[0][1] + offset[1]);
               }
            }

        }
    }
    return 0;
}

Void SwMsLink_drvTimerCb(UArg arg)
{
    SwMsLink_Obj *pObj = (SwMsLink_Obj *) arg;

    if(pObj->createArgs.enableProcessTieWithDisplay == FALSE)
    {
        Utils_tskSendCmd(&pObj->tsk, SW_MS_LINK_CMD_DO_SCALING);
    }
}

Void SwMsLink_drvDisplaySyncCb(Ptr arg)
{
    SwMsLink_Obj *pObj = (SwMsLink_Obj *) arg;
    UInt32 curTime, displayInterval;

    if(pObj->createArgs.enableProcessTieWithDisplay == TRUE)
    {
        curTime = Utils_getCurTimeInMsec();
        if (curTime > pObj->lastDisplayCBTS)
        {
            displayInterval = curTime - pObj->lastDisplayCBTS;
        }
        else
        {
            displayInterval = curTime + (0xFFFFFFFF - pObj->lastDisplayCBTS);
        }

        if(pObj->timerPeriod > (displayInterval + SWMS_LINK_DISPLAY_CB_OFFSET))
        {
            pObj->SwmsProcessTieWithDisplayLocalFlag ^= TRUE;
            pObj->enableOuputDupLocalFlag = TRUE;
        }
        else
        {
            pObj->SwmsProcessTieWithDisplayLocalFlag = TRUE;
            pObj->enableOuputDupLocalFlag = FALSE;
        }
        if(pObj->SwmsProcessTieWithDisplayLocalFlag)
        {
            Utils_tskSendCmd(&pObj->tsk, SW_MS_LINK_CMD_DO_SCALING);
        }
    pObj->lastDisplayCBTS = Utils_getCurTimeInMsec();
    }
}

Int32 SwMsLink_drvFvidCb(FVID2_Handle handle, Ptr appData, Ptr reserved)
{
    SwMsLink_DrvObj *pDrvObj = (SwMsLink_DrvObj *) appData;

    Semaphore_post(pDrvObj->complete);

    return FVID2_SOK;
}

Int32 SwMsLink_drvFvidErrCb(FVID2_Handle handle,
                            Ptr appData, Ptr errList, Ptr reserved)
{
    return FVID2_SOK;
}

Int32 SwMsLink_drvGetLayoutParams(SwMsLink_Obj * pObj, SwMsLink_LayoutPrm * layoutParams)
{
    SwMsLink_drvLock(pObj);

    memcpy(layoutParams, &pObj->layoutParams, sizeof(*layoutParams));

    SwMsLink_drvUnlock(pObj);

    return FVID2_SOK;

}

UInt32 SwMsLink_getDrvInstFromWinId(SwMsLink_Obj *pObj, UInt32 winId)
{
    UInt32 ipFrameChanNum, instId, i;

    instId = SWMS_LINK_MAP_INVALID;

    UTILS_assert (winId < SYSTEM_SW_MS_MAX_WIN);
    ipFrameChanNum = pObj->layoutParams.winInfo[winId].channelNum;
    if (ipFrameChanNum != SYSTEM_SW_MS_INVALID_ID)
    {
        instId = pObj->chNum2InstIdMap[ipFrameChanNum];
        UTILS_assert (instId != SWMS_LINK_MAP_INVALID);
    }
    else
    {
        /* No Channel associated with this window. We would require to process
            blank frame once, subsequently EDMA would be used to copy, no
            processing by scalar. */
        for (i = 0; i < pObj->createArgs.numSwMsInst; i++)
        {
            if (pObj->DrvObj[i].win2ScLChMap[winId] != SWMS_LINK_MAP_INVALID)
            {
                instId = i;
                break;
            }
        }
        UTILS_assert (instId != SWMS_LINK_MAP_INVALID);
    }

    return (instId);
}

Int32 SwMsLink_drvPrintLayoutParams(SwMsLink_Obj * pObj)
{
    UInt32 winId, chNum;
    SwMsLink_OutWinObj *pWinObj;
    char strDataFormat[8];

    Vps_printf( " \n"
            " *** [%s] Mosaic Parameters *** \n"
            " \n"
            " Output FPS: %d\n"
            " \n"
            " Win | Ch  | Input      | Input          | Input         | Input       | Output     |  Output         | Output        | Output      | Scan        | Low Cost | SWMS | Data  | Blank |\n"
            " Num | Num | Start X, Y | Width x Height | Pitch Y / C   | Memory Type | Start X, Y |  Width x Height | Pitch Y / C   | Memory Type | Fmt         | ON / OFF | Inst | Format| Frame |\n"
            " ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n",
            pObj->name,
            pObj->layoutParams.outputFPS
            );


    for (winId = 0; winId < pObj->layoutParams.numWin; winId++)
    {
        pWinObj = &pObj->winObj[winId];

        chNum = pObj->layoutParams.winInfo[winId].channelNum;

        if(pWinObj->scRtInFrmPrm.dataFormat==FVID2_DF_YUV422I_YUYV)
            strcpy(strDataFormat, "422I ");
        else
        if(pWinObj->scRtInFrmPrm.dataFormat==FVID2_DF_YUV420SP_UV)
            strcpy(strDataFormat, "420SP");
        else
            strcpy(strDataFormat, "UNKNWN");

        Vps_printf
            (" %3d | %3d | %4d, %4d | %5d x %6d | %5d / %5d | %s | %4d, %4d | %5d x %6d | %5d / %6d | %s | %11s | %8s | %4d | %6s | %5s |\n",
                winId,
                chNum,
                pWinObj->scRtCropCfg.cropStartX,
                pWinObj->scRtCropCfg.cropStartY,
                pWinObj->scRtInFrmPrm.width,
                pWinObj->scRtInFrmPrm.height,
                pWinObj->scRtInFrmPrm.pitch[0],
                pWinObj->scRtInFrmPrm.pitch[1],
                gSystem_nameMemoryType[pWinObj->scRtInFrmPrm.memType],
                pObj->layoutParams.winInfo[winId].startX,
                pObj->layoutParams.winInfo[winId].startY,
                pWinObj->scRtOutFrmPrm.width,
                pWinObj->scRtOutFrmPrm.height,
                pWinObj->scRtOutFrmPrm.pitch[0],
                pWinObj->scRtOutFrmPrm.pitch[1],
                gSystem_nameMemoryType[pWinObj->scRtOutFrmPrm.memType],
                (pObj->rtChannelInfo[chNum].scanFormat == FVID2_SF_PROGRESSIVE)
                        ? "PROGRESSIVE" : "INTERLACE  ",
                gSystem_nameOnOff[pObj->layoutParams.winInfo[winId].bypass],
                SwMsLink_getDrvInstFromWinId(pObj,winId),
                strDataFormat,
                gSystem_nameOnOff[(pWinObj->lastInFrame == &pWinObj->blankFrame)]
            );
    }

    Vps_printf( " \n");

    return FVID2_SOK;
}


Int32 SwMsLink_drvUpdateRtChannelInfo(SwMsLink_Obj * pObj)
{
    UInt32 chId;
    System_LinkChInfo *pChInfo;

    SwMsLink_drvLock(pObj);

   for (chId = 0; chId < pObj->inQueInfo.numCh; chId++)
   {
       UTILS_assert (chId < SYSTEM_SW_MS_MAX_CH_ID);
       pChInfo = &pObj->inQueInfo.chInfo[chId];

       pObj->rtChannelInfo[chId].startX = VpsUtils_align(pChInfo->startX, 2);
       pObj->rtChannelInfo[chId].startY = VpsUtils_align(pChInfo->startY, 2);
       pObj->rtChannelInfo[chId].width = VpsUtils_align(pChInfo->width, 2);
       pObj->rtChannelInfo[chId].height = VpsUtils_align(pChInfo->height, 2);
       pObj->rtChannelInfo[chId].pitch[0] = pChInfo->pitch[0];
       pObj->rtChannelInfo[chId].pitch[1] = pChInfo->pitch[1];
       pObj->rtChannelInfo[chId].pitch[2] = pChInfo->pitch[2];
       pObj->rtChannelInfo[chId].memType = pChInfo->memType;
       pObj->rtChannelInfo[chId].dataFormat = pChInfo->dataFormat;
       pObj->rtChannelInfo[chId].scanFormat = pChInfo->scanFormat;

#ifdef SYSTEM_DEBUG_SWMS
       Vps_printf(" %d: SWMS: %2d: Format: %s, %d x %d\n",
                   Utils_getCurTimeInMsec(),
                   chId, 
                   gSystem_nameScanFormat[pObj->rtChannelInfo[chId].scanFormat],
                   pObj->rtChannelInfo[chId].width,
                   pObj->rtChannelInfo[chId].height
                   );
#endif

   }

   SwMsLink_drvUnlock(pObj);

   return FVID2_SOK;
}

Int32 SwMsLink_drvGetInputChInfoFromWinId(SwMsLink_Obj * pObj,SwMsLink_WinInfo * pCropInfo)
{
    UInt32  chnl;

    SwMsLink_drvLock(pObj);

    chnl = pObj->layoutParams.winInfo[pCropInfo->winId].channelNum;

    pCropInfo->startX   = pObj->rtChannelInfo[chnl].startX;
    pCropInfo->startY   = pObj->rtChannelInfo[chnl].startY;
    pCropInfo->width    = pObj->rtChannelInfo[chnl].width;
    pCropInfo->height   = pObj->rtChannelInfo[chnl].height;

    if ((FVID2_SF_PROGRESSIVE == pObj->rtChannelInfo[chnl].scanFormat) &&
        pObj->layoutParams.winInfo[pCropInfo->winId].bypass &&
        VPS_VPDMA_MT_NONTILEDMEM == pObj->rtChannelInfo[chnl].memType
        )
    {
        pCropInfo->height   /= 2;
    }

    SwMsLink_drvUnlock(pObj);
    return FVID2_SOK;
}

Int32 SwMsLink_drvSetCropParam(SwMsLink_Obj * pObj,SwMsLink_WinInfo * pCropInfo)
{
    SwMsLink_OutWinObj *pWinObj;

    SwMsLink_drvLock(pObj);

    pWinObj = &pObj->winObj[pCropInfo->winId];

    pWinObj->scRtCropCfg.cropStartX =
        VpsUtils_align(pCropInfo->startX, 2);
    pWinObj->scRtCropCfg.cropStartY =
        VpsUtils_align(pCropInfo->startY, 2);
    pWinObj->scRtCropCfg.cropWidth = VpsUtils_align(pCropInfo->width, 2);
    pWinObj->scRtCropCfg.cropHeight =
        VpsUtils_align(pCropInfo->height, 2);

    pWinObj->scRtInFrmPrm.width =
        pWinObj->scRtCropCfg.cropStartX +
        pWinObj->scRtCropCfg.cropWidth;
    pWinObj->scRtInFrmPrm.height =
        pWinObj->scRtCropCfg.cropStartY +
        pWinObj->scRtCropCfg.cropHeight;

    pWinObj->applyRtPrm = TRUE;

    pObj->lastOutBufPtr = NULL;

    SwMsLink_drvUnlock(pObj);
    return FVID2_SOK;
}

Int32 SwMsLink_drvSetHmpCropParam(SwMsLink_Obj * pObj,
                                  SwMsLink_chDyHmpSetInputCrop * hmpCropPrm)
{
    SwMsLink_chDyHmpSetInputCrop *hmpCrop;

    SwMsLink_drvLock(pObj);

    UTILS_assert (hmpCropPrm->chId <= SYSTEM_SW_MS_MAX_CH_ID);
    hmpCrop = &pObj->chObj[hmpCropPrm->chId].hmpCrop;

    hmpCrop->enableHMPCropFlag = hmpCropPrm->enableHMPCropFlag;
    hmpCrop->chId = hmpCropPrm->chId;

    if (hmpCrop->enableHMPCropFlag == TRUE)
    {
        hmpCrop->cropStartX =
                 VpsUtils_align(hmpCropPrm->cropStartX, VPS_BUFFER_ALIGNMENT);
        hmpCrop->cropStartY =
                 VpsUtils_align(hmpCropPrm->cropStartY, VPS_BUFFER_ALIGNMENT);
        hmpCrop->cropWidth =
                 VpsUtils_align(hmpCropPrm->cropWidth, VPS_BUFFER_ALIGNMENT);
        hmpCrop->cropHeight =
                 VpsUtils_align(hmpCropPrm->cropHeight, VPS_BUFFER_ALIGNMENT);
    }
    pObj->lastOutBufPtr = NULL;

    SwMsLink_drvUnlock(pObj);
    return FVID2_SOK;
}

Int32 SwMsLink_drvSetScCoeffs(SwMsLink_Obj * pObj, FVID2_Handle fvidHandle,
                UInt32 curCoeffId_v, UInt32 curCoeffId_h, Bool isDei)
{
    Int32 retVal = FVID2_SOK;
    Vps_ScCoeffParams coeffPrms;
    static char *scCoeffName[VPS_SC_SET_MAX] =
    {
        "3/16",
        "4/16",
        "5/16",
        "6/16",
        "7/16",
        "8/16",
        "9/16",
        "10/16",
        "11/16",
        "12/16",
        "13/16",
        "14/16",
        "15/16",
        "UPSCALE",
        "1/1",
    };

    if(curCoeffId_h>=VPS_SC_SET_MAX)
        curCoeffId_h = VPS_SC_US_SET;

    if(curCoeffId_v>=VPS_SC_SET_MAX)
        curCoeffId_v = VPS_SC_US_SET;

    Vps_rprintf(" %d: %s    : Loading Vertical Co-effs (%s)x ... \n",
                Utils_getCurTimeInMsec(), pObj->name, scCoeffName[curCoeffId_v]
            );
    Vps_rprintf(" %d: %s    : Loading Horizontal Co-effs (%s)x ... \n",
                Utils_getCurTimeInMsec(), pObj->name, scCoeffName[curCoeffId_h]
            );

    coeffPrms.hScalingSet = curCoeffId_h;
    coeffPrms.vScalingSet = curCoeffId_v;

    coeffPrms.coeffPtr = NULL;
    coeffPrms.scalarId = isDei
        ? VPS_M2M_DEI_SCALAR_ID_DEI_SC : VPS_M2M_SC_SCALAR_ID_DEFAULT;

    /* Program DEI scalar coefficient - Always used */
    retVal = FVID2_control(fvidHandle, IOCTL_VPS_SET_COEFFS, &coeffPrms, NULL);
    UTILS_assert(FVID2_SOK == retVal);

    Vps_rprintf(" %d: SWMS    : Co-effs Loading ... DONE !!!\n",
                Utils_getCurTimeInMsec());

    return (retVal);
}

Int32 SwMsLink_drvSelectScCoeffs (SwMsLink_Obj * pObj)
{
    SwMsLink_OutWinObj *pWinObj;

    UInt32 inH, outH, inW, outW, numerator, denominator;
    Int32  coeffId_v, coeffId_h;
    Bool isDei;
    Int32 instId, i;

    /* This is an example of selecting scaling coefficients

            Need to apply the below co-effs todo antiflicker with downscaling / upscaling

            VPS_SC_US_SET - 1x1
            VPS_SC_DS_SET_4_16 - for other layouts

            Selecting scaling co-eff based on vertical resolution.
            Assuming horizontal scaling will be proportionate to vertical scaling.

            Selecting co-effs based on SC done for WIN0.
            Assuming WIN0 scaling is same as other window's OR WIN0 is bigger / primary window.
         */

    pWinObj = &pObj->winObj[0];

    inH = pWinObj->scRtCropCfg.cropHeight;
    outH = pWinObj->scRtOutFrmPrm.height;
    inW = pWinObj->scRtCropCfg.cropWidth;
    outW = pWinObj->scRtOutFrmPrm.width;

    denominator = 16;
    /* find best matching scaling co-effs */
    if (outH >= inH)
    {
        coeffId_v = VPS_SC_US_SET;
    }
    else
    {
        numerator = 15;
        for (coeffId_v = VPS_SC_DS_SET_15_16 ;
            coeffId_v >= VPS_SC_DS_SET_3_16 ; coeffId_v --)
        {
            if ((outH*denominator) > (inH*numerator))
                break;
            numerator --;
        }
    }

    if (outW >= inW)
    {
        coeffId_h = VPS_SC_US_SET;
    }
    else
    {
        numerator = 15;
        for (coeffId_h = VPS_SC_DS_SET_15_16 ;
            coeffId_h >= VPS_SC_DS_SET_3_16 ; coeffId_h --)
        {
            if ((outW*denominator) > (inW*numerator))
                break;
            numerator --;
        }
    }

    for (i = 0; i < pObj->createArgs.numSwMsInst; i++)
    {
        instId = i;
        if(pObj->DrvObj[instId].isDeiDrv == TRUE)
            isDei = TRUE;
        else
            isDei = FALSE;
        if (pObj->DrvObj[instId].fvidHandle)
            SwMsLink_drvSetScCoeffs(pObj, pObj->DrvObj[instId].fvidHandle,
                                    coeffId_v, coeffId_h, isDei);

        instId += SYSTEM_SW_MS_MAX_INST;
        if(pObj->DrvObj[instId].isDeiDrv == TRUE)
            isDei = TRUE;
        else
            isDei = FALSE;

        if (pObj->DrvObj[instId].fvidHandle)
            SwMsLink_drvSetScCoeffs(pObj, pObj->DrvObj[instId].fvidHandle,
                                    coeffId_v, coeffId_h, isDei);
    }

    return FVID2_SOK;
}

Int32 SwMsLink_drvSwitchLayout(SwMsLink_Obj * pObj,
                               SwMsLink_LayoutPrm * layoutParams,
                               Bool isLockAlredayTaken,
                               Bool isNewLayout)
{
    UInt32 winId, chNum;
    SwMsLink_OutWinObj *pWinObj;
    System_LinkChInfo *pChInfo;
    UInt32 drvInst;

    if (isLockAlredayTaken == FALSE)
    {
        SwMsLink_drvLock(pObj);
    }

    pObj->switchLayout = TRUE;

    if(layoutParams->onlyCh2WinMapChanged == FALSE)
    {
          SwMsLink_drvResetStatistics(pObj);

          pObj->skipProcessing = SW_MS_SKIP_PROCESSING;
          SwMsLink_drvSetBlankOutputFlag(pObj);
    }

    memcpy(&pObj->layoutParams, layoutParams, sizeof(*layoutParams));

    SwMsLink_updateLayoutParams(
        &pObj->layoutParams,
        pObj->outFrameFormat.pitch[0],
        pObj->outFrameFormat.dataFormat);

    SwMsLink_updateWinIdToScInstLChMap(pObj);

    SwMsLink_drvGetTimerPeriod(pObj, layoutParams);
    SwMsLink_drvClockPeriodReconfigure(pObj);

    for (winId = 0; winId < pObj->layoutParams.numWin; winId++)
    {
        drvInst = SwMsLink_getDrvInstFromWinId(pObj,winId);

        pWinObj = &pObj->winObj[winId];

        pWinObj->scRtOutFrmPrm.width = pObj->layoutParams.winInfo[winId].width;
        pWinObj->scRtOutFrmPrm.height = pObj->layoutParams.winInfo[winId].height;
        pWinObj->scRtOutFrmPrm.pitch[0] = pObj->outFrameFormat.pitch[0];
        pWinObj->scRtOutFrmPrm.pitch[1] = pObj->outFrameFormat.pitch[1];
        pWinObj->scRtOutFrmPrm.pitch[2] = pObj->outFrameFormat.pitch[2];
        pWinObj->scRtOutFrmPrm.memType =
            pObj->info.queInfo[0].chInfo[0].memType;

        pWinObj->scRtOutFrmPrm.dataFormat = pObj->outFrameFormat.dataFormat;

        chNum = pObj->layoutParams.winInfo[winId].channelNum;


        if(!(chNum == SYSTEM_SW_MS_INVALID_ID || chNum < pObj->inQueInfo.numCh))
        {
            Vps_printf(" WARNING: WIN%d: CH%d, Channel ID is INVALID (max allowed CH ID is CH%d), !!!\n", winId, chNum, pObj->inQueInfo.numCh-1);
        }

        if (chNum == SYSTEM_SW_MS_INVALID_ID || chNum >= pObj->inQueInfo.numCh)
        {
            pWinObj->scRtCropCfg.cropStartX = pObj->blankFrameScRtCropCfg.cropStartX;
            pWinObj->scRtCropCfg.cropStartY = pObj->blankFrameScRtCropCfg.cropStartY;
            pWinObj->scRtCropCfg.cropWidth  = pObj->blankFrameScRtCropCfg.cropWidth;
            pWinObj->scRtCropCfg.cropHeight = pObj->blankFrameScRtCropCfg.cropHeight;
            pWinObj->scRtInFrmPrm.width  = pObj->blankFrameScRtInFrmPrm.width;
            pWinObj->scRtInFrmPrm.height = pObj->blankFrameScRtInFrmPrm.height;
            pWinObj->scRtInFrmPrm.pitch[0] = pObj->blankFrameScRtInFrmPrm.pitch[0];
            pWinObj->scRtInFrmPrm.pitch[1] = pObj->blankFrameScRtInFrmPrm.pitch[1];
            pWinObj->scRtInFrmPrm.pitch[2] = pObj->blankFrameScRtInFrmPrm.pitch[2];
            pWinObj->scRtInFrmPrm.memType = pObj->blankFrameScRtInFrmPrm.memType;
            pWinObj->scRtInFrmPrm.dataFormat = pObj->blankFrameScRtInFrmPrm.dataFormat;

        }
        else
        {
            pChInfo = &pObj->rtChannelInfo[chNum];

            pWinObj->scRtCropCfg.cropStartX =
                VpsUtils_align(pChInfo->startX, 2);
            pWinObj->scRtCropCfg.cropStartY =
                VpsUtils_align(pChInfo->startY, 2);
            pWinObj->scRtCropCfg.cropWidth = VpsUtils_align(pChInfo->width, 2);
            pWinObj->scRtCropCfg.cropHeight =
                VpsUtils_align(pChInfo->height, 2);

            if (pObj->DrvObj[drvInst].isDeiDrv)
            {
                if((pObj->DrvObj[drvInst].forceBypassDei == FALSE) 
                     &&
                    (pObj->layoutParams.winInfo[winId].bypass == FALSE))
                {
                    if(FVID2_SF_INTERLACED == pChInfo->scanFormat)
                    {
                        pWinObj->scRtCropCfg.cropHeight *= 2;
                        Vps_printf(" SWMS: CH %d -> Interlaced - Setting cropHeight to %d *******\n",
                            chNum, pWinObj->scRtCropCfg.cropHeight);
                    }
                }
            }

            #ifdef SWMS_ENABLE_MODIFY_FRAME_POINTER_ALWAYS
            pWinObj->scRtCropCfg.cropStartX = 0;
            pWinObj->scRtCropCfg.cropStartY = 0;
            #endif

            pWinObj->scRtInFrmPrm.width =
                pWinObj->scRtCropCfg.cropStartX +
                pWinObj->scRtCropCfg.cropWidth;
            pWinObj->scRtInFrmPrm.height =
                pWinObj->scRtCropCfg.cropStartY +
                pWinObj->scRtCropCfg.cropHeight;
            pWinObj->scRtInFrmPrm.pitch[0] = pChInfo->pitch[0];
            pWinObj->scRtInFrmPrm.pitch[1] = pChInfo->pitch[1];
            pWinObj->scRtInFrmPrm.pitch[2] = pChInfo->pitch[2];
            pWinObj->scRtInFrmPrm.memType = pChInfo->memType;

            pWinObj->scRtInFrmPrm.dataFormat = pChInfo->dataFormat;

            pObj->chObj[chNum].displayStarted = FALSE;

        }

        if(pWinObj->scRtInFrmPrm.width > SW_MS_MAX_WIDTH_SUPPORTED)
        {
            pWinObj->scRtInFrmPrm.width = pWinObj->scRtCropCfg.cropWidth;
            pWinObj->scRtCropCfg.cropStartX = 0;
        }

        if (pObj->DrvObj[drvInst].isDeiDrv)
        {
            memset(&pWinObj->deiRtPrm, 0, sizeof(pWinObj->deiRtPrm));

            pWinObj->deiRtPrm.deiOutFrmPrms = &pWinObj->scRtOutFrmPrm;
            if (SwMsLink_drvIsVipOutputUsed(pObj->DrvObj[drvInst].drvInstId))
            {
                pWinObj->deiRtPrm.vipOutFrmPrms[SWMS_LINK_DEI_DEF_VIP_STREAMID] = &pWinObj->scRtOutFrmPrm;
                pWinObj->deiRtPrm.vipScCropCfg = &pWinObj->scRtCropCfg;
            }
            pWinObj->deiRtPrm.deiInFrmPrms = &pWinObj->scRtInFrmPrm;
            pWinObj->deiRtPrm.deiScCropCfg = &pWinObj->scRtCropCfg;
            pWinObj->deiRtPrm.deiRtCfg = &pWinObj->deiRtCfg;

            pWinObj->deiRtCfg.resetDei = FALSE;
            pWinObj->deiRtCfg.fldRepeat = FALSE;

            if(pObj->DrvObj[drvInst].forceBypassDei == TRUE)
            {
                /* In case captured data is progressive and window is bypass
                            * (don't care quality so much), SC takes only even lines and
                            * make S/W mosaic. This is sometimes needed due to SC
                           * performance. */
                if ((FVID2_SF_PROGRESSIVE ==
                     pObj->inQueInfo.chInfo[chNum].scanFormat)
                            &&
                    pObj->layoutParams.winInfo[winId].bypass
                            &&
                    VPS_VPDMA_MT_NONTILEDMEM ==
                            pObj->inQueInfo.chInfo[chNum].memType
                    )
                {
                    pWinObj->scRtCropCfg.cropStartY /= 2;
                    pWinObj->scRtCropCfg.cropHeight /= 2;
                    pWinObj->scRtInFrmPrm.height /= 2;
                    pWinObj->scRtInFrmPrm.pitch[0] *= 2;
                    pWinObj->scRtInFrmPrm.pitch[1] *= 2;
                    pWinObj->scRtInFrmPrm.pitch[2] *= 2;
                }
            }
        }
        else
        {
            memset(&pWinObj->scRtPrm, 0, sizeof(pWinObj->scRtPrm));

            pWinObj->scRtPrm.outFrmPrms = &pWinObj->scRtOutFrmPrm;
            pWinObj->scRtPrm.inFrmPrms = &pWinObj->scRtInFrmPrm;
            pWinObj->scRtPrm.srcCropCfg = &pWinObj->scRtCropCfg;
            pWinObj->scRtPrm.scCfg = NULL;

            /* In case captured data is progressive and window is bypass
                      * (don't care quality so much), SC takes only even lines and
                      * make S/W mosaic. This is sometimes needed due to SC
                      * performance. */
            if ((FVID2_SF_PROGRESSIVE ==
                 pObj->inQueInfo.chInfo[chNum].scanFormat) &&
                pObj->layoutParams.winInfo[winId].bypass   &&
                VPS_VPDMA_MT_NONTILEDMEM ==
                            pObj->inQueInfo.chInfo[chNum].memType)
            {
                pWinObj->scRtCropCfg.cropStartY /= 2;
                pWinObj->scRtCropCfg.cropHeight /= 2;
                pWinObj->scRtInFrmPrm.height /= 2;
                pWinObj->scRtInFrmPrm.pitch[0] *= 2;
                pWinObj->scRtInFrmPrm.pitch[1] *= 2;
                pWinObj->scRtInFrmPrm.pitch[2] *= 2;
            }
        }

        if (pWinObj->scRtInFrmPrm.width & 0x1)
        {
            Vps_printf(" SWMS: CH %d Changing odd width to even %d *******\n",
                chNum, pWinObj->scRtInFrmPrm.width);
            pWinObj->scRtInFrmPrm.width &= ~0x1;
        }

        if (pWinObj->scRtInFrmPrm.height & 0x1)
        {
            Vps_printf(" SWMS: CH %d Changing odd height to even %d *******\n",
                chNum, pWinObj->scRtInFrmPrm.height);
            pWinObj->scRtInFrmPrm.height &= ~0x1;
        }

        pWinObj->applyRtPrm = TRUE;

        /* Get the input frames channel number associated for this window */
        UTILS_assert (winId < SYSTEM_SW_MS_MAX_WIN);
        UTILS_assert(pObj->DrvObj[drvInst].win2ScLChMap[winId]
            != SWMS_LINK_MAP_INVALID);
        pWinObj->blankFrame.channelNum =
            pObj->DrvObj[drvInst].win2ScLChMap[winId];
        pWinObj->curOutFrame.channelNum =
            pWinObj->blankFrame.channelNum;
    }

    SwMsLink_drvSelectScCoeffs(pObj);

    SwMsLink_drvPrintLayoutParams(pObj);

    if ((isNewLayout) && (layoutParams->onlyCh2WinMapChanged == FALSE))
    {
        if (FALSE == pObj->createArgs.outputBufModified)
        {
            if (SwMsLink_drvIsOverlapLayout(pObj))
            {
                pObj->enableBufCopy = FALSE;
            }
            else
            {
                pObj->enableBufCopy = TRUE;
            }
        }
        else
        {
            pObj->enableBufCopy = FALSE;
        }
    }
    if (isLockAlredayTaken == FALSE)
    {
        SwMsLink_drvUnlock(pObj);
    }
    return FVID2_SOK;
}

Int32 SwMsLink_drvCreateOutInfo(SwMsLink_Obj * pObj, UInt32 outRes)
{
    Int32 status;
    System_LinkChInfo *pChInfo;
    UInt32 frameId, bufferPitch;
    UInt32 bufferWidth, bufferHeight;

    memset(&pObj->outFrameDrop, 0, sizeof(pObj->outFrameDrop));

    pObj->info.numQue = 1;
    pObj->info.queInfo[0].numCh = 1;

    pChInfo = &pObj->info.queInfo[0].chInfo[0];

    pChInfo->dataFormat = pObj->createArgs.outDataFormat;
    pChInfo->memType = VPS_VPDMA_MT_NONTILEDMEM;

    System_getOutSize(pObj->createArgs.maxOutRes, &bufferWidth, &bufferHeight);

    bufferPitch = VpsUtils_align(bufferWidth, VPS_BUFFER_ALIGNMENT * 2) * 2;

    if(pObj->createArgs.initOutRes == SYSTEM_STD_INVALID)
    {
        pObj->createArgs.initOutRes = pObj->createArgs.maxOutRes;
    }

    System_getOutSize(pObj->createArgs.initOutRes, &pChInfo->width, &pChInfo->height);

    if (SYSTEM_DF_YUV422I_YUYV == pObj->createArgs.outDataFormat)
    {
        pChInfo->pitch[0] = bufferPitch;
        pChInfo->pitch[1] = pChInfo->pitch[2] = 0;
    }
    else
    {
        pChInfo->pitch[0] = pChInfo->pitch[1] = pChInfo->pitch[2] = bufferPitch/2;
    }

    pChInfo->scanFormat = FVID2_SF_PROGRESSIVE;

    pObj->bufferFrameFormat.channelNum = 0;
    pObj->bufferFrameFormat.width = bufferWidth;
    pObj->bufferFrameFormat.height = bufferHeight;
    pObj->bufferFrameFormat.pitch[0] = pChInfo->pitch[0];
    pObj->bufferFrameFormat.pitch[1] = pChInfo->pitch[1];
    pObj->bufferFrameFormat.pitch[2] = pChInfo->pitch[2];
    pObj->bufferFrameFormat.fieldMerged[0] = FALSE;
    pObj->bufferFrameFormat.fieldMerged[1] = FALSE;
    pObj->bufferFrameFormat.fieldMerged[2] = FALSE;
    pObj->bufferFrameFormat.dataFormat = pChInfo->dataFormat;
    pObj->bufferFrameFormat.scanFormat = pChInfo->scanFormat;
    pObj->bufferFrameFormat.bpp = FVID2_BPP_BITS16;
    pObj->bufferFrameFormat.reserved = NULL;

    status = Utils_bufCreate(&pObj->bufOutQue, TRUE, FALSE);
    UTILS_assert(status == FVID2_SOK);

    /* set actual required width x height */
    pObj->outFrameFormat = pObj->bufferFrameFormat;
    pObj->outFrameFormat.width = pChInfo->width;
    pObj->outFrameFormat.height = pChInfo->height;

    for (frameId = 0; frameId < pObj->createArgs.numOutBuf; frameId++)
    {
        /* alloc buffer of max possible size but use only what is needed for a
         * given resolution */
        status = Utils_memFrameAlloc(&pObj->bufferFrameFormat,
                                     &pObj->outFrames[frameId], 1);
        UTILS_assert(status == FVID2_SOK);

        status = Utils_bufPutEmptyFrame(&pObj->bufOutQue,
                                        &pObj->outFrames[frameId]);
        UTILS_assert(status == FVID2_SOK);
        pObj->outFrameInfo[frameId].swMsBlankOutBuf = SWMS_LINK_DO_OUTBUF_BLANKING;
        pObj->outFrames[frameId].appData = &pObj->outFrameInfo[frameId];
    }

    return FVID2_SOK;
}

static
Int32 SwMsLink_drvCreateChannelObj(SwMsLink_Obj * pObj)
{
    Int i;
    Int status = FVID2_SOK;
    AvsyncLink_VidQueCreateParams cp;

    for (i = 0; i < SYSTEM_SW_MS_MAX_CH_ID; i++)
    {
        Avsync_vidQueCreateParamsInit(&cp);
        cp.chNum     = i;
        cp.maxElements = UTILS_ARRAYSIZE(pObj->chObj[i].inQueMem);
        cp.queueMem    = pObj->chObj[i].inQueMem;
        cp.syncLinkID = pObj->linkId;
        cp.displayID  = Avsync_mapDisplayLinkID2Index(pObj->createArgs.outQueParams.nextLink);
        status =
          Avsync_vidQueCreate(&cp,&pObj->chObj[i].inQue);
        UTILS_assert(status ==0);
        pObj->chObj[i].pCurInFrame = NULL;
        pObj->chObj[i].isPlaybackChannel = FALSE;
        pObj->chObj[i].isInputReceived = FALSE;
        pObj->chObj[i].displayStarted = FALSE;
        pObj->chObj[i].doFrameSkip = FALSE;
        pObj->chObj[i].hmpCrop.enableHMPCropFlag = FALSE;

    }
    pObj->lastOutBufPtr = NULL;
    pObj->lastDisplayCBTS = 0;
    pObj->SwmsProcessTieWithDisplayLocalFlag = FALSE;
    pObj->enableOuputDupLocalFlag = FALSE;
    if (pObj->createArgs.enableProcessTieWithDisplay)
    {
        AvsyncLink_VidSyncCbHookObj  cbHookObj;

        cbHookObj.cbFxn = SwMsLink_drvDisplaySyncCb;
        cbHookObj.ctx   = pObj;
        Avsync_vidSynchRegisterCbHook(Avsync_vidQueGetDisplayID(pObj->linkId),
                                      &cbHookObj);
    }

    return status;
}

static
Int32 SwMsLink_drvDeleteChannelObj(SwMsLink_Obj * pObj)
{
    Int i;
    Int status = FVID2_SOK;

    for (i = 0; i < SYSTEM_SW_MS_MAX_CH_ID; i++)
    {
        status =
          Avsync_vidQueDelete(&pObj->chObj[i].inQue);
        UTILS_assert(status ==0);
        pObj->chObj[i].pCurInFrame = NULL;
        pObj->chObj[i].isPlaybackChannel = FALSE;
        pObj->chObj[i].isInputReceived = FALSE;
        pObj->chObj[i].displayStarted = FALSE;
        pObj->chObj[i].hmpCrop.enableHMPCropFlag = FALSE;

    }
    pObj->lastOutBufPtr = NULL;
    return status;
}

static
Bool  SwMsLink_isPlaybackChannel(FVID2_Frame *frame)
{
    System_FrameInfo *frameInfo;
    Bool isPlaybackChannel = FALSE;

    frameInfo = frame->appData;
    if (frameInfo)
    {
        if (frameInfo->isPlayBackChannel == TRUE)
        {
            isPlaybackChannel = TRUE;
        }
    }
    return isPlaybackChannel;
}

static
Int32 SwMsLink_drvQueueInputFrames(SwMsLink_Obj * pObj,
                                   FVID2_FrameList *frameList)
{
    Int i;
    Int status = 0;
    FVID2_Frame *frame;
    System_LinkInQueParams *pInQueParams;
    UInt32 drvInst;
    UInt32 winId,chIdx;
    SwMsLink_OutWinObj *pWinObj;
    SwMsLink_LayoutWinInfo *pWinInfo;
    System_FrameInfo *pFrameInfo;

    pInQueParams = &pObj->createArgs.inQueParams;

    for (i = 0; i < frameList->numFrames; i++)
    {
        frame = frameList->frames[i];
        if (frame == NULL)
        {
            #ifdef SWMS_DEBUG_FRAME_REJECT
              Vps_printf(" SWMS: Frame Reject: NULL frame\n");
            #endif
            continue;
        }
        if (frame->channelNum >= SYSTEM_SW_MS_MAX_CH_ID)
        {
            #ifdef SWMS_DEBUG_FRAME_REJECT
              Vps_printf(" SWMS: Frame Reject: Invalid CH ID (%d)\n", frame->channelNum);
            #endif
            /* invalid ch ID */
            SwMsLink_freeFrame(pInQueParams, &pObj->freeFrameList, frame);
            continue;
        }
        UTILS_assert(frame->channelNum < UTILS_ARRAYSIZE(pObj->chObj));
        
        pFrameInfo = (System_FrameInfo*) frame->appData;
        UTILS_assert (pFrameInfo != NULL);
        /* If an valid frame, remmember the original channel number.
            Frames channel number will be modified while processing */
        pFrameInfo->swmsOrgChannelNum = frame->channelNum;

        if (!pObj->chObj[frame->channelNum].isPlaybackChannel)
        {
            pObj->chObj[frame->channelNum].isPlaybackChannel =
                SwMsLink_isPlaybackChannel(frame);
        }
        chIdx = frame->channelNum;
        winId = pObj->layoutParams.ch2WinMap[chIdx];
        if (winId < pObj->layoutParams.numWin)
        {
            pWinObj = &pObj->winObj[winId];
            pWinInfo = &pObj->layoutParams.winInfo[winId];
            drvInst = SwMsLink_getDrvInstFromWinId(pObj,winId);
            if (pWinInfo->bypass || !pObj->DrvObj[drvInst].isDeiDrv || pObj->DrvObj[drvInst].forceBypassDei)
            {
                /* window shows channel in bypass mode, then drop odd fields,
                 * i.e always expect even fields */
                pWinObj->expectedFid = 0;
            }
            /* Progressive frames handling if dei driver is enabled */
            if ((pObj->DrvObj[drvInst].isDeiDrv)
                    && 
                (pFrameInfo->rtChInfo.scanFormat == FVID2_SF_PROGRESSIVE))
            {
                pWinObj->expectedFid = 0;
            }

            if (frame->fid != pWinObj->expectedFid)
            {
                 pWinObj->framesFidInvalidCount++;

                 /* incoming frame fid does not match required fid */
                 SwMsLink_freeFrame(pInQueParams, &pObj->freeFrameList, frame);
                 frame = NULL;
            }
            if (frame)
            {
                if (pObj->DrvObj[drvInst].isDeiDrv && pObj->DrvObj[drvInst].forceBypassDei==FALSE)
                {
                    pWinObj->expectedFid ^= 1;
                }
            }
        }
        if (frame && pObj->chObj[frame->channelNum].doFrameSkip)
        {
            Bool doFrameDrop;

            doFrameDrop = Utils_doSkipFrame(&(pObj->chObj[frame->channelNum].frameSkipCtx));
            if (doFrameDrop)
            {
                /* incoming frame fid does not match required fid */
                SwMsLink_freeFrame(pInQueParams, &pObj->freeFrameList, frame);
                frame = NULL;
                if (winId < pObj->layoutParams.numWin)
                {
                    pWinObj = &pObj->winObj[winId];
                    pWinObj->framesRejectCount++;
                    drvInst = SwMsLink_getDrvInstFromWinId(pObj,winId);
                    if (pObj->DrvObj[drvInst].isDeiDrv && pObj->DrvObj[drvInst].forceBypassDei==FALSE)
                    {
                        pWinObj->expectedFid ^= 1;
                    }
                }
            }
        }

        if (frame)
        {
            status = Avsync_vidQuePut(&pObj->chObj[frame->channelNum].inQue,
                              frame);
            UTILS_assert(status == 0);
        }

    }
    return status;
}


static
Int32 SwMsLink_freeFrameList(SwMsLink_Obj * pObj,
                             FVID2_FrameList * freeFrameList)
{
    System_LinkInQueParams *pInQueParams;

    pInQueParams = &pObj->createArgs.inQueParams;

    UTILS_assert (freeFrameList->numFrames < FVID2_MAX_FVID_FRAME_PTR);
    if (freeFrameList->numFrames >= (FVID2_MAX_FVID_FRAME_PTR/4))
    {
        System_putLinksEmptyFrames(pInQueParams->prevLinkId,
                                   pInQueParams->prevLinkQueId, freeFrameList);

        freeFrameList->numFrames = 0;
    }
    return 0;
}


static
Int32 SwMsLink_drvUpdateChCurrentInFrame(SwMsLink_Obj * pObj)
{
    Int chIdx,chCount;
    Int status = 0;
    FVID2_Frame *frame;
    UInt32 winId;
    System_LinkInQueParams *pInQueParams;
    UInt32 drvInst,skipInputFrames;
    SwMsLink_OutWinObj *pWinObj;
    SwMsLink_LayoutWinInfo *pWinInfo;
    UInt32 syncMasterCh;

    pInQueParams = &pObj->createArgs.inQueParams;
    syncMasterCh = Avsync_vidQueGetMasterSynchChannel(pObj->linkId);
    chIdx = syncMasterCh;
    if (chIdx == AVSYNC_INVALID_CHNUM)
    {
        chIdx = 0;
    }

    for (chCount = 0;
         chCount < pObj->inQueInfo.numCh;
         chCount++)
    {
        winId = pObj->layoutParams.ch2WinMap[chIdx];
        pObj->chObj[chIdx].isInputReceived = FALSE;
        if ((winId >= pObj->layoutParams.numWin)
            &&
            (chIdx != syncMasterCh))
        {
            frame = NULL;
            /* channel is not mapped to active window.
             * Free all queued input frames
             */
           Avsync_vidQueFlush(&pObj->chObj[chIdx].inQue,
                              &frame,
                              &pObj->freeFrameList);
           SwMsLink_freeFrameList(pObj,&pObj->freeFrameList);
           if (frame)
           {
               if (pObj->chObj[chIdx].pCurInFrame != NULL)
               {
                   SwMsLink_freeFrame(pInQueParams,&pObj->freeFrameList,
                                      pObj->chObj[chIdx].pCurInFrame);
                   pObj->chObj[chIdx].pCurInFrame = NULL;
               }
               if (pObj->chObj[chIdx].isPlaybackChannel)
               {
                   pObj->chObj[chIdx].pCurInFrame = frame;
                   pObj->chObj[chIdx].isInputReceived = TRUE;
               }
               else
               {
                   SwMsLink_freeFrame(pInQueParams,&pObj->freeFrameList,
                                      frame);
               }
           }
        }
        else
        {
            UInt32 queCnt;

            frame = NULL;
            if (winId < pObj->layoutParams.numWin)
            {
                pWinObj = &pObj->winObj[winId];
                pWinInfo = &pObj->layoutParams.winInfo[winId];
                drvInst = SwMsLink_getDrvInstFromWinId(pObj,winId);
            }
            else
            {
                pWinObj = NULL;
                pWinInfo = NULL;
                drvInst  = 0;
            }
            queCnt = Avsync_vidQueGetQueLength(&pObj->chObj[chIdx].inQue);

            if (SYSTEM_SW_MS_DEFAULT_INPUT_QUE_LEN == pObj->createArgs.maxInputQueLen)
            {
                pObj->createArgs.maxInputQueLen = SW_MS_SKIP_INPUT_FRAMES_SC;
                if(pObj->DrvObj[drvInst].isDeiDrv && pObj->DrvObj[drvInst].forceBypassDei == FALSE)
                {
                    pObj->createArgs.maxInputQueLen = SW_MS_SKIP_INPUT_FRAMES_DEI;
                }
            }
            skipInputFrames = pObj->createArgs.maxInputQueLen;
            if ((queCnt > skipInputFrames)
                &&
                (!(AVSYNC_VIDQUE_IS_SYNCH_ENABLED(&pObj->chObj[chIdx].inQue))))
            {
                UInt32 numFramesToSkip = queCnt - skipInputFrames;
                UInt32 skipFrameIdx;

                if(pObj->DrvObj[drvInst].isDeiDrv && pObj->DrvObj[drvInst].forceBypassDei == FALSE)
                {
                    /* Make sure that if we use DEI we drop even number of fields */
                    if ((numFramesToSkip & 0x1) == 0)
                    {
                        numFramesToSkip++;
                    }
                }
                for (skipFrameIdx = 0; skipFrameIdx < numFramesToSkip ;skipFrameIdx++)
                {
                    UInt32 numFreeFramePreFlush = pObj->freeFrameList.numFrames;

                    frame = NULL;
                    Avsync_vidQueGet(&pObj->chObj[chIdx].inQue,
                                     TRUE,
                                     &frame,
                                     &pObj->freeFrameList);
                    UTILS_assert(frame != NULL);
                    UTILS_assert(pObj->freeFrameList.numFrames == numFreeFramePreFlush);
                    UTILS_assert(pObj->freeFrameList.numFrames <
                                 UTILS_ARRAYSIZE((pObj->freeFrameList.frames)));
                    pObj->freeFrameList.frames[pObj->freeFrameList.numFrames] = frame;
                    pObj->freeFrameList.numFrames++;
                }
                frame = NULL;
                /* After flushing extra frames get a new frame for display */
                Avsync_vidQueGet(&pObj->chObj[chIdx].inQue,
                                 TRUE,
                                 &frame,
                                 &pObj->freeFrameList);
                UTILS_assert(frame != NULL);

                UTILS_assert(pWinObj != NULL);
                pWinObj->framesAccEventCount++;
                if(queCnt>pWinObj->framesAccMax)
                    pWinObj->framesAccMax = queCnt;
                if(queCnt<pWinObj->framesAccMin)
                    pWinObj->framesAccMin = queCnt;
                pWinObj->framesRecvCount    += numFramesToSkip + 1;
                pWinObj->framesDroppedCount += numFramesToSkip;
                SwMsLink_freeFrameList(pObj,&pObj->freeFrameList);
            }
            else
            {
                UInt32 numFreeFramePreGet = pObj->freeFrameList.numFrames;
                Avsync_vidQueGet(&pObj->chObj[chIdx].inQue,
                                 FALSE,
                                 &frame,
                                 &pObj->freeFrameList);
                if(pWinObj != NULL)
                {
                    pWinObj->framesRecvCount    += pObj->freeFrameList.numFrames -
                                                   numFreeFramePreGet;
                    if (frame)
                    {
                        pWinObj->framesRecvCount++;
                    }
                    pWinObj->framesDroppedCount += pObj->freeFrameList.numFrames -
                                                  numFreeFramePreGet;
                }
                SwMsLink_freeFrameList(pObj,&pObj->freeFrameList);
            }
            if (frame != NULL)
            {
                if ((pWinInfo) && (pWinInfo->channelNum != frame->channelNum))
                {
                    #ifdef SWMS_DEBUG_FRAME_REJECT
                        Vps_printf
                        (" SWMS: Frame Reject: Win not mapped to this channel (%d)\n",
                         frame->channelNum);
                    #endif
                   UTILS_assert(pWinObj != NULL);
                   pWinObj->framesInvalidChCount++;

                   /* win is not assigned to this ch, normally this condition
                    * wont happen */
                   SwMsLink_freeFrame(pInQueParams, &pObj->freeFrameList, frame);
                   frame = NULL;
                }
            }

            if (frame)
            {
                if (pObj->chObj[chIdx].pCurInFrame != NULL)
                {
                    SwMsLink_freeFrame(pInQueParams,
                                       &pObj->freeFrameList,
                                       pObj->chObj[chIdx].pCurInFrame);
                }
                pObj->chObj[chIdx].pCurInFrame = frame;
                pObj->chObj[chIdx].isInputReceived = TRUE;
                if (pWinObj)
                {
                    pWinObj->framesQueuedCount++;
                }
                /* toggle next expected fid */
            }
            else
            {
                if (pWinObj)
                {
                    pWinObj->framesRepeatCount++;
                }
            }
        }
        chIdx = (chIdx + 1) % pObj->inQueInfo.numCh;
    }
    return status;
}

Int32 SwMsLink_makeBlankFrameInfo(SwMsLink_Obj * pObj)
{
    SwMsLink_OutWinObj *pWinObj;
    Int32 status;

    pObj->blankBufferFormat.channelNum      = 0;
    pObj->blankBufferFormat.width           = 320+32;
    pObj->blankBufferFormat.height          = 240+24;
    pObj->blankBufferFormat.fieldMerged[0]  = FALSE;
    pObj->blankBufferFormat.fieldMerged[1]  = FALSE;
    pObj->blankBufferFormat.fieldMerged[2]  = FALSE;
    if (SYSTEM_DF_YUV422I_YUYV != pObj->createArgs.outDataFormat)
        pObj->blankBufferFormat.pitch[0]        = VpsUtils_align(pObj->blankBufferFormat.width, VPS_BUFFER_ALIGNMENT);
    else
        pObj->blankBufferFormat.pitch[0]        = VpsUtils_align(pObj->blankBufferFormat.width*2, VPS_BUFFER_ALIGNMENT);
    pObj->blankBufferFormat.pitch[1]        = pObj->blankBufferFormat.pitch[0];
    pObj->blankBufferFormat.pitch[2]        = pObj->blankBufferFormat.pitch[0];
    pObj->blankBufferFormat.dataFormat      = pObj->createArgs.outDataFormat;
    pObj->blankBufferFormat.scanFormat      = FVID2_SF_PROGRESSIVE;
    pObj->blankBufferFormat.bpp             = FVID2_BPP_BITS12;
    pObj->blankBufferFormat.reserved        = NULL;

    pObj->blankFrameScRtInFrmPrm.width      = pObj->blankBufferFormat.width;
    pObj->blankFrameScRtInFrmPrm.height     = pObj->blankBufferFormat.height;
    pObj->blankFrameScRtInFrmPrm.pitch[0]   = pObj->blankBufferFormat.pitch[0];
    pObj->blankFrameScRtInFrmPrm.pitch[1]   = pObj->blankBufferFormat.pitch[1];
    pObj->blankFrameScRtInFrmPrm.pitch[2]   = pObj->blankBufferFormat.pitch[2];
    pObj->blankFrameScRtInFrmPrm.memType    = VPS_VPDMA_MT_NONTILEDMEM;
    pObj->blankFrameScRtInFrmPrm.dataFormat = pObj->blankBufferFormat.dataFormat;

    pObj->blankFrameScRtCropCfg.cropStartX  = 0;
    pObj->blankFrameScRtCropCfg.cropStartY  = 0;
    pObj->blankFrameScRtCropCfg.cropWidth   = pObj->blankFrameScRtInFrmPrm.width;
    pObj->blankFrameScRtCropCfg.cropHeight  = pObj->blankFrameScRtInFrmPrm.height;

    pWinObj = &pObj->winObj[0];

    /* alloc buffer of max possible size as input blank buffer */
    status = System_getBlankFrame(&pWinObj->blankFrame);
    UTILS_assert(status == FVID2_SOK);
    SwMsLink_fillBlankDataPattern(&pObj->blankBufferFormat,
                             &pWinObj->blankFrame);

    return FVID2_SOK;
}

Int32 SwMsLink_drvCreateWinObj(SwMsLink_Obj * pObj, UInt32 winId)
{
    SwMsLink_OutWinObj *pWinObj;
    Int32 status = FVID2_SOK;

    pWinObj = &pObj->winObj[winId];

    /* assume all CHs are of same input size, format, pitch */
    pWinObj->scanFormat =
        (FVID2_ScanFormat) pObj->inQueInfo.chInfo[0].scanFormat;
    pWinObj->expectedFid = 0;
    pWinObj->applyRtPrm = FALSE;

    pWinObj->isBlankFrameDisplay = FALSE;

    if (winId == 0)
    {
        SwMsLink_makeBlankFrameInfo(pObj);
    }
    else
    {
        pWinObj->blankFrame = pObj->winObj[0].blankFrame;
    }

    pWinObj->blankFrame.channelNum = winId;

    memset(&pWinObj->curOutFrame, 0, sizeof(pWinObj->curOutFrame));
    pWinObj->curOutFrame.addr[0][0] = NULL;
    pWinObj->curOutFrame.addr[0][1] = NULL;
    pWinObj->curOutFrame.channelNum = winId;
    pWinObj->lastInFrame = NULL;
    return status;
}

Int32 SwMsLink_drvAllocCtxMem(SwMsLink_DrvObj * pObj)
{
    Int32 retVal = FVID2_SOK;
    Vps_DeiCtxInfo deiCtxInfo;
    Vps_DeiCtxBuf deiCtxBuf;
    UInt32 chCnt, bCnt;

    for (chCnt = 0u; chCnt < pObj->cfg.dei.deiCreateParams.numCh; chCnt++)
    {
        /* Get the number of buffers to allocate */
        deiCtxInfo.channelNum = chCnt;
        retVal = FVID2_control(pObj->fvidHandle,
                               IOCTL_VPS_GET_DEI_CTX_INFO, &deiCtxInfo, NULL);
        UTILS_assert(FVID2_SOK == retVal);

        /* Allocate the buffers as requested by the driver */
        for (bCnt = 0u; bCnt < deiCtxInfo.numFld; bCnt++)
        {
            deiCtxBuf.fldBuf[bCnt] = Utils_memAlloc(deiCtxInfo.fldBufSize,
                                                    VPS_BUFFER_ALIGNMENT);
            UTILS_assert(NULL != deiCtxBuf.fldBuf[bCnt]);
        }
        for (bCnt = 0u; bCnt < deiCtxInfo.numMv; bCnt++)
        {
            deiCtxBuf.mvBuf[bCnt] = Utils_memAlloc(deiCtxInfo.mvBufSize,
                                                   VPS_BUFFER_ALIGNMENT);
            UTILS_assert(NULL != deiCtxBuf.mvBuf[bCnt]);
        }
        for (bCnt = 0u; bCnt < deiCtxInfo.numMvstm; bCnt++)
        {
            deiCtxBuf.mvstmBuf[bCnt] = Utils_memAlloc(deiCtxInfo.mvstmBufSize,
                                                      VPS_BUFFER_ALIGNMENT);
            UTILS_assert(NULL != deiCtxBuf.mvstmBuf[bCnt]);
        }

        /* Provided the allocated buffer to driver */
        deiCtxBuf.channelNum = chCnt;
        retVal = FVID2_control(pObj->fvidHandle,
                               IOCTL_VPS_SET_DEI_CTX_BUF, &deiCtxBuf, NULL);
        UTILS_assert(FVID2_SOK == retVal);
    }

    return (retVal);
}

Int32 SwMsLink_drvFreeCtxMem(SwMsLink_DrvObj * pObj)
{
    Int32 retVal = FVID2_SOK;
    Vps_DeiCtxInfo deiCtxInfo;
    Vps_DeiCtxBuf deiCtxBuf;
    UInt32 chCnt, bCnt;

    for (chCnt = 0u; chCnt < pObj->cfg.dei.deiCreateParams.numCh; chCnt++)
    {
        /* Get the number of buffers to allocate */
        deiCtxInfo.channelNum = chCnt;
        retVal = FVID2_control(pObj->fvidHandle,
                               IOCTL_VPS_GET_DEI_CTX_INFO, &deiCtxInfo, NULL);
        UTILS_assert(FVID2_SOK == retVal);

        /* Get the allocated buffer back from the driver */
        deiCtxBuf.channelNum = chCnt;
        retVal = FVID2_control(pObj->fvidHandle,
                               IOCTL_VPS_GET_DEI_CTX_BUF, &deiCtxBuf, NULL);
        UTILS_assert(FVID2_SOK == retVal);

        /* Free the buffers */
        for (bCnt = 0u; bCnt < deiCtxInfo.numFld; bCnt++)
        {
            Utils_memFree(deiCtxBuf.fldBuf[bCnt], deiCtxInfo.fldBufSize);
        }
        for (bCnt = 0u; bCnt < deiCtxInfo.numMv; bCnt++)
        {
            Utils_memFree(deiCtxBuf.mvBuf[bCnt], deiCtxInfo.mvBufSize);
        }
        for (bCnt = 0u; bCnt < deiCtxInfo.numMvstm; bCnt++)
        {
            Utils_memFree(deiCtxBuf.mvstmBuf[bCnt], deiCtxInfo.mvstmBufSize);
        }
    }

    return (retVal);
}

Int32 SwMsLink_drvCreateDeiDrv(SwMsLink_Obj * pObj, SwMsLink_DrvObj *pDrvObj)
{
    Semaphore_Params semParams;
    Vps_M2mDeiChParams *pDrvChPrm;
    UInt32 winId;
    System_LinkChInfo *pChInfo;
    FVID2_CbParams cbParams;
    FVID2_Format    *fmt;

    Semaphore_Params_init(&semParams);

    semParams.mode = Semaphore_Mode_BINARY;

    pDrvObj->complete = Semaphore_create(0u, &semParams, NULL);
    UTILS_assert(pDrvObj->complete != NULL);

    pDrvObj->cfg.dei.deiCreateParams.mode = VPS_M2M_CONFIG_PER_CHANNEL;
    if (pDrvObj->bypassDei)
    {
        pDrvObj->cfg.dei.deiCreateParams.numCh = SYSTEM_SW_MS_MAX_WIN_PER_SC;
    }
    else
    {
        pDrvObj->cfg.dei.deiCreateParams.numCh = SW_MS_MAX_DEI_CH;
    }

    pDrvObj->cfg.dei.deiCreateParams.deiHqCtxMode = VPS_DEIHQ_CTXMODE_DRIVER_ALL;
    pDrvObj->cfg.dei.deiCreateParams.chParams =
        (const Vps_M2mDeiChParams *) pDrvObj->cfg.dei.deiChParams;
    if(pDrvObj->cfg.dei.deiCreateParams.isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID]==TRUE)
        pDrvObj->cfg.dei.deiCreateParams.numVipOutStreams = 1u;
    else
        pDrvObj->cfg.dei.deiCreateParams.numVipOutStreams = 0u;

    Vps_rprintf(" %d: SWMS    : VipScReq is %s!!!\n",
                Utils_getCurTimeInMsec(),
                pDrvObj->cfg.dei.deiCreateParams.isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] ==
                    TRUE ? "TRUE" : "FALSE" );

    for (winId = 0; winId < pDrvObj->cfg.dei.deiCreateParams.numCh; winId++)
    {
        pDrvChPrm = &pDrvObj->cfg.dei.deiChParams[winId];

        /* assume all CHs are of same input size, format, pitch */
        pChInfo = &pObj->inQueInfo.chInfo[0];

        pDrvChPrm->inFmt.channelNum = winId;

        if(pChInfo->width > SW_MS_MAX_WIDTH_SUPPORTED)
        {
            pChInfo->width = SW_MS_MAX_WIDTH_SUPPORTED;
        }

        if(pChInfo->height > SW_MS_MAX_HEIGHT_SUPPORTED)
        {
            pChInfo->height = SW_MS_MAX_HEIGHT_SUPPORTED;
        }

        pDrvChPrm->inFmt.width = pChInfo->width;
        pDrvChPrm->inFmt.height = pChInfo->height;
        pDrvChPrm->inFmt.pitch[0] = pChInfo->pitch[0];
        pDrvChPrm->inFmt.pitch[1] = pChInfo->pitch[1];
        pDrvChPrm->inFmt.pitch[2] = pChInfo->pitch[2];
        pDrvChPrm->inFmt.fieldMerged[0] = FALSE;
        pDrvChPrm->inFmt.fieldMerged[1] = FALSE;
        pDrvChPrm->inFmt.fieldMerged[0] = FALSE;
        pDrvChPrm->inFmt.dataFormat = pChInfo->dataFormat;
        if (pDrvObj->bypassDei)
        {
            pDrvChPrm->inFmt.scanFormat = FVID2_SF_PROGRESSIVE;
        }
        else
        {
            pDrvChPrm->inFmt.scanFormat = FVID2_SF_INTERLACED;
        }
        pDrvChPrm->inFmt.bpp = FVID2_BPP_BITS16;

        pDrvChPrm->outFmtDei = &pDrvObj->drvOutFormat[winId];
        pDrvChPrm->outFmtDei->channelNum = winId;
        pDrvChPrm->outFmtDei->width = pObj->outFrameFormat.width;
        pDrvChPrm->outFmtDei->height = pObj->outFrameFormat.height;
        pDrvChPrm->outFmtDei->pitch[0] = pObj->outFrameFormat.pitch[0];
        pDrvChPrm->outFmtDei->pitch[1] = pObj->outFrameFormat.pitch[1];
        pDrvChPrm->outFmtDei->pitch[2] = pObj->outFrameFormat.pitch[2];
        pDrvChPrm->outFmtDei->fieldMerged[0] = FALSE;
        pDrvChPrm->outFmtDei->fieldMerged[1] = FALSE;
        pDrvChPrm->outFmtDei->fieldMerged[0] = FALSE;
        pDrvChPrm->outFmtDei->dataFormat = pObj->outFrameFormat.dataFormat;
        pDrvChPrm->outFmtDei->scanFormat = FVID2_SF_PROGRESSIVE;
        pDrvChPrm->outFmtDei->bpp = pObj->outFrameFormat.bpp;

        pDrvChPrm->inMemType = pChInfo->memType;
        pDrvChPrm->outMemTypeDei = VPS_VPDMA_MT_NONTILEDMEM;
        pDrvChPrm->outMemTypeVip[SWMS_LINK_DEI_DEF_VIP_STREAMID] =
            VPS_VPDMA_MT_NONTILEDMEM;
        pDrvChPrm->drnEnable = FALSE;
        pDrvChPrm->comprEnable = FALSE;

        pDrvChPrm->deiHqCfg = &pDrvObj->cfg.dei.deiHqCfg;
        pDrvChPrm->deiCfg = &pDrvObj->cfg.dei.deiCfg;

        pDrvChPrm->scCfg = &pDrvObj->scCfg;
        pDrvChPrm->deiCropCfg = &pDrvObj->scCropCfg[winId];

        pDrvChPrm->deiHqCfg->bypass = pDrvObj->bypassDei;
        pDrvChPrm->deiHqCfg->inpMode = VPS_DEIHQ_EDIMODE_EDI_LARGE_WINDOW;
        pDrvChPrm->deiHqCfg->tempInpEnable = TRUE;
        pDrvChPrm->deiHqCfg->tempInpChromaEnable = TRUE;
        pDrvChPrm->deiHqCfg->spatMaxBypass = FALSE;
        pDrvChPrm->deiHqCfg->tempMaxBypass = FALSE;
        pDrvChPrm->deiHqCfg->fldMode = VPS_DEIHQ_FLDMODE_5FLD;
        pDrvChPrm->deiHqCfg->lcModeEnable = TRUE;
        pDrvChPrm->deiHqCfg->mvstmEnable = TRUE;
        if (pDrvObj->bypassDei)
        {
            pDrvChPrm->deiHqCfg->tnrEnable = FALSE;
        }
        else
        {
            pDrvChPrm->deiHqCfg->tnrEnable = TRUE;
        }
        pDrvChPrm->deiHqCfg->snrEnable = TRUE;
        pDrvChPrm->deiHqCfg->sktEnable = FALSE;
        pDrvChPrm->deiHqCfg->chromaEdiEnable = TRUE;

        pDrvChPrm->deiCfg->bypass = pDrvObj->bypassDei;
        pDrvChPrm->deiCfg->inpMode = VPS_DEIHQ_EDIMODE_EDI_LARGE_WINDOW;
        pDrvChPrm->deiCfg->tempInpEnable = TRUE;
        pDrvChPrm->deiCfg->tempInpChromaEnable = TRUE;
        pDrvChPrm->deiCfg->spatMaxBypass = FALSE;
        pDrvChPrm->deiCfg->tempMaxBypass = FALSE;

        pDrvChPrm->scCfg->bypass = FALSE;
        pDrvChPrm->scCfg->nonLinear = FALSE;
        pDrvChPrm->scCfg->stripSize = 0;
        pDrvChPrm->scCfg->vsType = VPS_SC_VST_POLYPHASE;
        pDrvChPrm->scCfg->hsType = VPS_SC_HST_AUTO;
        pDrvChPrm->scCfg->enablePeaking = TRUE;

        pDrvChPrm->deiCropCfg->cropStartX = 0;
        pDrvChPrm->deiCropCfg->cropStartY = 0;
        pDrvChPrm->deiCropCfg->cropWidth = pDrvChPrm->inFmt.width;
        if (pDrvObj->bypassDei)
        {
            pDrvChPrm->deiCropCfg->cropHeight = pDrvChPrm->inFmt.height;
        }
        else
        {
            pDrvChPrm->deiCropCfg->cropHeight = pDrvChPrm->inFmt.height * 2;
        }

        /* VIP-SC settings - set some random out width & height as we dont really use the vip sc out */
        if (pDrvObj->cfg.dei.deiCreateParams.isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] == TRUE)
        {
            if (!SwMsLink_drvIsVipOutputUsed(pDrvObj->drvInstId))
            {
                pDrvChPrm->outFmtVip[SWMS_LINK_DEI_DEF_VIP_STREAMID] =
                    &pDrvObj->drvVipOutFormat[winId];
                fmt = &pDrvObj->drvVipOutFormat[winId];
                fmt->channelNum = winId;
                fmt->width = 352;
                fmt->height = 288;
                fmt->pitch[0] = pDrvChPrm->outFmtVip[SWMS_LINK_DEI_DEF_VIP_STREAMID]->width*2;
                fmt->pitch[1] = pDrvChPrm->outFmtVip[SWMS_LINK_DEI_DEF_VIP_STREAMID]->pitch[0];
                fmt->pitch[2] = 0;
                fmt->fieldMerged[0] = FALSE;
                fmt->fieldMerged[1] = FALSE;
                fmt->fieldMerged[0] = FALSE;
                fmt->dataFormat = pObj->outFrameFormat.dataFormat;
                fmt->scanFormat = FVID2_SF_PROGRESSIVE;
                fmt->bpp = pObj->outFrameFormat.bpp;

                pDrvChPrm->vipScCfg = &pDrvObj->vipScCfg;
                pDrvChPrm->vipCropCfg = &pDrvObj->vipScCropCfg[winId];

                pDrvChPrm->vipScCfg->bypass = TRUE;
                pDrvChPrm->vipScCfg->nonLinear = FALSE;
                pDrvChPrm->vipScCfg->stripSize = 0;
                pDrvChPrm->vipScCfg->vsType = VPS_SC_VST_POLYPHASE;
                pDrvChPrm->vipScCfg->hsType = VPS_SC_HST_AUTO;
                pDrvChPrm->vipScCfg->enablePeaking = TRUE;

                pDrvChPrm->vipCropCfg->cropStartX = 0;
                pDrvChPrm->vipCropCfg->cropStartY = 0;
                pDrvChPrm->vipCropCfg->cropWidth = pDrvChPrm->inFmt.width;
                if (pDrvObj->bypassDei)
                {
                    pDrvChPrm->vipCropCfg->cropHeight = pDrvChPrm->inFmt.height;
                }
                else
                {
                    pDrvChPrm->vipCropCfg->cropHeight = pDrvChPrm->inFmt.height * 2;
                }
            }
            else
            {
                pDrvChPrm->outFmtVip[SWMS_LINK_DEI_DEF_VIP_STREAMID] =
                    &pDrvObj->drvVipOutFormat[winId];
                fmt = &pDrvObj->drvVipOutFormat[winId];
                fmt->channelNum = winId;
                fmt->width = pObj->outFrameFormat.width;
                fmt->height = pObj->outFrameFormat.height;
                fmt->pitch[0] = pObj->outFrameFormat.pitch[0];
                fmt->pitch[1] = pObj->outFrameFormat.pitch[1];
                fmt->pitch[2] = pObj->outFrameFormat.pitch[2];
                fmt->fieldMerged[0] = FALSE;
                fmt->fieldMerged[1] = FALSE;
                fmt->fieldMerged[0] = FALSE;
                fmt->dataFormat = pObj->outFrameFormat.dataFormat;
                fmt->scanFormat = FVID2_SF_PROGRESSIVE;
                if (FVID2_DF_YUV420SP_UV == pObj->outFrameFormat.dataFormat)
                {
                    fmt->bpp = FVID2_BPP_BITS12;
                }
                else
                {
                    fmt->bpp = FVID2_BPP_BITS16;
                }
                pDrvChPrm->vipScCfg = &pDrvObj->vipScCfg;
                pDrvChPrm->vipCropCfg = &pDrvObj->vipScCropCfg[winId];

                pDrvChPrm->vipScCfg->bypass = FALSE;
                pDrvChPrm->vipScCfg->nonLinear = FALSE;
                pDrvChPrm->vipScCfg->stripSize = 0;
                pDrvChPrm->vipScCfg->vsType = VPS_SC_VST_POLYPHASE;
                pDrvChPrm->vipScCfg->hsType = VPS_SC_HST_AUTO;
                pDrvChPrm->vipScCfg->enablePeaking = TRUE;

                pDrvChPrm->vipCropCfg->cropStartX = 0;
                pDrvChPrm->vipCropCfg->cropStartY = 0;
                pDrvChPrm->vipCropCfg->cropWidth = pDrvChPrm->inFmt.width;
                if (pDrvObj->bypassDei)
                {
                    pDrvChPrm->vipCropCfg->cropHeight = pDrvChPrm->inFmt.height;
                }
                else
                {
                    pDrvChPrm->vipCropCfg->cropHeight = pDrvChPrm->inFmt.height * 2;
                }
            }
        }


        #if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)

        pDrvChPrm->deiHqCfg = NULL;

        /* for SC2 driver deiHqCfg and deiCfg MUST be NULL */
        if ((pDrvObj->drvInstId == VPS_M2M_INST_AUX_SC2_WB1) ||
            (pDrvObj->drvInstId == VPS_M2M_INST_AUX_SC4_VIP1) ||
            (pDrvObj->drvInstId == VPS_M2M_INST_AUX_SC2_SC4_WB1_VIP1))
        {
            pDrvChPrm->deiCfg = NULL;
        }
        #endif

        pDrvChPrm->subFrameParams = NULL;
    }

#ifdef TI_816X_BUILD
    pDrvObj->cfg.dei.deiCreateParams.enableWriteCtx = TRUE;
#endif
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
    pDrvObj->cfg.dei.deiCreateParams.enableWriteCtx = FALSE;
#endif

    memset(&cbParams, 0, sizeof(cbParams));

    cbParams.cbFxn = SwMsLink_drvFvidCb;
    cbParams.errCbFxn = SwMsLink_drvFvidErrCb;
    cbParams.errList = &pDrvObj->errCbProcessList;
    cbParams.appData = pDrvObj;

    pDrvObj->cfg.dei.deiCreateParams.numVipOutStreams = 1;

    pDrvObj->fvidHandle = FVID2_create(FVID2_VPS_M2M_DEI_DRV,
                                       pDrvObj->drvInstId,
                                       &pDrvObj->cfg.dei.deiCreateParams,
                                       &pDrvObj->cfg.dei.deiCreateStatus, &cbParams);
    UTILS_assert(pDrvObj->fvidHandle != NULL);

    if(pDrvObj->bypassDei == FALSE)
        SwMsLink_drvAllocCtxMem(pDrvObj);

    /* load co-effs only once */
    if (
        pDrvObj->bypassDei == FALSE
        ||
        ( /* if DEI is in force bypass mode,
                    then DEI in DEI mode driver is not created hence need load co-effs here
                  */
            pDrvObj->bypassDei == TRUE
            &&
            pDrvObj->forceBypassDei == TRUE
        )
        )
    {
        SwMsLink_drvSetScCoeffs(pObj, pDrvObj->fvidHandle,
                                VPS_SC_US_SET, VPS_SC_US_SET, TRUE);
    }

    pDrvObj->processList.numInLists = 1;
    pDrvObj->processList.numOutLists = 1;
    pDrvObj->processList.inFrameList[0] = &pDrvObj->inFrameList;
    pDrvObj->processList.outFrameList[0] = &pDrvObj->outFrameList;

    if (SwMsLink_drvIsVipScOnlyOutput(pDrvObj->drvInstId))
    {
        pDrvObj->processList.numOutLists = 1;
    }
    else
    {
        /* If in dual out mode, initialize VIP-SC outs to dummy frames */
        if (pDrvObj->cfg.dei.deiCreateParams.
                    isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] == TRUE)
        {
            Int32 i;

            pDrvObj->processList.numOutLists = 2;
            pDrvObj->processList.outFrameList[1] = &pObj->outFrameDropList;

            for (i=0; i<FVID2_MAX_FVID_FRAME_PTR; i++)
                pObj->outFrameDropList.frames[i] = &pObj->outFrameDrop;
            Vps_rprintf(" %d: SWMS    : OutFrames List -> %d !!!!!!!\n", Utils_getCurTimeInMsec(), pDrvObj->processList.numOutLists);
        }
    }
    return FVID2_SOK;
}

Int32 SwMsLink_drvCreateScDrv(SwMsLink_Obj * pObj, SwMsLink_DrvObj *pDrvObj)
{
    Semaphore_Params semParams;
    Vps_M2mScChParams *pDrvChPrm;
    UInt32 winId;
    System_LinkChInfo *pChInfo;
    FVID2_CbParams cbParams;

    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
    pDrvObj->complete = Semaphore_create(0u, &semParams, NULL);
    UTILS_assert(pDrvObj->complete != NULL);

    pDrvObj->cfg.sc.scCreateParams.mode = VPS_M2M_CONFIG_PER_CHANNEL;
    pDrvObj->cfg.sc.scCreateParams.numChannels = SYSTEM_SW_MS_MAX_WIN_PER_SC;
    pDrvObj->cfg.sc.scCreateParams.chParams
        = (Vps_M2mScChParams *) pDrvObj->cfg.sc.scChParams;

    for (winId = 0; winId < pDrvObj->cfg.sc.scCreateParams.numChannels; winId++)
    {
        pDrvChPrm = &pDrvObj->cfg.sc.scChParams[winId];

        /* assume all CHs are of same input size, format, pitch */
        pChInfo = &pObj->inQueInfo.chInfo[0];

        pDrvChPrm->inFmt.channelNum = winId;

        if(pChInfo->width > SW_MS_MAX_WIDTH_SUPPORTED)
        {
            pChInfo->width = SW_MS_MAX_WIDTH_SUPPORTED;
        }

        if(pChInfo->height > SW_MS_MAX_HEIGHT_SUPPORTED)
        {
            pChInfo->height = SW_MS_MAX_HEIGHT_SUPPORTED;
        }

        pDrvChPrm->inFmt.width = pChInfo->width;
        pDrvChPrm->inFmt.height = pChInfo->height;
        pDrvChPrm->inFmt.pitch[0] = pChInfo->pitch[0];
        pDrvChPrm->inFmt.pitch[1] = pChInfo->pitch[1];
        pDrvChPrm->inFmt.pitch[2] = pChInfo->pitch[2];
        pDrvChPrm->inFmt.fieldMerged[0] = FALSE;
        pDrvChPrm->inFmt.fieldMerged[1] = FALSE;
        pDrvChPrm->inFmt.fieldMerged[0] = FALSE;
        pDrvChPrm->inFmt.dataFormat = pChInfo->dataFormat;
        pDrvChPrm->inFmt.scanFormat = FVID2_SF_PROGRESSIVE;
        pDrvChPrm->inFmt.bpp = FVID2_BPP_BITS16;

        pDrvChPrm->outFmt.channelNum = winId;
        pDrvChPrm->outFmt.width = pObj->outFrameFormat.width;
        pDrvChPrm->outFmt.height = pObj->outFrameFormat.height;
        pDrvChPrm->outFmt.pitch[0] = pObj->outFrameFormat.pitch[0];
        pDrvChPrm->outFmt.pitch[1] = pObj->outFrameFormat.pitch[1];
        pDrvChPrm->outFmt.pitch[2] = pObj->outFrameFormat.pitch[2];
        pDrvChPrm->outFmt.fieldMerged[0] = FALSE;
        pDrvChPrm->outFmt.fieldMerged[1] = FALSE;
        pDrvChPrm->outFmt.fieldMerged[0] = FALSE;
        pDrvChPrm->outFmt.dataFormat = pObj->outFrameFormat.dataFormat;
        pDrvChPrm->outFmt.scanFormat = FVID2_SF_PROGRESSIVE;
        pDrvChPrm->outFmt.bpp = pObj->outFrameFormat.bpp;

        pDrvChPrm->inMemType = pChInfo->memType;
        pDrvChPrm->outMemType = VPS_VPDMA_MT_NONTILEDMEM;

        pDrvChPrm->scCfg = &pDrvObj->scCfg;
        pDrvChPrm->srcCropCfg = &pDrvObj->scCropCfg[winId];

        pDrvChPrm->scCfg->bypass = FALSE;
        pDrvChPrm->scCfg->nonLinear = FALSE;
        pDrvChPrm->scCfg->stripSize = 0;
        pDrvChPrm->scCfg->vsType = VPS_SC_VST_POLYPHASE;
        pDrvChPrm->scCfg->hsType = VPS_SC_HST_AUTO;
        pDrvChPrm->scCfg->enablePeaking = TRUE;

        pDrvChPrm->srcCropCfg->cropStartX = 0;
        pDrvChPrm->srcCropCfg->cropStartY = 0;
        pDrvChPrm->srcCropCfg->cropWidth = pDrvChPrm->inFmt.width;
        pDrvChPrm->srcCropCfg->cropHeight = pDrvChPrm->inFmt.height;
    }

    memset(&cbParams, 0, sizeof(cbParams));
    cbParams.cbFxn = SwMsLink_drvFvidCb;
    cbParams.errCbFxn = SwMsLink_drvFvidErrCb;
    cbParams.errList = &pDrvObj->errCbProcessList;
    cbParams.appData = pDrvObj;

    pDrvObj->fvidHandle = FVID2_create(FVID2_VPS_M2M_SC_DRV,
                                       pDrvObj->drvInstId,
                                       &pDrvObj->cfg.sc.scCreateParams,
                                       &pDrvObj->cfg.sc.scCreateStatus, &cbParams);
    UTILS_assert(pDrvObj->fvidHandle != NULL);

    SwMsLink_drvSetScCoeffs(pObj, pDrvObj->fvidHandle,
                            VPS_SC_US_SET, VPS_SC_US_SET, TRUE);

    pDrvObj->processList.numInLists = 1;
    pDrvObj->processList.numOutLists = 1;
    pDrvObj->processList.inFrameList[0] = &pDrvObj->inFrameList;
    pDrvObj->processList.outFrameList[0] = &pDrvObj->outFrameList;

    return FVID2_SOK;
}

Int32 SwMsLink_drvCreate(SwMsLink_Obj * pObj, SwMsLink_CreateParams * pPrm)
{
    Semaphore_Params semParams;
    Clock_Params clockParams;
    UInt32 winId;
    Int32 status;
    UInt32 i, j;
    Bool vip0Exist = FALSE;
    Bool sc5Exist  = FALSE;
#ifdef TI_816X_BUILD
    Vps_PlatformCpuRev cpuRev;
    UInt32 chId;
#endif

#ifdef SYSTEM_DEBUG_SWMS
    Vps_printf(" %d: SWMS: Create in progress !!!\n", Utils_getCurTimeInMsec());
#endif

    UTILS_MEMLOG_USED_START();
    SwMsLink_drvResetAvgStatistics(pObj);
    pObj->inFramePutCount = 0;
    pObj->inFrameGetCount = 0;

    pObj->frameCount = 0;
    pObj->totalTime = 0;

    pObj->skipProcessing = 0;
    pObj->switchLayout = FALSE;
    pObj->rtParamUpdate = FALSE;
    pObj->vipLockRequired = FALSE;
    pObj->enableBufCopy = TRUE;

    memcpy(&pObj->createArgs, pPrm, sizeof(*pPrm));

    if(pObj->createArgs.numOutBuf==0)
        pObj->createArgs.numOutBuf = SW_MS_LINK_MAX_OUT_FRAMES_DEFAULT;

    if(pObj->createArgs.numOutBuf>SW_MS_LINK_MAX_OUT_FRAMES)
        pObj->createArgs.numOutBuf = SW_MS_LINK_MAX_OUT_FRAMES;

    memset(pObj->winObj, 0, sizeof(pObj->winObj));

    for(i = 0; i < (2 * SYSTEM_SW_MS_MAX_INST); i++)
    {
        memset(&pObj->DrvObj[i], 0, sizeof(pObj->DrvObj[i]));
        memset(&pObj->DrvObj[i].win2ScLChMap,
                0xFF, sizeof(pObj->DrvObj[i].win2ScLChMap));
    }
    /* Mark channel to instance map as in-valid */
    memset(&pObj->chNum2InstIdMap[0], 0xFF, sizeof(pObj->chNum2InstIdMap));

    /* No update of RT prams required due to channel map reorder */
    pObj->scChMapRtParamUpdate = FALSE;

    status = System_linkGetInfo(pPrm->inQueParams.prevLinkId, &pObj->inTskInfo);
    UTILS_assert(status == FVID2_SOK);
    UTILS_assert(pPrm->inQueParams.prevLinkQueId < pObj->inTskInfo.numQue);

    memcpy(&pObj->inQueInfo,
           &pObj->inTskInfo.queInfo[pPrm->inQueParams.prevLinkQueId],
           sizeof(pObj->inQueInfo));

#ifdef TI_816X_BUILD
    cpuRev = Vps_platformGetCpuRev();
    for (chId = 0; chId < pObj->inQueInfo.numCh; chId++)
    {
        if (cpuRev < VPS_PLATFORM_CPU_REV_2_0)
        {
            if((pObj->inQueInfo.chInfo[chId].width >
                DEI_SC_DRV_422FMT_MAX_WIDTH_LIMIT_BEFORE_CPU_REV_2_0) &&
               ((pObj->inQueInfo.chInfo[chId].dataFormat != FVID2_DF_YUV420SP_UV) &&
                (pObj->inQueInfo.chInfo[chId].dataFormat != FVID2_DF_YUV420SP_VU) &&
                (pObj->inQueInfo.chInfo[chId].dataFormat != FVID2_DF_YUV420P)))
            {

                Vps_printf(" %u: Warning: This CPU Revision [%s] does not"
                             "support current set width %d\n",\
                    Utils_getCurTimeInMsec(), gVpss_cpuVer[cpuRev],\
                                              pObj->inQueInfo.chInfo[chId].width);
                Vps_printf(" %u: Warning: Limiting Input width to 960\n",
                          Utils_getCurTimeInMsec());
                {
                    pObj->inQueInfo.chInfo[chId].width =
                          DEI_SC_DRV_422FMT_MAX_WIDTH_LIMIT_BEFORE_CPU_REV_2_0;
                }
            }
        }
    }
#endif

    SwMsLink_drvGetTimerPeriod(pObj, &pPrm->layoutPrm);
    SwMsLink_drvCreateDupObj(pObj);

    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
    pObj->lock = Semaphore_create(1u, &semParams, NULL);
    UTILS_assert(pObj->lock != NULL);

    Clock_Params_init(&clockParams);
    clockParams.period = pObj->timerPeriod;
    clockParams.arg = (UArg) pObj;

    pObj->timer = Clock_create(SwMsLink_drvTimerCb,
                               pObj->timerPeriod, &clockParams, NULL);
    UTILS_assert(pObj->timer != NULL);

    SwMsLink_drvCreateOutInfo(pObj, pPrm->maxOutRes);

    status = SwMsLink_drvDmaCreate(pObj);
    UTILS_assert(0 == status);

    for (winId = 0; winId < SYSTEM_SW_MS_MAX_WIN; winId++)
    {
        SwMsLink_drvCreateWinObj(pObj, winId);
    }

    for (i = 0; i < pObj->createArgs.numSwMsInst; i++)
    {
        if (pObj->createArgs.swMsInstId[i] == SYSTEM_SW_MS_SC_INST_VIP0_SC)
            vip0Exist = TRUE;
        if (pObj->createArgs.swMsInstId[i] == SYSTEM_SW_MS_SC_INST_SC5)
            sc5Exist = TRUE;
        if(vip0Exist && sc5Exist)
        {
            Vps_printf("Didn't support SC5 and VIP0 both exist now !!!\n");
            UTILS_assert(0);
        }
    }

    SwMsLink_creatChanToInstMap(pObj);

    for (i = 0; i < pObj->createArgs.numSwMsInst; i++)
    {
        pObj->DrvObj[i].forceBypassDei = FALSE;

        /* Set the VIP Required flag to false */
        for (j = 0; j < VPS_M2M_DEI_MAX_VIP_STREAMS; j ++)
        {
            pObj->DrvObj[i].cfg.dei.deiCreateParams.isVipScReq[j] = FALSE;
            pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.isVipScReq[j] = FALSE;
        }

        switch (pObj->createArgs.swMsInstId[i])
        {
            case SYSTEM_SW_MS_SC_INST_DEIHQ_SC_NO_DEI:
                pObj->DrvObj[i].forceBypassDei = TRUE;
                /* rest of DrvObj setup would be same as DEI SW MS setup, hence no 'break' */

            case SYSTEM_SW_MS_SC_INST_DEIHQ_SC:
                pObj->DrvObj[i].isDeiDrv = TRUE;
                pObj->DrvObj[i].bypassDei = FALSE;
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                /* Use DEI-SC1 + VIP0-SC3 dual out mode only for 814x
                    * Mainly used in some Ce usecases where DEI link as well swMSLink opens same
                    * DrvInstances. 2 different driver instances - 1 using both single out mode & other in dual mode>
                    * sharing same SC intance is not supported, we open in dual out mode in swMS as well.
                    * We use VIP outframes as null frames <memset to 0> in order to not generate any output for VIP-SC
                    * So, effectively we get single output
                    */
                if(pObj->createArgs.includeVipScInDrvPath==TRUE)
                {
                    pObj->DrvObj[i].drvInstId = VPS_M2M_INST_MAIN_DEI_SC1_SC3_WB0_VIP0;
                    pObj->DrvObj[i].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;
                    pObj->vipLockRequired = TRUE;
                    pObj->vipInstId = SYSTEM_VIP_0;
                }
                else
                {
                    pObj->DrvObj[i].drvInstId = VPS_M2M_INST_MAIN_DEI_SC1_WB0;
                    pObj->DrvObj[i].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = FALSE;
                }
#else
                if(pObj->createArgs.includeVipScInDrvPath==FALSE)
                {
                    pObj->DrvObj[i].drvInstId = VPS_M2M_INST_MAIN_DEIH_SC1_WB0;
                    pObj->DrvObj[i].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = FALSE;
                }
                else
                {
                    pObj->DrvObj[i].drvInstId = VPS_M2M_INST_MAIN_DEIH_SC1_SC3_WB0_VIP0;
                    pObj->DrvObj[i].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;
                    pObj->vipLockRequired = TRUE;
                    pObj->vipInstId = SYSTEM_VIP_0;

                }
#endif
                if(pObj->DrvObj[i].forceBypassDei==FALSE)
                {
                    /* create DEI driver in DEI mode only if force DEI bypass is FALSE */
                    SwMsLink_drvCreateDeiDrv(pObj, &pObj->DrvObj[i]);
                }
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].isDeiDrv = TRUE;
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].bypassDei = TRUE;
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].forceBypassDei = pObj->DrvObj[i].forceBypassDei;
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                if(pObj->createArgs.includeVipScInDrvPath==TRUE)
                {
                    /* Use DEI-SC1 + VIP0-SC3 dual out mode only for 814x */
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_MAIN_DEI_SC1_SC3_WB0_VIP0;
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;
                    pObj->vipLockRequired = TRUE;
                    pObj->vipInstId = SYSTEM_VIP_0;
                }
                else
                {
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_MAIN_DEI_SC1_WB0;
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = FALSE;
                }
#else
                if(pObj->createArgs.includeVipScInDrvPath==FALSE)
                {
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_MAIN_DEIH_SC1_WB0;
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.
                        isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = FALSE;
                }
                else
                {
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_MAIN_DEIH_SC1_SC3_WB0_VIP0;
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.
                        isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;
                    pObj->vipLockRequired = TRUE;
                    pObj->vipInstId = SYSTEM_VIP_0;

                }
#endif
                SwMsLink_drvCreateDeiDrv(pObj, &pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST]);
                break;

            case SYSTEM_SW_MS_SC_INST_DEI_SC_NO_DEI:
                pObj->DrvObj[i].forceBypassDei = TRUE;
                /* rest of DrvObj setup would be same as DEI SW MS setup, hence no 'break' */

            case SYSTEM_SW_MS_SC_INST_DEI_SC:
                pObj->DrvObj[i].isDeiDrv = TRUE;
                pObj->DrvObj[i].bypassDei = FALSE;

#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                /* Use DEI-SC2 + VIP1-SC4 dual out mode only for 814x
                    * Mainly used in some Ce usecases where DEI link as well swMSLink opens same
                    * DrvInstances. 2 different driver instances - 1 using both single out mode & other in dual mode>
                    * sharing same SC intance is not supported, we open in dual out mode in swMS as well.
                    * We use VIP outframes as null frames <memset to 0> in order to not generate any output for VIP-SC
                    * So, effectively we get single output
                    */
                if(pObj->createArgs.includeVipScInDrvPath==TRUE)
                {
                    pObj->DrvObj[i].drvInstId = VPS_M2M_INST_AUX_SC2_SC4_WB1_VIP1;
                    pObj->DrvObj[i].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;
                    pObj->vipLockRequired = TRUE;
                    pObj->vipInstId = SYSTEM_VIP_1;
                }
                else
                {
                    pObj->DrvObj[i].drvInstId = VPS_M2M_INST_AUX_SC2_WB1;
                    pObj->DrvObj[i].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = FALSE;
                }
#else
                if(pObj->createArgs.includeVipScInDrvPath==FALSE)
                {
                    pObj->DrvObj[i].drvInstId = VPS_M2M_INST_AUX_DEI_SC2_WB1;
                    pObj->DrvObj[i].cfg.dei.deiCreateParams.
                        isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = FALSE;
                }
                else
                {
                    pObj->DrvObj[i].drvInstId = VPS_M2M_INST_AUX_DEI_SC2_SC4_WB1_VIP1;
                    pObj->DrvObj[i].cfg.dei.deiCreateParams.
                        isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;
                    pObj->vipLockRequired = TRUE;
                    pObj->vipInstId = SYSTEM_VIP_1;
                }
#endif
                if(pObj->DrvObj[i].forceBypassDei==FALSE)
                {
                    SwMsLink_drvCreateDeiDrv(pObj, &pObj->DrvObj[i]);
                }
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].isDeiDrv = TRUE;
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].bypassDei = TRUE;
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].forceBypassDei = pObj->DrvObj[i].forceBypassDei;

#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                if(pObj->createArgs.includeVipScInDrvPath==TRUE)
                {
                    /* Use DEI-SC2 + VIP1-SC4 dual out mode only for 814x */
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_AUX_SC2_SC4_WB1_VIP1;
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;
                    pObj->vipLockRequired = TRUE;
                    pObj->vipInstId = SYSTEM_VIP_1;
                }
                else
                {
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_AUX_SC2_WB1;
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = FALSE;
                }
#else
                if(pObj->createArgs.includeVipScInDrvPath==FALSE)
                {
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_AUX_DEI_SC2_WB1;
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = FALSE;
                }
                else
                {
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_AUX_DEI_SC2_SC4_WB1_VIP1;
                    pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.
                            isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;
                    pObj->vipLockRequired = TRUE;
                    pObj->vipInstId = SYSTEM_VIP_1;
                }
#endif
                SwMsLink_drvCreateDeiDrv(pObj, &pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST]);
                break;

            case SYSTEM_SW_MS_SC_INST_VIP0_SC:
                pObj->DrvObj[i].isDeiDrv = FALSE;
                pObj->DrvObj[i].drvInstId = VPS_M2M_INST_SEC0_SC3_VIP0;
                SwMsLink_drvCreateScDrv(pObj,&pObj->DrvObj[i]);
                break;
            case SYSTEM_SW_MS_SC_INST_VIP1_SC:
                pObj->DrvObj[i].isDeiDrv = FALSE;
                pObj->DrvObj[i].drvInstId = VPS_M2M_INST_SEC1_SC4_VIP1;
                SwMsLink_drvCreateScDrv(pObj,&pObj->DrvObj[i]);
                break;
            case SYSTEM_SW_MS_SC_INST_SC5:
                pObj->DrvObj[i].isDeiDrv = FALSE;
                pObj->DrvObj[i].drvInstId = VPS_M2M_INST_SEC0_SC5_WB2;
                SwMsLink_drvCreateScDrv(pObj,&pObj->DrvObj[i]);
                break;
            case SYSTEM_SW_MS_SC_INST_VIP1_SC_NO_DEI:
                pObj->DrvObj[i].forceBypassDei = TRUE;
                /* rest of DrvObj setup would be same as DEI SW MS setup, hence no 'break' */
                pObj->DrvObj[i].isDeiDrv = TRUE;
                pObj->DrvObj[i].bypassDei = FALSE;
                pObj->DrvObj[i].cfg.dei.deiCreateParams.
                        isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                /* Use DEI-SC2 + VIP1-SC4 dual out mode only for 814x
                    * Mainly used in some Ce usecases where DEI link as well swMSLink opens same
                    * DrvInstances. 2 different driver instances - 1 using both single out mode & other in dual mode>
                    * sharing same SC intance is not supported, we open in dual out mode in swMS as well.
                    * We use VIP outframes as null frames <memset to 0> in order to not generate any output for VIP-SC
                    * So, effectively we get single output
                    */
                pObj->DrvObj[i].drvInstId = VPS_M2M_INST_AUX_SC4_VIP1;
                pObj->vipLockRequired = TRUE;
                pObj->vipInstId = SYSTEM_VIP_1;
#else
                pObj->DrvObj[i].drvInstId = VPS_M2M_INST_AUX_DEI_SC4_VIP1;
                pObj->vipLockRequired = TRUE;
                pObj->vipInstId = SYSTEM_VIP_1;
#endif
                if(pObj->DrvObj[i].forceBypassDei==FALSE)
                {
                    SwMsLink_drvCreateDeiDrv(pObj, &pObj->DrvObj[i]);
                }
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].isDeiDrv = TRUE;
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].bypassDei = TRUE;
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].forceBypassDei = pObj->DrvObj[i].forceBypassDei;
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.
                    isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;

#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                /* Use DEI-SC2 + VIP1-SC4 dual out mode only for 814x */
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_AUX_SC4_VIP1;
                pObj->vipLockRequired = TRUE;
                pObj->vipInstId = SYSTEM_VIP_1;
#else
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_AUX_DEI_SC4_VIP1;
                pObj->vipLockRequired = TRUE;
                pObj->vipInstId = SYSTEM_VIP_1;
#endif
                SwMsLink_drvCreateDeiDrv(pObj, &pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST]);
                break;
            case SYSTEM_SW_MS_SC_INST_VIP0_SC_NO_DEI:
                pObj->DrvObj[i].forceBypassDei = TRUE;

            case SYSTEM_SW_MS_SC_INST_VIP0_DEI_SC:
                /* rest of DrvObj setup would be same as DEI SW MS setup, hence no 'break' */
                pObj->DrvObj[i].isDeiDrv = TRUE;
                pObj->DrvObj[i].bypassDei = FALSE;
                pObj->DrvObj[i].cfg.dei.deiCreateParams.
                        isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                /* Use DEI-SC2 + VIP1-SC4 dual out mode only for 814x
                    * Mainly used in some Ce usecases where DEI link as well swMSLink opens same
                    * DrvInstances. 2 different driver instances - 1 using both single out mode & other in dual mode>
                    * sharing same SC intance is not supported, we open in dual out mode in swMS as well.
                    * We use VIP outframes as null frames <memset to 0> in order to not generate any output for VIP-SC
                    * So, effectively we get single output
                    */
                pObj->DrvObj[i].drvInstId = VPS_M2M_INST_MAIN_DEI_SC3_VIP0;
                pObj->vipLockRequired = TRUE;
                pObj->vipInstId = SYSTEM_VIP_0;
#else
                pObj->DrvObj[i].drvInstId = VPS_M2M_INST_MAIN_DEIH_SC3_VIP0;
                pObj->vipLockRequired = TRUE;
                pObj->vipInstId = SYSTEM_VIP_0;
#endif
                if(pObj->DrvObj[i].forceBypassDei==FALSE)
                {
                    SwMsLink_drvCreateDeiDrv(pObj, &pObj->DrvObj[i]);
                }
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].isDeiDrv = TRUE;
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].bypassDei = TRUE;
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].forceBypassDei = pObj->DrvObj[i].forceBypassDei;
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].cfg.dei.deiCreateParams.
                    isVipScReq[SWMS_LINK_DEI_DEF_VIP_STREAMID] = TRUE;

#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
                /* Use DEI-SC2 + VIP1-SC4 dual out mode only for 814x */
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_MAIN_DEI_SC3_VIP0;
                pObj->vipLockRequired = TRUE;
                pObj->vipInstId = SYSTEM_VIP_0;
#else
                pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].drvInstId = VPS_M2M_INST_MAIN_DEIH_SC3_VIP0;
                pObj->vipLockRequired = TRUE;
                pObj->vipInstId = SYSTEM_VIP_0;
#endif
                SwMsLink_drvCreateDeiDrv(pObj, &pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST]);
                break;
            default:
                UTILS_assert(0);
                break;
        }
    }
    SwMsLink_drvCreateChannelObj(pObj);
    SwMsLink_drvUpdateRtChannelInfo(pObj);
    SwMsLink_drvSwitchLayout(pObj, &pObj->createArgs.layoutPrm, FALSE,TRUE);
    UTILS_MEMLOG_USED_END(pObj->memUsed);
    UTILS_MEMLOG_PRINT("SWMS",pObj->memUsed,UTILS_ARRAYSIZE(pObj->memUsed));

#ifdef SYSTEM_DEBUG_SWMS
    Vps_printf(" %d: SWMS: Create Done !!!\n", Utils_getCurTimeInMsec());
#endif

    return FVID2_SOK;
}

static
Int32 SwMsLink_freeFrame(System_LinkInQueParams * pInQueParams,
                         FVID2_FrameList * frameList, FVID2_Frame * pFrame)
{
    frameList->frames[frameList->numFrames] = pFrame;
    frameList->numFrames++;

    UTILS_assert (frameList->numFrames < FVID2_MAX_FVID_FRAME_PTR);
    if (frameList->numFrames >= (FVID2_MAX_FVID_FRAME_PTR/4))
    {
        System_putLinksEmptyFrames(pInQueParams->prevLinkId,
                                   pInQueParams->prevLinkQueId, frameList);

        frameList->numFrames = 0;
    }

    return FVID2_SOK;
}

Int32 SwMsLink_drvProcessData(SwMsLink_Obj * pObj)
{
    System_LinkInQueParams *pInQueParams;
    FVID2_FrameList frameList;

    pInQueParams = &pObj->createArgs.inQueParams;

    System_getLinksFullFrames(pInQueParams->prevLinkId,
                              pInQueParams->prevLinkQueId, &frameList);

    pObj->freeFrameList.numFrames = 0;

    if (frameList.numFrames)
    {
        SwMsLink_drvLock(pObj);
        SwMsLink_drvQueueInputFrames(pObj,&frameList);
        SwMsLink_drvUnlock(pObj);

        if (pObj->freeFrameList.numFrames)
        {
            pObj->inFramePutCount += pObj->freeFrameList.numFrames;
            System_putLinksEmptyFrames(pInQueParams->prevLinkId,
                                       pInQueParams->prevLinkQueId,
                                       &pObj->freeFrameList);
        }
    }

    return FVID2_SOK;
}

Int32 SwMsLink_dmaCopyWin (SwMsLink_Obj *pObj)
{
    Utils_DmaCopy2D dmaPrm;
    Int32 status=FVID2_SOK, i;
    UInt32 dstBufferPitch;

#ifdef SYSTEM_DEBUG_SWMS_RT
    Vps_printf(" %d: SWMS: Dma begin !!!\n", Utils_getCurTimeInMsec());
#endif

    dstBufferPitch = pObj->info.queInfo[0].chInfo[0].pitch[0];

    for (i=0; i<pObj->winCopyObj.numWindows; i++)
    {
        dmaPrm.destAddr[0]  = pObj->winCopyObj.cpObj[i].pDstWindow[0];
        dmaPrm.srcAddr[0]  = pObj->winCopyObj.cpObj[i].pSrcWindow[0];
        dmaPrm.dataFormat = pObj->createArgs.outDataFormat;

        if (SYSTEM_DF_YUV420SP_UV == dmaPrm.dataFormat)
        {
            dmaPrm.destAddr[1]  = pObj->winCopyObj.cpObj[i].pDstWindow[1];
            dmaPrm.srcAddr[1]  = pObj->winCopyObj.cpObj[i].pSrcWindow[1];
        }

        dmaPrm.width      = pObj->winCopyObj.cpObj[i].winWidth;
        dmaPrm.height     = pObj->winCopyObj.cpObj[i].winHeight;
        dmaPrm.destPitch[0] = dstBufferPitch;
        dmaPrm.destPitch[1] = dstBufferPitch;
        dmaPrm.srcPitch[0] = pObj->winCopyObj.cpObj[i].srcPitch;
        dmaPrm.srcPitch[1] = pObj->winCopyObj.cpObj[i].srcPitch;

        dmaPrm.srcStartX  = 0;
        dmaPrm.srcStartY  = 0;
        dmaPrm.destStartX = 0;
        dmaPrm.destStartY = 0;

        status = Utils_dmaCopy2D(&pObj->dmaObj, &dmaPrm, 1);
        UTILS_assert(status==FVID2_SOK);
    }

    pObj->winCopyObj.numWindows = 0;

#ifdef SYSTEM_DEBUG_SWMS_RT
    Vps_printf(" %d: SWMS: Dma end !!!\n", Utils_getCurTimeInMsec());
#endif

    return FVID2_SOK;
}

Int32 SwMsLink_dmaWinCopyObjUpdateOutBuf2OutBuf (SwMsLink_Obj *pObj,
                                    FVID2_Frame *pOutFrame,
                                    SwMsLink_LayoutWinInfo *pWinInfo)
{

    UTILS_assert(pObj->winCopyObj.numWindows < UTILS_ARRAYSIZE(pObj->winCopyObj.cpObj));

    FVID2_Frame *lastOutBufPtr = pObj->lastOutBufPtr;
    pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].pDstWindow[0] =
        (Ptr) ((UInt32) pOutFrame->addr[0][0] + pWinInfo->bufAddrOffset[0]);
    pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].pSrcWindow[0] =
        (Ptr) ((UInt32) lastOutBufPtr->addr[0][0] + pWinInfo->bufAddrOffset[0]);

    if (pObj->createArgs.outDataFormat == SYSTEM_DF_YUV420SP_UV)
    {
        pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].pDstWindow[1] =
            (Ptr) ((UInt32) pOutFrame->addr[0][1] + pWinInfo->bufAddrOffset[1]);
        pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].pSrcWindow[1] =
            (Ptr) ((UInt32) lastOutBufPtr->addr[0][1] + pWinInfo->bufAddrOffset[1]);
    }

    pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].winWidth =
                                                        pWinInfo->width;
    pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].winHeight =
                                                        pWinInfo->height;
    pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].srcPitch =
                                 pObj->info.queInfo[0].chInfo[0].pitch[0];
    pObj->winCopyObj.numWindows++;

    return FVID2_SOK;
}

Int32 SwMsLink_dmaWinCopyObjUpdateInBuf2OutBuf (SwMsLink_Obj *pObj,
                                    FVID2_Frame *pOutFrame,
                                    FVID2_Frame *pInFrame,
                                    SwMsLink_LayoutWinInfo *pWinInfo)
{
    UInt32 channelNum;

    UTILS_assert(pObj->winCopyObj.numWindows < UTILS_ARRAYSIZE(pObj->winCopyObj.cpObj));

    pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].pDstWindow[0] =
        (Ptr) ((UInt32) pOutFrame->addr[0][0] + pWinInfo->bufAddrOffset[0]);
    pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].pSrcWindow[0] =
        (Ptr) (pInFrame->addr[0][0]);

    if (pObj->createArgs.outDataFormat == SYSTEM_DF_YUV420SP_UV)
    {
        pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].pDstWindow[1] =
            (Ptr) ((UInt32) pOutFrame->addr[0][1] + pWinInfo->bufAddrOffset[1]);
        pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].pSrcWindow[1] =
            (Ptr) (pInFrame->addr[0][1]);
    }

    pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].winWidth =
                                                        pWinInfo->width;
    pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].winHeight =
                                                        pWinInfo->height;
    UTILS_assert(pInFrame->appData != NULL);
    channelNum = ((System_FrameInfo *)(pInFrame->appData))->swmsOrgChannelNum;
    UTILS_assert(channelNum < pObj->inQueInfo.numCh);
    pObj->winCopyObj.cpObj[pObj->winCopyObj.numWindows].srcPitch =
            pObj->inTskInfo.queInfo[pObj->createArgs.inQueParams.prevLinkQueId].chInfo[channelNum].pitch[0];

    pObj->winCopyObj.numWindows++;

    return FVID2_SOK;
}

Bool SwMsLink_cpySrc2DestPossible(SwMsLink_OutWinObj *pWinObj)
{
    Bool cpyPossible = FALSE;

    if ((pWinObj->scRtInFrmPrm.width == pWinObj->scRtOutFrmPrm.width)
        &&
        (pWinObj->scRtInFrmPrm.height == pWinObj->scRtOutFrmPrm.height)
        &&
        (pWinObj->scRtInFrmPrm.dataFormat == pWinObj->scRtOutFrmPrm.dataFormat))
    {
        cpyPossible = TRUE;
    }
    return cpyPossible;
}

static
Bool  SwMsLink_drvIsBlankFrameRtParamsApplied(SwMsLink_OutWinObj *pWinObj, SwMsLink_Obj * pObj)
{
    Bool isBlankFrameRtParamsApplied =
        (
          (pWinObj->deiRtPrm.deiInFrmPrms == &pObj->blankFrameScRtInFrmPrm) &&
          (pWinObj->deiRtPrm.deiScCropCfg == &pObj->blankFrameScRtCropCfg)  &&
          (pWinObj->scRtPrm.inFrmPrms == &pObj->blankFrameScRtInFrmPrm)     &&
          (pWinObj->scRtPrm.srcCropCfg == &pObj->blankFrameScRtCropCfg)
        );
    return isBlankFrameRtParamsApplied;
}

static
Void  SwMsLink_drvApplyBlankFrameRtParams(UInt32 drvInst, SwMsLink_OutWinObj *pWinObj, SwMsLink_Obj * pObj)
{
    pWinObj->deiRtPrm.deiInFrmPrms = &pObj->blankFrameScRtInFrmPrm;
    pWinObj->deiRtPrm.deiScCropCfg = &pObj->blankFrameScRtCropCfg;
    pWinObj->scRtPrm.inFrmPrms = &pObj->blankFrameScRtInFrmPrm;
    pWinObj->scRtPrm.srcCropCfg = &pObj->blankFrameScRtCropCfg;
    if (SwMsLink_drvIsVipOutputUsed(pObj->DrvObj[drvInst].drvInstId))
    {
        pWinObj->deiRtPrm.vipScCropCfg = &pObj->blankFrameScRtCropCfg;
    }
}

static
Void  SwMsLink_drvRestoreBlankFrameRtParams(SwMsLink_Obj * pObj)
{
    UInt32  maxChnl;
    UInt32 winId;
    SwMsLink_OutWinObj *pWinObj;

    maxChnl = pObj->maxWinId + 1;

    for (winId = 0; winId < maxChnl; winId++)
    {
        pWinObj = &pObj->winObj[winId];
        if (winId >= pObj->layoutParams.numWin)
        {
            continue;
        }
        if (pWinObj->isBlankFrameDisplay)
        {
            if (!SwMsLink_drvIsBlankFrameRtParamsApplied(pWinObj, pObj))
            {
                Vps_printf("SWMS:Restoring blank frame params for winId:%d",winId);
                SwMsLink_drvApplyBlankFrameRtParams(SwMsLink_getDrvInstFromWinId(pObj,winId), pWinObj, pObj);
            }
        }
    }
}

Int32 SwMsLink_drvMakeFrameLists(SwMsLink_Obj * pObj, FVID2_Frame * pOutFrame)
{
    UInt32 winId;
    SwMsLink_LayoutWinInfo *pWinInfo;
    SwMsLink_OutWinObj *pWinObj;
    SwMsLink_DrvObj *pDrvObj;
    FVID2_Frame *pInFrame;
    Bool repeatFld;
    System_LinkInQueParams *pInQueParams;
    UInt32                  i, drvInst;
    Bool rtParamUpdatePerFrame;
    System_FrameInfo *pFrameInfo;
    System_LinkChInfo *rtChannelInfo;
    UInt32  maxChnl;
    Bool isInputReceived;
    Bool copyInBuf2OutBuf;
    Bool copyOutBuf2OutBuf;
#ifdef SWMS_ENABLE_MODIFY_FRAME_POINTER_ALWAYS
    SwMsLink_chDyHmpSetInputCrop *hmpCrop;
#endif

    pInQueParams = &pObj->createArgs.inQueParams;

    for (i = 0; i < pObj->createArgs.numSwMsInst; i++)
    {
        pObj->DrvObj[i].inFrameList.numFrames = 0;
        pObj->DrvObj[i].outFrameList.numFrames = 0;
        if (pObj->DrvObj[i].isDeiDrv)
        {
            pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].inFrameList.numFrames = 0;
            pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].outFrameList.numFrames = 0;
        }
    }

    pObj->rtParamUpdate = FALSE;
    pObj->freeFrameList.numFrames = 0;
    pObj->winCopyObj.numWindows = 0;
    SwMsLink_drvCheckBlankOutputBuffer(pObj,pOutFrame);

    maxChnl = pObj->maxWinId + 1;

    if (maxChnl > SYSTEM_SW_MS_MAX_WIN)
        maxChnl = SYSTEM_SW_MS_MAX_WIN;

    SwMsLink_drvUpdateChCurrentInFrame(pObj);
    for (winId = 0; winId < maxChnl; winId++)
    {
        pWinObj = &pObj->winObj[winId];
        pWinInfo = &pObj->layoutParams.winInfo[winId];
        if (winId >= pObj->layoutParams.numWin)
        {
            continue;
        }

        drvInst = SwMsLink_getDrvInstFromWinId(pObj, winId);
        UTILS_assert(drvInst < pObj->createArgs.numSwMsInst );

        repeatFld = FALSE;

        pInFrame = NULL;

        if(pWinInfo->channelNum != SYSTEM_SW_MS_INVALID_ID)
        {
            UTILS_assert(pWinInfo->channelNum < UTILS_ARRAYSIZE(pObj->chObj));
            pInFrame = pObj->chObj[pWinInfo->channelNum].pCurInFrame;
        }
        if (NULL == pInFrame)
        {
            pInFrame = &pWinObj->blankFrame;
            pWinObj->isBlankFrameDisplay = TRUE;

            if(!SwMsLink_drvIsBlankFrameRtParamsApplied(pWinObj, pObj))
            {
                SwMsLink_drvApplyBlankFrameRtParams(drvInst, pWinObj, pObj);
                pWinObj->applyRtPrm = TRUE;
            }
        }
        else
        {
            UInt32 latency;

            pWinObj->framesUsedCount++;

            pWinObj->isBlankFrameDisplay = FALSE;
            pObj->avgStats.framesUsedCount[winId]++;

            latency = ((UInt32)(Avsync_getWallTime()) - pInFrame->timeStamp);

            if(latency>pWinObj->maxLatency)
                pWinObj->maxLatency = latency;
            if(latency<pWinObj->minLatency)
                pWinObj->minLatency = latency;

            if(pWinObj->lastInFrame == &pWinObj->blankFrame)
            {
                /* last Frame display was blank frame, set RT params of current frame */
                pWinObj->deiRtPrm.deiInFrmPrms = &pWinObj->scRtInFrmPrm;
                pWinObj->deiRtPrm.deiScCropCfg = &pWinObj->scRtCropCfg;

                pWinObj->scRtPrm.inFrmPrms  = &pWinObj->scRtInFrmPrm;
                pWinObj->scRtPrm.srcCropCfg = &pWinObj->scRtCropCfg;

                pWinObj->applyRtPrm = TRUE;
            }

        }

        pDrvObj = &pObj->DrvObj[drvInst];
        if (pObj->DrvObj[drvInst].isDeiDrv)
        {
            if (pWinInfo->bypass || pObj->DrvObj[drvInst].forceBypassDei == TRUE)
            {

                pDrvObj = &pObj->DrvObj[drvInst + SYSTEM_SW_MS_MAX_INST];
                /* [TODO] This is for avoiding bypass DEI driver getting FID
                             * = 1. Not clear why FID = 1 is passed to bypass DEI without
                             * this workaround. */
                if (FVID2_FID_TOP != pInFrame->fid)
                {
                    pInFrame->fid = FVID2_FID_TOP;
                }
            }
        }

        if (pObj->switchLayout)
        {
            pObj->lastOutBufPtr = NULL;
        }

        rtParamUpdatePerFrame = FALSE;
        pFrameInfo = (System_FrameInfo *) pInFrame->appData;
        if (pInFrame == &pWinObj->blankFrame)
        {
            UTILS_assert(pFrameInfo == NULL);
        }

#ifdef SWMS_ENABLE_MODIFY_FRAME_POINTER_ALWAYS
        UTILS_assert (pInFrame->channelNum < SYSTEM_SW_MS_MAX_CH_ID);
        hmpCrop = &pObj->chObj[pInFrame->channelNum].hmpCrop;
        if (hmpCrop->enableHMPCropFlag == TRUE)
        {
            UTILS_assert (hmpCrop->chId == pInFrame->channelNum);
            if (pObj->lastOutBufPtr == NULL)
            {
                pFrameInfo->rtChInfoUpdate = TRUE;
            }
        }
#endif

        if (pFrameInfo != NULL)
        {
            if (pFrameInfo->rtChInfoUpdate == TRUE)
            {
                UTILS_assert(pInFrame->channelNum<SYSTEM_SW_MS_MAX_CH_ID);

                rtChannelInfo = &pObj->rtChannelInfo[pInFrame->channelNum];

#ifdef SWMS_ENABLE_MODIFY_FRAME_POINTER_ALWAYS
                if (hmpCrop->enableHMPCropFlag == TRUE)
                {
                    pFrameInfo->rtChInfo.width = hmpCrop->cropWidth;
                    pFrameInfo->rtChInfo.height = hmpCrop->cropHeight;
                    pFrameInfo->rtChInfo.startX += hmpCrop->cropStartX;
                    pFrameInfo->rtChInfo.startY += hmpCrop->cropStartY;
                }
#endif
                if(pFrameInfo->rtChInfo.width > SW_MS_MAX_WIDTH_SUPPORTED)
                {
                    pFrameInfo->rtChInfo.width = SW_MS_MAX_WIDTH_SUPPORTED;
                }

                if(pFrameInfo->rtChInfo.height > SW_MS_MAX_HEIGHT_SUPPORTED)
                {
                    pFrameInfo->rtChInfo.height = SW_MS_MAX_HEIGHT_SUPPORTED;
                }

                if (pFrameInfo->rtChInfo.startX != rtChannelInfo->startX)
                {
                    rtChannelInfo->startX = pFrameInfo->rtChInfo.startX;
                    rtParamUpdatePerFrame = TRUE;
                }
                if (pFrameInfo->rtChInfo.startY != rtChannelInfo->startY)
                {
                    rtChannelInfo->startY = pFrameInfo->rtChInfo.startY;
                    rtParamUpdatePerFrame = TRUE;
                }
                if (pFrameInfo->rtChInfo.height != rtChannelInfo->height)
                {
                    rtChannelInfo->height = pFrameInfo->rtChInfo.height;
                    rtParamUpdatePerFrame = TRUE;
                }
                if (pFrameInfo->rtChInfo.width != rtChannelInfo->width)
                {
                    rtChannelInfo->width = pFrameInfo->rtChInfo.width;
                    rtParamUpdatePerFrame = TRUE;
                }

#ifdef SWMS_ENABLE_MODIFY_FRAME_POINTER_ALWAYS
                if (hmpCrop->enableHMPCropFlag == TRUE)
                {
                    pFrameInfo->rtChInfo.startX -= hmpCrop->cropStartX;
                    pFrameInfo->rtChInfo.startY -= hmpCrop->cropStartY;
                }
#endif
               
                /* Right now, only MPSCLR link updates the dataformat, runtime
                               Ensure this */
                if (pFrameInfo->isDataTypeChange)
                {
                    /* For switching between progressive and interlace frames */
                    if (pFrameInfo->rtChInfo.scanFormat !=
                        rtChannelInfo->scanFormat)
                    {
                        rtChannelInfo->scanFormat =
                            pFrameInfo->rtChInfo.scanFormat;
                        rtParamUpdatePerFrame = TRUE;
                        Vps_printf(" SWMS: ch:%d ScanFormat changed to %s....***\n",
                            pInFrame->channelNum, 
                            gSystem_nameScanFormat[pFrameInfo->rtChInfo.scanFormat]);
                    }

                    if (pFrameInfo->rtChInfo.dataFormat !=
                        rtChannelInfo->dataFormat)
                    {
                        rtChannelInfo->dataFormat =
                            pFrameInfo->rtChInfo.dataFormat;
                        rtParamUpdatePerFrame = TRUE;
                    }
                    if (pFrameInfo->rtChInfo.memType !=
                        rtChannelInfo->memType)
                    {
                        rtChannelInfo->memType =
                            pFrameInfo->rtChInfo.memType;
                        rtParamUpdatePerFrame = TRUE;
                    }
                    pFrameInfo->isDataTypeChange = FALSE;
                }
                if (pFrameInfo->rtChInfo.pitch[0] != rtChannelInfo->pitch[0])
                {
                    rtChannelInfo->pitch[0] = pFrameInfo->rtChInfo.pitch[0];
                    rtParamUpdatePerFrame = TRUE;
                }
                if (pFrameInfo->rtChInfo.pitch[1] != rtChannelInfo->pitch[1])
                {
                    rtChannelInfo->pitch[1] = pFrameInfo->rtChInfo.pitch[1];
                    rtParamUpdatePerFrame = TRUE;
                }
                pFrameInfo->rtChInfoUpdate = FALSE;
                if (pObj->DrvObj[drvInst].isDeiDrv)
                {
                    if (pFrameInfo->rtChInfo.scanFormat == FVID2_SF_PROGRESSIVE )
                    {
                        pDrvObj = &pObj->DrvObj[drvInst + SYSTEM_SW_MS_MAX_INST];
                    }
                }
            }
        }
        else
        {
            UTILS_assert(pInFrame == &pWinObj->blankFrame);
            /* Get the associated input frames channel id associated with this
                        window */
            UTILS_assert (winId < SYSTEM_SW_MS_MAX_WIN);
            UTILS_assert(pObj->DrvObj[drvInst].win2ScLChMap[winId]
                         != SWMS_LINK_MAP_INVALID);
            /* Convert the input channel number to
                        SC instance logical channel number */
            pInFrame->channelNum = pObj->DrvObj[drvInst].win2ScLChMap[winId];
        }

        pInFrame->perFrameCfg = NULL;
        pWinObj->curOutFrame.perFrameCfg = NULL;

        pWinObj->curOutFrame.addr[0][0] =
            (Ptr) ((UInt32) pOutFrame->addr[0][0] + pWinInfo->bufAddrOffset[0]);
        pWinObj->curOutFrame.addr[0][1] =
            (Ptr) ((UInt32) pOutFrame->addr[0][1] + pWinInfo->bufAddrOffset[1]);

        copyInBuf2OutBuf = FALSE;
        if (pWinObj->applyRtPrm || repeatFld || rtParamUpdatePerFrame || pObj->scChMapRtParamUpdate)
        {
            if (pObj->DrvObj[drvInst].isDeiDrv)
            {
                pInFrame->perFrameCfg = &pWinObj->deiRtPrm;
                pWinObj->curOutFrame.perFrameCfg = &pWinObj->deiRtPrm;
            }
            else
            {
                pInFrame->perFrameCfg = &pWinObj->scRtPrm;
                pWinObj->curOutFrame.perFrameCfg = &pWinObj->scRtPrm;
            }
            pWinObj->applyRtPrm = FALSE;

            if (repeatFld && pObj->DrvObj[drvInst].isDeiDrv && pObj->DrvObj[drvInst].forceBypassDei == FALSE)
            {
                pWinObj->deiRtCfg.fldRepeat = TRUE;
            }
            if ((rtParamUpdatePerFrame == TRUE) ||
                (pObj->scChMapRtParamUpdate == TRUE))
            {
                pObj->rtParamUpdate = TRUE;
            }
            copyInBuf2OutBuf = FALSE;
        }
        else
        {
            if (pObj->enableBufCopy)
            {
                copyInBuf2OutBuf = SwMsLink_cpySrc2DestPossible(pWinObj);
            }
            else
            {
                copyInBuf2OutBuf = FALSE;
            }
        }
        if( pInFrame == &pWinObj->blankFrame)
        {
            isInputReceived = FALSE;
        }
        else
        {
            isInputReceived = pObj->chObj[pWinInfo->channelNum].isInputReceived;
            if (pObj->chObj[pWinInfo->channelNum].displayStarted == FALSE)
            {
                pObj->chObj[pWinInfo->channelNum].displayStarted = TRUE;
                pObj->chObj[pWinInfo->channelNum].firstPTS = Avsync_getWallTime();
            }
            if (pObj->chObj[pWinInfo->channelNum].displayStarted == TRUE)
            {
                pObj->chObj[pWinInfo->channelNum].currPTS = Avsync_getWallTime();
            }
        }

        copyOutBuf2OutBuf = FALSE;
        if ((TRUE == pObj->enableBufCopy)
            &&
            (pObj->lastOutBufPtr != NULL)
            &&
            (isInputReceived == FALSE))
        {
            copyOutBuf2OutBuf = TRUE;
        }

        if(!copyOutBuf2OutBuf && !copyInBuf2OutBuf)
        {
            pDrvObj->inFrameList.frames[pDrvObj->inFrameList.numFrames]
                = pInFrame;
            pDrvObj->inFrameList.numFrames++;
            pDrvObj->outFrameList.frames[pDrvObj->outFrameList.numFrames]
                = &pWinObj->curOutFrame;
            pDrvObj->outFrameList.numFrames++;
        }
        else
        {
            if (copyOutBuf2OutBuf)
            {
                SwMsLink_dmaWinCopyObjUpdateOutBuf2OutBuf(pObj, pOutFrame, pWinInfo);
                pWinObj->framesOutBufCopyCount++;
            }
            else
            {
                UTILS_assert(copyInBuf2OutBuf == TRUE);
                SwMsLink_dmaWinCopyObjUpdateInBuf2OutBuf(pObj, pOutFrame,pInFrame,pWinInfo);
                pWinObj->framesInBufCopyCount++;
            }
        }
        /* Remmember the frame that would be processed */
        pWinObj->lastInFrame = pInFrame;
    }

    if (pObj->rtParamUpdate == TRUE)
    {
        Vps_printf(" SWMS: *** UPDATING RT Params ***\n");
        pObj->layoutParams.onlyCh2WinMapChanged = TRUE;
        SwMsLink_drvSwitchLayout(pObj, &pObj->layoutParams, TRUE,FALSE);
        SwMsLink_drvRestoreBlankFrameRtParams(pObj);
    }

    if (pObj->scChMapRtParamUpdate == TRUE)
    {
        pObj->scChMapRtParamUpdate = FALSE;
    }
    pObj->rtParamUpdate = FALSE;
    pObj->switchLayout = FALSE;

    if (pObj->freeFrameList.numFrames)
    {
        pObj->inFramePutCount += pObj->freeFrameList.numFrames;
        System_putLinksEmptyFrames(pInQueParams->prevLinkId,
                                   pInQueParams->prevLinkQueId,
                                   &pObj->freeFrameList);
    }

    return FVID2_SOK;
}

Int32 SwMsLink_DrvProcessFrames(SwMsLink_Obj * pObj)
{
    Int32 status = FVID2_SOK;
    UInt32 curTime;
    SwMsLink_DrvObj *pDrvObj[2*SYSTEM_SW_MS_MAX_INST];
    UInt32 i,drvId;

    for (i = 0; i < SYSTEM_SW_MS_MAX_INST; i++)
    {
        pDrvObj[i] = NULL;
        pDrvObj[i + SYSTEM_SW_MS_MAX_INST] = NULL;
    }

    drvId = 0;
    for (i = 0; i < pObj->createArgs.numSwMsInst; i++)
    {
        if (pObj->DrvObj[i].isDeiDrv)
        {
            if((pObj->DrvObj[i].forceBypassDei == FALSE) &&(pObj->DrvObj[i].inFrameList.numFrames))
            {
                pDrvObj[drvId++] = &pObj->DrvObj[i];
            }

            if (pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST].inFrameList.numFrames)
            {
                pDrvObj[drvId++] = &pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST];
            }
        }
        else
        {
            if(pObj->DrvObj[i].inFrameList.numFrames)
                pDrvObj[drvId++] = &pObj->DrvObj[i];
        }
    }

    //now we'll begin process framelist in parallel
    if (drvId)
    {
        curTime = Utils_getCurTimeInMsec();
    }
    else
    {
        if (pObj->winCopyObj.numWindows)
        {
            SwMsLink_dmaCopyWin(pObj);
        }
        return status;
    }

    /* VIP locking should be done in dual out mode as
       *  VIP access happens at capture during reset & DEI processing
       */
    if (pObj->vipLockRequired == TRUE)
    {
        System_lockVip(pObj->vipInstId);
    }

    for (i = 0; i < drvId; i++)
    {
        /* If in dual out mode; initialize VIP-SC numOutFrames */
        if (pDrvObj[i]->processList.numOutLists == 2)
        {
            pObj->outFrameDropList.numFrames = pDrvObj[i]->outFrameList.numFrames;
        }

        SwMsLink_drvModifyFramePointer(pObj, pDrvObj[i], 1);
        status = FVID2_processFrames(pDrvObj[i]->fvidHandle, &pDrvObj[i]->processList);
        UTILS_assert(status == FVID2_SOK);
        waitingOnDriverCbSWMS[i] = TRUE;
    }

    if (pObj->winCopyObj.numWindows)
    {
        SwMsLink_dmaCopyWin(pObj);
    }

    for (i = 0; i < drvId; i++)
    {
        Semaphore_pend(pDrvObj[i]->complete, BIOS_WAIT_FOREVER);
        waitingOnDriverCbSWMS[i] = FALSE;
        status = FVID2_getProcessedFrames(pDrvObj[i]->fvidHandle,
                                          &pDrvObj[i]->processList, BIOS_NO_WAIT);
        UTILS_assert(status == FVID2_SOK);

        SwMsLink_drvModifyFramePointer(pObj, pDrvObj[i], 0);
        pObj->frameCount += pDrvObj[i]->inFrameList.numFrames;
    }
    if (pObj->vipLockRequired == TRUE)
    {
        System_unlockVip(pObj->vipInstId);
    }

    curTime = Utils_getCurTimeInMsec() - curTime;
    pObj->totalTime += curTime;


    return status;
}

Int32 SwMsLink_drvDoScaling(SwMsLink_Obj * pObj)
{
    FVID2_Frame *pOutFrame;
    FVID2_Frame *pDupedOutFrame;
    Int32 status;
    UInt32 curTime = Utils_getCurTimeInMsec();
    Bool enableOuputDupLocalFlag = FALSE;
    UInt key;

    if (pObj->skipProcessing)
    {
        pObj->skipProcessing--;
        return FVID2_SOK;
    }

    if (pObj->prevDoScalingTime != 0)
    {
        if (curTime > pObj->prevDoScalingTime)
        {
            UInt32 curScalingInterval;

            curScalingInterval    = curTime - pObj->prevDoScalingTime;
            pObj->scalingInterval += curScalingInterval;
            if (curScalingInterval < pObj->scalingIntervalMin)
            {
                pObj->scalingIntervalMin = curScalingInterval;
            }
            if (curScalingInterval > pObj->scalingIntervalMax)
            {
                pObj->scalingIntervalMax = curScalingInterval;
            }
        }
    }
    pObj->prevDoScalingTime = curTime;
    pObj->framesOutReqCount++;

    status = Utils_bufGetEmptyFrame(&pObj->bufOutQue, &pOutFrame, BIOS_NO_WAIT);
    if (status != FVID2_SOK)
    {
        pObj->framesOutDropCount++;
        return status;
    }
    UTILS_assert(pOutFrame != NULL);
    SwMsLink_drv_init_outframe(pObj, pOutFrame);

    pOutFrame->timeStamp = (UInt32)Avsync_getWallTime();
    UTILS_assert(pOutFrame->appData != NULL);
    ((System_FrameInfo *)pOutFrame->appData)->ts64 = Avsync_getWallTime();

    SwMsLink_drvLock(pObj);

    SwMsLink_drvMakeFrameLists(pObj, pOutFrame);

    SwMsLink_DrvProcessFrames(pObj);

    SwMsLink_drvUnlock(pObj);

    if(pObj->createArgs.enableLayoutGridDraw == TRUE)
    {
        SwMsLink_drvDoDma(pObj,pOutFrame);
    }

    key = Hwi_disable();
    enableOuputDupLocalFlag = pObj->enableOuputDupLocalFlag;
    Hwi_restore(key);

    if (enableOuputDupLocalFlag && pObj->createArgs.enableOuputDup)
    {
        status = SwMsLink_dupFrame(pObj, pOutFrame, &pDupedOutFrame);
        status = Utils_bufPutFullFrame(&pObj->bufOutQue, pDupedOutFrame);
        UTILS_assert (status == FVID2_SOK);
    }

    pObj->lastOutBufPtr = pOutFrame;
    status = Utils_bufPutFullFrame(&pObj->bufOutQue, pOutFrame);
    UTILS_assert (status  == FVID2_SOK);
    pObj->framesOutCount++;
    pObj->avgStats.framesOutCount++;

    SwMsLink_drvPrintAvgStatistics(pObj);

    System_sendLinkCmd(pObj->createArgs.outQueParams.nextLink,
                       SYSTEM_CMD_NEW_DATA);

    return status;
}

Int32 SwMsLink_drvDeleteDrv(SwMsLink_Obj * pObj, SwMsLink_DrvObj *pDrvObj)
{
    Int32 status;

    if (pDrvObj->isDeiDrv && pDrvObj->bypassDei == FALSE)
    {
        SwMsLink_drvFreeCtxMem(pDrvObj);
    }

    status = FVID2_delete(pDrvObj->fvidHandle, NULL);
    UTILS_assert(FVID2_SOK == status);

    Semaphore_delete(&pDrvObj->complete);

    return FVID2_SOK;
}

Int32 SwMsLink_drvDelete(SwMsLink_Obj * pObj)
{
    UInt32 i;
    System_MemoryType memType;
    Int32 status;

#ifdef SYSTEM_DEBUG_SWMS
    Vps_printf(" %d: SWMS: Frames = %d (fps = %d) !!!\n",
               Utils_getCurTimeInMsec(),
               pObj->frameCount,
               pObj->frameCount * 100 / (pObj->totalTime / 10));
#endif

#ifdef SYSTEM_DEBUG_SWMS
    Vps_printf(" %d: SWMS: Delete in progress !!!\n", Utils_getCurTimeInMsec());
#endif

    memType = (System_MemoryType)pObj->inQueInfo.chInfo[0].memType;

    /* Unregister display callback tied DO_SCALING invocation on SwMs delete */
    if (pObj->createArgs.enableProcessTieWithDisplay)
    {
        AvsyncLink_VidSyncCbHookObj  cbHookObj;

        cbHookObj.cbFxn = NULL;
        cbHookObj.ctx   = NULL;
        Avsync_vidSynchRegisterCbHook(Avsync_vidQueGetDisplayID(pObj->linkId),
                                      &cbHookObj);
    }

    for (i = 0; i < pObj->createArgs.numSwMsInst; i++)
    {
        if(pObj->DrvObj[i].isDeiDrv)
        {
            if(pObj->DrvObj[i].forceBypassDei==FALSE)
            {   /* DEI driver in DEI mode is deleted only force DEI bypass is FALSE */
                SwMsLink_drvDeleteDrv(pObj, &pObj->DrvObj[i]);
            }
            SwMsLink_drvDeleteDrv(pObj, &pObj->DrvObj[i + SYSTEM_SW_MS_MAX_INST]);
        }
        else
        {
            SwMsLink_drvDeleteDrv(pObj, &pObj->DrvObj[i]);
        }
    }

    status = SwMsLink_drvDmaDelete(pObj);
    UTILS_assert(0 == status);

    Utils_bufDelete(&pObj->bufOutQue);

    Semaphore_delete(&pObj->lock);
    Clock_delete(&pObj->timer);
    SwMsLink_drvDeleteDupObj(pObj);
    SwMsLink_drvDeleteChannelObj(pObj);

    for(i=0; i<pObj->createArgs.numOutBuf; i++)
    {
        Utils_memFrameFree(&pObj->bufferFrameFormat,
                           &pObj->outFrames[i], 1);
    }

    if(memType==SYSTEM_MT_NONTILEDMEM)
    {
        /* blank frame need not be freed, its freed at system de-init */
        }
    else
    {
        // free tiler buffer
        SystemTiler_freeAll();
    }

#ifdef SYSTEM_DEBUG_SWMS
    Vps_printf(" %d: SWMS: Delete Done !!!\n", Utils_getCurTimeInMsec());
#endif

    return FVID2_SOK;
}

Int32 SwMsLink_drvStart(SwMsLink_Obj * pObj)
{
#ifdef SYSTEM_DEBUG_SWMS
    Vps_printf(" %d: SWMS: Start in Progress !!!\n", Utils_getCurTimeInMsec());
#endif

    SwMsLink_drvResetStatistics(pObj);
    Clock_start(pObj->timer);

#ifdef SYSTEM_DEBUG_SWMS
    Vps_printf(" %d: SWMS: Start Done !!!\n", Utils_getCurTimeInMsec());
#endif

    return FVID2_SOK;
}

Int32 SwMsLink_drvStop(SwMsLink_Obj * pObj)
{
#ifdef SYSTEM_DEBUG_SWMS
    Vps_printf(" %d: SWMS: Stop in Progress !!!\n", Utils_getCurTimeInMsec());
#endif

    Clock_stop(pObj->timer);

#ifdef SYSTEM_DEBUG_SWMS
    Vps_printf(" %d: SWMS: Stop Done !!!\n", Utils_getCurTimeInMsec());
#endif

    return FVID2_SOK;
}

Int32 SwMsLink_drvLock(SwMsLink_Obj * pObj)
{
    return Semaphore_pend(pObj->lock, BIOS_WAIT_FOREVER);
}

Int32 SwMsLink_drvUnlock(SwMsLink_Obj * pObj)
{
    Semaphore_post(pObj->lock);

    return FVID2_SOK;
}

Int32 SwMsLink_drvClockPeriodReconfigure(SwMsLink_Obj * pObj)
{
    UInt32 timerPeriod;

    Vps_rprintf(" %d: SWMS    : ******* Configuring clock %d secs... \n",
                Utils_getCurTimeInMsec(), pObj->timerPeriod);

    timerPeriod = pObj->timerPeriod;

    Clock_stop(pObj->timer);
    Clock_setPeriod(pObj->timer, timerPeriod);
    Clock_setTimeout(pObj->timer, timerPeriod);
    Clock_start(pObj->timer);

    return FVID2_SOK;
}

Int32 SwMsLink_drvGetTimerPeriod(SwMsLink_Obj * pObj,
                                 SwMsLink_LayoutPrm * layoutParams)
{
    if (layoutParams->outputFPS == 0 || layoutParams->outputFPS > 200)
    {
        pObj->timerPeriod = SW_MS_LINK_TIMER_DEFAULT_PERIOD;
    }
    else
    {
        pObj->timerPeriod =
              (1000/(layoutParams->outputFPS+(layoutParams->outputFPS/10)));
              //(1000/(layoutParams->outputFPS));
    }
    return FVID2_SOK;
}

Int32 SwMsLink_printBufferStatus (SwMsLink_Obj * pObj)
{
    Uint8 str[256];

    Vps_rprintf
        (" \n"
          " *** [%s] Mosaic Statistics *** \n"
          "%d: SWMS: Rcvd from prev = %d, Returned to prev = %d\r\n",
          pObj->name,
          Utils_getCurTimeInMsec(), pObj->inFrameGetCount, pObj->inFramePutCount);

    sprintf ((char *)str, "SWMS Out ");
    Utils_bufPrintStatus(str, &pObj->bufOutQue);
    return 0;
}

static
Void  SwMsLink_channelFlushBuffers(SwMsLink_Obj * pObj,
                                   UInt32 chNum,
                                   UInt32 holdLastFrame,
                                   FVID2_FrameList *freeFrameList)
{
    System_LinkInQueParams *pInQueParams;
    FVID2_Frame *frame = NULL;

    pInQueParams = &pObj->createArgs.inQueParams;

    Avsync_vidQueFlush(&pObj->chObj[chNum].inQue,
                       &frame,
                       freeFrameList);
    if (FALSE == holdLastFrame)
    {
        if (frame)
        {
            SwMsLink_freeFrame(pInQueParams,
                               freeFrameList,
                               frame);
        }
        if (pObj->chObj[chNum].pCurInFrame)
        {
            SwMsLink_freeFrame(pInQueParams,
                               freeFrameList,
                               pObj->chObj[chNum].pCurInFrame);
            pObj->chObj[chNum].pCurInFrame = NULL;
        }
    }
    else
    {
        if (frame)
        {
            if (pObj->chObj[chNum].pCurInFrame)
            {
                SwMsLink_freeFrame(pInQueParams,
                                   freeFrameList,
                                   pObj->chObj[chNum].pCurInFrame);
                pObj->chObj[chNum].pCurInFrame = NULL;
            }
            SwMsLink_freeFrame(pInQueParams,
                               freeFrameList,
                               frame);
        }
    }
    SwMsLink_freeFrameList(pObj,freeFrameList);
}

Int32 SwMsLink_flushBuffers(SwMsLink_Obj * pObj,SwMsLink_FlushParams *prm)
{
    Int32 status = FVID2_SOK;
    Int i;

    SwMsLink_drvLock(pObj);
    pObj->freeFrameList.numFrames = 0;
    if (prm->chNum == SYSTEM_SW_MS_ALL_CH_ID)
    {
        for (i = 0; i < pObj->inQueInfo.numCh; i++)
        {
            SwMsLink_channelFlushBuffers(pObj,i,
                                         prm->holdLastFrame,
                                         &pObj->freeFrameList);
        }
    }
    else
    {
        if (prm->chNum < pObj->inQueInfo.numCh)
        {
            SwMsLink_channelFlushBuffers(pObj,prm->chNum,
                                         prm->holdLastFrame,
                                         &pObj->freeFrameList);
        }
        else
        {
            Vps_printf("SWMS:!WARNING.Flush invoked for invalid channel number:%d",
                       prm->chNum);
            status = FVID2_EFAIL;
        }
    }

    if (pObj->freeFrameList.numFrames)
    {
        System_LinkInQueParams *pInQueParams;

        pInQueParams = &pObj->createArgs.inQueParams;
        pObj->inFramePutCount += pObj->freeFrameList.numFrames;
        System_putLinksEmptyFrames(pInQueParams->prevLinkId,
                                   pInQueParams->prevLinkQueId,
                                   &pObj->freeFrameList);
    }
    pObj->lastOutBufPtr = NULL;
    SwMsLink_drvUnlock(pObj);
    return status;
}

Int32 SwMsLink_SetFrameRate(SwMsLink_Obj * pObj, SwMsLink_ChFpsParams * params)
{
    Int32 status = FVID2_SOK;
    SwMsLink_chObj *pChObj;

    if ((params->chId <  pObj->inQueInfo.numCh)
        &&
        (params->inputFrameRate > params->outputFrameRate))
    {
        pChObj = &pObj->chObj[params->chId];

        pChObj->doFrameSkip = TRUE;
        pChObj->frameSkipCtx.firstTime = TRUE;
        pChObj->frameSkipCtx.inputFrameRate = params->inputFrameRate;
        pChObj->frameSkipCtx.outputFrameRate = params->outputFrameRate;
        Vps_printf("SWMS:Setting frame rate chId[%d],in:%d::out:%d",
                   params->chId,
                   pChObj->frameSkipCtx.inputFrameRate,
                   pChObj->frameSkipCtx.outputFrameRate);
    }
    else
    {
        status = FVID2_EFAIL;
    }
    return (status);
}

static Bool SwMsLink_drvIsOverlapWindow(SwMsLink_winPos *win1,SwMsLink_winPos *win2)
{
    Bool nonOverlappingWindow;
    /* For two windows to be non overlapping, it must satisfy one of the following condition:
     *  1.win1 should be above win2 (win1->yBotRight <= win2->yTopLeft)
     *  2.win1 should be below win2 (win1->yTopLeft  >= win2->yBotRight)
     *  3.win1 should be left of win2  (win1->xBotRight <= win2->xTopLeft)
     *  4.win1 should be right of win2 (win1->xTopLeft >= win2->xBotRight)
     */

    nonOverlappingWindow = ((win1->yBotRight <= win2->yTopLeft)  ||
                            (win1->yTopLeft  >= win2->yBotRight) ||
                            (win1->xBotRight <= win2->xTopLeft)  ||
                            (win1->xTopLeft >= win2->xBotRight));
    return !nonOverlappingWindow;
}

static Void SwMsLink_drvMapLayout2WinPos(SwMsLink_winPos *winPos,SwMsLink_LayoutWinInfo *winInfo)
{
    winPos->xTopLeft = winInfo->startX;
    winPos->yTopLeft = winInfo->startY;

    winPos->xBotRight = winInfo->startX + winInfo->width;
    winPos->yBotRight = winInfo->startY + winInfo->height;
}

static Bool SwMsLink_drvIsOverlapLayout(SwMsLink_Obj *pObj)
{
    UInt32 win1Id;
    UInt32 win2Id;
    Bool isOverlapLayout = FALSE;


    for (win1Id = 0; win1Id < (pObj->layoutParams.numWin - 1); win1Id++)
    {
        for (win2Id = (win1Id  + 1); win2Id < pObj->layoutParams.numWin; win2Id++)
        {
            SwMsLink_winPos winPos1;
            SwMsLink_winPos winPos2;

            SwMsLink_drvMapLayout2WinPos (&winPos1,&pObj->layoutParams.winInfo[win1Id]);
            SwMsLink_drvMapLayout2WinPos (&winPos2,&pObj->layoutParams.winInfo[win2Id]);
            if (SwMsLink_drvIsOverlapWindow(&winPos1,&winPos2))
            {
                Vps_printf("%d:SWMS:SwMs window overlap."
                           "WinId1[%d]:startX[%d]:startY[%d]:endX[%d]:endY[%d]::"
                           "WinId2[%d]:startX[%d]:startY[%d]:endX[%d]:endY[%d]",
                           Utils_getCurTimeInMsec(),
                           win1Id,
                           winPos1.xTopLeft,winPos1.yTopLeft,
                           winPos1.xBotRight,winPos1.yBotRight,
                           win2Id,
                           winPos2.xTopLeft,winPos2.yTopLeft,
                           winPos2.xBotRight,winPos2.yBotRight);
                isOverlapLayout = TRUE;
                break;
            }
        }
        if (isOverlapLayout)
            break;
    }
    return isOverlapLayout;
}


static Int32 SwMsLink_creatChanToInstMap(SwMsLink_Obj * pObj)
{
    UInt32 i, j, k, startWin, endWin;
    UInt32 unAssignWinIndex;
    UInt32 chUsedPerSc[SYSTEM_SW_MS_MAX_INST];
    UInt32 unAssignedWin[SYSTEM_SW_MS_MAX_CH_ID];

    pObj->maxWinId = 0;
    unAssignWinIndex = 0;

    for (i = 0; i < pObj->createArgs.numSwMsInst; i++)
    {
        chUsedPerSc[i] = 0;

        if (pObj->createArgs.numSwMsInst != 1)
            startWin = pObj->createArgs.swMsInstStartWin[i];
        else
            startWin = 0;

        if( i != (pObj->createArgs.numSwMsInst - 1))
            endWin = pObj->createArgs.swMsInstStartWin[i+1] - 1;
        else
        {
            if (SYSTEM_SW_MS_MAX_WIN > startWin)
                endWin = SYSTEM_SW_MS_MAX_WIN - 1;
            else
                endWin = startWin + 1;
        }

        if ((startWin + SYSTEM_SW_MS_MAX_WIN_PER_SC - 1) < endWin)
        {
            /* The End window that should have been processed by this SC inst */
            k = endWin;

            /* End window that could be scaled by this instance of SC */
            endWin = startWin + SYSTEM_SW_MS_MAX_WIN_PER_SC - 1;

            /* Please note, at this point endWin would be updated to windows
                that could be processed by this instance */
            for (j = endWin + 1; j <= k; j++)
            {
                unAssignedWin[unAssignWinIndex] = j;
                unAssignWinIndex++;
            }
        }

        pObj->maxWinId = endWin;

        for (j = startWin; j <= endWin; j++)
        {
            /* Update the instances instance map */
            pObj->chNum2InstIdMap[j] = i;
            chUsedPerSc[i]++;
        }

        Vps_printf("SWMS: instance %d, sc id %d, start win %d end win %d\n", i,
            pObj->createArgs.swMsInstId[i], startWin, endWin);
    }

    if (unAssignWinIndex != 0)
    {
        if (pObj->createArgs.numSwMsInst == 1)
        {
            /* Since all channels requires to be processed by single instance
                of SC, not other checks necessary */
            while (unAssignWinIndex > 0)
            {
                unAssignWinIndex--;
                UTILS_assert (SWMS_LINK_MAP_INVALID ==
                    pObj->chNum2InstIdMap[unAssignedWin[unAssignWinIndex]]);
                pObj->chNum2InstIdMap[unAssignedWin[unAssignWinIndex]] = 0;
            }
        }
        else
        {
            i = pObj->createArgs.numSwMsInst;
            while(i > 0)
            {
                i--;
                while (chUsedPerSc[i] < SYSTEM_SW_MS_MAX_WIN_PER_SC)
                {

                    if (unAssignWinIndex == 0)
                        break;

                    unAssignWinIndex--;
                    /* This instance can process un-assigned channel(s) */
                    chUsedPerSc[i]++;
                    UTILS_assert (SWMS_LINK_MAP_INVALID ==
                        pObj->chNum2InstIdMap[unAssignedWin[unAssignWinIndex]]);

                    pObj->chNum2InstIdMap[unAssignedWin[unAssignWinIndex]] = i;
                }
            }
        }

        if (unAssignWinIndex != 0)
        {
            Vps_printf(" %d: SWMS: ERROR - Could not allocate all windows to "
                    "scalar\n", Utils_getCurTimeInMsec());
            UTILS_assert (FALSE);
        }
    }

    return FVID2_SOK;
}


Int32 SwMsLink_drvGetChannelMapParams(SwMsLink_Obj * pObj,
                                        SwMsLink_ChannelMapParams * chMapParams)
{
    UTILS_assert (pObj != NULL);
    UTILS_assert (chMapParams != NULL);

    chMapParams->numScalarInst = pObj->createArgs.numSwMsInst;
    memcpy(chMapParams->chNumToScInstMap,
            &pObj->chNum2InstIdMap, sizeof(chMapParams->chNumToScInstMap));

    return FVID2_SOK;

}

Int32 SwMsLink_drvSetChannelMapParams(SwMsLink_Obj * pObj,
                                        SwMsLink_ChannelMapParams * chMapParams)
{
    Int32 rtnValue = FVID2_EFAIL;
    UInt32 localCh2InstMap[SYSTEM_SW_MS_MAX_CH_ID];
    UInt32 chNum, scInst, unAssignedChCnt, startChNo;
    UInt32 unAssignedCh[SYSTEM_SW_MS_MAX_CH_ID];
    UInt32 chUsedPerSc[SYSTEM_SW_MS_MAX_INST];

    UTILS_assert (pObj != NULL);
    UTILS_assert (chMapParams != NULL);

    if (pObj->createArgs.numSwMsInst != chMapParams->numScalarInst)
    {
        Vps_printf(" %d: SWMS: Channel remap ERROR!!!\n",
            Utils_getCurTimeInMsec());
        Vps_printf(" %d: SWMS: ERROR : Number of SWMS instance mismatch. "
                    "Created with %d - At remap %d specified!!!\n",
                    Utils_getCurTimeInMsec(),
                    pObj->createArgs.numSwMsInst,
                    chMapParams->numScalarInst);
        return FVID2_EFAIL;
    }

    if (chMapParams->numScalarInst < 2)
    {
        Vps_printf(" %d: SWMS: WARNING : Channel remap supported when multiple"
                    "instances of scalar are used. No action taken!!!\n",
                    Utils_getCurTimeInMsec());
        return FVID2_SOK;
    }

    /* All mappings in-valid */
    memset(&localCh2InstMap, 0xFF, sizeof(localCh2InstMap));
    memset (&chUsedPerSc, 0x0, sizeof(chUsedPerSc));
    unAssignedChCnt = 0;

    for (startChNo = 0; startChNo < SYSTEM_SW_MS_MAX_CH_ID; startChNo++)
    {
        if (chMapParams->chNumToScInstMap[startChNo] != SWMS_LINK_MAP_INVALID)
            break;
    }

    /* No valid start channel found */
    if (startChNo == SYSTEM_SW_MS_MAX_CH_ID)
    {
        Vps_printf(" %d: SWMS: ERROR : No valid channel mapping found !!! \n",
                    Utils_getCurTimeInMsec());
        return FVID2_EFAIL;
    }

    for (chNum = startChNo; ((chNum < (startChNo + SYSTEM_SW_MS_MAX_WIN)) &&
                             (chNum < SYSTEM_SW_MS_MAX_CH_ID)); chNum++)
    {
        UTILS_assert (unAssignedChCnt < SYSTEM_SW_MS_MAX_WIN);

        scInst = chMapParams->chNumToScInstMap[chNum];
        if (scInst >= chMapParams->numScalarInst)
        {
            /* Un Mapped channel - will be assigned to one of the scalar.
                No video streams are expected on these channels. */
            unAssignedCh[unAssignedChCnt] = chNum;
            unAssignedChCnt++;
            continue;
        }

        /* Update the Channel number to instance map */
        localCh2InstMap[chNum] = scInst;
        chUsedPerSc[scInst]++;

        if (chUsedPerSc[scInst] >= SYSTEM_SW_MS_MAX_WIN_PER_SC)
        {
            rtnValue = FVID2_EFAIL;
            Vps_printf(" %d: SWMS: ERROR : Number of channels that could be "
                    "Supported by scalar instance %d exceded!!! \n"
                    "Maximum %d channel could be supported by scalar %d\n",
                    Utils_getCurTimeInMsec(),
                    scInst, scInst, SYSTEM_SW_MS_MAX_WIN_PER_SC);
            break;
        }
        else
        {
            rtnValue = FVID2_SOK;
        }
    }

    if (rtnValue != FVID2_SOK)
    {
        return rtnValue;
    }

    /* For un-assigned channel, associate a scalar ID */
    /* Each scalar will support maximum of 18 channels, ensure to split
        the un-mapped channels accordingly */
    /* Policy - Start with last SC. Assing an many channels as possible */
    scInst = pObj->createArgs.numSwMsInst;
    while ((scInst > 0) && (unAssignedChCnt > 0))
    {
        scInst--;

        while ((unAssignedChCnt > 0) &&
               (chUsedPerSc[scInst] < SYSTEM_SW_MS_MAX_WIN_PER_SC))
        {
            unAssignedChCnt--;
            chUsedPerSc[scInst]++;

            UTILS_assert(localCh2InstMap[unAssignedCh[unAssignedChCnt]] ==
                SWMS_LINK_MAP_INVALID);

            localCh2InstMap[unAssignedCh[unAssignedChCnt]] = scInst;
        }

    }

    if (unAssignedChCnt != 0)
    {
        Vps_printf(" %d: SWMS: ERROR : Could not allocate SC instances for all "
                    "channels !!!\n", Utils_getCurTimeInMsec());
        return FVID2_EFAIL;
    }

#ifdef SYSTEM_DEBUG_SWMS
    Vps_printf("\n %d: SWMS: Link ID %d Channel map !!!\n", Utils_getCurTimeInMsec(), pObj->linkId);
    Vps_printf(" | In | SC   | \n");
    Vps_printf(" | Ch | Inst | \n");
    Vps_printf(" +----+------+ \n");
    for (chNum = 0; chNum < SYSTEM_SW_MS_MAX_CH_ID; chNum++)
    {
        scInst = localCh2InstMap[chNum];
        if (scInst != SWMS_LINK_MAP_INVALID)
        {
            Vps_printf(" |%4d|%6d|\n", chNum, scInst);
        }
    }
    Vps_printf(" +----+------+ \n");
    Vps_printf(" \n");
#endif /* SYSTEM_DEBUG_SWMS */
    SwMsLink_drvLock(pObj);

    memcpy(&pObj->chNum2InstIdMap[0],
           &localCh2InstMap[0],
           sizeof(pObj->chNum2InstIdMap));

    /* The channel to instace map could have changed, ensure to update the
        instances window to logical channel map */
    SwMsLink_updateWinIdToScInstLChMap(pObj);

    /* Update the rtparams - to indicate channel map might have changed */
    pObj->scChMapRtParamUpdate = TRUE;
    /* Otherwise, anyways update will occur */
    SwMsLink_drvUnlock(pObj);

    return FVID2_SOK;
}

static Int32 SwMsLink_updateWinIdToScInstLChMap(SwMsLink_Obj * pObj)
{
    UInt32 instId, i, winChanNum, winId;
    UInt32 scInstLChNum[SYSTEM_SW_MS_MAX_WIN_PER_SC];
    UInt32 winIdInvalidCh[SYSTEM_SW_MS_MAX_WIN];
    UInt32 winIdInvalidChIndex;

    for (instId = 0; instId < SYSTEM_SW_MS_MAX_INST; instId++)
    {
        memset(&pObj->DrvObj[instId].win2ScLChMap[0], 0xFF,
            sizeof(pObj->DrvObj[instId].win2ScLChMap));
    }
    memset(&scInstLChNum[0], 0, sizeof(scInstLChNum));
    memset(&winIdInvalidCh[0], 0, sizeof(winIdInvalidCh));
    winIdInvalidChIndex = 0;

    for (winId = 0; winId < pObj->layoutParams.numWin; winId++)
    {
        winChanNum = pObj->layoutParams.winInfo[winId].channelNum;
        if (winChanNum == SYSTEM_SW_MS_INVALID_ID)
        {
            winIdInvalidCh[winIdInvalidChIndex] = winId;
            winIdInvalidChIndex++;
            UTILS_assert(winIdInvalidChIndex < SYSTEM_SW_MS_MAX_WIN);
            continue;
        }

        instId = pObj->chNum2InstIdMap[winChanNum];

        UTILS_assert(scInstLChNum[instId] < SYSTEM_SW_MS_MAX_WIN_PER_SC);

        pObj->DrvObj[instId].win2ScLChMap[winId] = scInstLChNum[instId]++;
        pObj->DrvObj[instId+SYSTEM_SW_MS_MAX_INST].win2ScLChMap[winId] =
              pObj->DrvObj[instId].win2ScLChMap[winId];
    }

    /* Policy - Since we have only one instance of scalar which can process
        a maximum of SYSTEM_SW_MS_MAX_WIN_PER_SC channels,
        We will reuse logical channel numbers.
        Above code, would have ensured all unique logical number for all
        windows (with valid channel number) in a given layout,
        for others we will repeat logical channel numbers */
    if ((pObj->createArgs.numSwMsInst == 1) && (winIdInvalidChIndex != 0))
    {
        while (winIdInvalidChIndex > 0)
        {
            winIdInvalidChIndex--;
            winId = winIdInvalidCh[winIdInvalidChIndex];
            if (scInstLChNum[0] >= SYSTEM_SW_MS_MAX_WIN_PER_SC)
                scInstLChNum[0] = 0;
            /* Instace is always 0, as we have only one instance of SC */
            pObj->DrvObj[0].win2ScLChMap[winId] = scInstLChNum[0];
            pObj->DrvObj[0+SYSTEM_SW_MS_MAX_INST].win2ScLChMap[winId] = scInstLChNum[0];
            scInstLChNum[0]++;

        }
    }

    /* Policy - Window with invalid channel numbers would be assigned to
        scalar with highest ID followed next lower scalar ID */
    if ((pObj->createArgs.numSwMsInst > 1)&& (winIdInvalidChIndex != 0))
    {
        i = 0;
        instId = pObj->createArgs.numSwMsInst;
        while (instId > 0)
        {
            instId--;
            while ((scInstLChNum[instId] < SYSTEM_SW_MS_MAX_WIN_PER_SC) &&
                    (i < winIdInvalidChIndex))
            {
                winId = winIdInvalidCh[i];
                pObj->DrvObj[instId].win2ScLChMap[winId] =
                    scInstLChNum[instId]++;
                pObj->DrvObj[instId+SYSTEM_SW_MS_MAX_INST].win2ScLChMap[winId] =
                    pObj->DrvObj[instId].win2ScLChMap[winId];
                i++;
            }
        }

        UTILS_assert(i == winIdInvalidChIndex);
    }
    return FVID2_SOK;
}


Int32 SwMsLink_drvGetInputChTimeInfo(SwMsLink_Obj *pObj, SwMsLink_ChTimeParams *chParams)
{
    SwMsLink_drvLock(pObj);

    UTILS_assert(chParams != NULL);

    if (chParams->chId < pObj->inQueInfo.numCh)
    {
        chParams->displayStarted = pObj->chObj[chParams->chId].displayStarted;
        chParams->firstPTS = pObj->chObj[chParams->chId].firstPTS;
        chParams->currPTS = pObj->chObj[chParams->chId].currPTS;
    }
    else
    {
        chParams->displayStarted = FALSE;
        chParams->firstPTS = 0;
        chParams->currPTS = 0;
    }

    SwMsLink_drvUnlock(pObj);
    return FVID2_SOK;
}

