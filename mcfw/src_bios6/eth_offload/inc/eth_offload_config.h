/*
 * Ethernet Offload Configuration File
 * - A Common file between both host and remote processor code
 *
 * Copyright (C) 2012 Texas Instruments.
 *
 * ---------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ---------------------------------------------------------------------------
 * History:
 * 0   Viswanath Dibbur - Initial version.
 */

#ifndef ETH_OFFLOAD_CONFIG_H
#define ETH_OFFLOAD_CONFIG_H

#define ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES       (128)

/** \brief Floor a integer value. */
#define ETH_OFFLOAD_FLOOR(val, align)  (((val) / (align)) * (align))

/** \brief Align a integer value. */
#define ETH_OFFLOAD_ALIGN(val, align)  ETH_OFFLOAD_FLOOR(((val) + (align)-1), (align))
/*
 *  General and Common defines
 */
#if defined(TI_8107_BUILD)
#define ETH_OFFLOAD_PROCID_HOST			(2)
/* On DM8107 PROCID for Video M3 is zero */
#define ETH_OFFLOAD_PROCID_REMOTE	    	(0)
#else
#define ETH_OFFLOAD_PROCID_HOST			(3)
/*On DM8168 PROCID for DSP is zero */
#define ETH_OFFLOAD_PROCID_REMOTE		(0)
#endif

#define ETH_OFFLOAD_SHARED_REGION	 	(0)
#define ETH_OFFLOAD_SWDESC_MAX			(512)
#define ETH_OFFLOAD_HWDESC_MAX			(256)

/*
 *  Remote Procedure Call definitions
 */
#define ETH_OFFLOAD_RPC_REGIONID		ETH_OFFLOAD_SHARED_REGION
#define ETH_OFFLOAD_RPC_NOTIFY_LINEID		(0)
#define ETH_OFFLOAD_RPC_NOTIFY_EVENTID		(11)


#define ETH_OFFLOAD_SHM_PHYS_ADDR       (0x40300000)

/*
 *  ListMP adn Shared memory definitions for Queue Mech
 */
#define ETH_OFFLOAD_Q_REGIONID			ETH_OFFLOAD_SHARED_REGION
#define ETH_OFFLOAD_Q_FLUSH_THRESHOLD		(1)
#define ETH_OFFLOAD_Q_PERIODICITY		(5)
#define ETH_OFFLOAD_Q_WEIGHT			(128)

#define ETH_OFFLOAD_Q_NOTIFY_LINEID		(0)
#define ETH_OFFLOAD_Q_NOTIFY_EVENTID		(10)

#endif
