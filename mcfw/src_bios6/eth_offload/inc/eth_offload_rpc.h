/*
 * Ethernet Offload Remote Procedure Mechanism Definitions
 *
 * Copyright (C) 2012 Texas Instruments.
 *
 * ---------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ---------------------------------------------------------------------------
 * History:
 * 0   Viswanath Dibbur - Initial version.
 */

#ifndef ETH_OFFLOAD_RPC_H
#define ETH_OFFLOAD_RPC_H

#include <ti/ipc/Ipc.h>
#include <ti/ipc/ListMP.h>
#include <ti/ipc/HeapMemMP.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/Notify.h>
#include <ti/ipc/MessageQ.h>
#include "eth_offload_config.h"

enum funcid{
	ETH_OFFLOAD_EMAC_GET_STATISTICS,
	ETH_OFFLOAD_FUNCID_EMAC_SETUP,
	ETH_OFFLOAD_FUNCID_OPEN,
	ETH_OFFLOAD_FUNCID_CLOSE,
	ETH_OFFLOAD_FUNCID_START,
	ETH_OFFLOAD_FUNCID_COALESCE,
	ETH_OFFLOAD_FUNCID_UPDATE,
	ETH_OFFLOAD_FUNCID_OFLD_STATS,
	ETH_OFFLOAD_FUNCID_OFLD_SETFLAG,
	ETH_OFFLOAD_FUNCID_OFLD_UNSETFLAG
};

#define ETH_OFFLOAD_RPC_ARG_MAX 		1024

/*
 * ETH_OFFLOAD_RpcMsg: To manage rpc mechanism
 * @qMsg: basic message header
 * @rpcStatus: Status of the rpc call, basically the return value of rpc
 * @rpcFuncId: Remote procedure to be called
 * @rpcArg: Argument to the remote function
 * @rpcArgSize: Size of the argument data
 */
struct ETH_OFFLOAD_RpcMsg {
        volatile int                     rpcStatus;
        volatile unsigned int            rpcFuncId;
        volatile unsigned int            rpcArg[ETH_OFFLOAD_RPC_ARG_MAX];
        volatile unsigned int            rpcArgSize;
};

typedef Int32 (*RpcHandler)(struct ETH_OFFLOAD_RpcMsg *rpcMsg);

/*
 * Functions to (un)init RPC mechanism
 * Need to call if Rpc is invoked
 */
extern void ETH_OFFLOAD_Rpc_RegisterHandler(RpcHandler handler);
extern int ETH_OFFLOAD_Rpc_Init(void);
extern int ETH_OFFLOAD_Rpc_DeInit(void);


/* Functions to issue RPC call to remote */
extern int ETH_OFFLOAD_Rpc(unsigned int rpcFuncId, unsigned char *args, unsigned int rpcArgSize);



#endif
