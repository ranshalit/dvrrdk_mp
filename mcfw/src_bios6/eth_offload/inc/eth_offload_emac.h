/*
 * Ethernet Offload Medium Access Control Definitions
 *   A common include file between both Host and Remote
 *   processors driver source
 *
 * Copyright (C) 2012 Texas Instruments.
 *
 * ---------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ---------------------------------------------------------------------------
 * History:
 * 0   Viswanath Dibbur - Initial version.
 */

#ifndef ETH_OFFLOAD_EMAC_H
#define ETH_OFFLOAD_EMAC_H

#include "eth_offload_q.h"

/** @brief Base address of EMAC memory mapped registers */
#define ETH_OFFLOAD_EMAC_0_REGS                 (0x4A100000u)
#define ETH_OFFLOAD_EMAC_1_REGS                 (0x4A120000u)


#define ETH_OFFLOAD_FLAG_RX_CSUM                (0x0001)
#define ETH_OFFLOAD_FLAG_TX_CSUM                (0x0002)

/*
 * ETH_OFFLOAD_open: Header for Driver open populated by host used by remote.
 */
struct ETH_OFFLOAD_setup {
	unsigned int			offloadFlag;
};

struct ETH_OFFLOAD_coalesce {
	unsigned int coal_intvl; //in val
	unsigned int rx_coalesce_usecs; //out val
	unsigned int bus_freq_mhz;
};

struct ETH_OFFLOAD_open {
    unsigned int    phyid;
    unsigned int    mtu;
    unsigned char   nmacaddrs;
    unsigned char   macaddrs[6];
    struct ETH_OFFLOAD_coalesce coal;
};

struct EMAC_update {
	unsigned int		linkup;
	unsigned int		version;
	unsigned int		duplex;
	unsigned int		speed;
	unsigned int		rmii;
};


struct ETH_OFFLOAD_offloadStats {

        unsigned int dsp_hwisrcnt;
        unsigned int arm_napicnt;
        unsigned int arm_nfyisrcnt;
        unsigned int arm_rx_packet_drop;
};


struct ETH_OFFLOAD_stats {
	unsigned long 		multicast;
	unsigned long 		collisions;
	unsigned long 		rx_length_errors;
	unsigned long 		rx_over_errors;
	unsigned long 		rx_fifo_errors;
	unsigned long 		tx_carrier_errors;
	unsigned long 		tx_fifo_errors;
};

extern int ETH_OFFLOAD_Emac_Init();
extern int ETH_OFFLOAD_Emac_DeInit();

#endif
