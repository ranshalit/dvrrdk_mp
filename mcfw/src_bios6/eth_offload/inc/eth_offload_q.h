/*
 * Ethernet Offload Queue Mechanism Definitions
 *
 * Copyright (C) 2012 Texas Instruments.
 *
 * ---------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ---------------------------------------------------------------------------
 * History:
 * 0   Viswanath Dibbur - Initial version.
 */


#ifndef ETH_OFFLOAD_Q_H
#define ETH_OFFLOAD_Q_H

#include <ti/ipc/Ipc.h>
#include <ti/ipc/HeapMemMP.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/Notify.h>
#include <ti/ipc/SharedRegion.h>


#include "eth_offload_config.h"

#define ETH_OFFLOAD_Q_DEQUEUED 0
#define ETH_OFFLOAD_Q_ENQUEUED 1
#define ETH_OFFLOAD_Q_ARM      0
#define ETH_OFFLOAD_Q_DSP      2




#define ETH_OFFLOAD_Q_IS_ENQUEUED(elem, proc)  ETH_OFFLOAD_Q_CheckState(elem, (ETH_OFFLOAD_Q_ENQUEUED | proc))
#define ETH_OFFLOAD_Q_IS_DEQUEUED(elem, proc)  ETH_OFFLOAD_Q_CheckState(elem, (ETH_OFFLOAD_Q_DEQUEUED | proc))
#define ETH_OFFLOAD_Q_SET_ENQUEUED(elem, proc) ETH_OFFLOAD_Q_SetState(elem, (ETH_OFFLOAD_Q_ENQUEUED | proc))
#define ETH_OFFLOAD_Q_SET_DEQUEUED(elem, proc) ETH_OFFLOAD_Q_SetState(elem,(ETH_OFFLOAD_Q_DEQUEUED | proc))


#ifdef __linux__
#define ETH_OFFLOAD_Q_DECLARE_LOCK() spinlock_t lock;
#define MEM_BARRIER()                dmb()
#else
#define ETH_OFFLOAD_Q_DECLARE_LOCK()
#define MEM_BARRIER()
#endif

struct ETH_OFFLOAD_frags {
    volatile unsigned long       dma_address;   /* physical address of packet in memory */
    volatile unsigned int        length;        /* total len of fragment                */
};

#define ETH_OFFLOAD_MAX_FRAGS	5

struct ETH_OFFLOAD_desc {
    volatile unsigned int            state;
    struct ETH_OFFLOAD_frags     sg[ETH_OFFLOAD_MAX_FRAGS];
    volatile unsigned int        nfrags;
    volatile unsigned int        length;        /* total len               */
    volatile unsigned int            status;        /* packet status           */
    volatile void                    *skb;          /* skb addr (virtual addr) */
};


typedef struct {

    unsigned int               rsv0[128];
    struct ETH_OFFLOAD_desc    txSwDesc[ETH_OFFLOAD_SWDESC_MAX];
    unsigned int               rsv1[128];
    struct ETH_OFFLOAD_desc    rxSwDesc[ETH_OFFLOAD_SWDESC_MAX];

} ETH_OFFLOAD_shm;


/* Queue callback to handle incoming packets */
typedef int (*QCALLBACK)(void *hApp, struct ETH_OFFLOAD_desc *data);




/*
 * ETH_OFFLOAD_queue: Complete queue managment for Rx, Tx
 */
struct ETH_OFFLOAD_queue {
    Int8                    name[8];
    struct ETH_OFFLOAD_desc *poolStart; /* Heap pointer */
    struct ETH_OFFLOAD_desc *poolEnd;   /* Heap pointer */
    UInt32                  poolSize;   /* Heap size */
    struct ETH_OFFLOAD_desc *read;
    struct ETH_OFFLOAD_desc *write;
    QCALLBACK               callback;   /* register callback */
    void                    *hApp;      /* callback context */
    ETH_OFFLOAD_Q_DECLARE_LOCK()
};


static inline int ETH_OFFLOAD_Q_CheckState(struct ETH_OFFLOAD_desc *desc, unsigned int value)
{
    int status = 0;

    MEM_BARRIER();
    if(desc->state == value)
        status = 1;
    MEM_BARRIER();
    return status;
}

static inline void ETH_OFFLOAD_Q_SetState(struct ETH_OFFLOAD_desc *desc, unsigned int value)
{
    volatile UInt32 tmpRead; /* to ensure value is pushed to memory */

    MEM_BARRIER();
    desc->state = value;
    MEM_BARRIER();

    tmpRead = desc->state;
}

static inline struct ETH_OFFLOAD_desc *ETH_OFFLOAD_Q_move (struct ETH_OFFLOAD_queue *Q, struct ETH_OFFLOAD_desc *elem)
{
    elem++;
    if (elem >= Q->poolEnd) {
        elem = Q->poolStart;
    }
    return elem;
}


/* Function to create and destroy Rx,Tx Q */
extern struct ETH_OFFLOAD_queue *ETH_OFFLOAD_Q_Create(UInt32 maxElems, QCALLBACK handler, void *hApp, Int8 *name, struct ETH_OFFLOAD_desc *pPoolStart);
extern Int32 ETH_OFFLOAD_Q_Destroy(struct ETH_OFFLOAD_queue *);

/* Functions to nQ and dQ the Rx,Tx desc (packets) */
extern struct ETH_OFFLOAD_desc *ETH_OFFLOAD_Q_Dequeue(struct ETH_OFFLOAD_queue *Q);
extern Int32 ETH_OFFLOAD_Q_Enqueue(struct ETH_OFFLOAD_queue *Q, struct ETH_OFFLOAD_desc *element);

extern Int32 ETH_OFFLOAD_Q_Reset(struct ETH_OFFLOAD_queue *Q);

Int32 ETH_OFFLOAD_Q_Poll(struct ETH_OFFLOAD_queue *Q, UInt32 weight);


#endif
