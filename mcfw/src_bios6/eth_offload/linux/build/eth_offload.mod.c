#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x7a0cf9e5, "module_layout" },
	{ 0x42579342, "kmalloc_caches" },
	{ 0xf9a482f9, "msleep" },
	{ 0x75c6879d, "phy_disconnect" },
	{ 0xe7d52bd9, "clk_enable" },
	{ 0x877f0a0b, "mem_map" },
	{ 0xa90c928a, "param_ops_int" },
	{ 0x6723e788, "skb_pad" },
	{ 0x314119c2, "dev_set_drvdata" },
	{ 0xbbd45ed4, "hrtimer_forward" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0x79aa04a2, "get_random_bytes" },
	{ 0xf64144bf, "napi_complete" },
	{ 0x52760ca9, "getnstimeofday" },
	{ 0x4a5361cf, "hrtimer_cancel" },
	{ 0x6f418ff2, "clk_disable" },
	{ 0x34be80da, "Memory_free" },
	{ 0xc6215c1d, "netif_carrier_on" },
	{ 0x90f2b27c, "ethtool_op_get_sg" },
	{ 0x552e7173, "netif_carrier_off" },
	{ 0x44ad06b7, "phy_ethtool_gset" },
	{ 0x2e1ca751, "clk_put" },
	{ 0x4a37af33, "___dma_single_cpu_to_dev" },
	{ 0xfca87333, "clk_get_rate" },
	{ 0x6ccf7bd7, "__pv_phys_offset" },
	{ 0x406a0cc3, "phy_start_aneg" },
	{ 0xc524ac2b, "__pskb_pull_tail" },
	{ 0xf6288e02, "__init_waitqueue_head" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x5f754e5a, "memset" },
	{ 0xf6b546fb, "alloc_etherdev_mq" },
	{ 0x2853f453, "phy_start" },
	{ 0xbff6f85d, "dev_alloc_skb" },
	{ 0xc949e81c, "dev_err" },
	{ 0xea147363, "printk" },
	{ 0x538acb30, "ethtool_op_get_link" },
	{ 0x9f1d5edd, "SharedRegion_getHeap" },
	{ 0xdb3877d, "___dma_single_dev_to_cpu" },
	{ 0x18a766cb, "free_netdev" },
	{ 0x328a05f1, "strncpy" },
	{ 0x53f75676, "register_netdev" },
	{ 0x27455fb1, "netif_receive_skb" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x9fdb3bb, "platform_get_resource" },
	{ 0x383e1d3e, "netif_napi_add" },
	{ 0xc438efc1, "platform_driver_register" },
	{ 0x6b390089, "bus_find_device" },
	{ 0x2196324, "__aeabi_idiv" },
	{ 0x819dccdf, "dev_kfree_skb_any" },
	{ 0x8c13984b, "dev_notice" },
	{ 0x4ada2c49, "napi_gro_receive" },
	{ 0xdb175ce9, "_dev_info" },
	{ 0xc46385b6, "kmem_cache_alloc" },
	{ 0x2b835d22, "ethtool_op_set_sg" },
	{ 0x1bfae0f0, "__napi_schedule" },
	{ 0x92980dda, "Notify_unregisterEvent" },
	{ 0xe8b4bbc, "___dma_page_cpu_to_dev" },
	{ 0x1000e51, "schedule" },
	{ 0xcc63eadc, "___dma_page_dev_to_cpu" },
	{ 0xe9ce8b95, "omap_ioremap" },
	{ 0x72becf51, "hrtimer_start" },
	{ 0x15331242, "omap_iounmap" },
	{ 0xa2a059a4, "eth_type_trans" },
	{ 0xc27487dd, "__bug" },
	{ 0xa79bb9df, "Notify_registerEvent" },
	{ 0x5899d479, "clk_get" },
	{ 0xb9e52429, "__wake_up" },
	{ 0xf6ebc03b, "net_ratelimit" },
	{ 0x36cb7d13, "Memory_alloc" },
	{ 0xa6fff07b, "ethtool_op_set_tx_hw_csum" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0x75a17bed, "prepare_to_wait" },
	{ 0x6128b5fc, "__printk_ratelimit" },
	{ 0xc9cbfbd7, "hrtimer_init" },
	{ 0x1a72afc9, "phy_connect" },
	{ 0xe179b20d, "Notify_sendEvent" },
	{ 0xbf139082, "ethtool_op_get_tx_csum" },
	{ 0x8893fa5d, "finish_wait" },
	{ 0xbb9a2c75, "ktime_add_ns" },
	{ 0xb881a0f9, "dev_warn" },
	{ 0x3d8d9b27, "unregister_netdev" },
	{ 0x1639c687, "phy_ethtool_sset" },
	{ 0x8c5c8d67, "__netif_schedule" },
	{ 0xb5bec987, "mdio_bus_type" },
	{ 0xd2eca9a2, "consume_skb" },
	{ 0x19a7dcf8, "platform_driver_unregister" },
	{ 0xd546de5b, "SharedRegion_getSRPtr" },
	{ 0x695964f9, "skb_put" },
	{ 0x2c6c395f, "dev_get_drvdata" },
	{ 0xe914e41e, "strcpy" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=syslink";


MODULE_INFO(srcversion, "6F9679DFEDC0F28CF50CF5B");
