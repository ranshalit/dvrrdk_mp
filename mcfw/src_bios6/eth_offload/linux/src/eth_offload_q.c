#include <linux/spinlock.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <linux/netdevice.h>

#include <ti/syslink/Std.h>
#include <eth_offload_q.h>
#include <eth_offload_emac.h>

/*
 * ETH_OFFLOAD_Q_Poll: Function to process inQ on notification from host
 * @Q: Q in which the in coming packets have to be processed
 * @weight: weight to determine how many packets to flush at a time
 * @return: 0(on success)
 */
Int32 ETH_OFFLOAD_Q_Poll(struct ETH_OFFLOAD_queue *Q, UInt32 weight)
{
	UInt32 count = 0;

	/* Loop and process all dequeued elements */
	while (1) {

        if(ETH_OFFLOAD_Q_IS_ENQUEUED(Q->read, ETH_OFFLOAD_Q_DSP)==0)
            break;

		/* Call callback to process each desc */
		if (!Q->callback(Q->hApp, Q->read)) {
			break;
		}

		ETH_OFFLOAD_Q_SET_DEQUEUED(Q->read, ETH_OFFLOAD_Q_ARM);
		Q->read = ETH_OFFLOAD_Q_move(Q, Q->read);
		count++;

		if (count >= weight) {
			break;
		}
	}

	return count;
}

Int32 ETH_OFFLOAD_Q_Reset(struct ETH_OFFLOAD_queue *Q)
{
	struct ETH_OFFLOAD_desc *elem = Q->poolStart;

	while (elem < Q->poolEnd) {

		if (!ETH_OFFLOAD_Q_IS_DEQUEUED(elem, ETH_OFFLOAD_Q_ARM)) {
            {
	            struct sk_buff *skb = (struct sk_buff *)elem->skb;

                if(skb!=NULL)
                {
                    dev_kfree_skb_any(skb);
                }
            }
			ETH_OFFLOAD_Q_SET_DEQUEUED(elem, ETH_OFFLOAD_Q_ARM);
		}
		elem++;
	}
	Q->read = Q->write = Q->poolStart;
	return 0;
}

/*
 * ETH_OFFLOAD_Q_Destroy: Function to destroy the Q
 * @Q: Q pointer to ETH_OFFLOAD_queue
 * @return: 1 on success, else -1
 */
Int32 ETH_OFFLOAD_Q_Destroy(struct ETH_OFFLOAD_queue *Q)
{
	if (NULL == Q) {
		return -1;
	}

	/* Free the full Q */
	kfree(Q);

	return 1;
}

/*
 * ETH_OFFLOAD_Q_Create: Function to create a Q
 * @maxElems: num elements in freeepool
 * @elemSize: size of one element
 * @handler: callback for in packets
 * @in: queue name for inQ
 * @out: queue name for outQ
 * @return: new Q, null on failure
 */
struct ETH_OFFLOAD_queue *ETH_OFFLOAD_Q_Create(UInt32 maxElems, QCALLBACK handler, void *hApp, Int8 *name, struct ETH_OFFLOAD_desc *pPoolStart)
{
	struct ETH_OFFLOAD_queue *Q = (struct ETH_OFFLOAD_queue *)
					kmalloc(sizeof(struct ETH_OFFLOAD_queue), GFP_ATOMIC);

	if (NULL == Q) {
		return NULL;
	}

	/* Carve out pool size (maxElems) out of free pool */
	Q->poolSize = maxElems*sizeof(struct ETH_OFFLOAD_desc);
    Q->poolStart = (struct ETH_OFFLOAD_desc *)((UInt8*)pPoolStart);

	memset ((void*)Q->poolStart, 0, Q->poolSize);
	Q->poolEnd   = (struct ETH_OFFLOAD_desc *)((UInt8*)Q->poolStart + Q->poolSize);
	Q->read = Q->write = Q->poolStart;
	Q->callback = handler;
	Q->hApp = hApp;
	strncpy(Q->name, name, sizeof(Q->name));

	/* Requesting for a small memory */
	spin_lock_init(&Q->lock);

	return Q;
}

/*
 * ETH_OFFLOAD_Q_Dequeue: Function to dq desc from Tx or Rx Q
 * @Q: Q (Tx or Rx)
 * @return: desc, null otherwise
 */
struct ETH_OFFLOAD_desc *ETH_OFFLOAD_Q_Dequeue(struct ETH_OFFLOAD_queue *Q)
{
	struct ETH_OFFLOAD_desc *elem = NULL;
	unsigned long flags;

	spin_lock_irqsave(&Q->lock, flags);
	if (ETH_OFFLOAD_Q_IS_DEQUEUED(Q->write , ETH_OFFLOAD_Q_ARM)) {
		elem = Q->write;
		Q->write = ETH_OFFLOAD_Q_move(Q, Q->write);
	}
	spin_unlock_irqrestore(&Q->lock, flags);

	return elem;
}

void ETH_OFFLOAD_Q_Dump (struct ETH_OFFLOAD_queue *Q)
{
	struct ETH_OFFLOAD_desc  *elem = Q->poolStart;
	int i = 0;
	char *state[4] = {"ARM|DQ  - AVAILABLE at ARM", "ARM|ENQ - GIVEN to DSP", "DSP|DQ  - GIVEN to HW", "DSP|ENQ - GIVEN to ARM"};
	if (!Q)
		return;

	printk (KERN_ERR
			"Q name = %s\n poolstart=%p poolend=%p\n poolsize=%u \n"
			" read=%p write=%p\n\n",
			Q->name, Q->poolStart, Q->poolEnd, Q->poolSize,
			Q->read, Q->write);

	while (elem < Q->poolEnd){
		printk (KERN_ERR"%d: elem=%p state= (%d) %s\n",i++, elem, elem->state, state[elem->state&0x3]);
		elem++;
	}
	printk (KERN_ERR"------------------------------\n");
}
