/*
 * DSP Accelarated Ethernet Medium Access Controller
 *
 * Copyright (C) 2009 Texas Instruments.
 *
 * ---------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ---------------------------------------------------------------------------
 * History:
 * 0   Viswanath Dibbur - Initial version.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/string.h>
#include <linux/timer.h>
#include <linux/time.h>
#include <linux/syscalls.h>
#include <linux/errno.h>
#include <linux/in.h>
#include <linux/ioport.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/scatterlist.h>
#include <linux/skbuff.h>
#include <linux/ethtool.h>
#include <linux/highmem.h>
#include <linux/proc_fs.h>
#include <linux/ctype.h>
#include <linux/version.h>
#include <linux/spinlock.h>
#include <linux/dma-mapping.h>
#include <linux/clk.h>
#include <linux/platform_device.h>
#include <linux/semaphore.h>
#include <linux/phy.h>
#include <linux/bitops.h>
#include <linux/io.h>
#include <linux/uaccess.h>
#include <linux/delay.h>
#include <linux/davinci_emac.h>
#include <asm/irq.h>
#include <asm/page.h>
#include <linux/hrtimer.h>

#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/kthread.h>

#include <ti/syslink/Std.h> /* Need to remove this from this file */
#include <eth_offload_rpc.h>
#include <eth_offload_q.h>
#include <eth_offload_emac.h>


static struct clk *emac_clk;

static int gro_enable;

module_param(gro_enable, int, 0);
MODULE_PARM_DESC(gro_enable, "GRO ON/OFF, 1: ON, 0: OFF");

static int gso_enable;

module_param(gso_enable, int, 0);
MODULE_PARM_DESC(gso_enable, "GSO ON/OFF, 1: ON, 0: OFF");


static int ufo_enable;

module_param(ufo_enable, int, 0);
MODULE_PARM_DESC(ufo_enable, "UFO ON/OFF, 1: ON, 0: OFF");

static int debug_level;

module_param(debug_level, int, 1);
MODULE_PARM_DESC(debug_level, "DSP Accelarated EMAC debug level (NETIF_MSG bits)");

static unsigned int shared_mem_phys_addr=0;

module_param(shared_mem_phys_addr, int, 1);
MODULE_PARM_DESC(shared_mem_phys_addr, "Shared memory communication physical address");



/* in micro-sec's */
#define EMAC_POLL_INTERVAL_INIT_USECS    (10000u)   /* 10.0 ms */
#define EMAC_POLL_INTERVAL_MAX_USECS	 (1000u)    /*  1.0 ms */
#define EMAC_POLL_INTERVAL_MIN_USECS	 (100u)     /*  0.1 ms */
#define EMAC_POLL_INTERVAL_DEFAULT_USECS (500u)


#define EMAC_POLL_INTERVAL_INIT_NSECS   (EMAC_POLL_INTERVAL_INIT_USECS*1000u)
#define EMAC_POLL_INTERVAL_MAX_NSECS	(EMAC_POLL_INTERVAL_MAX_USECS*1000u)

#define PHY_CONFIG_REG		22
#define EMAC_MACCONTROL		0x160

#define EMAC_MACCONTROL_FULLDUPLEXEN	BIT(0)
#define EMAC_MACCONTROL_RMIISPEED_MASK	BIT(15)

#define EMAC2				(0x02)
#define EMAC_DEF_MIN_ETHPKTSIZE		(60) /* Minimum ethernet pkt size */
#define EMAC_DEF_MAX_FRAME_SIZE		(1500 + 14 + 4 + 4)

/* Netif debug messages possible */
#define OFFLOAD_DEBUG_FLAGS	    ( \
		NETIF_MSG_DRV       | \
		NETIF_MSG_PROBE     | \
		NETIF_MSG_LINK      | \
		NETIF_MSG_TIMER     | \
		NETIF_MSG_IFDOWN    | \
		NETIF_MSG_IFUP      | \
		NETIF_MSG_RX_ERR    | \
		NETIF_MSG_TX_ERR    | \
		NETIF_MSG_TX_QUEUED | \
		NETIF_MSG_INTR      | \
		NETIF_MSG_TX_DONE   | \
		NETIF_MSG_RX_STATUS | \
		NETIF_MSG_PKTDATA   | \
		NETIF_MSG_HW        | \
		NETIF_MSG_WOL       )

/* Netif supported offfload flags for GRO and GSO feature offload */
/*
	NETIF_F_GRO
*/
#define OFFLOAD_GRO_FLAGS	   ( NETIF_F_GRO )
#define OFFLOAD_GSO_FLAGS	   ( NETIF_F_GSO | NETIF_F_HW_CSUM | NETIF_F_SG)


#define ETH_OFFLOAD_MTU 	(1514)
#define ETH_OFFLOAD_LRO_SIZE	(64*1024)

/* version info */
#define OFFLOAD_MAJOR_VERSION	0
#define OFFLOAD_MINOR_VERSION	1
#define OFFLOAD_MODULE_VERSION	"0.1"
MODULE_VERSION(OFFLOAD_MODULE_VERSION);
static const char offload_version_string[] = "TI DSP Accelarated EMAC Linux v0.1";




/*************************************************************************
 *  Structures
 *************************************************************************/
/* offload_internal: EMAC internal data structure
 *
 * EMAC adapter internal data structure
 */
struct offload_internal {
	u32 msg_enable;
	struct net_device *ndev;
	struct platform_device *pdev;
	struct ETH_OFFLOAD_queue *txQ;
	struct ETH_OFFLOAD_queue *rxQ;
	struct ETH_OFFLOAD_offloadStats offloadstats;
	struct napi_struct napi;
	char mac_addr[6];
	u32 emac_base_phys;
	u32 link;   /* 1=link on, 0=link off */
	u32 speed;  /* 0=Auto Neg, 1=No PHY, 10,100, 1000 - mbps */
	u32 duplex; /* Link duplex: 0=Half, 1=Full */
	u32 rx_buf_size;
	u32 bus_freq_mhz;
	u32 coal_intvl_usecs;
    unsigned long coal_intvl_nsecs;
	u8 rmii_en;
	u8 version;
	u32 rx_addr_type;
	u16 nfy_procId;
	u16 nfy_lineId;
	u32 nfy_eventId;
	u32 rx_csum_enable;
	const char *phy_id;
	struct phy_device *phydev;
	spinlock_t txq_lock;

    unsigned long long statsStartTime;

    struct hrtimer poll_timer;
    int eth_if;
};

extern void ETH_OFFLOAD_Q_Dump (struct ETH_OFFLOAD_queue *Q); //TEMP to be removed

struct offload_internal *interglobal;

ETH_OFFLOAD_shm *pETH_OFFLOAD_shm=NULL;

/*************************************************************************
 *  Error Codes for both Rx and Tx
 *************************************************************************/

/* EMAC TX Host Error description strings */
char *emac_txhost_errcodes[16] = {
	"No error", "SOP error", "Ownership bit not set in SOP buffer",
	"Zero Next Buffer Descriptor Pointer Without EOP",
	"Zero Buffer Pointer", "Zero Buffer Length", "Packet Length Error",
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",
	"Reserved", "Reserved", "Reserved", "Reserved"
};

/* EMAC RX Host Error description strings */
char *emac_rxhost_errcodes[16] = {
	"No error", "Reserved", "Ownership bit not set in input buffer",
	"Wrong Checksum", "Zero Buffer Pointer", "Reserved", "Reserved",
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",
	"Reserved", "Reserved", "Reserved", "Reserved"
};


/*************************************************************************
 *  ethtool functions
 *************************************************************************/

unsigned long long offload_get_cur_time_in_usec(void)
{
	struct timespec tm;
	unsigned long usec = 1000000L;

	getnstimeofday (&tm);

    return (unsigned long long)(tm.tv_sec*usec + tm.tv_nsec/1000);
}

/**
 * offload_get_drvinfo: Get EMAC driver information
 * @ndev: The EMAC network adapter
 * @info: ethtool info structure containing name and version
 *
 * Returns EMAC driver information (name and version)
 *
 */
static void offload_get_drvinfo(struct net_device *ndev,
		struct ethtool_drvinfo *info)
{
	strcpy(info->driver, offload_version_string);
	strcpy(info->version, OFFLOAD_MODULE_VERSION);
}

/**
 * offload_get_settings: Get EMAC settings
 * @ndev: The EMAC network adapter
 * @ecmd: ethtool command
 *
 * Executes ethool get command
 *
 */
static int offload_get_settings(struct net_device *ndev,
		struct ethtool_cmd *ecmd)
{
	struct offload_internal *inter = netdev_priv(ndev);

	if (inter->phydev) {
		return phy_ethtool_gset(inter->phydev, ecmd);
	}
	else
		return -EOPNOTSUPP;
}

/**
 * offload_set_settings: Set EMAC settings
 * @ndev: The EMAC network adapter
 * @ecmd: ethtool command
 *
 * Executes ethool set command
 *
 */
static int offload_set_settings(struct net_device *ndev, struct ethtool_cmd *ecmd)
{
	struct offload_internal *inter = netdev_priv(ndev);
	if (inter->phydev) {
		return phy_ethtool_sset(inter->phydev, ecmd);
	}
	else
		return -EOPNOTSUPP;
}

static int emac_set_coalesce(struct net_device *ndev,
				struct ethtool_coalesce *coal){
	struct offload_internal *inter = netdev_priv(ndev);

	inter->coal_intvl_usecs = coal->rx_coalesce_usecs;
    if(inter->coal_intvl_usecs < EMAC_POLL_INTERVAL_MIN_USECS)
        inter->coal_intvl_usecs = EMAC_POLL_INTERVAL_MIN_USECS;

    if(inter->coal_intvl_usecs > EMAC_POLL_INTERVAL_MAX_USECS)
        inter->coal_intvl_usecs = EMAC_POLL_INTERVAL_MAX_USECS;

    inter->coal_intvl_nsecs = inter->coal_intvl_usecs*1000;

	printk(KERN_INFO " [ETH_OFFLOAD] set coal inrvl of %d usecs\n", inter->coal_intvl_usecs);

	return 0;
}

static int emac_get_coalesce(struct net_device *ndev,
		struct ethtool_coalesce *coal)
{
	struct offload_internal *inter = netdev_priv(ndev);

	coal->rx_coalesce_usecs = inter->coal_intvl_usecs;
	return 0;

}

static void offload_print_offstats(struct ETH_OFFLOAD_offloadStats *pstats, unsigned int elaspedTimeInMsec)
{
    printk (" EVENT INFORMATION\n");
    printk (" =================\n");
    printk (" \n");
    printk (" ARM NOTIFY ISR  : %u (%d per sec)\n", pstats->arm_nfyisrcnt, (pstats->arm_nfyisrcnt*10)/(elaspedTimeInMsec/100));
    printk (" ARM NAPI        : %u (%d per sec)\n", pstats->arm_napicnt  , (pstats->arm_napicnt*10)/(elaspedTimeInMsec/100));
    printk (" RX  PACKET DROP : %u (%d per sec)\n", pstats->arm_rx_packet_drop  , (pstats->arm_rx_packet_drop*10)/(elaspedTimeInMsec/100));
    printk ("\n");
}


static void offload_reset_perf(void)
{
    struct offload_internal *inter = interglobal;

    inter->offloadstats.arm_napicnt = 0;
    inter->offloadstats.arm_nfyisrcnt = 0;
    inter->offloadstats.arm_rx_packet_drop = 0;
    inter->statsStartTime = offload_get_cur_time_in_usec();
}

u32 offload_printall(struct net_device *netdev)
{
    struct offload_internal *inter = interglobal;
    unsigned int elaspedTimeInMsec;

    elaspedTimeInMsec = offload_get_cur_time_in_usec() - inter->statsStartTime;
    elaspedTimeInMsec /= 1000;

    printk (" \n");
    printk (" DETAILED OFFLOAD STATISTICS\n");
    printk (" ***************************\n");
    printk (" \n");
	printk (" Total Elasped Time : %d msec \n", elaspedTimeInMsec);
    printk (" \n");

	offload_print_offstats(&inter->offloadstats, elaspedTimeInMsec);

    offload_reset_perf();

	return 0;
}

u32 offload_get_rx_csum(struct net_device *dev)
{
    struct offload_internal *inter = netdev_priv(dev);

    return inter->rx_csum_enable;
}

int offload_set_rx_csum(struct net_device *dev, u32 data)
{
   struct offload_internal *inter = netdev_priv(dev);
   int flag = ETH_OFFLOAD_FLAG_RX_CSUM;

   if (data) {
      if (ETH_OFFLOAD_Rpc(ETH_OFFLOAD_FUNCID_OFLD_SETFLAG, (void *)&flag, sizeof(flag)) < 0) {
	  return 0;
      }
      inter->rx_csum_enable = 1;
      printk(" [ETH OFFLOAD] Rx Csum is ENABLED !!!\n");
   } else {
      if (ETH_OFFLOAD_Rpc(ETH_OFFLOAD_FUNCID_OFLD_UNSETFLAG, (void *)&flag, sizeof(flag)) < 0) {
	  return 0;
      }
      inter->rx_csum_enable = 0;
      printk(" [ETH OFFLOAD] Rx Csum is DISABLED !!!\n");
   }
   return 0;
}

int offload_set_tx_csum(struct net_device *dev, u32 data)
{
   int flag = ETH_OFFLOAD_FLAG_TX_CSUM;

   /* Only Tx csum, is a static configuration. Pause the queue,
    * then inform DSP, then set the features
    */
   netif_stop_queue(dev);
   /* ToDo: Wait till tx queue to dsp is flushed out */
   if (data) {
      if (ETH_OFFLOAD_Rpc(ETH_OFFLOAD_FUNCID_OFLD_SETFLAG, (void *)&flag, sizeof(flag)) < 0) {
	 return 0;
      }
      printk(" [ETH OFFLOAD] HW checksum offload is ENABLED !!!\n");
   } else {
      if (ETH_OFFLOAD_Rpc(ETH_OFFLOAD_FUNCID_OFLD_UNSETFLAG, (void *)&flag, sizeof(flag)) < 0) {
	 return 0;
      }
      printk(" [ETH OFFLOAD] HW checksum offload is DISABLED !!!\n");
   }
   ethtool_op_set_tx_hw_csum(dev, data);
   netif_wake_queue(dev);
   return 0;
}

int offload_set_gso(struct net_device *dev, u32 data)
{
   if (data) {
      dev->features |= NETIF_F_GSO;
      gso_enable = 1;
      printk(" [ETH OFFLOAD] GSO is ENABLED !!!\n");
   } else {
      dev->features &= ~NETIF_F_GSO;
      gso_enable = 0;
      printk(" [ETH OFFLOAD] GSO is DISABLED !!!\n");
   }
      return 0;
   }

u32 offload_get_gso(struct net_device *dev)
{
   return (dev->features & NETIF_F_GSO) != 0;
}

int offload_set_gro(struct net_device *dev, u32 data)
{
   if (data) {
      dev->features |= NETIF_F_GRO;
      gro_enable = 1;
      printk(" [ETH OFFLOAD] GRO is ENABLED !!!\n");
   } else {
      dev->features &= ~NETIF_F_GRO;
      gro_enable = 0;
      printk(" [ETH OFFLOAD] GRO is DISABLED !!!\n");
   }
   return 0;
}

u32 offload_get_gro(struct net_device *dev)
{
   return (dev->features & NETIF_F_GRO) != 0;
   }

int offload_set_ufo(struct net_device *dev, u32 data)
{
   if (data) {
      dev->features |= NETIF_F_UFO;
      ufo_enable = 1;
      printk(" [ETH OFFLOAD] UFO is ENABLED !!!\n");
   } else {
      dev->features &= ~NETIF_F_UFO;
      ufo_enable = 0;
      printk(" [ETH OFFLOAD] UFO is DISABLED !!!\n");
   }
   return 0;
}

u32 offload_get_ufo(struct net_device *dev)
{
   return (dev->features & NETIF_F_UFO) != 0;
}


/**
 * ethtool_ops: EMAC Ethtool structure
 *
 * Ethtool support for EMAC adapter
 *
 */
static const struct ethtool_ops ethtool_ops = {
	.get_link     = ethtool_op_get_link,
	.get_drvinfo  = offload_get_drvinfo,
	.get_settings = offload_get_settings,
	.set_settings = offload_set_settings,
	.set_coalesce =  emac_set_coalesce,
	.get_coalesce = emac_get_coalesce,

	.get_rx_csum  = offload_get_rx_csum,
	.set_rx_csum  = offload_set_rx_csum,
	.set_tx_csum  = offload_set_tx_csum,
	.get_tx_csum  = ethtool_op_get_tx_csum,
	.set_sg	      = ethtool_op_set_sg,
	.get_sg	      = ethtool_op_get_sg,
	.set_ufo      = offload_set_ufo,
	.get_ufo      = offload_get_ufo,
	#if 0
	/* not supported by 2.6.37 kernel, needs to be back-ported */
	.set_gso      = offload_set_gso,
	.get_gso      = offload_get_gso,
	.set_gro      = offload_set_gro,
	.get_gro      = offload_get_gro
	#endif
};

/*************************************************************************
 *  Support functions
 *************************************************************************/

static void offload_update_phystatus(struct offload_internal *inter)
{
	struct net_device *ndev = inter->ndev;
	struct EMAC_update update;

	memset(&update, 0, sizeof(update));
	if (inter->phydev){
		update.duplex = inter->phydev->duplex;
	}
	else{
		update.duplex = DUPLEX_FULL;
	}
	inter->duplex = update.duplex;

	if (inter->link) {
		update.linkup = 1;
	}
	update.speed = inter->speed;
	update.version = inter->version;
	update.rmii = inter->rmii_en;
	if (ETH_OFFLOAD_Rpc(ETH_OFFLOAD_FUNCID_UPDATE,(void *)&update, sizeof(update)) < 0) {
		printk(KERN_ERR "ETH_OFFLOAD: Phy status update Rpc failed\n");
	}
	if (inter->link) {
		/* link ON */
		if (!netif_carrier_ok(ndev))
			netif_carrier_on(ndev);
		/* reactivate the transmit queue if it is stopped */
		if (netif_running(ndev) && netif_queue_stopped(ndev)){
			netif_wake_queue(ndev);
		}
	} else {
		/* link OFF */
		if (netif_carrier_ok(ndev))
			netif_carrier_off(ndev);
		if (!netif_queue_stopped(ndev))
			netif_stop_queue(ndev);
	}
}

static int match_first_device(struct device *dev, void *data)
{
	static int id = 0;
	id++;
	return id == 3;
}

static void emac_set_phy_config(struct offload_internal *inter, struct phy_device *phy)
{
	struct emac_platform_data *pdata = inter->pdev->dev.platform_data;
	struct mii_bus *miibus;
	int phy_addr = 0;
	u16 val = 0;
	u16 tmp = 0;

	if (!pdata->gigabit_en)
		return;

	if (!phy)
		return;

	miibus = phy->bus;

	if (!miibus)
		return;

	phy_addr = phy->addr;

	/* Following lines enable gigbit advertisement capability even in case
	 * the advertisement is not enabled by default
	 */
	val = miibus->read(miibus, phy_addr, MII_BMCR);
	val |= (BMCR_SPEED100 | BMCR_ANENABLE | BMCR_FULLDPLX);
	miibus->write(miibus, phy_addr, MII_BMCR, val);
	tmp = miibus->read(miibus, phy_addr, MII_BMCR);

	tmp = miibus->read(miibus, phy_addr, MII_BMSR);
	if (tmp & 0x1) {
		val = miibus->read(miibus, phy_addr, MII_CTRL1000);
		val |= BIT(9);

		if(phy->phy_id == 0x001CC914)
			val = val & (~BIT(9));
		miibus->write(miibus, phy_addr, MII_CTRL1000, val);
		tmp = miibus->read(miibus, phy_addr, MII_CTRL1000);
	}

	val = miibus->read(miibus, phy_addr, MII_ADVERTISE);
	val |= (ADVERTISE_10HALF | ADVERTISE_10FULL | \
		ADVERTISE_100HALF | ADVERTISE_100FULL);
	miibus->write(miibus, phy_addr, MII_ADVERTISE, val);
	tmp = miibus->read(miibus, phy_addr, MII_ADVERTISE);

	/*Restart auto-negotiation*/
	phy_start_aneg(phy);

	/* TODO : This check is required. This should be
	 * moved to a board init section as its specific
	 * to a phy.*/
	if ((phy->phy_id == 0x0282F013) || (phy->phy_id == 0x0282F014)) {
		/* This enables TX_CLK-ing in case of 10/100MBps operation */
		val = miibus->read(miibus, phy_addr, PHY_CONFIG_REG);
		val |= BIT(5);
		miibus->write(miibus, phy_addr, PHY_CONFIG_REG, val);
		tmp = miibus->read(miibus, phy_addr, PHY_CONFIG_REG);
	}

	return;
}



static void offload_emac_adjust_link(struct net_device *ndev)

{
	struct offload_internal *inter = netdev_priv(ndev);
	struct phy_device *phydev = inter->phydev;
	struct device *emac_dev = &ndev->dev;
	u32 prev_link, prev_speed, prev_dup;
	int new_state = 0;

	prev_link = inter->link;
	prev_dup = inter->duplex;
	prev_speed = inter->speed;

	if (phydev->link) {
		/* check the mode of operation - full/half duplex */
		if (phydev->duplex != inter->duplex) {
			new_state = 1;
			inter->duplex = phydev->duplex;
		}
		if (phydev->speed != inter->speed) {
			new_state = 1;
			inter->speed = phydev->speed;
		}
		if (!inter->link) {
			new_state = 1;
			inter->link = 1;
			dev_info(emac_dev, "attached PHY driver [%s] "
				"(mii_bus:phy_addr=%s, id=%x, speed=%d)\n",
				inter->phydev->drv->name, dev_name(&inter->phydev->dev),
				inter->phydev->phy_id, inter->phydev->speed);
		}

	} else if (inter->link) {
		new_state = 1;
		inter->link = 0;
		inter->speed = 0;
		inter->duplex = ~0;
		dev_info(emac_dev, "Link is Down [%s]", inter->phydev->drv->name);
	}
	if (new_state){
		offload_update_phystatus(inter);
	}
}

static void offload_rx_queue(struct device *dev, struct offload_internal *inter, int size)
{
	struct ETH_OFFLOAD_desc *desc = NULL;

    if (ETH_OFFLOAD_Q_IS_DEQUEUED(inter->rxQ->write, ETH_OFFLOAD_Q_ARM))
        desc = inter->rxQ->write;

	if(desc!=NULL) {

		struct sk_buff *skb;

        skb = dev_alloc_skb(size);
		if (WARN_ON(!skb)) {
            printk(" [ETH_OFFLOAD]: skb alloc failed!!!\n");
			return;
		}
		skb->dev = inter->ndev;
		skb_reserve(skb, NET_IP_ALIGN);

	    desc->sg[0].dma_address = dma_map_single(dev, skb->data, skb->len, DMA_FROM_DEVICE);
	    desc->length = desc->sg[0].length = skb->len;
		desc->skb = skb;
	    desc->nfrags = 1;

        ETH_OFFLOAD_Q_SET_ENQUEUED(desc, ETH_OFFLOAD_Q_ARM);

        inter->rxQ->write = ETH_OFFLOAD_Q_move(inter->rxQ, inter->rxQ->write);
	}
}

static void offload_rx_queueing(struct device *dev, struct offload_internal *inter, int num, int size)
{
	int i = 0;

	while (i < num) {
        offload_rx_queue(dev, inter, size);
		i++;
	}
}



/*************************************************************************
 *  Callback handler for Rx & Tx
 *************************************************************************/

static unsigned int offload_rxHandler(void *hApp, struct ETH_OFFLOAD_desc *desc)
{
	struct sk_buff		*skb = (struct sk_buff *)desc->skb;
	struct net_device	*ndev = skb->dev;
	struct offload_internal	*inter = netdev_priv(ndev);
	struct device		*emac_dev = &ndev->dev;
	unsigned int len = desc->length;

        dma_unmap_single(emac_dev, desc->sg[0].dma_address, len, DMA_FROM_DEVICE);

	/* free and bail if we are shutting down */
	if (unlikely(!netif_running(ndev))) {
		dev_kfree_skb_any(skb);
		goto offload_rxHandler_exit;
	}

	/* let go the skb up the stack */
        skb_put(skb, len);
        skb->protocol = eth_type_trans(skb, ndev);
        skb->ip_summed = CHECKSUM_NONE;

	if (inter->rx_csum_enable && likely(desc->status==1)) {
		 skb->ip_summed = CHECKSUM_UNNECESSARY;
	}
        if (gro_enable) {
		napi_gro_receive(&inter->napi, skb);
        } else {
		netif_receive_skb(skb);
        }

        ndev->stats.rx_bytes += len;
        ndev->stats.rx_packets++;

	offload_rx_queue(emac_dev, inter, inter->rx_buf_size);

offload_rxHandler_exit:

	return 1;
}


static unsigned int offload_txHandler(void *hApp, struct ETH_OFFLOAD_desc *desc)
{
	struct sk_buff *skb = (struct sk_buff *)desc->skb;
	struct net_device *ndev = skb->dev;
	struct device *emac_dev = &ndev->dev;
	static struct sk_buff *prev = 0;
	static int prev_cnt = 0;
	int i ;

	ndev->stats.tx_packets++;
	ndev->stats.tx_bytes += desc->length;

	if(desc->nfrags != (skb_shinfo(skb)->nr_frags + 1)) {
		printk(KERN_NOTICE "Tx completiom handler, n_segs mismatch. nfrags=%d skb_frags=%d\n",
			desc->nfrags, skb_shinfo(skb)->nr_frags);
	}
        /*unmap direct mapped region*/
	dma_unmap_single(emac_dev, desc->sg[0].dma_address, desc->sg[0].length,
				DMA_TO_DEVICE);
	/*unmap fragments mapped before(if any) */
	for(i=1; i<desc->nfrags; i++) {
		dma_unmap_page(emac_dev, desc->sg[i].dma_address, desc->sg[i].length,
				DMA_TO_DEVICE);
	}

	if( (atomic_read(&skb->users)) > 0) {
		prev = skb;
		prev_cnt = atomic_read(&skb->users);
		dev_kfree_skb_any(skb);
	} else {
		printk(KERN_NOTICE "Freeing skb 0x%x with use count %d, prev=0x%x prev_cnt=%d\n",
			(__force u32) skb, atomic_read(&skb->users), (__force u32) prev, prev_cnt);
	}
	if (unlikely(netif_queue_stopped(ndev))){

		if(netif_running(ndev))
			netif_wake_queue(ndev);
	}

	return 1;
}

/*************************************************************************
 *  Driver ISR
 *************************************************************************/


static int offload_napi_poll (struct napi_struct *napi, int budget)
{
	struct offload_internal *inter = interglobal;
	int numpktsTx=0, numpktsRx=0;

	inter->offloadstats.arm_napicnt++;

    numpktsTx = ETH_OFFLOAD_Q_Poll(inter->txQ, budget);

    numpktsRx = ETH_OFFLOAD_Q_Poll(inter->rxQ, budget);

	if (numpktsRx < budget ){
		napi_complete(napi);

        hrtimer_start(&inter->poll_timer,
			ns_to_ktime(inter->coal_intvl_nsecs), HRTIMER_MODE_REL_PINNED);
	}

	return numpktsRx;
}


static enum hrtimer_restart emac_hrtimer_poll(struct hrtimer *timer)
{
	struct offload_internal *inter;
    int rxRdy, txRdy;

	inter = container_of(timer, struct offload_internal, poll_timer);

    inter->offloadstats.arm_nfyisrcnt++;

    rxRdy = ETH_OFFLOAD_Q_IS_ENQUEUED(inter->rxQ->read, ETH_OFFLOAD_Q_DSP);
    txRdy = ETH_OFFLOAD_Q_IS_ENQUEUED(inter->txQ->read, ETH_OFFLOAD_Q_DSP);

    if(rxRdy||txRdy)
    {
    	if(likely(netif_running(inter->ndev))) {
	    	napi_schedule(&inter->napi);
		    return HRTIMER_NORESTART;
	    }
        else
        {
            /* interface is not running, come back after 10msec*/
            hrtimer_forward_now(&inter->poll_timer, ns_to_ktime(EMAC_POLL_INTERVAL_MAX_NSECS));
            return HRTIMER_RESTART;
        }
    }

    /* interface is not running, come back after inter->coal_intvl_nsecs */
    hrtimer_forward_now(&inter->poll_timer, ns_to_ktime(inter->coal_intvl_nsecs));
    return HRTIMER_RESTART;
}


static unsigned long out_of_desc_count = 0;

/*************************************************************************
 *  Driver interface functions
 *************************************************************************/

/**
 * emac_dev_xmit: EMAC Transmit function
 * @skb: SKB pointer
 * @ndev: The EMAC network adapter
 *
 * Called by the system to transmit a packet  - we queue the packet in
 * EMAC hardware transmit queue
 *
 * Returns success(NETDEV_TX_OK) or error code (typically out of desc's)
 */
static int emac_dev_xmit(struct sk_buff *skb, struct net_device *ndev)
{
	struct device *emac_dev;
	int ret_code, num_sgs;
	struct offload_internal *inter;
	struct ETH_OFFLOAD_desc *desc = 0;
	unsigned long flags = 0;

	inter = netdev_priv(ndev);
	emac_dev = &ndev->dev;

	/* If no link, return */
	if (unlikely(!inter->link)) {
		if (netif_msg_tx_err(inter) && net_ratelimit())
			dev_err(emac_dev, "DSP Accelarated EMAC: No link to transmit");
		goto fail_tx;
	}

	if(unlikely(skb->len <= 0)) {
		printk(KERN_NOTICE "Skb has no data len=%d usage_count=%d destructor=0x%x\n", skb->len,
				atomic_read(&skb->users), (__force u32) skb->destructor);
		dev_kfree_skb_any(skb);
		return NETDEV_TX_OK;
	}

	/* Allow >mtu size skb only if UFO is enabled */
	if (skb->len > ETH_OFFLOAD_MTU && !(ndev->features & NETIF_F_UFO)) {
		if(printk_ratelimit()) {
			printk(KERN_NOTICE "Dropping: Packet size(%d)>MTU as UFO (features=%x) is not enabled\n",
				skb->len, (u32)ndev->features);
		}
			ndev->stats.tx_dropped++;
			dev_kfree_skb_any(skb);
			return NETDEV_TX_OK;
	}

	if (unlikely((skb->len < EMAC_DEF_MIN_ETHPKTSIZE))) {
		/*ensure we attempt padto on linear skb*/
	        if (unlikely(skb_is_nonlinear(skb))) {
			if (__skb_linearize(skb)) {
				dev_err(emac_dev, "Failed to linearize short skb, length=%d\n",skb->len);
				ndev->stats.tx_dropped++;
				dev_kfree_skb(skb);
				return NETDEV_TX_OK;
			} else if (printk_ratelimit()) {
				printk(KERN_NOTICE "Linearized short skb, length=%d\n",skb->len);
			}
		}

		ret_code = skb_padto(skb, EMAC_DEF_MIN_ETHPKTSIZE);
		if (unlikely(ret_code < 0)) {
			if (netif_msg_tx_err(inter) && net_ratelimit())
				dev_err(emac_dev, "DSP Accelarated EMAC: packet pad failed");
			ndev->stats.tx_dropped++;
			dev_kfree_skb(skb);
			return NETDEV_TX_OK;
		}
	}

	/*check if can accomodate all the segments, otherwise try to linearize */
	if((skb_is_nonlinear(skb)) && (skb_shinfo(skb)->nr_frags >= (ETH_OFFLOAD_MAX_FRAGS - 1))) {
		num_sgs = skb_shinfo(skb)->nr_frags;

		if (__skb_linearize(skb)) {
			dev_err(emac_dev, "Failed to linearize %d frags. Max allowed frags is %d\n",
					skb_shinfo(skb)->nr_frags, ETH_OFFLOAD_MAX_FRAGS);
			ndev->stats.tx_dropped++;
			dev_kfree_skb_any(skb);
			return NETDEV_TX_OK;
		} else if (printk_ratelimit()) {
			printk(KERN_NOTICE "Linearized skb with %d frags. Max allowed frags is %d\n",
					num_sgs, ETH_OFFLOAD_MAX_FRAGS);
		}
	}

	desc = (struct ETH_OFFLOAD_desc *)ETH_OFFLOAD_Q_Dequeue(inter->txQ);
	if (NULL == desc) {
		out_of_desc_count++ ;
		printk("Out of desc - drop packet. skb=0x%x, len=%d, count=%ld\n", (u32)skb, skb->len, out_of_desc_count);
		goto fail_tx;
	}

        local_irq_save(flags);

	/* Map the direct segment first */
	desc->skb = skb;
	desc->status = 0;
	if (unlikely((skb->len < EMAC_DEF_MIN_ETHPKTSIZE))) {
		/* if the skb is < EMAC_DEF_MIN_ETHPKTSIZE, we will have linearized and padded by now*/
		desc->sg[0].dma_address = dma_map_single(emac_dev, skb->data, EMAC_DEF_MIN_ETHPKTSIZE, DMA_TO_DEVICE);
		desc->sg[0].length = EMAC_DEF_MIN_ETHPKTSIZE;
		desc->length = EMAC_DEF_MIN_ETHPKTSIZE;
	} else {
		desc->sg[0].dma_address = dma_map_single(emac_dev, skb->data, skb_headlen(skb), DMA_TO_DEVICE);
		desc->sg[0].length = skb_headlen(skb);
		desc->length = skb->len;
	}

	if (skb_is_nonlinear(skb)) {
		int i  = 0;
		skb_frag_t *frag;

		num_sgs = skb_shinfo(skb)->nr_frags;

		for(i=0; i < num_sgs; i++) {
			frag = &skb_shinfo(skb)->frags[i];
			desc->sg[i+1].dma_address = dma_map_page(emac_dev,
				frag->page, frag->page_offset, frag->size, DMA_TO_DEVICE);
			desc->sg[i+1].length = frag->size;
		}

		desc->nfrags = num_sgs + 1;

	} else {
		desc->nfrags = 1;
	}

	ETH_OFFLOAD_Q_SET_ENQUEUED(desc, ETH_OFFLOAD_Q_ARM);

	local_irq_restore(flags);

	return NETDEV_TX_OK;

fail_tx:
	netif_stop_queue(ndev);
	return NETDEV_TX_BUSY;
}

/**
 * emac_dev_tx_timeout: EMAC Transmit timeout function
 * @ndev: The EMAC network adapter
 *
 * Called when system detects that a skb timeout period has expired
 * potentially due to a fault in the adapter in not being able to send
 * it out on the wire. We teardown the TX channel assuming a hardware
 * error and re-initialize the TX channel for hardware operation
 *
 */
static void emac_dev_tx_timeout(struct net_device *ndev)
{
	struct offload_internal *inter = netdev_priv(ndev);
	struct device *emac_dev = &ndev->dev;

	printk( KERN_INFO " [ETH OFFLOAD] TX timeout occured !!!\n");
	if (netif_msg_tx_err(inter))
		dev_err(emac_dev, "DSP Accelarated EMAC: xmit timeout, restarting TX");

	ndev->stats.tx_errors++;
	printk(KERN_DEBUG"Completed emac_dev_tx_timeout function\n\n");
}


/**
 * emac_dev_setmac_addr: Set mac address in the adapter
 * @ndev: The EMAC network adapter
 * @addr: MAC address to set in device
 *
 * Called by the system to set the mac address of the adapter (Device)
 *
 * Returns success (0) or appropriate error code (none as of now)
 */
static int emac_dev_setmac_addr(struct net_device *ndev, void *addr)
{
	struct offload_internal *inter = netdev_priv(ndev);
	struct device *emac_dev = &inter->ndev->dev;
	struct sockaddr *sa = addr;

	if (!is_valid_ether_addr(sa->sa_data))
		return -EINVAL;

	/* Store mac addr in inter and rx channel and set it in EMAC hw */
	memcpy(inter->mac_addr, sa->sa_data, ndev->addr_len);
	memcpy(ndev->dev_addr, sa->sa_data, ndev->addr_len);

	/* MAC address is configured only after the interface is enabled. */
	if (netif_running(ndev)) {
		memcpy(inter->mac_addr, sa->sa_data, ndev->addr_len);
	}

	if (netif_msg_drv(inter))
		dev_notice(emac_dev, "DSP Accelarated EMAC: emac_dev_setmac_addr %pM\n",
				inter->mac_addr);

	return 0;
}

/**
 * emac_devioctl: EMAC adapter ioctl
 * @ndev: The EMAC network adapter
 * @ifrq: request parameter
 * @cmd: command parameter
 *
 * EMAC driver ioctl function
 *
 * Returns success(0) or appropriate error code
 */
static int emac_devioctl(struct net_device *ndev, struct ifreq *ifrq, int cmd)
{
	dev_warn(&ndev->dev, "DSP Accelarated EMAC: ioctl not supported\n");

	if (!(netif_running(ndev)))
		return -EINVAL;

	return -EOPNOTSUPP;
}

/**
 * emac_dev_open: EMAC device open
 * @ndev: The EMAC network adapter
 *
 * Called when system wants to start the interface. We init TX/RX channels
 * and enable the hardware for packet reception/transmission and start the
 * network queue.
 *
 * Returns 0 for a successful open, or appropriate error code
 */
static int offload_emac_dev_open(struct net_device *ndev)
{
	struct device *emac_dev = &ndev->dev;
	struct offload_internal *inter = netdev_priv(ndev);
	struct ETH_OFFLOAD_open  devopen;
	struct ETH_OFFLOAD_setup devsetup;
	u32 cnt;
	int status;
	int max = ETH_OFFLOAD_SWDESC_MAX;

	netif_carrier_off(ndev);
	for (cnt = 0; cnt < ETH_ALEN; cnt++)
		ndev->dev_addr[cnt] = inter->mac_addr[cnt];

	memset(pETH_OFFLOAD_shm, 0 , sizeof(*pETH_OFFLOAD_shm));

	inter->speed = 0;
	inter->duplex  = 0;
	inter->link = 0;

	/* Configuration items */
	inter->rx_buf_size = EMAC_DEF_MAX_FRAME_SIZE + NET_IP_ALIGN;

	devsetup.offloadFlag = 0;
	if (gro_enable) {
		devsetup.offloadFlag |= ETH_OFFLOAD_FLAG_RX_CSUM;
		printk(" [ETH_OFFLOAD] RX CSUM is ENABLED !!!\n");
	}
	if (gso_enable) {
		devsetup.offloadFlag |= ETH_OFFLOAD_FLAG_TX_CSUM;
		printk(" [ETH_OFFLOAD] TX CSUM+SG is ENABLED !!!\n");
	} else if (ufo_enable) {
		devsetup.offloadFlag |= ETH_OFFLOAD_FLAG_TX_CSUM;
		printk(" [ETH_OFFLOAD] TX CSUM+SG+UFO is ENABLED !!!\n");
	}

	/* Emac setup */
	status = ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_EMAC_SETUP, (void *)&devsetup, sizeof(devsetup));
	if (status < 0){
		printk(KERN_ERR "OFFLOAD: Remote Emac setup failed\n");
		return -1; //TODO error codes
	}

	devopen.mtu = ETH_OFFLOAD_MTU;
	devopen.nmacaddrs = 1;

	if(inter->eth_if==0)
		devopen.phyid = 0;
	else
		devopen.phyid = 1;

	/* Enable Interrupt pacing if configured */
	devopen.coal.rx_coalesce_usecs = 0;

	memcpy(devopen.macaddrs, inter->mac_addr, 6);

	/* Emac open */
	status = ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_OPEN, (void *)&devopen, sizeof(devopen));
	if (status < 0){
		printk(KERN_ERR "OFFLOAD: Remote Emac open failed\n");
		return -1; //TODO error codes
	}

	/* Create ready rx skbs */
	offload_rx_queueing(emac_dev, inter, max, inter->rx_buf_size);

	/* Emac start */
	cnt = 2;
	do {
		/* Time given for all the rx desc to be sent to dsp */
		msleep(100);
	} while (ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_START, NULL, 0) < 0 && cnt--);

	if (cnt == 0) {
		printk(KERN_ERR "OFFLOAD: Remote Emac start failed\n");
		ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_CLOSE, NULL, 0);
		return -1;
	}

	/* Try starting the phy on mdio */

	if (!inter->phy_id) {
		struct device *phy;

		/* Find the corresponding mdio device */
		phy = bus_find_device(&mdio_bus_type, NULL, NULL,
				      match_first_device);
		if (phy)
			inter->phy_id = dev_name(phy);
	}
	if (inter->phy_id && *inter->phy_id) {
		/* Connect to the device */
		inter->phydev = phy_connect(ndev, inter->phy_id,
					   &offload_emac_adjust_link, 0,
					   PHY_INTERFACE_MODE_MII);

		if (IS_ERR(inter->phydev)) {
			dev_err(emac_dev, "could not connect to phy %s\n",
				inter->phy_id);
			inter->phydev = NULL;
			ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_CLOSE, NULL, 0);
			return PTR_ERR(inter->phydev);
		}
		emac_set_phy_config(inter, inter->phydev);

		inter->link = 0;
		inter->speed = 0;
		inter->duplex = ~0;
	} else {
		/* No PHY, fix the link, speed and duplex settings */
		dev_notice(emac_dev, "no phy, defaulting to 100/full\n");
		inter->link = 1;
		inter->speed = SPEED_100;
		inter->duplex = DUPLEX_FULL;
		offload_update_phystatus (inter);
	}

	/* Connected to the device, start it */
	if (inter->phydev)
		phy_start(inter->phydev);

	napi_enable (&inter->napi);

	ndev->flags |= IFF_UP;

	if (netif_msg_drv(inter))
		dev_notice(emac_dev, "DSP Accelarated EMAC: Opened %s\n", ndev->name);

	return 0;
}

/**
 * emac_dev_stop: EMAC device stop
 * @ndev: The EMAC network adapter
 *
 * Called when system wants to stop or down the interface. We stop the network
 * queue, disable interrupts and cleanup TX/RX channels.
 *
 * We return the statistics in net_device_stats structure pulled from emac
 */
static int emac_dev_stop(struct net_device *ndev)
{
	struct offload_internal *inter = netdev_priv(ndev);
	struct device *emac_dev = &ndev->dev;
	int num_pkts;

	/* inform the upper layers. */
	netif_stop_queue(ndev);
	napi_disable (&inter->napi);
	netif_carrier_off(ndev);

	if (inter->phydev)
		phy_disconnect(inter->phydev);

	ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_CLOSE, NULL, 0);

	/* the offload driver should have returned all queued descriptors, process them */
	do {
		num_pkts = ETH_OFFLOAD_Q_Poll(inter->txQ, ETH_OFFLOAD_SWDESC_MAX);
	} while (num_pkts > 0);

	do {
		num_pkts = ETH_OFFLOAD_Q_Poll(inter->rxQ, ETH_OFFLOAD_SWDESC_MAX);
	} while (num_pkts > 0);

	ETH_OFFLOAD_Q_Reset(inter->txQ);
	ETH_OFFLOAD_Q_Reset(inter->rxQ);

	if (netif_msg_drv(inter))
		dev_notice(emac_dev, "DSP Accelarated EMAC: %s stopped\n", ndev->name);

	return 0;
}

/**
 * emac_dev_getnetstats: EMAC get statistics function
 * @ndev: The EMAC network adapter
 *
 * Called when system wants to get statistics from the device.
 *
 * We return the statistics in net_device_stats structure pulled from emac
 */
static struct net_device_stats *emac_dev_getnetstats(struct net_device *ndev)
{
	struct ETH_OFFLOAD_stats stats;
	UInt32 status;

	memset(&stats, 0 , sizeof(stats));

	if(!ndev) {
		return NULL;
	}

	status = ETH_OFFLOAD_Rpc(ETH_OFFLOAD_EMAC_GET_STATISTICS, (void *)&stats, sizeof(stats));
	if (status != 1){
		return &ndev->stats;
	}

	ndev->stats.multicast += stats.multicast;

	ndev->stats.collisions += stats.collisions;

	ndev->stats.rx_length_errors += stats.rx_length_errors;

	ndev->stats.rx_over_errors += stats.rx_over_errors;
	ndev->stats.rx_fifo_errors += stats.rx_fifo_errors;
	ndev->stats.tx_carrier_errors += stats.tx_carrier_errors;
	ndev->stats.tx_fifo_errors = stats.tx_fifo_errors;
	return &ndev->stats;
}

static const struct net_device_ops emac_netdev_ops = {
	.ndo_open		= offload_emac_dev_open,

	.ndo_stop		= emac_dev_stop,
	.ndo_start_xmit		= emac_dev_xmit,
	.ndo_set_mac_address	= emac_dev_setmac_addr,
	.ndo_do_ioctl		= emac_devioctl,
	.ndo_tx_timeout		= emac_dev_tx_timeout,
	.ndo_get_stats		= emac_dev_getnetstats,
#ifdef CONFIG_NET_POLL_CONTROLLER
	.ndo_poll_controller	= emac_poll_controller,
#endif
};

static u64 offload_dma_mask = DMA_BIT_MASK(32);

/**
 * offload_dev_probe: EMAC device probe
 * @pdev: The EMAC device that we are removing
 *
 * Called when probing for emac devicesr. We get details of instances and
 * resource information from platform init and register a network device
 * and allocate resources necessary for driver to perform
 */
static int __devinit offload_dev_probe(struct platform_device *pdev)
{
	int rc = 0;
	struct net_device *ndev;
	struct offload_internal *inter;
	struct device *emac_dev;
	struct emac_platform_data *pdata;
	unsigned long emac_bus_frequency;
    struct resource *res;
    unsigned int emac_base_phys;

	emac_clk = clk_get(&pdev->dev, NULL);
	if (IS_ERR(emac_clk)) {
		printk (KERN_ERR "Clk Get Failed\n");
		return -EBUSY;
	}
	emac_bus_frequency = clk_get_rate(emac_clk);

	ndev = alloc_etherdev(sizeof(struct offload_internal));
	if (!ndev) {
		printk(KERN_ERR "DSP Accelarated EMAC: Error allocating net_device\n");
		clk_put(emac_clk);
		return -ENOMEM;
	}

	platform_set_drvdata(pdev, ndev);
	inter = netdev_priv(ndev);

	interglobal = inter;

    offload_reset_perf();

	inter->pdev = pdev;
	inter->ndev = ndev;
	inter->msg_enable = netif_msg_init(debug_level, OFFLOAD_DEBUG_FLAGS);
	inter->bus_freq_mhz = (u32)(emac_bus_frequency / 1000000);

	pdata = pdev->dev.platform_data;
	if (!pdata) {
		printk(KERN_ERR "DaVinci EMAC: No platform data\n");
		return -ENODEV;
	}

	spin_lock_init(&inter->txq_lock);

	if(shared_mem_phys_addr==0) {
		shared_mem_phys_addr = ETH_OFFLOAD_SHM_PHYS_ADDR; /* default address */
	}

	printk(" [ETH OFFLOAD] Shared memory address = 0x%08x, size =0x%08x !!!\n", shared_mem_phys_addr, sizeof(*pETH_OFFLOAD_shm));

	pETH_OFFLOAD_shm = ioremap_nocache(shared_mem_phys_addr, sizeof(*pETH_OFFLOAD_shm));
	if(pETH_OFFLOAD_shm==NULL) {
		printk(" [ETH OFFLOAD] ioremap(0x%08x) FAILED !!!\n", shared_mem_phys_addr);
	}

	inter->phy_id = pdata->phy_id;
	inter->rmii_en = pdata->rmii_en;

	/* MAC addr and PHY mask , RMII enable info from platform_data */
    /* set mac-address from platform */
    memcpy(inter->mac_addr, pdata->mac_addr, 6);

	inter->version = EMAC2;
	emac_dev = &ndev->dev;

	if (!is_valid_ether_addr(inter->mac_addr)) {
		/* Use random MAC if none passed */
		random_ether_addr(inter->mac_addr);
		printk(KERN_WARNING "%s: using random MAC addr: %pM\n",
				__func__, inter->mac_addr);
	}

	ndev->netdev_ops = &emac_netdev_ops;
	SET_ETHTOOL_OPS(ndev, &ethtool_ops);

	netif_napi_add (ndev, &inter->napi,
			offload_napi_poll, ETH_OFFLOAD_Q_WEIGHT);

	clk_enable(emac_clk);

	pdev->dev.dma_mask = &offload_dma_mask;
	pdev->dev.coherent_dma_mask = DMA_BIT_MASK(32);

	/* register the network device */
	SET_NETDEV_DEV(ndev, &pdev->dev);

    if (gro_enable) {
        ndev->features |= (NETIF_F_GRO);
        printk(" [ETH OFFLOAD] GRO is ENABLED !!!\n");
    }
    if (gso_enable) {
        ndev->features |= (NETIF_F_GSO | NETIF_F_HW_CSUM | NETIF_F_SG);
        printk(" [ETH OFFLOAD] GSO is ENABLED !!!\n");
    } else if (ufo_enable) {
	ndev->features |= (NETIF_F_UFO | NETIF_F_HW_CSUM | NETIF_F_SG);
		printk(" [ETH OFFLOAD] UFO is ENABLED !!!\n");
    }

	/* Get EMAC platform data */
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(emac_dev, " [ETH_OFFLOAD] Error getting res\n");
		rc = -ENOENT;
		return rc;
	}

	emac_base_phys = res->start + pdata->ctrl_reg_offset;

    if(emac_base_phys==ETH_OFFLOAD_EMAC_0_REGS)
        inter->eth_if = 0;
    else
        inter->eth_if = 1;

    printk(" [ETH OFFLOAD] EMAC-%d is ENABLED !!!\n", inter->eth_if);

	rc = register_netdev(ndev);
	if (rc) {
		dev_err(emac_dev, "DSP Accelarated EMAC: Error in register_netdev\n");
		return -ENODEV;
	}

	if (netif_msg_probe(inter)) {
		dev_notice(emac_dev, "DSP Accelarated EMAC Probe found device "\
				"(regs: %p, irq: %d)\n",
				(void *)inter->emac_base_phys, ndev->irq);
	}

	/* Create Qs */
	inter->txQ = ETH_OFFLOAD_Q_Create(ETH_OFFLOAD_SWDESC_MAX,
			(QCALLBACK)offload_txHandler,
			(void *)inter, "TX",
            &pETH_OFFLOAD_shm->txSwDesc[0]
            );

	inter->rxQ = ETH_OFFLOAD_Q_Create(ETH_OFFLOAD_SWDESC_MAX,
			(QCALLBACK)offload_rxHandler,
			(void *)inter, "RX",
            &pETH_OFFLOAD_shm->rxSwDesc[0]
            );

    inter->coal_intvl_usecs = EMAC_POLL_INTERVAL_DEFAULT_USECS;
    inter->coal_intvl_nsecs = inter->coal_intvl_usecs*1000;

	hrtimer_init(&inter->poll_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	inter->poll_timer.function = emac_hrtimer_poll;

	hrtimer_start(
            &inter->poll_timer,
            ktime_set(0, EMAC_POLL_INTERVAL_INIT_NSECS ),
			HRTIMER_MODE_REL_PINNED);

	return 0;
}

/**
 * offload_dev_remove: EMAC device remove
 * @pdev: The EMAC device that we are removing
 *
 * Called when removing the device driver. We disable clock usage and release
 * the resources taken up by the driver and unregister network device
 */
static int __devexit offload_dev_remove(struct platform_device *pdev)
{
	UInt32 status;
	struct net_device *ndev = platform_get_drvdata(pdev);
	struct offload_internal *inter;

	inter = netdev_priv(ndev);

    hrtimer_cancel(&inter->poll_timer);

	status = ETH_OFFLOAD_Q_Destroy(inter->txQ);
	if( status != 1  ){
		printk("offload_dev_remove: Tx ETH_OFFLOAD_Q_Destroy failed !!!\n");
		return -1;
	}
	status = ETH_OFFLOAD_Q_Destroy(inter->rxQ);
	if( status != 1){
		printk("offload_dev_remove: Rx ETH_OFFLOAD_Q_Destroy failed !!!\n");
		return -1;
	}

	dev_notice(&ndev->dev, " [ETH OFFLOAD] Driver is removed !!!\n");

	platform_set_drvdata(pdev, NULL);
	unregister_netdev(ndev);

	iounmap(pETH_OFFLOAD_shm);
	free_netdev(ndev);

	clk_disable(emac_clk);
	clk_put(emac_clk);

	return 0;
}

static int offload_dev_suspend(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct net_device *ndev = platform_get_drvdata(pdev);

	if (netif_running(ndev))
		emac_dev_stop(ndev);
	return 0;
}

static int offload_dev_resume(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct net_device *ndev = platform_get_drvdata(pdev);

	if (netif_running(ndev))
		offload_emac_dev_open(ndev);
	return 0;
}

static const struct dev_pm_ops offload_dev_pm_ops = {
	.suspend	= offload_dev_suspend,
	.resume		= offload_dev_resume,
};

/**
 * offload_dev_driver: EMAC platform driver structure
 */
static struct platform_driver offload_dev_driver = {
	.driver = {
		.name	 = "eth_offload",
		.owner	 = THIS_MODULE,
		.pm	 = &offload_dev_pm_ops,
	},
	.probe  = offload_dev_probe,
	.remove = __devexit_p(offload_dev_remove),
};

/**
 * offload_dev_init: EMAC driver module init
 *
 * Called when initializing the driver. We register the driver with
 * the platform.
 */
static int __init offload_dev_init(void)
{
	int ret = 0;

	/* Q init */
	ret = ETH_OFFLOAD_Rpc_Init();
	if (ret < 0){
		printk (KERN_ERR "OFFLOAD: RPC init Failed\n");
		return ret;
	}

	ret = platform_driver_register(&offload_dev_driver);
	if (ret) {
		return ret;
	}

	return 0;
}
module_init(offload_dev_init);

/**
 * offload_dev_exit: EMAC driver module exit
 *
 * Called when exiting the driver completely. We unregister the driver with
 * the platform and exit
 */
static void __exit offload_dev_exit(void)
{
	platform_driver_unregister(&offload_dev_driver);

	ETH_OFFLOAD_Rpc_DeInit();
}
module_exit(offload_dev_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("DSP Accelarated EMAC Maintainer: TI/Sylvan Labs");
MODULE_DESCRIPTION("DSP Accelarated EMAC Ethernet driver");
