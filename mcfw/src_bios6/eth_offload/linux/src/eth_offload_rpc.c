#include <linux/spinlock.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/wait.h>
#include <asm/io.h>

#include <ti/syslink/Std.h>
#include <ti/syslink/utils/Memory.h>

#include <eth_offload_rpc.h>

#define IHeap_Handle            Ptr

/*
 * ETH_OFFLOAD_RpcObj: To manage rpc functions
 * @rpcAckLock: Semaphore lock for ackment
 * @rpcHandler: rpc callback handler to handle rpc requests
 */
struct ETH_OFFLOAD_RpcObj {
	volatile UInt32			   rpcAckReceived;
	wait_queue_head_t 	       rpcAckLock;
	struct ETH_OFFLOAD_RpcMsg *pRpcMsg;
};

/*
 * Static variables
 */
static struct ETH_OFFLOAD_RpcObj RPC;


/********************************************************************************************/
/*                                    Private Functions                                     */
/********************************************************************************************/

/*
 * ETH_OFFLOAD_RPC_HeapAlloc: Function to allocate heap memory
 * @size: size to allocate, type unsigned integer
 * @return: starting addres of type void
 */
static void* ETH_OFFLOAD_RPC_HeapAlloc(UInt32 size)
{
        IHeap_Handle heapHndl;
        Error_Block eb;
        void *virt;
        void *shAddr;

        heapHndl = SharedRegion_getHeap(ETH_OFFLOAD_RPC_REGIONID);
        if (heapHndl == NULL){
                printk ("OFFLOAD: SharedRegion_getHeap Failed\n");
                return NULL;
        }

        Error_init(&eb);
        virt = Memory_alloc((IHeap_Handle) heapHndl, size, 0, &eb);
        if (virt == NULL){
                printk ("OFFLOAD: Memory_alloc Failed\n");
                return NULL;
        }
        shAddr = virt;

        return shAddr;
}

/*
 * ETH_OFFLOAD_RPC_HeapFree: Function to Deallocate the heap memory
 * @ptr: starting address of the heap memory
 * @size: size to deallocate
 * @return: None
 */
static Void ETH_OFFLOAD_RPC_HeapFree(Void *ptr, UInt32 size)
{
        IHeap_Handle heapHndl;

        heapHndl = SharedRegion_getHeap(ETH_OFFLOAD_RPC_REGIONID);
        if (heapHndl == NULL){
                printk ("OFFLOAD: SharedRegion_getHeap Failed\n");
                return;
        }

        Memory_free(heapHndl, (Ptr)ptr, size);
}

/*
 * ETH_OFFLOAD_RPC_GetSharedPtr: Function to Get shared ptr
 * @ptr: pointer to be converted
 * @return: address of type uint32
 */
static UInt32 ETH_OFFLOAD_RPC_GetSharedPtr (UInt32 ptr)
{
	return (UInt32)SharedRegion_getSRPtr((int *)ptr, ETH_OFFLOAD_RPC_REGIONID);
}

/*
 * ETH_OFFLOAD_RPC_NotifyHandler: Callback to receive notification from remote processor
 * @procId: Processor id of the remote (ignored)
 * @lineId: interrupt line id (ignored)
 * @eventId: event number on which the event was raised (=10)
 * @arg: any arg (ignored)
 * @payload: additonal data (ignored)
 * @return: None
 */
static Void ETH_OFFLOAD_RPC_NotifyHandler(UInt16 procId, UInt16 lineId,
					UInt32 eventId, UArg arg, UInt32 payload)
{
	struct ETH_OFFLOAD_RpcObj *rpc = &RPC;

    rpc->rpcAckReceived = 1;
    /* Wait for ack from remote */
	wake_up_interruptible(&rpc->rpcAckLock);

	return;
}

/********************************************************************************************/
/*                                    Public Functions                                      */
/********************************************************************************************/


/*
 * ETH_OFFLOAD_Rpc: Function for RPC
 * @rpcFuncId: Remote procedure to be called
 * @arg: can be any string
 * @size:Rpcargument size
 * @return: 1 if success, else 0
 */
Int32 ETH_OFFLOAD_Rpc(UInt32 funcId, UInt8 *arg, UInt32 size)
{
	struct ETH_OFFLOAD_RpcObj *rpc = &RPC;
	UInt32 status = 0;

    if(size > sizeof(rpc->pRpcMsg->rpcArg))
    {
        printk(" [ETH_OFFLOAD] RPC message size (%dB) exceeds max limit, aborting message send !!!\n", size);
        return -1;
    }

	/* Copy the contents */
	rpc->pRpcMsg->rpcArgSize = 0;
	if (arg) {
		rpc->pRpcMsg->rpcArgSize = size;
		memcpy((void*)rpc->pRpcMsg->rpcArg, arg, size);
	}

	rpc->pRpcMsg->rpcFuncId  = funcId;

    mb();

    funcId = rpc->pRpcMsg->rpcFuncId; /* read back to confirm value is written to memory */

	/* Send notification to remote processor */
	Notify_sendEvent(ETH_OFFLOAD_PROCID_REMOTE,
			ETH_OFFLOAD_RPC_NOTIFY_LINEID,
			ETH_OFFLOAD_RPC_NOTIFY_EVENTID,
			ETH_OFFLOAD_RPC_GetSharedPtr((UInt32)rpc->pRpcMsg),
			TRUE);

    rpc->rpcAckReceived = 0;

	/* Wait for ack from remote */
	wait_event_interruptible(rpc->rpcAckLock, rpc->rpcAckReceived == 1);

	/* Collect the status */
	status = rpc->pRpcMsg->rpcStatus;

	/* Give Back the arg to Caller */
	if (arg && size > 0) {
		memcpy(arg, (void*)rpc->pRpcMsg->rpcArg, size);
	}

	return status;
}

/*
 * ETH_OFFLOAD_Rpc_Init: Function to Initiate Rpc
 * @return: +ve inetger
 */
Int32 ETH_OFFLOAD_Rpc_Init()
{
	struct ETH_OFFLOAD_RpcObj *rpc = &RPC;
    int status;

	memset(rpc, 0, sizeof(struct ETH_OFFLOAD_RpcObj));

	/* create semaphore to manage critical sections of timer data */
	init_waitqueue_head(&rpc->rpcAckLock);

	rpc->pRpcMsg = (struct ETH_OFFLOAD_RpcMsg *) ETH_OFFLOAD_RPC_HeapAlloc(sizeof(struct ETH_OFFLOAD_RpcMsg));
	if (rpc->pRpcMsg == NULL) {
		printk(" [ETH_OFFLOAD] Memory alloc failed for RPC message !!!\n");
		return 0;
	}

	/* Register callback */
	status = Notify_registerEvent(ETH_OFFLOAD_PROCID_REMOTE,
			ETH_OFFLOAD_RPC_NOTIFY_LINEID,
			ETH_OFFLOAD_RPC_NOTIFY_EVENTID,
			ETH_OFFLOAD_RPC_NotifyHandler, NULL);

	if(status!=0) {
        printk(" [ETH_OFFLOAD] Notify register for RPC message failed !!!\n");
    }

	return 0;
}

/*
 * ETH_OFFLOAD_Rpc_DeInit: Function to DeInit Rpc
 * @return: 1 if success
 */
Int32 ETH_OFFLOAD_Rpc_DeInit()
{
	struct ETH_OFFLOAD_RpcObj *rpc = &RPC;
    int status;

	status = Notify_unregisterEvent(ETH_OFFLOAD_PROCID_REMOTE,
			ETH_OFFLOAD_RPC_NOTIFY_LINEID,
			ETH_OFFLOAD_RPC_NOTIFY_EVENTID,
			ETH_OFFLOAD_RPC_NotifyHandler, NULL);

	if(status!=0) {
        printk(" [ETH_OFFLOAD] Notify unregister for RPC message failed !!!\n");
    }

	/* Free the shared memory */
	ETH_OFFLOAD_RPC_HeapFree(rpc->pRpcMsg, sizeof(struct ETH_OFFLOAD_RpcMsg));

	return 0;
}
