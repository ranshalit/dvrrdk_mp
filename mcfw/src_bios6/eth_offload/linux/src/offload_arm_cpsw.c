/*
 * DSP Accelarated Ethernet Medium Access Controller
 *
 * Copyright (C) 2009 Texas Instruments.
 *
 * ---------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ---------------------------------------------------------------------------
 * History:
 * 0   Viswanath Dibbur - Initial version.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/string.h>
#include <linux/timer.h>
#include <linux/time.h>
#include <linux/syscalls.h>
#include <linux/errno.h>
#include <linux/in.h>
#include <linux/ioport.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/scatterlist.h>
#include <linux/skbuff.h>
#include <linux/ethtool.h>
#include <linux/highmem.h>
#include <linux/proc_fs.h>
#include <linux/ctype.h>
#include <linux/version.h>
#include <linux/spinlock.h>
#include <linux/dma-mapping.h>
#include <linux/clk.h>
#include <linux/platform_device.h>
#include <linux/semaphore.h>
#include <linux/phy.h>
#include <linux/bitops.h>
#include <linux/io.h>
#include <linux/uaccess.h>
#include <linux/delay.h>
#include <linux/davinci_emac.h>
#include <asm/irq.h>
#include <asm/page.h>
#include <linux/hrtimer.h>
#include <linux/cpsw.h>


#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/kthread.h>

#include <ti/syslink/Std.h> /* Need to remove this from this file */
#include <eth_offload_rpc.h>
#include <eth_offload_q.h>
#include <eth_offload_cpsw.h>


static struct clk *cpsw_clk;

static int gro_enable;

module_param(gro_enable, int, 0);
MODULE_PARM_DESC(gro_enable, "GRO ON/OFF, 1: ON, 0: OFF");

static int gso_enable;

module_param(gso_enable, int, 0);
MODULE_PARM_DESC(gso_enable, "GSO ON/OFF, 1: ON, 0: OFF");


static int debug_level;

module_param(debug_level, int, 1);
MODULE_PARM_DESC(debug_level, "DSP Accelarated EMAC debug level (NETIF_MSG bits)");

static unsigned int shared_mem_phys_addr=0;

module_param(shared_mem_phys_addr, int, 1);
MODULE_PARM_DESC(shared_mem_phys_addr, "Shared memory communication physical address");



/* in micro-sec's */
#define EMAC_POLL_INTERVAL_INIT_USECS    (10000u)   /* 10.0 ms */
#define EMAC_POLL_INTERVAL_MAX_USECS	 (1000u)    /*  1.0 ms */
#define EMAC_POLL_INTERVAL_MIN_USECS	 (100u)     /*  0.1 ms */
#define EMAC_POLL_INTERVAL_DEFAULT_USECS (500u)


#define EMAC_POLL_INTERVAL_INIT_NSECS   (EMAC_POLL_INTERVAL_INIT_USECS*1000u)
#define EMAC_POLL_INTERVAL_MAX_NSECS	(EMAC_POLL_INTERVAL_MAX_USECS*1000u)

#define PHY_CONFIG_REG		22
#define EMAC_MACCONTROL		0x160

#define EMAC_MACCONTROL_FULLDUPLEXEN	BIT(0)
#define EMAC_MACCONTROL_RMIISPEED_MASK	BIT(15)

#define EMAC2				(0x02)
#define EMAC_DEF_MIN_ETHPKTSIZE		(60) /* Minimum ethernet pkt size */
#define EMAC_DEF_MAX_FRAME_SIZE		(1500 + 14 + 4 + 4)

/* Netif debug messages possible */
#define OFFLOAD_DEBUG_FLAGS	    ( \
		NETIF_MSG_DRV       | \
		NETIF_MSG_PROBE     | \
		NETIF_MSG_LINK      | \
		NETIF_MSG_TIMER     | \
		NETIF_MSG_IFDOWN    | \
		NETIF_MSG_IFUP      | \
		NETIF_MSG_RX_ERR    | \
		NETIF_MSG_TX_ERR    | \
		NETIF_MSG_TX_QUEUED | \
		NETIF_MSG_INTR      | \
		NETIF_MSG_TX_DONE   | \
		NETIF_MSG_RX_STATUS | \
		NETIF_MSG_PKTDATA   | \
		NETIF_MSG_HW        | \
		NETIF_MSG_WOL       )


/* Netif supported offfload flags for GRO and GSO feature offload */
/*
	NETIF_F_GRO
*/
#define OFFLOAD_GRO_FLAGS	   ( NETIF_F_GRO )
#define OFFLOAD_GSO_FLAGS	   ( NETIF_F_GSO | NETIF_F_HW_CSUM)


#define ETH_OFFLOAD_MTU 	(1514)
#define ETH_OFFLOAD_LRO_SIZE	(64*1024)

/* version info */
#define OFFLOAD_MAJOR_VERSION	0
#define OFFLOAD_MINOR_VERSION	1
#define OFFLOAD_MODULE_VERSION	"0.1"
MODULE_VERSION(OFFLOAD_MODULE_VERSION);
static const char offload_version_string[] = "TI DSP Accelarated EMAC Linux v0.1";


#ifdef CONFIG_TI_CPSW_DUAL_EMAC
#define cpsw_slave_phy_index(priv)	((priv)->emac_port)
#else
#define cpsw_slave_phy_index(priv)	0
#endif




/*************************************************************************
 *  Structures
 *************************************************************************/

struct cpsw_slave {
	int			             	slave_num;
	struct cpsw_slave_data		*data;
	struct phy_device		    *phy;
};

/* offload_internal: EMAC internal data structure
 *
 * EMAC adapter internal data structure
 */
struct offload_internal {
	u32 msg_enable;
	struct net_device *ndev;
	struct platform_device *pdev;
	struct ETH_OFFLOAD_queue *txQ;
	struct ETH_OFFLOAD_queue *rxQ;
	struct ETH_OFFLOAD_offloadStats offloadstats;
	struct napi_struct napi;
	struct cpsw_platform_data	data;
	struct cpsw_slave *slaves;
	struct clk	*clk;
	char mac_addr[6];
	u32 cpsw_base_phys;
	u32 rx_buf_size;
	u32 bus_freq_mhz;
	u32 coal_intvl_usecs;
    unsigned long coal_intvl_nsecs;
	u8 rmii_en;
	u8 version;
	int link[2];
	int duplex[2];
	int speed[2];
	u32 rx_addr_type;
	u16 nfy_procId;
	u16 nfy_lineId;
	u32 nfy_eventId;
	spinlock_t txq_lock;
	int				host_port;
#define for_each_slave(priv, func, arg...)			\
	do {							\
		int idx;					\
		for (idx = 0; idx < (priv)->data.slaves; idx++)	\
			(func)((priv)->slaves + idx, ##arg);	\
	} while (0)
#define slave(priv, idx)		((priv)->slaves + idx)


    unsigned long long statsStartTime;

    struct hrtimer poll_timer;
    int eth_if;
};

extern void ETH_OFFLOAD_Q_Dump (struct ETH_OFFLOAD_queue *Q); //TEMP to be removed

struct offload_internal *interglobal;

ETH_OFFLOAD_shm *pETH_OFFLOAD_shm=NULL;

/*************************************************************************
 *  Error Codes for both Rx and Tx
 *************************************************************************/

/* EMAC TX Host Error description strings */
char *cpsw_txhost_errcodes[16] = {
	"No error", "SOP error", "Ownership bit not set in SOP buffer",
	"Zero Next Buffer Descriptor Pointer Without EOP",
	"Zero Buffer Pointer", "Zero Buffer Length", "Packet Length Error",
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",
	"Reserved", "Reserved", "Reserved", "Reserved"
};

/* EMAC RX Host Error description strings */
char *cpsw_rxhost_errcodes[16] = {
	"No error", "Reserved", "Ownership bit not set in input buffer",
	"Wrong Checksum", "Zero Buffer Pointer", "Reserved", "Reserved",
	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved",
	"Reserved", "Reserved", "Reserved", "Reserved"
};


/*************************************************************************
 *  ethtool functions
 *************************************************************************/

unsigned long long offload_get_cur_time_in_usec(void)
{
	struct timespec tm;
	unsigned long usec = 1000000L;

	getnstimeofday (&tm);

    return (unsigned long long)(tm.tv_sec*usec + tm.tv_nsec/1000);
}

/**
 * offload_get_drvinfo: Get EMAC driver information
 * @ndev: The EMAC network adapter
 * @info: ethtool info structure containing name and version
 *
 * Returns EMAC driver information (name and version)
 *
 */
static void offload_get_drvinfo(struct net_device *ndev,
		struct ethtool_drvinfo *info)
{
	printk(KERN_DEBUG "Entered offload_get_drvinfo function\n\n");
	strcpy(info->driver, offload_version_string);
	strcpy(info->version, OFFLOAD_MODULE_VERSION);
	printk(KERN_DEBUG "completed offload_get_drvinfo function\n");
}

/**
 * cpsw_get_settings: Get EMAC settings
 * @ndev: CPSW network adapter
 * @ecmd: ethtool command
 *
 * Executes ethool get command
 *
 */
static int offload_get_settings(struct net_device *ndev,
			     struct ethtool_cmd *ecmd)
{
	struct offload_internal *inter = netdev_priv(ndev);
	int slave_no = cpsw_slave_phy_index(inter);

	if (inter->slaves[slave_no].phy)
		return phy_ethtool_gset(inter->slaves[slave_no].phy, ecmd);
	else
		return -EOPNOTSUPP;

}

/**
 * cpsw_set_settings: Set EMAC settings
 * @ndev: CPSW network adapter
 * @ecmd: ethtool command
 *
 * Executes ethool set command
 *
 */
static int offload_set_settings(struct net_device *ndev, struct ethtool_cmd *ecmd)
{
	struct offload_internal *inter = netdev_priv(ndev);
	int slave_no = cpsw_slave_phy_index(inter);

	if (inter->slaves[slave_no].phy)
		return phy_ethtool_sset(inter->slaves[slave_no].phy, ecmd);
	else
		return -EOPNOTSUPP;

}

static int cpsw_set_coalesce(struct net_device *ndev,
				struct ethtool_coalesce *coal){
	struct offload_internal *inter = netdev_priv(ndev);

	inter->coal_intvl_usecs = coal->rx_coalesce_usecs;
    if(inter->coal_intvl_usecs < EMAC_POLL_INTERVAL_MIN_USECS)
        inter->coal_intvl_usecs = EMAC_POLL_INTERVAL_MIN_USECS;

    if(inter->coal_intvl_usecs > EMAC_POLL_INTERVAL_MAX_USECS)
        inter->coal_intvl_usecs = EMAC_POLL_INTERVAL_MAX_USECS;

    inter->coal_intvl_nsecs = inter->coal_intvl_usecs*1000;

	// TBD: send command to M3 to update coalesing

	printk(KERN_INFO " [ETH_OFFLOAD] set coal inrvl of %d usecs\n", inter->coal_intvl_usecs);

	return 0;
}

static int cpsw_get_coalesce(struct net_device *ndev,
		struct ethtool_coalesce *coal)
{
	struct offload_internal *inter = netdev_priv(ndev);

	coal->rx_coalesce_usecs = inter->coal_intvl_usecs;
	return 0;

}

static void offload_print_offstats(struct ETH_OFFLOAD_offloadStats *pstats, unsigned int elaspedTimeInMsec)
{
    printk (" EVENT INFORMATION\n");
    printk (" =================\n");
    printk (" \n");
    printk (" ARM NOTIFY ISR  : %u (%d per sec)\n", pstats->arm_nfyisrcnt, (pstats->arm_nfyisrcnt*10)/(elaspedTimeInMsec/100));
    printk (" ARM NAPI        : %u (%d per sec)\n", pstats->arm_napicnt  , (pstats->arm_napicnt*10)/(elaspedTimeInMsec/100));
    printk (" RX  PACKET DROP : %u (%d per sec)\n", pstats->arm_rx_packet_drop  , (pstats->arm_rx_packet_drop*10)/(elaspedTimeInMsec/100));
    printk ("\n");
}


static void offload_reset_perf(void)
{
    struct offload_internal *inter = interglobal;

    inter->offloadstats.arm_napicnt = 0;
    inter->offloadstats.arm_nfyisrcnt = 0;
    inter->offloadstats.arm_rx_packet_drop = 0;
    inter->statsStartTime = offload_get_cur_time_in_usec();
}

u32 offload_printall(struct net_device *netdev)
{
    struct offload_internal *inter = interglobal;
    unsigned int elaspedTimeInMsec;

    elaspedTimeInMsec = offload_get_cur_time_in_usec() - inter->statsStartTime;
    elaspedTimeInMsec /= 1000;

    printk (" \n");
    printk (" DETAILED OFFLOAD STATISTICS\n");
    printk (" ***************************\n");
    printk (" \n");
	printk (" Total Elasped Time : %d msec \n", elaspedTimeInMsec);
    printk (" \n");

	offload_print_offstats(&inter->offloadstats, elaspedTimeInMsec);

    offload_reset_perf();

    #if 0
    printk (" SHARED MEMORY QUEUE INFORMATION\n");
    printk (" ================================\n");
    printk (" \n");
    ETH_OFFLOAD_Q_Dump (inter->txQ);
    printk (" \n");
    ETH_OFFLOAD_Q_Dump (inter->rxQ);
    printk (" \n");
    #endif

	return 0;
}

/**
 * ethtool_ops: EMAC Ethtool structure
 *
 * Ethtool support for EMAC adapter
 *
 */
static const struct ethtool_ops ethtool_ops = {
	.get_link     = ethtool_op_get_link,
	.get_drvinfo  = offload_get_drvinfo,
	.get_settings = offload_get_settings,
	.set_settings = offload_set_settings,
/*	.get_tx_csum  = offload_printall,  //temp hack, move it to ioctl */
	.set_coalesce =  cpsw_set_coalesce,
	.get_coalesce = cpsw_get_coalesce,
};

/*************************************************************************
 *  Support functions
 *************************************************************************/

static void _cpsw_adjust_link(struct cpsw_slave *slave,
			      struct offload_internal *inter, bool *link)
{
	struct phy_device	*phy = slave->phy;
	struct ETH_OFFLOAD_update update;
	struct device		*cpsw_dev = &inter->ndev->dev;

	int new_state = 0;

	if (!phy)
		return;

	if (phy->link) {
		/* check the mode of operation - full/half duplex */
		if (phy->duplex != inter->duplex[slave->slave_num]) {
			new_state = 1;
			inter->duplex[slave->slave_num] = phy->duplex;
		}
		if (phy->speed != inter->speed[slave->slave_num]) {
			new_state = 1;
			inter->speed[slave->slave_num] = phy->speed;
		}
		if (!inter->link[slave->slave_num]) {
			new_state = 1;
			inter->link[slave->slave_num] = 1;
			dev_info(cpsw_dev, "attached PHY driver [%s] "
				"(mii_bus:phy_addr=%s, id=%x, speed=%d)\n",
				phy->drv->name, dev_name(&phy->dev),
				phy->phy_id, phy->speed);
		}
		*link = true;
	} else if (inter->link[slave->slave_num]) {
		new_state = 1;
		inter->link[slave->slave_num]   = 0;
		inter->speed[slave->slave_num]  = 0;
		inter->duplex[slave->slave_num] = ~0;
		dev_info(cpsw_dev, "Link is Down [%s]", phy->drv->name);
	}

	if (new_state){

		memset(&update, 0, sizeof(update));
		if (phy){
			update.duplex = phy->duplex;
		}
		else{
			update.duplex = DUPLEX_FULL;
		}
		phy->duplex = update.duplex;

		if (phy->link) {
			update.linkup = 1;
		}
		update.speed   = phy->speed;
		update.version = inter->version;
		update.rmii    = inter->rmii_en;
		update.phy_id  = phy->phy_id;
		if (ETH_OFFLOAD_Rpc(ETH_OFFLOAD_FUNCID_UPDATE,(void *)&update, sizeof(update)) < 0) {
			printk(KERN_ERR "ETH_OFFLOAD: Phy status update Rpc failed\n");
		}

	}
}

static void cpsw_adjust_link(struct net_device *ndev)
{
	struct offload_internal	*inter = netdev_priv(ndev);
	bool		         link = false;

	for_each_slave(inter, _cpsw_adjust_link, inter, &link);

	if (link) {
		netif_carrier_on(ndev);
		if (netif_running(ndev))
		{
			netif_wake_queue(ndev);
		}
	} else {
		netif_carrier_off(ndev);
		netif_stop_queue(ndev);
	}
}

static void cpsw_set_phy_config(struct offload_internal *inter, struct phy_device *phy)
{
	struct cpsw_platform_data *pdata = inter->pdev->dev.platform_data;
	struct mii_bus *miibus;
	int phy_addr = 0;
	u16 val = 0;
	u16 tmp = 0;

	if (!pdata->gigabit_en)
		return;

	if (!phy)
		return;

	miibus = phy->bus;

	if (!miibus)
		return;

	phy_addr = phy->addr;

	/* Following lines enable gigbit advertisement capability even in case
	 * the advertisement is not enabled by default
	 */
	val = miibus->read(miibus, phy_addr, MII_BMCR);
	val |= (BMCR_SPEED100 | BMCR_ANENABLE | BMCR_FULLDPLX);
	miibus->write(miibus, phy_addr, MII_BMCR, val);
	tmp = miibus->read(miibus, phy_addr, MII_BMCR);

	tmp = miibus->read(miibus, phy_addr, MII_BMSR);
	if (tmp & 0x1) {
		val = miibus->read(miibus, phy_addr, MII_CTRL1000);
		val |= BIT(9);
		miibus->write(miibus, phy_addr, MII_CTRL1000, val);
		tmp = miibus->read(miibus, phy_addr, MII_CTRL1000);
	}

	val = miibus->read(miibus, phy_addr, MII_ADVERTISE);
	val |= (ADVERTISE_10HALF | ADVERTISE_10FULL | \
		ADVERTISE_100HALF | ADVERTISE_100FULL);
	miibus->write(miibus, phy_addr, MII_ADVERTISE, val);
	tmp = miibus->read(miibus, phy_addr, MII_ADVERTISE);

	#if defined(CONFIG_MACH_UD8107_DVR)
	/* select Page 7  */
	miibus->write(miibus, phy_addr, 31, 7);
	/* read Register 16 RMII Mode setting (RMSR) */
	val = miibus->read(miibus, phy_addr, 16);
	val |= 1 << 3; //# RMII set
	val &= ~(1 << 12); /* RMII reference from PHY */
	miibus->write(miibus, phy_addr, 16, val);
	/* read Register 19 */
	val = miibus->read(miibus, phy_addr, 19);
	val &= 0xffcF; //clear led function
	val |= 1 << 4; //led sel_1
	miibus->write(miibus, phy_addr, 19, val);
	/* select Page 0 */
	miibus->write(miibus, phy_addr, 31, 0);
	#endif

	return;
}



static void offload_rx_queue(struct device *dev, struct offload_internal *inter, int size)
{
	struct ETH_OFFLOAD_desc *desc = NULL;

    if (ETH_OFFLOAD_Q_IS_DEQUEUED(inter->rxQ->write, ETH_OFFLOAD_Q_ARM))
        desc = inter->rxQ->write;

	if(desc!=NULL) {

		struct sk_buff *skb;

        skb = dev_alloc_skb(size);
		if (WARN_ON(!skb)) {
            printk(" [ETH_OFFLOAD]: skb alloc failed!!!\n");
			return;
		}
		skb->dev = inter->ndev;
		skb_reserve(skb, NET_IP_ALIGN);

        desc->dma_address = dma_map_single(dev, skb->data, skb->len, DMA_FROM_DEVICE);
        desc->length = skb->len;
		desc->skb = skb;

        ETH_OFFLOAD_Q_SET_ENQUEUED(desc, ETH_OFFLOAD_Q_ARM);

        inter->rxQ->write = ETH_OFFLOAD_Q_move(inter->rxQ, inter->rxQ->write);
	}
}

static void offload_rx_queueing(struct device *dev, struct offload_internal *inter, int num, int size)
{
	int i = 0;

	while (i < num) {
        offload_rx_queue(dev, inter, size);
		i++;
	}
}



/*************************************************************************
 *  Callback handler for Rx & Tx
 *************************************************************************/

static unsigned int offload_rxHandler(void *hApp, struct ETH_OFFLOAD_desc *desc)
{
	struct sk_buff		*skb = (struct sk_buff *)desc->skb;
	struct net_device	*ndev = skb->dev;
	struct offload_internal	*inter = netdev_priv(ndev);
	struct device		*cpsw_dev = &ndev->dev;
    unsigned int len = desc->length;

	/* free and bail if we are shutting down */
	if (unlikely(!netif_running(ndev))) {
		dev_kfree_skb_any(skb);
		goto offload_rxHandler_exit;
	}

    #if 0
    if(unlikely(desc->status!=0))
    {
        /* error case, mostly checksum error */
        dev_kfree_skb_any(skb);
        inter->offloadstats.arm_rx_packet_drop++;
    }
    else
    #endif
    {
        dma_unmap_single(cpsw_dev, desc->dma_address, len, DMA_FROM_DEVICE);

        do {

            /* let go the skb up the stack */
            skb_put(skb, len);
            skb->protocol = eth_type_trans(skb, ndev);

            if(gro_enable)
            {
                if(unlikely(desc->status!=0))
                {
                    /* if checksum error detected then let linux do checksum again and confirm the error
                        i.e let linux redo checksum and drop packet internally if required
                    */
                    skb->ip_summed = CHECKSUM_NONE;
                }
                else
                {
                    skb->ip_summed = CHECKSUM_UNNECESSARY;
                }
                napi_gro_receive(&inter->napi, skb);
            }
            else
            {
                netif_receive_skb(skb);
            }

            ndev->stats.rx_bytes += len;
            ndev->stats.rx_packets++;
        } while(0);
    }

	offload_rx_queue(cpsw_dev, inter, inter->rx_buf_size);

offload_rxHandler_exit:

	return 1;
}


static unsigned int offload_txHandler(void *hApp, struct ETH_OFFLOAD_desc *desc)
{
	struct sk_buff *skb = (struct sk_buff *)desc->skb;
	struct net_device *ndev = skb->dev;

	ndev->stats.tx_packets++;
	ndev->stats.tx_bytes += desc->length;
	dev_kfree_skb_any(skb);

	if (unlikely(netif_queue_stopped(ndev))){
        /*
        printk( KERN_ERR " [ETH_OFFLOAD] TX: wake netif queue !!!\n");
        */
		netif_wake_queue(ndev);
	}

	return 1;
}

/*************************************************************************
 *  Driver ISR
 *************************************************************************/


static int offload_napi_poll (struct napi_struct *napi, int budget)
{
	struct offload_internal *inter = interglobal;
	int numpktsTx=0, numpktsRx=0;

	inter->offloadstats.arm_napicnt++;

    numpktsRx = ETH_OFFLOAD_Q_Poll(inter->rxQ, budget);

    numpktsTx = ETH_OFFLOAD_Q_Poll(inter->txQ, budget);

	if (numpktsRx < budget ){

		napi_complete(napi);

        hrtimer_start(&inter->poll_timer,
			ns_to_ktime(inter->coal_intvl_nsecs), HRTIMER_MODE_REL_PINNED);
	}

	return numpktsRx;
}


static enum hrtimer_restart cpsw_hrtimer_poll(struct hrtimer *timer)
{
	struct offload_internal *inter;
    int rxRdy, txRdy;

	inter = container_of(timer, struct offload_internal, poll_timer);

    inter->offloadstats.arm_nfyisrcnt++;

	if(inter->ndev->flags & IFF_UP)
	{

	    rxRdy = ETH_OFFLOAD_Q_IS_ENQUEUED(inter->rxQ->read, ETH_OFFLOAD_Q_DSP);
	    txRdy = ETH_OFFLOAD_Q_IS_ENQUEUED(inter->txQ->read, ETH_OFFLOAD_Q_DSP);

	    if(rxRdy||txRdy)
	    {
		if(likely(netif_running(inter->ndev))) {
			napi_schedule(&inter->napi);
			    return HRTIMER_NORESTART;
		    }
	    }
	}

    /* interface is not running, come back after inter->coal_intvl_nsecs */
    hrtimer_forward_now(&inter->poll_timer, ns_to_ktime(EMAC_POLL_INTERVAL_INIT_NSECS));
    return HRTIMER_RESTART;
}



/*************************************************************************
 *  Driver interface functions
 *************************************************************************/

/**
 * cpsw_dev_xmit: EMAC Transmit function
 * @skb: SKB pointer
 * @ndev: The EMAC network adapter
 *
 * Called by the system to transmit a packet  - we queue the packet in
 * EMAC hardware transmit queue
 *
 * Returns success(NETDEV_TX_OK) or error code (typically out of desc's)
 */
static int cpsw_dev_xmit(struct sk_buff *skb, struct net_device *ndev)
{
	struct device *cpsw_dev = &ndev->dev;
	struct cpsw_slave *slave = NULL;
	int ret_code, idx;
	struct offload_internal *inter = netdev_priv(ndev);
	struct ETH_OFFLOAD_desc *desc = 0;
	unsigned long addr;

	for (idx = 0; idx < (inter)->data.slaves; idx++)
	{		/* If no link, return */
		slave= inter->slaves + idx;
		if (unlikely(!(slave->phy->link))){
			if (netif_msg_tx_err(inter) && net_ratelimit())
				dev_err(cpsw_dev, "DSP Accelarated EMAC: No link to transmit");
			goto fail_tx;
		}
		else
			break;
	}

	ret_code = skb_padto(skb, EMAC_DEF_MIN_ETHPKTSIZE);
	if (unlikely(ret_code < 0)) {
		if (netif_msg_tx_err(inter) && net_ratelimit())
			dev_err(cpsw_dev, "DSP Accelarated EMAC: packet pad failed");
		goto fail_tx;
	}

	addr = dma_map_single(cpsw_dev, skb->data, skb->len, DMA_TO_DEVICE);

	desc = (struct ETH_OFFLOAD_desc *)ETH_OFFLOAD_Q_Dequeue(inter->txQ);
	if (NULL == desc) {
		goto fail_tx;
	}

    desc->dma_address = addr;
    desc->length = skb->len;
	desc->skb = skb;

    ETH_OFFLOAD_Q_SET_ENQUEUED(desc, ETH_OFFLOAD_Q_ARM);

	return NETDEV_TX_OK;

fail_tx:
	ndev->stats.tx_dropped++;
	netif_stop_queue(ndev);
/*    printk( KERN_ERR " [ETH_OFFLOAD] TX: stopping netif queue !!!\n"); */
	return NETDEV_TX_BUSY;
}

/**
 * cpsw_dev_tx_timeout: EMAC Transmit timeout function
 * @ndev: The EMAC network adapter
 *
 * Called when system detects that a skb timeout period has expired
 * potentially due to a fault in the adapter in not being able to send
 * it out on the wire. We teardown the TX channel assuming a hardware
 * error and re-initialize the TX channel for hardware operation
 *
 */
static void cpsw_dev_tx_timeout(struct net_device *ndev)
{
	struct offload_internal *inter = netdev_priv(ndev);
	struct device *cpsw_dev = &ndev->dev;

	printk( KERN_INFO " [ETH OFFLOAD] TX timeout occured !!!\n");
	if (netif_msg_tx_err(inter))
		dev_err(cpsw_dev, "DSP Accelarated EMAC: xmit timeout, restarting TX");

	ndev->stats.tx_errors++;
	printk(KERN_DEBUG"Completed cpsw_dev_tx_timeout function\n\n");
}


/**
 * cpsw_dev_setmac_addr: Set mac address in the adapter
 * @ndev: The EMAC network adapter
 * @addr: MAC address to set in device
 *
 * Called by the system to set the mac address of the adapter (Device)
 *
 * Returns success (0) or appropriate error code (none as of now)
 */
static int cpsw_dev_setmac_addr(struct net_device *ndev, void *addr)
{
	struct offload_internal *inter = netdev_priv(ndev);
	struct device *cpsw_dev = &inter->ndev->dev;
	struct sockaddr *sa = addr;

	printk(KERN_DEBUG"Entered cpsw_dev_setmac_addr function\n\n");
	if (!is_valid_ether_addr(sa->sa_data))
		return -EINVAL;

	/* Store mac addr in inter and rx channel and set it in EMAC hw */
	memcpy(inter->mac_addr, sa->sa_data, ndev->addr_len);
	memcpy(ndev->dev_addr, sa->sa_data, ndev->addr_len);

	/* MAC address is configured only after the interface is enabled. */
	if (netif_running(ndev)) {
		memcpy(inter->mac_addr, sa->sa_data, ndev->addr_len);
	}

	if (netif_msg_drv(inter))
		dev_notice(cpsw_dev, "DSP Accelarated EMAC: cpsw_dev_setmac_addr %pM\n",
				inter->mac_addr);
	printk(KERN_DEBUG"Completed cpsw_dev_setmac_addr function\n\n");

	return 0;
}

/**
 * cpsw_devioctl: EMAC adapter ioctl
 * @ndev: The EMAC network adapter
 * @ifrq: request parameter
 * @cmd: command parameter
 *
 * EMAC driver ioctl function
 *
 * Returns success(0) or appropriate error code
 */
static int cpsw_devioctl(struct net_device *ndev, struct ifreq *ifrq, int cmd)
{
	printk(KERN_DEBUG "Entered cpsw_devioctl function\n\n");
	dev_warn(&ndev->dev, "DSP Accelarated EMAC: ioctl not supported\n");

	if (!(netif_running(ndev)))
		return -EINVAL;

	return -EOPNOTSUPP;
}


static void cpsw_slave_open(struct cpsw_slave *slave, struct offload_internal *inter)
{
	slave->phy = phy_connect(inter->ndev, slave->data->phy_id,
				 &cpsw_adjust_link, 0, slave->data->phy_if);
	if (IS_ERR(slave->phy)) {
		printk(KERN_ERR "phy %s not found on slave %d\n",
		    slave->data->phy_id, slave->slave_num);
		slave->phy = NULL;

	} else {
		printk(KERN_ERR "\nCPSW phy found : id is : 0x%x\n",
			slave->phy->phy_id);
		cpsw_set_phy_config(inter, slave->phy);
		inter->link[slave->slave_num]   = 0;
		inter->speed[slave->slave_num]  = 0;
		inter->duplex[slave->slave_num] = ~0;
		phy_start(slave->phy);
	}
}

static void cpsw_init_host_port(struct offload_internal *inter)
{
	printk("\n cpsw_init_host_port executed on m3\n");
}

/**
 * cpsw_dev_open: EMAC device open
 * @ndev: The EMAC network adapter
 *
 * Called when system wants to start the interface. We init TX/RX channels
 * and enable the hardware for packet reception/transmission and start the
 * network queue.
 *
 * Returns 0 for a successful open, or appropriate error code
 */
static int offload_cpsw_dev_open(struct net_device *ndev)
{
	struct device *cpsw_dev = &ndev->dev;
	struct offload_internal *inter = netdev_priv(ndev);
	struct ETH_OFFLOAD_open      devopen;
	struct ETH_OFFLOAD_setup     devsetup;
	struct ETH_OFFLOAD_coalesce  devcoalesce;
	struct cpsw_slave            *slave;
	u32 cnt;
	int status;
	int ret;
	int i;
	int max = ETH_OFFLOAD_SWDESC_MAX;

	netif_carrier_off(ndev);

	ret = clk_enable(cpsw_clk);
	if (ret < 0) {
		dev_err(cpsw_dev, "unable to turn on device clock\n");
		return ret;
	}

	for (cnt = 0; cnt < ETH_ALEN; cnt++)
		ndev->dev_addr[cnt] = inter->mac_addr[cnt];

	/* Configuration items */
	inter->rx_buf_size = EMAC_DEF_MAX_FRAME_SIZE + NET_IP_ALIGN;

    devsetup.offloadFlag = 0;
    if(gro_enable)
    {
	    devsetup.offloadFlag |= ETH_OFFLOAD_FLAG_RX_CSUM;

        printk(" [ETH_OFFLOAD] RX CSUM is ENABLED !!!\n");
    }
    if(gso_enable)
    {
	    devsetup.offloadFlag |= ETH_OFFLOAD_FLAG_TX_CSUM;

        printk(" [ETH_OFFLOAD] TX CSUM is ENABLED !!!\n");
    }

	/* Emac setup */
	status = ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_EMAC_SETUP, (void *)&devsetup, sizeof(devsetup));
	if (status < 0){
		printk(KERN_ERR "OFFLOAD: Remote Emac setup failed");
		return -1; //TODO error codes
	}

	devopen.mtu = ETH_OFFLOAD_MTU;


    /* phyid has no meaning on M3 as all configuration is done on A8 */

	devopen.nmacaddrs = 2;
    devopen.phyid= 0;
	/* Enable Interrupt pacing if configured */
    devopen.coal.rx_coalesce_usecs = EMAC_POLL_INTERVAL_DEFAULT_USECS;

	for (i = 0; i < devopen.nmacaddrs; i++)
	{
		slave = (inter->slaves + i);
		devopen.macaddrs[i][0] = slave->data->mac_addr[0];
		devopen.macaddrs[i][1] = slave->data->mac_addr[1];
		devopen.macaddrs[i][2] = slave->data->mac_addr[2];
		devopen.macaddrs[i][3] = slave->data->mac_addr[3];
		devopen.macaddrs[i][4] = slave->data->mac_addr[4];
		devopen.macaddrs[i][5] = slave->data->mac_addr[5];
	}

	/* Emac open */
	status = ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_OPEN, (void *)&devopen, sizeof(devopen));
	if (status < 0){
		printk(KERN_ERR "OFFLOAD: Remote Emac open failed");
		return -1; //TODO error codes
	}

	/* Create ready rx skbs */
	offload_rx_queueing(cpsw_dev, inter, max, inter->rx_buf_size);

	/* Emac start */
	cnt = 2;
	do {
		/* Time given for all the rx desc to be sent to dsp */
		msleep(100);
	} while (ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_START, NULL, 0) < 0 && cnt--);

	if (cnt == 0) {
		printk(KERN_ERR "OFFLOAD: Remote Emac start failed");
		ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_CLOSE, NULL, 0);
		return -1;
	}

	cpsw_init_host_port(inter);
	for_each_slave(inter, cpsw_slave_open, inter);


// TBD: This code might be needed when stats are enabled

// 		/* disable priority elevation and enable statistics
// 		on all ports */
// 		__raw_writel(0, &priv->regs->ptype);

// 		/* enable statistics collection only on the host port */
// 		/*__raw_writel(BIT(priv->host_port),
// 				&priv->regs->stat_port_en);*/
// 		__raw_writel(0x7, &priv->regs->stat_port_en);

	devcoalesce.coal_intvl        = inter->coal_intvl_nsecs; //in val
	devcoalesce.rx_coalesce_usecs = inter->coal_intvl_usecs; //out val
	devcoalesce.bus_freq_mhz      = inter->bus_freq_mhz;

	status = ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_COALESCE, (void *)&devcoalesce, sizeof(devcoalesce));
	if (status < 0){
		printk(KERN_ERR "OFFLOAD: Remote Emac open failed");
		return -1; //TODO error codes
	}
	napi_enable(&inter->napi);

	ndev->flags |= IFF_UP;

	if (netif_msg_drv(inter))
		dev_notice(cpsw_dev, "DSP Accelarated EMAC: Opened %s\n", ndev->name);

	return 0;
}

static void cpsw_slave_stop(struct cpsw_slave *slave, struct offload_internal *inter)
{

	if (!slave->phy)
		return;
	phy_stop(slave->phy);
	phy_disconnect(slave->phy);
	slave->phy = NULL;
}

/**
 * cpsw_dev_stop: EMAC device stop
 * @ndev: The EMAC network adapter
 *
 * Called when system wants to stop or down the interface. We stop the network
 * queue, disable interrupts and cleanup TX/RX channels.
 *
 * We return the statistics in net_device_stats structure pulled from emac
 */
static int cpsw_dev_stop(struct net_device *ndev)
{
	struct offload_internal *inter = netdev_priv(ndev);
	struct device *cpsw_dev = &ndev->dev;

	/* inform the upper layers. */
	netif_stop_queue(ndev);
	napi_disable (&inter->napi);
	netif_carrier_off(ndev);

	for_each_slave(inter, cpsw_slave_stop, inter);

	ETH_OFFLOAD_Rpc (ETH_OFFLOAD_FUNCID_CLOSE, NULL, 0);

	if (inter->data.phy_control)
		(*inter->data.phy_control)(false);
	clk_disable(cpsw_clk);

	ETH_OFFLOAD_Q_Reset(inter->txQ);
	ETH_OFFLOAD_Q_Reset(inter->rxQ);

	if (netif_msg_drv(inter))
		dev_notice(cpsw_dev, "DSP Accelarated EMAC: %s stopped\n", ndev->name);

	return 0;
}

/**
 * cpsw_dev_getnetstats: EMAC get statistics function
 * @ndev: The EMAC network adapter
 *
 * Called when system wants to get statistics from the device.
 *
 * We return the statistics in net_device_stats structure pulled from emac
 */
static struct net_device_stats *cpsw_dev_getnetstats(struct net_device *ndev)
{
	struct ETH_OFFLOAD_stats stats;
	UInt32 status;

	memset(&stats, 0 , sizeof(stats));

	if(!ndev) {
		return NULL;
	}

	status = ETH_OFFLOAD_Rpc(ETH_OFFLOAD_EMAC_GET_STATISTICS, (void *)&stats, sizeof(stats));
	if (status != 1){
		return &ndev->stats;
	}

	ndev->stats.multicast += stats.multicast;

	ndev->stats.collisions += stats.collisions;

	ndev->stats.rx_length_errors += stats.rx_length_errors;

	ndev->stats.rx_over_errors += stats.rx_over_errors;
	ndev->stats.rx_fifo_errors += stats.rx_fifo_errors;
	ndev->stats.tx_carrier_errors += stats.tx_carrier_errors;
	ndev->stats.tx_fifo_errors = stats.tx_fifo_errors;
	return &ndev->stats;
}

static const struct net_device_ops cpsw_netdev_ops = {
	.ndo_open		= offload_cpsw_dev_open,

	.ndo_stop		= cpsw_dev_stop,
	.ndo_start_xmit		= cpsw_dev_xmit,
	.ndo_set_mac_address	= cpsw_dev_setmac_addr,
	.ndo_do_ioctl		= cpsw_devioctl,
	.ndo_tx_timeout		= cpsw_dev_tx_timeout,
	.ndo_get_stats		= cpsw_dev_getnetstats,
#ifdef CONFIG_NET_POLL_CONTROLLER
	.ndo_poll_controller	= cpsw_poll_controller,
#endif
};

static void cpsw_slave_init(struct cpsw_slave *slave, struct offload_internal *inter)
{
// 	void __iomem		*regs = inter->regs;
	int			slave_num = slave->slave_num;
	struct cpsw_slave_data	*data = inter->data.slave_data + slave_num;

	slave->data	= data;
}

/**
 * offload_dev_probe: EMAC device probe
 * @pdev: The EMAC device that we are removing
 *
 * Called when probing for emac devicesr. We get details of instances and
 * resource information from platform init and register a network device
 * and allocate resources necessary for driver to perform
 */
static int __devinit offload_dev_probe(struct platform_device *pdev)
{
	int rc = 0;
	struct net_device *ndev;
	struct offload_internal *inter;
	struct device *cpsw_dev = &ndev->dev;
	struct cpsw_platform_data *pdata;
//     struct resource *res;
//     unsigned int cpsw_base_phys;
	int i, ret;

	pdata = pdev->dev.platform_data;
	if (!pdata) {
		printk(KERN_ERR "DaVinci EMAC: No platform data\n");
		return -ENODEV;
	}

	cpsw_clk = clk_get(&pdev->dev, NULL);
	if (IS_ERR(cpsw_clk)) {
		printk (KERN_ERR "Clk Get Failed\n");
		return -EBUSY;
	}

	ndev = alloc_etherdev(sizeof(struct offload_internal));
	if (!ndev) {
		printk(KERN_ERR "DSP Accelarated EMAC: Error allocating net_device\n");
		clk_put(cpsw_clk);
		return -ENOMEM;
	}

	platform_set_drvdata(pdev, ndev);
	inter = netdev_priv(ndev);
	interglobal = inter;
    offload_reset_perf();
	inter->data = *pdata;
	inter->pdev = pdev;
	inter->ndev = ndev;
	inter->msg_enable = netif_msg_init(debug_level, OFFLOAD_DEBUG_FLAGS);
	inter->bus_freq_mhz = clk_get_rate(cpsw_clk) / 1000000;

	spin_lock_init(&inter->txq_lock);

	cpsw_dev = &ndev->dev;

	inter->clk = cpsw_clk;

	inter->slaves = kzalloc(sizeof(struct cpsw_slave) * pdata->slaves,
			       GFP_KERNEL);
	if (!inter->slaves) {
		dev_err(cpsw_dev, "failed to allocate slave ports\n");
		ret = -EBUSY;
		goto clean_ndev_ret;
	}
	for (i = 0; i < pdata->slaves; i++)
	{
		inter->slaves[i].slave_num = i;
		inter->slaves[i].data = &pdata->slave_data[i]; //+ (i * sizeof(struct cpsw_slave_data));
	}


	// put phy_id under slave
// 	inter->phy_id = pdata->phy_id;
// 	inter->slave(inter, 0)->data->phy_id = pdata->slave(inter, 0)->data->phy_id;
// 	inter->slave(inter, 1)->data->phy_id = pdata->slave(inter, 1)->data->phy_id;

	inter->rmii_en = pdata->rmii_en;

	if (is_valid_ether_addr(pdata->mac_addr)) {
		memcpy(inter->mac_addr, pdata->mac_addr, ETH_ALEN);
		printk(KERN_INFO "Detected MACID=%x:%x:%x:%x:%x:%x\n",
			inter->mac_addr[0], inter->mac_addr[1],
			inter->mac_addr[2], inter->mac_addr[3],
			inter->mac_addr[4], inter->mac_addr[5]);
	} else {
		random_ether_addr(inter->mac_addr);
		printk(KERN_INFO "Random MACID=%x:%x:%x:%x:%x:%x\n",
			inter->mac_addr[0], inter->mac_addr[1],
			inter->mac_addr[2], inter->mac_addr[3],
			inter->mac_addr[4], inter->mac_addr[5]);
	}

	inter->version = EMAC2;

	ndev->netdev_ops = &cpsw_netdev_ops;
	SET_ETHTOOL_OPS(ndev, &ethtool_ops);

	netif_napi_add (ndev, &inter->napi,
			offload_napi_poll, ETH_OFFLOAD_Q_WEIGHT);

	for_each_slave(inter, cpsw_slave_init, inter);

	/* register the network device */
	SET_NETDEV_DEV(ndev, &pdev->dev);

    if(gro_enable)
    {
        ndev->features |= (OFFLOAD_GRO_FLAGS);
        printk(" [ETH OFFLOAD] GRO is ENABLED !!!\n");
    }
    if(gso_enable)
    {
        ndev->features |= (OFFLOAD_GSO_FLAGS);
        printk(" [ETH OFFLOAD] GSO is ENABLED !!!\n");
    }

	rc = register_netdev(ndev);
	if (rc) {
		dev_err(cpsw_dev, "DSP Accelarated EMAC: Error in register_netdev\n");
		return -ENODEV;
	}

	if (netif_msg_probe(inter)) {
		dev_notice(cpsw_dev, "DSP Accelarated EMAC Probe found device "\
				"(regs: %p, irq: %d)\n",
				(void *)inter->cpsw_base_phys, ndev->irq);
	}

	/* Create Qs */
	inter->txQ = ETH_OFFLOAD_Q_Create(ETH_OFFLOAD_SWDESC_MAX,
			(QCALLBACK)offload_txHandler,
			(void *)inter, "TX",
            &pETH_OFFLOAD_shm->txSwDesc[0]
            );

	inter->rxQ = ETH_OFFLOAD_Q_Create(ETH_OFFLOAD_SWDESC_MAX,
			(QCALLBACK)offload_rxHandler,
			(void *)inter, "RX",
            &pETH_OFFLOAD_shm->rxSwDesc[0]
            );

    inter->coal_intvl_usecs = EMAC_POLL_INTERVAL_DEFAULT_USECS;
    inter->coal_intvl_nsecs = inter->coal_intvl_usecs*1000;

	hrtimer_init(&inter->poll_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	inter->poll_timer.function = cpsw_hrtimer_poll;

	hrtimer_start(
            &inter->poll_timer,
            ktime_set(0, EMAC_POLL_INTERVAL_INIT_NSECS ),
			HRTIMER_MODE_REL_PINNED);

	return 0;

clean_ndev_ret:
	free_netdev(ndev);
	return ret;
}

/**
 * offload_dev_remove: EMAC device remove
 * @pdev: The EMAC device that we are removing
 *
 * Called when removing the device driver. We disable clock usage and release
 * the resources taken up by the driver and unregister network device
 */
static int __devexit offload_dev_remove(struct platform_device *pdev)
{
	UInt32 status;
	struct net_device *ndev = platform_get_drvdata(pdev);
	struct offload_internal *inter;

	inter = netdev_priv(ndev);

    hrtimer_cancel(&inter->poll_timer);

	status = ETH_OFFLOAD_Q_Destroy(inter->txQ);
	if( status != 1  ){
		printk("cpsw_dev_stop:ETH_OFFLOAD_Q_Destroy failed !!!\n");
		return -1;
	}
	status = ETH_OFFLOAD_Q_Destroy(inter->rxQ);
	if( status != 1){
		printk("cpsw_dev_stop:ETH_OFFLOAD_Q_Destroy failed !!!\n");
		return -1;
	}

	dev_notice(&ndev->dev, " [ETH OFFLOAD] Driver is removed !!!\n");

	clk_put(cpsw_clk);
	kfree(inter->slaves);
	unregister_netdev(ndev);
	free_netdev(ndev);

	return 0;
}

static int offload_dev_suspend(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct net_device *ndev = platform_get_drvdata(pdev);

	if (netif_running(ndev))
		cpsw_dev_stop(ndev);
	return 0;
}

static int offload_dev_resume(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct net_device *ndev = platform_get_drvdata(pdev);

	if (netif_running(ndev))
		offload_cpsw_dev_open(ndev);
	return 0;
}

static const struct dev_pm_ops offload_dev_pm_ops = {
	.suspend	= offload_dev_suspend,
	.resume		= offload_dev_resume,
};

/**
 * offload_dev_driver: EMAC platform driver structure
 */
static struct platform_driver offload_dev_driver = {
	.driver = {
		.name	 = "cpsw_offload",
		.owner	 = THIS_MODULE,
		.pm	 = &offload_dev_pm_ops,
	},
	.probe  = offload_dev_probe,
	.remove = __devexit_p(offload_dev_remove),
};

/**
 * offload_dev_init: EMAC driver module init
 *
 * Called when initializing the driver. We register the driver with
 * the platform.
 */
static int __init offload_dev_init(void)
{
	int ret = 0;

    if(shared_mem_phys_addr==0)
    {
        shared_mem_phys_addr = ETH_OFFLOAD_SHM_PHYS_ADDR; /* default address */
    }

    printk(" [ETH OFFLOAD] Shared memory address = 0x%08x !!!\n", shared_mem_phys_addr);

    pETH_OFFLOAD_shm = ioremap_nocache(shared_mem_phys_addr, sizeof(*pETH_OFFLOAD_shm));
    if(pETH_OFFLOAD_shm==NULL)
    {
        printk(" [ETH OFFLOAD] ioremap(0x%08x) FAILED !!!\n", shared_mem_phys_addr);
    }

	/* Q init */
	ret = ETH_OFFLOAD_Rpc_Init();
	if (ret < 0){
		printk (KERN_ERR "OFFLOAD: RPC init Failed\n");
		return ret;
	}

	ret = platform_driver_register(&offload_dev_driver);
	if (ret) {
		return ret;
	}

	return 0;
}
module_init(offload_dev_init);

/**
 * offload_dev_exit: EMAC driver module exit
 *
 * Called when exiting the driver completely. We unregister the driver with
 * the platform and exit
 */
static void __exit offload_dev_exit(void)
{
	platform_driver_unregister(&offload_dev_driver);

	ETH_OFFLOAD_Rpc_DeInit();
}
module_exit(offload_dev_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("DSP Accelarated EMAC Maintainer: TI/Sylvan Labs");
MODULE_DESCRIPTION("DSP Accelarated EMAC Ethernet driver");
