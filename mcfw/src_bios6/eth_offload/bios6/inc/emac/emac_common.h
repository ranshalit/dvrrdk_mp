/**
 *   @file  emac_common.h
 *
 *   @brief
 *      Common definitions
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2008, Texas Instruments, Inc.
 *
 *  \par
 */

/* =============================================================================
 *  Revision History
 *  ===============
 *  27-Jul-2011 Added DM8168 Support
 * =============================================================================*/


#ifndef _EMAC_COMMON_H_
#define _EMAC_COMMON_H_

/*
 * EMAC Descriptor section
 */


/**
 * @brief
 *  EMAC Buffer descriptor format.
 *
 * @details
 *  The following is the format of a single buffer descriptor
 *  on the EMAC.
 */
typedef struct _EMAC_Desc
{
   /**
    * @brief        Pointer to next descriptor in chain.
    */
  volatile struct _EMAC_Desc *pNext;
   /**
    * @brief        Pointer to data buffer.
    */
  volatile Uint8             *pBuffer;
   /**
    * @brief        Buffer Offset(MSW) and Length(LSW).
    */
  volatile Uint32            BufOffLen;
   /**
    * @brief        Packet Flags(MSW) and Length(LSW).
    */
  volatile Uint32            PktFlgLen;
   /**
    * @brief        Field for software
    */
  volatile Uint32            SwField;
} EMAC_Desc;


/* ------------------------ */
/* DESCRIPTOR ACCESS MACROS */
/* ------------------------ */

/* Packet Flags */
#define EMAC_DSC_FLAG_SOP                       0x80000000u
#define EMAC_DSC_FLAG_EOP                       0x40000000u
#define EMAC_DSC_FLAG_OWNER                     0x20000000u
#define EMAC_DSC_FLAG_EOQ                       0x10000000u
#define EMAC_DSC_FLAG_TDOWNCMPLT                0x08000000u
#define EMAC_DSC_FLAG_PASSCRC                   0x04000000u

/* The following flags are RX only */
#define EMAC_DSC_FLAG_JABBER                    0x02000000u
#define EMAC_DSC_FLAG_OVERSIZE                  0x01000000u
#define EMAC_DSC_FLAG_FRAGMENT                  0x00800000u
#define EMAC_DSC_FLAG_UNDERSIZED                0x00400000u
#define EMAC_DSC_FLAG_CONTROL                   0x00200000u
#define EMAC_DSC_FLAG_OVERRUN                   0x00100000u
#define EMAC_DSC_FLAG_CODEERROR                 0x00080000u
#define EMAC_DSC_FLAG_ALIGNERROR                0x00040000u
#define EMAC_DSC_FLAG_CRCERROR                  0x00020000u
#define EMAC_DSC_FLAG_NOMATCH                   0x00010000u

#endif /* _EMAC_COMMON_H_ */
