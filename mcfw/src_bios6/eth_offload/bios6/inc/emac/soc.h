/*  ============================================================================
 *  Copyright (c) Texas Instruments Inc 2002, 2003, 2004, 2005, 2006
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *  ===========================================================================
 */
/* =============================================================================
 *  Revision History
 *  ===============
 *  04-Sep-2006 	Raghu 					File Created
 *  21-Sep-2006 	NG    					Updated the file.
 *  22-Sep-2006 	Shiv  					Updated EDMA related stuffs
 *  07-Oct-2006 	NG    					Updated VPSS related stuffs
 *  27-Jul-2011 	Prasad V. Joshi 		Added DM8168 Support
 * =============================================================================
 */
#ifndef _SOC_H_
#define _SOC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <mcfw/src_bios6/eth_offload/bios6/inc/emac/cslr.h>
#include <mcfw/src_bios6/eth_offload/inc/eth_offload_emac.h>
/**************************************************************************\
* 64 soc file
\**************************************************************************/

/*****************************************************************************\
* Static inline definition
\*****************************************************************************/
#ifndef CSL_IDEF_INLINE
#define CSL_IDEF_INLINE static inline
#endif

/**************************************************************************\
* Peripheral Instance count
\**************************************************************************/

/** @brief Number of Emac instances */
#define CSL_EMAC_PER_CNT 				 1

/**************************************************************************\
* Peripheral Instance definitions.
\**************************************************************************/

/** @brief Peripheral Instance for EMAC */
#define  CSL_EMAC                       (0)


/**************************************************************************\
* Peripheral Base Address
\**************************************************************************/

/*****************************************************************************\
* Interface 0
\*****************************************************************************/

/** @brief Base address of EMAC memory mapped registers */
#define CSL_EMAC_0_REGS                 (ETH_OFFLOAD_EMAC_0_REGS)

/** @brief Base address of ECTL memory mapped registers */
#define CSL_ECTL_0_REGS                 (0x4A100900u)

/*****************************************************************************\
* Interrupt Event IDs
\*****************************************************************************/
/**
 * @brief   Interrupt Event IDs
 */

/* CPGMAC Wrapper interrupt */
#define    CSL_CIC3_EVENTID_MACINT0         (32)    /* Ethernet MAC interrupt */
#define    CSL_CIC3_EVENTID_MACINTR0        (33)    /* Ethernet MAC interrupt */
#define    CSL_CIC3_EVENTID_MACINTX0        (34)    /* Ethernet MAC interrupt */

/*****************************************************************************\
* Interface 1
\*****************************************************************************/


/** @brief Base address of EMAC memory mapped registers */
#define CSL_EMAC_1_REGS                 (ETH_OFFLOAD_EMAC_1_REGS)

/** @brief Base address of ECTL memory mapped registers */
#define CSL_ECTL_1_REGS                 (0x4A120900u)

/******************************************************************************\
* EMAC Descriptor section
\******************************************************************************/
#define CSL_EMAC_0_DSC_BASE_ADDR          (0x4A102000u)
#define CSL_EMAC_1_DSC_BASE_ADDR          (0x4A122000u)

/*****************************************************************************\
* Interrupt Event IDs
\*****************************************************************************/
/**
 * @brief   Interrupt Event IDs
 */

/* CPGMAC Wrapper interrupt */
#define    CSL_CIC3_EVENTID_MACINT1         (36)    /* Ethernet MAC interrupt */
#define    CSL_CIC3_EVENTID_MACINTR1        (37)    /* Ethernet MAC interrupt */
#define    CSL_CIC3_EVENTID_MACINTX1        (38)    /* Ethernet MAC interrupt */

typedef Void* Handle;

#ifdef __cplusplus
}
#endif

#endif  /* _SOC_H_ */
