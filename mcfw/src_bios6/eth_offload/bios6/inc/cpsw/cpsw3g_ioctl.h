/*******************************************************************************
**+--------------------------------------------------------------------------+**
**|                            ****                                          |**
**|                            ****                                          |**
**|                            ******o***                                    |**
**|                      ********_///_****                                   |**
**|                      ***** /_//_/ ****                                   |**
**|                       ** ** (__/ ****                                    |**
**|                           *********                                      |**
**|                            ****                                          |**
**|                            ***                                           |**
**|                                                                          |**
**|         Copyright (c) 1998-2006 Texas Instruments Incorporated           |**
**|                        ALL RIGHTS RESERVED                               |**
**|                                                                          |**
**+--------------------------------------------------------------------------+**
*******************************************************************************/

/** \file   cpsw3g_ioctl.h
    \brief  CPSW3G Ioctl header file

    This file provides data structures that are required by the Ioctls. The
    application or the driver can include this file to use the Ioctl's
 */

#ifndef _CPSW3G_IOCTL_H
#define _CPSW3G_IOCTL_H

#ifdef __cplusplus
extern "C" {
#endif

/* IOCTL are classified by their type which is further qualified by opcodes within that type  */

/* IOCTL command code type */
#define CPSW3G_IOCTL_SWITCH_CMD   1u
#define CPSW3G_IOCTL_STAT_CMD 	  2u

/* IOCTL Opcodes for typr Switch Command */
#define CPSW3G_IOCTL_BASE                                         0u
#define CPSW3G_IOCTL_ALE_ADDMLT                     (CPSW3G_IOCTL_BASE + 1u)
#define CPSW3G_IOCTL_ALE_ADDUNI                     (CPSW3G_IOCTL_BASE + 2u)
#define CPSW3G_IOCTL_ALE_ADDOUI                     (CPSW3G_IOCTL_BASE + 3u)
#define CPSW3G_IOCTL_ALE_ADDVLAN                    (CPSW3G_IOCTL_BASE + 4u)
#define CPSW3G_IOCTL_ALE_DELADDR                    (CPSW3G_IOCTL_BASE + 5u)
#define CPSW3G_IOCTL_ALE_DELVLAN                    (CPSW3G_IOCTL_BASE + 6u)
#define CPSW3G_IOCTL_SET_PORT_VLAN_CONFIG           (CPSW3G_IOCTL_BASE + 7u)
#define CPSW3G_IOCTL_ALE_TIMEOUT                    (CPSW3G_IOCTL_BASE + 8u)
#define CPSW3G_IOCTL_ALE_DUMP                       (CPSW3G_IOCTL_BASE + 9u)
#define CPSW3G_IOCTL_SET_SWITCH_FLOW_CONTROL        (CPSW3G_IOCTL_BASE + 10u)
#define CPSW3G_IOCTL_SET_PRIORITY_MAPPING           (CPSW3G_IOCTL_BASE + 11u)
#define CPSW3G_IOCTL_ALE_FINDADDR                   (CPSW3G_IOCTL_BASE + 12u)
#define CPSW3G_IOCTL_PORT_STATISTICS_ENABLE         (CPSW3G_IOCTL_BASE + 13u)
#define CPSW3G_IOCTL_DUMP_CONFIG                    (CPSW3G_IOCTL_BASE + 14u)
#define CPSW3G_IOCTL_RATELIMIT                      (CPSW3G_IOCTL_BASE + 15u)
#define CPSW3G_IOCTL_ALE_FINDVLAN                   (CPSW3G_IOCTL_BASE + 16u)
#define CPSW3G_IOCTL_VIDINGRESSCHECK                (CPSW3G_IOCTL_BASE + 17u)
#define CPSW3G_IOCTL_ADD_UNKNOWN_VLAN_INFO          (CPSW3G_IOCTL_BASE + 18u)
#define CPSW3G_IOCTL_802_1                          (CPSW3G_IOCTL_BASE + 19u)
#define CPSW3G_IOCTL_MACAUTH                        (CPSW3G_IOCTL_BASE + 20u)
#define CPSW3G_IOCTL_RESET                          (CPSW3G_IOCTL_BASE + 21u)
#define CPSW3G_IOCTL_SET_PORT_CONFIG                (CPSW3G_IOCTL_BASE + 22u)
#define CPSW3G_IOCTL_GET_PORT_CONFIG                (CPSW3G_IOCTL_BASE + 23u)
#define CPSW3G_IOCTL_STP_CONFIG                     (CPSW3G_IOCTL_BASE + 24u)



/* Opcode definition for switch command */
#define CPSW3G_IOCTL_STAT_BASE 		0u
#define CPSW3G_IOCTL_STAT_CLEAR  	(CPSW3G_IOCTL_STAT_BASE +1u)
#define CPSW3G_IOCTL_STAT_GET		(CPSW3G_IOCTL_STAT_BASE +2u)

/**
 *  \brief CPMAC Single Multicast Ioctl
 *
 *  - CPMAC_DDC_IOCTL_MULTICAST_ADDR operations
 *  - Add/Del operations for adding/deleting a single multicast address
 */
typedef enum
{
    CPMAC_MULTICAST_ADD = 0,    /**< Add a single mcast address to the hardware mcast list */
    CPMAC_MULTICAST_DEL         /**< Delete a single mcast address from the hardware mcast list */
} CpmacSingleMultiOper;

/**
 *  \brief CPMAC All Multicast Ioctl
 *
 *  - CPMAC_DDC_IOCTL_ALL_MULTI operations
 *  - Set/Clear all multicast operation
 */
typedef enum
{
    CPMAC_ALL_MULTI_SET = 0,
    CPMAC_ALL_MULTI_CLR
} CpmacAllMultiOper;

/**
 * \brief MII Read/Write PHY register
 *
 * Parameters to read/write a PHY register via MII interface
 */
typedef struct
{
    Uint32      phyNum;             /**< Phy number to be read/written */
    Uint32      regAddr;            /**< Register to be read/written */
    Uint32      data;               /**< Data to be read/written */
} Cpsw3gPhyParams;

/**
 * \brief MAC  Address params
 *
 * Parameters for Configuring Mac address
 */
typedef struct
{
    Uint32      channel;            /**< Channel number for addr params */
    String      macAddress;         /**< Mac address  */
} CpmacAddressParams;


/* make the s/w  stats counters 64-bit wide */
typedef unsigned long long STATCTR;


/**
 * \brief CPSW3G Statistics
 *
 *  Statistics counters: Reflect cumulative statistics of enabled ports . The names
 *  of the counters in  this structure are of "MIB style" and correspond directly
 *  to the hardware counters provided by CPSW3G.
 */
typedef struct
{
  volatile STATCTR   ifInGoodFrames;
  volatile STATCTR   ifInBroadcasts;
  volatile STATCTR   ifInMulticasts;
  volatile STATCTR   ifInPauseFrames;
  volatile STATCTR   ifInCRCErrors;
  volatile STATCTR   ifInAlignCodeErrors;
  volatile STATCTR   ifInOversizedFrames;
  volatile STATCTR   ifInJabberFrames;
  volatile STATCTR   ifInUndersizedFrames;
  volatile STATCTR   ifInFragments;
  volatile STATCTR   reserved;
  volatile STATCTR   reserved2;
  volatile STATCTR   ifInOctets;
  volatile STATCTR   ifOutGoodFrames;
  volatile STATCTR   ifOutBroadcasts;
  volatile STATCTR   ifOutMulticasts;
  volatile STATCTR   ifOutPauseFrames;
  volatile STATCTR   ifDeferredTransmissions;
  volatile STATCTR   ifCollisionFrames;
  volatile STATCTR   ifSingleCollisionFrames;
  volatile STATCTR   ifMultipleCollisionFrames;
  volatile STATCTR   ifExcessiveCollisionFrames;
  volatile STATCTR   ifLateCollisions;
  volatile STATCTR   ifOutUnderrun;
  volatile STATCTR   ifCarrierSenseErrors;
  volatile STATCTR   ifOutOctets;
  volatile STATCTR   if64OctetFrames;
  volatile STATCTR   if65To127OctetFrames;
  volatile STATCTR   if128To255OctetFrames;
  volatile STATCTR   if256To511OctetFrames;
  volatile STATCTR   if512To1023OctetFrames;
  volatile STATCTR   if1024ToUPOctetFrames;
  volatile STATCTR   ifNetOctets;
  volatile STATCTR   ifRxSofOverruns;
  volatile STATCTR   ifRxMofOverruns;
  volatile STATCTR   ifRxDMAOverruns;
} Cpsw3gStatistics;


typedef struct _ALE_VLANCMD_T
 {
        Uint32  port;
        Uint32  vid;           /*VLAN identifier*/
        Uint32  prio_port;            /*port priority 0 -7*/
        Uint32  CFI_port;         /*port CFI  0 /1 */
        Uint8   mem_port;      /* port number 0 - 2 */
        Uint8   unreg_port;    /*unregistered multicast Egress Ports */
        Uint8   regag_port;    /*register multicast Egress ports*/
        Uint8   untag_port;    /* Untag ports */
        Uint8   add[6];
        Uint32  flag;
        Uint32  blocked;
        Uint32  secure;
        Uint32  ageable;
        Uint32  aleTimeout; /* timeout Expressed in millisecs */
        Uint8   aledump;  /* if 1 print ale table for debug purpose*/
 }ALE_VLANCMD;

typedef struct _PORT_CMD_T
{
        Uint32  threshold;
        Uint8   port;             /*Port number*/
        Uint32  value;            /* Value */
        Uint32  bl;               /* broadcast limit */
        Uint32  ml;               /* multicast limit */
        Uint8   EnDis;           /* 1 -enable/0- Disable */
        Uint8   add[6];       /* Mac address */

}PORT_CMD;

typedef struct _PRIO_CMD_T
{
        Uint8 port;             /*Port number*/
        Uint8 prio_rx;          /* Receiving packet priority */
        Uint8 prio_tx;          /* Transmitting packet priority */
        Uint8 prio_switch;      /* switch priority */
        Uint8 hostRxCh;     /* packet with switch priority receive on this channel */
        Uint8 hostRxChSuper;     /* supervisory packet receive on
                                                   this channel */
}PRIO_CMD;



/** The following structure is used as param for all IOCTL of type switch command */
typedef struct SWITCHCONSCMD
{
    Uint32 	opcode;           /*  opcode for switch control operation */
    /* Function pointer to a print function */
    int		(*consolePrint)(const char*,...);
    Uint8       usePrintFn;	  /* flag that indicates that the provided Printfn must be used
				   * Must be set to 1, if consolePrint is to be used for logging */
    union
    {
        ALE_VLANCMD		 	AleVlanCmd;

        PORT_CMD			PortCmd;

        PRIO_CMD			PrioCmd;


    }CmdType;

       Uint32 ret_type;   /* Return  Success/Failure */

}SwitchConsCmd;



/** The following structure is used as param for all IOCTL related to STATS */
typedef struct STATSCMD
{
	Uint32 opcode ;  /* opcode for stats operations */
	Cpsw3gStatistics  statsObj; /* used with get stats command */
}StatsCmd;

/* for now include the llPacketIoctl interface here */
// extern void llPacketIoctl(Uint32 dev,Uint32 cmd, void *param);

#ifdef __cplusplus
}
#endif

#endif /*_CPSW3G_IOCTL_H */
