#ifndef CSL_EMAC_H
#define CSL_EMAC_H

#ifdef __cplusplus
extern "C" {
#endif

#include <mcfw/src_bios6/eth_offload/bios6/inc/810x/emac_common.h>

/*Compile Time switch for using PHY1 or PHY0 on PG2.1. This will not break the backward compatibility of PHY between PG2.1 & PG1.0 */

#define USE_PHY1

/**/

/**************************************************************************\
* Peripheral Base Address
\**************************************************************************/
/** 3 Port Gigabit Switch registers                                         */
#define CPSW3G_REGS                         ((CSL_Cpsw3gRegs *)CPSW3G_BASEADDR)

/** EMAC Control Module registers                                           */
#define ECTL_REGS                           ((CSL_EctlRegs *)CPSW_3GSS_S_BASEADDR)

/**
 *  \defgroup   EMACDevice  EMAC
 *
 *  Constants, objects and interfaces for EMAC
 *  @{
 */
/**
 *  \defgroup   EMACConstants   EMAC Constants
 *  @{
 */

/* Board Specific Parameters
 *
 */
#define CPSW3G_MAX_INSTANCES                1u           /**< Max CPSW3G instances */
#define CPMAC_MIN_ETHERNET_PKT_SIZE         60u          /**< Minimum Ethernet packet size */

/*Use PHY1 or PHY0 on PG2.1*/
#ifdef USE_PHY1
	#define CPSW3G_NUM_PORTS                    3u           /**< CPSW3G Port Count */
	#define CPSW3G_NUM_MAC_PORTS                2u           /**< MAC Port Count */
#else
	#define CPSW3G_NUM_PORTS                    2u           /**< CPSW3G Port Count */
	#define CPSW3G_NUM_MAC_PORTS                1u           /**< MAC Port Count */
#endif
/*Use PHY1 or PHY0 on PG2.1*/


#define CPSW3G_DMA_PORT_NUM                 0u           /**< DMA Port number */
#define CPSW3G_BRD_PORT                     0xFFu        /**< Virtual Broadcast Port from the  Host Port */
#define CPDMA_MAX_DIR                       2u           /**< Number of DMA data directions (tx and rx) */
#define CPSW3G_MAX_VLANS                    1024u        /**< Maximum number of VLANs supported */
#define CPDMA_MAX_CHANNELS                  8u           /**< Maximum num of channels supported by the DMA */

#define CPSW3G_MIN_FREQUENCY_FOR_10MBPS     5500000u     /**< Minimum CPMAC bus frequency for 10   Mbps operation is 5.5 MHz (as per specs) */
#define CPSW3G_MIN_FREQUENCY_FOR_100MBPS    55000000u    /**< Minimum CPMAC bus frequency for 100  Mbps operation is 55  MHz (as per specs) */
#define CPSW3G_MIN_FREQUENCY_FOR_1000MBPS   125000000u   /**< Minimum CPMAC bus frequency for 1000 Mbps operation is 125 MHz (as per specs) */


#define CPSW3G_ALE_TABLE_DEPTH              1024u        /**< Maximum number of ALE table entries */

#define CPSW3G_USE_DEFAULT                  0x0afbdce1u  /**< Flag to indicate use of a default */
#define CPSW3G_MIN_ETHERNET_PKT_SIZE        60u          /**< Minimum Ethernet packet size */
#define CPSW3G_PORT_STATE_DISABLED          0u
#define CPSW3G_PORT_STATE_BLOCKED           1u
#define CPSW3G_PORT_STATE_LEARN             2u
#define CPSW3G_PORT_STATE_FORWARD           3u
#define CPSW3G_NUM_STAT_REGS                36u
#define CPSW3G_STAT_CLEAR                   0xFFFFFFFFu


#define NET_CH_DIR_TX                       0u
#define NET_CH_DIR_RX                       1u
#define CONFIG_CPGMAC_NOPHY                 9999u

/* ALE_DUMP_PRINT macro should be mapped to cpsw3g_Printf ideally */
#define ALE_DUMP_PRINT                      printf

/**
 * \defgroup EMACPktFlags Packet Buffer Flags set in Flags
 *
 *  @{
 */
#define EMAC_PKT_FLAGS_SOP                  0x80000000u
/**< Start of packet                                                        */
#define EMAC_PKT_FLAGS_EOP                  0x40000000u
/**< End of packet                                                          */
/* @} */

/**
 *  \defgroup   RecvPktFlags    Receive Packet flags
 *
 * The Following Packet flags are set in Flags on RX packets only
 *
 * @{
 */
#define EMAC_PKT_FLAGS_HASCRC               0x04000000u
/**< RxCrc: PKT has 4byte CRC                                               */
#define EMAC_PKT_FLAGS_JABBER               0x02000000u
/**< RxErr: Jabber                                                          */
#define EMAC_PKT_FLAGS_OVERSIZE             0x01000000u
/**< RxErr: Oversize                                                        */
#define EMAC_PKT_FLAGS_FRAGMENT             0x00800000u
/**< RxErr: Fragment                                                        */
#define EMAC_PKT_FLAGS_UNDERSIZED           0x00400000u
/**< RxErr: Undersized                                                      */
#define EMAC_PKT_FLAGS_CONTROL              0x00200000u
/**< RxCtl: Control Frame                                                   */
#define EMAC_PKT_FLAGS_OVERRUN              0x00100000u
/**< RxErr: Overrun                                                         */
#define EMAC_PKT_FLAGS_CODEERROR            0x00080000u
/**< RxErr: Code Error                                                      */
#define EMAC_PKT_FLAGS_ALIGNERROR           0x00040000u
/**< RxErr: Alignment Error                                                 */
#define EMAC_PKT_FLAGS_CRCERROR             0x00020000u
/**< RxErr: Bad CRC                                                         */
#define EMAC_PKT_FLAGS_NOMATCH              0x00010000u
/**< RxPrm: No Match                                                        */
/* @} */

/**
 *  \defgroup   macModeFlags/dmaModeFlags Configuration Mode Flags
 *
 * @{
 */
#define EMAC_CONFIG_MODEFLG_CHPRIORITY      0x00001
/**< Use Tx channel priority                                                */
#define EMAC_CONFIG_MODEFLG_MACLOOPBACK     0x00002
/**< MAC internal loopback                                                  */
#define EMAC_CONFIG_MODEFLG_RXCRC           0x00004
/**< Include CRC in RX frames                                               */
#define EMAC_CONFIG_MODEFLG_TXCRC           0x00008
/**< Tx frames include CRC                                                  */
#define EMAC_CONFIG_MODEFLG_PASSERROR       0x00010
/**< Pass error frames                                                      */
#define EMAC_CONFIG_MODEFLG_PASSCONTROL     0x00020
/**< Pass control frames                                                    */
#define EMAC_CONFIG_MODEFLG_PASSALL         0x00040
/**< pass all frames                                                        */
#define EMAC_CONFIG_MODEFLG_RXQOS           0x00080
/**< Enable QOS at receive side                                             */
#define EMAC_CONFIG_MODEFLG_RXNOCHAIN       0x00100
/**< Select no buffer chaining                                              */
#define EMAC_CONFIG_MODEFLG_RXOFFLENBLOCK   0x00200
/**< Enable offset/length blocking                                          */
#define EMAC_CONFIG_MODEFLG_RXOWNERSHIP     0x00400
/**< Use ownership bit as 1                                                 */
#define EMAC_CONFIG_MODEFLG_RXFIFOFLOWCNTL  0x00800
/**< Enable rx fifo flow control                                            */
#define EMAC_CONFIG_MODEFLG_CMDIDLE         0x01000
/**< Enable IDLE command                                                    */
#define EMAC_CONFIG_MODEFLG_TXSHORTGAPEN    0x02000
/**< Enable tx short gap                                                    */
#define EMAC_CONFIG_MODEFLG_TXPACE          0x04000
/**< Enable tx pacing                                                       */
#define EMAC_CONFIG_MODEFLG_TXFLOWCNTL      0x08000
/**< Enable tx flow control                                                 */
#define EMAC_CONFIG_MODEFLG_RXBUFFERFLOWCNTL    0x10000
/**< Enable rx buffer flow control                                          */
#define EMAC_CONFIG_MODEFLG_FULLDUPLEX      0x20000
/**< Set full duplex mode                                                   */
#define EMAC_CONFIG_MODEFLG_GIGABIT         0x40000
/**< Set gigabit                                                            */
#define EMAC_CONFIG_MODEFLG_EXTEN           0x80000
/**< Set external enable bit                                                */
#define EMAC_CONFIG_MODEFLG_RMII            0x100000
/**< Enable RMII                                                            */
#define EMAC_CONFIG_MODEFLG_IFCTLA          0x200000
/**< Enable IFCTL_A                                                         */
#define EMAC_CONFIG_MODEFLG_IFCTLB          0x400000
/**< Enable IFCTL_B                                                         */

/**
 *  \defgroup   aleModeFlags Configuration Flags for ALE
 *
 * @{
 */
#define EMAC_CONFIG_ALE_ENABLE              0x00001
/**< Enable ALE                                                             */
#define EMAC_CONFIG_ALE_CLRTABLE            0x00002
/**< Clear ALE Address table.                                               */
#define EMAC_CONFIG_ALE_AGEOUTNOW           0x00004
/**< Age out address table now                                              */
#define EMAC_CONFIG_ALE_LEARNNOVID          0x00008
/**< Learn no VID                                                           */
#define EMAC_CONFIG_ALE_ENVID0MODE          0x00010
/**< Enable VLAN ID = 0 Mode                                                */
#define EMAC_CONFIG_ALE_ENOUIDENY           0x00020
/**< Enable OUI Deny Mode                                                   */
#define EMAC_CONFIG_ALE_ALEBYPASS           0x00040
/**< ALE bypass mode                                                        */
#define EMAC_CONFIG_ALE_RATELIMITTX         0x00080
/**< Rate limit transmit mode                                               */
#define EMAC_CONFIG_ALE_ENVLANAWARE         0x00100
/**< ALE Vlan Aware                                                         */
#define EMAC_CONFIG_ALE_ENAUTHMODE          0x00200
/**< Enable MAC Authorization mode                                          */
#define EMAC_CONFIG_ALE_ENRATELIMIT         0x00400
/**< Enable broadcast and multicast rate limit                              */

/**
 *  \defgroup   cpswCtlModeFlags Configuration Flags for Switch control
 *
 * @{
 */
#define EMAC_CONFIG_CPSW_P2PASSPRITAG       0x00001
/**< Port 2 Pass Priority Tagged                                            */
#define EMAC_CONFIG_CPSW_P1PASSPRITAG       0x00002
/**< Port 1 Pass Priority Tagged                                            */
#define EMAC_CONFIG_CPSW_P0PASSPRITAG       0x00004
/**< Port 0 Pass Priority Tagged                                            */
#define EMAC_CONFIG_CPSW_ENVLANAWARE        0x00008
/**< VLAN aware mode                                                        */
#define EMAC_CONFIG_CPSW_RXVLANENCAP        0x00010
/**< Port 2 VLAN Encapsulation                                              */
#define EMAC_CONFIG_CPSW_P2P2RXFLOWEN       0x00020
/**< Port 2 Tx flow enable from Port 2 Rx FIFO                              */
#define EMAC_CONFIG_CPSW_P2P1TXFLOWEN       0x00040
/**< Port 2 Tx flow enable from Port 1 Tx FIFO                              */
#define EMAC_CONFIG_CPSW_P2P0TXFLOWEN       0x00080
/**< Port 2 Tx flow enable from Port 0 Tx FIFO                              */
#define EMAC_CONFIG_CPSW_P1P2TXFLOWEN       0x00100
/**< Port 1 Rx flow enable from Port 2 Tx FIFO                              */
#define EMAC_CONFIG_CPSW_P1P0TXFLOWEN       0x00200
/**< Port 1 Rx flow enable from Port 0 Tx FIFO                              */
#define EMAC_CONFIG_CPSW_P0P2TXFLOWEN       0x00400
/**< Port 0 Rx flow enable from Port 2 Tx FIFO                              */
#define EMAC_CONFIG_CPSW_P0P1TXFLOWEN       0x00800
/**< Port 0 Rx flow enable from Port 1 Tx FIFO                              */
#define EMAC_CONFIG_CPSW_FIFOLOOPBACK       0x01000
/**< FIFO Loopback mode                                                     */

/**
 *  \defgroup   cpswPtypeModeFlags Configuration Flags for CPSW PTYPE
 *
 * @{
 */
#define EMAC_CONFIG_PTYPE_P2PTYPEESC       0x00001
/**< Port 2 Priority Type escalate                                          */
#define EMAC_CONFIG_PTYPE_P1PTYPEESC       0x00002
/**< Port 1 Priority Type escalate                                          */
#define EMAC_CONFIG_PTYPE_P0PTYPEESC       0x00004
/**< Port 0 Priority Type escalate                                          */

/**
 *  \defgroup   cpswStatModeFlags Configuration Flags for CPSW STAT
 *
 * @{
 */
#define EMAC_CONFIG_STAT_P2STATEN           0x00001
/**< Port 2 statistics enable                                               */
#define EMAC_CONFIG_STAT_P1STATEN           0x00002
/**< Port 1 statistics enable                                               */
#define EMAC_CONFIG_STAT_P0STATEN           0x00004
/**< Port 0 statistics enable                                               */

/**
 *  \defgroup   mdioModeFlags  MDIO Configuration Mode Flags
 *
 *  These flags determine how the MDIO module behaves
 *  @{
 */
#define MDIO_MODEFLG_AUTONEG                0x0001
/**< Use Autonegotiation                                                    */

/* The following bits are used for manual and fallback configuration        */
#define MDIO_MODEFLG_HD10                   0x0002
/**< Use 10Mb/s Half Duplex                                                 */
#define MDIO_MODEFLG_FD10                   0x0004
/**< Use 10Mb/s Full Duplex                                                 */
#define MDIO_MODEFLG_HD100                  0x0008
/**< Use 100Mb/s Half Duplex                                                */
#define MDIO_MODEFLG_FD100                  0x0010
/**< Use 100Mb/s Full Duplex                                                */
#define MDIO_MODEFLG_FD1000                 0x0020
/**< Use 1000Mb/s Full Duplex                                               */
#define MDIO_MODEFLG_LOOPBACK               0x0040
/**< Use internal PHY Loopback                                              */
#define MDIO_MODEFLG_NOPHY                  0x0080
/**< Use external SMA connectors instead of PHY                             */


/* @} */

/**
 *  \defgroup   Descriptor memory selection Flags
 *
 * @{
 */
#define EMAC_DESC_BASE_L2                   0x00001
/**< Use L2 as Descriptor memory                                            */
#define EMAC_DESC_BASE_CPPI                 0x00002
/**< Use CPPI RAM as desriptor memory                                       */
#define EMAC_DESC_BASE_DDR                  0x00004
/**< Use DDR as descriptor memory                                           */


#define EMAC_TEARDOWN_CHANNEL(x)            (1 << x)
/**< Macro to tear down selective Rx/Tx channels                            */

/**
 *  \defgroup   PktFiltering Packet Filtering
 *
 *  Packet Filtering Settings (cumulative)
 *
 *  @{
 */
#define EMAC_RXFILTER_NOTHING               0
/**< Receive filter set to Nothing                                          */
#define EMAC_RXFILTER_DIRECT                1
/**< Receive filter set to Direct                                           */
#define EMAC_RXFILTER_BROADCAST             2
/**< Receive filter set to Broadcast                                        */
#define EMAC_RXFILTER_MULTICAST             3
/**< Receive filter set to Multicast                                        */
#define EMAC_RXFILTER_ALLMULTICAST          4
/**< Receive filter set to All Mcast                                        */
#define EMAC_RXFILTER_ALL                   5
/**< Receive filter set to All                                              */
/* @} */

/**
 *  \defgroup   ErrCodes    STANDARD ERROR CODES
 *  @{
 */
/** Internal functions return error codes */
#define CPSW3G_INSTANCE_CODE                        0u
#define CPSW3G_ERROR_BASE                           (0x200001Fu)
#define CPSW3G_ERROR_CODE                           ((CPSW3G_ERROR_BASE | (CPSW3G_INSTANCE_CODE << 16)))
#define CPSW3G_ERROR_INFO                           (CPSW3G_ERROR_CODE)
#define CPSW3G_ERROR_WARNING                        (CPSW3G_ERROR_CODE | 0x10000000u)
#define CPSW3G_ERROR_MINOR                          (CPSW3G_ERROR_CODE | 0x20000000u)
#define CPSW3G_ERROR_MAJOR                          (CPSW3G_ERROR_CODE | 0x30000000u)
#define CPSW3G_ERROR_CRITICAL                       (CPSW3G_ERROR_CODE | 0x40000000u)

/* CPMAC Success code */
#define CPSW3G_SUCCESS                              0u

/* CPSW3G Error codes */
#define CPSW3G_ERR_DEV_ALREADY_INSTANTIATED(instID) (0x30000000u + CPSW3G_ERROR_BASE + ((instId) << 16) )
/**< Device with same instance ID already created.                          */
#define CPSW3G_ERR_DEV_NOT_INSTANTIATED             (CPSW3G_ERROR_MAJOR + 1u)
/**< Device is not instantiated yet.                                        */
#define CPSW3G_INVALID_PARAM                        (CPSW3G_ERROR_MAJOR + 2u)
/**< Function or calling parameter is invalid                               */
#define CPSW3G_ERR_CH_INVALID                       (CPSW3G_ERROR_CRITICAL + 3u)
/**< Channel number invalid                                                 */
#define CPSW3G_ERR_CH_ALREADY_INIT                  (CPSW3G_ERROR_MAJOR + 4u)
/**< Channel already initialized and setup                                  */
#define CPSW3G_ERR_TX_CH_ALREADY_CLOSED             (CPSW3G_ERROR_MAJOR + 5u)
/**< Tx Channel already  closed. Channel close failed                       */
#define CPSW3G_ERR_TX_CH_NOT_OPEN                   (CPSW3G_ERROR_MAJOR + 6u)
/**< Tx Channel not open.                                                   */
#define CPSW3G_ERR_TX_NO_LINK                       (CPSW3G_ERROR_MAJOR + 7u)
/**< Tx Link not up.                                                        */
#define CPSW3G_ERR_TX_OUT_OF_BD                     (CPSW3G_ERROR_MAJOR + 8u)
/**< Tx ran out of Buffer descriptors to use.                               */
#define CPSW3G_ERR_RX_CH_INVALID                    (CPSW3G_ERROR_CRITICAL + 9u)
/**< Rx Channel invalid number.                                             */
#define CPSW3G_ERR_RX_CH_ALREADY_INIT               (CPSW3G_ERROR_MAJOR + 10u)
/**< Rx Channel already setup.                                              */
#define CPSW3G_ERR_RX_CH_ALREADY_CLOSED             (CPSW3G_ERROR_MAJOR + 11u)
/**< Rx Channel already closed. Channel close failed.                       */
#define CPSW3G_ERR_RX_CH_NOT_OPEN                   (CPSW3G_ERROR_MAJOR + 12u)
/**< Rx Channel not open yet.                                               */

#define CPSW3G_ERR_DEV_ALREADY_CREATED              (CPSW3G_ERROR_MAJOR + 13u)
/**< EMAC device already created.                                           */
#define CPSW3G_ERR_DEV_NOT_OPEN                     (CPSW3G_ERROR_MAJOR + 14u)
/**< Device is not open or not ready                                        */
#define CPSW3G_ERR_DEV_ALREADY_CLOSED               (CPSW3G_ERROR_MAJOR + 15u)
/**< Device close failed. Device already closed.                            */
#define CPSW3G_ERR_DEV_ALREADY_OPEN                 (CPSW3G_ERROR_MAJOR + 16u)
/**< Device open failed. Device already open.                               */
#define CPSW3G_ERR_RX_BUFFER_ALLOC_FAIL             (CPSW3G_ERROR_CRITICAL +17u)
/**< Rx Buffer Descriptor allocation failed.                                */
#define CPSW3G_INTERNAL_FAILURE                     (CPSW3G_ERROR_MAJOR + 18u)
/**< EMAC Internal failure.                                                 */
#define CPSW3G_VLAN_UNAWARE_MODE                    (CPSW3G_ERROR_MAJOR + 19u)
/**< VLAN support not enabled in EMAC                                       */
#define CPSW3G_ALE_TABLE_FULL                       (CPSW3G_ERROR_MAJOR + 20u)
/**< ALE Table full.                                                        */
#define CPSW3G_ADDR_NOTFOUND                        (CPSW3G_ERROR_MAJOR + 21u)
/**< Multicast/Unicast/OUI Address not found in ALE.                        */
#define CPSW3G_INVALID_VLANID                       (CPSW3G_ERROR_MAJOR + 22u)
/**< Invalid VLAN Id.                                                       */
#define CPSW3G_INVALID_PORT                         (CPSW3G_ERROR_MAJOR + 23u)
/**< Invalid Port Specified.                                                */
#define CPSW3G_BD_ALLOC_FAIL                        (CPSW3G_ERROR_MAJOR + 24u)
/**< Buffer Descriptor Allocation failure. OOM                              */
#define CPSW3G_ERR_BADPACKET                        (CPSW3G_ERROR_MAJOR + 25u)
/**< Supplied packet was invalid                                            */
#define CPSW3G_ERR_MACFATAL                         (CPSW3G_ERROR_CRITICAL + 26u)
/**< Fatal Error - EMAC_close() required                                    */
/* @} */

#define EMAC_DEVMAGIC                       0x0aceface
/**< Device Magic number                                                    */
#define EMAC_NUMSTATS                       36
/**< Number of statistics regs                                              */

#ifdef USE_PHY1
	#define EMAC_NUMCORES                       2
#else
	#define EMAC_NUMCORES                       1
#endif
/**< Number of cores                                                        */

/* Control Codes for internal control function*/
#define CPSW3G_PHY_STATUS_UPDATE			0u

/* @} */

/**
 *  \defgroup   EMACObjects   EMAC Objects
 *  @{
 */

/**
 *  \brief  EMAC_Pkt
 *
 *  The packet structure defines the basic unit of memory used to hold data
 *  packets for the EMAC device.
 *
 *  A packet is comprised of one or more packet buffers. Each packet buffer
 *  contains a packet buffer header, and a pointer to the buffer data.
 *  The EMAC_Pkt structure defines the packet buffer header.
 *
 *  The pDataBuffer field points to the packet data. This is set when the
 *  buffer is allocated, and is not altered.
 *
 *  BufferLen holds the the total length of the data buffer that is used to
 *  store the packet (or packet fragment). This size is set by the entity
 *  that originally allocates the buffer, and is not altered.
 *
 *  The Flags field contains additional information about the packet
 *
 *  ValidLen holds the length of the valid data currently contained in the
 *  data buffer.
 *
 *  DataOffset is the byte offset from the start of the data buffer to the
 *  first byte of valid data. Thus (ValidLen+DataOffet)<=BufferLen.
 *
 *  Note that for receive buffer packets, the DataOffset field may be
 *  assigned before there is any valid data in the packet buffer. This allows
 *  the application to reserve space at the top of data buffer for private
 *  use. In all instances, the DataOffset field must be valid for all packets
 *  handled by EMAC.
 *
 *  The data portion of the packet buffer represents a packet or a fragment
 *  of a larger packet. This is determined by the Flags parameter. At the
 *  start of every packet, the SOP bit is set in Flags. If the EOP bit is
 *  also set, then the packet is not fragmented. Otherwise; the next packet
 *  structure pointed to by the pNext field will contain the next fragment in
 *  the packet. On either type of buffer, when the SOP bit is set in Flags,
 *  then the PktChannel, PktLength, and PktFrags fields must also be valid.
 *  These fields contain additional information about the packet.
 *
 *  The PktChannel field detetmines what channel the packet has arrived on,
 *  or what channel it should be transmitted on. The EMAC library supports
 *  only a single receive channel, but allows for up to eight transmit
 *  channels. Transmit channels can be treated as round-robin or priority
 *  queues.
 *
 *  The PktLength field holds the size of the entire packet. On single frag
 *  packets (both SOP and EOP set in BufFlags), PktLength and ValidLen will
 *  be equal.
 *
 *  The PktFrags field holds the number of fragments (EMAC_Pkt records) used
 *  to describe the packet. If more than 1 frag is present, the first record
 *  must have EMAC_PKT_FLAGS_SOP flag set, with corresponding fields validated.
 *  Each frag/record must be linked list using the pNext field, and the final
 *  frag/record must have EMAC_PKT_FLAGS_EOP flag set and pNext=0.
 *
 *  In systems where the packet resides in cacheable memory, the data buffer
 *  must start on a cache line boundary and be an even multiple of cache
 *  lines in size. The EMAC_Pkt header must not appear in the same cache line
 *  as the data portion of the packet. On multi-fragment packets, some packet
 *  fragments may reside in cacheable memory where others do not.
 *
 *  @verbatim
    <b> NOTE: It is up to the caller to assure that all packet buffers
    residing in cacheable memory are not currently stored in L1 or L2
    cache when passed to any EMAC function. </b>
    @endverbatim
 *
 *  Some of the packet Flags can only be set if the device is in the
 *  proper configuration to receive the corresponding frames. In order to
 *  enable these flags, the following modes must be set:
 *        RxCrc Flag  : RXCRC Mode in Cpsw3gInitConfig
 *        RxErr Flags : PASSERROR Mode in Cpsw3gInitConfig
 *        RxCtl Flags : PASSCONTROL Mode in Cpsw3gInitConfig
 *        RxPrm Flag  : EMAC_RXFILTER_ALL in EMAC_setReceiveFilter()
 *
 */
typedef struct _EMAC_Pkt {
    Uint32           AppPrivate;
    /**< For use by the application                                         */
    struct _EMAC_Pkt *pPrev;
    /**< Previous record                                                    */
    struct _EMAC_Pkt *pNext;
    /**< Next record                                                        */
    Uint8            *pDataBuffer;
    /**< Pointer to Data Buffer (read only)                                 */
    Uint32           BufferLen;
    /**< Physical Length of buffer (read only)                              */
    Uint32           Flags;
    /**< Packet Flags                                                       */
    Uint32           ValidLen;
    /**< Length of valid data in buffer                                     */
    Uint32           DataOffset;
    /**< Byte offset to valid data                                          */
    Uint32           PktChannel;
    /**< Tx/Rx Channel/Priority 0-7 (SOP only)                              */
    Uint32           PktLength;
    /**< Length of Packet (SOP only) (same as ValidLen on single frag Pkt)  */
    Uint32           PktFrags;
    /**< No of frags in packet (SOP only) frag is EMAC_Pkt record-normally 1*/
} EMAC_Pkt;

/**
 *  \brief Packet Queue
 *
 * We keep a local packet queue for transmit and receive packets.
 * The queue structure is OS independent.
 */
typedef struct _pktq {
   Uint32             depth;
   /**< Number of packets in queue                                           */
   EMAC_Desc          *head;
   /**< Pointer to first packet                                              */
   EMAC_Desc          *tail;
   /**< Pointer to last packet                                               */

} PKTQ;

/**
 *  \brief Channel Info structure
 *
 */
typedef struct
{
    Uint32	        chNum;
    /**< Channel Number                                                     */
    Uint32          chDir;
    /**< Channel Direction                                                  */
    Uint32          numBD;
    /**< Maximum Number of Buffer Descriptors allocated for this channel    */
    Uint32          bufSize;
    /**< Buffer size                                                        */
    Uint32          TxChanEnable;
    /**< Which specific Tx Channels(0-7) to use
      * if TxChannels = 0 does not allocate the Tx channels for the core
      * if TxChannels = 1, 2, 4, 8, ... allocates which specific TxChannels to use
	  * 0x01: channel 0, 0x02: channel 1, 0x04: channel 2, 0x80: channel 7
      * User has to take care of allocating a portion from total allocation
      * for the cores */
    Uint32          RxChanEnable;
    /**< Which specific Rx Channels(0-7) to use
      *  if RxChannel = 0 does not allocate the Rx channel for the core
      *  if RxChannel = 1, 2,4,8,... which specific channel to use for the core
	  * 0x01: channel 0, 0x02: channel 1, 0x04: channel 2, 0x80: channel 7 */

}Cpsw3gChInfo;


/**
 *  \brief Transmit/Receive Descriptor Channel Structure
 *
 *  (One receive and up to 8 transmit in this example)
 */
typedef struct _EMAC_DescCh {
    struct _Cpsw3gDevice *pd;
    /**< Pointer to parent structure                                        */
    Uint32          ChannelIndex;
    /**< Channel index 0-7                                                  */
    PKTQ            DescHwQ;
    /**< Packets queued as desc                                             */
    PKTQ            DescFreeQ;
    /**< Packets waiting for TX desc                                        */
    Cpsw3gChInfo    *chInfo;
    /**< Channel info                                                       */
    Uint32          DescMax;
    /**< Max number of desc (buffs)                                         */
    Uint32          DescCount;
    /**< Current number of desc                                             */
    EMAC_Desc       *pDescFirst;
    /**< First desc location                                                */
    EMAC_Desc       *pDescLast;
    /**< Last desc location                                                 */
    EMAC_Desc       *pDescRead;
    /**< Location to read next desc                                         */
    EMAC_Desc       *pDescWrite;
    /**< Location to write next desc                                        */
} EMAC_DescCh;

typedef EMAC_DescCh Cpsw3gTxCppiCh;
typedef EMAC_DescCh Cpsw3gRxCppiCh;

typedef enum
{
        BROADCAST = 0,
        MULTICAST,
        UNICAST
}typeOfData;


/**
 *  \brief Port structure
 *
 */
typedef struct
{
  Uint32            portNum;
  /**< Port number 0-2                                                  */
  Uint32            chNum;
  /**< Ch number 0-7 (currently unused - may not need it)               */
} Cpsw3gPort;

/**
 *  \brief  Gigabit MAC Info Instance Structure
 *
 */
typedef struct CpgmacObj_t
{
    PHY_DEVICE      PhyDev;
    /**< MII-MDIO module device structure                               */
    Uint32          portState;
    /**< State of each port                                             */

    /* Status related fields */
    Uint32          PhyLinked;
    /**< Link status: 1=Linked, 0=No link                               */
    Uint32          PhyDuplex;
    /**< Duplex status: 1=Full Duplex, 0=Half Duplex                    */
    Uint32          PhySpeed;
    /**< Link Speed = 2= 1000 mbps, 1= 100 mbps, 0= 10 mbps             */
    Uint32          PhyNum;
    /**< Phy number - useful if phy number is discovered                */

    /* mode of operation related s/w fields */
    Bool	        intPhy;
    /**< Flag to indicate if this is an internal PHY                    */
    Bool	        promiscousEnable;
    /**< Flag to indicate if Promiscuous mode is enabled on this port   */
    Bool	        broadcastEnable;
    /**< Flag to indicate if Broadcast is enabled on this port          */
    Bool	        multicastEnable;
    /**< Flag to indicate if Multicast is enabled on this port          */
}CpgmacObj;


/**
 *  \brief  CPDMA CPPI 3.0 Compliant Interface Instance Structure
 *
 */
typedef struct CpdmaObj_t
{
    Cpsw3gTxCppiCh  txCppi[CPDMA_MAX_CHANNELS];
    /**< Tx Control struct ptrs                                         */
    Cpsw3gRxCppiCh  rxCppi[CPDMA_MAX_CHANNELS];
    /**< Rx Control struct ptrs                                         */
    Bool            chIsInit[CPDMA_MAX_DIR][CPDMA_MAX_CHANNELS];
    /**< channel initialized ?                                          */
    Bool            chIsOpen[CPDMA_MAX_DIR][CPDMA_MAX_CHANNELS];
    /**< channel open ?                                                 */
    Bool            tdPending[CPDMA_MAX_DIR][CPDMA_MAX_CHANNELS];
    /**< teardown pending ?                                             */
    Int             txIntThreshold[CPDMA_MAX_CHANNELS];
    /**< TX  Completion Threshold count                                 */
    Uint32          portState;
    /**< State of each port                                             */

    /* Status related fields */
    Uint32          hwStatus;
    /**< Either NO_ERROR or combination of error bits from above status codes */
    Uint32          hwErrInfo;
    /**< If error, contains DMASTATUS register contents                 */
}CpdmaObj;

/**
 *  \brief  EMAC_Statistics
 *
 *  The statistics structure is the used to retrieve the current count
 *  of various packet events in the system. These values represent the
 *  delta values from the last time the statistics were read.
 */
typedef struct _EMAC_Statistics {
    Uint32 RxGoodFrames;     /**< Good Frames Received                      */
    Uint32 RxBCastFrames;    /**< Good Broadcast Frames Received            */
    Uint32 RxMCastFrames;    /**< Good Multicast Frames Received            */
    Uint32 RxPauseFrames;    /**< PauseRx Frames Received                   */
    Uint32 RxCRCErrors;      /**< Frames Received with CRC Errors           */
    Uint32 RxAlignCodeErrors;/**< Frames Received with Alignment/Code Errors*/
    Uint32 RxOversized;      /**< Oversized Frames Received                 */
    Uint32 RxJabber;         /**< Jabber Frames Received                    */
    Uint32 RxUndersized;     /**< Undersized Frames Received                */
    Uint32 RxFragments;      /**< Rx Frame Fragments Received               */
    Uint32 reserved;         /**< Rx Frames Filtered Based on Address       */
    Uint32 reserved2;        /**< Rx Frames Filtered Based on QoS Filtering */
    Uint32 RxOctets;         /**< Total Received Bytes in Good Frames       */
    Uint32 TxGoodFrames;     /**< Good Frames Sent                          */
    Uint32 TxBCastFrames;    /**< Good Broadcast Frames Sent                */
    Uint32 TxMCastFrames;    /**< Good Multicast Frames Sent                */
    Uint32 TxPauseFrames;    /**< PauseTx Frames Sent                       */
    Uint32 TxDeferred;       /**< Frames Where Transmission was Deferred    */
    Uint32 TxCollision;      /**< Total Frames Sent With Collision          */
    Uint32 TxSingleColl;     /**< Frames Sent with Exactly One Collision    */
    Uint32 TxMultiColl;      /**< Frames Sent with Multiple Colisions       */
    Uint32 TxExcessiveColl;  /**< Tx Frames Lost Due to Excessive Collisions*/
    Uint32 TxLateColl;       /**< Tx Frames Lost Due to a Late Collision    */
    Uint32 TxUnderrun;       /**< Tx Frames Lost with Tx Underrun Error     */
    Uint32 TxCarrierSLoss;   /**< Tx Frames Lost Due to Carrier Sense Loss  */
    Uint32 TxOctets;         /**< Total Transmitted Bytes in Good Frames    */
    Uint32 Frame64;          /**< Total Tx&Rx with Octet Size of 64         */
    Uint32 Frame65t127;      /**< Total Tx&Rx with Octet Size of 65 to 127  */
    Uint32 Frame128t255;     /**< Total Tx&Rx with Octet Size of 128 to 255 */
    Uint32 Frame256t511;     /**< Total Tx&Rx with Octet Size of 256 to 511 */
    Uint32 Frame512t1023;    /**< Total Tx&Rx with Octet Size of 512 to 1023*/
    Uint32 Frame1024tUp;     /**< Total Tx&Rx with Octet Size of >=1024     */
    Uint32 NetOctets;        /**< Sum of all Octets Tx or Rx on the Network */
    Uint32 RxSOFOverruns;    /**< Total Rx Start of Frame Overruns          */
    Uint32 RxMOFOverruns;    /**< Total Rx Middle of Frame Overruns         */
    Uint32 RxDMAOverruns;    /**< Total Rx DMA Overruns                     */
} EMAC_Statistics;


/**
 *  \brief DMA Net Status
 *
 */
typedef struct _CpdmaNetStatus
{
  Bool   rxPending;  /**< rx packets pending? */
  Bool   txPending;  /**< tx packets pending? */
  Bool   errPending; /**< host DMA error occurred */
  Bool   statPending; /**<Stats Pending Interrupt */

  Uint32 rxChannel;  /**< channel needing service for Rx */
  Uint32 txChannel;  /**< channel needing service for Tx */
} CpdmaNetStatus;

/**
 *  \brief MAC addresses configuration Structure
 *
 */
typedef struct _EMAC_AddrConfig {
	Uint8 ChannelNum;
	/* Receive Channel number to which the MAC address to be assigned */
	Uint8 Addr[6];
	/* MAC address specific to channel */
} EMAC_AddrConfig;

/**
 *  \brief CPGMAC SL Init Configuration
 *
 *  Configuration information provided during initialization.
 *  The config info can come from various sources - static compiled in info,
 *  boot time (Flash) info etc. Currently static compiled values supported.
 */
typedef struct
{
  Uint32      PhyMask;
  /**< Phy Mask for this CPMAC Phy                                          */
  Uint32      MLinkMask;
  /**< MLink Mask for this CPMAC Phy                                        */
  Uint32      phyMode;
  /**< Phy mode settings - Speed,Duplex                                     */
  Uint8       portPri;
  /**< Port VLAN priority (7 is highest)                                    */
  Bool        portCfi;
  /**< Port CFI bit                                                         */
  Uint32      portVID;
  /**< Port VLAN ID                                                         */
  Uint32      macModeFlags;
  /**< Configuation Mode Flags                                              */
  Uint32      mdioModeFlags;
  /**< MDIO configuration flags for this MAC port                           */
} CpgmacMacConfig;

/**
 *  \brief CPGMAC_SL PHY Status values
 *
 *  Provides PHY status of the device.
 *
 */
typedef struct
{
  Uint32      PhyLinked;
  /**< Link status: 1=Linked, 0=No link                                     */
  Uint32      PhyDuplex;
  /**< Duplex status: 1=Full Duplex, 0=Half Duplex                          */
  Uint32      PhySpeed;
  /**< Link Speed = 2= 1000 mbps, 1= 100 mbps, 0= 10 mbps                   */
  Uint32      PhyNum;
  /**< Phy number - useful if phy number is discovered                      */
} CpgmacMacStatus;


/**
 *  \brief CPDMA Init Configuration
 *
 *  Configuration information provided during initialization.
 *  The config info can come from various sources - static compiled in info,
 *  boot time (Flash) info etc. Currently static compiled values supported.
 */
typedef struct
{
  Uint8         portPri;
  /**< Port VLAN priority (7 is highest)                                    */
  Bool          portCfi;
  /**< Port CFI bit                                                         */
  Uint32        portVID;
  /**< Port CFI bit                                                         */
  Uint32        rxBufferOffset;
  /**< Receive buffer offset                                                */
  Uint32        numChannels[CPDMA_MAX_DIR];
  /**< Number of Channels supported in each dir                             */

  /**Register control fields                                                */
  /* DMA Control register                                                   */
   Uint32       dmaModeFlags;
  /**< Configuation Mode Flags                                              */

  /* CPDMA_RX_CH_MAP Register: Host ch mapping only used when in bypass mode */
  Uint8         hostRxCh_gmac0Pri0;
  /**< Receive pkts from SL0 w/pri 0x0 on this ch                           */
  Uint8         hostRxCh_gmac0Pri1;
  /**< Receive pkts from SL0 w/pri 0x1 on this ch                           */
  Uint8         hostRxCh_gmac0Pri2;
  /**< Receive pkts from SL0 w/pri 0x2 on this ch                           */
  Uint8         hostRxCh_gmac0Pri3;
  /**< Receive pkts from SL0 w/pri 0x3 on this ch                           */
  Uint8         hostRxCh_gmac1Pri0;
  /**< Receive pkts from SL1 w/pri 0x0 on this ch                           */
  Uint8         hostRxCh_gmac1Pri1;
  /**< Receive pkts from SL1 w/pri 0x1 on this ch                           */
  Uint8         hostRxCh_gmac1Pri2;
  /**< Receive pkts from SL1 w/pri 0x2 on this ch                           */
  Uint8         hostRxCh_gmac1Pri3;
  /**< Receive pkts from SL1 w/pri 0x3 on this ch                           */
} CpdmaConfig;

/**
 *  \brief ALE configuration
 *
 *  This data structure contains configuration items related to the
 *  ALE_CONTROL register fields.
 */
typedef struct
{
  /* ALE Control fields                                                     */
  Uint32        aleModeFlags;

  /* ALE Prescale register                                                  */
  Uint32        alePrescale;
  /**< ALE Prescale                                                         */

  /* ALE Unknown VLAN register                                              */
  Uint8         unknownForceUntaggedEgress;
  /**< Unknown VLAN Force Untagged Egress (except on port 2)                */
  Uint8         unknownRegMcastFloodMask;
  /**< Unknown VLAN Registered Multicast Flood Mask                         */
  Uint8         unknownMcastFloodMask;
  /**< Unknown VLAN Multicast Flood Mask                                    */
  Uint8         unknownVlanMemberList;
  /**< Unknown VLAN Member List                                             */
} AleConfig;

/**
 *  \brief ALE Port configuration
 *
 *  This data structure contains configuration items related to the
 *  ALE_Port_Control register fields.
 */
typedef struct
{
  Uint8         bcastLimit;
  /**< Broadcast Packet Rate Limit                                          */
  Uint8         mcastLimit;
  /**< Multicast Packet Rate Limit                                          */
  Bool          noLearn;
  /**< No Learn mode                                                        */
  Bool          vidIngressCheck;
  /**< VLAN ID Ingress Check                                                */
  Bool          dropUntagged;
  /**< Drop untagged packets                                                */
  Uint8         portState;
  /**< Port State: 0-disabled, 1-blocked, 2-learn, 3-forward                */
} AlePortConfig;

/**
 *  \brief CPSW configuration
 *
 *  This data structure contains configuration items related to the
 *  CPSW_CONTROL and PTYPE register fields.
 */
typedef struct
{
  /* CPSW_CONTROL                                                           */
  Uint32        cpswCtlModeFlags;
  /**< Configuration Mode flags for Switch control                          */

  /* PTYPE register                                                         */
  Uint32        cpswPtypeModeFlags;
  /**< Configuration Mode flags for Switch PTYPE                            */
  Uint8         escPriLdVal;
  /**< Escalate priority load value                                         */

  /* Stat_Port_en register                                                  */
  Uint32        cpswStatModeFlags;
  /**< Configuration Mode flags for Switch Stat                             */

  /* Misc                                                                   */
  Uint32        cpmacBusFrequency;
  /**< Bus frequency at which this module is operating                      */
  Uint32        mdioBusFrequency;
  /**< Bus frequency for the MII module                                     */
  Uint32        mdioClockFrequency;
  /**< Clock frequency for MDIO link                                        */
  Uint32        mdioTickMSec;
  /**<  MDIO Tick count in milliSeconds                                     */
} Cpsw3gConfig;

/**
 *  \brief  EMAC_Status
 *
 *  The status structure contains information about the MAC's run-time
 *  status.
 *
 *  The following is a short description of the configuration fields:
 *
 *  MdioLinkStatus - Current link stat (non-zero on link; see CSL_MDIO.H)
 *
 *  PhyDev         - Current PHY device in use (0-31)
 *
 *  RxPktHeld      - Current number of Rx packets held by the EMAC device
 *
 *  TxPktHeld      - Current number of Tx packets held by the EMAC device
 *
 *  FatalError     - Fatal Error Code (TBD)
 */
typedef struct _EMAC_Status {
    CpdmaNetStatus  DmaStatus;

    CpgmacMacStatus MacStatus[CPSW3G_NUM_MAC_PORTS];

} EMAC_Status;


/**
 *  \brief  Cpsw3gInitConfig
 *
 *  The config structure defines how the EMAC device should operate. It is
 *  passed to the device when the device is opened, and remains in effect
 *  until the device is closed.
 *
 *  The following is a short description of the configuration fields:
 *
 *  UseMdio      - Uses MDIO configuration if required. In case of SGMII
 *                 MAC to MAC communication MDIO is not required. If this
 *                 field is one (1) configures MDIO
 *                          zero (0) does not configure MDIO
 *
 *  ModeFlags    - Specify the Fixed Operating Mode of the Device:
 *      - EMAC_CONFIG_MODEFLG_CHPRIORITY  - Treat TX channels as Priority Levels
 *                                   (Channel 7 is highest, 0 is lowest)
 *      - EMAC_CONFIG_MODEFLG_MACLOOPBACK - Set MAC in Internal Loopback for
 *                                          Testing
 *      - EMAC_CONFIG_MODEFLG_RXCRC       - Include the 4 byte EtherCRC in RX
 *                                          frames
 *      - EMAC_CONFIG_MODEFLG_TXCRC       - Assume TX Frames Include 4 byte
 *                                          EtherCRC
 *      - EMAC_CONFIG_MODEFLG_PASSERROR   - Receive Error Frames for Testing
 *      - EMAC_CONFIG_MODEFLG_PASSCONTROL - Receive Control Frames for
 *                                          Testing
 *
 *  MdioModeFlags - Specify the MDIO/PHY Operation (See csl_MDIO.H)
 *
 *  TxChannels    - Number of TX Channels to use (1-8, usually 1)
 *
 *  MacAddr       - Device MAC address
 *
 *  RxMaxPktPool  - Max Rx packet buffers to get from pool
 *                  (Must be in the range of 8 to 192)
 *
 *  A list of callback functions is used to register callback functions with
 *  a particular instance of the EMAC peripheral. Callback functions are
 *  used by EMAC to communicate with the application. These functions are
 *  REQUIRED for operation. The same callback table can be used for multiple
 *  driver instances.
 *
 *  The callback functions can be used by EMAC during any EMAC function, but
 *  mostly occur during calls to EMAC_statusIsr() and EMAC_statusPoll().
 *
 *  <b>pfcbGetPacket </b> -  Called by EMAC to get a free packet buffer from
 *                   the application layer for receive data. This function
 *                   should return NULL is no free packets are available.
 *                   The size of the packet buffer must be large enough
 *                   to accommodate a full sized packet (1514 or 1518
 *                   depending on the EMAC_CONFIG_MODEFLG_RXCRC flag), plus
 *                   any application buffer padding (DataOffset).
 *
 *  <b>pfcbFreePacket </b> - Called by EMAC to give a free packet buffer back
 *                   to the application layer. This function is used to
 *                   return transmit packets. Note that at the time of the
 *                   call, structure fields other than pDataBuffer and
 *                   BufferLen are in an undefined state.
 *
 *  <b>pfcbRxPacket </b>   - Called to give a received data packet to the
 *                   application layer. The applicaiton must accept the packet.
 *                   When the application is finished with the packet, it
 *                   can return it to its own free queue. This function also
 *                   returns a pointer to a free packet to replace the received
 *                   packet on the EMAC free list. It returns NULL when no free
 *                   packets are available. The return packet is the same as
 *                   would be returned by pfcbGetPacket. Thus if a newly
 *                   received packet is not desired, it can simply be returned
 *                   to EMAC via the return value.
 *
 *  <b>pfcbStatus </b>     - Called to indicate to the application that it
 *                   should call EMAC_getStatus() to read the current
 *                   device status. This call is made when device status
 *                   changes.
 *
 *  <b>pfcbStatistics </b> - Called to indicate to the application that it
 *                   should call EMAC_getStatistics() to read the
 *                   current Ethernet statistics. Called when the
 *                   statistic counters are to the point of overflow.
 *
 *  The hApplication calling calling argument is the application's handle
 *  as supplied to the EMAC device in the EMAC_open() function.
 */
typedef struct _Cpsw3gInitConfig {
    Uint8               CoreNum;
    /**< This member is for core selction to does the EMAC configuration
          i.e user can select the specific core to configure EMAC one time  */
    Uint8               DescBase;
    Uint32              PktMTU;
    /**< MTU for the interfaces.                                            */

    /* Port specific items                                                  */
    CpgmacMacConfig     macInitCfg[CPSW3G_NUM_MAC_PORTS];
    /**< CPGMAC SL Init Configuration                                       */
    CpdmaConfig         dmaInitCfg;
    /**< DMA Init Configuration                                             */

    /* ALE specific items                                                   */
    AleConfig           aleCfg;
    /**< ALE Init Configuration                                             */
    AlePortConfig       alePortCfg[CPSW3G_NUM_PORTS];
    /**< ALE Port Configuration                                             */

    /* Switch specific items */
    Cpsw3gConfig        cpswCfg;
    /**< CPSW Control register fields                                       */
    Uint32              Mib64CntMsec;
    /**< This member is for descriptor memory selction to place the EMAC
	     descriptors in CPPI RAM or  L2 RAM or DDR memory                   */

    /* Channel configuration - provision is made for max number of channels */
    Cpsw3gChInfo	    chInfo[CPDMA_MAX_DIR][CPDMA_MAX_CHANNELS];
    /**< Channel configuration                                              */

    Uint32              linkSpeed[CPSW3G_NUM_MAC_PORTS];
    /**<Link Speed                                                          */
    Uint32              linkMode[CPSW3G_NUM_MAC_PORTS];
    /**<Link Mode                                                           */
    Uint32              linkStatus[CPSW3G_NUM_MAC_PORTS];
    /**<Link Status                                                         */
    Uint32		        aleTicks;
    /**< Ticks for this timer                                               */
    Uint32 		        aleTimerActive;
    /**<ALE ageout timer active?                                            */

	Uint8               TotalNumOfMacAddrs;
	/**< Total number of MAC addresses to be assigned for all receive channels */
    EMAC_AddrConfig     MacAddr[2];
    /**< Mac Addresses structure                                            */
    Uint32              RxMaxPktPool;
    /**< Max Rx packet buffers to get from pool                             */
    Uint32              RxFilter;
    /* Receive Filter settings                                              */

//     EMAC_Pkt *          (*pfcbGetPacket)(Handle hApplication);
//     /**< Get packet call back                                               */
//     void                (*pfcbFreePacket)(Handle hApplication, EMAC_Pkt *pPacket);
//     /**< Free packet call back                                              */
//     EMAC_Pkt *          (*pfcbRxPacket)(Handle hApplication, EMAC_Pkt *pPacket);
//     /**< Receive packet call back                                           */
//     void                (*pfcbStatus)(Handle hApplication);
//     /**< Get status call back                                               */
//     void                (*pfcbStatistics)(Handle hApplication);
//     /**< Get statistics call back                                           */


    void	(*pfcbFreePacket)(Handle hApp, Uint32 token, Uint32 flagLen);
    /**< Free packet call back                                              */
    void	(*pfcbRxPacket)(Handle hApp, Uint32 token, Uint32 flagLen);
    /**< Receive packet call back                                           */
    void	(*pfcbStatus)(Handle hApplication, Uint32 status);
    /**< Get status call back                                               */
    void	(*pfcbStatistics)(Handle hApplication);
    /**< Get statistics call back                                           */

} Cpsw3gInitConfig;


/**
 *  \brief  EMAC Main Device Instance Structure
 *
 */
typedef struct _Cpsw3gDevice {
    Uint32              DevMagic;
    /**< Magic ID for this instance                                         */
    Handle              hApplication;
    /**< Calling Application's Handle                                       */
    Uint32              PktMTU;
    /**< Max physical packet size                                           */
    Uint32              FatalError;
    /**< Fatal Error Code                                                   */
    Uint32              Weight;
    /**< Weight for Rx/Tx desc processing                                   */
    CpgmacObj		    cpgmacMac[CPSW3G_NUM_MAC_PORTS];
    /**<CPGMAC MAC Book keeping structures                                  */
    CpdmaObj		    cpdma;
    /**<CPDMA Book keeping structures                                       */
    Cpsw3gPort		    cpsw3gPort[CPSW3G_NUM_PORTS];
    /**<Port Mapping structures                                             */

    Cpsw3gInitConfig    Config;
    /**< Original User Configuration                                        */

    EMAC_Statistics     Stats;
    /**< Current running statistics                                         */
} Cpsw3gDevice;

/* @} */

/**
 *  \defgroup   EMACInterfaces   EMAC Interfaces
 *
 *  Interfaces for EMAC module.
 *
 *  \par
 *  <b> Note: </b>The application is charged with verifying that only
 *  one of the following API calls may be executing at a given time across all
 *  threads and all interrupt functions.
 *  \par
 *  Hence, when used in an multitasking environment, no EMAC function may be
 *  called while another EMAC function is operating on the same device
 *  handle in another thread. It is the responsibility of the application
 *  to assure adherence to this restriction.
 *
 *  @{
 */

/** ============================================================================
 *  @n@b  EMAC_enumerate()
 *
 *  @b Description
 *  @n Enumerates the EMAC peripherals installed in the system and returns an
 *     integer count. The EMAC devices are enumerated in a consistent
 *     fashion so that each device can be later referenced by its physical
 *     index value ranging from "1" to "n" where "n" is the count returned
 *     by this function.
 *
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  None
 *
 *  @b Example
 *  @verbatim
        EMAC_enumerate( );
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_enumerate( void );

/** ============================================================================
 *  @n@b  EMAC_open()
 *
 *  @b Description
 *  @n Opens the EMAC peripheral at the given physical index and initializes
 *     it to an embryonic state.
 *
 *     The calling application must supply a operating configuration that
 *     includes a callback function table. Data from this config structure is
 *     copied into the device's internal instance structure so the structure
 *     may be discarded after EMAC_open() returns. In order to change an item
 *     in the configuration, the EMAC device must be closed and then
 *     re-opened with the new configuration.
 *
 *     The application layer may pass in an hApplication callback handle,
 *     that will be supplied by the EMAC device when making calls to the
 *     application callback functions.
 *
 *     An EMAC device handle is written to phEMAC. This handle must be saved
 *     by the caller and then passed to other EMAC device functions.
 *
 *     The default receive filter prevents normal packets from being received
 *     until the receive filter is specified by calling EMAC_receiveFilter().
 *
 *     A device reset is achieved by calling EMAC_close() followed by EMAC_open().
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error codes include:
 *       CPSW3G_ERR_DEV_ALREADY_OPEN   - The device is already open
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        physicalIndex   physical index
        hApplication    application handle
        pEMACConfig     EMAC's configuration structure
        phEMAC          handle to the EMAC device
    @endverbatim
 *
 *
 *  <b> Return Value </b>  Success (0)
 *  @n     CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *  @n     CPSW3G_ERR_DEV_ALREADY_OPEN   - The device is already open
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Opens the EMAC peripheral at the given physical index and initializes it.
 *
 *  @b Example
 *  @verbatim
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_open(int physicalIndex, Handle hApplication,
                 Cpsw3gInitConfig *pEMACConfig, Handle *phEMAC);

/** ============================================================================
 *  @n@b  EMAC_close()
 *
 *  @b Description
 *  @n Closed the EMAC peripheral indicated by the supplied instance handle.
 *     When called, the EMAC device will shutdown both send and receive
 *     operations, and free all pending transmit and receive packets.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *         CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        hEMAC   handle to opened the EMAC device
    @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC_open function must be called before calling this API.
 *
 *  <b> Post Condition </b>
 *  @n  The EMAC device will shutdown both send and receive
 *      operations, and free all pending transmit and receive packets.
 *
 *  @b Example
 *  @verbatim
        Cpsw3gInitConfig  ecfg;
        Handle       hEMAC = 0;
        //Open the EMAC peripheral
        EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

        //Close the EMAC peripheral
        EMAC_close( hEMAC );
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_close( Handle hEMAC );

/** ============================================================================
 *  @n@b  EMAC_getStatus()
 *
 *  @b Description
 *  @n Called to get the current status of the device. The device status
 *     is copied into the supplied data structure.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        hEMAC   handle to the opened EMAC device
        pStatus Status of the EMAC
    @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API.
 *
 *  <b> Post Condition </b>
 *  @n  The current status of the device is copied into the supplied data
 *      structure.
 *
 *  @b Example
 *  @verbatim
        EMAC_Status pStatus;
        Cpsw3gInitConfig  ecfg;
        Handle       hEMAC = 0;

        //Open the EMAC peripheral
        EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

        EMAC_getStatus( hEMAC, &status);
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_getStatus( Handle hEMAC, EMAC_Status *pStatus);

/** ============================================================================
 *  @n@b  EMAC_setReceiveFilter()
 *
 *  @b Description
 *  @n Called to set the packet filter for received packets. The filtering
 *     level is inclusive, so BROADCAST would include both BROADCAST and
 *     DIRECTED (UNICAST) packets.
 *
 *     Available filtering modes include the following:
 *         - EMAC_RXFILTER_NOTHING      - Receive nothing
 *         - EMAC_RXFILTER_DIRECT       - Receive only Unicast to local MAC addr
 *         - EMAC_RXFILTER_BROADCAST    - Receive direct and Broadcast
 *         - EMAC_RXFILTER_MULTICAST    - Receive above plus multicast in mcast list
 *         - EMAC_RXFILTER_ALLMULTICAST - Receive above plus all multicast
 *         - EMAC_RXFILTER_ALL          - Receive all packets
 *
 *     Note that if error frames and control frames are desired, reception of
 *     these must be specified in the device configuration.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *         CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
         hEMAC           handle to the opened EMAC device
         ReceiveFilter   Filtering modes
         masterChannel   Channel number on which broadcast is to be enabled.
    @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API
 *
 *  <b> Post Condition </b>
 *  @n  Sets the packet filter for received packets
 *
 *  @b Example
 *  @verbatim
        #define EMAC_RXFILTER_DIRECT       1
        Cpsw3gInitConfig  ecfg;
        Handle       hEMAC = 0;

        EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

        EMAC_setReceiveFilter(hEMAC, EMAC_RXFILTER_DIRECT );

    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_setReceiveFilter( Handle hEMAC, Uint32 ReceiveFilter, Uint8 masterChannel );

/** ============================================================================
 *  @n@b  EMAC_getReceiveFilter()
 *
 *  @b Description
 *  @n Called to get the current packet filter setting for received packets.
 *     The filter values are the same as those used in EMAC_setReceiveFilter().
 *
 *     The current filter value is written to the pointer supplied in
 *     pReceiveFilter.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        hEMAC           handle to the opened EMAC device
        pReceiveFilter  Current receive packet filter
    @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API and
 *      must be set the packet filter value.
 *
 *  <b> Post Condition </b>
 *  @n  The current filter value is written to the pointer supplied
 *
 *  @b Example
 *  @verbatim
        #define EMAC_RXFILTER_DIRECT       1
        Cpsw3gInitConfig  ecfg;
        Handle       hEMAC = 0;
        Uint32         pReceiveFilter;

        EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

        EMAC_setReceiveFilter(hEMAC, EMAC_RXFILTER_DIRECT );

        EMAC_getReceiveFilter(hEMAC, &pReceiveFilter );
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_getReceiveFilter( Handle hEMAC, Uint32 *pReceiveFilter );

/** ============================================================================
 *  @n@b  EMAC_getStatistics()
 *
 *  @b Description
 *  @n Called to get the current device statistics. The statistics structure
 *     contains a collection of event counts for various packet sent and
 *     receive properties. Reading the statistics also clears the current
 *     statistic counters, so the values read represent a delta from the last
 *     call.
 *
 *     The statistics information is copied into the structure pointed to
 *     by the pStatistics argument.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        hEMAC       handle to the opened EMAC device
        pStatistics Get the device statistics
    @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API
 *
 *  <b> Post Condition </b>
 *      -# Statistics are read for various packects sent and received.
        -# Reading the statistics also clears the current
           statistic counters, so the values read represent a delta from the
           last call.
 *
 *  @b Example
 *  @verbatim
        Cpsw3gInitConfig      ecfg;
        Handle           hEMAC = 0;
        EMAC_Statistics  pStatistics;

        EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

        EMAC_getStatistics(hEMAC, &pStatistics );
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_getStatistics( Handle hEMAC, EMAC_Statistics *pStatistics );

/** ============================================================================
 *  @n@b  EMAC_setMulticast()
 *
 *  @b Description
 *  @n This function is called to install a list of multicast addresses for
 *     use in multicast address filtering. Each time this function is called,
 *     any current multicast configuration is discarded in favor of the new
 *     list. Thus a call to EMAC_clearMulticast will remove all multicast
 *     addresses from the device.
 *
 *     Note that the multicast list configuration is stateless in that the
 *     list of multicast addresses used to build the configuration is not
 *     retained. Thus it is impossible to examine a list of currently installed
 *     addresses.
 *
 *     The addresses to install are pointed to by pMCastList. The length of
 *     this list in bytes is 6 times the value of AddrCnt. When AddrCnt is
 *     zero, the pMCastList parameter can be NULL.
 *
 *     The function returns zero on success, or an error code on failure.
 *     The multicast list settings are not altered in the event of a failure
 *     code.
 *
 *     Possible error code include:
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        hEMAC       handle to the opened EMAC device
        AddrCount   number of addresses to multicast
        pMCastList  pointer to the multi cast list
    @endverbatim
 *
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened and set multicast filter.
 *
 *  <b> Post Condition </b>
        -# Install a list of multicast addresses for use in multicast
           address filtering.
 *
 *  @b Example
 *  @verbatim
        #define EMAC_RXFILTER_ALLMULTICAST 4

        Handle       hEMAC = 0;
        Uint32         AddrCnt, oldMcastCnt;
        Uint8        pMCastList, pOldMcastList;
        Cpsw3gInitConfig  ecfg;

        EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

        EMAC_setReceiveFilter( hEMAC, EMAC_RXFILTER_ALLMULTICAST );

        EMAC_clearMulticast(hEMAC, oldMcastCnt, &pOldMcastList);

        EMAC_setMulticast( hEMAC, AddrCnt, &pMCastList );
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_setMulticast( Handle hEMAC, Uint32 AddrCnt, Uint8 *pMCastList );

/** ============================================================================
 *  @n@b  EMAC_clearMulticast()
 *
 *  @b Description
 *  @n This function is called to clear a list of multicast addresses previously
 *     installed for use in multicast address filtering.
 *
 *     Note that the multicast list configuration is stateless in that the
 *     list of multicast addresses used to build the configuration is not
 *     retained. Thus it is impossible to examine a list of currently installed
 *     addresses.
 *
 *     The addresses to clean up are pointed to by pMCastList. The length of
 *     this list in bytes is 6 times the value of AddrCnt.
 *
 *     The function returns zero on success, or an error code on failure.
 *     The multicast list settings are not altered in the event of a failure
 *     code.
 *
 *     Possible error code include:
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        hEMAC       handle to the opened EMAC device
        AddrCount   number of addresses to clean up
        pMCastList  pointer to the multi cast list
    @endverbatim
 *
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened and set/clear multicast filter.
 *
 *  <b> Post Condition </b>
        -# Removes all multicast addresses from the device.
 *
 *  @b Example
 *  @verbatim
        #define EMAC_RXFILTER_ALLMULTICAST 4

        Handle       hEMAC = 0;
        Uint32         AddrCnt;
        Uint8        pMCastList;
        Cpsw3gInitConfig  ecfg;

        EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

        EMAC_setReceiveFilter( hEMAC, EMAC_RXFILTER_ALLMULTICAST );

        EMAC_clearMulticast( hEMAC, AddrCnt, &pMCastList );
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_clearMulticast(Handle hEMAC, Uint32 AddrCnt, Uint8 *pMCastList);

/** ============================================================================
 *  @n@b  EMAC_setConfig()
 *
 *  @b Description
 *  @n This function is called to perform various get/set/dump operations on the
 *     EMAC ALE table, statistics counters etc.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        hEMAC       handle to the opened EMAC device
        pBuf        pointer to the buffer that holds the configuration
                    command and any data required.
        size        size of the buffer
    @endverbatim
 *
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened.
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
 *  @verbatim
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_setConfig(Handle hEMAC, void* pBuf, Uint32 size);


/** ============================================================================
 *  @n@b  EMAC_sendPacket()
 *
 *  @b Description
 *  @n Sends a Ethernet data packet out the EMAC device. On a non-error return,
 *     the EMAC device takes ownership of the packet. The packet is returned
 *     to the application's free pool once it has been transmitted.
 *
 *     The function returns zero on success, or an error code on failure.
 *     When an error code is returned, the EMAC device has not taken ownership
 *     of the packet.
 *
 *     Possible error codes include:
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *       CPSW3G_ERR_BADPACKET - The packet structure is invalid
 *
 *  @b Arguments
 *  @verbatim
        hEMAC       handle to the opened EMAC device
        pPkt        EMAC packet structure
    @endverbatim
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *  @n      CPSW3G_ERR_BADPACKET - The packet structure is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened and get a packet
 *      buffer from private queue
 *
 *  <b> Post Condition </b>
 *  @n  Sends a ethernet data packet out the EMAC device and is returned to the
 *      application,s free pool once it has been transmitted.
 *
 *  @b Example
 *  @verbatim
        #define EMAC_RXFILTER_DIRECT       1
        #define EMAC_PKT_FLAGS_SOP         0x80000000u
        #define EMAC_PKT_FLAGS_EOP         0x40000000u

        Cpsw3gInitConfig ecfg;
        EMAC_Pkt    *pPkt;
        Handle      hEMAC = 0;
        Uint32      size, TxCount = 0;

        //open the EMAC device
        EMAC_open( 1, (Handle)0x12345678, &ecfg, &hEMAC );

        //set the receive filter
        EMAC_setReceiveFilter( hEMAC, EMAC_RXFILTER_DIRECT );

        //Fill the packet options fields
        size = TxCount + 60;
        pPkt->Flags      = EMAC_PKT_FLAGS_SOP | EMAC_PKT_FLAGS_EOP;
        pPkt->ValidLen   = size;
        pPkt->DataOffset = 0;
        pPkt->PktChannel = 0;
        pPkt->PktLength  = size;
        pPkt->PktFrags   = 1;

        EMAC_sendPacket( hEMAC, pPkt );

    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_sendPacket(Handle hEMAC, EMAC_Pkt *pPkt);

/** ============================================================================
 *  @n@b  EMAC_RxServiceCheck()
 *
 *  @b Description
 *  @n This function should be called every time there is an EMAC device Rx
 *     interrupt. It maintains the status the EMAC.
 *
 *     Note that the application has the responsibility for mapping the
 *     physical device index to the correct EMAC_serviceCheck() function. If
 *     more than one EMAC device is on the same interrupt, the function must be
 *     called for each device.
 *
 *     Possible error codes include:
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *       CPSW3G_ERR_MACFATAL  - Fatal error in the MAC - Call EMAC_close()
 *
 *  @b Arguments
 *  @verbatim
        hEMAC       handle to the opened EMAC device
    @endverbatim
 *  <b> Return Value </b>  Success (0)
 *  @n     CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *  @n     CPSW3G_ERR_MACFATAL  - Fatal error in the MAC - Call EMAC_close()
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC_open function must be called before calling this API.
 *
 *  <b> Post Condition </b>
 *  @n  None
 *
 *  @b Example
 *  @verbatim
        static CSL_IntcContext context;
        static CSL_IntcEventHandlerRecord Record[13];
        static CSL_IntcObj intcEMACRx;
        static CSL_IntcHandle hIntcEMACRx;

        //CSL_IntcParam vectId1;
        CSL_IntcParam vectId2;

        CSL_IntcGlobalEnableState state;

        // Setup the global Interrupt
        context.numEvtEntries = 13;
        context.eventhandlerRecord = Record;

        // VectorID for the Event
        vectId2 = CSL_INTC_VECTID_6;

        CSL_intcInit(&context);
        // Enable NMIs
        CSL_intcGlobalNmiEnable();
        // Enable Global Interrupts
        CSL_intcGlobalEnable(&state);

        // Opening a handle for EMAC Rx interrupt
        hIntcEMACRx=CSL_intcOpen(&intcEMACRx,CSL_INTC_EVENTID_MACRXINT,&vectId2,NULL);

        //Hook the ISRs
        CSL_intcHookIsr(vectId2,&HwRxInt);

        CSL_intcHwControl(hIntcEMACRx, CSL_INTC_CMD_EVTENABLE, NULL);

        // This function is called when Rx interrupt occurs
        Void HwRxInt (void)
        {
	        // Note : get the Emac Handle(hEMAC) by calling EMAC_open function
            EMAC_RxServiceCheck(hEMAC);
        }

    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_RxServiceCheck( Handle hEMAC);


/** ============================================================================
 *  @n@b  EMAC_TxServiceCheck()
 *
 *  @b Description
 *  @n This function should be called every time there is an EMAC device Tx
 *     interrupt. It maintains the status the EMAC.
 *
 *     Note that the application has the responsibility for mapping the
 *     physical device index to the correct EMAC_serviceCheck() function. If
 *     more than one EMAC device is on the same interrupt, the function must be
 *     called for each device.
 *
 *     Possible error codes include:
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *       CPSW3G_ERR_MACFATAL  - Fatal error in the MAC - Call EMAC_close()
 *
 *  @b Arguments
 *  @verbatim
        hEMAC       handle to the opened EMAC device
    @endverbatim
 *  <b> Return Value </b>  Success (0)
 *  @n     CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *  @n     CPSW3G_ERR_MACFATAL  - Fatal error in the MAC - Call EMAC_close()
 *
 *  <b> Pre Condition </b>
 *  @n EMAC_open function must be called before calling this API.
 *
 *  <b> Post Condition </b>
 *  @n  None
 *
 *  @b Example
 *  @verbatim
        static CSL_IntcContext context;
        static CSL_IntcEventHandlerRecord Record[13];
        static CSL_IntcObj intcEMACTx;
        static CSL_IntcHandle hIntcEMACTx;

        //CSL_IntcParam vectId1;
        CSL_IntcParam vectId2;

        CSL_IntcGlobalEnableState state;

        // Setup the global Interrupt
        context.numEvtEntries = 13;
        context.eventhandlerRecord = Record;

        // VectorID for the Event
        vectId2 = CSL_INTC_VECTID_6;

        CSL_intcInit(&context);
        // Enable NMIs
        CSL_intcGlobalNmiEnable();
        // Enable Global Interrupts
        CSL_intcGlobalEnable(&state);

        // Opening a handle for EMAC Tx interrupt
        hIntcEMACTx=CSL_intcOpen(&intcEMACTx,CSL_INTC_EVENTID_MACTXINT,&vectId2,NULL);

        //Hook the ISRs
        CSL_intcHookIsr(vectId2,&HwTxInt);

        CSL_intcHwControl(hIntcEMACTx, CSL_INTC_CMD_EVTENABLE, NULL);

        // This function is called when Rx interrupt occurs
        Void HwTxInt (void)
        {
	        // Note : get the Emac Handle(hEMAC) by calling EMAC_open function
            EMAC_TxServiceCheck(hEMAC);
        }

    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_TxServiceCheck( Handle hEMAC);


/** ============================================================================
 *  @n@b  EMAC_timerTick()
 *
 *  @b Description
 *  @n This function should be called for each device in the system on a
 *     periodic basis of 100mS (10 times a second). It is used to check the
 *     status of the EMAC and MDIO device, and to potentially recover from
 *     low Rx buffer conditions.
 *
 *     Strict timing is not required, but the application should make a
 *     reasonable attempt to adhere to the 100mS mark. A missed call should
 *     not be "made up" by making multiple sequential calls.
 *
 *     A "polling" driver (one that calls EMAC_serviceCheck() in a tight loop),
 *     must also adhere to the 100mS timing on this function.
 *
 *     Possible error codes include:
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid

 *  @b Arguments
 *  @verbatim
        hEMAC       handle to the opened EMAC device
    @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened
 *
 *  <b> Post Condition </b>
 *  @n  Re-fill Rx buffer queue if needed and modifies  EMAC CONTROL register.
 *
 *  @b Example
 *  @verbatim
        Cpsw3gInitConfig ecfg;
        Handle      hEMAC = 0;

        //open the EMAC device
        EMAC_open( 1, (Handle)0x12345678, &ecfg, &hEMAC );

        EMAC_timerTick( hEMAC);
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_timerTick( Handle hEMAC);

/** ============================================================================
 *  @n@b cpsw3g_Update_Phystatus()
 *
 *  @b Description
 *  @n This function configures the MACCONTROL register according to the user
 *  defined PHY speed, duplex link settings.
 *
 *  @b Arguments
 *  @verbatim
 *      gmacObject          GMAC Object whose PHY needs to be updated
 *      instanceNum         MAC port identifier
    @endverbatim
 *
 *  <b> Return Value </b>
 *  @n  Always returns CPSW3G_SUCCESS
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Configures the GMAC_MACCONTROL register
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
Uint32 cpsw3g_Update_Phystatus(CpgmacObj* gmacObject , Uint32 instanceNum);

/** ============================================================================
 *  @n@b cpsw3g_ControlCb()
 *
 *  @b Description
 *  @n This function is called to update the local copy of device configuration
 *  with the updated Phy status and parameters.
 *
 *  @b Arguments
 *  @verbatim
 *      devPtr              CPSW3G device pointer to be updated
 *      cmd                 Type of update being received. Currently, only one
 *                          supported, i.e. CPSW3G_PHY_STATUS_UPDATE
 *      cmdArg              Command argument, pointer to the updated link parameters
 *      param               Pointer to MAC port object to update.
    @endverbatim
 *
 *  <b> Return Value </b>
 *  @n  Always returns CPSW3G_SUCCESS
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
void cpsw3g_ControlCb(Cpsw3gDevice* devPtr,Uint32 cmd,Ptr cmdArg, Ptr param);

/* @} */

/* @} */

struct EMAC_updateData {
	unsigned int		linkup;
	unsigned int		version;
	unsigned int		duplex;
	unsigned int		speed;
	unsigned int		rmii;
	unsigned int        phy_id;
};

Uint32 EMAC_init( Void );
Uint32 EMAC_deinit( Void );
Int32 EMAC_start( Handle hEMAC );
Int32 EMAC_update(struct EMAC_updateData *update);
Int32 EMAC_enqTxDesc(Handle hEMAC, Uint8 *buffer, Uint32 size, Uint32 flag, Uint32 token);
Int32 EMAC_enqRxDesc(Handle hEMAC, Uint8 *buffer, Uint32 token);
Int32 EMAC_txFreeCount(Handle hEMAC);
Uint32 EMAC_set_coalesce (Uint32 rx_coalesce_usecs, Uint32 bus_freq_mhz);
Int32 ETH_OFFLOAD_EMAC_Poll(Void *arg);




#ifdef __cplusplus
}
#endif

//Structure for debugging
typedef struct _raweth_stat{
	Uint32 cslRxCnt;
	Uint32 ethRxCnt;
	Uint32 nimuRxCnt;
	Uint32 notGoCnt;
	Uint32 appRxCnt;
	Uint32 intPktEnq;
}RAWETH_STAT;
extern RAWETH_STAT raweth_stat;

#endif /* _CSL_EMAC_H_ */
