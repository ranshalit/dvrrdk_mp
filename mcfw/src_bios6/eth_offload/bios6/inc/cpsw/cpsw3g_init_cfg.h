/*******************************************************************************
**+--------------------------------------------------------------------------+**
**|                            ****                                          |**
**|                            ****                                          |**
**|                            ******o***                                    |**
**|                      ********_///_****                                   |**
**|                      ***** /_//_/ ****                                   |**
**|                       ** ** (__/ ****                                    |**
**|                           *********                                      |**
**|                            ****                                          |**
**|                            ***                                           |**
**|                                                                          |**
**|         Copyright (c) 1998-2005 Texas Instruments Incorporated           |**
**|                        ALL RIGHTS RESERVED                               |**
**|                                                                          |**
**+--------------------------------------------------------------------------+**
*******************************************************************************/

/** \file   cpsw3g_init_cfg.h
    \brief  Switch Default init time configuration header file.

    This file contains the default configuration parameters for initializing
    the CPSW.
*/

#ifndef _CPSW3G_INIT_CFG_H
#define _CPSW3G_INIT_CFG_H

#ifdef __cplusplus
extern "C" {
#endif

/* System value for ticks per seconds
 #define CPSW3G_DDA_TICKS_PER_SEC         HZ */

/* Default TX channel number */
#define CPSW3G_DEFAULT_TX_CHANNEL        0u

/* Default RX channel number */
#define CPSW3G_DEFAULT_RX_CHANNEL        0u

#define CPSW3G_MIB_TIMER_TIMEOUT             5000u /* 5 seconds should be enough */

/*ALE time out value*/
#define ALE_AGE_OUT_TIME            300000u
#define CPGMAC_DEFAULT_MAX_MULTICAST_ADDRESSES  64u


/*CPSW3G default config*/

#define CPSW3G_DEFAULT_P2_PASSPRIOTAG                          0u
#define CPSW3G_DEFAULT_P1_PASSPRIOTAG                          0u
#define CPSW3G_DEFAULT_P0_PASSPRIOTAG                          0u
/*flow enable*/
#define CPSW3G_DEFAULT_P2_P2RXFLOWEN                           1u
#define CPSW3G_DEFAULT_P2_P1TXFLOWEN                           1u
#define CPSW3G_DEFAULT_P2_P0TXFLOWEN                           1u
#define CPSW3G_DEFAULT_P1_P2TXFLOWEN                           1u
#define CPSW3G_DEFAULT_P1_P0TXFLOWEN                           0u
#define CPSW3G_DEFAULT_P0_P2TXFLOWEN                           1u
#define CPSW3G_DEFAULT_P0_P1TXFLOWEN                           0u
#define CPSW3G_DEFAULT_FIFO_LOOPBACK                           0u
#define CPSW3G_DEFAULT_P2PTYPE_ESC                             0u
#define CPSW3G_DEFAULT_P1PTYPE_ESC                         0u
#define CPSW3G_DEFAULT_P0PTYPE_ESC                         0u
#define CPSW3G_DEFAULT_ESCPRI_LD_VAL                       0u
#define CPSW3G_DEFAULT_P2STATEN                            1u
#define CPSW3G_DEFAULT_P1STATEN                            1u
#define CPSW3G_DEFAULT_P0STATEN                            1u

#define CPSW3G_DEFAULT_MDIOBUSFREQ               			 (2200000u)// Centaurus:MDIO clk freq < 2.5MHz (0u)
#define CPSW3G_DEFAULT_MDIOCLOCKFREQ                        (200000000)//(0u)
#define CPSW3G_DEFAULT_CPMACBUSFREQ                 (125000000u)
#define CPSW3G_DEFAULT_MDIOTICK                          1000u

/*Default Ale Config*/
#define CPSW3G_DEFAULT_ENABLEALE                                1u
#define CPSW3G_DEFAULT_CLEARTABLE                               1u
#define CPSW3G_DEFAULT_AGEOUTNOW                								0u
#define CPSW3G_DEFAULT_LEARNNOVID                               0u
#define CPSW3G_DEFAULT_ENVID0MODE                               0u
#define CPSW3G_DEFAULT_ENABLEOUIDENY                            0u
#define CPSW3G_DEFAULT_RATELIMITTX                              0u
#define CPSW3G_DEFAULT_ALEVLANAWARE                             1u
#define CPSW3G_DEFAULT_ENAUTHMODE                               0u
#define CPSW3G_DEFAULT_EnRATELIMIT                              0u
/* prescalar divides down to 1ms interval*/
#define CPSW3G_DEFAULT_ALEPRESCALE                           	(CPSW3G_DEFAULT_CPMACBUSFREQ /1000u)
/* old val=0xAE62D*/
#define CPSW3G_DEFAULT_UNK_FORCE_UNTAG_EGR                      63u//dm648:7u//centaurus:bits24:29
#define CPSW3G_DEFAULT_UNK_REGMCAST_FLOODMASK                   6u//dm648:3u,centaurus:6u
#define CPSW3G_DEFAULT_UNK_MCAST_FLOODMASK                      6u//dm648:3u,centaurus:6u
#define CPSW3G_DEFAULT_UNK_VLANMEMLIST                          63u//dm648:7u,centaurus:bits0:5
#define CPGMAC_DEFAULT_UNK_FORCE_UNTAG_EGR                      7u
#define CPGMAC_DEFAULT_UNK_REGMCAST_FLOODMASK                   0u
#define CPGMAC_DEFAULT_UNK_MCAST_FLOODMASK                      0u
#define CPGMAC_DEFAULT_UNK_VLANMEMLIST                          4u
/* Default Ale Port Config*/
#define CPSW3G_DEFAULT_BCASTLIMIT                               0u
#define CPSW3G_DEFAULT_MCASTLIMIT                               0u
#define CPSW3G_DEFAULT_LEARN                                    0u
#define CPSW3G_DEFAULT_NOLEARN                                  1u

#define CPSW3G_DEFAULT_VIDINGRESSCHECK                          0u
#define CPSW3G_DEFAULT_DROPUNTAGGED                             0u
#define CPSW3G_DEFAULT_PORTSTATE                		3u

/* Default Dma config*/
#define CPSW3G_DEFAULT_DMA_PORTPRI                              0u
#define CPSW3G_DEFAULT_DMA_PORTCFI                              0u
#define CPSW3G_DEFAULT_DMA_PORTVID                              1u
#define CPSW3G_DEFAULT_RXBUFOFF                                 0u
#define CPSW3G_DEFAULT_RXCEF                                    0u
#define CPSW3G_DEFAULT_CMDIDLE                                  0u
#define CPSW3G_DEFAULT_RXOFFLEN                                 0u
#define CPSW3G_DEFAULT_RXOWNERSHIP                              0u
#define CPSW3G_DEFAULT_TXPTYPE                                  1u
#define CPSW3G_DEFAULT_SL0_PRI0                                 0u
#define CPSW3G_DEFAULT_SL0_PRI1                                 0u
#define CPSW3G_DEFAULT_SL0_PRI2                                 0u
#define CPSW3G_DEFAULT_SL0_PRI3                                 0u
#define CPSW3G_DEFAULT_SL1_PRI0                                 1u
#define CPSW3G_DEFAULT_SL1_PRI1                                 1u
#define CPSW3G_DEFAULT_SL1_PRI2                                 1u
#define CPSW3G_DEFAULT_SL1_PRI3                                 1u

/* Default MAC  config*/
#define CPSW3G_DEFAULT_MAC_PORTPRI                              0u
#define CPSW3G_DEFAULT_MAC_PORTCFI                              0u
#define CPSW3G_DEFAULT_MAC_PORTVID                              1u
#define CPSW3G_DEFAULT_PASSCRC                                  0u
#define CPSW3G_DEFAULT_RXCMFEN                                  0u
#define CPSW3G_DEFAULT_RXCSFEN                                  0u
#define CPSW3G_DEFAULT_RXCEFEN                                  0u
#define CPSW3G_DEFAULT_EXTEN                                    1u
#define CPSW3G_DEFAULT_GIGFORCE                                 0u
#define CPSW3G_DEFAULT_IFCTLB                                   0u
#define CPSW3G_DEFAULT_IFCTLA                                   1u
#define CPSW3G_DEFAULT_MAC_CMDIDLE                              0u
#define CPSW3G_DEFAULT_TXSHORTGAPEN                             0u
#define CPSW3G_DEFAULT_GIGABITEN                                1u
#define CPSW3G_DEFAULT_TXPACINGEN                               0u
#define CPSW3G_DEFAULT_GMIIEN                                   1u
#define CPSW3G_DEFAULT_TXFLOWEN                                 1u
#define CPSW3G_DEFAULT_RXFLOWEN                                 1u
#define CPSW3G_DEFAULT_LOOPBKEN                                 0u
#define CPSW3G_DEFAULT_RXMAXLEN                                 1518u
#define CPSW3G_DEFAULT_FD                                       1u



/* In future use default init string - parse from static defined init string to derive the init params*/

/* format for specifier is as follows:
   "speed0:duplex0:intPhy0:BroadEn0:MultiEn0:speed1:duplex1:intPhy1
   :BroadEn1:MultiEn1:txNumBDs:txNumChannel:rxNumBDs:rxNumChannel:PromEn:vlanAware"

 */

#define CPSW3G_INITPARAM_SPEED0    	0u
#define CPSW3G_INITPARAM_DUPLEX0	0u
#define CPSW3G_INITPARAM_INTPHY0	1u
#define CPSW3G_INITPARAM_BROADEN0	0u
#define CPSW3G_INITPARAM_MULTIEN0	0u
#define CPSW3G_INITPARAM_SPEED1		0u
#define CPSW3G_INITPARAM_DUPLEX1	0u
#define CPSW3G_INITPARAM_INTPHY1	1u
#define CPSW3G_INITPARAM_BROADEN1	0u
#define CPSW3G_INITPARAM_MULTIEN1	0u
#define CPSW3G_INITPARAM_PROMEN		0u
#define CPSW3G_INITPARAM_VLANAWARE	1u
#define CPSW3G_INITPARAM_TXNUMBDS	128 //192u
#define CPSW3G_INITPARAM_RXNUMBDS   128 //	64u

/* only 1 Tx/Rx channel. Resort to Multiple channels when QOS/Prirority is needed
  * When using multiple channels, the switch must be configured for Rx Channel Mapping
  * based on switch prority and also the Rx CPPI service routine will have to be suitably
  * modified for priority/channel based servicing(from CPPI dma perspective all
  * Rx Channels have same priority, hence only way of handling priority is to implement
  * priority handling policy as part of the RX CPPI completion service mechanism) .
  *
  * Similarly on Tx Path, based on priority/vlan tag we should be enqueuing the packet
  * into corresponding TX Channel(also configure the Tx channels to use priority instead
  * of round robin */
#define CPSW3G_INITPARAM_TXNUMCHAN	1u
#define CPSW3G_INITPARAM_RXNUMCHAN	1u



/* we use the interrupt pacing blocks to cut down on interrupt processing
 * overhead. Following macros define the number of Interrupts the driver will handle
 * every millisec */
 #define NUM_TX_INTS_PERMSEC 		1u
 #define NUM_RX_INTS_PERMSEC		1u

#ifdef __cplusplus
}
#endif

#endif /* _CPSW3G_INIT_CFG_H */
