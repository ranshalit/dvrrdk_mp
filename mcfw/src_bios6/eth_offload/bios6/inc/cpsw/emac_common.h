/**
 *   @file  emac_common.h
 *
 *   @brief
 *      Common definitions
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2008, Texas Instruments, Inc.
 *
 *  \par
 */
#ifndef _EMAC_COMMON_H_
#define _EMAC_COMMON_H_

#include <xdc/std.h>

#include "cslr.h"
#include "cslr_ectl.h"

#include "cslr_cpsw3g.h"
#include "cpsw3g_ioctl.h"       /* Data structures required by Ioctls */
#include "cpsw3g_init_cfg.h"    /* Init time static config params */
#include "cpsw_miimdio.h"

#include <stdio.h>

/*
 * Device Register
 */
#define  DEVICEID_DEVREV				(*(volatile Uint32*)0x48140600)
#define  DEVICEID_DEVREV_MASK			(0xF0000000u)

/*
 * Peripheral register structures
 */

#define CPSW3G_BASEADDR     			(0x4A100000U)
#define MDIO_BASEADDR				    (0x4A100800U)
#define CPSW_3GSS_S_BASEADDR			(0x4A100900U)

/* Interrupt number to the M3 Core */
#ifdef NSP_M3_BUILD
#define RX_PULSE_INTNUM			        65
#define RX_THRESH_PULSE_INTNUM		    64
#define TX_PULSE_INTNUM			        66
#define MISC_PULSE_INTNUM		        67
#endif

/* Interrupt number to the ARM CPU */
#ifdef NSP_A8_BUILD
#define RX_PULSE_INTNUM			        41
#define RX_THRESH_PULSE_INTNUM		    40
#define TX_PULSE_INTNUM			        42
#define MISC_PULSE_INTNUM		        43
#endif

/* Interrupt number to the C674X Core */
#ifdef NSP_DSP_BUILD
#define RX_PULSE_INTNUM			        33
#define RX_THRESH_PULSE_INTNUM		    32
#define TX_PULSE_INTNUM			        34
#define MISC_PULSE_INTNUM		        35
#endif

/**-----------------------------------------------------------------------------
 * EMAC Descriptor section
 */
#define CSL_EMAC_DSC_BASE_ADDR         	0x4A102000 //CPPI RAM ;0x02D00000  /*0X02C82000U;0x01c82000u*/
#define _EMAC_DSC_SIZE                  8192
#define _EMAC_DSC_ENTRY_SIZE        	16
#define _EMAC_DSC_ENTRY_COUNT       	(_EMAC_DSC_SIZE/_EMAC_DSC_ENTRY_SIZE)

#define CSL_EMAC_DSC_BASE_ADDR_L2       0x40300000 //0x4A102000  /* Put descriptors in last half of L2- */
#define CSL_EMAC_DSC_BASE_ADDR_DDR      0x80000000//0x4A102000  /* Same as DDR2 */

#define CSL_EMAC_MAX_DESCRIPTORS        256 /* Descriptors per channels (tx and rx) */


/**
 * @brief
 *  EMAC Buffer descriptor format.
 *
 * @details
 *  The following is the format of a single buffer descriptor
 *  on the EMAC.
 */
typedef struct _EMAC_Desc
{
   /**
    * @brief        Pointer to next descriptor in chain.
    */
  struct _EMAC_Desc *pNext;
   /**
    * @brief        Pointer to data buffer.
    */
  Uint8             *pBuffer;
   /**
    * @brief        Buffer Offset(MSW) and Length(LSW).
    */
  Uint32            BufOffLen;
   /**
    * @brief        Packet Flags(MSW) and Length(LSW).
    */
  Uint32            PktFlgLen;
   /**
    * @brief        Field for software
    */
  volatile Uint32   SwField;

} EMAC_Desc;


/* ------------------------ */
/* DESCRIPTOR ACCESS MACROS */
/* ------------------------ */

/* Packet Flags */
#define EMAC_DSC_FLAG_SOP                               0x80000000u
#define EMAC_DSC_FLAG_EOP                               0x40000000u
#define EMAC_DSC_FLAG_OWNER                             0x20000000u
#define EMAC_DSC_FLAG_EOQ                               0x10000000u
#define EMAC_DSC_FLAG_TDOWNCMPLT                        0x08000000u
#define EMAC_DSC_FLAG_PASSCRC                           0x04000000u

/* The following flags are RX only */
#define EMAC_DSC_FLAG_JABBER                            0x02000000u
#define EMAC_DSC_FLAG_OVERSIZE                          0x01000000u
#define EMAC_DSC_FLAG_FRAGMENT                          0x00800000u
#define EMAC_DSC_FLAG_UNDERSIZED                        0x00400000u
#define EMAC_DSC_FLAG_CONTROL                           0x00200000u
#define EMAC_DSC_FLAG_OVERRUN                           0x00100000u
#define EMAC_DSC_FLAG_CODEERROR                         0x00080000u
#define EMAC_DSC_FLAG_ALIGNERROR                        0x00040000u
#define EMAC_DSC_FLAG_CRCERROR                          0x00020000u
#define EMAC_DSC_FLAG_NOMATCH                           0x00010000u

/*MAC Invector definitions */
#define CSL_CPSW_MACINVECTOR_STATPEND_MASK 		        (0x20000000u)
#define CSL_CPSW_MACINVECTOR_STATPEND_SHIFT 		    (0x0000001Du)
#define CSL_CPSW_MACINVECTOR_STATPEND_RESETVAL 		    (0x00000000u)

#define CSL_CPSW_MACINVECTOR_HOSTPEND_MASK 		        (0x10000000u)
#define CSL_CPSW_MACINVECTOR_HOSTPEND_SHIFT 		    (0x0000001Cu)
#define CSL_CPSW_MACINVECTOR_HOSTPEND_RESETVAL		    (0x00000000u)

#define CSL_CPSW_MACINVECTOR_LINKINT_MASK 		        (0x0C000000u)
#define CSL_CPSW_MACINVECTOR_LINKINT_SHIFT 		        (0x0000001Au)
#define CSL_CPSW_MACINVECTOR_LINKINT_RESETVAL 		    (0x00000000u)

#define CSL_CPSW_MACINVECTOR_USERINT_MASK 		        (0x03000000u)
#define CSL_CPSW_MACINVECTOR_USERINT_SHIFT 		        (0x00000018u)
#define CSL_CPSW_MACINVECTOR_USERINT_RESETVAL 		    (0x00000000u)

#define CSL_CPSW_MACINVECTOR_TXPEND_MASK 		        (0x00FF0000u)
#define CSL_CPSW_MACINVECTOR_TXPEND_SHIFT 		        (0x00000010u)
#define CSL_CPSW_MACINVECTOR_TXPEND_RESETVAL 		    (0x00000000u)

#define CSL_CPSW_MACINVECTOR_RXTHRE_MASK 		        (0x0000FF00u)
#define CSL_CPSW_MACINVECTOR_RXTHRE_SHIFT 		        (0x00000008u)
#define CSL_CPSW_MACINVECTOR_RXTHRE_RESETVAL 		    (0x00000000u)

#define CSL_CPSW_MACINVECTOR_RXPEND_MASK 		        (0x000000FFu)
#define CSL_CPSW_MACINVECTOR_RXPEND_SHIFT 		        (0x00000000u)
#define CSL_CPSW_MACINVECTOR_RXPEND_RESETVAL 		    (0x00000000u)

#define CSL_CPSW_MACINVECTOR_RESETVAL 			        (0x00000000u)

/* EOI vector definitions */
#define CSL_CPSW_MACEOIVECTOR_DMA_EOI_VECTOR_MASK 	    (0x0000001Fu)
#define CSL_CPSW_MACEOIVECTOR_DMA_EOI_VECTOR_SHIFT 	    (0x00000000u)
#define CSL_CPSW_MACEOIVECTOR_DMA_EOI_VECTOR_RESETVAL 	(0x00000000u)

#define CSL_CPSW_MACEOIVECTOR_RESETVAL 			        (0x00000000u)

#define EOI_RXTHRESH_PULSE 	                            (0x0u)
#define EOI_RX_PULSE		                            (0x1u)
#define EOI_TX_PULSE		                            (0x2u)
#define EOI_MISC_PULSE		                            (0x3u)

/* Miscellaneous register related defines */
#define CPSW3G_ALE_TABLE_WRITE                          (0x1u << 31)
#define CPSW3G_SOFT_RESET_BIT                           (0x1u << 0)
#define CPSW3G_SOFT_RESET_COMPLETE                      (0x0u << 0)
#define CPSW3G_TX_CONTROL_TX_ENABLE_VAL                 (0x1u << 0)
#define CPSW3G_RX_CONTROL_RX_ENABLE_VAL                 (0x1u << 0)
#define CPSW3G_HOST_ERR_INTMASK_VAL                     (0x1u << 1)
#define CPSW3G_STAT_PEND_INTMASK_VAL		            (0x1u << 0)

/* macro for Delay, if taskdelay works ,just map it to taskDelay of Bios. Default to tight loop
 *  Rough calculation, have a counter that runs 1024 times the input value*/
#define waitMsecs(numMSecs) 		{\
									volatile Uint32 ctr = (numMSecs) << 10;\
									do{\
										ctr --;\
										}while (ctr!=0);\
									}

#define CPSW3G_SWBD_SIZE		(sizeof(EMAC_Desc))
#define CPU_TO_HW(addr)    		(addr)
#define HW_TO_CPU(addr)    		(addr)

#define CPU_TO_HWBD(addr)    		(((Uint32)(addr) - 0x7E000))
#define HW_TO_CPUBD(addr)    		(((Uint32)(addr) + 0x7E000))

/* The BDs are internal, no need for cache ops here- define as NOOP */
#define CACHE_FLUSH_BD(addr,size)
#define CACHE_INVALIDATE_BD(addr,size)

typedef void*       Handle;

#endif /* _EMAC_COMMON_H_ */
