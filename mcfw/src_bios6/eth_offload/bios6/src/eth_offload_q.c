/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/


#include <xdc/std.h>
#include <mcfw/src_bios6/utils/utils_trace.h>
#include <mcfw/src_bios6/links_common/system/system_priv_common.h>


#include <mcfw/src_bios6/eth_offload/inc/eth_offload_q.h>
#include <mcfw/src_bios6/utils/utils_dmtimer.h>






/*
 * ETH_OFFLOAD_Q_Poll: Function to process inQ on notification from host
 * @Q: Q in which the in coming packets have to be processed
 * @weight: weight to determine how many packets to flush at a time
 * @return: 0(on success)
 */
Int32 ETH_OFFLOAD_Q_Poll(struct ETH_OFFLOAD_queue *Q, UInt32 weight)
{
	UInt32 count = 0;

	/* Loop and process all dequeued elements */
	while (ETH_OFFLOAD_Q_IS_ENQUEUED(Q->read, ETH_OFFLOAD_Q_ARM)) {

		/* Call callback to process each desc */
		if (!Q->callback(Q->hApp, Q->read)) {
			break;
		}

		ETH_OFFLOAD_Q_SET_DEQUEUED(Q->read, ETH_OFFLOAD_Q_DSP);

		Q->read = ETH_OFFLOAD_Q_move(Q, Q->read);
		count++;

		if (count >= weight) {
			break;
		}
	}

	return count;
}

/*
 * ETH_OFFLOAD_Q_Destroy: Function to destroy the Q
 * @Q: Q pointer to ETH_OFFLOAD_queue
 * @return: 1 on success, else -1
 */
Int32 ETH_OFFLOAD_Q_Destroy(struct ETH_OFFLOAD_queue *Q)
{
	if (NULL == Q) {
		return -1;
	}

	/* Free the full Q */
	free(Q);

	return 1;
}

/*
 * ETH_OFFLOAD_Q_Create: Function to create a Q
 * @maxElems: num elements in freeepool
 * @elemSize: size of one element
 * @handler: callback for in packets
 * @in: queue name for inQ
 * @out: queue name for outQ
 * @return: new Q, null on failure
 */
struct ETH_OFFLOAD_queue *ETH_OFFLOAD_Q_Create(UInt32 maxElems, QCALLBACK handler, void *hApp, Int8 *name, struct ETH_OFFLOAD_desc *pPoolStart)
{
	struct ETH_OFFLOAD_queue *Q = (struct ETH_OFFLOAD_queue *)
					malloc(sizeof(struct ETH_OFFLOAD_queue));
	if (NULL == Q) {
		return NULL;
	}

	/* Carve out pool size (maxElems) out of free pool */
	Q->poolSize  = maxElems*sizeof(struct ETH_OFFLOAD_desc);
	Q->poolStart = pPoolStart;
	Q->poolEnd   = (struct ETH_OFFLOAD_desc *)((UInt8*)Q->poolStart + Q->poolSize);
	Q->read      = Q->write     = Q->poolStart;
	Q->callback  = handler;
	Q->hApp      = hApp;
	strncpy((char *)Q->name, (const char *)name, sizeof(Q->name));

	return Q;
}

/*
 * ETH_OFFLOAD_Q_Enqueue: Function to nq desc to Tx or Rx Q
 * @Q: Q (Tx or Rx)
 * @element: desc to be Qed
 * @priority: currently unused
 * @return: 1 if success, 0 otherwise
 */
int ETH_OFFLOAD_Q_Enqueue(struct ETH_OFFLOAD_queue *Q, struct ETH_OFFLOAD_desc *element)
{
	ETH_OFFLOAD_Q_SET_ENQUEUED(element, ETH_OFFLOAD_Q_DSP);

	if(Q->write != element)
    {
        Vps_rprintf(" [ETH_OFFLOAD] Enqueue out of sequence (enqueing %08x but expected %08x) !!!\n",
                element, Q->write
                );
        Q->write = element;
    }

    Q->write = ETH_OFFLOAD_Q_move(Q, Q->write);

	return 0;
}


Int32 ETH_OFFLOAD_Q_Reset(struct ETH_OFFLOAD_queue *Q)
{
	Q->read = Q->write = Q->poolStart;
	return 0;
}
