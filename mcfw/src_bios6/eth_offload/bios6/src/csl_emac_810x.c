/*  ============================================================================
 *   Copyright (c) Texas Instruments Inc 2002, 2003, 2004, 2005, 2006
 *
 *   Use of this software is controlled by the terms and conditions found in the
 *   license agreement under which this software has been supplied.
 *   ===========================================================================
 */

/** ============================================================================
 *   @file  csl_emac.c
 *
 *   PATH:  \$(CSLPATH)\\src\\emac
 *
 *   @brief  EMAC CSL Implementation on DSP side
 *
 *  \par
 *  NOTE:
 *  When used in an multitasking environment, no EMAC function may be
 *  called while another EMAC function is operating on the same device
 *  handle in another thread. It is the responsibility of the application
 *  to assure adherence to this restriction.
 *
 *  \par
 *  Although the EMAC API is defined to support multiple device instances,
 *  this version supports a single device instance
 */

/* =============================================================================
 *  Revision History
 *  ===============
 *  10-Jul-07 PSK   Added multiple MAC addresses support per channel and Si bug workaround
 *  09-Jan-07 dzhou To support all cores use EMAC simultaneously
 * =============================================================================
 */
#include <string.h>
#include <stdio.h>

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <ti/psp/vps/vps.h>

#include <mcfw/src_bios6/utils/utils_trace.h>
#include <mcfw/src_bios6/links_common/system/system_priv_common.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/cpsw/csl_emac.h>

#define EMAC_CORENUM 0

static Uint32           openFlag = 0;
/**< Flag to indicate if the EMAC is already opened.                        */

static Cpsw3gDevice      localDev;
/**< Local copy of the EMAC device instance                                 */

Uint32                   corenum = 0;
/* Core number */

Uint32                  numBdsAllocated = 0;
/* Number of EMAC BDs allocated so far. Shared global variable between the
 * Tx and Rx InitChannel routines.                                          */


#define EMAC_DM646X_CMINTMAX_CNT	63
#define EMAC_DM646X_CMINTMIN_CNT	2
#define EMAC_DM646X_INTPACEEN		(0x30 << 16)
#define EMAC_DM646X_INTPRESCALE_MASK	(0x7FF << 0)
#define EMAC_DM646X_CMINTMAX_INTVL	(1000 / EMAC_DM646X_CMINTMIN_CNT)
#define EMAC_DM646X_CMINTMIN_INTVL	((1000 / EMAC_DM646X_CMINTMAX_CNT) + 1)




Uint32 EMAC_init(Void)
{

    return 1;
}

Uint32 EMAC_deinit(Void)
{

	return 1;
}


/**********************************************************************
 ********************** BD Management Functions ***********************
 **********************************************************************/


/** ============================================================================
 *  @n@b EMAC_pop()
 *
 *  @b Description
 *  @n Pop a desc buffer off a queue
 *
 *  @b Arguments
 *  @verbatim
 q      pointer to desc queue
 @endverbatim
 *
 *  <b> Return Value </b>  Pointer to EMAC desc
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Descriptor buffer from the Queue is removed
 *
 *  @b Example
 *  @verbatim
 EMAC_Desc *q;
 EMAC_pop( q );
 @endverbatim
 * ============================================================================
 */
static EMAC_Desc *EMAC_pop(PKTQ *q)
{
	EMAC_Desc *desc = q->head;

	if (desc) {
		q->head = (EMAC_Desc *)desc->pNext;
		desc->pNext = 0;
		q->depth--;
	}

	return desc;
}

/** ============================================================================
 *  @n@b EMAC_push()
 *
 *  @b Description
 *  @n Push a desc onto a queue
 *
 *  @b Arguments
 *  @verbatim
 q     pointer to desc queue
 desc  pointer to the EMAC desc
 @endverbatim
 *
 *
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Descriptor buffer from the Queue is added
 *
 *  @b Example
 *  @verbatim
 PKTQ *q;
 EMAC_Desc *desc

 EMAC_push( q, desc );
 @endverbatim
 * ============================================================================
 */
static void EMAC_push(PKTQ *q, EMAC_Desc *desc)
{
    volatile UInt32 tmpRead; /* to ensure data is pushed to memory */
	desc->pNext = 0;

	if (!q->head) {
		// Queue is empty
		q->head = desc;
	} else {
		// Queue is not empty
		q->tail->pNext = desc;

        tmpRead = (UInt32)q->tail->pNext;
	}
	q->tail = desc;
	q->depth++;
}

Uint32 EMAC_move(PKTQ *d, PKTQ *s)
{
	if (!d->head) {
		d->head = s->head;
	} else {
		d->tail->pNext = s->head;
	}
	d->tail = s->tail;
	d->depth += s->depth;

	s->head = 0;
	s->depth = 0;

    return 0;
}

static Uint32 EMAC_empty(PKTQ *q)
{
	return q->depth == 0;
}

static Uint32 EMAC_depth(PKTQ *q)
{
	return q->depth;
}

static void EMAC_pushHw(PKTQ *q, EMAC_Desc *desc, volatile UInt32 *pReg)
{
    volatile UInt32 tmpRead; /* to ensure data is pushed to memory */
    EMAC_Desc *prev = q->tail;

	desc->pNext = 0;
    /* tmpRead = desc->pNext; */

	if (!q->head) {
		// Queue is empty
		q->head = desc;
        q->tail = desc;
        *pReg = (UInt32)desc;
	} else {

		// Queue is not empty
		prev->pNext = desc;
        tmpRead = (UInt32)prev->pNext;
        q->tail = desc;

      if ( (*pReg == NULL) &&  ((prev->PktFlgLen & (EMAC_DSC_FLAG_EOQ | EMAC_DSC_FLAG_OWNER) == EMAC_DSC_FLAG_EOQ ))  )
        {
            /* Queue is not running, start it up */
            *pReg = (UInt32)desc;
        }

	}

	q->depth++;

    tmpRead = *pReg;

}

/**
 * EMAC_enqTxDesc: Enqueue Tx packet/frag
 * @hEMAC: Handle to an opened and started emac driver
 * @buffer: Buffer ptr (phy addr) of the frag's data
 * @size: Size of frag
 * @flag: 1LSB bit carries 1 for last frag, rest has the total packet len
 * @token: Software reference
 *
 * Pre enqueued desc ready to receive data from network
 *
 */
Int32 EMAC_enqTxDesc(Handle hEMAC, Uint8 *buffer, Uint32 size, Uint32 flag, Uint32 token)
{
	volatile UInt32 *pReg = 0;
	Uint32 chan = 0, len = (flag>>16);
    volatile UInt32 tmpRead; /* to ensure value is pushed to memory */
	Cpsw3gDevice *pd = (Cpsw3gDevice *)hEMAC;
	EMAC_Desc *desc = EMAC_pop(&pd->cpdma.txCppi[chan].DescFreeQ);

    pReg = &CPSW3G_REGS->TX_HDP[0];
    pReg += pd->cpdma.txCppi[chan].ChannelIndex;

	if (!desc) {
        Vps_rprintf(" [ETH_OFFLOAD] TX: EMAC_pop(): No HW desc available !!!\n");
		return 0;
	}

	desc->pBuffer   = buffer;
	desc->BufOffLen = size;
	desc->SwField   = token;
	desc->pNext     = NULL;
	desc->PktFlgLen = len | EMAC_DSC_FLAG_OWNER | EMAC_DSC_FLAG_SOP | EMAC_DSC_FLAG_EOP;

    tmpRead = desc->PktFlgLen;

    EMAC_pushHw(&pd->cpdma.txCppi[chan].DescHwQ, desc, pReg);

	return 1;
}

/**
 * EMAC_enqRxDesc: Enqueue Rx packet
 * @hEMAC: Handle to a opened and started emac driver
 * @buffer: Buffer ptr (phy addr) for the packet's data
 * @token: Software reference
 *
 * Pre enqueued desc ready to receive data from network
 *
 */
Int32 EMAC_enqRxDesc(Handle hEMAC, Uint8 *buffer, Uint32 token)
{
	Uint32 chan = 0;
	volatile UInt32 *pReg = 0;
	Cpsw3gDevice *pd = (Cpsw3gDevice *)hEMAC;
	EMAC_Desc *desc = EMAC_pop(&pd->cpdma.rxCppi[chan].DescFreeQ);
    volatile UInt32 tmpRead; /* to ensure value is pushed to memory */

    pReg = &CPSW3G_REGS->RX_HDP[0];
    pReg += pd->cpdma.rxCppi[chan].ChannelIndex;

	if (!desc) {
		return 0;
	}

	desc->pBuffer   = buffer;
	desc->BufOffLen = localDev.PktMTU;
	desc->SwField   = token;
	desc->pNext     = NULL;
	desc->PktFlgLen = EMAC_DSC_FLAG_OWNER;

    tmpRead = desc->PktFlgLen;

	EMAC_pushHw(&pd->cpdma.rxCppi[chan].DescHwQ, desc, pReg);

	return 1;
}

/**
 * EMAC_rxFreeCount: Number of free Rx desc
 * @hEMAC: Handle to a opened and started emac driver
 *
 */
Int32 EMAC_rxFreeCount(Handle hEMAC)
{
	UInt32 chan = 0;
	Cpsw3gDevice *pd = (Cpsw3gDevice *)hEMAC;
	return EMAC_depth(&pd->cpdma.rxCppi[chan].DescFreeQ);
}

/**
 * EMAC_txFreeCount: Number of free Tx desc
 * @hEMAC: Handle to a opened and started emac driver
 *
 */
Int32 EMAC_txFreeCount(Handle hEMAC)
{
	Uint32 chan = 0;
	Cpsw3gDevice *pd = (Cpsw3gDevice *)hEMAC;
	return EMAC_depth(&pd->cpdma.txCppi[chan].DescFreeQ);
}


Uint32 EMAC_set_coalesce (Uint32 rx_coalesce_usecs, Uint32 bus_freq_mhz)
{
	Uint32 int_ctrl, num_interrupts = 0;
	Uint32 prescale = 0, addnl_dvdr = 1, coal_intvl = 0;

	if (!rx_coalesce_usecs)
		return 0;

	Vps_printf("\nrx_coalesce_usecs = %d bus_freq_mhz = %d\n",rx_coalesce_usecs ,bus_freq_mhz);

	coal_intvl = rx_coalesce_usecs;

	int_ctrl =  ECTL_REGS->INT_CONTROL;
	prescale = bus_freq_mhz * 4;

	if (coal_intvl < EMAC_DM646X_CMINTMIN_INTVL)
		coal_intvl = EMAC_DM646X_CMINTMIN_INTVL;

	if (coal_intvl > EMAC_DM646X_CMINTMAX_INTVL) {
		/*
		 * Interrupt pacer works with 4us Pulse, we can
		 * throttle further by dilating the 4us pulse.
		 */
		addnl_dvdr = EMAC_DM646X_INTPRESCALE_MASK / prescale;

		if (addnl_dvdr > 1) {
			prescale *= addnl_dvdr;
			if (coal_intvl > (EMAC_DM646X_CMINTMAX_INTVL
						* addnl_dvdr))
				coal_intvl = (EMAC_DM646X_CMINTMAX_INTVL
						* addnl_dvdr);
		} else {
			addnl_dvdr = 1;
			coal_intvl = EMAC_DM646X_CMINTMAX_INTVL;
		}
	}

	num_interrupts = (1000 * addnl_dvdr) / coal_intvl;

	int_ctrl |= EMAC_DM646X_INTPACEEN;
	int_ctrl &= (~EMAC_DM646X_INTPRESCALE_MASK);
	int_ctrl |= (prescale & EMAC_DM646X_INTPRESCALE_MASK);
	ECTL_REGS->INT_CONTROL = int_ctrl;

	ECTL_REGS->C0_RX_IMAX = num_interrupts;
	ECTL_REGS->C0_TX_IMAX = num_interrupts;

        Vps_rprintf("OFFLOAD: Set coalesce to %d usecs.\n", coal_intvl);
	return coal_intvl;
}


/**********************************************************************
 ***************** Switch Configuration Functions *********************
 **********************************************************************/


/** ============================================================================
 *  @n@b cpsw3g_EnableChannel()
 *
 *  @b Description
 *  @n This function configures the appropriate registers to initialize a
 *  DMA channel.
 *
 *  @b Arguments
 *  @verbatim
 *      channel             Channel number
 *      direction           Channel Direction, i.e., NET_CH_DIR_TX/NET_CH_DIR_RX
    @endverbatim
 *
 *  <b> Return Value </b>
 *  @n  Always returns CPSW3G_SUCCESS
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Initializes the Tx/Rx HDP and enables interrupts on the specific channel.
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_EnableChannel(Uint32 channel, Uint32 direction)
{
    CpdmaObj *dmaObj = &localDev.cpdma;

    if(direction == NET_CH_DIR_TX)
    {
        /* enable a TX Dma channel for transfers here */

        /* Init Head Pointer */
        CPSW3G_REGS->TX_HDP[channel] = 0;

        /* enable the corresponding channel interrupt */
        CPSW3G_REGS->TX_INTMASK_SET = (1 << channel);

        /* enable the channel interrupt in CPSW_3G_SS  wrapper*/
        ECTL_REGS->C0_TX_EN |= (1<<channel);

       /* mark the channel as open */
        dmaObj->chIsOpen[NET_CH_DIR_TX][channel] = TRUE;
    }
    else
    {
        /* enable channel interrupt*/
        CPSW3G_REGS->RX_INTMASK_SET = (1 << channel) ;
        /* enable the channel interrupt in CPSW_3G_SS  wrapper*/
        ECTL_REGS->C0_RX_EN |= (1<<channel);

        /* mark the channel as open */
        dmaObj->chIsOpen[NET_CH_DIR_RX][channel] = TRUE;
    }

    return (CPSW3G_SUCCESS);
}

/** ============================================================================
 *  @n@b cpsw3g_DisableChannel()
 *
 *  @b Description
 *  @n This function configures the appropriate registers to de-initialize a
 *  DMA channel.
 *
 *  @b Arguments
 *  @verbatim
 *      channel             Channel number
 *      direction           Channel Direction, i.e., NET_CH_DIR_TX/NET_CH_DIR_RX
    @endverbatim
 *
 *  <b> Return Value </b>
 *  @n  Always returns CPSW3G_SUCCESS
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  De-Initializes the channel by resetting Tx/Rx HDP and disabling interrupts on
 *  the specific channel.
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_DisableChannel(Uint32 channel, Uint32 direction)
{
    CpdmaObj    *dmaObj = &localDev.cpdma;

    dmaObj->tdPending[direction][channel] = TRUE;

    if(direction == NET_CH_DIR_TX)
    {
        /* disable the requested Tx DMA channel here */

        /* command teardown  */
        CPSW3G_REGS->TX_TEARDOWN = channel;

        /* wait for ack ,examine CP for 0xfffffffc */
        while(CPSW3G_REGS->TX_CP[channel] != 0xFFFFFFFC)
        {
        }

        /*disable Tx Interrupt on this channel */
        CPSW3G_REGS->TX_INTMASK_CLEAR =  (1<< channel);

        /* also disable in the wrapper */
        ECTL_REGS->C0_TX_EN &= (~(1<<channel));
    }
    else
    {
	/* disable the requested Rx Dma channel here */

	/* command teardown  */
        CPSW3G_REGS->RX_TEARDOWN = channel;

        /* wait for ack ,examine CP for 0xfffffffc */
	    while(CPSW3G_REGS->RX_CP[channel] != 0xFFFFFFFC)
	    {
	    }

        /*disable Tx Interrupt on this channel */
        CPSW3G_REGS->RX_INTMASK_CLEAR =  (1<< channel);

        /* also disable in the wrapper */
        ECTL_REGS->C0_RX_EN &= (~(1<<channel));
    }

    dmaObj->tdPending[direction][channel] = FALSE;
    dmaObj->chIsOpen[direction][channel] = FALSE;

    return (CPSW3G_SUCCESS);
}

/** ============================================================================
 *  @n@b cpsw3g_InitTxChannel()
 *
 *  @b Description
 *  @n This function sets up the Transmit Buffer descriptors.
 *
 *  @b Arguments
 *  @verbatim
 *      chInfo              Channel object
    @endverbatim
 *
 *  <b> Return Value </b>
 *  CPSW3G_INVALID_PARAM  -   Returned on BD allocation error
 *  CPSW3G_SUCCESS      -   On Success
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Sets up the Tx Buffer descriptors. Tx channel ready for send.
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_InitTxChannel(Cpsw3gChInfo *chInfo)
{
    CpdmaObj*       dmaObj = &localDev.cpdma;
    Cpsw3gRxCppiCh* txChan = &dmaObj->txCppi[chInfo->chNum];
    Cpsw3gRxCppiCh* rxChan = &dmaObj->rxCppi[chInfo->chNum];
    EMAC_Desc       *pDesc;
	Uint32           j,utemp1;

    /* zero init the book keeping structure */
    memset(txChan, 0, sizeof(Cpsw3gTxCppiCh));

    /* store pointer to channel info structure */
    txChan->chInfo = chInfo;

    /*
     * Setup Transmit Buffers
     */

    /*
     * We give the first descriptors to RX. The rest of the descriptors
     * will be divided evenly among the TX channels. Odds are this
     * will leave TX with a very large number of TX descriptors, but
     * we'll only use what we need (driven from the application send
     * requests). The RX descriptors are always kept fully populated.
     */
    /* Pointer to first descriptor to use on RX */
	if (localDev.Config.DescBase == EMAC_DESC_BASE_CPPI) {
		pDesc = (EMAC_Desc *)CSL_EMAC_DSC_BASE_ADDR;
	}else if (localDev.Config.DescBase == EMAC_DESC_BASE_L2) {
		pDesc = (EMAC_Desc *)CSL_EMAC_DSC_BASE_ADDR_L2;
	}else if (localDev.Config.DescBase == EMAC_DESC_BASE_DDR){
		pDesc = (EMAC_Desc *)CSL_EMAC_DSC_BASE_ADDR_DDR;
	}else {
		return (CPSW3G_INVALID_PARAM);
	}

    pDesc += numBdsAllocated; /* advance to next free BD */

    if( (numBdsAllocated + chInfo->numBD) > (_EMAC_DSC_ENTRY_COUNT))
    {
        /* not enough room for the requested number of BDs, fail request */
        printf("InitTx Channel : Unable to allocate %d BDs for channel %d.%d BDs already in use\n",
                chInfo->numBD,chInfo->chNum,numBdsAllocated);
        return CPSW3G_INVALID_PARAM;
    }

    /*
     * Setup Transmit Buffers
     */
    txChan->pd         = &localDev;
	txChan->DescMax    = chInfo->numBD;
	/*Pointer for first TX desc = pointer to RX + num of RX desc.*/
	txChan->pDescFirst = pDesc;
	txChan->pDescLast  = pDesc + (chInfo->numBD - 1);
	txChan->pDescRead  = pDesc;
	txChan->pDescWrite = pDesc;

	/*
	 * Setup Transmit Buffers
	 */

	/* Pointer to first descriptor to use on TX Increase of pointer should not be done,
	   because RX descriptor loop already increases pointer (line 1080 (PSG00001758 fix)*/
	/*pDesc += utemp1;*/

	utemp1 = CPSW3G_INITPARAM_TXNUMBDS;

	/* Init the Tx channels used by local core only */
// 	for(j=0; j<EMAC_NUMCORES; j++) {
// 	for (i = 0; i < CPDMA_MAX_CHANNELS; i++) {
			if((1<<chInfo->chNum) & (localDev.Config.chInfo[NET_CH_DIR_TX][corenum].TxChanEnable)) {
					/*Pointer for first TX desc = pointer to RX + num of RX desc.*/
					pDesc = rxChan->DescFreeQ.tail + 1;
					memset(&(txChan->DescFreeQ), 0,
						sizeof(txChan->DescFreeQ));
					memset(&(txChan->DescHwQ), 0,
						sizeof(txChan->DescHwQ));
					/* Put all desc to free pool */
					for (j = 0;j < utemp1; j++) {
						EMAC_push(&(txChan->DescFreeQ), (pDesc + j));
				}
			}
//  		}
// 	}


    /* clear the teardown pending flag */
    dmaObj->tdPending[NET_CH_DIR_TX][chInfo->chNum] = FALSE;
    /* update the Bd allocation count */
    numBdsAllocated += chInfo->numBD;

    dmaObj->chIsInit[NET_CH_DIR_TX][chInfo->chNum] = TRUE;

    return (CPSW3G_SUCCESS);
}

/** ============================================================================
 *  @n@b cpsw3g_InitRxChannel()
 *
 *  @b Description
 *  @n This function sets up the Receive Buffer descriptors.
 *
 *  @b Arguments
 *  @verbatim
 *      chInfo              Channel object
    @endverbatim
 *
 *  <b> Return Value </b>
 *  CPSW3G_INVALID_PARAM  -   Returned on BD allocation error
 *  CPSW3G_SUCCESS      -   On Success
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Sets up the Rx Buffer descriptors. Rx channel ready for receive.
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_InitRxChannel(Cpsw3gChInfo *chInfo)
{
    CpdmaObj*       dmaObj = &localDev.cpdma;
    Cpsw3gRxCppiCh* rxChan = &dmaObj->rxCppi[chInfo->chNum];
    EMAC_Desc       *pDesc;
	Uint32          j;

    /* zero init the book keeping structure */
    memset(rxChan, 0, sizeof(Cpsw3gRxCppiCh));

    /* store pointer to channel info structure */
    rxChan->chInfo = chInfo;

    /*
     * Setup Receive Buffers
     */

    /*
     * We give the first descriptors to RX. The rest of the descriptors
     * will be divided evenly among the TX channels. Odds are this
     * will leave TX with a very large number of TX descriptors, but
     * we'll only use what we need (driven from the application send
     * requests). The RX descriptors are always kept fully populated.
     */

    /* Pointer to first descriptor to use on RX */
	if (localDev.Config.DescBase == EMAC_DESC_BASE_CPPI) {
		pDesc = (EMAC_Desc *)CSL_EMAC_DSC_BASE_ADDR;
	}else if (localDev.Config.DescBase == EMAC_DESC_BASE_L2) {
		pDesc = (EMAC_Desc *)CSL_EMAC_DSC_BASE_ADDR_L2;
	}else if (localDev.Config.DescBase == EMAC_DESC_BASE_DDR){
		pDesc = (EMAC_Desc *)CSL_EMAC_DSC_BASE_ADDR_DDR;
	}else {
		return (CPSW3G_INVALID_PARAM);
	}

    pDesc += numBdsAllocated; /* advance to next free BD */

    if( (numBdsAllocated + chInfo->numBD) > (_EMAC_DSC_ENTRY_COUNT))
    {
        /* not enough room for the requested number of BDs, fail request */
        printf("InitRx Channel : Unable to allocate %d BDs for channel %d.%d BDs already in use\n",
                chInfo->numBD,chInfo->chNum,numBdsAllocated);
        return CPSW3G_INVALID_PARAM;
    }

    /* Init the Rx channel */
    rxChan->pd         = &localDev;
	rxChan->DescMax    = chInfo->numBD;
	rxChan->pDescFirst = pDesc;
	rxChan->pDescLast  = pDesc + (chInfo->numBD - 1);
	rxChan->pDescRead  = pDesc;
	rxChan->pDescWrite = pDesc;

	/* Number of descriptors for RX channel */
	/* CSL_EMAC_MAX_DESCRIPTORS desc per channel*/

	/* Init the Rx channels used by local core only */
	if((1<<chInfo->chNum) & (localDev.Config.chInfo[NET_CH_DIR_RX][corenum].RxChanEnable)) {
		memset(&(rxChan->DescFreeQ), 0,
				sizeof(rxChan->DescFreeQ));
		memset(&(rxChan->DescHwQ), 0,
				sizeof(rxChan->DescHwQ));
		/* Put all desc to free pool */
		for (j = 0;j < localDev.Config.RxMaxPktPool;j++) {
			EMAC_push(&rxChan->DescFreeQ,
				(pDesc + j));
		}
	}

    /* clear the teardown pending flag */
    dmaObj->tdPending[NET_CH_DIR_RX][chInfo->chNum] = FALSE;
    numBdsAllocated += chInfo->numBD;

    dmaObj->chIsInit[NET_CH_DIR_RX][chInfo->chNum] = TRUE;

   return (CPSW3G_SUCCESS);
}

/** ============================================================================
 *  @n@b cpsw3g_UnInitTxChannel()
 *
 *  @b Description
 *  @n This function frees up the enqueued Transmit Buffer descriptors and the
 *  packets held in any of its queues.
 *
 *  @b Arguments
 *  @verbatim
 *      chInfo              Channel object
    @endverbatim
 *
 *  <b> Return Value </b>
 *  Always returns CPSW3G_SUCCESS
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_UnInitTxChannel(Cpsw3gChInfo *chInfo)
{
	CpdmaObj*       dmaObj = &localDev.cpdma;
// 	Cpsw3gRxCppiCh* txChan = &dmaObj->txCppi[chInfo->chNum];
// 	EMAC_Pkt*       pPkt;

//TBD : Change to EMAC_pop()

//     while (pPkt = pqPop( &txChan->DescHwQ ))
// 	    (localDev.Config.pfcbFreePacket)(localDev.hApplication, pPkt);
//     while (pPkt = pqPop( &txChan->DescFreeQ ))
// 	    (localDev.Config.pfcbFreePacket)(localDev.hApplication, pPkt);

	dmaObj->chIsInit[NET_CH_DIR_TX][chInfo->chNum] = FALSE;

    return (CPSW3G_SUCCESS);
}

/** ============================================================================
 *  @n@b cpsw3g_UnInitRxChannel()
 *
 *  @b Description
 *  @n This function frees up the enqueued Receive Buffer descriptors and any
 *  packets held in the Rx queue.
 *
 *  @b Arguments
 *  @verbatim
 *      chInfo              Channel object
    @endverbatim
 *
 *  <b> Return Value </b>
 *  Always returns CPSW3G_SUCCESS
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_UnInitRxChannel(Cpsw3gChInfo *chInfo)
{
	CpdmaObj*       dmaObj = &localDev.cpdma;
// 	Cpsw3gRxCppiCh* rxChan = &dmaObj->rxCppi[chInfo->chNum];
// 	EMAC_Pkt*       pPkt;

//TBD: Change to EMAC_pop()
//     while (pPkt = pqPop( &rxChan->DescHwQ ))
// 	    (localDev.Config.pfcbFreePacket)(localDev.hApplication, pPkt);

	dmaObj->chIsInit[NET_CH_DIR_RX][chInfo->chNum] = FALSE;

    return (CPSW3G_SUCCESS);
}

/** ============================================================================
 *  @n@b cpsw3g_NetChOpen()
 *
 *  @b Description
 *  @n This function opens a data channel on the CPPI DMA engine.
 *
 *  @b Arguments
 *  @verbatim
 *      chInfo              Channel object to setup.
    @endverbatim
 *
 *  <b> Return Value </b>
 *  CPSW3G_SUCCESS          - Channel setup successful
 *  CPSW3G_ERR_CH_ALREADY_INIT  - Channel already initialized
 *  Other error values if Channel init failed.
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_NetChOpen(Cpsw3gChInfo *chInfo)
{
    Cpsw3gInitConfig*   cpsw3gCfg = &localDev.Config;
    Uint32 retVal;

    /* Perform sanity checks on input params */
    if( chInfo->chNum >= cpsw3gCfg->dmaInitCfg.numChannels[chInfo->chDir])
    {
        printf("NetChOpen: Channel number invalid \n");
	return (CPSW3G_ERR_CH_INVALID);
    }
    if(localDev.cpdma.chIsInit[chInfo->chDir][chInfo->chNum] == TRUE)
    {
        printf("NetChOpen: %s Channel %d already initialized\n",
	        ((chInfo->chDir == NET_CH_DIR_TX) ? "TX" : "RX"), chInfo->chNum);
        return (CPSW3G_ERR_CH_ALREADY_INIT);
	}

    /* Perform book keeping for indv channel */
	if (chInfo->chDir == NET_CH_DIR_TX)
	{
        retVal = cpsw3g_InitTxChannel(chInfo);
	}
	else
	{
        retVal = cpsw3g_InitRxChannel( chInfo);
	}

	if (retVal != CPSW3G_SUCCESS)
	{
        printf("NetChOpen: Error in initializing %s channel %d",
	        ((chInfo->chDir == NET_CH_DIR_TX) ? "TX" : "RX"), chInfo->chNum);
        return (retVal);
	}

	/**CHECK : if device already opened then enable ?? */
	/* Enable this channel for use */
    retVal = cpsw3g_EnableChannel(chInfo->chNum, chInfo->chDir);
	if (retVal != CPSW3G_SUCCESS)
    {
        printf("NetChOpen: Error enabling channel %d in %d direction\n",
            chInfo->chNum, chInfo->chDir);
        return (retVal);
    }

	return (CPSW3G_SUCCESS);
}

/** ============================================================================
 *  @n@b cpsw3g_NetChClose()
 *
 *  @b Description
 *  @n This function closes a previously open data channel on the CPPI DMA engine
 *  and frees up any memory held associated with it.
 *
 *  @b Arguments
 *  @verbatim
 *      chInfo              Channel object to clean up.
    @endverbatim
 *
 *  <b> Return Value </b>
 *  Always returns CPSW3G_SUCCESS
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_NetChClose(Cpsw3gChInfo *chInfo)
{
	CpdmaObj *dmaObj = &localDev.cpdma;

	if(dmaObj->chIsOpen[chInfo->chDir][chInfo->chNum]  == TRUE)
    {
	    cpsw3g_DisableChannel(chInfo->chNum, chInfo->chDir);
	}
	if(dmaObj->chIsInit[chInfo->chDir][chInfo->chNum] == TRUE)
	{
	    if(chInfo->chDir == NET_CH_DIR_TX)
		{
		    cpsw3g_UnInitTxChannel(chInfo);
		}
		else
		{
		    cpsw3g_UnInitRxChannel(chInfo);
		}
    }

	return (CPSW3G_SUCCESS);
}



/** ============================================================================
 *  @n@b cpsw3g_SetMacCfg()
 *
 *  @b Description
 *  @n Configures the MAC port settings.
 *
 *  @b Arguments
 *  @verbatim
        hPort       pointer to port object
    @endverbatim
 *
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Configures the MAC_CONTROL register for the specific port
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static void cpsw3g_SetMacCfg(Cpsw3gPort* hPort)
{
    Cpsw3gInitConfig*   cpsw3gCfg = &localDev.Config;
    Uint32 macControlVal = 0,portNo = 0,macNo = 0;
    CpgmacMacConfig*    lpMacConfign = 0;

    portNo = hPort->portNum;
    if (portNo > 0)
	macNo =  portNo - 1;

    if(portNo > 2)//only GMAC port 0 and port 1 are valid
    {
        /* error condition */
        printf("Error in SetMacCfg\n");
        /* return without doing anything, you
         * cant dereference past instances 0 & 1
         * of CPGMAC_SL config structures */
    }
    else
    {
        lpMacConfign = &cpsw3gCfg->macInitCfg[macNo];

	if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_RXCRC)
        {
            localDev.PktMTU = (cpsw3gCfg->PktMTU) + 4;
        }
        else
        {
            localDev.PktMTU = (cpsw3gCfg->PktMTU);
        }

		/* If PASSCONTROL is set, enable control frames */
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_PASSCONTROL)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_RX_CMF_EN, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_PASSERROR)
        {
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_RX_CEF_EN, 1);
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_RX_CSF_EN, 1);
        }
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_EXTEN)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_EXT_EN, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_GIGABIT)
        {
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_GIG, 1);
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_GMII_EN, 1);
        }
 //NDK 2.0 bug-Enable GMII_EN in 100Mbps
        else
        {
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_GMII_EN, 1);
        }
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_IFCTLB)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_IFCTL_B, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_IFCTLA)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_IFCTL_A, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_CMDIDLE)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_CMD_IDLE, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_TXSHORTGAPEN)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_TX_SHORT_GAP_EN, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_TXSHORTGAPEN)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_TX_SHORT_GAP_EN, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_TXPACE)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_TX_PACE, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_TXPACE)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_TX_PACE, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_TXFLOWCNTL)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_TX_FLOW_EN, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_RXBUFFERFLOWCNTL)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_RX_FLOW_EN, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_MACLOOPBACK)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_LOOPBACK, 1);
		if (lpMacConfign->macModeFlags & EMAC_CONFIG_MODEFLG_FULLDUPLEX)
            macControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_FULLDUPLEX, 1);

        CPSW3G_REGS->GMAC[macNo].SL_MACCONTROL = macControlVal;

    }

    return;
}

/** ============================================================================
 *  @n@b cpsw3g_SetDmaCfg()
 *
 *  @b Description
 *  @n Configures the DMA port(CPU facing port) settings.
 *
 *  @b Arguments
 *  @verbatim
        hPort       pointer to port object
    @endverbatim
 *
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Configures the DMA_CONTROL,CPDMA_RX_CH_MAP registers for the DMA port
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static void cpsw3g_SetDmaCfg(Cpsw3gPort* hPort)
{
    Cpsw3gInitConfig*   cpsw3gCfg = &localDev.Config;
    CpdmaConfig*        lpDmaConfign = &cpsw3gCfg->dmaInitCfg;
    Uint32              dmaControlVal = 0;

    (void)hPort;/*to suppress warning */

	if (lpDmaConfign->dmaModeFlags & EMAC_CONFIG_MODEFLG_PASSERROR)
        dmaControlVal |= CSL_FMK(CPSW3G_DMACONTROL_RX_CEF, 1);
	if (lpDmaConfign->dmaModeFlags & EMAC_CONFIG_MODEFLG_CMDIDLE)
        dmaControlVal |= CSL_FMK(CPSW3G_GMAC_MACCONTROL_CMD_IDLE, 1);
	if (lpDmaConfign->dmaModeFlags & EMAC_CONFIG_MODEFLG_RXOFFLENBLOCK)
        dmaControlVal |= CSL_FMK(CPSW3G_DMACONTROL_RX_OFFLEN_BLOCK, 1);
	if (lpDmaConfign->dmaModeFlags & EMAC_CONFIG_MODEFLG_RXOWNERSHIP)
        dmaControlVal |= CSL_FMK(CPSW3G_DMACONTROL_RX_OWNERSHIP, 1);
	if (lpDmaConfign->dmaModeFlags & EMAC_CONFIG_MODEFLG_CHPRIORITY)
        dmaControlVal |= CSL_FMK(CPSW3G_DMACONTROL_TX_PTYPE, 1);

    CPSW3G_REGS->DMACONTROL = dmaControlVal;

    /* Centaurus configuration is differnet here */

}


/** ============================================================================
 *  @n@b cpsw3g_SetPortVlanCfg()
 *
 *  @b Description
 *  @n Configures the default VLAN ID for the port.
 *
 *  @b Arguments
 *  @verbatim
        hPort       pointer to port object
        pri         user priority for this port
        cfi         CFI for this port
        vlanId      VLAN ID to configure

    @endverbatim
 *
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Configures the GMAC_PORT.P_PORT_VLAN for a MAC port or P2_PORT_VLAN register
 *      for the DMA port.
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static void cpsw3g_SetPortVlanCfg(Cpsw3gPort* hPort, Uint32 pri, Uint32 cfi, Uint32 vlanId)
{
    Uint32 macNo = 0;

    if(hPort->portNum > 0)
    {
	macNo = hPort->portNum - 1;

        CPSW3G_REGS->GMAC_PORT[macNo].P_PORT_VLAN =
                            CSL_FMK(CPSW3G_P_PORT_VLAN_PORT_PRI, pri) |
                            CSL_FMK(CPSW3G_P_PORT_VLAN_PORT_CFI, cfi) |
                            CSL_FMK(CPSW3G_P_PORT_VLAN_PORT_VID, vlanId);
    }
    else
    {
        CPSW3G_REGS->P0_PORT_VLAN =
                            CSL_FMK(CPSW3G_P_PORT_VLAN_PORT_PRI, pri) |
                            CSL_FMK(CPSW3G_P_PORT_VLAN_PORT_CFI, cfi) |
                            CSL_FMK(CPSW3G_P_PORT_VLAN_PORT_VID, vlanId);

    }
    return;
}


/** ============================================================================
 *  @n@b cpsw3g_SetCpswCfg()
 *
 *  @b Description
 *  @n Configures the switch settings based on user configuration.
 *
 *  @b Arguments
 *  @verbatim
 *      None
    @endverbatim
 *
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Configures the CPSW_CONTROL, CPSW_PTYPE, CPSW_STAT_PORT_EN registers
 *      for the DMA port.
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static void cpsw3g_SetCpswCfg(void)
{
    Cpsw3gInitConfig*   cpsw3gCfg = &localDev.Config;
    /* Local Pointer to Switch Configuration */
    Cpsw3gConfig*       lpCpswCfg = &cpsw3gCfg->cpswCfg;
    Uint32              cpswCtlVal = 0, cpswPtypeCtlVal = 0, cpswStatCtlVal = 0;

	if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_P2PASSPRITAG)
        cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_P2_PASS_PRI_TAGGED, 1);
	if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_P1PASSPRITAG)
        cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_P1_PASS_PRI_TAGGED, 1);
	if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_P0PASSPRITAG)
        cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_P0_PASS_PRI_TAGGED, 1);
	if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_ENVLANAWARE)
        cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_VLAN_AWARE, 1);
	if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_RXVLANENCAP)
        cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_RX_VLAN_ENCAP, 1);
	//if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_P2P2RXFLOWEN)
      //  cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_P2_P2RX_FLOW_EN, 1);
	//if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_P2P1TXFLOWEN)
      //  cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_P2_P1TX_FLOW_EN, 1);
	//if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_P2P0TXFLOWEN)
      //  cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_P2_P0TX_FLOW_EN, 1);
	//if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_P1P2TXFLOWEN)
      //  cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_P1_P2TX_FLOW_EN, 1);
	//if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_P1P0TXFLOWEN)
      //  cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_P1_P0TX_FLOW_EN, 1);
	//if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_P0P2TXFLOWEN)
      //  cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_P0_P2TX_FLOW_EN, 1);
	//if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_P0P1TXFLOWEN)
      //  cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_P0_P1TX_FLOW_EN, 1);
	//if (lpCpswCfg->cpswCtlModeFlags & EMAC_CONFIG_CPSW_FIFOLOOPBACK)
      //  cpswCtlVal |= CSL_FMK(CPSW3G_CPSW_CONTROL_FIFO_LOOPBACK, 1);

    CPSW3G_REGS->CPSW_CONTROL = cpswCtlVal;


	if (lpCpswCfg->cpswPtypeModeFlags & EMAC_CONFIG_PTYPE_P2PTYPEESC)
        cpswPtypeCtlVal |= CSL_FMK(CPSW3G_CPSW_PTYPE_P2_PTYPE_ESC, 1);
	if (lpCpswCfg->cpswPtypeModeFlags & EMAC_CONFIG_PTYPE_P1PTYPEESC)
        cpswPtypeCtlVal |= CSL_FMK(CPSW3G_CPSW_PTYPE_P1_PTYPE_ESC, 1);
	if (lpCpswCfg->cpswPtypeModeFlags & EMAC_CONFIG_PTYPE_P0PTYPEESC)
        cpswPtypeCtlVal |= CSL_FMK(CPSW3G_CPSW_PTYPE_P0_PTYPE_ESC, 1);

    CPSW3G_REGS->CPSW_PTYPE =  cpswPtypeCtlVal |
                              CSL_FMK(CPSW3G_CPSW_PTYPE_ESC_PRI_LD_VAL, lpCpswCfg->escPriLdVal);

    /* Configure use of statistics */
	if (lpCpswCfg->cpswStatModeFlags & EMAC_CONFIG_STAT_P2STATEN)
        cpswStatCtlVal |= CSL_FMK(CPSW3G_CPSW_STAT_PORT_EN_P2_STAT_EN, 1);
	if (lpCpswCfg->cpswStatModeFlags & EMAC_CONFIG_STAT_P1STATEN)
        cpswStatCtlVal |= CSL_FMK(CPSW3G_CPSW_STAT_PORT_EN_P1_STAT_EN, 1);
	if (lpCpswCfg->cpswStatModeFlags & EMAC_CONFIG_STAT_P0STATEN)
        cpswStatCtlVal |= CSL_FMK(CPSW3G_CPSW_STAT_PORT_EN_P0_STAT_EN, 1);

    CPSW3G_REGS->CPSW_STAT_PORT_EN = cpswStatCtlVal;

}

/** ============================================================================
 *  @n@b cpsw3g_SetAleCfg()
 *
 *  @b Description
 *  @n Configures Address lookup engine (ALE) of EMAC.
 *
 *  @b Arguments
 *  @verbatim
 *      None
    @endverbatim
 *
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Configures the ALE_CONTROL, ALE_PRESCALE, ALE_UNKNOWN_VLAN registers
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static void cpsw3g_SetAleCfg(void)
{
    Cpsw3gInitConfig*   cpsw3gCfg = &localDev.Config;
    /* Local Pointer to Ale Configuration */
    AleConfig*          lpAleConfig = &cpsw3gCfg->aleCfg;
    Uint32              aleCtlVal = 0;

	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_ENABLE)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_ENABLE_ALE, 1U);
	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_CLRTABLE)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_CLEAR_TABLE, 1);
	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_AGEOUTNOW)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_AGE_OUT_NOW, 1);
	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_LEARNNOVID)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_LEARN_NO_VID, 1);
	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_ENVID0MODE)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_EN_VID0_MODE, 1);
	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_ENOUIDENY)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_ENABLE_OUI_DENY, 1);
	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_ALEBYPASS)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_ALE_BYPASS, 1);
	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_RATELIMITTX)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_RATE_LIMIT_TX, 1);
	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_ENVLANAWARE)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_ALE_VLAN_AWARE, 1);
	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_ENAUTHMODE)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_ENABLE_AUTH_MODE, 1);
	if (lpAleConfig->aleModeFlags & EMAC_CONFIG_ALE_ENRATELIMIT)
        aleCtlVal |= CSL_FMK(CPSW3G_ALE_CONTROL_ENABLE_RATE_LIMIT, 1);

    CPSW3G_REGS->ALE_CONTROL = aleCtlVal;

    /* Clear out toggle bits */
    lpAleConfig->aleModeFlags &= ~(EMAC_CONFIG_ALE_CLRTABLE
                                 | EMAC_CONFIG_ALE_AGEOUTNOW);

    /* Configure ALE Prescale register */
    CPSW3G_REGS->ALE_PRESCALE = lpAleConfig->alePrescale;

    /* Configure Unknown VLAN items */
    CPSW3G_REGS->ALE_UNKNOWN_VLAN =
                                    CSL_FMK(CPSW3G_ALE_UNKNOWN_VLAN_UNKNOWN_FORCE_UNTAGGED_EGRESS,
                                            lpAleConfig->unknownForceUntaggedEgress)
                                    |CSL_FMK(CPSW3G_ALE_UNKNOWN_VLAN_UNKNOWN_REG_MCAST_FLOOD_MASK,
                                            lpAleConfig->unknownRegMcastFloodMask)
                                    |CSL_FMK(CPSW3G_ALE_UNKNOWN_VLAN_UNKNOWN_MCAST_FLOOD_MASK,
                                            lpAleConfig->unknownMcastFloodMask)
                                    |CSL_FMK(CPSW3G_ALE_UNKNOWN_VLAN_UNKNOWN_VLAN_MEMBER_LIST,
                                            lpAleConfig->unknownVlanMemberList);
}

/** ============================================================================
 *  @n@b cpsw3g_SetAlePortCfg()
 *
 *  @b Description
 *  @n Configures Address lookup engine (ALE) settings for the specific MAC port.
 *
 *  @b Arguments
 *  @verbatim
 *      None
    @endverbatim
 *
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Configures the ALE_PORTCTL register
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static void cpsw3g_SetAlePortCfg(Cpsw3gPort* hPort)
{
    Cpsw3gInitConfig*   cpsw3gCfg = &localDev.Config;
    Uint32 alePortControlVal=0, portNo = 0;
    /* Local Pointer to AlePort Configuration */
    AlePortConfig *lpAlePortConfign = 0;

    portNo = hPort->portNum;
    lpAlePortConfign = &cpsw3gCfg->alePortCfg[portNo];

    alePortControlVal =
                        CSL_FMK(CPSW3G_ALE_PORTCTL_BCAST_LIMIT, lpAlePortConfign->bcastLimit) |
                        CSL_FMK(CPSW3G_ALE_PORTCTL_MCAST_LIMIT, lpAlePortConfign->mcastLimit) |
                        CSL_FMK(CPSW3G_ALE_PORTCTL_NO_LEARN, lpAlePortConfign->noLearn) |
                        CSL_FMK(CPSW3G_ALE_PORTCTL_VID_INGRESS_CHECK, lpAlePortConfign->vidIngressCheck) |
                        CSL_FMK(CPSW3G_ALE_PORTCTL_DROP_UNTAGGED, lpAlePortConfign->dropUntagged) |
                        CSL_FMK(CPSW3G_ALE_PORTCTL_PORT_STATE, lpAlePortConfign->portState);
    if (portNo > 0)
    {
	    CpgmacObj * gmacObject;
	    gmacObject = &localDev.cpgmacMac[portNo];

	    gmacObject->portState = CSL_FEXT(alePortControlVal, CPSW3G_ALE_PORTCTL_PORT_STATE);
	    CPSW3G_REGS->ALE_PORTCTL[portNo] = alePortControlVal;
    }
    else
    {
	    CpdmaObj * dmaObj;
	    dmaObj = &localDev.cpdma;

	    dmaObj->portState = CSL_FEXT(alePortControlVal, CPSW3G_ALE_PORTCTL_PORT_STATE);
	    CPSW3G_REGS->ALE_PORTCTL[portNo] = alePortControlVal;
    }
}



/** ============================================================================
 *  @n@b cpgmacMacOpen()
 *
 *  @b Description
 *  @n This function opens and sets up a MAC port for communication.
 *
 *  @b Arguments
 *  @verbatim
 *      hPort               Handle to the Port object to setup
    @endverbatim
 *
 *  <b> Return Value </b>   None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static void cpgmacMacOpen(Cpsw3gPort *hPort)
{
    Cpsw3gInitConfig*	cpsw3gCfg = &localDev.Config;
    Uint32 				instId = hPort->portNum -1 ;//centaurus:port0-cpdma,port1&2-SL
    CpgmacMacConfig*	macInitCfg = &cpsw3gCfg->macInitCfg[instId];
	int				    j; // index
    Uint32              tmpval;
	EMAC_AddrConfig*	tmp;

    /* Do a soft reset of the module */
    CPSW3G_REGS->GMAC[instId].SL_SOFT_RESET = CPSW3G_SOFT_RESET_BIT;
    /* wait for reset complete */
    while (CPSW3G_REGS->GMAC[instId].SL_SOFT_RESET != CPSW3G_SOFT_RESET_COMPLETE)
    {
        /* Taskdelay if necessary */
    }

    /* Set MacControl Register */
    cpsw3g_SetMacCfg(hPort);

    tmp = &(cpsw3gCfg->MacAddr[instId]);

	tmpval = 0;
	for (j = 3; j >= 0; j--)
	    tmpval = (tmpval << 8) | tmp->Addr[j];

	CPSW3G_REGS->GMAC_PORT[instId].SL_SA_HI = tmpval;

	tmpval = tmp->Addr[5];
	CPSW3G_REGS->GMAC_PORT[instId].SL_SA_LO = (tmpval << 8) | tmp->Addr[4];


    /* Configure port VLAN information */
    cpsw3g_SetPortVlanCfg(hPort, macInitCfg->portPri, macInitCfg->portCfi, macInitCfg->portVID);

    /* write rx maxlen register */
    CPSW3G_REGS->GMAC[instId].SL_RX_MAXLEN = localDev.PktMTU + 4;

    /* Finally Set the Mac Control register. Enable MII */
    CPSW3G_REGS->GMAC[instId].SL_MACCONTROL |= (1 << CSL_CPSW3G_GMAC_MACCONTROL_GMII_EN_SHIFT);
}

/** ============================================================================
 *  @n@b cpgmacMacClose()
 *
 *  @b Description
 *  @n This function closes and de-initializes the MAC port.
 *
 *  @b Arguments
 *  @verbatim
 *      hPort               Handle to the Port object to setup
    @endverbatim
 *
 *  <b> Return Value </b>   None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static void cpgmacMacClose(Cpsw3gPort *hPort)
{
    Uint32 instId = hPort->portNum;

    /* soft reset */
    CPSW3G_REGS->GMAC[instId].SL_SOFT_RESET = CPSW3G_SOFT_RESET_BIT;
    /* wait for reset complete */
    while (CPSW3G_REGS->GMAC[instId].SL_SOFT_RESET != CPSW3G_SOFT_RESET_COMPLETE)
    {
        /* CHECK :: Task Delay if necessary */
    }

    return;
}

/** ============================================================================
 *  @n@b cpdmaOpen()
 *
 *  @b Description
 *  @n This function opens and sets up the DMA port and channel for communication.
 *
 *  @b Arguments
 *  @verbatim
 *      hPort               Handle to the Port object to setup
    @endverbatim
 *
 *  <b> Return Value </b>
 *      CPSWG3_SUCCESS      DMA open successful
 *      error value         DMA open failed
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpdmaOpen(Cpsw3gPort *hPort)
{
    Cpsw3gInitConfig*   cpsw3gCfg = &localDev.Config;
    CpdmaConfig*        dmaInitCfg = &cpsw3gCfg->dmaInitCfg;
    Uint32              channel; //, dir;

    /* soft reset */
    CPSW3G_REGS->SOFT_RESET = CPSW3G_SOFT_RESET_BIT;

    /* wait for reset complete */
    while (CPSW3G_REGS->SOFT_RESET != CPSW3G_SOFT_RESET_COMPLETE);

    {
        /**CHECK : task delay if necessary */
    }

    for (channel = 0; channel < CPDMA_MAX_CHANNELS; channel++)
    {
        /* program tx/rx headDescriptorPointes to 0 */
        CPSW3G_REGS->TX_HDP[channel] = 0;
        CPSW3G_REGS->RX_HDP[channel] = 0;
        /* program tx/rx completionPointers to 0 */
        CPSW3G_REGS->TX_CP[channel] = 0;
        CPSW3G_REGS->RX_CP[channel] = 0;
    }


    /* Set the DMACONTROL register */
    cpsw3g_SetDmaCfg(hPort);

    cpsw3g_SetPortVlanCfg(hPort, dmaInitCfg->portPri, dmaInitCfg->portCfi, dmaInitCfg->portVID);

    /* Set the rx buffer offset */
    CPSW3G_REGS->RX_BUFFER_OFFSET = dmaInitCfg->rxBufferOffset;

    /* Centaurus: Disable interrupts TX_INTMASK_CLEAR,RX_INTMASK_CLEAR & DMA_INTMASK_CLEAR */
    CPSW3G_REGS->TX_INTMASK_CLEAR = CSL_CPSW3G_TX_INTMASK_CLEAR_RESETVAL;
    CPSW3G_REGS->RX_INTMASK_CLEAR = CSL_CPSW3G_TX_INTMASK_CLEAR_RESETVAL;
    CPSW3G_REGS->DMA_INTMASK_CLEAR = CSL_CPSW3G_TX_INTMASK_CLEAR_RESETVAL;

    /* Enable TX/RX DMA */
    CPSW3G_REGS->TX_CONTROL |= CPSW3G_TX_CONTROL_TX_ENABLE_VAL;
    CPSW3G_REGS->RX_CONTROL |= CPSW3G_RX_CONTROL_RX_ENABLE_VAL;

    /* Enable Adapter check interrupts - enable stats interupt */
    CPSW3G_REGS->DMA_INTMASK_SET =  CPSW3G_HOST_ERR_INTMASK_VAL |
							    CPSW3G_STAT_PEND_INTMASK_VAL;

    /* enable host,stats interrupt in cpsw_3gss_s wrapper */
    ECTL_REGS->C0_MISC_EN =    CPSW3G_ECTL_HOSTERR_INTMASK |
						CPSW3G_ECTL_STATPEND_INTMASK;

    return (CPSW3G_SUCCESS);
}

/** ============================================================================
 *  @n@b cpdmaClose()
 *
 *  @b Description
 *  @n This function closes and de-initializes up the DMA port and channels.
 *
 *  @b Arguments
 *  @verbatim
 *      hPort               Handle to the Port object to close
    @endverbatim
 *
 *  <b> Return Value </b>
 *      CPSWG3_SUCCESS      DMA close successful
 *      error value         DMA close failed
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpdmaClose(Cpsw3gPort* hPort)
{
    Uint32 retVal=CPSW3G_SUCCESS;
    Uint32 channel;

    (void)hPort;/*to suppress warning */

    /* Close TX Channels */
    for (channel = 0; channel < CPDMA_MAX_CHANNELS; channel++)
    {
        retVal = cpsw3g_NetChClose(&localDev.Config.chInfo[NET_CH_DIR_TX][channel]);
    }

    /* Close RX Channels */
    for (channel = 0; channel < CPDMA_MAX_CHANNELS; channel++)
    {
	retVal = cpsw3g_NetChClose(&localDev.Config.chInfo[NET_CH_DIR_RX][channel]);

	}

    /* Disable TX/RX DMA */
    CPSW3G_REGS->TX_CONTROL = 0;
    CPSW3G_REGS->RX_CONTROL = 0;

    /* Disable Adapter check interrupts - Disable stats interupt */
    CPSW3G_REGS->DMA_INTMASK_CLEAR = CPSW3G_HOST_ERR_INTMASK_VAL |
							     CPSW3G_STAT_PEND_INTMASK_VAL;

    /* Disable host,stats interrupt in cpsw_3gss_s wrapper */
    ECTL_REGS->C0_MISC_EN &= ( ~( CPSW3G_ECTL_HOSTERR_INTMASK |
						CPSW3G_ECTL_STATPEND_INTMASK));
    /* soft reset */
    CPSW3G_REGS->SOFT_RESET = CPSW3G_SOFT_RESET_BIT;

    /* wait for reset complete */
    while (CPSW3G_REGS->SOFT_RESET != CPSW3G_SOFT_RESET_COMPLETE)
	{
        waitMsecs(1);
	}

    return (retVal);
}


/** ============================================================================
 *  @n@b cpsw3g_port_open()
 *
 *  @b Description
 *  @n This function opens and sets up the port and channel for communication.
 *
 *  @b Arguments
 *  @verbatim
 *      hPort               Handle to the Port object to setup
    @endverbatim
 *
 *  <b> Return Value </b>
 *      CPSWG3_SUCCESS      port open successful
 *      error value         port open failed
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static int cpsw3g_port_open(Cpsw3gPort *hPort)
{
     Cpsw3gInitConfig*   cpsw3gCfg = &localDev.Config;

    /* Open DDC */
    if(hPort->portNum > 0)
    {
	    /* Ports 1 & 2 are CPGMAC_SL */
	    cpgmacMacOpen(hPort);
    }
    else
    {
	    cpdmaOpen(hPort);
    }

    /**CHECK  sequence here */

    /*Program the CPSW_Control register */
    if (hPort->portNum == 0)
	    cpsw3g_SetCpswCfg();

    /* Program the ALE Control register */
    if (hPort->portNum == 0)
	    cpsw3g_SetAleCfg();

    /* Configure ALE Port Control Register */
    cpsw3g_SetAlePortCfg(hPort);

    /**CHECK: kick start any timers here */

    /**Port num 0: Program DMA channels here */
    if (hPort->portNum == 0)
    {
        Uint32 ch,retCode;
		UInt32 dir;
		dir = NET_CH_DIR_RX;
        for (ch = 0; ch < cpsw3gCfg->dmaInitCfg.numChannels[dir]; ch++)
        {
            /*Create a channel */
             retCode = cpsw3g_NetChOpen( &localDev.Config.chInfo[dir][ch]);
            if (retCode != CPSW3G_SUCCESS)
            {
                return (-1);
            }

        }/* end of for loop over ch */
		dir = NET_CH_DIR_TX;
        for (ch = 0; ch < cpsw3gCfg->dmaInitCfg.numChannels[dir]; ch++)
        {
            /*Create a channel */
             retCode = cpsw3g_NetChOpen( &localDev.Config.chInfo[dir][ch]);
            if (retCode != CPSW3G_SUCCESS)
            {
                return (-1);
            }

        }/* end of for loop over ch */

    }

    return (CPSW3G_SUCCESS);
}

/** ============================================================================
 *  @n@b cpsw3g_port_close()
 *
 *  @b Description
 *  @n This function calls the appropriate functions for closing the port and
 *  cleaning up the associated channels.
 *
 *  @b Arguments
 *  @verbatim
 *      hPort               Handle to the Port object to setup
    @endverbatim
 *
 *  <b> Return Value </b>
 *      CPSWG3_SUCCESS      port open successful
 *      error value         port open failed
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static int cpsw3g_port_close(Cpsw3gPort* hPort)
{
    if(hPort->portNum == 0)
    {
	    /* handle DMA port cleanup here */
	    cpdmaClose(hPort);
	}
	else
	{
	    cpgmacMacClose(hPort);
	}

    return (CPSW3G_SUCCESS);
}

/** ============================================================================
 *  @n@b cpsw3g_AleFindAddress()
 *
 *  @b Description
 *  @n This function will attempt to find the address in the ALE table, if
 *  successful, it will return index to the entry in the ALE table.
 *
 *  @b Arguments
 *  @verbatim
 *      addr        IP Address to find in the ALE table.
 *      vlanId      VLAN Id
    @endverbatim
 *
 *  <b> Return Value </b>
 *  @n  Index to entry if found in ALE table.
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Int32 cpsw3g_AleFindAddress( Uint8 *addr, Uint32 vlanId)
{
    Uint32 i=0,VID=0,mask1=0, info1=0, info0=0,reg0=0,reg1=0,reg2=0;
    Int32 ret = -1;

    /* We only need to check the entry type and address fields */
    mask1=0x0000ffff;

    /* figure out the expected values to match with */
    info1=((addr[0]&0x0ff)<<8) | ((addr[1]&0x0ff)<<0);
    info1=info1 & 0x0000FFFF;
    info0=((addr[2]&0x0ff)<<24) | ((addr[3]&0x0ff)<<16) | ((addr[4]&0x0ff)<<8)
            | ((addr[5]&0x0ff)<<0);

    for(i=0;i<CPSW3G_ALE_TABLE_DEPTH;i++)
    {
		//i &= 0x00000FFF;
        /* read from the ALE table */
        CPSW3G_REGS->ALE_TBLCTL = i;
        reg0=CPSW3G_REGS->ALE_TBLW0;
        reg1=CPSW3G_REGS->ALE_TBLW1;
        reg2=CPSW3G_REGS->ALE_TBLW2;
		(void)reg2;
        if((((reg1 >>28) & 0x3) == 0x1)|| (((reg1 >>28) & 0x3)== 0x3))
        {
            if(((reg1&mask1) == info1) && ((reg0 & 0xFFFFFFFF) == info0))
            {
                if(vlanId < 4096)
                {
                    VID = reg1 >> 16;
                    if((VID & 0x0FFF) == vlanId)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }

    if(i != CPSW3G_ALE_TABLE_DEPTH)
    {
        ret = i;
    }

    return ret;
}



/** ============================================================================
 *  @n@b cpsw3g_AleFindVlan()
 *
 *  @b Description
 *  @n This function finds the specified VLAN entry in the ALE table.  The
 *  information associated with the entry is returned through the parameters.
 *
 *  @b Arguments
 *  @verbatim
 *      vlanId                  VLAN Id
 *      vlanMemberList          VLAN member list indicating the ports which
 *                              are mapped to this VLAN.
 *      unregMcastFloodMask     Unknown VLAN registered multicast flood mask
 *      regMcastFloodMask       registered VLAN multicast flood mask
 *      forceUntaggedEgress     VLAN force untagged egress
    @endverbatim
 *
 *  <b> Return Value </b>
 *  @n  -1                          - Error in finding the entry.
 *      index to the new entry added- Success.
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Int32 cpsw3g_AleFindVlan(Uint32 vlanId, Uint32 *vlanMemberList,
            Uint32 *unregMcastFloodMask, Uint32 *regMcastFloodMask,
            Uint32 *forceUntaggedEgress)
{
    Uint32 i=0;
    Uint32 info0=0,info1=0, VID=0;
    Int32 ret = -1;

    for(i=0;i<CPSW3G_ALE_TABLE_DEPTH;i++)
    {
        CPSW3G_REGS->ALE_TBLCTL = i;
        info0 = CPSW3G_REGS->ALE_TBLW0;
        info1 = CPSW3G_REGS->ALE_TBLW1;
        VID = info1 >> 16;
        if(((info1 >>28) & 0x3) == 0x2)
        {
            if((VID & 0x0FFF) == vlanId)
            {
                info0 = CPSW3G_REGS->ALE_TBLW0;
                if(i<CPSW3G_ALE_TABLE_DEPTH)
                {
                    if(0u != vlanMemberList)
                    {
                        *vlanMemberList = ( info0 & 0x00000007);
                    }
                    if(0u != unregMcastFloodMask)
                    {
                        *unregMcastFloodMask = ((info0 & 0x00000700) >> 8);
                    }
                    if(0u != regMcastFloodMask)
                    {
                        *regMcastFloodMask   = ((info0 & 0x00070000) >> 16);
                    }
                    if(0u != forceUntaggedEgress)
                    {
                        *forceUntaggedEgress = ((info0 & 0x07000000) >> 24);
                    }

                    ret = i;
                    break;
                }
            }/* end of id VID == vlanId block */
        }
    }/* end of for i loop */

    return ret;
}


/** ============================================================================
 *  @n@b cpsw3g_AleGetFreeEntry()
 *
 *  @b Description
 *  @n Finds index to the next free entry in ALE table.
 *
 *  @b Arguments
 *  @verbatim
 *      None
    @endverbatim
 *
 *  <b> Return Value </b>
 *  @n  Index to next free entry in ALE table.
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_AleGetFreeEntry()
{
    Uint32 i=0,reg1=0,ret=0;

    for(i=0; i<CPSW3G_ALE_TABLE_DEPTH; i++)
    {
        CPSW3G_REGS->ALE_TBLCTL = i ;
        reg1 = CPSW3G_REGS->ALE_TBLW1;

        if((reg1 & 0x30000000) == 0 )
	    {
            break;
	    }
    }

    if(i==CPSW3G_ALE_TABLE_DEPTH)
    {
        ret = CPSW3G_ALE_TABLE_FULL;
    }
    else
    {
        ret =i;
    }

    return ret;
}

/** ============================================================================
 *  @n@b cpsw3g_AleAddVlan()
 *
 *  @b Description
 *  @n This function adds a VLAN ALE entry to the table.
 *
 *  @b Arguments
 *  @verbatim
 *      vlanId                  VLAN Id
 *      vlanMemberList          VLAN member list indicating the ports which
 *                              are mapped to this VLAN.
 *      unregMcastFloodMask     Unknown VLAN registered multicast flood mask
 *      regMcastFloodMask       registered VLAN multicast flood mask
 *      forceUntaggedEgress     VLAN force untagged egress
    @endverbatim
 *
 *  <b> Return Value </b>
 *  @n  CPSW3G_VLAN_UNAWARE_MODE    - VLAN aware mode not enabled. error in adding
 *                                    the VLAN entry.
 *      CPSW3G_ALE_TABLE_FULL       - Error in adding the entry. ALE table full.
 *      index to the new entry added- Success.
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_AleAddVlan(Uint32 vlanId,Uint32 vlanMemberList,
        Uint32 unregMcastFloodMask, Uint32 regMcastFloodMask,
        Uint32 forceUntaggedEgress)
{
    Uint32 vlanAware =0,freeEntry=0,ret = CPSW3G_SUCCESS;
    Int32 vlanEntry = 0;

    /* Check the current setting for vlanAware */
    vlanAware = CSL_FEXT(CPSW3G_REGS->CPSW_CONTROL, CPSW3G_CPSW_CONTROL_VLAN_AWARE);

    if (vlanAware == 0)
    {
	    ret = (CPSW3G_VLAN_UNAWARE_MODE);
    }
    else
    {
        vlanId &= 0x0fff;
        vlanMemberList &= 0x7;
        unregMcastFloodMask &= 0x7;
        regMcastFloodMask &= 0x7;
        forceUntaggedEgress &= 0x7;

        vlanEntry = cpsw3g_AleFindVlan(vlanId,(void *)0,(void *)0,(void *)0,(void *)0);
        if((vlanEntry != -1) && (vlanEntry < CPSW3G_ALE_TABLE_DEPTH))
        {
            /* update existing entry */
            freeEntry = (Uint32)vlanEntry ;
        }
        else
        {
            /* Update VLAN Info */
            freeEntry = cpsw3g_AleGetFreeEntry();
        }

        if(freeEntry == CPSW3G_ALE_TABLE_FULL)
        {
            ret = freeEntry;
        }
        else
        {
	        CPSW3G_REGS->ALE_TBLW2 = 0;
	        CPSW3G_REGS->ALE_TBLW1 = (2<<28)|(vlanId<<16);
	        CPSW3G_REGS->ALE_TBLW0 = (forceUntaggedEgress<<24)|
                                     (regMcastFloodMask<<16)|(unregMcastFloodMask<<8)|
                                     (vlanMemberList<<0);
	        CPSW3G_REGS->ALE_TBLCTL = freeEntry | ((Uint32)CPSW3G_ALE_TABLE_WRITE);
	    }
    }

    return(ret);

}

/** ============================================================================
 *  @n@b cpsw3g_AleAddUniAddr()
 *
 *  @b Description
 *  @n This function adds a given Unicast address to the ALE table.
 *
 *  @b Arguments
 *  @verbatim
 *      hPort                   MAC port corresponding to the address
 *      addr                    Unicast IP address
 *      vlanId                  VLAN Id
 *      blocked                 flags to indicate whether packets matching this
 *                              IP must be dropped/forwarded.
 *      secure                  Flags to indicate if the packet with matching source
 *                              address should be dropped if the received port is
 *                              not same as table entry.
 *      ageable                 Flags to indicate if this address is ageable.
    @endverbatim
 *
 *  <b> Return Value </b>
 *  @n  error code          - Error in adding the entry.
 *      CPSW3G_SUCCESS      - Success.
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b> None
 *
 *  @b Example
    @endverbatim
 * ============================================================================
 */
static Uint32 cpsw3g_AleAddUniAddr(Cpsw3gPort *hPort, Uint8 * addr, Uint32 vlanId,
                                    Uint32 blocked, Uint32 secure, Uint32 ageable)
{
    Uint32 freeEntry, vlanAware, info0, info1, info2,ret;
    Int32 addrEntry = 0;

    /* Check the current setting for vlanAware */
    vlanAware = CSL_FEXT(CPSW3G_REGS->CPSW_CONTROL, CPSW3G_CPSW_CONTROL_VLAN_AWARE);

    /* ignore input vlanId if not in VLAN aware mode */
    if (vlanAware == 0)
    {
	    vlanId = 0;
    }
    /* Check for space in the ALE table */
    addrEntry = cpsw3g_AleFindAddress(addr, vlanId);
    if(addrEntry == -1)
    {
        freeEntry = cpsw3g_AleGetFreeEntry();
    }
    else
    {
        /* update the existing entry itself */
        freeEntry = (Uint32) addrEntry;
    }

    if (freeEntry == CPSW3G_ALE_TABLE_FULL)
    {
        ret = freeEntry;
    }
    else
    {
        /* Add new entry to ALE table */
        info2=(ageable<<2)|(hPort->portNum&3);
        info1=((blocked<<31)|(secure<<30)|(1<<28)|
            ((vlanId&0x0fff)<<16)|((addr[0]&0x0ff)<<8)|
            ((addr[1]&0x0ff)<<0));

        if(vlanId != 0)
        {
            info1 |= (1<<29);
        }

        info0=(((addr[2]&0x0ff)<<24)|((addr[3]&0x0ff)<<16)|
            ((addr[4]&0x0ff)<<8)|((addr[5]&0x0ff)<<0));

        CPSW3G_REGS->ALE_TBLW2 = info2;
        CPSW3G_REGS->ALE_TBLW1 = info1;
        CPSW3G_REGS->ALE_TBLW0 = info0;
        CPSW3G_REGS->ALE_TBLCTL = freeEntry | CPSW3G_ALE_TABLE_WRITE;
        ret = CPSW3G_SUCCESS;
    }

    return(ret);

}


/** ============================================================================
 *  @n@b  EMAC_open()
 *
 *  @b Description
 *  @n Opens the EMAC peripheral at the given physical index and initializes
 *     it to an embryonic state.
 *
 *     The calling application must supply a operating configuration that
 *     includes a callback function table. Data from this config structure is
 *     copied into the device's internal instance structure so the structure
 *     may be discarded after EMAC_open() returns. In order to change an item
 *     in the configuration, the EMAC device must be closed and then
 *     re-opened with the new configuration.
 *
 *     The application layer may pass in an hApplication callback handle,
 *     that will be supplied by the EMAC device when making calls to the
 *     application callback functions.
 *
 *     An EMAC device handle is written to phEMAC. This handle must be saved
 *     by the caller and then passed to other EMAC device functions.
 *
 *     The default receive filter prevents normal packets from being received
 *     until the receive filter is specified by calling EMAC_receiveFilter().
 *
 *     A device reset is achieved by calling EMAC_close() followed by EMAC_open().
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error codes include:
 *       CPSW3G_ERR_DEV_ALREADY_OPEN   - The device is already open
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        physicalIndex   physical index
        hApplication    application handle
        pEMACConfig     EMAC's configuration structure
        phEMAC          handle to the EMAC device
    @endverbatim
 *
 *
 *  <b> Return Value </b>  Success (0)
 *  @n     CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *  @n     CPSW3G_ERR_DEV_ALREADY_OPEN   - The device is already open
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Opens the EMAC peripheral at the given physical index and initializes it.
 *
 *  @b Example
 *  @verbatim
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_open(int physicalIndex, Handle hApplication,
                 Cpsw3gInitConfig *pEMACConfig, Handle *phEMAC)
{
    int              i, index;
	EMAC_AddrConfig  *tmp;


    /* If the device is alread open, return an error */
    if (openFlag)
        return( CPSW3G_ERR_DEV_ALREADY_OPEN );

    /*
     *  Validate the supplied configuration structure
     */
    if (!pEMACConfig || !phEMAC)
        return (CPSW3G_INVALID_PARAM);

    /* MAC address must be supplied and not a multicast address */
	for (index=0; (Uint8)index<(pEMACConfig->TotalNumOfMacAddrs); index++){
		tmp = &(pEMACConfig->MacAddr[index]);
	if(tmp->Addr[0] & 1 )
		return( CPSW3G_INVALID_PARAM );
	    for (i = 0; (i < 6) && (!(tmp->Addr[i])); i++)
	if (i == 6)
		return (CPSW3G_INVALID_PARAM);
	}

    /* Callback functions must be supplied */
    if (    !pEMACConfig->pfcbFreePacket ||
            !pEMACConfig->pfcbRxPacket   ||
			!pEMACConfig->pfcbStatus     ||
            !pEMACConfig->pfcbStatistics)
        return (CPSW3G_INVALID_PARAM);

    /*
     * Init the instance structure
     */

    /* Default everything in our instance structure to zero */
    memset(&localDev, 0, sizeof(Cpsw3gDevice));

    /* Set the hApplication and RxFilter */
    localDev.hApplication = hApplication;

	localDev.Weight       = ETH_OFFLOAD_HWQ_WEIGHT;

    /* Setup the new configuration */
    localDev.Config = *pEMACConfig;

    /* there are soft resets in indv. modules, and one from LPSC */
    /* order of reset CPDMA->SL0->SL1->3G->SG0->SG1->3Gss */

    /* Soft reset CPDMA */
    CSL_FINS(CPSW3G_REGS->SOFT_RESET, CPSW3G_SOFT_RESET_SOFT_RESET, CSL_CPSW3G_SOFT_RESET_SOFT_RESET_RESET);
    while(CSL_FEXT(CPSW3G_REGS->SOFT_RESET, CPSW3G_SOFT_RESET_SOFT_RESET) !=
		CSL_CPSW3G_SOFT_RESET_SOFT_RESET_NORESET)
    {
    }
    /* soft reset CPGMAC_SL0 */
    CSL_FINS(CPSW3G_REGS->GMAC[0].SL_SOFT_RESET,
             CPSW3G_SL_SOFT_RESET_SOFT_RESET, CSL_CPSW3G_SL_SOFT_RESET_SOFT_RESET_RESET);
    while(CSL_FEXT(CPSW3G_REGS->GMAC[0].SL_SOFT_RESET, CPSW3G_SL_SOFT_RESET_SOFT_RESET) !=
		CSL_CPSW3G_SL_SOFT_RESET_SOFT_RESET_NORESET)
    {
    }

    /* soft reset CPGMAC_SL1 */
    CSL_FINS(CPSW3G_REGS->GMAC[1].SL_SOFT_RESET,
             CPSW3G_SL_SOFT_RESET_SOFT_RESET, CSL_CPSW3G_SL_SOFT_RESET_SOFT_RESET_RESET);
    while(CSL_FEXT(CPSW3G_REGS->GMAC[1].SL_SOFT_RESET, CPSW3G_SL_SOFT_RESET_SOFT_RESET) !=
		CSL_CPSW3G_SL_SOFT_RESET_SOFT_RESET_NORESET)
    {
    }

    /*soft reset CPSW_3G */
    CSL_FINS(CPSW3G_REGS->CPSW_SOFT_RESET,
             CPSW3G_CPSW_SOFT_RESET_SOFT_RESET, CSL_CPSW3G_CPSW_SOFT_RESET_SOFT_RESET_RESET);
    while(CSL_FEXT(CPSW3G_REGS->CPSW_SOFT_RESET, CPSW3G_CPSW_SOFT_RESET_SOFT_RESET) !=
		CSL_CPSW3G_CPSW_SOFT_RESET_SOFT_RESET_NORESET)
    {
    }

    /* Soft reset CPSW_3G_SS */
    CSL_FINS(ECTL_REGS->SOFT_RESET,
		ECTL_SOFT_RESET_SOFT_RESET,CSL_ECTL_SOFT_RESET_SOFT_RESET_YES);

    /* Open all ports */
    for(i=0;i<CPSW3G_NUM_PORTS; i++)
    {
	    localDev.cpsw3gPort[i].portNum = i;
	    localDev.cpsw3gPort[i].chNum = i;
	    {
		cpsw3g_port_open(&localDev.cpsw3gPort[i]);
	    }

    }/* end of for loop over PORTS */

    /* Add vlan entry into Ale for default PORT VID*/
    if(pEMACConfig->cpswCfg.cpswCtlModeFlags & EMAC_CONFIG_CPSW_ENVLANAWARE)
    {
        /* Modified Unreg_multicast_mask from 0x7 to 0x3(0x6 for centaurus?) to exclude CPU port*/
        cpsw3g_AleAddVlan(CPSW3G_DEFAULT_MAC_PORTVID,CPSW3G_DEFAULT_UNK_VLANMEMLIST,
                6,7,CPSW3G_DEFAULT_UNK_FORCE_UNTAG_EGR);
    }

    /* Kick start transfer queues here */
    /* Transmission should be started if we have link on port 0/1 */

    /* Add ALE entry for CPU port.i.e.Port 2 - DM648, Port 0 - centaurus */
    cpsw3g_AleAddUniAddr(&localDev.cpsw3gPort[0],localDev.Config.MacAddr[0].Addr,0,0,1,0);

    /* Activate the ALE ageout timer here */
    localDev.Config.aleTimerActive = 1;

	/* Validate the device handle */
    localDev.DevMagic = EMAC_DEVMAGIC;

    /* Set the open flag */
    openFlag = 1;

    /* Give a handle back to the caller */
    *phEMAC = &localDev;

    /* Return Success */
    return( CPSW3G_SUCCESS );
}



/** ============================================================================
 *  @n@b  EMAC_close()
 *
 *  @b Description
 *  @n Closed the EMAC peripheral indicated by the supplied instance handle.
 *     When called, the EMAC device will shutdown both send and receive
 *     operations, and free all pending transmit and receive packets.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *         CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        hEMAC   handle to opened the EMAC device
    @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC_open function must be called before calling this API.
 *
 *  <b> Post Condition </b>
 *  @n  The EMAC device will shutdown both send and receive
 *      operations, and free all pending transmit and receive packets.
 *
 *  @b Example
 *  @verbatim
        Cpsw3gInitConfig  ecfg;
        Handle       hEMAC = 0;
        //Open the EMAC peripheral
        EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

        //Close the EMAC peripheral
        EMAC_close( hEMAC );
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_close(Handle hEMAC)
{
	Cpsw3gDevice    *pd = (Cpsw3gDevice *)hEMAC;
    Uint32          index;


    /* Validate our handle */
    if (!pd || pd->DevMagic != EMAC_DEVMAGIC)
        return (CPSW3G_INVALID_PARAM);

    /* Close all ports */
    for(index=0;index<CPSW3G_NUM_PORTS; index++)
    {
	    cpsw3g_port_close(&pd->cpsw3gPort[index]);
    }/* end of for loop over PORTS */

    /* De-Activate the ALE ageout timer here */
    pd->Config.aleTimerActive = 0;

    /* Invalidate the EMAC handle */
    pd->DevMagic = 0;

    /* Clear the open flag */
    openFlag = 0;

    /* Exit with interrupts still disabled in the wrapper */
    return (CPSW3G_SUCCESS);
}


/**
 * EMAC_update: Update Phy status
 * @update: Update structure to update emac status
 *
 * Updates phy status and takes action for network queue if required
 * based upon link status
 *
 */
Int32 EMAC_update(struct EMAC_updateData *update)
{
	Uint32 mac_control;
	Uint32 cur_duplex;
	Uint32 instanceNum;

	if(update->phy_id == 0)
		instanceNum = 0;
	else
		instanceNum = 1;

	mac_control = CPSW3G_REGS->GMAC[instanceNum].SL_MACCONTROL;
	cur_duplex  = (mac_control & CSL_CPSW3G_GMAC_MACCONTROL_FULLDUPLEX_MASK) ? 1 : 0;

	/* We get called only if link has changed (speed/duplex/status) */
	if (update->linkup && (update->duplex != cur_duplex)) {
		if (1 == update->duplex)
			CSL_FINST(mac_control,
					CPSW3G_GMAC_MACCONTROL_FULLDUPLEX, ENABLE);
		else
			CSL_FINST(mac_control,
					CPSW3G_GMAC_MACCONTROL_FULLDUPLEX, DISABLE);
	}

	if (update->speed == 1000) {
		CSL_FINST(mac_control, CPSW3G_GMAC_MACCONTROL_GIG_FORCE, ONE);
		CSL_FINST(mac_control, CPSW3G_GMAC_MACCONTROL_GIG, ENABLE);
	} else {
		/* Clear the GIG bit and GIGFORCE bit */
		CSL_FINST(mac_control, CPSW3G_GMAC_MACCONTROL_GIG_FORCE, ZERO);
		CSL_FINST(mac_control, CPSW3G_GMAC_MACCONTROL_GIG, DISABLE);
	}
	mac_control = CPSW3G_REGS->GMAC[instanceNum].SL_MACCONTROL;

	return 0;
}


/** ============================================================================
 *  @n@b  EMAC_start(Handle hEMAC)
 *
 *  @b Description
 *  @n Enable Rx and Tx the EMAC peripheral indicated by the supplied instance handle.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *         EMAC_ERROR_NOTREADY - Rx Desc is still not filled up
 *
 *  @b Arguments
 *  @verbatim
 hEMAC   handle to opened the EMAC device
 @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      EMAC_ERROR_NOTREADY   - Rx Desc is still not filled up
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC_open function must be called before calling this API.
 *
 *  <b> Post Condition </b>
 *  @n  The EMAC device will be ready to send and received data
 *
 *  @b Example
 *  @verbatim
 EMAC_Config  ecfg;
 Handle       hEMAC = 0;
//Open the EMAC peripheral
EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

//Close the EMAC peripheral
EMAC_start( hEMAC );
@endverbatim
 */
Int32 EMAC_start(Handle hEMAC)
{
	Cpsw3gDevice     *pd = (Cpsw3gDevice *)hEMAC;
	volatile Uint32 *pRegAddr;
	Uint32 i;

	/*
	 * If we didn't get the number of descriptor buffers that the
	 * application said we should, then the app lied to us. This is
	 * bad because we'll be constantly calling to the app to fill
	 * up our buffers. So we'll close now to make the problem
	 * obvious.
	 */
	for (i = 0; i < CPDMA_MAX_CHANNELS; i++) {
		if((1<<i) & (pd->Config.chInfo[NET_CH_DIR_RX][corenum].RxChanEnable)) {
			if (EMAC_depth(&pd->cpdma.rxCppi[i].DescHwQ) + 1 < localDev.Config.RxMaxPktPool) {
				/* Return the error condition */
				return (-1);
			}
		}
	}

	/* Startup RX */
	for (i = 0; i < CPDMA_MAX_CHANNELS; i++) {
		if((1<<i) & (pd->Config.chInfo[NET_CH_DIR_RX][corenum].RxChanEnable)) {
			pRegAddr = &(CPSW3G_REGS->RX_HDP[0]);
			*(pRegAddr + i) = (Uint32)pd->cpdma.rxCppi[i].DescHwQ.head;
		}
	}

	return 0;
}

/** ============================================================================
 *  @n@b  EMAC_setReceiveFilter()
 *
 *  @b Description
 *  @n Called to set the packet filter for received packets. The filtering
 *     level is inclusive, so BROADCAST would include both BROADCAST and
 *     DIRECTED (UNICAST) packets.
 *
 *     Available filtering modes include the following:
 *         - EMAC_RXFILTER_NOTHING      - Receive nothing
 *         - EMAC_RXFILTER_DIRECT       - Receive only Unicast to local MAC addr
 *         - EMAC_RXFILTER_BROADCAST    - Receive direct and Broadcast
 *         - EMAC_RXFILTER_MULTICAST    - Receive above plus multicast in mcast list
 *         - EMAC_RXFILTER_ALLMULTICAST - Receive above plus all multicast
 *         - EMAC_RXFILTER_ALL          - Receive all packets
 *
 *     Note that if error frames and control frames are desired, reception of
 *     these must be specified in the device configuration.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *         CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
         hEMAC           handle to the opened EMAC device
         ReceiveFilter   Filtering modes
         masterChannel   Channel number on which broadcast is to be enabled.
    @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API
 *
 *  <b> Post Condition </b>
 *  @n  Sets the packet filter for received packets
 *
 *  @b Example
 *  @verbatim
        #define EMAC_RXFILTER_DIRECT       1
        Cpsw3gInitConfig  ecfg;
        Handle       hEMAC = 0;

        EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

        EMAC_setReceiveFilter(hEMAC, EMAC_RXFILTER_DIRECT );

    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_setReceiveFilter(Handle hEMAC, Uint32 ReceiveFilter, Uint8 masterChannel)
{
    Cpsw3gDevice  *pd = (Cpsw3gDevice *)hEMAC;

    /* Validate our handle */
    if (!pd || pd->DevMagic != EMAC_DEVMAGIC || ReceiveFilter > EMAC_RXFILTER_ALL)
        return (CPSW3G_INVALID_PARAM);

	pd->Config.RxFilter = ReceiveFilter;

	return (0);
}

/** ============================================================================
 *  @n@b  EMAC_getReceiveFilter()
 *
 *  @b Description
 *  @n Called to get the current packet filter setting for received packets.
 *     The filter values are the same as those used in EMAC_setReceiveFilter().
 *
 *     The current filter value is written to the pointer supplied in
 *     pReceiveFilter.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *       CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
        hEMAC           handle to the opened EMAC device
        pReceiveFilter  Current receive packet filter
    @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      CPSW3G_INVALID_PARAM   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API and
 *      must be set the packet filter value.
 *
 *  <b> Post Condition </b>
 *  @n  The current filter value is written to the pointer supplied
 *
 *  @b Example
 *  @verbatim
        #define EMAC_RXFILTER_DIRECT       1
        Cpsw3gInitConfig  ecfg;
        Handle       hEMAC = 0;
        Uint32         pReceiveFilter;

        EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

        EMAC_setReceiveFilter(hEMAC, EMAC_RXFILTER_DIRECT );

        EMAC_getReceiveFilter(hEMAC, &pReceiveFilter );
    @endverbatim
 * ============================================================================
 */
Uint32 EMAC_getReceiveFilter(Handle hEMAC, Uint32 *pReceiveFilter)
{
    Cpsw3gDevice  *pd = (Cpsw3gDevice *)hEMAC;

    /* Validate our handle */
    if (!pd || pd->DevMagic != EMAC_DEVMAGIC || !pReceiveFilter)
        return (CPSW3G_INVALID_PARAM);

    *pReceiveFilter = pd->Config.RxFilter;
    return (0);
}


/**
 * EMAC_process: Process all intr in hw
 * @src: Source Desc Q (usually HwDescQ)
 * @dst: Destination Desc Q (usually Local Q)
 * @weight: Max desc tto be processed in this call
 * @callback: Callback to be called after deq
 * @regcp: CP reg pointer
 * @reghdp: HDP reg pointer
 *
 * Process Rx/Tx intrs
 *
 */

static UInt32 EMAC_process(Cpsw3gDevice *pd, PKTQ *hwq, PKTQ *freeq, Uint32 weight,
			void (*callback)(Handle, Uint32, Uint32),
			volatile Uint32 *regcp, volatile Uint32 *reghdp)
{
	volatile Uint32 end = *regcp;
	EMAC_Desc *desc = (EMAC_Desc *)end;
	EMAC_Desc *next = (EMAC_Desc *)desc->pNext;

	/* Check if packet is still owned by the hw */
	if (desc->PktFlgLen & EMAC_DSC_FLAG_OWNER) {
		return weight;
	} else if (weight == 0 || EMAC_empty(hwq)) {
		Vps_rprintf("%s: Hitting spurious case w=%d\n", __func__, weight);
		return weight;
	}

	/* If the hw has stopped and we have more descriptors, then restart */

	if (next && (desc->PktFlgLen & EMAC_DSC_FLAG_EOQ)) {
		*reghdp = (Uint32)next;
	}

	/* Process the finished packets now */
	do {
		if (desc = (EMAC_Desc *)EMAC_pop(hwq)) {
			/* Call tx callback for free in the packet */
			callback(pd->hApplication, desc->SwField, desc->PktFlgLen);
			EMAC_push(freeq, desc);
		} else {
			Vps_rprintf(" [ETH_OFFLOAD] NULL hw desc!!!!");
			break;
		}
		/* Acknowledge */
		*regcp = (Uint32)desc;

	} while ((Uint32)desc != end && weight--);


	return weight;
}

/** ============================================================================
 *  @n@b EMAC_updateStats()
 *
 *  @b Description
 *  @n Update our local copy of the statistics
 *
 *  @b Arguments
 *  @verbatim
        pd  pointer to EMAC object
    @endverbatim
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened
 *
 *  <b> Post Condition </b>
 *  @n  Update local copy of the statistics to the EMAC registers
 *
 *  @b Example
 *  @verbatim
        Cpsw3gDevice *pd;

        EMAC_updateStats( pd );
    @endverbatim
 * ============================================================================
 */
static void EMAC_updateStats( Cpsw3gDevice *pd )
{
    int             i;
    volatile Uint32 *pRegAddr;
    Uint32          *pStatAddr;
    Uint32          statval;

    pRegAddr = &CPSW3G_REGS->RXGOODFRAMES;
    pStatAddr = (Uint32 *)(&pd->Stats);

    /*
     * There are "EMAC_NUMSTATS" statistics registers
     * Note that when GMIIEN is set in MACCONTROL, these registers
     * are "write to decrement".
     */
    for (i = 0; i < EMAC_NUMSTATS; i++)
    {
        statval = *pRegAddr;
        *pRegAddr++ = statval;
        statval += *pStatAddr;
        *pStatAddr++ = statval;
    }
}

/** ============================================================================
 *  @n@b  EMAC_getStatistics()
 *
 *  @b Description
 *  @n Called to get the current device statistics. The statistics structure
 *     contains a collection of event counts for various packet sent and
 *     receive properties. Reading the statistics also clears the current
 *     statistic counters, so the values read represent a delta from the last
 *     call.
 *
 *     The statistics information is copied into the structure pointed to
 *     by the pStatistics argument.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
 hEMAC       handle to the opened EMAC device
 pStatistics Get the device statistics
 @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API
 *
 *  <b> Post Condition </b>
 *      -# Statistics are read for various packects sent and received.
 -# Reading the statistics also clears the current
 statistic counters, so the values read represent a delta from the
 last call.
 *
 *  @b Example
 *  @verbatim
 EMAC_Config      ecfg;
 Handle           hEMAC = 0;
 EMAC_Statistics  pStatistics;

 EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

 EMAC_getStatistics(hEMAC, &pStatistics );
 @endverbatim
 * ============================================================================
 */
Uint32 EMAC_getStatistics(Handle hEMAC, EMAC_Statistics *pStatistics)
{
	Cpsw3gDevice  *pd = (Cpsw3gDevice *)hEMAC;

	/* Validate our handle */
	if(!pd || pd->DevMagic != EMAC_DEVMAGIC || !pStatistics)
		return (CPSW3G_INVALID_PARAM);

	/* Update the stats */
	EMAC_updateStats(pd);

	/* Copy the updated stats to the application */
	*pStatistics = pd->Stats;

	/* Clear our copy */
	memset(&pd->Stats, 0, sizeof(EMAC_Statistics));

	return (0);
}




Uint32 EMAC_check(Handle hEMAC, Uint32 weight)
{
	Cpsw3gDevice    *pd = (Cpsw3gDevice *)hEMAC;
	Uint32          intflags;
	int             i;


	/* Validate our handle */
	if (!pd || pd->DevMagic != EMAC_DEVMAGIC)
	return (CPSW3G_INVALID_PARAM);

	/* Read the interrupt cause */
	intflags = CPSW3G_REGS->CPDMA_IN_VECTOR;

	/* Look for fatal errors first */
	if (intflags & CSL_FMK(CPSW_MACINVECTOR_HOSTPEND, 1))
	{
		/* Read the error status - we'll decode it by hand */
		pd->FatalError = CPSW3G_REGS->DMASTATUS;

		/* Tell the application */
		(*localDev.Config.pfcbStatus)(pd->hApplication, pd->FatalError);

		/*ack by writing to EOI */
		CPSW3G_REGS->CPDMA_EOI_VECTOR = 0x3u;

		/* return with interrupt disabled in the wrapper */
		return (CPSW3G_ERR_MACFATAL);
	}

	/* Look for statistics interrupt */
	if (intflags & CSL_FMK(CPSW_MACINVECTOR_STATPEND, 1))
	{
		/* Read the stats and reset to zero         */
		/* This is necessary to clear the interrupt */
		EMAC_updateStats(pd);

		/* Tell the application */
		(*localDev.Config.pfcbStatistics)(pd->hApplication);

		/*ack by writing to EOI */
		CPSW3G_REGS->CPDMA_EOI_VECTOR = 0x3u;
	}

    Uint32          numRxTx = 0;

	/* Look for Rx/Tx interrupt */
	for(i=0; i<CPDMA_MAX_CHANNELS; i++) {
		/* Check for Rx first */

		/* find out which channel is enabled */
		if((1<<i) & (pd->Config.chInfo[NET_CH_DIR_RX][corenum].RxChanEnable)) {
			/* find out if interrupt happend */
			if (intflags & CSL_FMK(CPSW_MACINVECTOR_RXPEND, 1 << i)) {
				pd->cpdma.rxCppi[i].ChannelIndex = i;
				EMAC_process(pd, &pd->cpdma.rxCppi[i].DescHwQ,
						&pd->cpdma.rxCppi[i].DescFreeQ,
						weight, pd->Config.pfcbRxPacket,
						(&CPSW3G_REGS->RX_CP[0]) + i,
						(&CPSW3G_REGS->RX_HDP[0]) + i);
				CPSW3G_REGS->CPDMA_EOI_VECTOR = 0x1;
                numRxTx++;
			}
		}
		/* Check for Tx next */

		/* find out which channel is enabled */
		if((1<<i) & (pd->Config.chInfo[NET_CH_DIR_TX][corenum].TxChanEnable)) {
			/* find out if interrupt happend */
			if (intflags & CSL_FMK(CPSW_MACINVECTOR_TXPEND, 1 << i)) {
				pd->cpdma.txCppi[i].ChannelIndex = i;
				EMAC_process(pd, &pd->cpdma.txCppi[i].DescHwQ,
						&pd->cpdma.txCppi[i].DescFreeQ,
						weight, pd->Config.pfcbFreePacket,
						(&CPSW3G_REGS->TX_CP[0]) + i,
						(&CPSW3G_REGS->TX_HDP[0]) + i);
				CPSW3G_REGS->CPDMA_EOI_VECTOR = 0x2;
                numRxTx++;
			}
		}
	}

	return numRxTx;
}

Int32 ETH_OFFLOAD_EMAC_Poll(Void *arg)
{
	Cpsw3gDevice *pd = (Cpsw3gDevice *)arg;

	/* Process all desc that are "done" */
	return EMAC_check(pd, pd->Weight);
}
