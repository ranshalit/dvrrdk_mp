
#include <mcfw/src_bios6/utils/utils.h>

#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <mcfw/src_bios6/eth_offload/inc/eth_offload_rpc.h>
#if defined(TI_8107_BUILD) || defined(TI_814X_BUILD)
#include <mcfw/src_bios6/eth_offload/inc/eth_offload_cpsw.h>
#else
#include <mcfw/src_bios6/eth_offload/inc/eth_offload_emac.h>
#endif
#include <mcfw/src_bios6/eth_offload/bios6/inc/eth_offload_main.h>

/*
 *ETH_OFFLOAD_init:init Offload engine specific resources
*/
int ETH_OFFLOAD_init(void)
{
	Int32 status;

	Vps_rprintf(" ETH_OFFLOAD: Init !!!\n");

	status = ETH_OFFLOAD_Rpc_Init();
	if (status < 0)
	{
		Vps_rprintf (" [ETH_OFFLOAD] RPC init failed !!!\n");
		return -1;
	}
#if defined(TI_8107_BUILD) || defined(TI_814X_BUILD)
	status = ETH_OFFLOAD_Cpsw_Init();
#endif
#if defined(TI_816X_BUILD)
	status = ETH_OFFLOAD_Emac_Init();
#endif

	if (status < 0)
	{
		Vps_rprintf (" [ETH_OFFLOAD] EMAC init failed !!!\n");
		return -1;
	}

	return 0;
}

/*
 *ETH_OFFLOAD_deInit:De init offload engine specific resources
*/
int ETH_OFFLOAD_deInit()
{
	Int32 status;

	Vps_rprintf(" ETH_OFFLOAD: DeInit !!!\n");

	status = ETH_OFFLOAD_Rpc_DeInit();
	if (status < 0)
	{
        Vps_rprintf (" [ETH_OFFLOAD] RPC deinit failed !!!\n");
		return -1;
	}
#if defined(TI_8107_BUILD) || defined(TI_814X_BUILD)
	status = ETH_OFFLOAD_Cpsw_DeInit();
#endif
#if defined(TI_816X_BUILD)
	status = ETH_OFFLOAD_Emac_DeInit();
#endif
	if (status < 0)
	{
		Vps_rprintf (" [ETH_OFFLOAD] EMAC deinit failed !!!\n");
		return -1;
	}
	return 0;
}

