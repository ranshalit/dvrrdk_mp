/*  ============================================================================
 *   Copyright (c) Texas Instruments Inc 2002, 2003, 2004, 2005, 2006
 *
 *   Use of this software is controlled by the terms and conditions found in the
 *   license agreement under which this software has been supplied.
 *   ===========================================================================
 */

/** ============================================================================
 *   @file  csl_emac.c
 *
 *   PATH:  \$(CSLPATH)\\src\\emac
 *
 *   @brief  EMAC CSL Implementation on DSP side
 *
 *  \par
 *  NOTE:
 *  When used in an multitasking environment, no EMAC function may be
 *  called while another EMAC function is operating on the same device
 *  handle in another thread. It is the responsibility of the application
 *  to assure adherence to this restriction.
 *
 *  \par
 *  Although the EMAC API is defined to support multiple device instances,
 *  this version supports a single device instance
 */

/* =============================================================================
 *  Revision History
 *  ===============
 *  10-Jul-07 PSK   Added multiple MAC addresses support per channel and Si bug workaround
 *  09-Jan-07 dzhou To support all cores use EMAC simultaneously
 * =============================================================================
 */
#include <string.h>
#include <stdio.h>

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <ti/psp/vps/vps.h>

#include <mcfw/src_bios6/utils/utils_trace.h>
#include <mcfw/src_bios6/links_common/system/system_priv_common.h>

#include <mcfw/src_bios6/eth_offload/bios6/inc/emac/cslr_ectl.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/emac/csl_emac.h>




#define EMAC_CORENUM 0
#define EMAC_MAX_DESCRIPTORS	256

#define EMAC_DM646X_CMINTMAX_CNT	63
#define EMAC_DM646X_CMINTMIN_CNT	2
#define EMAC_DM646X_INTPACEEN		(0x30 << 16)
#define EMAC_DM646X_INTPRESCALE_MASK	(0x7FF << 0)
#define EMAC_DM646X_CMINTMAX_INTVL	(1000 / EMAC_DM646X_CMINTMIN_CNT)
#define EMAC_DM646X_CMINTMIN_INTVL	((1000 / EMAC_DM646X_CMINTMAX_CNT) + 1)


/**< Flag to indicate if the EMAC is already opened.	*/
static Uint32           openFlag = 0;

/**< Local copy of the EMAC device instance		*/
static EMAC_Device      localDev;

/* Tx channels mask for all the three cores		*/
Uint32                  TxChanEnable_all = 0;

/* Rx channels mask for all the three cores		*/
Uint32                  RxChanEnable_all = 0;

/* Core number						*/
Uint32			corenum = EMAC_CORENUM;

CSL_EmacRegs    *EMAC_REGS = 0;
CSL_EctlRegs    *ECTL_REGS = 0;

/*
 * Local Helper Functions
 */
extern Int32 Vps_rprintf(char * format, ... );
extern struct ETH_OFFLOAD_offloadStats offload_stats;

static void EMAC_updateStats(EMAC_Device *pd);
Uint32 EMAC_check(Handle hEMAC, Uint32 weight);


/** ============================================================================
 *  @n@b EMAC_pop()
 *
 *  @b Description
 *  @n Pop a desc buffer off a queue
 *
 *  @b Arguments
 *  @verbatim
 q      pointer to desc queue
 @endverbatim
 *
 *  <b> Return Value </b>  Pointer to EMAC desc
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Descriptor buffer from the Queue is removed
 *
 *  @b Example
 *  @verbatim
 EMAC_Desc *q;
 EMAC_pop( q );
 @endverbatim
 * ============================================================================
 */
static EMAC_Desc *EMAC_pop(EMAC_q *q)
{
	EMAC_Desc *desc = q->head;

	if (desc) {
		q->head = (EMAC_Desc *)desc->pNext;
		desc->pNext = 0;
		q->depth--;
	}

	return desc;
}

/** ============================================================================
 *  @n@b EMAC_push()
 *
 *  @b Description
 *  @n Push a desc onto a queue
 *
 *  @b Arguments
 *  @verbatim
 q     pointer to desc queue
 desc  pointer to the EMAC desc
 @endverbatim
 *
 *
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Descriptor buffer from the Queue is added
 *
 *  @b Example
 *  @verbatim
 EMAC_q *q;
 EMAC_Desc *desc

 EMAC_push( q, desc );
 @endverbatim
 * ============================================================================
 */
static void EMAC_push(EMAC_q *q, EMAC_Desc *desc)
{
    volatile UInt32 tmpRead; /* to ensure data is pushed to memory */
	desc->pNext = 0;

	if (!q->head) {
		// Queue is empty
		q->head = desc;
	} else {
		// Queue is not empty
		q->tail->pNext = desc;

        tmpRead = (UInt32)q->tail->pNext;
	}
	q->tail = desc;
	q->depth++;
}

Uint32 EMAC_move(EMAC_q *d, EMAC_q *s)
{
	if (!d->head) {
		d->head = s->head;
	} else {
		d->tail->pNext = s->head;
	}
	d->tail = s->tail;
	d->depth += s->depth;

	s->head = 0;
	s->depth = 0;

    return 0;
}

static Uint32 EMAC_empty(EMAC_q *q)
{
	return q->depth == 0;
}

static Uint32 EMAC_depth(EMAC_q *q)
{
	return q->depth;
}

/** ============================================================================
 *  @n@b EMAC_updateStats()
 *
 *  @b Description
 *  @n Update our local copy of the statistics
 *
 *  @b Arguments
 *  @verbatim
 pd  pointer to EMAC object
 @endverbatim
 *  <b> Return Value </b>  None
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened
 *
 *  <b> Post Condition </b>
 *  @n  Update local copy of the statistics to the EMAC registers
 *
 *  @b Example
 *  @verbatim
 EMAC_Device *pd;

 EMAC_updateStats( pd );
 @endverbatim
 * ============================================================================
 */
static void EMAC_updateStats( EMAC_Device *pd )
{
	int             i;
	volatile Uint32 *pRegAddr;
	Uint32          *pStatAddr;
	Uint32          statval;

	pRegAddr = &EMAC_REGS->RXGOODFRAMES;
	pStatAddr = (Uint32 *)(&pd->Stats);

	/*
	 * There are "EMAC_NUMSTATS" statistics registers
	 * Note that when GMIIEN is set in MACCONTROL, these registers
	 * are "write to decrement".
	 */
	for (i = 0; i < EMAC_NUMSTATS; i++) {
		statval = *pRegAddr;
		*pRegAddr++ = statval;
		statval += *pStatAddr;
		*pStatAddr++ = statval;
	}
}

Int32 ETH_OFFLOAD_EMAC_Poll(Void *arg)
{
	EMAC_Device *pd = (EMAC_Device *)arg;

	/* Process all desc that are "done" */
	return EMAC_check(pd, pd->Weight);
}


/*-----------------------------------------------------------------------*\
 * STANDARD API FUNCTIONS
 *
 * Note on Exclusion (Serialization):
 *   The application is charged with verifying that only one of the
 * following API calls may only be executing at a given time across
 * all threads and all interrupt functions.
 *
 *-----------------------------------------------------------------------*/


/** ============================================================================
 *  @n@b  EMAC_enumerate()
 *
 *  @b Description
 *  @n Enumerates the EMAC peripherals installed in the system and returns an
 *     integer count. The EMAC devices are enumerated in a consistent
 *     fashion so that each device can be later referenced by its physical
 *     index value ranging from "1" to "n" where "n" is the count returned
 *     by this function.
 *
 *  <b> Return Value </b>  Uint32
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  None
 *
 *  @b Example
 *  @verbatim
 EMAC_enumerate( );
 @endverbatim
 * ============================================================================
 */

Uint32 EMAC_enumerate(Void)
{
	return (1);
}

Uint32 EMAC_init(Void)
{
	//EMAC_intrOpen(&localDev, ETH_OFFLOAD_Emac_emacIsr);

	return 1;
}

Uint32 EMAC_deinit(Void)
{

	return 1;
}

Uint32 EMAC_set_coalesce (Uint32 rx_coalesce_usecs, Uint32 bus_freq_mhz)
{
	Uint32 int_ctrl, num_interrupts = 0;
	Uint32 prescale = 0, addnl_dvdr = 1, coal_intvl = 0;

	if (!rx_coalesce_usecs)
		return 0;

	coal_intvl = rx_coalesce_usecs;

	int_ctrl =  ECTL_REGS->INTCONTROL;
	prescale = bus_freq_mhz * 4;

	if (coal_intvl < EMAC_DM646X_CMINTMIN_INTVL)
		coal_intvl = EMAC_DM646X_CMINTMIN_INTVL;

	if (coal_intvl > EMAC_DM646X_CMINTMAX_INTVL) {
		/*
		 * Interrupt pacer works with 4us Pulse, we can
		 * throttle further by dilating the 4us pulse.
		 */
		addnl_dvdr = EMAC_DM646X_INTPRESCALE_MASK / prescale;

		if (addnl_dvdr > 1) {
			prescale *= addnl_dvdr;
			if (coal_intvl > (EMAC_DM646X_CMINTMAX_INTVL
						* addnl_dvdr))
				coal_intvl = (EMAC_DM646X_CMINTMAX_INTVL
						* addnl_dvdr);
		} else {
			addnl_dvdr = 1;
			coal_intvl = EMAC_DM646X_CMINTMAX_INTVL;
		}
	}

	num_interrupts = (1000 * addnl_dvdr) / coal_intvl;

	int_ctrl |= EMAC_DM646X_INTPACEEN;
	int_ctrl &= (~EMAC_DM646X_INTPRESCALE_MASK);
	int_ctrl |= (prescale & EMAC_DM646X_INTPRESCALE_MASK);
	ECTL_REGS->INTCONTROL = int_ctrl;

	ECTL_REGS->C0RXIMAX = num_interrupts;
	ECTL_REGS->C0TXIMAX = num_interrupts;

        Vps_rprintf("OFFLOAD: Set coalesce to %d usecs.\n", coal_intvl);
	return coal_intvl;
}

/** ============================================================================
 *  @n@b  EMAC_open()
 *
 *  @b Description
 *  @n Opens the EMAC peripheral at the given physical index and initializes
 *     it to an embryonic state.
 *
 *     The calling application must supply a operating configuration that
 *     includes a callback function table. Data from this config structure is
 *     copied into the device's internal instance structure so the structure
 *     may be discarded after EMAC_open() returns. In order to change an item
 *     in the configuration, the EMAC device must be closed and then
 *     re-opened with the new configuration.
 *
 *     The application layer may pass in an hApplication callback handle,
 *     that will be supplied by the EMAC device when making calls to the
 *     application callback functions.
 *
 *     An EMAC device handle is written to phEMAC. This handle must be saved
 *     by the caller and then passed to other EMAC device functions.
 *
 *     The default receive filter prevents normal packets from being received
 *     until the receive filter is specified by calling EMAC_receiveFilter().
 *
 *     A device reset is achieved by calling EMAC_close() followed by EMAC_open().
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error codes include:
 *       EMAC_ERROR_ALREADY   - The device is already open
 *       EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
 physicalIndex   physical index
 hApplication    application handle
 pEMACConfig     EMAC's configuration structure
 phEMAC          handle to the EMAC device
 @endverbatim
 *
 *
 *  <b> Return Value </b>  Success (0)
 *  @n     EMAC_ERROR_INVALID   - A calling parameter is invalid
 *  @n     EMAC_ERROR_ALREADY   - The device is already open
 *
 *  <b> Pre Condition </b>
 *  @n  None
 *
 *  <b> Post Condition </b>
 *  @n  Opens the EMAC peripheral at the given physical index and initializes it.
 *
* ============================================================================
	*/
Uint32 EMAC_open(int physicalIndex, Handle hApplication,
		EMAC_Config *pEMACConfig, Handle *phEMAC)
{
	int              i, index, j;
	volatile Uint32  *pRegAddr;
	Uint32           tmpval;
	Uint32           utemp1;
	EMAC_Desc        *pDesc;
	EMAC_AddrConfig *tmp;

	/* We only handle a two instance */
	if (physicalIndex >= 2)
		return (EMAC_ERROR_INVALID);

	/* If the device is alread open, return an error */
	if (openFlag)
		return( EMAC_ERROR_ALREADY );

	/*
	 *  Validate the supplied configuration structure
	 */
	if (!pEMACConfig || !phEMAC)
		return (EMAC_ERROR_INVALID);

	/* Rx pool must be at least 8 to 60 */
	/* Max num of descriptors is 512 --> 64 for a channel*/
	if (pEMACConfig->RxMaxPktPool < 8 ||
			pEMACConfig->RxMaxPktPool >= EMAC_MAX_DESCRIPTORS)
		return( EMAC_ERROR_INVALID );

	/* MAC address must be supplied and not a multicast address */
	for (index=0; (Uint8)index<(pEMACConfig->TotalNumOfMacAddrs); index++) {
		tmp = pEMACConfig->MacAddr[index];
		if(tmp->Addr[0] & 1 )
			return( EMAC_ERROR_INVALID );
		for (i = 0; (i < 6) && (!(tmp->Addr[i])); i++)
			if (i == 6)
				return (EMAC_ERROR_INVALID);
	}

	/* test TxChanEnable and RxChanEnable*/
	for (i = 0; i < EMAC_NUMCORES; i++) {
		if (pEMACConfig->ChannelInfo[i].TxChanEnable > 255)
			return (EMAC_ERROR_INVALID);
	}

	for (i = 0; i < EMAC_NUMCORES; i++) {
		if (pEMACConfig->ChannelInfo[i].RxChanEnable > 255)
			return (EMAC_ERROR_INVALID);
	}

	utemp1 = 0;
	for (i = 0; i < EMAC_NUMCORES; i++) {
		TxChanEnable_all |= pEMACConfig->ChannelInfo[i].TxChanEnable;
		utemp1 +=pEMACConfig->ChannelInfo[i].TxChanEnable;
	}
	if(TxChanEnable_all != utemp1) {
		/* if not mutually exclusive */
		return (EMAC_ERROR_INVALID);
	}

	utemp1 = 0;
	for (i = 0; i < EMAC_NUMCORES; i++) {
		RxChanEnable_all |= pEMACConfig->ChannelInfo[i].RxChanEnable;
		utemp1 +=pEMACConfig->ChannelInfo[i].RxChanEnable;
	}
	if(RxChanEnable_all != utemp1) {
	       /* if not mutually exclusive */
	       return (EMAC_ERROR_INVALID);
	}

	/* Callback functions must be supplied */
	if (!pEMACConfig->pfcbFreePacket
		|| !pEMACConfig->pfcbRxPacket
		|| !pEMACConfig->pfcbStatus
		|| !pEMACConfig->pfcbStatistics)
		return (EMAC_ERROR_INVALID);

	/*
	 * Init the instance structure
	 */

	/* Default everything in our instance structure to zero */
	memset(&localDev, 0, sizeof(EMAC_Device));

	/* Set the hApplication and RxFilter */
	localDev.hApplication = hApplication;
	localDev.RxFilter     = EMAC_RXFILTER_NOTHING;
	localDev.Weight       = 64;

	/* Setup the new configuration */
	localDev.Config = *pEMACConfig;

    localDev.PhyId = physicalIndex;
    if (physicalIndex == 0) {
        EMAC_REGS = (CSL_EmacRegs *)CSL_EMAC_0_REGS;
        ECTL_REGS = (CSL_EctlRegs *)CSL_ECTL_0_REGS;
    } else {
        EMAC_REGS = (CSL_EmacRegs *)CSL_EMAC_1_REGS;
        ECTL_REGS = (CSL_EctlRegs *)CSL_ECTL_1_REGS;
    }

	if(corenum == localDev.Config.CoreNum) {
		/*
		 *  Initialize the EMAC devices
		 */

		/* Disable wrapper interrupt pacing */
		ECTL_REGS->INTCONTROL = 0x00;
		/*Give soft reset to Wrapper*/
		ECTL_REGS->SOFTRESET = 0x01;
		/* Wait until reset has occured */
		while (ECTL_REGS->SOFTRESET != 0x00000000);

		/* Give soft reset to EMAC */
		EMAC_REGS->SOFTRESET = 0x00000001;
		/* Wait until reset has occured */
		while (EMAC_REGS->SOFTRESET != 0x00000000);

		/*
		 *  Setup the EMAC
		 */

		/* Reset MAC Control */
		EMAC_REGS->MACCONTROL = 0 ;

		/* Must manually init HDPs to NULL */
		pRegAddr = &EMAC_REGS->TX0HDP;
		for (i = 0; i < EMAC_NUMCHANNELS; i++)
			*pRegAddr++ = 0;
		pRegAddr = &EMAC_REGS->RX0HDP;
		for (i = 0; i < EMAC_NUMCHANNELS; i++)
			*pRegAddr++ = 0;

		/*
		 * While GMIIEN is clear in MACCONTROL, we can write directly to
		 * the statistics registers (there are "EMAC_NUMSTATS" of them).
		 */
		pRegAddr = &EMAC_REGS->RXGOODFRAMES;
		for (i=0; i<EMAC_NUMSTATS; i++)
			*pRegAddr++ = 0;

		/* Initialize the RAM locations */
		for (i = 0; i < 32; i++) {
			EMAC_REGS->MACINDEX = i;
			EMAC_REGS->MACADDRHI = 0;
			EMAC_REGS->MACADDRLO = 0;
		}

		/* Setup device MAC addresses */
		for (index = 0;
			(Uint8)index < (localDev.Config.TotalNumOfMacAddrs);
			index++) {
			tmp = localDev.Config.MacAddr[index];
			EMAC_REGS->MACINDEX = index;

			tmpval = 0;
			for (j = 3; j >= 0; j--)
				tmpval = (tmpval << 8) | tmp->Addr[j];

			EMAC_REGS->MACADDRHI = tmpval;

			tmpval = tmp->Addr[5];
			EMAC_REGS->MACADDRLO =
				CSL_FMKT(EMAC_MACADDRLO_VALID, VALID)
				| CSL_FMKT(EMAC_MACADDRLO_MATCHFILT, MATCH)
				| CSL_FMK(EMAC_MACADDRLO_CHANNEL, tmp->ChannelNum)
				| (tmpval << 8)
				| tmp->Addr[4];
		}

		/*
		 * Setup Special Receive Conditions (loopback, error frames, etc)
		 */

		/* For us buffer offset will always be zero */
		EMAC_REGS->RXBUFFEROFFSET = 0;

		/* Reset RX (M)ulticast (B)roadcast (P)romiscuous Enable register */
		EMAC_REGS->RXMBPENABLE = 0;
		EMAC_REGS->MACHASH1 = 0;
		EMAC_REGS->MACHASH2 = 0;

		/* Clear Unicast RX on channel 0-7 */
		EMAC_REGS->RXUNICASTCLEAR = 0xFF;
	}

	/* Set the pass RX CRC mode and adjust max buffer accordingly */
	if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_RXCRC) {
		if(corenum == localDev.Config.CoreNum) {
			CSL_FINST(EMAC_REGS->RXMBPENABLE,
				EMAC_RXMBPENABLE_RXPASSCRC, INCLUDE);
		}
		localDev.PktMTU = (pEMACConfig->PktMTU) + 4;
	} else {
		localDev.PktMTU = (pEMACConfig->PktMTU);
	}

	if(corenum == localDev.Config.CoreNum) {
		EMAC_REGS->RXMAXLEN = ((pEMACConfig->PktMTU) + 4);
		/* If PASSERROR is set, enable both ERROR and short frames */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_PASSERROR){
			CSL_FINST(EMAC_REGS->RXMBPENABLE,
					EMAC_RXMBPENABLE_RXCEFEN, ENABLE);
			CSL_FINST(EMAC_REGS->RXMBPENABLE,
					EMAC_RXMBPENABLE_RXCSFEN, ENABLE);
		}
		/* If PASSCONTROL is set, enable control frames */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_PASSCONTROL)
			CSL_FINST(EMAC_REGS->RXMBPENABLE,
					EMAC_RXMBPENABLE_RXCMFEN, ENABLE);
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_PASSALL)
			CSL_FINST(EMAC_REGS->RXMBPENABLE,
					EMAC_RXMBPENABLE_RXCAFEN, ENABLE);
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_RXQOS)
			CSL_FINST(EMAC_REGS->RXMBPENABLE,
					EMAC_RXMBPENABLE_RXQOSEN, ENABLE);
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_RXNOCHAIN)
			CSL_FINST(EMAC_REGS->RXMBPENABLE,
					EMAC_RXMBPENABLE_RXNOCHAIN, ENABLE);

		/* Set the channel configuration to priority if requested */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_CHPRIORITY)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_TXPTYPE, CHANNELPRI);

		/* Set MAC loopback if requested */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_MACLOOPBACK)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_LOOPBACK, ENABLE);
		/* Set full duplex mode if requested */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_FULLDUPLEX)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_FULLDUPLEX, FULLDUPLEX);
		/* Enable rx offset/length blocing if requested */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_RXOFFLENBLOCK)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_RXOFFLENBLOCK, BLOCK);
		/* Use rx owership one if requested */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_RXOWNERSHIP)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_RXOWNERSHIP, ONE);
		/* Enable IDLE command bit if requested */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_CMDIDLE)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_CMDIDLE, ENABLE);
		/* Enable tx pace if requested */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_TXPACE)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_TXPACE, ENABLE);
		/* Enable tx flow control */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_TXFLOWCNTL)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_TXFLOWEN, ENABLE);
		/* Enable rx  buffer flow control if requested */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_RXBUFFERFLOWCNTL)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_RXBUFFERFLOWEN, ENABLE);
		/* Enable RMII at 100Mbps if specified */
		if (localDev.Config.ModeFlags & EMAC_CONFIG_MODEFLG_RGMII)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_GIG, ENABLE);

		/*
		 * Enable TX and RX channel interrupts (set mask bits)
		 * Enable Host interrupts
		 */
		EMAC_REGS->RXINTMASKCLEAR = 0xFF;
		EMAC_REGS->TXINTMASKCLEAR = 0xFF;

		EMAC_REGS->RXINTMASKSET = RxChanEnable_all;
		EMAC_REGS->TXINTMASKSET = TxChanEnable_all;

		EMAC_REGS->MACINTMASKSET = CSL_FMK(EMAC_MACINTMASKSET_HOSTMASK, 1) |
			CSL_FMK(EMAC_MACINTMASKSET_STATMASK, 1);
	}

	/*
	 * We give the first descriptors to RX. The rest of the descriptors
	 * will be divided evenly among the TX channels. Odds are this
	 * will leave TX with a very large number of TX descriptors, but
	 * we'll only use what we need (driven from the application send
	 * requests). The RX descriptors are always kept fully populated.
	 */

	/* Pointer to first descriptor to use on RX */
	if (localDev.Config.DescBase == EMAC_DESC_BASE_CPPI) {
        if(physicalIndex==1)
		    pDesc = (EMAC_Desc *)CSL_EMAC_1_DSC_BASE_ADDR;
        else
		    pDesc = (EMAC_Desc *)CSL_EMAC_0_DSC_BASE_ADDR;
	} else {
		return (EMAC_ERROR_INVALID);
	}

	/* Number of descriptors for RX channel */
	/* EMAC_MAX_DESCRIPTORS desc per channel*/
	utemp1 = EMAC_MAX_DESCRIPTORS - (localDev.Config.RxMaxPktPool);

	/* Init the Rx channels used by local core only */
	for (i = 0; i < EMAC_NUMCHANNELS; i++) {
		if((1<<i) & (localDev.Config.ChannelInfo[corenum].RxChanEnable)) {
			localDev.RxCh[i].pd         = &localDev;
			memset(&localDev.RxCh[i].DescFreeQ, 0,
					sizeof(localDev.RxCh[i].DescFreeQ));
			memset(&localDev.RxCh[i].DescHwQ, 0,
					sizeof(localDev.RxCh[i].DescHwQ));
			/* Put all desc to free pool */
			for (j = 0;j < localDev.Config.RxMaxPktPool;j++) {
				EMAC_push(&localDev.RxCh[i].DescFreeQ,
					(pDesc + j));
			}
		}
		pDesc += EMAC_MAX_DESCRIPTORS;
	}

	/*
	 * Setup Transmit Buffers
	 */

	/* Pointer to first descriptor to use on TX Increase of pointer should not be done,
	   because RX descriptor loop already increases pointer (line 1080 (PSG00001758 fix)*/
	/*pDesc += utemp1;*/

	/* Init the Tx channels used by local core only */
	for(j=0; j<EMAC_NUMCORES; j++) {
		for (i = 0; i < EMAC_NUMCHANNELS; i++) {
			if((1<<i) & (localDev.Config.ChannelInfo[j].TxChanEnable)) {
				if(j == corenum) {
					localDev.TxCh[i].pd         = &localDev;
					/*Pointer for first TX desc = pointer to RX + num of RX desc.*/
					pDesc = localDev.RxCh[i].DescFreeQ.tail + 1;
					memset(&localDev.TxCh[i].DescFreeQ, 0,
						sizeof(localDev.TxCh[i].DescFreeQ));
					memset(&localDev.TxCh[i].DescHwQ, 0,
						sizeof(localDev.TxCh[i].DescHwQ));
					/* Put all desc to free pool */
					for (j = 0;j < utemp1;j++) {
						EMAC_push(&localDev.TxCh[i].DescFreeQ, (pDesc + j));
					}
				}
			}
		}
	}

	/* Validate the device handle */
	localDev.DevMagic = EMAC_DEVMAGIC;

	/* Set the open flag */
	openFlag = 1;

	/* Give a handle back to the caller */
	*phEMAC = &localDev;

	/* Return Success */
	return( 0 );
}

/** ============================================================================
 *  @n@b  EMAC_start(Handle hEMAC)
 *
 *  @b Description
 *  @n Enable Rx and Tx the EMAC peripheral indicated by the supplied instance handle.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *         EMAC_ERROR_NOTREADY - Rx Desc is still not filled up
 *
 *  @b Arguments
 *  @verbatim
 hEMAC   handle to opened the EMAC device
 @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      EMAC_ERROR_NOTREADY   - Rx Desc is still not filled up
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC_open function must be called before calling this API.
 *
 *  <b> Post Condition </b>
 *  @n  The EMAC device will be ready to send and received data
 *
 *  @b Example
 *  @verbatim
 EMAC_Config  ecfg;
 Handle       hEMAC = 0;
//Open the EMAC peripheral
EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

//Close the EMAC peripheral
EMAC_start( hEMAC );
@endverbatim
 */
Int32 EMAC_start(Handle hEMAC)
{
	EMAC_Device     *pd = (EMAC_Device *)hEMAC;
	volatile Uint32 *pRegAddr;
	Uint32 i;

	/*
	 * If we didn't get the number of descriptor buffers that the
	 * application said we should, then the app lied to us. This is
	 * bad because we'll be constantly calling to the app to fill
	 * up our buffers. So we'll close now to make the problem
	 * obvious.
	 */
	for (i = 0; i < EMAC_NUMCHANNELS; i++) {
		if((1<<i) & (pd->Config.ChannelInfo[corenum].RxChanEnable)) {
			if (EMAC_depth(&pd->RxCh[i].DescHwQ) + 1 < localDev.Config.RxMaxPktPool) {
				/* Return the error condition */
				return (EMAC_ERROR_NOTREADY);
			}
		}
	}

	/*
	 * Enable RX, TX, and GMII
	 *
	 * Note in full duplex mode we also need to set the FULLDUPLEX
	 * bit in MACCRONTROL. However, we don't know what to set until
	 * we have a link. Also, we must be able to dynamically change
	 * this bit if the cable is unplugged and re-linked with a different
	 * duplex.
	 */
	if(corenum == pd->Config.CoreNum) {
		CSL_FINST(EMAC_REGS->TXCONTROL, EMAC_TXCONTROL_TXEN, ENABLE);
		CSL_FINST(EMAC_REGS->RXCONTROL, EMAC_RXCONTROL_RXEN, ENABLE);
		CSL_FINST(EMAC_REGS->MACCONTROL, EMAC_MACCONTROL_GMIIEN, ENABLE);
	}

	/* Startup RX */
	for (i = 0; i < EMAC_NUMCHANNELS; i++) {
		if((1<<i) & (pd->Config.ChannelInfo[corenum].RxChanEnable)) {
			pRegAddr = &EMAC_REGS->RX0HDP;
			*(pRegAddr + i) = (Uint32)pd->RxCh[i].DescHwQ.head;
		}
	}

	/* Only enable locals */
	if (corenum == 0) {
		ECTL_REGS->C0RXEN = pd->Config.ChannelInfo[corenum].RxChanEnable;
		ECTL_REGS->C0TXEN = pd->Config.ChannelInfo[corenum].TxChanEnable;
	}
	else {
		// Core 1 not present in Netra chip
		//ECTL_REGS->C1RXEN = pd->Config.ChannelInfo[corenum].RxChanEnable;
		//ECTL_REGS->C1TXEN = pd->Config.ChannelInfo[corenum].TxChanEnable;
	}

	return 0;
}

/** ============================================================================
 *  @n@b  EMAC_close()
 *
 *  @b Description
 *  @n Closed the EMAC peripheral indicated by the supplied instance handle.
 *     When called, the EMAC device will shutdown both send and receive
 *     operations, and free all pending transmit and receive packets.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *         EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
 hEMAC   handle to opened the EMAC device
 @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC_open function must be called before calling this API.
 *
 *  <b> Post Condition </b>
 *  @n  The EMAC device will shutdown both send and receive
 *      operations, and free all pending transmit and receive packets.
 *
 *  @b Example
 *  @verbatim
 EMAC_Config  ecfg;
 Handle       hEMAC = 0;
//Open the EMAC peripheral
EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

//Close the EMAC peripheral
EMAC_close( hEMAC );
@endverbatim
 * ============================================================================
 */
Uint32 EMAC_close(Handle hEMAC)
{
	EMAC_Device     *pd = (EMAC_Device *)hEMAC;
	EMAC_Desc       *desc;
	Uint32          tmp, index;
	volatile Uint32 *pRegAddr;

	/* Validate our handle */
	if (!pd || pd->DevMagic != EMAC_DEVMAGIC){
		return (EMAC_ERROR_INVALID);
	}

	/* Only master core will be able to do complete EMAC close ,
	 * non-master core will only disable interrupts in wrapper and free Rx & Tx buffers */
	if (corenum == pd->Config.CoreNum) {
		/* Disable EMAC interrupts pacing in wrapper */
		ECTL_REGS->INTCONTROL = 0x00000000; /*Disable Wrapper interrupts pacing */
		ECTL_REGS->C0RXEN = 0x00;
		ECTL_REGS->C0TXEN = 0x00;

		/*
		 * The close process consists of tearing down all the active
		 * channels (RX and TX) and then waiting for the teardown
		 * complete indication from the MAC. Then, all queued packets
		 * will be returned.
		 */

		/* Teardown all 8 RX channels */
		for (index = 0; index < EMAC_NUMCHANNELS; index++)
			EMAC_REGS->RXTEARDOWN = index ;

		/* Teardown TX channels in use */
		for (index = 0; index < EMAC_NUMCHANNELS; index++)
			EMAC_REGS->TXTEARDOWN = index ;

		/* Only check teardown status if there was no fatal error         */
		/* Otherwise; the EMAC is halted and can't be shut down gracefully */
		if (!pd->FatalError) {
			/* Wait for the teardown to complete */
			pRegAddr = &EMAC_REGS->RX0CP;
			for (index = 0; index < EMAC_NUMCHANNELS; index++) {
				for (tmp = 0; tmp != 0xFFFFFFFC;tmp = *(pRegAddr + index));
				*(pRegAddr + index) = tmp;
			}

			pRegAddr = &EMAC_REGS->TX0CP;
			for (index=0; index< EMAC_NUMCHANNELS; index++) {
				for (tmp = 0; tmp != 0xFFFFFFFC;tmp = *(pRegAddr + index));
				*(pRegAddr + index) = tmp;
			}
		}

		/* Disable RX, TX, and Clear MACCONTROL */
		CSL_FINST(EMAC_REGS->TXCONTROL, EMAC_TXCONTROL_TXEN, DISABLE);
		CSL_FINST(EMAC_REGS->RXCONTROL, EMAC_RXCONTROL_RXEN, DISABLE);
		EMAC_REGS->MACCONTROL = 0;

	} else {
		ECTL_REGS->C0RXEN = 0x00;
		ECTL_REGS->C0TXEN = 0x00;
	}


	/* Free all RX buffers */
	for(index=0; index<EMAC_NUMCHANNELS; index++) {
		if((1<<index) & (pd->Config.ChannelInfo[corenum].RxChanEnable)) {
			while (desc = EMAC_pop(&pd->RxCh[index].DescHwQ)) {
				(*pd->Config.pfcbRxPacket)(pd->hApplication,
						desc->SwField, desc->PktFlgLen);
			}
		}
	}

	/* Free all TX buffers */
	for(index=0; index<EMAC_NUMCHANNELS; index++) {
		if((1<<index) & (pd->Config.ChannelInfo[corenum].TxChanEnable)) {
			while (desc = EMAC_pop(&pd->TxPktQ)) {
				EMAC_push(&pd->TxCh[index].DescHwQ, desc);
			}
			while (desc = EMAC_pop(&pd->TxCh[index].DescHwQ)){
				(*pd->Config.pfcbFreePacket)(pd->hApplication,
						desc->SwField, desc->PktFlgLen);
			}
		}
	}

	/* Invalidate the EMAC handle */
	pd->DevMagic = 0;

	/* Clear the open flag */
	openFlag = 0;

	/* Exit with interrupts still disabled in the wrapper */
	return (0);
}


/** ============================================================================
 *  @n@b  EMAC_getStatus()
 *
 *  @b Description
 *  @n Called to get the current status of the device. The device status
 *     is copied into the supplied data structure.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
 hEMAC   handle to the opened EMAC device
 pStatus Status of the EMAC
 @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API.
 *
 *  <b> Post Condition </b>
 *  @n  The current status of the device is copied into the supplied data
 *      structure.
 *
 *  @b Example
 *  @verbatim
 EMAC_Status pStatus;
 EMAC_Config  ecfg;
 Handle       hEMAC = 0;

//Open the EMAC peripheral
EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

EMAC_getStatus( hEMAC, &status);
@endverbatim
 * ============================================================================
 */
Uint32 EMAC_getStatus(Handle hEMAC, EMAC_Status *pStatus)
{
	EMAC_Device  *pd = (EMAC_Device *)hEMAC;
	Uint32        i, tmp;

	/* Validate our handle */
	if (!pd || pd->DevMagic != EMAC_DEVMAGIC || !pStatus)
		return (EMAC_ERROR_INVALID);

	/* Number of rx packets held */
	tmp = 0;
	for(i=0; i<EMAC_NUMCHANNELS; i++) {
		if((1<<i)&(pd->Config.ChannelInfo[corenum].RxChanEnable)) {
			tmp += pd->RxCh[i].DescHwQ.depth;
		}
	}

	pStatus->RxPktHeld = tmp;

	/* Number of tx packets held */
	tmp = 0;
	for(i=0; i<EMAC_NUMCHANNELS; i++) {
		if((1<<i)&(pd->Config.ChannelInfo[corenum].TxChanEnable)) {
			tmp += pd->TxCh[i].DescHwQ.depth;
		}
	}
	pStatus->TxPktHeld = tmp;

	/* Fatal error value */
	pStatus->FatalError = pd->FatalError;

	return (0);
}


/** ============================================================================
 *  @n@b  EMAC_setReceiveFilter()
 *
 *  @b Description
 *  @n Called to set the packet filter for received packets. The filtering
 *     level is inclusive, so BROADCAST would include both BROADCAST and
 *     DIRECTED (UNICAST) packets.
 *
 *     Available filtering modes include the following:
 *         - EMAC_RXFILTER_NOTHING      - Receive nothing
 *         - EMAC_RXFILTER_DIRECT       - Receive only Unicast to local MAC addr
 *         - EMAC_RXFILTER_BROADCAST    - Receive direct and Broadcast
 *         - EMAC_RXFILTER_MULTICAST    - Receive above plus multicast in mcast list
 *         - EMAC_RXFILTER_ALLMULTICAST - Receive above plus all multicast
 *         - EMAC_RXFILTER_ALL          - Receive all packets
 *
 *     Note that if error frames and control frames are desired, reception of
 *     these must be specified in the device configuration.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *         EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
 hEMAC           handle to the opened EMAC device
 ReceiveFilter   Filtering modes
 @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API
 *
 *  <b> Post Condition </b>
 *  @n  Sets the packet filter for received packets
 *
 *  @b Example
 *  @verbatim
#define EMAC_RXFILTER_DIRECT       1
EMAC_Config  ecfg;
Handle       hEMAC = 0;

EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

EMAC_setReceiveFilter(hEMAC, EMAC_RXFILTER_DIRECT );

@endverbatim
 * ============================================================================
 */
Uint32 EMAC_setReceiveFilter(Handle hEMAC, Uint32 ReceiveFilter, Uint8 masterChannel)
{
	EMAC_Device  *pd = (EMAC_Device *)hEMAC;

	/* Validate our handle */
	if (!pd || pd->DevMagic != EMAC_DEVMAGIC || ReceiveFilter > EMAC_RXFILTER_ALL)
		return (EMAC_ERROR_INVALID);

	/*
	 * The following code relies on the numeric relation of the filter
	 * value such that the higher filter values receive more types of
	 * packets.
	 */

	/* Disable Section */
	if (ReceiveFilter < EMAC_RXFILTER_ALL)
		CSL_FINST(EMAC_REGS->RXMBPENABLE, EMAC_RXMBPENABLE_RXCAFEN, DISABLE);
	if (ReceiveFilter < EMAC_RXFILTER_ALLMULTICAST) {
		EMAC_REGS->MACHASH1 = pd->MacHash1;
		EMAC_REGS->MACHASH2 = pd->MacHash2;
	}
	if (ReceiveFilter < EMAC_RXFILTER_MULTICAST)
		CSL_FINST(EMAC_REGS->RXMBPENABLE, EMAC_RXMBPENABLE_RXMULTEN, DISABLE);
	if (ReceiveFilter < EMAC_RXFILTER_BROADCAST)
		CSL_FINST(EMAC_REGS->RXMBPENABLE, EMAC_RXMBPENABLE_RXBROADEN, DISABLE);
	if (ReceiveFilter < EMAC_RXFILTER_DIRECT) {
		EMAC_REGS->RXUNICASTCLEAR = RxChanEnable_all;
	}

	/* Enable Section */
	if (ReceiveFilter >= EMAC_RXFILTER_DIRECT) {
		EMAC_REGS->RXUNICASTSET = RxChanEnable_all;
	}

	if (ReceiveFilter >= EMAC_RXFILTER_BROADCAST) {
		CSL_FINST(EMAC_REGS->RXMBPENABLE, EMAC_RXMBPENABLE_RXBROADEN, ENABLE);
		/*Only one channel can receive broadcast frames, set it to be a master*/
		CSL_FINS(EMAC_REGS->RXMBPENABLE, EMAC_RXMBPENABLE_RXBROADCH, masterChannel);
	}
	if (ReceiveFilter >= EMAC_RXFILTER_MULTICAST) {
		CSL_FINST(EMAC_REGS->RXMBPENABLE, EMAC_RXMBPENABLE_RXMULTEN, ENABLE);
		/*Only one channel can receive multicast frames, set it to be a master*/
		CSL_FINS(EMAC_REGS->RXMBPENABLE, EMAC_RXMBPENABLE_RXMULTCH, masterChannel);
	}
	if (ReceiveFilter >= EMAC_RXFILTER_ALLMULTICAST) {
		EMAC_REGS->MACHASH1 = 0xffffffff;
		EMAC_REGS->MACHASH1 = 0xffffffff;
	}
	if (ReceiveFilter == EMAC_RXFILTER_ALL) {
		CSL_FINST(EMAC_REGS->RXMBPENABLE, EMAC_RXMBPENABLE_RXCAFEN, ENABLE);
		CSL_FINS(EMAC_REGS->RXMBPENABLE, EMAC_RXMBPENABLE_RXPROMCH, masterChannel);
	}

	pd->RxFilter = ReceiveFilter;

	return (0);
}


/** ============================================================================
 *  @n@b  EMAC_getReceiveFilter()
 *
 *  @b Description
 *  @n Called to get the current packet filter setting for received packets.
 *     The filter values are the same as those used in EMAC_setReceiveFilter().
 *
 *     The current filter value is written to the pointer supplied in
 *     pReceiveFilter.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *       EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
 hEMAC           handle to the opened EMAC device
 pReceiveFilter  Current receive packet filter
 @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API and
 *      must be set the packet filter value.
 *
 *  <b> Post Condition </b>
 *  @n  The current filter value is written to the pointer supplied
 *
 *  @b Example
 *  @verbatim
#define EMAC_RXFILTER_DIRECT       1
EMAC_Config  ecfg;
Handle       hEMAC = 0;
Uint32         pReceiveFilter;

EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

EMAC_setReceiveFilter(hEMAC, EMAC_RXFILTER_DIRECT );

EMAC_getReceiveFilter(hEMAC, &pReceiveFilter );
@endverbatim
 * ============================================================================
 */
Uint32 EMAC_getReceiveFilter(Handle hEMAC, Uint32 *pReceiveFilter)
{
	EMAC_Device  *pd = (EMAC_Device *)hEMAC;

	/* Validate our handle */
	if (!pd || pd->DevMagic != EMAC_DEVMAGIC || !pReceiveFilter)
		return (EMAC_ERROR_INVALID);

	*pReceiveFilter = pd->RxFilter;
	return (0);
}


/** ============================================================================
 *  @n@b  EMAC_getStatistics()
 *
 *  @b Description
 *  @n Called to get the current device statistics. The statistics structure
 *     contains a collection of event counts for various packet sent and
 *     receive properties. Reading the statistics also clears the current
 *     statistic counters, so the values read represent a delta from the last
 *     call.
 *
 *     The statistics information is copied into the structure pointed to
 *     by the pStatistics argument.
 *
 *     The function returns zero on success, or an error code on failure.
 *
 *     Possible error code include:
 *      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
 hEMAC       handle to the opened EMAC device
 pStatistics Get the device statistics
 @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened before calling this API
 *
 *  <b> Post Condition </b>
 *      -# Statistics are read for various packects sent and received.
 -# Reading the statistics also clears the current
 statistic counters, so the values read represent a delta from the
 last call.
 *
 *  @b Example
 *  @verbatim
 EMAC_Config      ecfg;
 Handle           hEMAC = 0;
 EMAC_Statistics  pStatistics;

 EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

 EMAC_getStatistics(hEMAC, &pStatistics );
 @endverbatim
 * ============================================================================
 */
Uint32 EMAC_getStatistics(Handle hEMAC, EMAC_Statistics *pStatistics)
{
	EMAC_Device  *pd = (EMAC_Device *)hEMAC;

	/* Validate our handle */
	if(!pd || pd->DevMagic != EMAC_DEVMAGIC || !pStatistics)
		return (EMAC_ERROR_INVALID);

	/* Update the stats */
	EMAC_updateStats(pd);

	/* Copy the updated stats to the application */
	*pStatistics = pd->Stats;

	/* Clear our copy */
	memset(&pd->Stats, 0, sizeof(EMAC_Statistics));

	return (0);
}


/** ============================================================================
 *  @n@b  EMAC_setMulticast()
 *
 *  @b Description
 *  @n This function is called to install a list of multicast addresses for
 *     use in multicast address filtering. Each time this function is called,
 *     any current multicast configuration is discarded in favor of the new
 *     list. Thus a set with a list size of zero will remove all multicast
 *     addresses from the device.
 *
 *     Note that the multicast list configuration is stateless in that the
 *     list of multicast addresses used to build the configuration is not
 *     retained. Thus it is impossible to examine a list of currently installed
 *     addresses.
 *
 *     The addresses to install are pointed to by pMCastList. The length of
 *     this list in bytes is 6 times the value of AddrCnt. When AddrCnt is
 *     zero, the pMCastList parameter can be NULL.
 *
 *     The function returns zero on success, or an error code on failure.
 *     The multicast list settings are not altered in the event of a failure
 *     code.
 *
 *     Possible error code include:
 *       EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  @b Arguments
 *  @verbatim
 hEMAC       handle to the opened EMAC device
 AddrCount   number of addresses to multicast
 pMCastList  pointer to the multi cast list
 @endverbatim
 *
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened and set multicast filter.
 *
 *  <b> Post Condition </b>
 -# Install a list of multicast addresses for use in multicast
 address filtering.
 -# A set with a list size of zero will remove all multicast addresses
 from the device.
 *
 *  @b Example
 *  @verbatim
#define EMAC_RXFILTER_ALLMULTICAST 4

Handle       hEMAC = 0;
Uint32         AddrCnt;
Uint8        pMCastList;
EMAC_Config  ecfg;

EMAC_open(1, (Handle)0x12345678, &ecfg, &hEMAC);

EMAC_setReceiveFilter( hEMAC, EMAC_RXFILTER_ALLMULTICAST );

EMAC_setMulticast( hEMAC, AddrCnt, &pMCastList );
@endverbatim
 * ============================================================================
 */
Uint32 EMAC_setMulticast(Handle hEMAC, Uint32 AddrCnt, Uint8 *pMCastList)
{
	EMAC_Device  *pd = (EMAC_Device *)hEMAC;
	Uint32        tmp1,tmp2;
	Uint8       HashVal,tmpval;

	/* Validate our handle */
	if (!pd || pd->DevMagic != EMAC_DEVMAGIC || (AddrCnt && !pMCastList))
		return (EMAC_ERROR_INVALID);

	/* Clear the hash bits */
	pd->MacHash1 = 0;
	pd->MacHash2 = 0;

	/* For each address in the list, hash and set the bit */
	for (tmp1=0; tmp1<AddrCnt; tmp1++) {
		HashVal=0;

		for (tmp2=0; tmp2 < 2; tmp2++) {
			tmpval = *pMCastList++;
			HashVal ^= (tmpval >> 2) ^ (tmpval << 4);
			tmpval = *pMCastList++;
			HashVal ^= (tmpval >> 4) ^ (tmpval << 2);
			tmpval = *pMCastList++;
			HashVal ^= (tmpval >> 6) ^ (tmpval);
		}

		if (HashVal & 0x20)
			pd->MacHash2 |= (1 << (HashVal & 0x1f));
		else
			pd->MacHash1 |= (1 << (HashVal & 0x1f));
	}

	/* We only write the hash table if the filter setting allows */
	if (pd->RxFilter < EMAC_RXFILTER_ALLMULTICAST) {
		EMAC_REGS->MACHASH1 = pd->MacHash1;
		EMAC_REGS->MACHASH2 = pd->MacHash2;
	}

	return (0);
}

/**
 * EMAC_process: Process all intr in hw
 * @src: Source Desc Q (usually HwDescQ)
 * @dst: Destination Desc Q (usually Local Q)
 * @weight: Max desc tto be processed in this call
 * @callback: Callback to be called after deq
 * @regcp: CP reg pointer
 * @reghdp: HDP reg pointer
 *
 * Process Rx/Tx intrs
 *
 */

static UInt32 EMAC_process(EMAC_Device *pd, EMAC_q *hwq, EMAC_q *freeq, Uint32 weight,
			void (*callback)(Handle, Uint32, Uint32),
			volatile Uint32 *regcp, volatile Uint32 *reghdp)
{
	volatile Uint32 end = *regcp;
	volatile Uint32 read_back ;

	EMAC_Desc *desc = (EMAC_Desc *)end;
	EMAC_Desc *next = (EMAC_Desc *)desc->pNext;

	/* Check if packet is still owned by the hw */
	if (desc->PktFlgLen & EMAC_DSC_FLAG_OWNER) {
		return weight;
	} else if (weight == 0 || EMAC_empty(hwq)) {
		Vps_rprintf("%s: Hitting spurious case w=%d\n", __func__, weight);
		return weight;
	}

	/* If the hw has stopped and we have more descriptors, then restart */

	if (next && (desc->PktFlgLen & EMAC_DSC_FLAG_EOQ)) {
		*reghdp = (Uint32)next;
		read_back = *reghdp;
	}

	/* Process the finished packets now */
	do {
		if (desc = (EMAC_Desc *)EMAC_pop(hwq)) {
			/* Call tx callback for free in the packet */
			callback(pd->hApplication, desc->SwField, desc->PktFlgLen);
			EMAC_push(freeq, desc);
		} else {
			Vps_rprintf(" [ETH_OFFLOAD] NULL hw desc!!!!");
			break;
		}
		/* Acknowledge */
		*regcp = (Uint32)desc;

	} while ((Uint32)desc != end && weight--);


	return weight;
}

/** ============================================================================
 *  @n@b  EMAC_RxServiceCheck()
 *
 *  @b Description
 *  @n This function should be called every time there is an EMAC device Rx
 *     interrupt. It maintains the status the EMAC.
 *
 *     Note that the application has the responsibility for mapping the
 *     physical device index to the correct EMAC_serviceCheck() function. If
 *     more than one EMAC device is on the same interrupt, the function must be
 *     called for each device.
 *
 *     Possible error codes include:
 *       EMAC_ERROR_INVALID   - A calling parameter is invalid
 *       EMAC_ERROR_MACFATAL  - Fatal error in the MAC - Call EMAC_close()
 *
 *  @b Arguments
 *  @verbatim
 hEMAC       handle to the opened EMAC device
 @endverbatim
 *  <b> Return Value </b>  Success (0)
 *  @n     EMAC_ERROR_INVALID   - A calling parameter is invalid
 *  @n     EMAC_ERROR_MACFATAL  - Fatal error in the MAC - Call EMAC_close()
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC_open function must be called before calling this API.
 *
 *  <b> Post Condition </b>
 *  @n  None
 *
 *  @b Example
 *  @verbatim
 static CSL_IntcContext context;
 static CSL_IntcEventHandlerRecord Record[13];
 static CSL_IntcObj intcEMACRx;
 static CSL_IntcHandle hIntcEMACRx;

//CSL_IntcParam vectId1;
CSL_IntcParam vectId2;

CSL_IntcGlobalEnableState state;

// Setup the global Interrupt
context.numEvtEntries = 13;
context.eventhandlerRecord = Record;

// VectorID for the Event
vectId2 = CSL_INTC_VECTID_6;

CSL_intcInit(&context);
// Enable NMIs
CSL_intcGlobalNmiEnable();
// Enable Global Interrupts
CSL_intcGlobalEnable(&state);

// Opening a handle for EMAC Rx interrupt
hIntcEMACRx=CSL_intcOpen(&intcEMACRx,CSL_INTC_EVENTID_MACRXINT,&vectId2,NULL);

//Hook the ISRs
CSL_intcHookIsr(vectId2,&HwRxInt);

CSL_intcHwControl(hIntcEMACRx, CSL_INTC_CMD_EVTENABLE, NULL);

// This function is called when Rx interrupt occurs
Void HwRxInt (void)
{
// Note : get the Emac Handle(hEMAC) by calling EMAC_open function
EMAC_RxServiceCheck(hEMAC);
}

@endverbatim
* ============================================================================
	*/

Uint32 EMAC_check(Handle hEMAC, Uint32 weight)
{
	EMAC_Device     *pd = (EMAC_Device *)hEMAC;
	Uint32          intflags, i;
    Uint32          numRxTx = 0;

	/* Read the interrupt cause */
	intflags = EMAC_REGS->MACINVECTOR;

	/* Look for fatal errors first */
	if (intflags & CSL_FMK(EMAC_MACINVECTOR_HOSTPEND, 1)) {
		/* Read the error status - we'll decode it by hand */
		pd->FatalError = EMAC_REGS->MACSTATUS;
		EMAC_REGS->MACEOIVECTOR = 0x3;
		/* Tell the application */
		(*localDev.Config.pfcbStatus)(pd->hApplication, pd->FatalError);

		/* return with interrupt disabled in the wrapper */
		return 0;
	}

	/* Look for statistics interrupt */
	if (intflags & CSL_FMK(EMAC_MACINVECTOR_STATPEND, 1)) {
		/* Read the stats and reset to zero         */
		/* This is necessary to clear the interrupt */
		EMAC_updateStats(pd);
		EMAC_REGS->MACEOIVECTOR = 0x3;

		/* Tell the application */
		(*localDev.Config.pfcbStatistics)(pd->hApplication);
	}

	/* Look for Rx/Tx interrupt */
	for(i=0; i<EMAC_NUMCHANNELS; i++) {
		/* Check for Rx first */

		/* find out which channel is enabled */
		if((1<<i) & (pd->Config.ChannelInfo[corenum].RxChanEnable)) {
			/* find out if interrupt happend */
			if (intflags & CSL_FMK(EMAC_MACINVECTOR_RXPEND, 1 << i)) {
				pd->RxCh[i].ChannelIndex = i;
				EMAC_process(pd, &pd->RxCh[i].DescHwQ,
						&pd->RxCh[i].DescFreeQ,
						weight, pd->Config.pfcbRxPacket,
						(&EMAC_REGS->RX0CP) + i,
						(&EMAC_REGS->RX0HDP) + i);
				EMAC_REGS->MACEOIVECTOR = 0x1;
                numRxTx++;
			}
		}
		/* Check for Tx next */

		/* find out which channel is enabled */
		if((1<<i) & (pd->Config.ChannelInfo[corenum].TxChanEnable)) {
			/* find out if interrupt happend */
			if (intflags & CSL_FMK(EMAC_MACINVECTOR_TXPEND, 1 << i)) {
				pd->TxCh[i].ChannelIndex = i;
				EMAC_process(pd, &pd->TxCh[i].DescHwQ,
						&pd->TxCh[i].DescFreeQ,
						weight, pd->Config.pfcbFreePacket,
						(&EMAC_REGS->TX0CP) + i,
						(&EMAC_REGS->TX0HDP) + i);
				EMAC_REGS->MACEOIVECTOR = 0x2;
                numRxTx++;
			}
		}
	}

	return numRxTx;
}

/** ============================================================================
 *  @n@b  EMAC_timerTick()
 *
 *  @b Description
 *  @n This function should be called for each device in the system on a
 *     periodic basis of 100mS (10 times a second). It is used to check the
 *     status of the EMAC device, and to potentially recover from
 *     low Rx buffer conditions.
 *
 *     Strict timing is not required, but the application should make a
 *     reasonable attempt to adhere to the 100mS mark. A missed call should
 *     not be "made up" by making multiple sequential calls.
 *
 *     A "polling" driver (one that calls EMAC_serviceCheck() in a tight loop),
 *     must also adhere to the 100mS timing on this function.
 *
 *     Possible error codes include:
 *       EMAC_ERROR_INVALID   - A calling parameter is invalid

 *  @b Arguments
 *  @verbatim
 hEMAC       handle to the opened EMAC device
 @endverbatim
 *
 *  <b> Return Value </b>  Success (0)
 *  @n      EMAC_ERROR_INVALID   - A calling parameter is invalid
 *
 *  <b> Pre Condition </b>
 *  @n  EMAC peripheral instance must be opened
 *
 *  <b> Post Condition </b>
 *  @n  Re-fill Rx buffer queue if needed and modifies  EMAC CONTROL register.
 *
 *  @b Example
 *  @verbatim
 EMAC_Config ecfg;
 Handle      hEMAC = 0;

//open the EMAC device
EMAC_open( 1, (Handle)0x12345678, &ecfg, &hEMAC );

EMAC_timerTick( hEMAC);
@endverbatim
 * ============================================================================
 */
Uint32 EMAC_timerTick(Handle hEMAC)
{
	EMAC_Device  *pd = (EMAC_Device *)hEMAC;

	/* Validate our handle */
	if (!pd || pd->DevMagic != EMAC_DEVMAGIC)
		return( EMAC_ERROR_INVALID );

	return (0);
}

/**
 * EMAC_update: Update Phy status
 * @update: Update structure to update emac status
 *
 * Updates phy status and takes action for network queue if required
 * based upon link status
 *
 */
Int32 EMAC_update(struct EMAC_updateData *update)
{
	Uint32 mac_control;
	Uint32 cur_duplex;

	mac_control = EMAC_REGS->MACCONTROL;

	cur_duplex = (mac_control & CSL_EMAC_MACCONTROL_FULLDUPLEX_MASK) ?
			1 : 0;

	/* We get called only if link has changed (speed/duplex/status) */
	if (update->linkup && (update->duplex != cur_duplex)) {
		if (1 == update->duplex)
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_FULLDUPLEX, FULLDUPLEX);
		else
			CSL_FINST(EMAC_REGS->MACCONTROL,
					EMAC_MACCONTROL_FULLDUPLEX, HALFDUPLEX);
	}

	if (update->speed == 1000) {
		CSL_FINST(EMAC_REGS->MACCONTROL, EMAC_MACCONTROL_GIGFORCE, ONE);
		CSL_FINST(EMAC_REGS->MACCONTROL, EMAC_MACCONTROL_GIG, ENABLE);
	} else {
		/* Clear the GIG bit and GIGFORCE bit */
		CSL_FINST(EMAC_REGS->MACCONTROL, EMAC_MACCONTROL_GIGFORCE, ZERO);
		CSL_FINST(EMAC_REGS->MACCONTROL, EMAC_MACCONTROL_GIG, DISABLE);
	}
	mac_control = EMAC_REGS->MACCONTROL;

	return 0;
}

static void EMAC_pushHw(EMAC_q *q, EMAC_Desc *desc, volatile UInt32 *pReg)
{
    volatile UInt32 tmpRead; /* to ensure data is pushed to memory */
    EMAC_Desc *prev = q->tail;

	//desc->pNext = 0;
    /* tmpRead = desc->pNext; */

	if (!q->head) {
		// Queue is empty
		q->head = desc;
        q->tail = desc;
        *pReg = (UInt32)desc;
	} else {

		// Queue is not empty
		prev->pNext = desc;
        tmpRead = (UInt32)prev->pNext;
        q->tail = desc;

      if ( (*pReg == NULL) &&  ((prev->PktFlgLen & (EMAC_DSC_FLAG_EOQ | EMAC_DSC_FLAG_OWNER) == EMAC_DSC_FLAG_EOQ ))  )
        {
            /* Queue is not running, start it up */
            *pReg = (UInt32)desc;
        }

	}

	q->depth++;

    tmpRead = *pReg;

}

static void EMAC_pushHwQ(EMAC_q *hw, EMAC_q *sw, volatile UInt32 *pReg)
{
    volatile UInt32 tmpRead; /* to ensure data is pushed to memory */
    EMAC_Desc *prev = hw->tail;

	//desc->pNext = 0;
    /* tmpRead = desc->pNext; */

	if (!hw->head) {
		// Queue is empty
		hw->head = sw->head;
        hw->tail = sw->tail;
        *pReg = (UInt32)hw->head;
	} else {

		// Queue is not empty
		prev->pNext = sw->head;
        tmpRead = (UInt32)prev->pNext;
        hw->tail = sw->tail;

      if ( (*pReg == NULL) &&  ((prev->PktFlgLen & (EMAC_DSC_FLAG_EOQ | EMAC_DSC_FLAG_OWNER) == EMAC_DSC_FLAG_EOQ ))  )
        {
            /* Queue is not running, start it up */
            *pReg = (UInt32)sw->head;
        }

	}

	hw->depth += sw->depth;
	sw->head = sw->tail = NULL;
	sw->depth = 0;

    tmpRead = *pReg;

}


/**
 * EMAC_enqTxDesc: Enqueue Tx packet/frag
 * @hEMAC: Handle to an opened and started emac driver
 * @buffer: Buffer ptr (phy addr) of the frag's data
 * @size: Size of frag
 * @flag: 1LSB bit carries 1 for last frag, rest has the total packet len
 * @token: Software reference
 *
 * Pre enqueued desc ready to receive data from network
 *
 */
Int32 EMAC_enqTxDesc(Handle hEMAC, Uint8 *buffer, Uint32 size, Uint32 flag, Uint32 token)
{
	Uint32 chan = 0;
    volatile UInt32 tmpRead; /* to ensure value is pushed to memory */
	EMAC_Device *pd = (EMAC_Device *)hEMAC;
	EMAC_Desc *desc = EMAC_pop(&pd->TxCh[chan].DescFreeQ);

	if (!desc) {
		return 0;
	}

	desc->pBuffer   = buffer;
	desc->BufOffLen = size;
	desc->SwField   = token;
	desc->pNext     = NULL;
	desc->PktFlgLen = (flag&0xC000FFFF) | EMAC_DSC_FLAG_OWNER;

	if( ((flag & EMAC_DSC_FLAG_SOP) == EMAC_DSC_FLAG_SOP) &&
	    ((flag & EMAC_DSC_FLAG_EOP) == EMAC_DSC_FLAG_EOP) &&
	    ( (flag & 0xFFFF) < 60)) {
		desc->PktFlgLen &= (~(0xFFFF));
		desc->PktFlgLen |= 60;
		desc->BufOffLen = 60;
	        Vps_rprintf("Pad min. frame size to 60, flglen=0x%x\n",desc->PktFlgLen);
	}

	if((flag & 0xFFFF) < 60)
		Vps_rprintf("size less than 60, size=%d\n", (flag & 0xFFFF));

    tmpRead = desc->PktFlgLen;

	EMAC_push(&pd->TxPktQ, desc);

	return 1;
}

Int32 EMAC_enqToHw(Handle hEMAC)
{
	volatile UInt32 *pReg = 0;
	Uint32 chan = 0;
	EMAC_Device *pd = (EMAC_Device *)hEMAC;

	pReg = &EMAC_REGS->TX0HDP;
	pReg += pd->TxCh[chan].ChannelIndex;

	EMAC_pushHwQ(&pd->TxCh[chan].DescHwQ, &pd->TxPktQ, pReg);
	return 1;
}

/**
 * EMAC_enqRxDesc: Enqueue Rx packet
 * @hEMAC: Handle to a opened and started emac driver
 * @buffer: Buffer ptr (phy addr) for the packet's data
 * @token: Software reference
 *
 * Pre enqueued desc ready to receive data from network
 *
 */
Int32 EMAC_enqRxDesc(Handle hEMAC, Uint8 *buffer, Uint32 token)
{
	Uint32 chan = 0;
	volatile UInt32 *pReg = 0;
	EMAC_Device *pd = (EMAC_Device *)hEMAC;
	EMAC_Desc *desc = EMAC_pop(&pd->RxCh[chan].DescFreeQ);
    volatile UInt32 tmpRead; /* to ensure value is pushed to memory */

    pReg = &EMAC_REGS->RX0HDP;
    pReg += pd->RxCh[chan].ChannelIndex;

	if (!desc) {
		return 0;
	}

	desc->pBuffer   = buffer;
	desc->BufOffLen = localDev.PktMTU;
	desc->SwField   = token;
	desc->pNext     = NULL;
	desc->PktFlgLen = EMAC_DSC_FLAG_OWNER;

    tmpRead = desc->PktFlgLen;

	EMAC_pushHw(&pd->RxCh[chan].DescHwQ, desc, pReg);

	return 1;
}

/**
 * EMAC_rxFreeCount: Number of free Rx desc
 * @hEMAC: Handle to a opened and started emac driver
 *
 */
Int32 EMAC_rxFreeCount(Handle hEMAC)
{
	UInt32 chan = 0;
	EMAC_Device *pd = (EMAC_Device *)hEMAC;
	return EMAC_depth(&pd->RxCh[chan].DescFreeQ);
}

/**
 * EMAC_txFreeCount: Number of free Tx desc
 * @hEMAC: Handle to a opened and started emac driver
 *
 */
Int32 EMAC_txFreeCount(Handle hEMAC)
{
	Uint32 chan = 0;
	EMAC_Device *pd = (EMAC_Device *)hEMAC;
	return EMAC_depth(&pd->TxCh[chan].DescFreeQ);
}
