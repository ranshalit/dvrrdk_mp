/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <mcfw/src_bios6/utils/utils.h>

#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/hal/Hwi.h>

#include <mcfw/src_bios6/eth_offload/inc/eth_offload_config.h>
#include <mcfw/src_bios6/eth_offload/inc/eth_offload_rpc.h>
#include <mcfw/src_bios6/eth_offload/inc/eth_offload_q.h>
#include <mcfw/src_bios6/eth_offload/inc/eth_offload_emac.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/emac/csl.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/emac/cslr_emac.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/emac/csl_emac.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/emac/cslr_ectl.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/emac/soc.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/emac/emac_common.h>


#define ETH_OFFLOAD_ETHTYPE_ETH_8021q       0x0081
#define ETH_OFFLOAD_ETHTYPE_ETH_8021q_2     0xA888
#define ETH_OFFLOAD_ETHTYPE_ETH_IPV4        0x0008
#define ETH_OFFLOAD_ETHTYPE_ETH_IPV6        0xDD86

#define ETH_OFFLOAD_NET_IPUDP           17
#define ETH_OFFLOAD_NET_IPTCP           6

#define ETH_OFFLOAD_NUM_HEADERS         ETH_OFFLOAD_HWDESC_MAX
#define ETH_OFFLOAD_MAX_HEADER_SIZE     128
#define ETH_OFFLOAD_EMAC_NUMLAYERS      3
#define ETH_OFFLOAD_ETH_MINFRAMESIZE        60
#define ETH_OFFLOAD_TSK_STACK_SIZE      (16*1024)

#define ETH_OFFLOAD_LRO_TABLE_SIZE      31

#define htons(a)                ((((a)&0xFF)<<8)|(((a)&0xFF00)>>8))

#define htonl(n) (((((unsigned long)(n) & 0xFF)) << 24) | \
                  ((((unsigned long)(n) & 0xFF00)) << 8) | \
                  ((((unsigned long)(n) & 0xFF0000)) >> 8) | \
                  ((((unsigned long)(n) & 0xFF000000)) >> 24))

#define ETH_OFFLOAD_PARAMS_FLAG_EOP		(0x40000000u)
#define ETH_OFFLOAD_PARAMS_FLAG_SOP		(0x80000000u)
#define ETH_OFFLOAD_PARAMS_FLAG_EOS		(0x10000000u)
#define ETH_OFFLOAD_PARAMS_FLAG_SOS		(0x20000000u)
#define ETH_OFFLOAD_PARAMS_FLAG_PARTIAL		(0x08000000u)
#define ETH_OFFLOAD_PARAMS_FLAG_HEADER		(0x04000000u)
#define ETH_OFFLOAD_PARAMS_FLAG_DONE		(0x02000000u)
#define ETH_OFFLOAD_PARAMS_FLAG_ONESEG		(0x30000000u)
#define ETH_OFFLOAD_PARAMS_FLAG_FIRST		(0xA0000000u)
#define ETH_OFFLOAD_PARAMS_FLAG_LAST		(0x50000000u)

#define ETH_OFFLOAD_PARAMS_RESETFLAG(p, f)  ((p)->flag = (((p)->flag&0xFFFFFFF0)|ETH_OFFLOAD_PARAMS_FLAG_##f))
#define ETH_OFFLOAD_PARAMS_SETFLAG(p, f)    ((p)->flag |= ETH_OFFLOAD_PARAMS_FLAG_##f)
#define ETH_OFFLOAD_PARAMS_UNSETFLAG(p, f)  ((p)->flag &= ~ETH_OFFLOAD_PARAMS_FLAG_##f)
#define ETH_OFFLOAD_PARAMS_CHECKFLAG(p, f)  ((p)->flag & ETH_OFFLOAD_PARAMS_FLAG_##f)

/* Task related variable */
#pragma DATA_ALIGN(gTskStack, 32)
#pragma DATA_SECTION(gTskStack, ".bss:taskStackSection")
static UInt8 gTskStack[ETH_OFFLOAD_TSK_STACK_SIZE];
/***/

struct ETH_OFFLOAD_params;
struct ETH_OFFLOAD_Emac_pktInfo;

/* Supported Network headers */

/* Ethernet headers */
struct ETH_OFFLOAD_Emac_Eth1 {
    UInt8               dest[6];
    UInt8               source[6];
    UInt16              type;
} __attribute__((packed));

struct ETH_OFFLOAD_Emac_Eth2 {
    struct ETH_OFFLOAD_Emac_Eth1    eth1;
    UInt16              tci;
    UInt16              encap_proto;
} __attribute__((packed));

struct ETH_OFFLOAD_Emac_Ipv4Frag {
    UInt16
                    offset:13,
                    mf:1,
                    df:1,
                    rsvd:1;
};

/* IPV4 header */
struct ETH_OFFLOAD_Emac_Ipv4 {
    UInt8               ihl:4,
                    version:4;
    UInt8               tos;
    UInt16              len;
    UInt16              id;
    struct ETH_OFFLOAD_Emac_Ipv4Frag frag;
    UInt8               ttl;
    UInt8               proto;
    UInt16              check;
    UInt8               saddr[4];
    UInt8               daddr[4];
};

/* IPV6 header */
struct ETH_OFFLOAD_Emac_Ipv6 {
    UInt32              version : 4,
                    traffic_class : 8,
                    flow_label : 20;
    UInt16              length;
    UInt8               next_header;
    UInt8               hop_limit;
        UInt8               saddr[16];
        UInt8               daddr[16];
};

/* UDP header */
struct ETH_OFFLOAD_Emac_Udp {
    UInt16              srcport;
    UInt16              destport;
    UInt16              len;
    UInt16              csum;
};

/* TCP header */
struct ETH_OFFLOAD_Emac_Tcp  {
    UInt16              source;
    UInt16              dest;
    UInt32              seq;
    UInt32              ack_seq;
    UInt8               res1:4,
                    doff:4;
    UInt8               fin:1,
                    syn:1,
                    rst:1,
                    psh:1,
                    ack:1,
                    urg:1,
                    ece:1,
                    cwr:1;
    UInt16              window;
    UInt16              csum;
    UInt16              urg_ptr;
};

struct ETH_OFFLOAD_Emac_layerInfo {
    Void (*handler)         (struct ETH_OFFLOAD_params *params,
                        struct ETH_OFFLOAD_Emac_pktInfo *pkt);
    UInt16              offset;
    UInt16              size;
};


struct ETH_OFFLOAD_params {
    UInt32              status;
    UInt32              flag;
    UInt32              seq;
    UInt32              total;
    Int32               csum;
    struct ETH_OFFLOAD_frags      header;
    struct ETH_OFFLOAD_frags      data;
    struct ETH_OFFLOAD_Emac_layerInfo
                    layer[ETH_OFFLOAD_EMAC_NUMLAYERS];
    struct ETH_OFFLOAD_Emac_Ipv4Frag frag_off;
    struct ETH_OFFLOAD_inter    *pin;
};

struct ETH_OFFLOAD_Emac_pktInfo {
    UInt32              nfrags;
    UInt32              token;
    UInt32              tot_len;
    Int32               csum;
};

struct ETH_OFFLOAD_packet {
	Int32				header_size;
	Int32				net_offset;
	Int32				tport_offset;
	Int32				frag_off;
	Int32				mtu;
	Int32				csum;
	Int32				hdr_csum;
	UInt32				flag;
	UInt32				token;
	UInt32				total_frags;
	UInt16				*pcsum;
	UInt32				len;
	UInt32				count;
	UInt32				nfrags;
	UInt32				header;
	UInt32				residue;
	UInt32				org_address;
	UInt32				org_length;
	UInt32				data_offset;
	UInt32				seq;
	struct ETH_OFFLOAD_frags	*sg;
};

struct ETH_OFFLOAD_hashEntry {
    UInt32              hash;
    struct ETH_OFFLOAD_desc     *desc;
};


/*
 * txQ:pointer to ETH_OFFLOAD_queue
 * rxQ:pointer to Eth_OFFLOAD_queue
 */
struct ETH_OFFLOAD_inter {
    UInt32              offloadFlag;
    UInt32              MTU;
    UInt32              eventId;
    Handle              hEMAC;
    Task_Handle         hWorker;
    Semaphore_Handle        eventSem;
    struct ETH_OFFLOAD_offloadStats offloadStats;
    struct ETH_OFFLOAD_queue    *txQ;
    struct ETH_OFFLOAD_queue    *rxQ;
    struct ETH_OFFLOAD_packet	txPacket;
    UInt16          csumUnalignBuffer[1024];
    UInt32             isInit;
    UInt32          coal_intvl; // in usecs;
    Int32           semPendTimeout;
    UInt16              pktHeaderCnt;
    UInt8               (*pktHeader)[ETH_OFFLOAD_MAX_HEADER_SIZE];
    /* Memory for packet headers, as every header is copied to this memory to avoid cache alignment issues b/w arm and dsp */
    UInt8           gPktHeaderMem[ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES +
                    ETH_OFFLOAD_NUM_HEADERS*ETH_OFFLOAD_MAX_HEADER_SIZE];
};

static struct ETH_OFFLOAD_inter inter;
static void ETH_OFFLOAD_emacRxPacket(Handle hApp, UInt32 token, UInt32 flagLen);


#pragma DATA_SECTION(gETH_OFFLOAD_shm, ".eth_offload_shm")
ETH_OFFLOAD_shm gETH_OFFLOAD_shm;

Int32 ETH_OFFLOAD_Q_Poll(struct ETH_OFFLOAD_queue *Q, UInt32 weight);

/*
 * ETH_OFFLOAD_emacFreePacket: This function sends finished Tx packets to host
 * @hApp: Pointer to App handle
 * @pPkt: Paccket to be freed
 * @return: None
 */
static void ETH_OFFLOAD_emacFreePacket(Handle hApp, UInt32 token, UInt32 flagLen)
{
    struct ETH_OFFLOAD_inter *pin =
            (struct ETH_OFFLOAD_inter *)hApp;
    struct ETH_OFFLOAD_desc *desc =
            (struct ETH_OFFLOAD_desc *)token;

    if (desc) {
        /* Send the packet to host for freeing */
        ETH_OFFLOAD_Q_Enqueue(pin->txQ, desc);
    }
}

/*
 * ETH_OFFLOAD_emacStatus: Callback for fatal errors
 * @hApp: Pointer to App handle
 * @return: None
 */
static void ETH_OFFLOAD_emacStatus(Handle hApp, UInt32 macStatus)
{
    Vps_rprintf(" [ETH_OFFLOAD] Fatal Error : MACSTATUS = 0x%x !!!\n", macStatus);
}

/*
 * ETH_OFFLOAD_emacStats: Callback for stats update
 * @hApp: Pointer to App handle
 * @return: None
 */
static void ETH_OFFLOAD_emacStats(Handle hApp)
{

}

static int ETH_OFFLOAD_emacSetup(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    struct ETH_OFFLOAD_inter *pin = &inter;
    struct ETH_OFFLOAD_setup *devsetup = (struct ETH_OFFLOAD_setup*)
                        rpcMsg->rpcArg;

    pin->offloadFlag = devsetup->offloadFlag;

    return 1;
}

static int ETH_OFFLOAD_setCoalesce (struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    struct ETH_OFFLOAD_inter *pin = &inter;
    struct ETH_OFFLOAD_coalesce *coal = (struct ETH_OFFLOAD_coalesce *)
                        rpcMsg->rpcArg;

    if(coal->rx_coalesce_usecs>2000)
        coal->rx_coalesce_usecs = 2000;

    #if 0
    coal->coal_intvl = EMAC_set_coalesce(coal->rx_coalesce_usecs, coal->bus_freq_mhz);
    #endif

    coal->coal_intvl = coal->rx_coalesce_usecs;

    pin->coal_intvl = coal->coal_intvl;

    return 0;
}

static int ETH_OFFLOAD_ofldStats(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    struct ETH_OFFLOAD_inter *pin = &inter;
    struct ETH_OFFLOAD_offloadStats *ofldStats = (struct ETH_OFFLOAD_offloadStats*)
                                                rpcMsg->rpcArg;
    *ofldStats = pin->offloadStats;

    return 0;
}

/*
 * ETH_OFFLOAD_emacOpen: function open emac open,set freepool for transimt and receive
 * @rpcMsg: Pointer to ETH_OFFLOAD_RpcMsg structure
 * @return: 0 (no error)
 */
static int ETH_OFFLOAD_emacOpen(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    EMAC_Config  ecfg;
    EMAC_AddrConfig *addrCfg;
    EMAC_AddrConfig *addrs[1];
    EMAC_AddrConfig addr;
    struct ETH_OFFLOAD_inter *pin = &inter;
    struct ETH_OFFLOAD_open *devopen = (struct ETH_OFFLOAD_open*)
                        rpcMsg->rpcArg;

    memset(&ecfg, 0, sizeof(ecfg));
    /* Core 0 is master core in terms of EMAC */
    ecfg.CoreNum        = 0;
    /* Packet size */
    ecfg.PktMTU         = pin->MTU = devopen->mtu;
    /* Rx packet pool */
    ecfg.RxMaxPktPool   = ETH_OFFLOAD_HWDESC_MAX/2;
    /* Total number of MAC addresses for all receive channels assigned */
    ecfg.TotalNumOfMacAddrs = devopen->nmacaddrs;

    /* Selecting CPPI as EMAC descriptor memory */
    ecfg.DescBase       = EMAC_DESC_BASE_CPPI;
    /* Setup the EMAC local loopback */
    ecfg.ModeFlags      = EMAC_CONFIG_MODEFLG_FULLDUPLEX ;

    /* channel usage must be mutual exclusive among cores */
    ecfg.ChannelInfo[0].TxChanEnable = 1;  // 00000001, channel 0

    ecfg.ChannelInfo[0].RxChanEnable = 1;  // 00000001, channel 0

    /*
     * Below call back functions sould be define by the application
     * get packet call back
     */
    /* Free packet call back */
    ecfg.pfcbFreePacket            = ETH_OFFLOAD_emacFreePacket;
    /* Receive packet call back */
    ecfg.pfcbRxPacket              = ETH_OFFLOAD_emacRxPacket;
    /* Status update call back */
    ecfg.pfcbStatus                = ETH_OFFLOAD_emacStatus;
    /* Statistics update call back */
    ecfg.pfcbStatistics            = ETH_OFFLOAD_emacStats;

    ecfg.MacAddr = addrs;

    addrs[0] = &addr;
    addr.ChannelNum = ecfg.CoreNum;
    addrCfg = ecfg.MacAddr[0];
    memcpy(addrCfg->Addr, devopen->macaddrs, 6);

    if(0 != EMAC_open(devopen->phyid, (Handle)pin, &ecfg, &pin->hEMAC)){
        Vps_rprintf ("ERROR: EMAC_open Failed\n");
        return -1;
    }

    /* Setup the Header memory pointers */
    {
	    UInt32 s = (UInt32)pin->gPktHeaderMem;
	    UInt32 c = ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES;
	    pin->pktHeader = (UInt8 (*)[ETH_OFFLOAD_MAX_HEADER_SIZE])(((s + c)/c)*c);
	    pin->pktHeaderCnt = 0;
    }
    pin->semPendTimeout = 0;
    pin->isInit = TRUE;

    Vps_rprintf(" [ETH_OFFLOAD] OPENED !!!\n");

    return 0;
}

/*
 * ETH_OFFLOAD_emacStart: function to start the emac driver
 * @rpcMsg: Pointer to ETH_OFFLOAD_RpcMsg structure
 * @return: 0 (no error)
 */
static int ETH_OFFLOAD_emacStart(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    struct ETH_OFFLOAD_inter *pin = &inter;
    Int32 status;

    status = EMAC_start(pin->hEMAC);
    Vps_rprintf ("EMAC_start returned %d\n", status);

    if (0 != EMAC_setReceiveFilter(pin->hEMAC, EMAC_RXFILTER_ALL, 0)) {
        Vps_rprintf ("ERROR: EMAC_setReceiveFilter Failed \n");
        return -1;
    }

    return status;
}

static int ETH_OFFLOAD_getStatistics(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    UInt32 status = 0;
    EMAC_Statistics stats = {0};
    struct ETH_OFFLOAD_inter *pin = &inter;
    struct ETH_OFFLOAD_stats *ethoffload_stats =(void *)rpcMsg->rpcArg;

    status = EMAC_getStatistics(pin->hEMAC, &stats);
    if (status != 0) {
        return -1;
    }

    ethoffload_stats->multicast = (UInt32) (stats.RxMCastFrames);
    ethoffload_stats->collisions    = (UInt32) (stats.TxCollision
                    + stats.TxSingleColl
                        + stats.TxMultiColl);
    ethoffload_stats->rx_length_errors = (UInt32) (stats. RxOversized
                    + stats.RxJabber
                    + stats.RxUndersized);
    ethoffload_stats->rx_over_errors = (UInt32)(stats.RxSOFOverruns
                    + stats.RxMOFOverruns);
    ethoffload_stats->rx_fifo_errors = (UInt32)(stats.RxDMAOverruns);
    ethoffload_stats->tx_carrier_errors = (UInt32)(stats.TxCarrierSLoss);
    ethoffload_stats->tx_fifo_errors = (UInt32)(stats.TxUnderrun);

    return 1;
}

/*
 * ETH_OFFLOAD_emacClose: function to close the emac driver
 * @rpcMsg: Pointer to ETH_OFFLOAD_RpcMsg structure
 * @return: 0 (no error)
 */
static int ETH_OFFLOAD_emacClose(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    struct ETH_OFFLOAD_inter *pin = &inter;
    Uint32 status;
    pin->offloadFlag = 0;

    status = EMAC_close(pin->hEMAC);

    if(status != 0 ) {
        Vps_rprintf("Emac close returned error -status is %d!!!!!\n",status);
        return -1;
    }

    ETH_OFFLOAD_Q_Reset(pin->txQ);
    ETH_OFFLOAD_Q_Reset(pin->rxQ);

    pin->isInit = FALSE;
    pin->semPendTimeout = 10; /* dont wake from pend continously, instead timeout after 10ms */

    Vps_rprintf(" [ETH_OFFLOAD] CLOSED !!!\n");
    return 0;
}

/*
 * ETH_OFFLOAD_Emac_RpcHandler: function open the rpc's
 * @rpcMsg: Pointer to ETH_OFFLOAD_RpcMsg structure
 * @return: status value (which int type)
 */
static Int32 ETH_OFFLOAD_Emac_RpcHandler(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    int status = 1;

    switch(rpcMsg->rpcFuncId)
    {
        case ETH_OFFLOAD_EMAC_GET_STATISTICS:
            status = ETH_OFFLOAD_getStatistics(rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_EMAC_SETUP:
            status = ETH_OFFLOAD_emacSetup(rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_OPEN:
            status = ETH_OFFLOAD_emacOpen(rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_START:
            status = ETH_OFFLOAD_emacStart(inter.hEMAC);
            break;
        case ETH_OFFLOAD_FUNCID_CLOSE:
            status = ETH_OFFLOAD_emacClose(rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_COALESCE:
            status = ETH_OFFLOAD_setCoalesce(rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_UPDATE:
            status = EMAC_update((struct EMAC_updateData *)
                    rpcMsg->rpcArg);
            break;
        case ETH_OFFLOAD_FUNCID_OFLD_STATS:
            status = ETH_OFFLOAD_ofldStats (rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_OFLD_SETFLAG:
	    inter.offloadFlag |= ((UInt32)*rpcMsg->rpcArg);
            status = 0;
            break;
        case ETH_OFFLOAD_FUNCID_OFLD_UNSETFLAG:
	    inter.offloadFlag &= ~((UInt32)*rpcMsg->rpcArg);
            status = 0;
            break;
        default:
            status = -1;
    }
    return status;
}

static inline Void ETH_OFFLOAD_Emac_cacheWb(UInt8 *ptr, UInt32 size)
{
	Cache_wbInv((Ptr)ETH_OFFLOAD_FLOOR(((UInt32)ptr), ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES),
		ETH_OFFLOAD_ALIGN(size+ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES, ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES),
		Cache_Type_ALL, TRUE);
}

static inline Void ETH_OFFLOAD_Emac_cacheInv(UInt8 *ptr, UInt32 size)
{
	Cache_inv((Ptr)ETH_OFFLOAD_FLOOR(((UInt32)ptr), ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES),
		ETH_OFFLOAD_ALIGN(size+ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES, ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES),
		Cache_Type_ALL, TRUE);
}

/*
 * ETH_OFFLOAD_Emac_csum: Function to do check sum
 * @addr: address of short type
 * @len: length
 * @return: answer of integer type
 */
static UInt32 ETH_OFFLOAD_Emac_csum(struct ETH_OFFLOAD_inter *pin, volatile UInt16 *addr, UInt32 numbytes)
{
    register UInt32 sum = 0;
    volatile UInt16 *w = addr;
    register UInt16 last = 0;

    /* Align to 16-bit address */
    addr = (volatile UInt16 *)(((UInt32)addr)&(~0x1));

    if (w != addr) {
        memcpy((Ptr)pin->csumUnalignBuffer, (Ptr)w, numbytes);
        w = (UInt16*)pin->csumUnalignBuffer;
    }

    /*
     * Algorithm is simple, using a 32 bit accumulator (sum), we add
     * sequential 16 bit words to it, and at the end, fold back all the
     * carry bits from the top 16 bits into the lower 16 bits.
     */
    while (numbytes > 1)  {
        sum += htons(*w);
        w++;
        numbytes -= 2;
    }

    /* mop up an odd byte, if necessary */
    if (numbytes == 1) {
        last = htons(*w);
        sum += last&0xFF00;
    }

    return sum;
}

static UInt16 ETH_OFFLOAD_Emac_csumFold(UInt32 sum)
{
    UInt16 answer = 0;

    /* add back carry outs from top 16 bits to low 16 bits */
    sum = (sum >> 16) + (sum & 0xffff); /* add hi 16 to low 16 */
    sum += (sum >> 16);         /* add carry */
    answer = ~sum;              /* truncate to 16 bits */
    return htons(answer);
}

static Int32 ETH_OFFLOAD_Emac_csumIpPesudoHeader(struct ETH_OFFLOAD_inter *pin,
			struct ETH_OFFLOAD_Emac_Ipv4 *ipv4)
{
	return (htons(ipv4->len) - (ipv4->ihl<<2))
		  + (ipv4->proto&0xFF)
		  + ETH_OFFLOAD_Emac_csum(pin,
			(UInt16*)ipv4->saddr, 2*sizeof(ipv4->saddr));
}

static Int32 ETH_OFFLOAD_Emac_ipv4Csum(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_packet *pkt)
{
	struct ETH_OFFLOAD_Emac_Ipv4 *ipv4 = 0;
	UInt16 computed_csum;

	/* update ipheader checksum */
	ipv4 = (struct ETH_OFFLOAD_Emac_Ipv4 *)(pkt->header + pkt->net_offset);
	ipv4->check = 0;
	computed_csum = ETH_OFFLOAD_Emac_csumFold(
			ETH_OFFLOAD_Emac_csum(pin, (UInt16 *)ipv4,
			sizeof(struct ETH_OFFLOAD_Emac_Ipv4)));
	ipv4->check = computed_csum;
	return 1;
}

static UInt8 *ETH_OFFLOAD_Emac_copyHeader(struct ETH_OFFLOAD_inter *pin,
		 struct ETH_OFFLOAD_packet *pkt)
{
	UInt32 pcsum = (UInt32)pkt->pcsum;
	UInt8 *header = pin->pktHeader[pin->pktHeaderCnt];

	if (pin->pktHeaderCnt++ > ETH_OFFLOAD_NUM_HEADERS) {
		pin->pktHeaderCnt = 0;
	}
    ETH_OFFLOAD_Emac_cacheInv((UInt8*)header, ETH_OFFLOAD_MAX_HEADER_SIZE);
	if (pkt->header) {
		memcpy(header, (UInt8 *)pkt->header, pkt->header_size);
	} else {
		memcpy(header, (UInt8*)pkt->sg[0].dma_address, pkt->header_size);
		pkt->pcsum = (UInt16 *)(header + pcsum - pkt->sg[0].dma_address);
		*pkt->pcsum = 0;
		/* Compute csum of UDP/TCP Header */
		pkt->csum += ETH_OFFLOAD_Emac_csum(pin,
		                    (UInt16 *)(header + pkt->tport_offset),
		                    pkt->header_size - pkt->tport_offset);
	}
	pkt->header = (UInt32)header;
	pkt->flag |= ETH_OFFLOAD_PARAMS_FLAG_HEADER;
	return header;
}

static Int32 ETH_OFFLOAD_Emac_csumHeader(struct ETH_OFFLOAD_inter *pin,
		 struct ETH_OFFLOAD_packet *pkt)
{
	UInt8 *start = 0;
	Int32 type = 0, proto = 0, header = 0;
	struct ETH_OFFLOAD_Emac_Ipv4 *ipv4 = 0;

	pkt->mtu = pin->MTU;
	pkt->pcsum = 0;
	start = (UInt8 *)pkt->sg[pkt->count].dma_address;
	type = ((struct ETH_OFFLOAD_Emac_Eth1 *)start)->type;

	if(pkt->sg[pkt->count].length < (sizeof(struct ETH_OFFLOAD_Emac_Eth2) +
					sizeof(struct ETH_OFFLOAD_Emac_Ipv4))) {
		Vps_rprintf("First frag len only %d, doesnt contain ip header\n",pkt->sg[pkt->count].length);
	}

	if (type == ETH_OFFLOAD_ETHTYPE_ETH_8021q) {
		type = ((struct ETH_OFFLOAD_Emac_Eth2 *)start)->encap_proto;
		header += sizeof(struct ETH_OFFLOAD_Emac_Eth2);
	} else {
		header += sizeof(struct ETH_OFFLOAD_Emac_Eth1);
	}

	pkt->net_offset = header;

	if (type == ETH_OFFLOAD_ETHTYPE_ETH_IPV4) {
		ipv4 = (struct ETH_OFFLOAD_Emac_Ipv4 *)(start + header);
		proto = ipv4->proto;
		header += (ipv4->ihl<<2);
	} else {
		pkt->net_offset = 0;
		pkt->tport_offset = 0;
		return 0;
	}

	pkt->tport_offset = header;

	if (proto == ETH_OFFLOAD_NET_IPUDP) {
		pkt->pcsum = &((struct ETH_OFFLOAD_Emac_Udp *)(start + header))->csum;
		header += sizeof(struct ETH_OFFLOAD_Emac_Udp);
	} else if (proto == ETH_OFFLOAD_NET_IPTCP) {
		if(pkt->sg[pkt->count].length < header + sizeof (struct ETH_OFFLOAD_Emac_Tcp)) {
			Vps_rprintf("First frag len only %d, doesnt contain tcp header\n",pkt->sg[pkt->count].length);
		}
		pkt->pcsum = &((struct ETH_OFFLOAD_Emac_Tcp *)(start + header))->csum;
		header += ((struct ETH_OFFLOAD_Emac_Tcp *)(start + header))->doff*4;
	} else {
		pkt->net_offset = 0;
		return 0;
	}

	pkt->header_size = header;

	/* Check if data is on this frag */
	if (pkt->sg[pkt->count].length < header) {
		/* full header is not in first fragment, we cannot handle this*/
		Vps_rprintf("First frag len only %d, doesnt contain full transport header\n",pkt->sg[pkt->count].length);
	} else if (pkt->sg[pkt->count].length == header && pkt->nfrags > 1) {
		pkt->count++;
		pkt->data_offset = 0;
	} else {
		pkt->data_offset = header;
	}

	if (ipv4) {
		return ETH_OFFLOAD_Emac_csumIpPesudoHeader(pin, ipv4);
	}
	return 0;
}


static Int32 ETH_OFFLOAD_Emac_rxOffloadHandler(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_desc *desc)
{
	struct ETH_OFFLOAD_packet pkt;

    ETH_OFFLOAD_Emac_cacheInv((Ptr)desc->sg[0].dma_address, desc->length);

	/* Setup values */
	pkt.sg = desc->sg;
	pkt.len = desc->length;
	pkt.count = 0;
	pkt.csum = ETH_OFFLOAD_Emac_csumHeader(pin, &pkt);

    /* Compute checksum of data */
	if (pkt.pcsum) {
		pkt.csum += ETH_OFFLOAD_Emac_csum(pin,
				(UInt16 *)(desc->sg[0].dma_address + pkt.tport_offset),
				desc->sg[0].length - pkt.tport_offset);
		if (ETH_OFFLOAD_Emac_csumFold(pkt.csum)) {
			/* Checksum has failed, so drop the packet */
			return 0;
		}
	}

	/* Accept the packet */
	return 1;
}

/*
 * ETH_OFFLOAD_emacRxPacket: This function sends rx packet to host
 * @hApp: Pointer to App handle
 * @pPkt: Paccket to be freed
 * @return: None
 */
static void ETH_OFFLOAD_emacRxPacket(Handle hApp, UInt32 token, UInt32 flagLen)
{
    struct ETH_OFFLOAD_inter *pin =
            (struct ETH_OFFLOAD_inter *)hApp;
    struct ETH_OFFLOAD_desc *desc =
            (struct ETH_OFFLOAD_desc *)token;
    int csumOk;

    if (desc == NULL) {
        return;
    }

    /* extract values */
    desc->length = desc->sg[0].length = flagLen&0xFFFF;
    desc->status = 0;

    /* do offloading work for rx path */
    if(pin->offloadFlag & ETH_OFFLOAD_FLAG_RX_CSUM) {
	desc->status = 0x1;
        csumOk = ETH_OFFLOAD_Emac_rxOffloadHandler(pin, desc);

        if(!csumOk) {
            /* Dropping the packet so re-queue */
            desc->status |= 0x2;
        }
    }

    /* Push received packet to host */
    ETH_OFFLOAD_Q_Enqueue(pin->rxQ, desc);
}


/*
 * ETH_OFFLOAD_Emac_rxHandler: Funtion receive handler which call Emac loopback function
 * @return: 1 on success
 */
static Int32 ETH_OFFLOAD_Emac_rxHandler(void *hApp, struct ETH_OFFLOAD_desc *desc)
{
    struct ETH_OFFLOAD_inter *pin = (struct ETH_OFFLOAD_inter *)hApp;

    if (EMAC_enqRxDesc(pin->hEMAC, (UInt8 *)desc->sg[0].dma_address,
            (UInt32)desc)) {
        return 1;
    }

    /* Oops failed to queue te desc */
    return 0;
}

static UInt32 enq_fail_count = 0;

static inline Int32 ETH_OFFLOAD_Emac_enqueueFrag(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_packet *pkt)
{
	UInt32 done = ETH_OFFLOAD_PARAMS_FLAG_LAST | ETH_OFFLOAD_PARAMS_FLAG_DONE;
	UInt32 token = 0  ;
	UInt32 flags = 0;
	UInt32 address = pkt->sg[pkt->count].dma_address;
	UInt32 length = pkt->sg[pkt->count].length;

	if ((pkt->flag&ETH_OFFLOAD_PARAMS_FLAG_HEADER)) {
		flags = pkt->len | ETH_OFFLOAD_PARAMS_FLAG_SOP;
		if ((pkt->nfrags == 1) && (pkt->sg[pkt->count].length == pkt->data_offset)) {
			flags |= ETH_OFFLOAD_PARAMS_FLAG_EOP;
			token = (UInt32)pkt->token;
		}
		if (!EMAC_enqTxDesc(pin->hEMAC, (UInt8* )pkt->header,
			pkt->header_size, flags, token)) {
			/* Could not enqueue the header, so retry later */
			pkt->flag |= ETH_OFFLOAD_PARAMS_FLAG_PARTIAL;
			return 0;
		}
		pkt->flag &= ~(ETH_OFFLOAD_PARAMS_FLAG_HEADER|ETH_OFFLOAD_PARAMS_FLAG_SOP);
	}

	if ((pkt->flag&done) == done) {
		token = (UInt32)pkt->token;
	}

	/* Ensure there is some data to give to hw */
	if (pkt->sg[pkt->count].length > pkt->data_offset) {
		if (!EMAC_enqTxDesc(pin->hEMAC,
			(UInt8* )pkt->sg[pkt->count].dma_address + pkt->data_offset,
			pkt->sg[pkt->count].length - pkt->data_offset,
			pkt->flag, token)) {
			pkt->flag |= ETH_OFFLOAD_PARAMS_FLAG_PARTIAL;
			/* Could not enqueue the data, so retry later */
			enq_fail_count++;
			return 0;
		}
	}
	pkt->data_offset = 0;
	pkt->flag &= 0x3200FFFF;
	pkt->count++;
	return 1;
}

static inline Int32 ETH_OFFLOAD_Emac_enqueuePacket(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_packet *pkt)
{
	UInt16 computed_csum = 0;

	if ((pkt->flag&ETH_OFFLOAD_PARAMS_FLAG_PARTIAL)
		== ETH_OFFLOAD_PARAMS_FLAG_PARTIAL) {
		if (!ETH_OFFLOAD_Emac_enqueueFrag(pin, pkt)) {
			return 0;
		}
	} else {
		pkt->flag |= pkt->len | ETH_OFFLOAD_PARAMS_FLAG_SOP;
	}

	if (pkt->pcsum &&
		((pkt->flag&ETH_OFFLOAD_PARAMS_FLAG_FIRST)
			 == ETH_OFFLOAD_PARAMS_FLAG_FIRST)) {
		/* According to the RFC, the udp/tcp header */
		/* should reset the csum value while computing the csum */
		*pkt->pcsum = 0;
	}

	while (pkt->count < pkt->nfrags) {
		if ((pkt->count + 1) == pkt->nfrags) {
			pkt->flag |= ETH_OFFLOAD_PARAMS_FLAG_EOP;
		}
		if (pkt->pcsum) {
			pkt->csum += ETH_OFFLOAD_Emac_csum(pin,
					(UInt16 *)(pkt->sg[pkt->count].dma_address + pkt->data_offset),
					pkt->sg[pkt->count].length - pkt->data_offset);
			if ((pkt->flag&ETH_OFFLOAD_PARAMS_FLAG_LAST)
					== ETH_OFFLOAD_PARAMS_FLAG_LAST) {
				computed_csum = ETH_OFFLOAD_Emac_csumFold(pkt->csum);
				*pkt->pcsum = computed_csum;
				ETH_OFFLOAD_Emac_cacheWb((UInt8*)pkt->pcsum, 2);
			}
		}
		if (!ETH_OFFLOAD_Emac_enqueueFrag(pin, pkt)) {
			return 0;
		}
	}
	/* Flush cache after the complete packet is queued */
	if(pkt->header && pkt->header_size) {
		ETH_OFFLOAD_Emac_cacheWb((UInt8*)pkt->header,
					pkt->header_size);
	}
	return 1;
}

static inline Int32 ETH_OFFLOAD_Emac_updateHeader(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_packet *pkt)
{
	struct ETH_OFFLOAD_Emac_Ipv4Frag *frag;
	struct ETH_OFFLOAD_Emac_Ipv4 *ipv4 = 0;
	UInt16 data16;

	if (ETH_OFFLOAD_PARAMS_FLAG_ONESEG
			== (pkt->flag&ETH_OFFLOAD_PARAMS_FLAG_ONESEG)) {
		/* One segment, all values are already in place */
		return 0;
	} else if (pkt->net_offset) {
		ipv4 = (struct ETH_OFFLOAD_Emac_Ipv4 *)(pkt->header + pkt->net_offset);
		ipv4->len = pkt->len - pkt->net_offset;
		ipv4->len = htons(ipv4->len);
	} else {
		/* Not ipv4 packet. Then no updations */
		return 0;
	}

	if (ipv4->proto == ETH_OFFLOAD_NET_IPUDP) {
		data16 = *(UInt16 *)&ipv4->frag;
		data16 = htons(data16);
		frag = (struct ETH_OFFLOAD_Emac_Ipv4Frag *)&data16;
		/* UFO: ip header fragmentation */
		frag->offset += pkt->frag_off/8;
		pkt->frag_off = pkt->len - pkt->tport_offset;
		if (pkt->flag & ETH_OFFLOAD_PARAMS_FLAG_DONE) {
			frag->mf = 0;
		} else {
			frag->mf = 1;
		}
		data16 = htons(data16);
		ipv4->frag = *frag;
	}
	/* update ipheader checksum */
	ETH_OFFLOAD_Emac_ipv4Csum(pin, pkt);

	return 1;
}

static inline Int32 ETH_OFFLOAD_Emac_nextPacket(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_packet *pkt)
{
	/* 60(min packet size)-34(udp header)=26 */
	UInt32 min_size = ETH_OFFLOAD_ETH_MINFRAMESIZE - pkt->header_size;

	if (pkt->flag&ETH_OFFLOAD_PARAMS_FLAG_SOS) {
		/* First fragment, also the header */
		pkt->header = 0;
		pkt->nfrags = 0;
		pkt->len = 0;
		pkt->frag_off = 0;
		pkt->residue = 0;
		pkt->seq = 0;
		pkt->org_address = 0;
		pkt->org_length = 0;
	} else {
		pkt->header_size = pkt->tport_offset;
		pkt->len = pkt->header_size;
		if (pkt->residue) pkt->count--;
	}
	ETH_OFFLOAD_Emac_copyHeader(pin, pkt);

	/* Do segmentation on mtu/mss */
	while (pkt->nfrags < pkt->total_frags &&
	        pkt->residue == 0 && pkt->len <= pkt->mtu) {
		pkt->len += pkt->sg[pkt->nfrags].length;
		pkt->nfrags++;
	}

	if (pkt->len > pkt->mtu) {
		pkt->org_address = pkt->sg[pkt->nfrags-1].dma_address;
		pkt->org_length = pkt->sg[pkt->nfrags-1].length;
		pkt->residue = pkt->sg[pkt->nfrags-1].length;
		pkt->sg[pkt->nfrags-1].length = 0;
		pkt->len -= pkt->residue;
	}
	if (pkt->residue + pkt->len > pkt->mtu) {
		pkt->residue -= pkt->mtu - pkt->len;
		pkt->sg[pkt->nfrags-1].dma_address += pkt->sg[pkt->nfrags-1].length;
		pkt->sg[pkt->nfrags-1].length = pkt->mtu - pkt->len;
		pkt->len = pkt->mtu;
	} else if (pkt->residue > 0) {
		pkt->len += pkt->residue;
		pkt->sg[pkt->nfrags-1].dma_address += pkt->sg[pkt->nfrags-1].length;
		pkt->sg[pkt->nfrags-1].length = pkt->residue;
		pkt->residue = 0;
		}

	/* Avoid less than 60 bytes packets */
	if (pkt->residue > 0 && pkt->residue < min_size) {
		/* Ensure min_size is nearest multiple of 8 */
		min_size = ((min_size - pkt->residue + 8)/8)*8;
		pkt->len -= min_size;
		pkt->sg[pkt->nfrags-1].length -= min_size;
		pkt->residue += min_size;
	}

	if (pkt->nfrags >= pkt->total_frags && pkt->residue == 0) {
		return ETH_OFFLOAD_PARAMS_FLAG_EOS|ETH_OFFLOAD_PARAMS_FLAG_DONE;
	}

	return 0;
}

static Int32 ETH_OFFLOAD_Emac_txOffloadHandlerAll(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_desc *desc)
{
	struct ETH_OFFLOAD_packet *pkt = &pin->txPacket;
	int i;

	/* Is this a partially queued prev packet? */
	if ((pkt->flag&ETH_OFFLOAD_PARAMS_FLAG_PARTIAL)
		== ETH_OFFLOAD_PARAMS_FLAG_PARTIAL) {
			goto resume_fragment;
	} else {
		/* Fresh packet, so setup values */
		pkt->sg = desc->sg;
		pkt->flag = ETH_OFFLOAD_PARAMS_FLAG_SOS;
		pkt->total_frags = desc->nfrags;
		pkt->count = 0;
		pkt->token = (UInt32)desc;

		/* Invalidate all the frags */
		for (i=0;i < desc->nfrags;i++) {
			ETH_OFFLOAD_Emac_cacheInv((UInt8 *)desc->sg[i].dma_address, desc->sg[i].length);
		}
		/* Parse the header and gather vitals */
		pkt->csum = ETH_OFFLOAD_Emac_csumHeader(pin, pkt);
	}

	/* segment packets based on mtu/mss */
	while (!(pkt->flag&ETH_OFFLOAD_PARAMS_FLAG_DONE)) {

		/* Get a packet and update its header */
		pkt->flag |= ETH_OFFLOAD_Emac_nextPacket(pin, pkt);

		ETH_OFFLOAD_Emac_updateHeader(pin, pkt);

	resume_fragment:
		/* Queue the packet to hw */
		if (!ETH_OFFLOAD_Emac_enqueuePacket(pin, pkt)) {
			/* Ran out of desc */
			return 0;
		}

		if (pkt->residue == 0 && pkt->org_address > 0 && pkt->org_length > 0) {
			/* restore the original values of dma_address and length */
			pkt->sg[pkt->nfrags-1].dma_address = pkt->org_address;
			pkt->sg[pkt->nfrags-1].length = pkt->org_length;
			pkt->org_address = 0;
			pkt->org_length = 0;
		}

		if (pkt->flag&ETH_OFFLOAD_PARAMS_FLAG_DONE) {
			/* Finished the seg */
			pkt->flag = 0;
			break;
		}
		pkt->flag = 0;
	}
	EMAC_enqToHw(pin->hEMAC);

	return 1;
}


Int32 ETH_OFFLOAD_Emac_txOffloadHandlerSG(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_desc *desc)
{
	struct ETH_OFFLOAD_packet *pkt = &pin->txPacket;
	int i;

	/* Fresh packet, so setup values */
	pkt->header = 0;
	pkt->header_size = 0;
	pkt->sg = desc->sg;
	pkt->len = desc->length;

	pkt->flag = ETH_OFFLOAD_PARAMS_FLAG_SOS
		  | ETH_OFFLOAD_PARAMS_FLAG_EOS
		  | ETH_OFFLOAD_PARAMS_FLAG_DONE;
	pkt->nfrags = desc->nfrags;
	pkt->count = 0;
	pkt->token = (UInt32)desc;


	/* Invalidate all the frags */
	for (i=0;i < desc->nfrags;i++) {
		ETH_OFFLOAD_Emac_cacheInv((UInt8 *)desc->sg[i].dma_address, desc->sg[i].length);
	}

	/* Parse the header and gather vitals */
	pkt->csum = ETH_OFFLOAD_Emac_csumHeader(pin, pkt);

	if(pkt->pcsum) {
		ETH_OFFLOAD_Emac_copyHeader(pin, pkt);
		ETH_OFFLOAD_Emac_ipv4Csum(pin, pkt);
	}

	ETH_OFFLOAD_Emac_enqueuePacket(pin, pkt);
	EMAC_enqToHw(pin->hEMAC);
	return 1;
}

Int32 ETH_OFFLOAD_Emac_txOffloadHandlerCsum(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_desc *desc)
{
	struct ETH_OFFLOAD_packet pkt;
	UInt16 computed_csum = 0;

    ETH_OFFLOAD_Emac_cacheInv((Ptr)desc->sg[0].dma_address, desc->length);

	/* Setup values */
	pkt.header = desc->sg[0].dma_address;
	pkt.sg = desc->sg;
	pkt.len = desc->length;
	pkt.count = 0;
	pkt.csum = ETH_OFFLOAD_Emac_csumHeader(pin, &pkt);

	if (pkt.pcsum) {
		*pkt.pcsum = 0;
		pkt.csum += ETH_OFFLOAD_Emac_csum(pin,
				(UInt16 *)(desc->sg[0].dma_address + pkt.tport_offset),
				desc->length - pkt.tport_offset);
		computed_csum = ETH_OFFLOAD_Emac_csumFold(pkt.csum);
		ETH_OFFLOAD_Emac_cacheInv((UInt8 *)pkt.pcsum, 2);
		*pkt.pcsum = computed_csum;
		ETH_OFFLOAD_Emac_cacheWb((UInt8 *)pkt.pcsum, 2);
		ETH_OFFLOAD_Emac_ipv4Csum(pin, &pkt);
    }
    return 1;
}

/*
 * ETH_OFFLOAD_Emac_txHandler: Function transmit handler which call Enqueue function
 * @return: 1 on success
 */
static Int32 ETH_OFFLOAD_Emac_txHandler(void *hApp, struct ETH_OFFLOAD_desc *data)
{
    struct ETH_OFFLOAD_inter *pin = (struct ETH_OFFLOAD_inter *)hApp;
    struct ETH_OFFLOAD_desc *desc = (struct ETH_OFFLOAD_desc *)data;
    Int32 status = 1;


    if (EMAC_txFreeCount(pin->hEMAC) < desc->nfrags+1) {
        /* Not enough hw desc available, try next time */
        return 0;
    }

    if (desc->length > pin->MTU) { //pin->offloadFlag & ETH_OFFLOAD_FLAG_TX_UFO) {
	/* Handle combination of checksum, SG & >mtu packets */
        return ETH_OFFLOAD_Emac_txOffloadHandlerAll(pin, desc);
    } else if (desc->nfrags > 1) { //pin->offloadFlag & ETH_OFFLOAD_FLAG_TX_SG) {
	/* Handle checksum & SG */
        return ETH_OFFLOAD_Emac_txOffloadHandlerSG(pin, desc);
    } else if (pin->offloadFlag & ETH_OFFLOAD_FLAG_TX_CSUM) {
	/* Handle only Checksum */
        //ETH_OFFLOAD_Emac_txOffloadHandlerCsum(pin, desc);
		return ETH_OFFLOAD_Emac_txOffloadHandlerSG(pin, desc);
    }

    status = EMAC_enqTxDesc(pin->hEMAC,
                (Uint8 *)desc->sg[0].dma_address,
                desc->sg[0].length,
                (desc->length | ETH_OFFLOAD_PARAMS_FLAG_SOP | ETH_OFFLOAD_PARAMS_FLAG_EOP),
                (UInt32)desc);
    EMAC_enqToHw(pin->hEMAC);

    return status;
}

static Void ETH_OFFLOAD_Emac_worker(UArg arg0, UArg arg1)
{
    UInt32 key = 0;
    struct ETH_OFFLOAD_inter *pin = &inter;


    while (1) {

       /* Block the Event Handling Task */
       Semaphore_pend(pin->eventSem, pin->semPendTimeout);

        if(!pin->isInit)
            continue;

        key = Hwi_disable();

        ETH_OFFLOAD_EMAC_Poll(pin->hEMAC);

        ETH_OFFLOAD_Q_Poll(pin->txQ, ETH_OFFLOAD_Q_WEIGHT);
        ETH_OFFLOAD_Q_Poll(pin->rxQ, ETH_OFFLOAD_Q_WEIGHT);

        Hwi_restore(key);
    }
}

/*
 * ETH_OFFLOAD_TaskInit: Function to init task structures
 * @return: 1 if success, else 0
 */
static Int32 ETH_OFFLOAD_Emac_TaskInit (struct ETH_OFFLOAD_inter *pin, UInt8 *stack, UInt32 stackSize)
{
        Task_Params tskParams;
    Semaphore_Params semParams;

    /* create semaphore to manage critical sections of task data */
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
    pin->eventSem = Semaphore_create(1u, &semParams, NULL);

    pin->isInit = FALSE;
    pin->semPendTimeout = 10;

    /* No point continuing with out task */
    UTILS_assert(pin->eventSem != NULL);

    /* Create task to handle the packet processing task */
    Task_Params_init(&tskParams);
    tskParams.arg0 = (UArg)pin;
    tskParams.stack = stack;
    tskParams.stackSize = stackSize;
    tskParams.priority = 1; /* keep this to lowest priority, since this is polling continously */
    pin->hWorker = Task_create(ETH_OFFLOAD_Emac_worker, &tskParams, NULL);
    UTILS_assert(pin->hWorker != NULL);

    return 0;
}

/*
 * ETH_OFFLOAD_QInit: Function to init task structures
 * @return: 1 if success, else 0
 */
static Void ETH_OFFLOAD_Emac_QInit (struct ETH_OFFLOAD_inter *pin)
{
    pin->txQ = ETH_OFFLOAD_Q_Create(ETH_OFFLOAD_SWDESC_MAX,
                        (QCALLBACK)ETH_OFFLOAD_Emac_txHandler,
                        (void *)pin, "TX",
                        &gETH_OFFLOAD_shm.txSwDesc[0]
                        );

    pin->rxQ = ETH_OFFLOAD_Q_Create(ETH_OFFLOAD_SWDESC_MAX,
                        (QCALLBACK)ETH_OFFLOAD_Emac_rxHandler,
                        (void *)pin, "RX",
                        &gETH_OFFLOAD_shm.rxSwDesc[0]
                        );
}

/*
 * ETH_OFFLOAD_Q_DeinitTask: Function to destroy task
 * @return: None
 */
static Void ETH_OFFLOAD_DeinitTask(Void)
{
    struct ETH_OFFLOAD_inter *pin = &inter;

    /* Remove the semaphore when unloading module */
    Semaphore_delete(&pin->eventSem);
    /* Kill the task here */
    Task_delete(&pin->hWorker);
}

/*
 * ETH_OFFLOAD_Emac_Init: It is Init fucntion for Emac for where transmit and receive ETH _OFFLOAD_Q are created
 * @return: 1 on success
 */
Int32 ETH_OFFLOAD_Emac_Init(Void)
{
    struct ETH_OFFLOAD_inter *pin = &inter;

    memset(pin, 0, sizeof(*pin));

    ETH_OFFLOAD_Rpc_RegisterHandler(ETH_OFFLOAD_Emac_RpcHandler);
    EMAC_init();

    ETH_OFFLOAD_Emac_QInit (pin);
    ETH_OFFLOAD_Emac_TaskInit (pin, gTskStack, sizeof(gTskStack));

    return 0;
}



/*
 * ETh_OFFLOAD_Emac_DeInit: function Deinit Emac driver
 * @return: 1 on success
 */
Int32 ETH_OFFLOAD_Emac_DeInit(Void)
{
    UInt32 status;
    struct ETH_OFFLOAD_inter *pin = &inter;

    /* Kill the Task */
    ETH_OFFLOAD_DeinitTask();


    EMAC_deinit();

    status = ETH_OFFLOAD_Q_Destroy(pin->txQ);
    if( status != 1) {
        Vps_rprintf("DSP:IN DEInit: ETH_OFFLOAD_Q_Destroy is failed!!!!!\n");
        return -1;
    }

    status = ETH_OFFLOAD_Q_Destroy(pin->rxQ);
    if( status != 1){
        Vps_rprintf("DSP:IN DEInit: ETH_OFFLOAD_Q_Destroy is failed!!!!!\n");
        return -1;
    }

    return 1;
}
