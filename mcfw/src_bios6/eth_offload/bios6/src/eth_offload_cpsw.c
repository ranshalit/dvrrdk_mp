/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <mcfw/src_bios6/utils/utils.h>

#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/hal/Hwi.h>

#include <mcfw/src_bios6/eth_offload/inc/eth_offload_config.h>
#include <mcfw/src_bios6/eth_offload/inc/eth_offload_rpc.h>
#include <mcfw/src_bios6/eth_offload/inc/eth_offload_q.h>
#include <mcfw/src_bios6/eth_offload/inc/eth_offload_cpsw.h>
// #include <mcfw/src_bios6/eth_offload/bios6/inc/cpsw/csl.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/cpsw/emac_common.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/cpsw/csl_emac.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/cpsw/csl_emacAux.h>
#include <mcfw/src_bios6/eth_offload/bios6/inc/cpsw/cpsw_miimdio.h>



#define ETH_OFFLOAD_ETHTYPE_ETH_8021q       0x0081
#define ETH_OFFLOAD_ETHTYPE_ETH_8021q_2     0xA888
#define ETH_OFFLOAD_ETHTYPE_ETH_IPV4        0x0008
#define ETH_OFFLOAD_ETHTYPE_ETH_IPV6        0xDD86

#define ETH_OFFLOAD_NET_IPUDP           17
#define ETH_OFFLOAD_NET_IPTCP           6

#define ETH_OFFLOAD_NUM_HEADERS         64
#define ETH_OFFLOAD_MAX_HEADER_SIZE         64
#define ETH_OFFLOAD_EMAC_NUMLAYERS      3
#define ETH_OFFLOAD_ETH_MINFRAMESIZE        60
#define ETH_OFFLOAD_TSK_STACK_SIZE      (16*1024)

#define ETH_OFFLOAD_LRO_TABLE_SIZE      31

#define htons(a)                ((((a)&0xFF)<<8)|(((a)&0xFF00)>>8))
#define htonl(a)                ((((a)&0xFF)<<24)|(((a)&0xFF00)<<8)|(((a)&0xFF0000)>>8)|(((a)&0xFF000000)>>24))

#define ETH_OFFLOAD_PARAMS_FLAG_MOP     (0x00)
#define ETH_OFFLOAD_PARAMS_FLAG_EOP     (0x01)
#define ETH_OFFLOAD_PARAMS_FLAG_SOP     (0x02)
#define ETH_OFFLOAD_PARAMS_FLAG_NOP     (0x04)
#define ETH_OFFLOAD_PARAMS_FLAG_RX      (0x08)
#define ETH_OFFLOAD_PARAMS_FLAG_GFO     (0x10)
#define ETH_OFFLOAD_PARAMS_FLAG_GSO     (0x20)
#define ETH_OFFLOAD_PARAMS_FLAG_CSUM        (0x40)
#define ETH_OFFLOAD_PARAMS_FLAG_DROP        (0x80)

#define ETH_OFFLOAD_PARAMS_RESETFLAG(p, f)  ((p)->flag = (((p)->flag&0xFFFFFFF0)|ETH_OFFLOAD_PARAMS_FLAG_##f))
#define ETH_OFFLOAD_PARAMS_SETFLAG(p, f)    ((p)->flag |= ETH_OFFLOAD_PARAMS_FLAG_##f)
#define ETH_OFFLOAD_PARAMS_UNSETFLAG(p, f)  ((p)->flag &= ~ETH_OFFLOAD_PARAMS_FLAG_##f)
#define ETH_OFFLOAD_PARAMS_CHECKFLAG(p, f)  ((p)->flag & ETH_OFFLOAD_PARAMS_FLAG_##f)

/* Task related variable */
#pragma DATA_ALIGN(gTskStack, 32)
#pragma DATA_SECTION(gTskStack, ".bss:taskStackSection")
static UInt8 gTskStack[ETH_OFFLOAD_TSK_STACK_SIZE];
/***/

struct ETH_OFFLOAD_params;
struct ETH_OFFLOAD_Cpsw_pktInfo;

/* Supported Network headers */

/* Ethernet headers */
struct ETH_OFFLOAD_Cpsw_Eth1 {
    UInt8               dest[6];
    UInt8               source[6];
    UInt16              type;
} __attribute__((packed));

struct ETH_OFFLOAD_Cpsw_Eth2 {
    struct ETH_OFFLOAD_Cpsw_Eth1    eth1;
    UInt16              tci;
    UInt16              encap_proto;
} __attribute__((packed));


struct ETH_OFFLOAD_Cpsw_Ipv4Frag {
    UInt16
                    offset:13,
                    mf:1,
                    df:1,
                    rsvd:1;
};

/* IPV4 header */
struct ETH_OFFLOAD_Cpsw_Ipv4 {
    UInt8               ihl:4,
                    version:4;
    UInt8               tos;
    UInt16              len;
    UInt16              id;
    struct ETH_OFFLOAD_Cpsw_Ipv4Frag frag;
    UInt8               ttl;
    UInt8               proto;
    UInt16              check;
    UInt8               saddr[4];
    UInt8               daddr[4];
};

/* IPV6 header */
struct ETH_OFFLOAD_Cpsw_Ipv6 {
    UInt32              version : 4,
                    traffic_class : 8,
                    flow_label : 20;
    UInt16              length;
    UInt8               next_header;
    UInt8               hop_limit;
        UInt8               saddr[16];
        UInt8               daddr[16];
};

/* UDP header */
struct ETH_OFFLOAD_Cpsw_Udp {
    UInt16              srcport;
    UInt16              destport;
    UInt16              len;
    UInt16              csum;
};

/* TCP header */
struct ETH_OFFLOAD_Cpsw_Tcp  {
    UInt16              source;
    UInt16              dest;
    UInt32              seq;
    UInt32              ack_seq;
    UInt8               res1:4,
                    doff:4;
    UInt8               fin:1,
                    syn:1,
                    rst:1,
                    psh:1,
                    ack:1,
                    urg:1,
                    ece:1,
                    cwr:1;
    UInt16              window;
    UInt16              csum;
    UInt16              urg_ptr;
};

struct ETH_OFFLOAD_Cpsw_layerInfo {
    Void (*handler)         (struct ETH_OFFLOAD_params *params,
                        struct ETH_OFFLOAD_Cpsw_pktInfo *pkt);
    UInt16              offset;
    UInt16              size;
};


struct ETH_OFFLOAD_params {
    UInt32              status;
    UInt32              flag;
    UInt32              seq;
    UInt32              total;
    Int32               csum;
    struct ETH_OFFLOAD_Cpsw_layerInfo
                    layer[ETH_OFFLOAD_EMAC_NUMLAYERS];
    struct ETH_OFFLOAD_Cpsw_Ipv4Frag frag_off;
    struct ETH_OFFLOAD_inter    *pin;
};

struct ETH_OFFLOAD_Cpsw_pktInfo {
    UInt32              nfrags;
    UInt32              token;
    UInt32              tot_len;
    Int32               csum;
};

struct ETH_OFFLOAD_hashEntry {
    UInt32              hash;
    struct ETH_OFFLOAD_desc     *desc;
};


/*
 * txQ:pointer to ETH_OFFLOAD_queue
 * rxQ:pointer to Eth_OFFLOAD_queue
 */
struct ETH_OFFLOAD_inter {
    UInt32              offloadFlag;
    UInt32              MTU;
    UInt32              eventId;
    Handle              hEMAC;
    Task_Handle         hWorker;
    Semaphore_Handle        eventSem;
    struct ETH_OFFLOAD_offloadStats offloadStats;
    struct ETH_OFFLOAD_queue    *txQ;
    struct ETH_OFFLOAD_queue    *rxQ;
    struct ETH_OFFLOAD_hashEntry    lroTable[ETH_OFFLOAD_LRO_TABLE_SIZE];
    UInt32              pktHeaderCnt;
    UInt8               pktHeader
                    [ETH_OFFLOAD_NUM_HEADERS]
                    [ETH_OFFLOAD_MAX_HEADER_SIZE];
    UInt16          csumUnalignBuffer[1024];
    int             isInit;
    UInt32          coal_intvl; // in usecs;
    Int32           semPendTimeout;
};

/* Switch Configuration structure       */
Cpsw3gInitConfig            cpsw3gCfg = {0};

/* flag that indicates whether we use SMA connectors(no-PHY) mode or
 * interface the SERDES to PHYs. APP must set this using driver
 * callback before NDK/Driver Initialization
 */
static      Uint32          use_sma_port0 = 0u,use_sma_port1 =0u;

static struct ETH_OFFLOAD_inter inter;
static void ETH_OFFLOAD_emacRxPacket(Handle hApp, UInt32 token, UInt32 flagLen);


#pragma DATA_SECTION(gETH_OFFLOAD_shm, ".eth_offload_shm")
ETH_OFFLOAD_shm gETH_OFFLOAD_shm;

Int32 ETH_OFFLOAD_Q_Poll(struct ETH_OFFLOAD_queue *Q, UInt32 weight);

/*
 * ETH_OFFLOAD_emacFreePacket: This function sends finished Tx packets to host
 * @hApp: Pointer to App handle
 * @pPkt: Paccket to be freed
 * @return: None
 */
static void ETH_OFFLOAD_emacFreePacket(Handle hApp, UInt32 token, UInt32 flagLen)
{
    struct ETH_OFFLOAD_inter *pin =
            (struct ETH_OFFLOAD_inter *)hApp;
    struct ETH_OFFLOAD_desc *desc =
            (struct ETH_OFFLOAD_desc *)token;

    if (desc) {
        /* Send the packet to host for freeing */
        ETH_OFFLOAD_Q_Enqueue(pin->txQ, desc);
    }
}

/*
 * ETH_OFFLOAD_emacStatus: Callback for fatal errors
 * @hApp: Pointer to App handle
 * @return: None
 */
static void ETH_OFFLOAD_emacStatus(Handle hApp, UInt32 macStatus)
{
    Vps_rprintf(" [ETH_OFFLOAD] Fatal Error : MACSTATUS = 0x%x !!!\n", macStatus);
}

/*
 * ETH_OFFLOAD_emacStats: Callback for stats update
 * @hApp: Pointer to App handle
 * @return: None
 */
static void ETH_OFFLOAD_emacStats(Handle hApp)
{

}

static int ETH_OFFLOAD_emacSetup(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    struct ETH_OFFLOAD_inter *pin = &inter;
    struct ETH_OFFLOAD_setup *devsetup = (struct ETH_OFFLOAD_setup*)
                        rpcMsg->rpcArg;

    pin->offloadFlag = devsetup->offloadFlag;

    return 1;
}

static int ETH_OFFLOAD_setCoalesce (struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    struct ETH_OFFLOAD_inter *pin = &inter;
    struct ETH_OFFLOAD_coalesce *coal = (struct ETH_OFFLOAD_coalesce *)
                        rpcMsg->rpcArg;

    if(coal->rx_coalesce_usecs>2000)
        coal->rx_coalesce_usecs = 2000;

    coal->coal_intvl = EMAC_set_coalesce(coal->rx_coalesce_usecs, coal->bus_freq_mhz);

    pin->coal_intvl = coal->coal_intvl;

    return 0;
}

/**
 *  @b Setup_EmacInitConfig
 *  @n
 *     Populate the configuration structures with
 *     the switch's default initialization parameters.
 *
 *  @param[in]
 *      void
 *
 *  @retval
 *      void
 */
static void Setup_EmacInitConfig(void)
{


    Cpsw3gInitConfig    *iCfg = NULL;
    Int32               i; // j;
// 	EMAC_AddrConfig     *addrCfg;

    /* Allocate Switch configuration structure */
    memset(&cpsw3gCfg,sizeof(Cpsw3gInitConfig), 0);

    iCfg = &cpsw3gCfg;

    /* Configure the Number of Rx, Tx channels */
    iCfg->dmaInitCfg.numChannels[NET_CH_DIR_TX] = CPSW3G_INITPARAM_TXNUMCHAN;
    iCfg->dmaInitCfg.numChannels[NET_CH_DIR_RX] = CPSW3G_INITPARAM_RXNUMCHAN;

    /* Initialize Tx,Rx channels with number of BDs to be allocated for
     * each of them.
     */
    for(i=0; i<iCfg->dmaInitCfg.numChannels[NET_CH_DIR_TX]; i++)
    {
        Cpsw3gChInfo *txChCfg = &cpsw3gCfg.chInfo[NET_CH_DIR_TX][i];

        txChCfg->chNum = i;
        txChCfg->chDir = NET_CH_DIR_TX;
        txChCfg->numBD = CPSW3G_INITPARAM_TXNUMBDS;
    }
    for(i=0; i<iCfg->dmaInitCfg.numChannels[NET_CH_DIR_RX]; i++)
    {
        Cpsw3gChInfo *rxChCfg = &cpsw3gCfg.chInfo[NET_CH_DIR_RX][i];

        rxChCfg->chNum = i;
        rxChCfg->chDir = NET_CH_DIR_RX;
        /* Configure the Buffer size for plain ethernet packets. VLANs disabled by
         * default. If using VLANs make sure to add in 4 bytes to the buffer size
         * below to accomodate VLAN header.
         */
        rxChCfg->bufSize = 1522;//1522=1500+14+4+4
        rxChCfg->numBD = CPSW3G_INITPARAM_RXNUMBDS;
    }

    /* parse the default speed/duplex params*/
    if(0u == use_sma_port0)
    {
	    /* use auto-neg */
        iCfg->macInitCfg[0].mdioModeFlags |= MDIO_MODEFLG_AUTONEG;
    }
    else
    {
	    /* Always linked state*/
	    iCfg->macInitCfg[0].mdioModeFlags |= MDIO_MODEFLG_NOPHY;
    }
    if(0u == use_sma_port1)
    {
	    /* use auto-neg */
	    iCfg->macInitCfg[1].mdioModeFlags |= MDIO_MODEFLG_AUTONEG;
    }
    else
    {
	    /* Always linked state */
	    iCfg->macInitCfg[1].mdioModeFlags |= MDIO_MODEFLG_NOPHY;
    }

    /* Enable this to turn on VLAN support in the EMAC */
    //iCfg->cpswCfg.cpswCtlModeFlags |= EMAC_CONFIG_CPSW_ENVLANAWARE;

    /* Program default MAC SL config */
    for (i=0; i < CPSW3G_NUM_MAC_PORTS; i++)
    {
	    if(0 == i)
	    {
		if(0 == (DEVICEID_DEVREV & DEVICEID_DEVREV_MASK))
			{
				//program phy_mask for port 1
				iCfg->macInitCfg[i].PhyMask = 0x2;//corresponding to Addr=1
				iCfg->macInitCfg[i].MLinkMask = 0x0;
			}
			else
			{
				//program phy_mask for port 1
				iCfg->macInitCfg[i].PhyMask = 0x1;//corresponding to Addr=1
				iCfg->macInitCfg[i].MLinkMask = 0x0;
			}
	    }
	    else
	    {

			/* program phy_mask for port 2 */
		    #ifdef USE_PHY1
				iCfg->macInitCfg[i].PhyMask = 0x2;/*corresponding to address 2 */
				iCfg->macInitCfg[i].MLinkMask = 0x1;
			#else
				iCfg->macInitCfg[i].PhyMask = 0x4;/*corresponding to address 2 */
				iCfg->macInitCfg[i].MLinkMask = 0x0;
			#endif


	    }/* end of else block covering settings for port 1 */

        iCfg->macInitCfg[i].portPri = CPSW3G_DEFAULT_MAC_PORTPRI;
        iCfg->macInitCfg[i].portCfi = CPSW3G_DEFAULT_MAC_PORTCFI;
        iCfg->macInitCfg[i].portVID = CPSW3G_DEFAULT_MAC_PORTVID;

        iCfg->macInitCfg[i].macModeFlags |= //EMAC_CONFIG_MODEFLG_RXCRC
                                            // EMAC_CONFIG_MODEFLG_EXTEN /*Centaurus:-NA-*/
                                             EMAC_CONFIG_MODEFLG_IFCTLA
                                           | EMAC_CONFIG_MODEFLG_GIGABIT    /* also enables GMII */
                                           | EMAC_CONFIG_MODEFLG_FULLDUPLEX
                                           | EMAC_CONFIG_MODEFLG_TXFLOWCNTL
                                           | EMAC_CONFIG_MODEFLG_RXBUFFERFLOWCNTL;

    }/* end of for loop over CPSW3G_NUM_MAC_PORTS */

    /* Program Default Dma COnfig */
    iCfg->dmaInitCfg.portPri = CPSW3G_DEFAULT_DMA_PORTPRI;
    iCfg->dmaInitCfg.portCfi = CPSW3G_DEFAULT_DMA_PORTCFI;
    iCfg->dmaInitCfg.portVID = CPSW3G_DEFAULT_DMA_PORTVID;
    iCfg->dmaInitCfg.rxBufferOffset = CPSW3G_DEFAULT_RXBUFOFF;
    iCfg->dmaInitCfg.hostRxCh_gmac0Pri0 = CPSW3G_DEFAULT_SL0_PRI0;
    iCfg->dmaInitCfg.hostRxCh_gmac0Pri1 = CPSW3G_DEFAULT_SL0_PRI1;
    iCfg->dmaInitCfg.hostRxCh_gmac0Pri2 = CPSW3G_DEFAULT_SL0_PRI2;
    iCfg->dmaInitCfg.hostRxCh_gmac0Pri3 = CPSW3G_DEFAULT_SL0_PRI3;
    iCfg->dmaInitCfg.hostRxCh_gmac1Pri0 = CPSW3G_DEFAULT_SL1_PRI0;
    iCfg->dmaInitCfg.hostRxCh_gmac1Pri1 = CPSW3G_DEFAULT_SL1_PRI1;
    iCfg->dmaInitCfg.hostRxCh_gmac1Pri2 = CPSW3G_DEFAULT_SL1_PRI2;
    iCfg->dmaInitCfg.hostRxCh_gmac1Pri3 = CPSW3G_DEFAULT_SL1_PRI3;
    iCfg->dmaInitCfg.dmaModeFlags |=  EMAC_CONFIG_MODEFLG_CHPRIORITY;

    /* Program Default ALE Configuration */
    iCfg->aleCfg.aleModeFlags |= EMAC_CONFIG_ALE_ENABLE
                                 | EMAC_CONFIG_ALE_CLRTABLE;
    /* Enable this to turn on VLAN support in the EMAC */
                                //| EMAC_CONFIG_ALE_ENVLANAWARE;
    iCfg->aleCfg.unknownForceUntaggedEgress =  CPSW3G_DEFAULT_UNK_FORCE_UNTAG_EGR;
    iCfg->aleCfg.unknownRegMcastFloodMask = CPSW3G_DEFAULT_UNK_REGMCAST_FLOODMASK;
    iCfg->aleCfg.unknownMcastFloodMask = CPSW3G_DEFAULT_UNK_MCAST_FLOODMASK;
    iCfg->aleCfg.unknownVlanMemberList = CPSW3G_DEFAULT_UNK_VLANMEMLIST;

    /** CHECK: Set this to correct value.Set Ratelimit counter for 1msec*/
    iCfg->aleCfg.alePrescale =  CPSW3G_DEFAULT_ALEPRESCALE;

    /* Ale Port Config */
    for(i=0; i < CPSW3G_NUM_PORTS; i++)
    {
        iCfg->alePortCfg[i].bcastLimit = CPSW3G_DEFAULT_BCASTLIMIT;
        iCfg->alePortCfg[i].mcastLimit = CPSW3G_DEFAULT_MCASTLIMIT;
        iCfg->alePortCfg[i].noLearn =  CPSW3G_DEFAULT_LEARN;
        iCfg->alePortCfg[i].portState = CPSW3G_DEFAULT_PORTSTATE;
        iCfg->alePortCfg[i].vidIngressCheck = CPSW3G_DEFAULT_VIDINGRESSCHECK;
        iCfg->alePortCfg[i].dropUntagged = CPSW3G_DEFAULT_DROPUNTAGGED;
    }

    /*CPSW3G default config*/
#if 0 //not in cpsw_control at centaurus
    //iCfg->cpswCfg.cpswCtlModeFlags |= EMAC_CONFIG_CPSW_P2P2RXFLOWEN
                                   // | EMAC_CONFIG_CPSW_P2P1TXFLOWEN
                                   // | EMAC_CONFIG_CPSW_P2P0TXFLOWEN
                                   // | EMAC_CONFIG_CPSW_P1P2TXFLOWEN
                                   // | EMAC_CONFIG_CPSW_P0P2TXFLOWEN;
#endif
    iCfg->cpswCfg.cpswPtypeModeFlags = 0;
    iCfg->cpswCfg.escPriLdVal = CPSW3G_DEFAULT_ESCPRI_LD_VAL;

    iCfg->cpswCfg.cpswStatModeFlags |= EMAC_CONFIG_STAT_P2STATEN
                                    | EMAC_CONFIG_STAT_P1STATEN
                                    | EMAC_CONFIG_STAT_P0STATEN;

    iCfg->cpswCfg.mdioTickMSec = CPSW3G_DEFAULT_MDIOTICK;
    iCfg->cpswCfg.mdioBusFrequency = CPSW3G_DEFAULT_MDIOBUSFREQ;
    iCfg->cpswCfg.mdioClockFrequency = CPSW3G_DEFAULT_MDIOCLOCKFREQ;
    iCfg->cpswCfg.cpmacBusFrequency = CPSW3G_DEFAULT_CPMACBUSFREQ ;
    iCfg->Mib64CntMsec = CPSW3G_MIB_TIMER_TIMEOUT;

	/* We count in 100ms Ticks */
	cpsw3gCfg.aleTicks = (ALE_AGE_OUT_TIME / 100);
	cpsw3gCfg.aleTimerActive        = 0;

	cpsw3gCfg.CoreNum               = 0;

    /* Add 4 bytes for VLAN header if configuring for VLANs */
	cpsw3gCfg.PktMTU                = 1518; //rxcrc is present
	cpsw3gCfg.DescBase              = EMAC_DESC_BASE_CPPI;
    /* Configure the callback functions */
    /* Free packet call back */
    cpsw3gCfg.pfcbFreePacket        = ETH_OFFLOAD_emacFreePacket;
    /* Receive packet call back */
    cpsw3gCfg.pfcbRxPacket          = ETH_OFFLOAD_emacRxPacket;
    /* Status update call back */
    cpsw3gCfg.pfcbStatus            = ETH_OFFLOAD_emacStatus;
    /* Statistics update call back */
    cpsw3gCfg.pfcbStatistics        = ETH_OFFLOAD_emacStats;
	#ifdef USE_PHY1
		cpsw3gCfg.TotalNumOfMacAddrs     = 2;
	#else
		cpsw3gCfg.TotalNumOfMacAddrs     = 1;
    #endif

	cpsw3gCfg.RxFilter                   = EMAC_RXFILTER_NOTHING;


    return;
}

/*
 * ETH_OFFLOAD_emacOpen: function open emac open,set freepool for transimt and receive
 * @rpcMsg: Pointer to ETH_OFFLOAD_RpcMsg structure
 * @return: 0 (no error)
 */
static int ETH_OFFLOAD_emacOpen(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
//     EMAC_AddrConfig *addrCfg;
//     EMAC_AddrConfig *addrs[1];
//     EMAC_AddrConfig addr;
    struct ETH_OFFLOAD_inter *pin = &inter;
    struct ETH_OFFLOAD_open *devopen = (struct ETH_OFFLOAD_open*)
                        rpcMsg->rpcArg;
	Uint8 j;

	Setup_EmacInitConfig();

    /* Packet size */
    cpsw3gCfg.PktMTU   = pin->MTU = devopen->mtu;
    /* Rx packet pool */
    cpsw3gCfg.RxMaxPktPool   = CPSW3G_INITPARAM_RXNUMBDS;
    /* Total number of MAC addresses for all receive channels assigned */
    cpsw3gCfg.TotalNumOfMacAddrs = devopen->nmacaddrs;

// 	/* Allocate mem for and configure MAC address */
// 	cpsw3gCfg.MacAddr = (EMAC_AddrConfig **)
//      		       malloc(cpsw3gCfg.TotalNumOfMacAddrs * sizeof(EMAC_AddrConfig *));


// 	for ( j=0; j<cpsw3gCfg.TotalNumOfMacAddrs; j++ )
//     {
// 		cpsw3gCfg.MacAddr[j] = (EMAC_AddrConfig *)malloc(sizeof(EMAC_AddrConfig));
// 	}

    for( j=0; (Uint8)j<(cpsw3gCfg.TotalNumOfMacAddrs); j++ )
    {
		cpsw3gCfg.MacAddr[j].Addr[0]    = devopen->macaddrs[j][0];
		cpsw3gCfg.MacAddr[j].Addr[1]    = devopen->macaddrs[j][1];
		cpsw3gCfg.MacAddr[j].Addr[2]    = devopen->macaddrs[j][2];
		cpsw3gCfg.MacAddr[j].Addr[3]    = devopen->macaddrs[j][3];
		cpsw3gCfg.MacAddr[j].Addr[4]    = devopen->macaddrs[j][4];
		cpsw3gCfg.MacAddr[j].Addr[5]    = devopen->macaddrs[j][5];
		cpsw3gCfg.MacAddr[j].ChannelNum = cpsw3gCfg.CoreNum;
	}


    /* channel usage must be mutual exclusive among cores */
    cpsw3gCfg.chInfo[NET_CH_DIR_TX][0].TxChanEnable = 1;  // 00000001, channel 0

    cpsw3gCfg.chInfo[NET_CH_DIR_RX][0].RxChanEnable = 1;  // 00000001, channel 0

    if(0 != EMAC_open(devopen->phyid, (Handle)pin, &cpsw3gCfg, &pin->hEMAC)){
        Vps_rprintf ("ERROR: EMAC_open Failed\n");
        return -1;
    }

    pin->semPendTimeout = 0;
    pin->isInit = TRUE;

    Vps_rprintf(" [ETH_OFFLOAD] OPENED !!!\n");

    return 0;
}

/*
 * ETH_OFFLOAD_emacStart: function to start the emac driver
 * @rpcMsg: Pointer to ETH_OFFLOAD_RpcMsg structure
 * @return: 0 (no error)
 */
static int ETH_OFFLOAD_emacStart(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    struct ETH_OFFLOAD_inter *pin = &inter;
    Int32 status = EMAC_start(pin->hEMAC);

    if (0 != EMAC_setReceiveFilter(pin->hEMAC, EMAC_RXFILTER_ALL, 0)) {
        Vps_rprintf ("ERROR: EMAC_setReceiveFilter Failed \n");
        return -1;
    }

    return status;
}

static int ETH_OFFLOAD_getStatistics(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    UInt32 status = 0;
    EMAC_Statistics stats = {0};
    struct ETH_OFFLOAD_inter *pin = &inter;
    struct ETH_OFFLOAD_stats *ethoffload_stats =(void *)rpcMsg->rpcArg;

    status = EMAC_getStatistics(pin->hEMAC, &stats);
    if (status != 0) {
        return -1;
    }

    ethoffload_stats->multicast = (UInt32) (stats.RxMCastFrames);
    ethoffload_stats->collisions    = (UInt32) (stats.TxCollision
                    + stats.TxSingleColl
                        + stats.TxMultiColl);
    ethoffload_stats->rx_length_errors = (UInt32) (stats. RxOversized
                    + stats.RxJabber
                    + stats.RxUndersized);
    ethoffload_stats->rx_over_errors = (UInt32)(stats.RxSOFOverruns
                    + stats.RxMOFOverruns);
    ethoffload_stats->rx_fifo_errors = (UInt32)(stats.RxDMAOverruns);
    ethoffload_stats->tx_carrier_errors = (UInt32)(stats.TxCarrierSLoss);
    ethoffload_stats->tx_fifo_errors = (UInt32)(stats.TxUnderrun);

    return 1;
}

/*
 * ETH_OFFLOAD_emacClose: function to close the emac driver
 * @rpcMsg: Pointer to ETH_OFFLOAD_RpcMsg structure
 * @return: 0 (no error)
 */
static int ETH_OFFLOAD_emacClose(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    struct ETH_OFFLOAD_inter *pin = &inter;
    pin->offloadFlag = 0;
    if(EMAC_close(pin->hEMAC)) {
        Vps_rprintf("Error in emac close !!!!!\n");
        return -1;
    }

    ETH_OFFLOAD_Q_Reset(pin->txQ);
    ETH_OFFLOAD_Q_Reset(pin->rxQ);

    pin->isInit = FALSE;
    pin->semPendTimeout = 10; /* dont wake from pend continously, instead timeout after 10ms */

    Vps_rprintf(" [ETH_OFFLOAD] CLOSED !!!\n");
    return 0;
}

/*
 * ETH_OFFLOAD_Cpsw_RpcHandler: function open the rpc's
 * @rpcMsg: Pointer to ETH_OFFLOAD_RpcMsg structure
 * @return: status value (which int type)
 */
static Int32 ETH_OFFLOAD_Cpsw_RpcHandler(struct ETH_OFFLOAD_RpcMsg *rpcMsg)
{
    int status = 1;

    switch(rpcMsg->rpcFuncId)
    {
        case ETH_OFFLOAD_EMAC_GET_STATISTICS:
            status = ETH_OFFLOAD_getStatistics(rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_EMAC_SETUP:
            status = ETH_OFFLOAD_emacSetup(rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_OPEN:
            status = ETH_OFFLOAD_emacOpen(rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_START:
            status = ETH_OFFLOAD_emacStart(inter.hEMAC);
            break;
        case ETH_OFFLOAD_FUNCID_CLOSE:
            status = ETH_OFFLOAD_emacClose(rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_COALESCE:
            status = ETH_OFFLOAD_setCoalesce(rpcMsg);
            break;
        case ETH_OFFLOAD_FUNCID_UPDATE:
            status = EMAC_update((struct EMAC_updateData *) rpcMsg->rpcArg);
            break;
        default:
            status = -1;
    }
    return status;
}

/*
 * ETH_OFFLOAD_Cpsw_csum: Function to do check sum
 * @addr: address of short type
 * @len: length
 * @return: answer of integer type
 */
static UInt32 ETH_OFFLOAD_Cpsw_csum(struct ETH_OFFLOAD_inter *pin, volatile UInt16 *addr, UInt32 numbytes)
{
    register UInt32 sum = 0;
    volatile UInt16 *w = addr;
    register UInt16 last = 0;

    /* Align to 16-bit address */
    addr = (volatile UInt16 *)(((UInt32)addr)&(~0x1));

    if (w != addr) {
        memcpy((Ptr)pin->csumUnalignBuffer, (Ptr)w, numbytes);
        w = (UInt16*)pin->csumUnalignBuffer;
    }

    /*
     * Algorithm is simple, using a 32 bit accumulator (sum), we add
     * sequential 16 bit words to it, and at the end, fold back all the
     * carry bits from the top 16 bits into the lower 16 bits.
     */
    while (numbytes > 1)  {
        sum += htons(*w);
        w++;
        numbytes -= 2;
    }

    /* mop up an odd byte, if necessary */
    if (numbytes == 1) {
        last = htons(*w);
        sum += last&0xFF00;
    }

    return sum;
}

static UInt16 ETH_OFFLOAD_Cpsw_csumFold(UInt32 sum)
{
    UInt16 answer = 0;

    /* add back carry outs from top 16 bits to low 16 bits */
    sum = (sum >> 16) + (sum & 0xffff); /* add hi 16 to low 16 */
    sum += (sum >> 16);         /* add carry */
    answer = ~sum;              /* truncate to 16 bits */
    return htons(answer);
}

static UInt32 ETH_OFFLOAD_Cpsw_csumHeader(struct ETH_OFFLOAD_inter *pin,
         struct ETH_OFFLOAD_desc *desc, UInt32 *ipload, UInt16 **pcsum)
{
    Int32 type = 0, proto = 0, header = 0;
    UInt8 *start = (UInt8 *)desc->dma_address;
    struct ETH_OFFLOAD_Cpsw_Ipv4 *ipv4 = 0;

    type = ((struct ETH_OFFLOAD_Cpsw_Eth1 *)start)->type;
    if (type == ETH_OFFLOAD_ETHTYPE_ETH_8021q) {
        header += sizeof(struct ETH_OFFLOAD_Cpsw_Eth2);
    } else {
        header += sizeof(struct ETH_OFFLOAD_Cpsw_Eth1);
    }

    if (type == ETH_OFFLOAD_ETHTYPE_ETH_IPV4) {
        ipv4 = (struct ETH_OFFLOAD_Cpsw_Ipv4 *)(start + header);
        proto = ipv4->proto;
        header += sizeof(struct ETH_OFFLOAD_Cpsw_Ipv4);
    } else if (0) { //type == ETH_OFFLOAD_ETHTYPE_ETH_IPV6) {
        proto = ((struct ETH_OFFLOAD_Cpsw_Ipv6 *)(start + header))->next_header;
        header += sizeof(struct ETH_OFFLOAD_Cpsw_Ipv6);
    } else {
        return 0;
    }

    *ipload = header;

    if (proto == ETH_OFFLOAD_NET_IPUDP) {
        *pcsum = &((struct ETH_OFFLOAD_Cpsw_Udp *)(start + header))->csum;
    } else if (proto == ETH_OFFLOAD_NET_IPTCP) {
        *pcsum = &((struct ETH_OFFLOAD_Cpsw_Tcp *)(start + header))->csum;
    } else {
        return 0;
    }

    if (ipv4) {
        return  (htons(ipv4->len) - (ipv4->ihl<<2))
            + (proto&0xFF)
            + ETH_OFFLOAD_Cpsw_csum(pin,
                (UInt16*)ipv4->saddr, 2*sizeof(ipv4->saddr));
    }
    return 0;
}

static Int32 ETH_OFFLOAD_Cpsw_rxOffloadHandler(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_desc *desc)
{
    UInt32 csum = 0, ipload = 0;
    UInt16 *pcsum = 0;

    Cache_inv(  (Ptr)ETH_OFFLOAD_FLOOR(desc->dma_address, ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES  ),
                     ETH_OFFLOAD_ALIGN(desc->length+ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES, ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES),
                Cache_Type_ALL, TRUE
        );

    csum = ETH_OFFLOAD_Cpsw_csumHeader(pin, desc, &ipload, &pcsum);

    /* Compute checksum of data */
    if (pcsum) {
        csum += ETH_OFFLOAD_Cpsw_csum(pin,
                    (UInt16 *)(desc->dma_address + ipload),
                desc->length - ipload);
        if (ETH_OFFLOAD_Cpsw_csumFold(csum)) {
            /* Checksum has failed, so drop the packet */
            #if 0
            Vps_rprintf(" [ETH_OFFLOAD] %d: Checksum error (addr=%08x, len=%d, ip payload offset=%d) !!!\n",
                Utils_getCurTimeInMsec(),
                desc->dma_address,
                desc->length,
                ipload
            );
            #endif
            return 0;
        }
    }

    /* Accept the packet */
    return 1;
}

/*
 * ETH_OFFLOAD_emacRxPacket: This function sends rx packet to host
 * @hApp: Pointer to App handle
 * @pPkt: Paccket to be freed
 * @return: None
 */
static void ETH_OFFLOAD_emacRxPacket(Handle hApp, UInt32 token, UInt32 flagLen)
{
    struct ETH_OFFLOAD_inter *pin =
            (struct ETH_OFFLOAD_inter *)hApp;
    struct ETH_OFFLOAD_desc *desc =
            (struct ETH_OFFLOAD_desc *)token;
    int csumOk;

    if (desc == NULL) {
        return;
    }

    /* extract values */
    desc->length = flagLen&0xFFFF;

    desc->status = 0;

    /* do offloading work for rx path */
    if(pin->offloadFlag & ETH_OFFLOAD_FLAG_RX_CSUM)
    {
        csumOk = ETH_OFFLOAD_Cpsw_rxOffloadHandler(pin, desc);

        if(!csumOk)
        {
            /* Dropping the packet so re-queue */
            desc->status = 0x3;
        }
    }

    /* Push received packet to host */
    ETH_OFFLOAD_Q_Enqueue(pin->rxQ, desc);
}


/*
 * ETH_OFFLOAD_Cpsw_rxHandler: Funtion receive handler which call Emac loopback function
 * @return: 1 on success
 */
static Int32 ETH_OFFLOAD_Cpsw_rxHandler(void *hApp, struct ETH_OFFLOAD_desc *desc)
{
    struct ETH_OFFLOAD_inter *pin = (struct ETH_OFFLOAD_inter *)hApp;

    if (EMAC_enqRxDesc(pin->hEMAC, (UInt8 *)desc->dma_address,
            (UInt32)desc)) {
        return 1;
    }

    /* Oops failed to queue te desc */
    return 0;
}



Int32 ETH_OFFLOAD_Cpsw_txOffloadHandlerCsum(struct ETH_OFFLOAD_inter *pin, struct ETH_OFFLOAD_desc *desc)
{
    UInt32 csum = 0, ipload = 0;
    volatile UInt16 *pcsum = 0;

    Cache_inv(  (Ptr)ETH_OFFLOAD_FLOOR(desc->dma_address, ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES  ),
                     ETH_OFFLOAD_ALIGN(desc->length+ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES, ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES),
                Cache_Type_ALL, TRUE
        );

    csum = ETH_OFFLOAD_Cpsw_csumHeader(pin, desc, &ipload, (UInt16 **)&pcsum);

    if (pcsum) {

        *pcsum = 0;

        csum += ETH_OFFLOAD_Cpsw_csum(pin,
                (UInt16 *)(desc->dma_address + ipload),
                desc->length - ipload);

        *pcsum = ETH_OFFLOAD_Cpsw_csumFold(csum);

        Cache_wbInv((Ptr)ETH_OFFLOAD_FLOOR((UInt32)pcsum, ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES  ),
                     ETH_OFFLOAD_CACHE_LINE_ALIGN_BYTES,
                Cache_Type_ALL, TRUE
        );
    }
    return 0;
}

/*
 * ETH_OFFLOAD_Cpsw_txHandler: Function transmit handler which call Enqueue function
 * @return: 1 on success
 */
static Int32 ETH_OFFLOAD_Cpsw_txHandler(void *hApp, struct ETH_OFFLOAD_desc *data)
{
    struct ETH_OFFLOAD_inter *pin = (struct ETH_OFFLOAD_inter *)hApp;
    struct ETH_OFFLOAD_desc *desc = (struct ETH_OFFLOAD_desc *)data;
    Int32 status;

    if (EMAC_txFreeCount(pin->hEMAC) < 1 ) {
        /* Not enough hw desc available, try next time */
        return 0;
    } else if (desc->length < ETH_OFFLOAD_ETH_MINFRAMESIZE) {
        /* < 60 bytes (ARP, ...); make is min size */
        desc->length = ETH_OFFLOAD_ETH_MINFRAMESIZE;
    }

    if (pin->offloadFlag & ETH_OFFLOAD_FLAG_TX_CSUM)
    {
        ETH_OFFLOAD_Cpsw_txOffloadHandlerCsum(pin, desc);
    }
    {
        status = EMAC_enqTxDesc(pin->hEMAC,
                (Uint8 *)desc->dma_address,
                desc->length,
                    (desc->length << 16) | 0x3,
                (UInt32)desc);
    }

    return status;
}

static Void ETH_OFFLOAD_Cpsw_worker(UArg arg0, UArg arg1)
{
    UInt32 key = 0;
    struct ETH_OFFLOAD_inter *pin = &inter;

    while (1) {

       /* Block the Event Handling Task */
       Semaphore_pend(pin->eventSem, pin->semPendTimeout);

        if(!pin->isInit)
            continue;

        key = Hwi_disable();

        ETH_OFFLOAD_EMAC_Poll(pin->hEMAC);

        ETH_OFFLOAD_Q_Poll(pin->rxQ, ETH_OFFLOAD_Q_WEIGHT);
        ETH_OFFLOAD_Q_Poll(pin->txQ, ETH_OFFLOAD_Q_WEIGHT);


        Hwi_restore(key);
    }
}

/*
 * ETH_OFFLOAD_TaskInit: Function to init task structures
 * @return: 1 if success, else 0
 */
static Int32 ETH_OFFLOAD_Cpsw_TaskInit (struct ETH_OFFLOAD_inter *pin, UInt8 *stack, UInt32 stackSize)
{
    Task_Params tskParams;
    Semaphore_Params semParams;

    /* create semaphore to manage critical sections of task data */
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
//     pin->eventSem = Semaphore_create(1u, &semParams, NULL);
    pin->eventSem = Semaphore_create(0u, &semParams, NULL);

    pin->isInit = FALSE;
    pin->semPendTimeout = 10;

    /* No point continuing with out task */
    UTILS_assert(pin->eventSem != NULL);

    /* Create task to handle the packet processing task */
    Task_Params_init(&tskParams);
    tskParams.arg0 = (UArg)pin;
    tskParams.stack = stack;
    tskParams.stackSize = stackSize;
    tskParams.priority = 1; /* keep this to lowest priority, since this is polling continously */
    pin->hWorker = Task_create(ETH_OFFLOAD_Cpsw_worker, &tskParams, NULL);
    UTILS_assert(pin->hWorker != NULL);

    return 0;
}

/*
 * ETH_OFFLOAD_QInit: Function to init task structures
 * @return: 1 if success, else 0
 */
static Void ETH_OFFLOAD_Cpsw_QInit (struct ETH_OFFLOAD_inter *pin)
{
    pin->txQ = ETH_OFFLOAD_Q_Create(ETH_OFFLOAD_SWDESC_MAX,
                        (QCALLBACK)ETH_OFFLOAD_Cpsw_txHandler,
                        (void *)pin, "TX",
                        &gETH_OFFLOAD_shm.txSwDesc[0]
                        );

    pin->rxQ = ETH_OFFLOAD_Q_Create(ETH_OFFLOAD_SWDESC_MAX,
                        (QCALLBACK)ETH_OFFLOAD_Cpsw_rxHandler,
                        (void *)pin, "RX",
                        &gETH_OFFLOAD_shm.rxSwDesc[0]
                        );
}

/*
 * ETH_OFFLOAD_Q_DeinitTask: Function to destroy task
 * @return: None
 */
static Void ETH_OFFLOAD_DeinitTask(Void)
{
    struct ETH_OFFLOAD_inter *pin = &inter;

    /* Remove the semaphore when unloading module */
    Semaphore_delete(&pin->eventSem);
    /* Kill the task here */
    Task_delete(&pin->hWorker);
}

/*
 * ETH_OFFLOAD_Cpsw_Init: It is Init fucntion for Emac for where transmit and receive ETH _OFFLOAD_Q are created
 * @return: 1 on success
 */
Int32 ETH_OFFLOAD_Cpsw_Init(Void)
{
    struct ETH_OFFLOAD_inter *pin = &inter;

    memset(pin, 0, sizeof(*pin));

    ETH_OFFLOAD_Rpc_RegisterHandler(ETH_OFFLOAD_Cpsw_RpcHandler);
    EMAC_init();

    ETH_OFFLOAD_Cpsw_QInit (pin);
    ETH_OFFLOAD_Cpsw_TaskInit (pin, gTskStack, sizeof(gTskStack));

    return 0;
}



/*
 * ETh_OFFLOAD_Cpsw_DeInit: function Deinit Emac driver
 * @return: 1 on success
 */
Int32 ETH_OFFLOAD_Cpsw_DeInit(Void)
{
    UInt32 status;
    struct ETH_OFFLOAD_inter *pin = &inter;

    /* Kill the Task */
    ETH_OFFLOAD_DeinitTask();

    EMAC_deinit();

    status = ETH_OFFLOAD_Q_Destroy(pin->txQ);
    if( status != 1) {
        Vps_rprintf("DSP:IN DEInit: ETH_OFFLOAD_Q_Destroy is failed!!!!!\n");
        return -1;
    }

    status = ETH_OFFLOAD_Q_Destroy(pin->rxQ);
    if( status != 1){
        Vps_rprintf("DSP:IN DEInit: ETH_OFFLOAD_Q_Destroy is failed!!!!!\n");
        return -1;
    }

    return 1;
}
