/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include <mcfw/src_bios6/utils/utils.h>

#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>

#include <mcfw/src_bios6/eth_offload/inc/eth_offload_rpc.h>
#include <mcfw/src_bios6/links_common/system/system_priv_common.h>

#define ETH_OFFLOAD_RPC_TSK_STACK_SIZE 32*1024
#define ETH_OFFLAOD_RPC_PAYLOAD_Q_MAX  128

#pragma DATA_ALIGN(grpc_tskStack, 32)
#pragma DATA_SECTION(grpc_tskStack, ".bss:taskStackSection")
UInt8 grpc_tskStack[ETH_OFFLOAD_RPC_TSK_STACK_SIZE];

typedef struct ETH_OFFLOAD_PAYLOADQ {
	UInt32                          qData[ETH_OFFLAOD_RPC_PAYLOAD_Q_MAX];
	UInt32                          qHead;
	UInt32                          qTail;
}RpcPayloadQ;

/*
 * ETH_OFFLOAD_RpcObj: To manage rpc functions
 * @rpcAckLock: Semaphore lock for ackment
 * @rpcHandler: rpc callback handler to handle rpc requests
 */
struct ETH_OFFLOAD_RpcObj {

	RpcHandler		                rpcHandler;
    Task_Handle                     hWorker;
	char                            rpcTskName[50];
    ti_sysbios_knl_Semaphore_Struct eventSemMem;
    ti_sysbios_knl_Semaphore_Handle eventSem;
	RpcPayloadQ                     payloadQ;
};

/*
 * Static variables
 */
static struct ETH_OFFLOAD_RpcObj RPC;


/********************************************************************************************/
/*                                    Private Functions                                     */
/********************************************************************************************/

/*
 * ETH_OFFLOAD_RPC_GetLocalPtr: Function to Get local ptr
 * @ptr: pointer to be converted
 * @return: address of type uint32
 */
static UInt32 ETH_OFFLOAD_RPC_GetLocalPtr (UInt32 ptr)
{
	return (UInt32)SharedRegion_getPtr(ptr);
}

/*
 * ETH_OFFLOAD_RPC_NotifyHandler: Callback to receive notification from host processor
 * @procId: Processor id of the host (ignored)
 * @lineId: interrupt line id (ignored)
 * @eventId: event number on which the event was raised (=10)
 * @arg: any arg (ignored)
 * @payload: additonal data (ignored)
 * @return: None
 */
static Void ETH_OFFLOAD_RPC_NotifyHandler(UInt16 procId, UInt16 lineId,
					UInt32 eventId, UArg arg, UInt32 payload)
{
	struct ETH_OFFLOAD_RpcObj *rpc = &RPC;

	/* Get the payload and offload remaining work to deferred task.
     * Store the payload in a queue so that payloads from the interrupts between Semaphore_post() and
     * the defferred task schedule are not lost
     */

	rpc->payloadQ.qData[rpc->payloadQ.qTail] = payload;
	rpc->payloadQ.qTail++;

	UTILS_assert(rpc->payloadQ.qTail <  ETH_OFFLAOD_RPC_PAYLOAD_Q_MAX);

	Semaphore_post(rpc->eventSem);

	return;
}


static Void ETH_OFFLOAD_Rpc_Worker(UArg arg0, UArg arg1)
{

    struct ETH_OFFLOAD_RpcObj *rpc = NULL;
	struct ETH_OFFLOAD_RpcMsg *pRpcMsg = NULL;
    volatile int status;

	rpc = &RPC;

    while (1) {

       /* Block the Event Handling Task */
       Semaphore_pend(rpc->eventSem, BIOS_WAIT_FOREVER);

		/* Process all RPC messages accumulated before/after Semaphore_post() in the callback
         * and before this task got scheduled
         */
		while(rpc->payloadQ.qHead < rpc->payloadQ.qTail)
		{
			pRpcMsg = (struct ETH_OFFLOAD_RpcMsg *)ETH_OFFLOAD_RPC_GetLocalPtr(rpc->payloadQ.qData[rpc->payloadQ.qHead]);

			if(!pRpcMsg) {
				Vps_rprintf(" [ETH_OFFLOAD] Recevied invalid RPC message from host (payload = 0x%x)\n", rpc->payloadQ.qData[rpc->payloadQ.qHead]);
			}
			else {

				/* Read data from messageq and get the funcid */
				pRpcMsg->rpcStatus = rpc->rpcHandler(pRpcMsg);

		        status = pRpcMsg->rpcStatus; /* read back value to confirm its written to memory */

				/* Send notification to host processor */
				Notify_sendEvent(ETH_OFFLOAD_PROCID_HOST,
						ETH_OFFLOAD_RPC_NOTIFY_LINEID,
						ETH_OFFLOAD_RPC_NOTIFY_EVENTID,
						rpc->payloadQ.qData[rpc->payloadQ.qHead],
						TRUE);

				/* To ensure valid payload check for next message */
				pRpcMsg = NULL;
			}
			rpc->payloadQ.qHead++;
		}
		/* Reset payload Q for next interrupt */
		rpc->payloadQ.qHead = rpc->payloadQ.qTail = 0;
	}
}


/********************************************************************************************/
/*                                    Public Functions                                      */
/********************************************************************************************/

/*
 * ETH_OFFLOAD_Rpc_Init: Function to Initiate Rpc
 * @return: +ve inetger
 */
Int32 ETH_OFFLOAD_Rpc_Init()
{
	struct ETH_OFFLOAD_RpcObj *rpc = &RPC;
    int status;

    Task_Params tskParams;
    ti_sysbios_knl_Semaphore_Params semPrms;

	memset(rpc, 0, sizeof(struct ETH_OFFLOAD_RpcObj));

    ti_sysbios_knl_Semaphore_Params_init(&semPrms);
    semPrms.mode = ti_sysbios_knl_Semaphore_Mode_BINARY;
    ti_sysbios_knl_Semaphore_construct(&rpc->eventSemMem,
                                       0,
                                       &semPrms);
    UTILS_assert(rpc->eventSem == NULL);
    rpc->eventSem = ti_sysbios_knl_Semaphore_handle(&rpc->eventSemMem);


    Task_Params_init(&tskParams);
    tskParams.arg0 = (UArg)rpc;
    tskParams.stack = grpc_tskStack;
    tskParams.stackSize = sizeof(grpc_tskStack);
    tskParams.priority = CPSW_RPC_WORKER_TSK_PRI;
	strcpy(rpc->rpcTskName,"ETH_OFFLOAD_RPC_WORKER");
	tskParams.instance->name = rpc->rpcTskName;
    rpc->hWorker = Task_create(ETH_OFFLOAD_Rpc_Worker, &tskParams, NULL);
    UTILS_assert(rpc->hWorker != NULL);

    Utils_prfLoadRegister(rpc->hWorker,
                          rpc->rpcTskName);

	memset(&(rpc->payloadQ), 0, sizeof(RpcPayloadQ));

	/* Register callback */
	status = Notify_registerEvent(ETH_OFFLOAD_PROCID_HOST,
			ETH_OFFLOAD_RPC_NOTIFY_LINEID,
			ETH_OFFLOAD_RPC_NOTIFY_EVENTID,
			ETH_OFFLOAD_RPC_NotifyHandler, NULL);
    if(status!=0)
    {
        Vps_rprintf(" [ETH_OFFLOAD] Notify register for RPC failed !!!\n");
    }

	return 0;
}

/*
 * ETH_OFFLOAD_Rpc_RegisterHandler: Function to Register Rpc  Handler
 * @handler: variable to RpcHandler for handling Rpc
 * @return: None
 */
Void ETH_OFFLOAD_Rpc_RegisterHandler(RpcHandler handler)
{
	struct ETH_OFFLOAD_RpcObj *rpc = &RPC;

	if (NULL == handler) {
		Vps_rprintf("Invalid handler passed\n");
		return;
	}
	rpc->rpcHandler = handler;
}

/*
 * ETH_OFFLOAD_Rpc_DeInit: Function to DeInit Rpc
 * @return: 1 if success
 */
Int32 ETH_OFFLOAD_Rpc_DeInit()
{
    int status;
	struct ETH_OFFLOAD_RpcObj *rpc = &RPC;


	status = Notify_unregisterEvent(ETH_OFFLOAD_PROCID_HOST,
			ETH_OFFLOAD_RPC_NOTIFY_LINEID,
			ETH_OFFLOAD_RPC_NOTIFY_EVENTID,
			ETH_OFFLOAD_RPC_NotifyHandler, NULL);

    ti_sysbios_knl_Semaphore_destruct(&rpc->eventSemMem);
    UTILS_assert(rpc->eventSem != NULL);
    rpc->eventSem = NULL;

    Utils_prfLoadUnRegister(rpc->hWorker);
	Task_delete(&rpc->hWorker);

    if(status!=0)
    {
        Vps_rprintf(" [ETH_OFFLOAD] Notify unregister for RPC failed !!!\n");
    }

	return 0;
}
