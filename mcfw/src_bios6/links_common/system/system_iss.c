/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include "system_priv_common.h"
#include <mcfw/src_bios6/utils/utils_common.h>
#include <ti/sysbios/knl/Task.h>

#include <mcfw/src_bios6/alg/simcop/inc/cpisCore.h>
#include <mcfw/src_bios6/alg/simcop/inc/iss_init.h>

#include <ti/sysbios/knl/Semaphore.h>

typedef struct
{
    CPIS_Init cpisInitPrm;
    Semaphore_Handle vcopLock;

} System_VcopObj;

System_VcopObj gSystem_VcopObj;

void System_vcopCacheWbInv(void *addr, Uint32 size, Bool wait)
{
    Cache_wbInv(addr, size, TRUE, NULL);
}

void System_lockVcop(void *arg)
{
    Semaphore_pend(gSystem_VcopObj.vcopLock,BIOS_WAIT_FOREVER);
}


void System_unlockVcop(void *arg)
{
    Semaphore_post(gSystem_VcopObj.vcopLock);
}


Int32 System_initVcop()
{
    Int32 status;
    Semaphore_Params vcopLockParams;

#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : VCOP Init in progress !!!\n",
               Utils_getCurTimeInMsec());
#endif

    memset(&gSystem_VcopObj.cpisInitPrm, 0, sizeof(gSystem_VcopObj.cpisInitPrm));

    Semaphore_Params_init(&vcopLockParams);
    vcopLockParams.mode = ti_sysbios_knl_Semaphore_Mode_BINARY;

    gSystem_VcopObj.vcopLock =
            Semaphore_create(1,&vcopLockParams, NULL);
    UTILS_assert(gSystem_VcopObj.vcopLock != NULL);


    gSystem_VcopObj.cpisInitPrm.cacheWbInv          = System_vcopCacheWbInv;
    gSystem_VcopObj.cpisInitPrm.staticDmaAlloc      = 1;
    gSystem_VcopObj.cpisInitPrm.maxNumDma           = 1;
    gSystem_VcopObj.cpisInitPrm.maxNumProcFunc      = 1;
    gSystem_VcopObj.cpisInitPrm.lock                = System_lockVcop;
    gSystem_VcopObj.cpisInitPrm.unlock              = System_unlockVcop;
    gSystem_VcopObj.cpisInitPrm.initFC              = CPIS_INIT_FC_ALL;
    gSystem_VcopObj.cpisInitPrm.engineName          = "alg_server";
    gSystem_VcopObj.cpisInitPrm.codecEngineHandle   = NULL;

    gSystem_VcopObj.cpisInitPrm.memSize = CPIS_getMemSize(gSystem_VcopObj.cpisInitPrm.maxNumProcFunc);

#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : VCOP needs %d B of memory !!!\n",
               Utils_getCurTimeInMsec(),
               gSystem_VcopObj.cpisInitPrm.memSize
                );
#endif

    gSystem_VcopObj.cpisInitPrm.mem = (void *)malloc(gSystem_VcopObj.cpisInitPrm.memSize);
    if(gSystem_VcopObj.cpisInitPrm.mem == NULL)
       return FVID2_EFAIL;

    /* Initialize CPIS */
    status = CPIS_init(&gSystem_VcopObj.cpisInitPrm);

    UTILS_assert(status==FVID2_SOK);

    {
        extern Task_Handle VICP_IP_RUN_hTsk;

        UTILS_assert(VICP_IP_RUN_hTsk!=NULL);

        status = Utils_prfLoadRegister( VICP_IP_RUN_hTsk, "VCOP");
        UTILS_assert(status==FVID2_SOK);
    }

#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : VCOP Init in progress DONE !!!\n",
               Utils_getCurTimeInMsec());
#endif

    return status;
}

Int32 System_deInitVcop()
{
#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : VCOP De-Init in progress !!!\n",
               Utils_getCurTimeInMsec());
#endif

    /* CPIS De Init */
    CPIS_deInit();

    if(gSystem_VcopObj.cpisInitPrm.mem!=NULL)
        free(gSystem_VcopObj.cpisInitPrm.mem);

    Semaphore_delete(&gSystem_VcopObj.vcopLock);

#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : VCOP De-Init in progress DONE !!!\n",
               Utils_getCurTimeInMsec());
#endif

    return FVID2_SOK;
}

Int32 System_initIss()
{
   Int32 status = FVID2_SOK;

#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : ISS Init in progress !!!\n",
               Utils_getCurTimeInMsec());
#endif

#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : ISS Power-ON in progress !!!\n",
               Utils_getCurTimeInMsec());
#endif

    /* Power ON Iss */
    *(volatile UInt32*)0x48180D00 = 0x2; /* PM_ISP_PWRSTCTRL     */
    *(volatile UInt32*)0x48180D10 = 0x3; /* RM_ISP_RSTCTRL       */
    *(volatile UInt32*)0x48180700 = 0x2; /* CM_ISP_CLKSTCTRL     */
    *(volatile UInt32*)0x48180720 = 0x2; /* CM_ISP_ISP_CLKCTRL   */
    *(volatile UInt32*)0x48180724 = 0x2; /* CM_ISP_FDIF_CLKCTRL  */
    Task_sleep(10);

#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : ISS Power-ON in progress DONE !!!\n",
               Utils_getCurTimeInMsec());
#endif

    status = Iss_init(NULL);
    UTILS_assert(status == 0);

#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : ISS Init in progress DONE !!!\n",
               Utils_getCurTimeInMsec());
#endif

/** This macro is controlled by Power Optimization Mode in Rules.make**/
#ifdef _POWERSAVE_MODE_
    System_setVcopBoostMode(FALSE);
#else
    System_setVcopBoostMode(TRUE);
#endif
    System_initVcop();

   return status;
}


Int32 System_deInitIss()
{
   Int32 status = FVID2_SOK;

    System_deInitVcop();

#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : ISS De-Init in progress !!!\n",
               Utils_getCurTimeInMsec());
#endif



    status = Iss_deInit(NULL);
    UTILS_assert(status == 0);

#ifdef SYSTEM_DEBUG
    Vps_printf(" %d: SYSTEM  : ISS De-Init in progress DONE !!!\n",
               Utils_getCurTimeInMsec());
#endif


   return status;
}

Int32 System_setVcopBoostMode(Bool enable)
{
    Int32 status = FVID2_SOK;

    /**
        SIMCOP_CLKCTRL(0x64). LDCIMXNSF_BOOST(Bit.8)
        LDCIMXNSF_BOOST   0:regular speed, 1:2X speed
       **/
    if(enable == TRUE)
        WR_MEM_32(0x55060064,0x1FF);
    else
        WR_MEM_32(0x55060064,0x0FF);

    if(((RD_MEM_32(0x55060064))&(1<<8)))
    {    Vps_printf(" %d: VCOP BOOST BIT is Set\n",
              Utils_getCurTimeInMsec());
    }

    return status;
}


