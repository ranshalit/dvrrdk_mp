/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/


#include "system_priv_common.h"
#include "system_priv_ipc.h"


static Void system_ipc_msgq_register_rpe_msgq_heap();

#define SYSTEM_GET_IPC_MSGQ_HEAPID(procId) ((procId) + SYSTEM_IPC_MSGQ_HEAP)
#define SYSTEM_IPC_MSGQ_VALIDATE_MSGPTR(msg) \
    UTILS_assert(((((char *)(msg) -          \
                    (char *)gSystem_ipcObj.msgStartAddr) / SYSTEM_IPC_MSGQ_MSG_SIZE_MAX) \
                 <                                                                           \
                 OSA_ARRAYSIZE(gSystem_ipcObj.freeMsgQueMem))                              \
                 &&                                                                          \
                 ((((char *)(msg) -                                                          \
                    (char *)gSystem_ipcObj.msgStartAddr) % SYSTEM_IPC_MSGQ_MSG_SIZE_MAX) \
                 ==                                                                           \
                 0));


static Void system_ipcmsgq_createfreeq()
{
    Int32 status;
    Int i;
    UInt32 procId;

    procId = System_getSelfProcId();
    UTILS_assert(procId != SYSTEM_PROC_INVALID);

    status =
    OSA_queCreate(&gSystem_ipcObj.freeMsgQue,
                    OSA_ARRAYSIZE(gSystem_ipcObj.freeMsgQueMem));
    UTILS_assert(status == 0);

    for (i = 0; i <  OSA_ARRAYSIZE(gSystem_ipcObj.freeMsgQueMem);i++)
    {
        MessageQ_Msg allocMsg;

        allocMsg =
        MessageQ_alloc(SYSTEM_GET_IPC_MSGQ_HEAPID(procId),SYSTEM_IPC_MSGQ_MSG_SIZE_MAX);
        UTILS_assert(allocMsg != NULL);
        if (0 == i)
        {
            gSystem_ipcObj.msgStartAddr = allocMsg;
        }
        status =
        OSA_quePut(&gSystem_ipcObj.freeMsgQue,(Int32)allocMsg,OSA_TIMEOUT_NONE);
        UTILS_assert(status == 0);
    }
}

static Void system_ipcmsgq_deletefreeq()
{

    Int32 status;

    UTILS_assert(OSA_queGetQueuedCount(&gSystem_ipcObj.freeMsgQue) == OSA_ARRAYSIZE(gSystem_ipcObj.freeMsgQueMem));

    status =
    OSA_queDelete(&gSystem_ipcObj.freeMsgQue);
    UTILS_assert(status == 0);
}

static MessageQ_Msg system_ipcmsgq_alloc( UInt32 size)
{
    MessageQ_Msg allocMsg;
    Int32 status;

    UTILS_assert(size <= SYSTEM_IPC_MSGQ_MSG_SIZE_MAX);
    status =
    OSA_queGet(&gSystem_ipcObj.freeMsgQue,
                 (Int32 *)&allocMsg,
                 OSA_TIMEOUT_NONE);
    UTILS_assert(status == 0);
    return allocMsg;
}

static Int system_ipcmsgq_free( MessageQ_Msg freeMsg)
{
    Int32 status;


    SYSTEM_IPC_MSGQ_VALIDATE_MSGPTR(freeMsg);
    status =
    OSA_quePut(&gSystem_ipcObj.freeMsgQue,
                 (Int32)freeMsg,
                 OSA_TIMEOUT_NONE);
    UTILS_assert(status == 0);

    return MessageQ_S_SUCCESS;
}

Int32 System_ipcMsgQHeapCreate()
{
    UInt32 procId;
    IHeap_Handle srHeap;
    HeapMemMP_Params heapMemParams;
    GateMP_Params gateMPParams;
    char msgq_heap_name[128];

    procId = System_getSelfProcId();
    if (procId != SYSTEM_PROC_INVALID)
    {
        snprintf(msgq_heap_name,
                 sizeof(msgq_heap_name) - 1,
                 "%s_%d",SYSTEM_IPC_MSGQ_HEAP_NAME,procId);
        msgq_heap_name[sizeof(msgq_heap_name) - 1] = 0;
        /* create heap */
        printf(" %d: SYSTEM: Creating MsgQ Heap [%s] ...\n",
                  OSA_getCurTimeInMsec(), msgq_heap_name);
    	printf("System_ipcGetSRHeap 01\n");
    	//usleep(100000); //ranran
        srHeap = System_ipcGetSRHeap(SYSTEM_IPC_SR_NON_CACHED_DEFAULT);
        UTILS_assert(srHeap != NULL);

    	printf("System_ipcGetSRHeap 02\n");

        gSystem_ipcObj.msgQHeapBaseAddr =
            Memory_alloc(srHeap, SYSTEM_IPC_MSGQ_HEAP_SIZE, 0, NULL);

    	printf("System_ipcGetSRHeap 03\n");

        UTILS_assert(gSystem_ipcObj.msgQHeapBaseAddr != NULL);

    	printf("System_ipcGetSRHeap 04\n");

        GateMP_Params_init(&gateMPParams);

    	printf("System_ipcGetSRHeap 05\n");

        gateMPParams.regionId = SYSTEM_IPC_SR_NON_CACHED_DEFAULT;
        gateMPParams.remoteProtect = GateMP_RemoteProtect_CUSTOM1;
        gSystem_ipcObj.msgQHeapGate = GateMP_create(&gateMPParams);
        UTILS_assert(gSystem_ipcObj.msgQHeapGate != NULL);
    	printf("System_ipcGetSRHeap 06\n");

        HeapMemMP_Params_init(&heapMemParams);
    	printf("System_ipcGetSRHeap 07\n");

        heapMemParams.name = SYSTEM_IPC_MSGQ_HEAP_NAME;
        heapMemParams.sharedAddr = gSystem_ipcObj.msgQHeapBaseAddr;
        heapMemParams.sharedBufSize = SYSTEM_IPC_MSGQ_HEAP_SIZE;
        heapMemParams.gate = gSystem_ipcObj.msgQHeapGate;

        gSystem_ipcObj.msgQHeapHndl = HeapMemMP_create(&heapMemParams);
    	printf("System_ipcGetSRHeap 08\n");

        UTILS_assert(gSystem_ipcObj.msgQHeapHndl != NULL);
    	printf("System_ipcGetSRHeap 09\n");

        /* Register this heap with MessageQ */
        MessageQ_registerHeap((IHeap_Handle) gSystem_ipcObj.msgQHeapHndl,
                              SYSTEM_GET_IPC_MSGQ_HEAPID(procId));

    	printf("System_ipcGetSRHeap 10\n");

        system_ipcmsgq_createfreeq();
        system_ipc_msgq_register_rpe_msgq_heap();
    }


    return OSA_SOK;
}

Int32 System_ipcMsgQHeapDelete()
{
    Int32 status;
    UInt32 procId;
    IHeap_Handle srHeap;

    procId = System_getSelfProcId();

    system_ipcmsgq_deletefreeq();
    MessageQ_unregisterHeap(SYSTEM_GET_IPC_MSGQ_HEAPID(procId));

    /* delete heap */
    status = HeapMemMP_delete(&gSystem_ipcObj.msgQHeapHndl);
    UTILS_assert(status == OSA_SOK);

    srHeap = System_ipcGetSRHeap(SYSTEM_IPC_SR_NON_CACHED_DEFAULT);
    UTILS_assert(srHeap != NULL);

    Memory_free(srHeap,
                gSystem_ipcObj.msgQHeapBaseAddr, SYSTEM_IPC_MSGQ_HEAP_SIZE);
    status = GateMP_delete(&gSystem_ipcObj.msgQHeapGate);
    UTILS_assert(status == GateMP_S_SUCCESS);

    return OSA_SOK;
}

Int32 System_ipcMsgQTskCreate()
{
    Int32 status;

    status = OSA_mutexCreate(&gSystem_ipcObj.msgQLock);
    UTILS_assert(status==OSA_SOK);

    gSystem_ipcObj.msgQTaskExit = FALSE;
    gSystem_ipcObj.msgQTaskExitDone = FALSE;

    /*
     * Create task
     */
    status = OSA_thrCreate(
                &gSystem_ipcObj.msgQTask,
                System_ipcMsgQTaskMain,
                SYSTEM_MSGQ_TSK_PRI,
                SYSTEM_MSGQ_TSK_STACK_SIZE,
                NULL
            );
    UTILS_assert(status==OSA_SOK);

    return OSA_SOK;
}

Int32 System_ipcMsgQTskDelete()
{
    Int32 status;

    gSystem_ipcObj.msgQTaskExit = TRUE;

    /* unblock task */
    MessageQ_unblock(gSystem_ipcObj.selfMsgQ);

    while(gSystem_ipcObj.msgQTaskExitDone == FALSE)
    {
       /* wait for command to be received
              and task to be exited */
       OSA_waitMsecs(10);
    }

    OSA_thrDelete(&gSystem_ipcObj.msgQTask);

    status = OSA_mutexDelete ( &gSystem_ipcObj.msgQLock );
    UTILS_assert(status==OSA_SOK);

    return OSA_SOK;
}

Int32 System_ipcMsgQCreate()
{
    UInt32 i;
    UInt32 procId;
    Int32 status;
    Int32 retryCount;
    MessageQ_Params msgQParams;
    char msgQName[64];
    char ackMsgQName[64];

    i=0;

    while(gSystem_ipcEnableProcId[i]!=SYSTEM_PROC_MAX)
    {
        procId = gSystem_ipcEnableProcId[i];

        if (procId != SYSTEM_PROC_INVALID)
        {
            System_ipcGetMsgQName(procId, msgQName, ackMsgQName);

            if(procId==System_getSelfProcId())
            {
                /* create MsgQ */
                MessageQ_Params_init(&msgQParams);

                printf(" %u: SYSTEM: Creating MsgQ [%s] ...\n",
                    OSA_getCurTimeInMsec(),
                    msgQName
                );

                gSystem_ipcObj.selfMsgQ = MessageQ_create(msgQName, &msgQParams);
                UTILS_assert(gSystem_ipcObj.selfMsgQ!=NULL);

                MessageQ_Params_init(&msgQParams);

                printf(" %u: SYSTEM: Creating MsgQ [%s] ...\n",
                    OSA_getCurTimeInMsec(),
                    ackMsgQName
                );

                gSystem_ipcObj.selfAckMsgQ = MessageQ_create(ackMsgQName, &msgQParams);
                UTILS_assert(gSystem_ipcObj.selfMsgQ!=NULL);
            }
           else
           {
//#ifdef SKIP_DSP
             if (procId == SYSTEM_PROC_DSP) //ranran skip dsp
             {
                 i++;
                 continue;
             }

//#endif
                /* open MsgQ */

                retryCount=10;
                while(retryCount)
                {
                    printf(" %u: SYSTEM: Opening MsgQ [%s] ...\n",
                        OSA_getCurTimeInMsec(),
                        msgQName
                    );

                    status = MessageQ_open(msgQName, &gSystem_ipcObj.remoteProcMsgQ[procId]);
                    if(status==MessageQ_E_NOTFOUND)
                        OSA_waitMsecs(1000);
                    else
                    if(status==MessageQ_S_SUCCESS)
                        break;

                    retryCount--;
                    if(retryCount<=0)
                        UTILS_assert(0);
                }

                /* no need to open ack msgq,
                    since ack msgq id is embeeded in the received message
                */
          //  }
           }
        }
        i++;
    }
    return OSA_SOK;
}

Int32 System_ipcMsgQDelete()
{
    UInt32 i;
    UInt32 procId;
    Int32 status;

    i=0;

    while(gSystem_ipcEnableProcId[i]!=SYSTEM_PROC_MAX)
    {
        procId = gSystem_ipcEnableProcId[i];

        if (procId != SYSTEM_PROC_INVALID)
        {
            if(procId==System_getSelfProcId())
            {
                /* delete MsgQ */

                status = MessageQ_delete(&gSystem_ipcObj.selfMsgQ);
                UTILS_assert(status==0);

                status = MessageQ_delete(&gSystem_ipcObj.selfAckMsgQ);
                UTILS_assert(status==0);
            }
            else
            {
                               printf("ranran 33 %d\n",procId);
//#ifdef SKIP_DSP
             if (procId == SYSTEM_PROC_DSP) //ranran skip dsp
             {
                 i++;
                 continue;
             }
       //      {
      //         printf("ranran 3 %d\n",procId);
//#endif
                status = MessageQ_close(&gSystem_ipcObj.remoteProcMsgQ[procId]);
                UTILS_assert(status==0);
             }
                /* no need to close ack msgq */
           // }
        }
        i++;
    }
    return OSA_SOK;
}

Int32 System_ipcMsgQInit()
{
    System_ipcMsgQHeapCreate();
    System_ipcMsgQCreate();
    System_ipcMsgQTskCreate();

    return OSA_SOK;
}

Int32 System_ipcMsgQDeInit()
{
    System_ipcMsgQTskDelete();
    System_ipcMsgQDelete();
    System_ipcMsgQHeapDelete();

    return OSA_SOK;
}

Void *System_ipcMsgQTaskMain(Void *arg)
{
    UInt32 prmSize;
    SystemIpcMsgQ_Msg *pMsgCommon;
    Void *pPrm;
    Int32 status;

    while(1)
    {
        status = MessageQ_get(gSystem_ipcObj.selfMsgQ, (MessageQ_Msg*)&pMsgCommon, OSA_TIMEOUT_FOREVER);

        if(status == MessageQ_E_UNBLOCKED || gSystem_ipcObj.msgQTaskExit == TRUE )
        {
            break;
        }

        if(status!=MessageQ_S_SUCCESS)
        {
            /* TODO: Commenting this print for release purpose. Need to enable &
                        * fix this failure occurring on disp resolution change
                        */
           printf(" %u: MSGQ: MsgQ get failed !!!\n",
                       OSA_getCurTimeInMsec()
                       );
            continue;
        }

        #if 0
        printf(" %u: MSGQ: Received command [0x%04x] (prmSize = %d) for [%s][%02d] (waitAck=%d)\n",
            OSA_getCurTimeInMsec(),
            pMsgCommon->cmd,
            pMsgCommon->prmSize,
            System_getProcName(SYSTEM_GET_PROC_ID(pMsgCommon->linkId)),
            SYSTEM_GET_LINK_ID(pMsgCommon->linkId),
            pMsgCommon->waitAck
            );
        #endif

        prmSize = pMsgCommon->prmSize;

        pPrm = SYSTEM_IPC_MSGQ_MSG_PAYLOAD_PTR(pMsgCommon);

        if(pMsgCommon->cmd==SYSTEM_CMD_GET_INFO)
        {
            UTILS_assert(prmSize == sizeof(System_LinkInfo));

            pMsgCommon->status = System_linkGetInfo_local(pMsgCommon->linkId, pPrm);
        }
        else
        {
            pMsgCommon->status = System_linkControl_local(
                                    pMsgCommon->linkId,
                                    pMsgCommon->cmd,
                                    pPrm,
                                    prmSize,
                                    pMsgCommon->waitAck
                                 );
        }
        if(pMsgCommon->waitAck)
        {
            MessageQ_QueueId replyMsgQ;

            replyMsgQ = MessageQ_getReplyQueue(pMsgCommon);

            status = MessageQ_put(replyMsgQ, (MessageQ_Msg)pMsgCommon);

            UTILS_assert(status == MessageQ_S_SUCCESS);
        }
        else
        {
            UTILS_assert(FALSE);
        }
    }
    gSystem_ipcObj.msgQTaskExitDone = TRUE;

    return NULL;
}

Int32 System_ipcMsgQSendMsg(UInt32 linkId, UInt32 cmd, Void *pPrm, UInt32 prmSize, Bool waitAck, UInt32 timeout)
{
    Int32 status=OSA_SOK;
    SystemIpcMsgQ_Msg *pMsgCommon;
    UInt32 procId;
    Void *pMsgPrm;

    UTILS_assert(prmSize<=SYSTEM_IPC_MSGQ_MSG_SIZE_MAX);

    procId = SYSTEM_GET_PROC_ID(linkId);

    #ifdef TI_8107_BUILD
    if(procId==SYSTEM_PROC_DSP)
    {
        printf(" %u: MSGQ: WARNING: Trying to send command [0x%04x] to link [%d] on processor [%s], BUT [%s] is NOT present on this platform !!!\n",
                        OSA_getCurTimeInMsec(),
                        cmd,
                        SYSTEM_GET_LINK_ID(linkId),
                        System_getProcName(procId),
                        System_getProcName(procId)
                        );

        /* return SUCCESS so that calling API can continue */
        return status;
    }
    #endif

    OSA_mutexLock(&gSystem_ipcObj.msgQLock);

    UTILS_assert(  procId < SYSTEM_PROC_MAX);

    pMsgCommon = (SystemIpcMsgQ_Msg *)system_ipcmsgq_alloc(
                    sizeof(*pMsgCommon)+prmSize
                    );

    UTILS_assert(pMsgCommon!=NULL);

    if(prmSize && pPrm)
    {
        pMsgPrm = SYSTEM_IPC_MSGQ_MSG_PAYLOAD_PTR(pMsgCommon);
        memcpy(pMsgPrm, pPrm, prmSize);
    }

    pMsgCommon->linkId = linkId;
    pMsgCommon->cmd = cmd;
    pMsgCommon->prmSize = prmSize;
    pMsgCommon->waitAck = waitAck;
    pMsgCommon->status = OSA_SOK;

    MessageQ_setReplyQueue(gSystem_ipcObj.selfAckMsgQ, (MessageQ_Msg)pMsgCommon);
    MessageQ_setMsgId(pMsgCommon, linkId);

    status = MessageQ_put(gSystem_ipcObj.remoteProcMsgQ[procId], (MessageQ_Msg)pMsgCommon);
    if(status!=MessageQ_S_SUCCESS)
    {
        printf(" %u: MSGQ: MsgQ put for [%s] failed !!!\n",
                        OSA_getCurTimeInMsec(),
                        System_getProcName(procId)
                        );
        system_ipcmsgq_free((MessageQ_Msg)pMsgCommon);
        OSA_mutexUnlock(&gSystem_ipcObj.msgQLock);
        return status;
    }

    if(waitAck)
    {
        SystemIpcMsgQ_Msg *pAckMsg;
        Bool retryMsgGet = FALSE;
        UInt32 retryCount = 1000;
        if (OSA_TIMEOUT_FOREVER == timeout)
        {
            timeout = MessageQ_FOREVER;
        }

        do {
            status = MessageQ_get(gSystem_ipcObj.selfAckMsgQ, (MessageQ_Msg*)&pAckMsg, timeout);
            if(status==MessageQ_E_TIMEOUT)
            {
                /* if timeout then break out of retry loop */
                break;
            }
            if(status!=MessageQ_S_SUCCESS)
            {
                /* normally this situation should not happen, this more for safety and debug purpose */
                retryCount--;

                printf(" %u: MSGQ: MsgQ Ack get from [%s] failed for link %d, cmdId 0x%04x !!! (retrying - %d times)\n",
                           OSA_getCurTimeInMsec(),
                           System_getProcName(procId),
                           SYSTEM_GET_LINK_ID(linkId),
                           cmd,
                           retryCount
                            );

                if(retryCount==0)
                {
                    retryMsgGet = FALSE;
                }
                else
                {
                    retryMsgGet = TRUE;
                }
            }
            if (status == MessageQ_S_SUCCESS)
            {
                if (!((pAckMsg->linkId == linkId)
                      &&
                      (pAckMsg->cmd    == cmd)))
                {
                    /* normally this situation should not happen, this more for safety and debug purpose */

                    printf(" %u: MSGQ: MsgQ Ack get from [%s] failed for link %d, cmdId 0x%04x !!! \n"
                           "           Received unexpected Ack from [%s] link %d, cmdId 0x%04x !!! \n",
                           OSA_getCurTimeInMsec(),
                           System_getProcName(procId),
                           SYSTEM_GET_LINK_ID(linkId),
                           cmd,
                           System_getProcName(SYSTEM_GET_PROC_ID(pAckMsg->linkId)),
                           SYSTEM_GET_LINK_ID(pAckMsg->linkId),
                           pAckMsg->cmd
                           );

                    system_ipcmsgq_free((MessageQ_Msg)pAckMsg);
                    retryMsgGet = TRUE;
                }
            }

        } while(TRUE == retryMsgGet);

        if (status!=MessageQ_S_SUCCESS)
        {
            /* Do not free msg if MsgQ_Get timesout.
             * Since we dont have Msg ownership, we should not free the msg
             */
            /* MessageQ_free((MessageQ_Msg)pMsgCommon); */
            OSA_mutexUnlock(&gSystem_ipcObj.msgQLock);
            return status;
        }

        if(prmSize && pPrm)
        {
            pMsgPrm = SYSTEM_IPC_MSGQ_MSG_PAYLOAD_PTR(pAckMsg);
            memcpy(pPrm, pMsgPrm, prmSize);
        }

        status = pAckMsg->status;

        system_ipcmsgq_free((MessageQ_Msg)pAckMsg);
    }

    OSA_mutexUnlock(&gSystem_ipcObj.msgQLock);

    return status;
}

static Void system_ipc_msgq_register_rpe_msgq_heap()
{
#ifdef  DSP_RPE_AUDIO_ENABLE
    extern UInt16  Rpe_messageqHeapId;
    IHeap_Handle srHeap;

    UTILS_assert(Rpe_messageqHeapId != SYSTEM_IPC_MSGQ_HEAP);
    srHeap = System_ipcGetSRHeap(SYSTEM_IPC_SR_NON_CACHED_DEFAULT);
    UTILS_assert(srHeap != NULL);

    MessageQ_registerHeap(srHeap,
                          Rpe_messageqHeapId);
#endif
}
