/*******************************************************************************
 *                                                                            
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com/      
 *                        ALL RIGHTS RESERVED                                  
 *                                                                            
 ******************************************************************************/

#include <ti_media_common_def.h>
#include <osa.h>
#include <osa_thr.h>
#include <osa_sem.h>
#include <osa_que.h>
#include <link_api/audioLink.h>
#include <alsa/asoundlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ti/syslink/utils/Cache.h>

// #define WRITE_ALL_DATA_TO_FILE  // Debug purpose: just write interleaved data to file and no further processing
// #define REOPEN_CAPTURE_DEVICE_ON_ERROR /* Hack - Reopen capture device if overrun occurs */

/** < All these macros are specific to tvp5158 */

/** < All these values are in samples. Bytes required should be calculated by multiplying with sample len */
#define ACAP_BUFFERING_REQUIREMENT        1      /* For some reason, we have to read / feed this much data for playback */


#define ACAP_DRIVER_BUFFER_SIZE           (4*1024)
#define ACAP_MIN_FRAMES_2_READ_PER_CH    (1024)       //< Ensure to align with hw param BUFFER_SIZE; only < 256 works right now
#define ACAP_BUFFER_SIZE_PER_CH           (ACAP_MIN_FRAMES_2_READ_PER_CH   \
                                                     * ACAP_BUFFERING_REQUIREMENT \
                                              )  


#define ACAP_SAMPLE_LEN                     2
/**< The big capture buffer len for all channels in bytes */
#define ACAP_CAPTURE_BUFFER_LENGTH        (ACAP_BUFFER_SIZE_PER_CH \
                                                     * ACAP_CHANNELS_MAX   \
                                                     * ACAP_SAMPLE_LEN \
                                              )

#define ACAP_CAPTURE_TSK_PRI               18
#define ACAP_CAPTURE_TSK_STACK_SIZE       (10*1024)
#define ACAP_MAX_CH_PCM_BUFFERS                 8

#define     AUD_DEVICE_PRINT_ERROR_AND_RETURN(str, err, hdl)        \
        fprintf (stderr, "\n\r [host] AUDIO >> " str, snd_strerror (err));  \
        snd_pcm_close (hdl);    \
        return  -1;

#define AUDIO_DATA_MAX_PENDING_RECV_SEM_COUNT   1

#define AUDIO_ENCODE_FRAME_SIZE_IN_SAMPLES      (1*1024)

typedef struct  
{
    Int32   errorCnt;
    Int32   lastError;
    UInt32  totalCaptureLen;
} ACAP_STATS_S;

typedef struct
{
    snd_pcm_t       *alsa_handle;
    ACAP_STATS_S    acapStats;
    Int8            audioPhyToDataIndexMap[ACAP_CHANNELS_MAX];
    /* Have twice the buffer length as for tvp5158 capture, even 8ch data works 
          * in 256fs mode which means capture buffer size should be for 16ch 
          */
    UInt16          captureBuf[ACAP_CAPTURE_BUFFER_LENGTH*2]; 
    Int32           captureLen;
    OSA_ThrHndl     captureThrHandle;
    Bool            taskExitFlag;
    UInt32          captureChannelMask;
    Bool            captureTaskActive;
    Bool            captureStarted;

    UInt16          *chData[ACAP_CHANNELS_MAX];
    Int32           chDataMaxSize[ACAP_CHANNELS_MAX];
    Int32           skipSample; /* In 256fs for 8ch tvp capture, there is padding to be skipped */
    Audio_CaptureParams prm;

} Audio_ContextInt;

/** Globals */
static Audio_ContextInt    gAcapContextInt;
static UInt16             *gChWriteBuf[ACAP_CHANNELS_MAX];
static Int32               gChWriteBufMissCount[ACAP_CHANNELS_MAX];
static Int32               gChBufMaxSize[ACAP_CHANNELS_MAX];
static Int32               gChWrIdx[ACAP_CHANNELS_MAX];
static UInt32              gChBufList[ACAP_CHANNELS_MAX][ACAP_MAX_CH_PCM_BUFFERS];

static Void               *gEncHandle[ACAP_CHANNELS_MAX];
static UInt8              *gEncBuf[ACAP_CHANNELS_MAX];
static Int32               gEncBufMaxSize[ACAP_CHANNELS_MAX];
static Int32               gTotalFramesEncoded[ACAP_CHANNELS_MAX];
static Int32               gTotalFramesFailed[ACAP_CHANNELS_MAX];

static UInt8              *gIntEncodeBuffer[ACAP_CHANNELS_MAX];
static Int32               gIntEncBufSize[ACAP_CHANNELS_MAX];
static Int32               gEncodeCallCount[ACAP_CHANNELS_MAX];
static Int32               gEncodeTimeSpent[ACAP_CHANNELS_MAX];

static Int32               gChPCMBufsWritten[ACAP_CHANNELS_MAX];
static Int32               gChPCMBufsEmptied[ACAP_CHANNELS_MAX];
static Int32               gChPCMBufsRead[ACAP_CHANNELS_MAX];

static OSA_QueHndl         gChEmptyBufQue[ACAP_CHANNELS_MAX];
static OSA_QueHndl         gChPCMBufQue[ACAP_CHANNELS_MAX];

static  Void *Audio_captureTaskFxn(Void * prm);
static  Int32 Audio_InitCaptureDevice (Int8 *device, Int32 numChannels, UInt32 sampleRate);
static  Int32 Audio_deInitCaptureDevice(Void);
static  Int32 Audio_captureData(UInt16 *buffer, Int32 *numSamples);
#ifndef      WRITE_ALL_DATA_TO_FILE
static  Void Audio_demux_tvp5158(UInt16 *captureBuf, Int32 captureSize);
static  Void Audio_demux_other(UInt16 *captureBuf, Int32 captureSize);
#endif

#ifdef      WRITE_ALL_DATA_TO_FILE
Void Audio_writeToFile (UInt16 *captureBuf, Int32 captureSize)
{
    static FILE *fp = NULL;

    if (fp == NULL)
    {
        fp = fopen("/media/sda2/allCh.pcm", "wb");

        if (fp)
            setvbuf(fp,
                  NULL,
                  _IOFBF,
                  (512*1024));
    }

    if (fp)
        fwrite (captureBuf, captureSize, sizeof(UInt16) * gAcapContextInt.prm.numChannels, fp);
    return;
}
#endif

/************************** TVP5158 CAPTURED AUDIO DATA MAPPING*******************************/
/* Audio data captured from the TVP5158 is interleaved*/
/* TVP5158 Daughter card has following configuration for Audio Input (Hardware pins)
    --------------------------------------------------------------------------------------
    | AIN15 | AIN13 | AIN11 | AIN9  | AIN7 | AIN5 | AIN3 | AIN1 |
    --------------------------------------------------------------------------------------
    | AIN16 | AIN14 | AIN12 | AIN10 | AIN8 | AIN6 | AIN4 | AIN2 |
    --------------------------------------------------------------------------------------
    */

/*
    Channel Mapping for 4-channels audio capture
    <-----------------64bits----------------->
    <-16bits->
    --------------------------------------------
    | S16-0  | S16-1  | S16-2  | S16-3 |
    --------------------------------------------
    | AIN 1   | AIN 3   | AIN 2   | AIN 0  |
    --------------------------------------------
    */

static Void    Audio_initTvp5158ChMapping(Int32 numChannels)
{
    if (numChannels == 4)
    {
        gAcapContextInt.audioPhyToDataIndexMap[0]   = 1;
        gAcapContextInt.audioPhyToDataIndexMap[1]   = 3;
        gAcapContextInt.audioPhyToDataIndexMap[2]   = 2;
        gAcapContextInt.audioPhyToDataIndexMap[3]   = 0;
    }
    else /* Mapping same for 8 / 16 channels */
    {    
        gAcapContextInt.audioPhyToDataIndexMap[0]   = 1;
        gAcapContextInt.audioPhyToDataIndexMap[1]   = 9;
        gAcapContextInt.audioPhyToDataIndexMap[2]   = 2;
        gAcapContextInt.audioPhyToDataIndexMap[3]   = 10;

        gAcapContextInt.audioPhyToDataIndexMap[4]   = 3;
        gAcapContextInt.audioPhyToDataIndexMap[5]   = 11;
        gAcapContextInt.audioPhyToDataIndexMap[6]   = 4;
        gAcapContextInt.audioPhyToDataIndexMap[7]   = 12;

        gAcapContextInt.audioPhyToDataIndexMap[8]   = 5;
        gAcapContextInt.audioPhyToDataIndexMap[9]   = 13;
        gAcapContextInt.audioPhyToDataIndexMap[10]  = 6;
        gAcapContextInt.audioPhyToDataIndexMap[11]  = 14;

        gAcapContextInt.audioPhyToDataIndexMap[12]  = 7;
        gAcapContextInt.audioPhyToDataIndexMap[13]  = 15;
        gAcapContextInt.audioPhyToDataIndexMap[14]  = 8;
        gAcapContextInt.audioPhyToDataIndexMap[15]  = 0;
    }
}


Int32   Audio_getMinCaptureChBufSize (Void)
{
    return (ACAP_BUFFER_SIZE_PER_CH * ACAP_MAX_CH_PCM_BUFFERS);   // This is per sample & not in bytes 
}

Int32   Audio_getMinEncodeOutBufSize (Void)
{
    Int32 len = ACAP_BUFFER_SIZE_PER_CH;

    if (len < AUDIO_ENCODE_FRAME_SIZE_IN_SAMPLES)
        len = AUDIO_ENCODE_FRAME_SIZE_IN_SAMPLES;
    return (len);
}

Int32   Audio_getSampleLenInBytes(Void)
{
    return ACAP_SAMPLE_LEN;
}

Int32   Audio_getMaxBuffers(Void)
{
    return ACAP_MAX_CH_PCM_BUFFERS;
}


Int32   Audio_deInitCapture (Int32 successCh, OSA_QueHndl bufQue[])
{
    Int32 i;
    for (i=0; i<successCh; i++)
    {
        OSA_queDelete(&bufQue[i]);
    }
    return OSA_SOK;
}

Int32   Audio_deInitEncode (Audio_CaptureParams *pPrm)
{
    Int32 ch;
    Audio_EncodeCreateParams *encParam;

    for (ch=0; ch<pPrm->numChannels; ch++)
    {
        encParam = &pPrm->chPrm[ch].encodeParam;

        if (gEncHandle[ch] != NULL)
        {
            Audio_deleteEncAlgorithm(gEncHandle[ch]);
            Audio_freeMem(gEncHandle[ch]);
            gEncHandle[ch] = NULL;
        }
        if (gIntEncodeBuffer[ch] != NULL)
        {
            Audio_freeSharedRegionBuf(gIntEncodeBuffer[ch], gIntEncBufSize[ch]);
            gIntEncodeBuffer[ch] = NULL;
        }
    }
    return OSA_SOK;
}

Int32   Audio_initEncode (Audio_CaptureParams *pPrm)
{
    Int32 ch;
    Audio_EncodeCreateParams *encParam;
    Void *handle = NULL;

    memset(gEncodeCallCount, 0, sizeof(gEncodeCallCount));
    memset(gEncodeTimeSpent, 0, sizeof(gEncodeTimeSpent));

    for (ch=0; ch<pPrm->numChannels; ch++)
    {
        encParam = &pPrm->chPrm[ch].encodeParam;

        gEncHandle[ch] = gIntEncodeBuffer[ch] = NULL;
        gEncBuf[ch] = (UInt8*)pPrm->chPrm[ch].encodeBuf.dataBuf;
        gEncBufMaxSize[ch] = pPrm->chPrm[ch].encodeBuf.dataBufSize;
        gTotalFramesEncoded[ch] = gTotalFramesFailed[ch] = 0;

        if (pPrm->chPrm[ch].enableEncode)
        {
            encParam->sampleRate = gAcapContextInt.prm.sampleRate;
#ifndef  DSP_RPE_AUDIO_ENABLE
            encParam->encoderType = AUDIO_CODEC_TYPE_G711;
#endif

            switch (encParam->encoderType )
            {
                case AUDIO_CODEC_TYPE_AAC_LC:
                {
                    handle = Audio_allocMem(Audio_getEncoderContextSize(encParam->encoderType));
                    if (handle)
                    {
                        if (Audio_createAacEncAlgorithm(handle, encParam) != NULL)
                        {
                            gEncHandle[ch] = handle;
                            if (gEncBufMaxSize[ch] < encParam->minOutBufSize ||
                                    gAcapContextInt.chDataMaxSize[ch] < encParam->minInBufSize)
                            {
                                AUDIO_ERROR_PRINT(("AUDIO: CAPTURE -> enc or capture buffer less for ch %d\n", ch));
                                return OSA_EFAIL;
                            }
                        }
                        else
                        {
                            Audio_freeMem(handle);
                            return OSA_EFAIL;
                        }
                    }
                }
                break;
            
                case AUDIO_CODEC_TYPE_G711:
                {
                    handle = Audio_allocMem(Audio_getEncoderContextSize(encParam->encoderType));
                    if (handle)
                    {
                        if (Audio_createG711EncAlgorithm(handle) != NULL)
                        {
                            gEncHandle[ch] = handle;
                        }
                        else
                        {
                            Audio_freeMem(handle);
                            return OSA_EFAIL;
                        }
                    }
                }
            }
            if (gEncHandle[ch] == NULL) 
            {
                AUDIO_ERROR_PRINT(("AUDIO: CAPTURE -> Encoder creation failed for CH - %d\n", ch));
                return OSA_EFAIL;
            }
            else
            {
                if (encParam->encoderType == AUDIO_CODEC_TYPE_AAC_LC)
                {
                    gIntEncBufSize[ch] = encParam->minOutBufSize;
                    gIntEncodeBuffer[ch] = Audio_allocateSharedRegionBuf(gIntEncBufSize[ch]);
                    if (gIntEncodeBuffer[ch] == NULL)
                    {
                        AUDIO_ERROR_PRINT(("AUDIO: CAPTURE -> Encoder creation failed for CH - %d\n", ch));
                        Audio_deleteEncAlgorithm(gEncHandle[ch]);
                        Audio_freeMem(gEncHandle[ch]);
                        return OSA_EFAIL;
                    }
                }
                AUDIO_ERROR_PRINT(("AUDIO: CAPTURE -> Encoder creation SUCCESS for CH - %d\n", ch));
            }
        }
    }
    return OSA_SOK;
}

Int32   Audio_startCapture(Audio_CaptureParams *pPrm)
{
    Int32 status = ERROR_FAIL, ch;

    memset(&gAcapContextInt, 0, sizeof(Audio_ContextInt));
    gAcapContextInt.alsa_handle   = NULL;
    gAcapContextInt.taskExitFlag  = FALSE;
    gAcapContextInt.prm           = *pPrm;

    memset(gIntEncodeBuffer, 0, sizeof(gIntEncodeBuffer));
    memset(gIntEncBufSize, 0, sizeof(gIntEncBufSize));
    memset(gEncHandle, 0, sizeof(gEncHandle));
    memset(gEncBuf, 0, sizeof(gEncBuf));
    memset(gEncBufMaxSize, 0, sizeof(gEncBufMaxSize));

    if (pPrm->enableTVP5158 == TRUE)
    {
        Audio_initTvp5158ChMapping(pPrm->numChannels);
    }
    
    for (ch=0; ch<pPrm->numChannels; ch++)
    {
        UInt8 bufs;

        gAcapContextInt.chData[ch] = (UInt16*) pPrm->chPrm[ch].captureBuf.dataBuf;
        gAcapContextInt.chDataMaxSize[ch] = pPrm->chPrm[ch].captureBuf.dataBufSize;

        gChWriteBuf[ch] = NULL;
        gChWrIdx[ch] = 0;
        gChWriteBufMissCount[ch] = 0;
        gChBufMaxSize[ch] = (gAcapContextInt.chDataMaxSize[ch] / ACAP_MAX_CH_PCM_BUFFERS);

        /* for statistics */
        gChPCMBufsWritten[ch] = gChPCMBufsRead[ch] = gChPCMBufsEmptied[ch] = 0;

        /* create the ch empty buf & read data queue */
        status = OSA_queCreate(&gChEmptyBufQue[ch], ACAP_MAX_CH_PCM_BUFFERS*2);
        OSA_assert(status == OSA_SOK);

        status = OSA_queCreate(&gChPCMBufQue[ch], ACAP_MAX_CH_PCM_BUFFERS*2);
        OSA_assert(status == OSA_SOK);

        for (bufs=0; bufs<ACAP_MAX_CH_PCM_BUFFERS; bufs++)
        {
            gChBufList[ch][bufs] = (UInt32) (((UInt32) gAcapContextInt.chData[ch]) + (bufs * gChBufMaxSize[ch] * ACAP_SAMPLE_LEN));
/*
            AUDIO_INFO_PRINT (("AUDIO: Org capPtr: %X <%d>,  Buffer: %X <%d>\n", 
                    (UInt32)gAcapContextInt.chData[ch], gAcapContextInt.chDataMaxSize[ch],
                    (UInt32)gChBufList[ch][bufs],gChBufMaxSize[ch]));
*/
            status = OSA_quePut(&gChEmptyBufQue[ch], (Int32)gChBufList[ch][bufs], OSA_TIMEOUT_NONE);
            OSA_assert(status == OSA_SOK);
        }
    }
    
    if (Audio_initEncode(pPrm) != OSA_SOK)
    {
        Audio_deInitCapture(ch, gChEmptyBufQue);
        Audio_deInitCapture(ch, gChPCMBufQue);
        Audio_deInitEncode(pPrm);
        return ERROR_FAIL;
    }

    if (status == OSA_SOK)
    {
        status = OSA_thrCreate(&gAcapContextInt.captureThrHandle,
                      Audio_captureTaskFxn,
                      OSA_THR_PRI_MAX, 
                      ACAP_CAPTURE_TSK_STACK_SIZE, 
                      &gAcapContextInt);
    }
    AUDIO_INFO_PRINT(("AUDIO: Audio_startCapture() success....\n"));
    
    for (ch=0; ch<pPrm->numChannels; ch++)
    {
        AUDIO_INFO_PRINT (("AUDIO: capPtr: %X <%d> encBuf: %X <%d> encOutBuf: %X <%d> encHdl: %X\n", 
                (UInt32)gAcapContextInt.chData[ch], gAcapContextInt.chDataMaxSize[ch],
                (UInt32)gEncBuf[ch], gEncBufMaxSize[ch],
                (UInt32)gIntEncodeBuffer[ch], gIntEncBufSize[ch],
                (UInt32)gEncHandle[ch]));
    }
    gAcapContextInt.captureStarted = TRUE;
    return  status;
}

Int32   Audio_stopCapture(Void)
{
    /* Only 5158 config supported now */
    if (gAcapContextInt.captureTaskActive == TRUE)
    {
        gAcapContextInt.taskExitFlag = TRUE;

        OSA_waitMsecs(100);
        OSA_thrDelete(&gAcapContextInt.captureThrHandle);
        Audio_deInitEncode(&gAcapContextInt.prm);

        OSA_waitMsecs(100);

        Audio_deInitCapture(gAcapContextInt.prm.numChannels, gChEmptyBufQue);
        Audio_deInitCapture(gAcapContextInt.prm.numChannels, gChPCMBufQue);
        Audio_deInitCaptureDevice();
        gAcapContextInt.captureTaskActive = FALSE;
        gAcapContextInt.captureStarted = FALSE;
    }
    return OSA_SOK;
}

Void Audio_enableCapChannel(Int32 chNum)
{
    if (chNum < gAcapContextInt.prm.numChannels)
    {
        gAcapContextInt.captureChannelMask |= (1 << chNum);
    }
}

extern Int32 putLine, getLine;

Void Audio_disableCapChannel(Int32 chNum)
{
    if (chNum < gAcapContextInt.prm.numChannels)
    {
        gAcapContextInt.captureChannelMask &= ~(1 << chNum);

        if (gAcapContextInt.captureStarted == TRUE)
        {
            Int32 status;

            if (OSA_queIsEmpty(&gChPCMBufQue[chNum]) == TRUE)
            {
                /* Put a dummy buffer to capture queue to unblock app threads waiting on getData() */
                status = OSA_quePut(&gChPCMBufQue[chNum], (Int32)NULL, OSA_TIMEOUT_NONE);
                OSA_assert(status == OSA_SOK);
            }
        }
    }
}

static
Void *Audio_captureTaskFxn(Void * prm)
{
    Audio_ContextInt  *ctx;
    UInt16            *captureBuf;
    Int32             len, err, buffering;
    Int32             readLen;  
    char                  threadName[128];

    snprintf(threadName,sizeof(threadName),"%s_%x",__func__,0);
    threadName[sizeof(threadName) - 1] = 0;
    OSA_printTID(threadName);


    ctx = prm;

    ctx->captureTaskActive = TRUE;

    while (!ctx->captureChannelMask)
    {
        usleep(1000);
        if (ctx->taskExitFlag == TRUE)
            break;
    }

    AUDIO_ERROR_PRINT(("\n\nAUDIO: STARTING AUDIO CAPTURE!!!!!\n"));

    if (Audio_InitCaptureDevice(gAcapContextInt.prm.captureDevice, gAcapContextInt.prm.numChannels, gAcapContextInt.prm.sampleRate) != ERROR_NONE)
    {
        AUDIO_ERROR_PRINT(("\n\nAUDIO: Capture -> device init failed....- capture will not happen!!!!!\n"));
       gAcapContextInt.taskExitFlag = TRUE; 

    }

    if ((gAcapContextInt.prm.enableTVP5158 == TRUE)
        &&
        (gAcapContextInt.prm.numChannels == 8))
    {
        readLen = (ACAP_MIN_FRAMES_2_READ_PER_CH * 2);
        gAcapContextInt.skipSample = 1;
    }
    else
    {
        readLen = ACAP_MIN_FRAMES_2_READ_PER_CH;
        gAcapContextInt.skipSample = 0;
    }

    while (ctx->taskExitFlag == FALSE)
    {
        buffering = 0;
        ctx->captureLen = 0;
        captureBuf = ctx->captureBuf;
        while (buffering < ACAP_BUFFERING_REQUIREMENT)
        {
            if (ctx->taskExitFlag == TRUE)
                break;

            len = readLen;
            err = Audio_captureData(captureBuf, &len);
            if (len > 0)
            {
                ctx->acapStats.totalCaptureLen += len;
                ctx->captureLen += len;
                captureBuf += (len * ctx->prm.numChannels);
                buffering++;
            }
            else
            {
#ifdef      REOPEN_CAPTURE_DEVICE_ON_ERROR
                Audio_deInitCaptureDevice();
                if (Audio_InitCaptureDevice(gAcapContextInt.prm.captureDevice, gAcapContextInt.prm.numChannels, gAcapContextInt.prm.sampleRate) != ERROR_NONE)
                {
                    AUDIO_ERROR_PRINT(("\n\nAUDIO: Capture -> device init failed....- capture will not happen!!!!!\n"));
                   gAcapContextInt.taskExitFlag = TRUE; 
                
                }
#else
                #ifdef  WRITE_ALL_DATA_TO_FILE
                AUDIO_ERROR_PRINT(("\n\nAUDIO: Capture -> capture error %s, waiting to exit !!!!!\n", snd_strerror(err)));
                while (ctx->taskExitFlag == FALSE)
                    usleep(100);
                #else
                AUDIO_ERROR_PRINT(("\n\nAUDIO: Capture -> capture error %s, Calling recover !!!!!\n", snd_strerror(err)));
                snd_pcm_recover(gAcapContextInt.alsa_handle, err, 1);
//                snd_pcm_prepare(gAcapContextInt.alsa_handle);
                #endif                
#endif
            }
            if (!(ctx->acapStats.totalCaptureLen % (20*16000)))
                AUDIO_INFO_PRINT(("%d: AUDIO: CAPTURE -> %d samples captured.. Err Ct %d, last Err %d <%s> / wr-%d : rd-%d : empty-%d : miss-%d\n", 
                    OSA_getCurTimeInMsec(),
                    ctx->acapStats.totalCaptureLen, 
                    ctx->acapStats.errorCnt,
                    ctx->acapStats.lastError,
                    snd_strerror(ctx->acapStats.lastError),
                    (UInt32)gChPCMBufsWritten[0], gChPCMBufsRead[0], gChPCMBufsEmptied[0], gChWriteBufMissCount[0]
                    ));
        }
        if ((ctx->captureLen) && (ctx->taskExitFlag == FALSE))
        {
#ifdef      WRITE_ALL_DATA_TO_FILE
            Audio_writeToFile(ctx->captureBuf, ctx->captureLen);
#else
            if (gAcapContextInt.prm.enableTVP5158 == TRUE)
                Audio_demux_tvp5158(ctx->captureBuf, ctx->captureLen);
            else
                Audio_demux_other(ctx->captureBuf, ctx->captureLen);
#endif
        }
    }
    AUDIO_ERROR_PRINT(("\n\nAUDIO: ENDING AUDIO CAPTURE!!!!!\n"));
    return NULL;
}

static 
Int32 Audio_InitCaptureDevice (Int8 *device, Int32 numChannels, UInt32 sampleRate)
{
    snd_pcm_hw_params_t *hw_params;
    Int32 err;
    snd_pcm_t       *alsa_handle;
    Int32 resample;
    static snd_pcm_uframes_t    frames;
    Int32 dir;

    if ((err = snd_pcm_open (&alsa_handle, device, SND_PCM_STREAM_CAPTURE, 0)) < 0)
    {
        AUDIO_ERROR_PRINT(("\n\nAUDIO >> Cannot open audio device %s (%s)\n", device, snd_strerror (err)));
        return  OSA_EFAIL;
    }

    if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0)
    {
        AUD_DEVICE_PRINT_ERROR_AND_RETURN("\n\nAUDIO >>  cannot allocate hardware parameter structure (%s)\n", err, alsa_handle);
    }

    if ((err = snd_pcm_hw_params_any (alsa_handle, hw_params)) < 0)
    {
        AUD_DEVICE_PRINT_ERROR_AND_RETURN("\n\nAUDIO >>  cannot initialize hardware parameter structure (%s)\n", err, alsa_handle);
    }

    if ((err = snd_pcm_hw_params_set_access (alsa_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
    {
        AUD_DEVICE_PRINT_ERROR_AND_RETURN("\n\nAUDIO >>  cannot set access type (%s)\n", err, alsa_handle);
    }

    if ((err = snd_pcm_hw_params_set_format (alsa_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0)
    {
        AUD_DEVICE_PRINT_ERROR_AND_RETURN("\n\nAUDIO >>  cannot set sample format (%s)\n", err, alsa_handle);
    }

    if ((err = snd_pcm_hw_params_set_rate_near (alsa_handle, hw_params, &sampleRate, 0)) < 0)
    {
        AUD_DEVICE_PRINT_ERROR_AND_RETURN("\n\nAUDIO >>  cannot set sample rate (%s)\n", err, alsa_handle);
    }

    if ((err = snd_pcm_hw_params_set_channels (alsa_handle, hw_params, numChannels)) < 0)
    {
        AUD_DEVICE_PRINT_ERROR_AND_RETURN("\n\nAUDIO >>  cannot set channel count (%s)\n", err, alsa_handle);
    }

    resample = 1;        
    snd_pcm_hw_params_set_rate_resample(alsa_handle, hw_params, resample);  
  
    frames = 1024;  
    err = snd_pcm_hw_params_set_period_size_near(alsa_handle, hw_params, &frames, &dir);        
    if (err < 0)        
    {            
       printf("AUDIO >> Unable to set period size %li err: %s\n", frames, snd_strerror(err));            
    }

    if ((err = snd_pcm_hw_params (alsa_handle, hw_params)) < 0)
    {
        AUD_DEVICE_PRINT_ERROR_AND_RETURN("\n\nAUDIO >>  cannot set parameters (%s)\n", err, alsa_handle);
    }

    snd_pcm_hw_params_free (hw_params);

    if ((err = snd_pcm_prepare (alsa_handle)) < 0)
    {
        AUD_DEVICE_PRINT_ERROR_AND_RETURN("\n\nAUDIO >>  cannot prepare audio interface for use (%s)\n", err, alsa_handle);
    }
    gAcapContextInt.alsa_handle = alsa_handle;
    AUDIO_ERROR_PRINT(("\n\nAUDIO: AUDIO CAPTURE DEVICE Init Done!!!!!\n"));
    usleep(100);
    return err;
}


static 
Int32    Audio_captureData(UInt16 *buffer, Int32 *numSamples)
{
    Int32 err = OSA_EFAIL;

    if (gAcapContextInt.alsa_handle)
    {
        if ((err = snd_pcm_readi (gAcapContextInt.alsa_handle, buffer, *numSamples)) != *numSamples)
        {
            *numSamples = 0;
            gAcapContextInt.acapStats.errorCnt++;
            gAcapContextInt.acapStats.lastError = err;

        }
        else
        {
            gAcapContextInt.acapStats.lastError = 0;
        }
    }
    else
    {
        *numSamples = 0;
    }
    return err;
}


static 
Int32 Audio_deInitCaptureDevice(Void)
{

    if (gAcapContextInt.alsa_handle)
    {
        snd_pcm_drain(gAcapContextInt.alsa_handle);
        snd_pcm_close(gAcapContextInt.alsa_handle);
        gAcapContextInt.alsa_handle = NULL;
        AUDIO_ERROR_PRINT(("AUDIO: Capture device deInit done....\n"));
    }
    AUDIO_ERROR_PRINT(("\n\nAUDIO: AUDIO CAPTURE DEVICE De_Init Done!!!!!\n"));
    return OSA_SOK;
}

Int32 Audio_doEncode (UInt8 chNum, Audio_getDataParams * pCapPrm)
{
    Int32   bytesGenerated = 0, iter = 0;
    Int32 remainingSamples = 0;

    if (gAcapContextInt.prm.chPrm[chNum].encodeParam.encoderType == AUDIO_CODEC_TYPE_AAC_LC)
    {
        Int32 outBufAvailable;
        Audio_EncodeProcessParams prm;
        UInt16                  *input;
        UInt8                   *output;
        UInt32                  prevTime;

        iter =  pCapPrm->captureDataSize / AUDIO_ENCODE_FRAME_SIZE_IN_SAMPLES;
        // TODO: to be handled!!!!    
        remainingSamples = pCapPrm->captureDataSize % AUDIO_ENCODE_FRAME_SIZE_IN_SAMPLES;

        gEncodeCallCount[chNum] += iter;
        prevTime = OSA_getCurTimeInMsec();
        
        input = (UInt16*) pCapPrm->captureDataBuf;
        output = gEncBuf[chNum];
        outBufAvailable = gEncBufMaxSize[chNum];

        /* Assuming that encode buffer will be sufficient */
        while (iter)
        {
            prm.inBuf.dataBuf = input; 
            prm.inBuf.dataBufSize = (AUDIO_ENCODE_FRAME_SIZE_IN_SAMPLES*ACAP_SAMPLE_LEN);
            /*
                        Cache_wbInv(prm.inBuf.dataBuf,
                            prm.inBuf.dataBufSize, Cache_Type_ALL, TRUE);
                    */
            prm.outBuf.dataBuf = gIntEncodeBuffer[chNum];
            prm.outBuf.dataBufSize = gIntEncBufSize[chNum];
            Audio_encode(gEncHandle[chNum], &prm);
            /** ?????? ****/
            if (prm.outBuf.dataBufSize == 0)
            {
                AUDIO_ERROR_PRINT (("AUDIO: ENC -> Encode failed [Enc:%d/ Failed: %d] - input %X, output %d, iter %d\n", 
                        gTotalFramesEncoded[chNum], gTotalFramesFailed[chNum],
                        (UInt32)input, prm.outBuf.dataBufSize, iter));
                gTotalFramesFailed[chNum] ++;
            }
            else
            {
                gTotalFramesEncoded[chNum] ++;
                if (outBufAvailable >= prm.outBuf.dataBufSize)
                {
                    memcpy(output, prm.outBuf.dataBuf, prm.outBuf.dataBufSize); 
                    output += prm.outBuf.dataBufSize;
                    outBufAvailable -= prm.outBuf.dataBufSize;
                    bytesGenerated += prm.outBuf.dataBufSize;
                }
                else
                {
                    break;
                }
            }
            input += AUDIO_ENCODE_FRAME_SIZE_IN_SAMPLES;
            iter--;
        }
        gEncodeTimeSpent[chNum] += (OSA_getCurTimeInMsec() - prevTime);

    }
    else if (gAcapContextInt.prm.chPrm[chNum].encodeParam.encoderType == AUDIO_CODEC_TYPE_G711)
    {
        Audio_EncodeProcessParams     prm;

        prm.inBuf.dataBuf = pCapPrm->captureDataBuf; 
        prm.inBuf.dataBufSize = pCapPrm->captureDataSize * ACAP_SAMPLE_LEN;
        prm.outBuf.dataBuf = gEncBuf[chNum];
        prm.outBuf.dataBufSize = gEncBufMaxSize[chNum];

        Audio_encode(gEncHandle[chNum], &prm);
        bytesGenerated += prm.outBuf.dataBufSize;
    }
    pCapPrm->encodeDataBuf = gEncBuf[chNum];
    pCapPrm->encodeDataSize = bytesGenerated;
    return OSA_SOK;
}

Int32 Audio_getChEncodeStats (Int32 chNum, Int32 *totalEncCalls, UInt32 *totalTimeInMS)
{
    if (totalEncCalls && totalTimeInMS)
    {
        *totalTimeInMS = gEncodeTimeSpent[chNum];
        *totalEncCalls = gEncodeCallCount[chNum];
    }
    return 0;
}

Int32 Audio_printChStats (UInt8 chNum)
{
    AUDIO_ERROR_PRINT(("\r\n ----------- AUDIO: Stats -----------"));
    AUDIO_ERROR_PRINT(("\r\n CH bufs wr: %d, rd: %d, empty: %d, missed: %d", 
        gChPCMBufsWritten[chNum], gChPCMBufsRead[chNum], gChPCMBufsEmptied[chNum], gChWriteBufMissCount[chNum]));
    AUDIO_ERROR_PRINT(("\r\n CH Que Stat PCM Q: %d, Empty (Read) Q: %d", 
        OSA_queGetQueuedCount(&gChPCMBufQue[chNum]), OSA_queGetQueuedCount(&gChEmptyBufQue[chNum])));
    return OSA_SOK;
}


Int32 Audio_getCapChData(UInt8 chNum, Audio_getDataParams *pPrm)
{
	Int32 status;
	
    if ((gAcapContextInt.taskExitFlag == FALSE) && (gAcapContextInt.captureChannelMask & (1 << chNum)))
    {
        pPrm->captureDataBuf = NULL;
        pPrm->captureDataSize = 0;
        status = OSA_queGet(&gChPCMBufQue[chNum], (Int32 *)(&pPrm->captureDataBuf), OSA_TIMEOUT_NONE);
        
        if ((gAcapContextInt.taskExitFlag == TRUE) ||
            (!(gAcapContextInt.captureChannelMask & (1 << chNum))) ||
            (pPrm->captureDataBuf == NULL))
            goto AUDIO_FAILURE;

        if (pPrm->captureDataBuf)
        {
            pPrm->captureDataSize = gChBufMaxSize[chNum];
	        gChPCMBufsRead[chNum] += 1;
            if (gAcapContextInt.prm.chPrm[chNum].enableEncode)
            {
                Audio_doEncode(chNum, pPrm);
            }
        }
        else
        {
            goto AUDIO_FAILURE;
        }
            
        return OSA_SOK;
    }
AUDIO_FAILURE:
    pPrm->captureDataBuf = NULL;
    pPrm->captureDataSize = 0;
    pPrm->encodeDataBuf = NULL;
    pPrm->encodeDataSize = 0;
    return OSA_EFAIL;
}

/**
    \brief     Set data consumed info 

    \param  chNum [IN]  channel number for which data is already consumed 
    \param   pPrm [IN]  Channel data pointers already given via Audio_getCapChData()
 
    \return 0 on success
 */
Int32 Audio_setCapConsumedChData(UInt8 chNum, Audio_getDataParams *pPrm)
{
    Int32 status;

    if (pPrm)
    {
        if (pPrm->captureDataBuf)
        {    
            gChPCMBufsEmptied[chNum]++;
            status = OSA_quePut(&gChEmptyBufQue[chNum], (Int32)pPrm->captureDataBuf, OSA_TIMEOUT_NONE);
            OSA_assert(status == OSA_SOK);
        }
    }
	return OSA_SOK;
}
                              

Void Audio_demux_tvp5158(UInt16 *captureBuf, Int32 captureSize)
{
    Int32 stored, ch, status;
    stored = 0;
   
    for (ch=0; ch<gAcapContextInt.prm.numChannels; ch++)
    {
        if (gAcapContextInt.captureChannelMask & (1 << ch))
        {
            if (gChWriteBuf[ch] == NULL)
            {
                status = OSA_queGet(&gChEmptyBufQue[ch], (Int32 *)(&gChWriteBuf[ch]), OSA_TIMEOUT_NONE);
            }
        }
    }

    while (stored < captureSize)
    {
        for (ch=0; ch<gAcapContextInt.prm.numChannels; ch++)
        {
            if ((gAcapContextInt.captureChannelMask & (1 << ch)) && gChWriteBuf[ch])
            {
                if (gChWriteBuf[ch] == NULL)
                {
                    status = OSA_queGet(&gChEmptyBufQue[ch], (Int32 *)(&gChWriteBuf[ch]), OSA_TIMEOUT_NONE);
                    if (gChWriteBuf[ch] == NULL)
                    {
                        /* No empty buffer available, this chunk of channel data will not be given to app */
                        gChWriteBufMissCount[ch]++;
                        continue;
                    }
                }

                *(gChWriteBuf[ch] + gChWrIdx[ch]) = 
                    *(captureBuf + gAcapContextInt.audioPhyToDataIndexMap[ch]);

                gChWrIdx[ch] ++;
                if (gChWrIdx[ch] >= gChBufMaxSize[ch])
                {
                    gChPCMBufsWritten[ch] += 1;
                    status = OSA_quePut(&gChPCMBufQue[ch], (Int32)gChWriteBuf[ch], OSA_TIMEOUT_NONE);
                    OSA_assert(status == OSA_SOK);
                    
                    /* No write buffer available now. The individual ch data size provided to this function should fit in a single chWriteBuf. 
                                       * So, its ok to get an empty buf in next call to this function.
                                       */
                    gChWriteBuf[ch] = NULL;
                    gChWrIdx[ch] = 0;
                }
            }
        }
        captureBuf += (gAcapContextInt.prm.numChannels * (gAcapContextInt.skipSample + 1));
        stored += (gAcapContextInt.skipSample + 1);
    }
}

Void Audio_demux_other(UInt16 *captureBuf, Int32 captureSize)
{
    Int32 stored, ch, samplesCaptured = 0, status;
    stored = 0;
   
    for (ch=0; ch<gAcapContextInt.prm.numChannels; ch++)
    {
        if (gAcapContextInt.captureChannelMask & (1 << ch))
        {
            if (gChWriteBuf[ch] == NULL)
            {
                status = OSA_queGet(&gChEmptyBufQue[ch], (Int32 *)(&gChWriteBuf[ch]), OSA_TIMEOUT_NONE);
            }
        }
    }

    while (stored < captureSize)
    {
        for (ch=0; ch<gAcapContextInt.prm.numChannels; ch++)
        {
            if ((gAcapContextInt.captureChannelMask & (1 << ch)) && gChWriteBuf[ch])
            {
                if (gChWriteBuf[ch] == NULL)
                {
                    status = OSA_queGet(&gChEmptyBufQue[ch], (Int32 *)(&gChWriteBuf[ch]), OSA_TIMEOUT_NONE);
                    if (gChWriteBuf[ch] == NULL)
                    {
                        /* No empty buffer available, this chunk of channel data will not be given to app */
                        gChWriteBufMissCount[ch]++;
                    }
                }

                *(gChWriteBuf[ch] + gChWrIdx[ch]) = 
                    *(captureBuf + ch);

                gChWrIdx[ch] ++;
                if (gChWrIdx[ch] >= gChBufMaxSize[ch])
                {
                    gChPCMBufsWritten[ch] += 1;
                    status = OSA_quePut(&gChPCMBufQue[ch], (Int32)gChWriteBuf[ch], OSA_TIMEOUT_NONE);
                    OSA_assert(status == OSA_SOK);
                    
                    /* No write buffer available now. The individual ch data size provided to this function should fit in a single chWriteBuf. 
                                       * So, its ok to get an empty buf in next call to this function.
                                       */
                    gChWriteBuf[ch] = NULL;
                    gChWrIdx[ch] = 0;
                }
            }
            samplesCaptured++;
        }
        captureBuf += gAcapContextInt.prm.numChannels;
        stored ++;
    }
}



