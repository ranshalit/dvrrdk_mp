/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/
#include <stddef.h>
#include <stdlib.h>
#include "ipcBitsOutLink_priv.h"
#include <ti/syslink/utils/IHeap.h>
#include <ti/syslink/utils/Memory.h>
#include <ti/syslink/utils/Cache.h>
#include <ti/syslink/ProcMgr.h>
#include <mcfw/interfaces/link_api/avsync_hlos.h>

/* Ensure that each buffer returned is cache aligned; though filledLen can be any value */
#define RING_BUF_ALIGN_SIZE     128   


static
Void  IpcBitsOutLink_printBufferStats(IpcBitsOutLink_Obj * pObj)
{
    Int i;

    printf("\r\n IPCBITSOUTLINK:Buffer Statistics");
    printf("\r\n Num Channels:%d",pObj->numChn);
    printf("\r\n ChId | TotalBufCnt | FreeBufCnt | BufSize | AppAllocCount");
    for (i = 0; i < pObj->numChn;i++)
    {
        printf("\r\n %7d|%13d|%11d|%8d|%13d",
               i,
               pObj->queDepth[i],
               OSA_queGetQueuedCount(&pObj->listElemQue[i]),
               pObj->bitBufSize[i],
               pObj->appAllocBufCnt[i]);
    }
    printf("\n");
}

static
Int32 IpcBitsOutLink_putEmptyBufs(IpcBitsOutLink_Obj * pObj,
                                  Bitstream_BufList * pBufList);



IpcBitsOutLink_Obj gIpcBitsOutLink_obj[IPC_BITS_OUT_LINK_OBJ_MAX];

Void IpcBitsOutLink_notifyCb(OSA_TskHndl * pTsk)
{
    OSA_tskSendMsg(pTsk, NULL, SYSTEM_IPC_CMD_RELEASE_FRAMES, NULL, 0);
}

Void *IpcBitsOutLink_periodicTaskFxn(Void * prm)
{
    IpcBitsOutLink_Obj *pObj = (IpcBitsOutLink_Obj *) prm;
    Int32 status;
    UInt32 printWarnCounter = 0;
    char threadName[128];

    snprintf(threadName,sizeof(threadName),"%s_%x",__func__,pObj->tskId);
    threadName[sizeof(threadName) - 1] = 0;
    OSA_printTID(threadName);

    while (FALSE == pObj->prd.exitThread)
    {
        OSA_waitMsecs(IPC_BITS_OUT_PROCESS_PERIOD_MS);
        OSA_mutexLock(&pObj->apiMutex);
        if (pObj->pDeleteBufMsg)
        {
            UInt32 cmd;
            IpcBitsOutHLOSLink_deleteChBufParams *bufDelPrms;
            UInt32 chId;

            cmd = OSA_msgGetCmd(pObj->pDeleteBufMsg);
            bufDelPrms = OSA_msgGetPrm(pObj->pDeleteBufMsg);
            OSA_assert(IPCBITSOUT_LINK_CMD_DELETE_CH_BUFFER == cmd);
            chId = bufDelPrms->chId;
            OSA_assert((pObj->bitBufState[chId] ==
                        IPCBITSOUTHLOS_BUFPOOL_STATE_DELETEINPROGRESS_FLUSH_DONE)
                        ||
                        (pObj->bitBufState[chId] ==
                        IPCBITSOUTHLOS_BUFPOOL_STATE_DELETEINPROGRESS_WAIT_APP_BUF_FREE));
            if (pObj->bitBufState[chId] ==
                IPCBITSOUTHLOS_BUFPOOL_STATE_DELETEINPROGRESS_FLUSH_DONE)
            {
                if (pObj->createArgs.baseCreateParams.noNotifyMode)
                {
                    if (pObj->prd.numPendingCmd < IPC_BITSOUT_MAX_PENDING_RELEASE_FRAMES_CMDS)
                    {
                        pObj->prd.numPendingCmd++;
                        status = OSA_tskSendMsg(&pObj->tsk , NULL, SYSTEM_IPC_CMD_RELEASE_FRAMES,NULL,0);
                    }
                    else
                    {
                        UInt32 curTime = OSA_getCurTimeInMsec();

                        OSA_printf("IPC_BITSOUTLINK:!WARNING!.Commands not being processed by link."
                                   "TimeSinceLastMsgProcess:%d",
                                   (curTime - pObj->delMsgReceiveTime));
                    }
                }
            }
            if ((OSA_getCurTimeInMsec() - pObj->delMsgReceiveTime) >
                IPC_BITSOUT_LINK_DELBUF_CMD_PENDING_WARNING_THRESHOLD_MS)
            {
                if ((printWarnCounter % IPC_BITSOUT_STATS_WARN_INTERVAL) == 0)
                {
                    OSA_printf("IPC_BITSOUTLINK:!WARNING!.IPCBITSOUT_LINK_CMD_DELETE_CH_BUFFER pending for [%d] ms"
                               " DeleteChBufState:%d",
                               (OSA_getCurTimeInMsec() - pObj->delMsgReceiveTime),
                               pObj->bitBufState[chId]);
                }
                printWarnCounter++;
            }
        }
        else
        {
            printWarnCounter = 0;
        }
        OSA_mutexUnlock(&pObj->apiMutex);
    }
    return NULL;
}


static
Int32 IpcBitsOutLink_createPrdObj(IpcBitsOutLink_Obj * pObj)
{
    pObj->prd.numPendingCmd = 0;
    pObj->prd.exitThread = FALSE;
    OSA_thrCreate(&pObj->prd.thrHandle,
                  IpcBitsOutLink_periodicTaskFxn,
                  IPC_LINK_TSK_PRI, IPC_LINK_TSK_STACK_SIZE, pObj);
    return IPC_BITSOUT_LINK_S_SUCCESS;
}

static
Int32 IpcBitsOutLink_deletePrdObj(IpcBitsOutLink_Obj * pObj)
{
    pObj->prd.exitThread = TRUE;
    OSA_thrDelete(&pObj->prd.thrHandle);
    pObj->prd.numPendingCmd = 0;
    return IPC_BITSOUT_LINK_S_SUCCESS;
}

static
Void  ipcbitsoutlink_init_flush_frame_info(Bitstream_Buf *buf,
                                           UInt32 chId)
{
    memset(buf,0,sizeof(*buf));
    buf->addr = NULL;
    buf->phyAddr  = 0;
    buf->bufSize = 0;
    buf->fillLength = 0;
    buf->allocPoolID = IPC_BITSOUT_INVALID_ALLOC_POOL_ID;
    buf->channelNum  = chId;
    buf->flushFrame = TRUE;

}


static
Void ipcbitsoutlink_send_flush_frame(IpcBitsOutLink_Obj * pObj,
                                      UInt32 chId)
{
    SystemIpcBits_ListElem *pListElem;
    Int32 status;

    IPCBITSOUTLINK_INFO_LOG(pObj->tskId,"Flush Frame Sent for chId[%d]",chId);
    OSA_assert(chId < pObj->numChn);
    pObj->bitBufState[chId] =
                            IPCBITSOUTHLOS_BUFPOOL_STATE_DELETEINPROGRESS_FLUSH_DONE;
    status = OSA_queGet(&pObj->listElemFreeQue,(Int32 *)&pListElem,OSA_TIMEOUT_NONE);
    OSA_assert(OSA_SOK == status);
    ipcbitsoutlink_init_flush_frame_info(&pListElem->bitBuf,chId);
    SYSTEM_IPC_BITS_SET_BUFSTATE(pListElem->bufState, IPC_BITBUF_STATE_OUTQUE);
    status = ListMP_putTail(pObj->listMPOutHndl, (ListMP_Elem *) pListElem);
    OSA_assert(status == ListMP_S_SUCCESS);
}

static Int IpcBitsOutLink_start(IpcBitsOutLink_Obj * pObj)
{
    pObj->startProcessing = TRUE;
    return IPC_BITSOUT_LINK_S_SUCCESS;
}

static Int IpcBitsOutLink_stop(IpcBitsOutLink_Obj * pObj)
{
    pObj->startProcessing = FALSE;
    return IPC_BITSOUT_LINK_S_SUCCESS;
}

static Void IpcBitsOutLink_initStats(IpcBitsOutLink_Obj * pObj)
{
    memset(&pObj->stats, 0, sizeof(pObj->stats));
}

static Ptr IpcBitsOutLink_MapUsrVirt2Phy(Ptr usrVirt)
{
    Ptr phyAddr = 0;

    phyAddr =  Memory_translate (usrVirt,
                                 Memory_XltFlags_Virt2Phys);

    OSA_printf("IPCBITSOUTLINK:Translated Addr Virt:%p To Phy:%p",
                usrVirt,phyAddr);
    return phyAddr;
}

Void IpcBitsOutLink_createFreeListElemQue(IpcBitsOutLink_Obj *pObj)
{
    Int status;
    Int i;

    status = OSA_queCreate(&pObj->listElemFreeQue,
                           OSA_ARRAYSIZE(pObj->listElem));
    OSA_assert(status == OSA_SOK);
    for (i = 0; i < OSA_ARRAYSIZE(pObj->listElem);i++)
    {
        status = OSA_quePut(&pObj->listElemFreeQue,
                            (Int32)pObj->listElem[i],
                            OSA_TIMEOUT_NONE);
        OSA_assert(status == OSA_SOK);
    }
}


Void IpcBitsOutLink_deleteFreeListElemQue(IpcBitsOutLink_Obj *pObj)
{
    Int status;

    status = OSA_queDelete(&pObj->listElemFreeQue);
    OSA_assert(status == OSA_SOK);
}


static Int IpcBitsOutLink_createOutObj(IpcBitsOutLink_Obj * pObj)
{
    Int status = OSA_SOK;
    Int32 chId, elemId, bufId;
    IHeap_Handle srBitBufHeapHandle;
    UInt32 bufSize, numBufs, totBufSize, cacheLineSize;
    const UInt32 srIndex = SYSTEM_IPC_SR_CACHED;

    elemId = 0;
    srBitBufHeapHandle = System_ipcGetSRHeap(srIndex);
    OSA_assert(srBitBufHeapHandle != NULL);
    cacheLineSize = SharedRegion_getCacheLineSize(srIndex);

    IpcBitsOutLink_createFreeListElemQue(pObj);
    pObj->numChn = pObj->createArgs.inQueInfo.numCh;

    for (chId = 0; chId < pObj->numChn; chId++)
    {
        status = OSA_queCreate(&pObj->listElemQue[chId],
                               SYSTEM_IPC_BITS_MAX_LIST_ELEM);
        OSA_assert(status == OSA_SOK);

        bufSize =
            OSA_align(pObj->createArgs.chMaxReqBufSize[chId], cacheLineSize);
        numBufs = pObj->createArgs.maxQueueDepth[chId];

        totBufSize = OSA_align(pObj->createArgs.totalBitStreamBufferSize [chId], cacheLineSize);

        pObj->bitBufBasePtr[chId] =
            Memory_alloc(srBitBufHeapHandle, totBufSize, cacheLineSize, NULL);
        OSA_assert(pObj->bitBufBasePtr[chId] != NULL);
        OSA_printf("IPC_BITSOUT:BitBuffer Alloc.ChID:%d,Size:0x%X",
                    chId,totBufSize);

        status = RingBufferInit(&pObj->ringBufHdnl[chId], pObj->bitBufBasePtr[chId], totBufSize);
        OSA_assert(status == OSA_SOK);

        pObj->bitBufSize[chId] = bufSize;
        pObj->queDepth[chId] = numBufs;
        pObj->bitBufTotalSize[chId] = totBufSize;
        pObj->appAllocBufCnt[chId] = 0;

        pObj->bitBufPhyAddr[chId] = IpcBitsOutLink_MapUsrVirt2Phy(pObj->bitBufBasePtr[chId]);

        OSA_printf ("\n###Bit buff of size from the SR # %d : %d / bufsize %d, maxQueueDepth %d \n", srIndex, totBufSize, bufSize, numBufs);
        OSA_printf ("###Bit buff BasePtr: %X / PhyAddr %X\n", (UInt32) pObj->bitBufBasePtr[chId], (UInt32) pObj->bitBufPhyAddr[chId]);

        for (bufId = 0; bufId < numBufs; bufId++)
        {
            SystemIpcBits_ListElem *listElem;

            OSA_assert(elemId < SYSTEM_IPC_BITS_MAX_LIST_ELEM);
            status = OSA_queGet(&pObj->listElemFreeQue,(Int32 *)&listElem,OSA_TIMEOUT_NONE);
            OSA_assert(OSA_SOK == status);
            elemId++;
            SYSTEM_IPC_BITS_SET_BUFOWNERPROCID(listElem->bufState);
            SYSTEM_IPC_BITS_SET_BUFSTATE(listElem->bufState,
                                         IPC_BITBUF_STATE_FREE);


            listElem->bitBuf.allocPoolID = chId;
            listElem->bitBuf.bufSize = bufSize;
            listElem->bitBuf.fillLength = 0;
            listElem->bitBuf.mvDataFilledSize = 0;
            listElem->bitBuf.temporalId = 0;
            listElem->bitBuf.numTemporalLayerSetInCodec = 0;
            listElem->bitBuf.startOffset = 0;
            listElem->bitBuf.bottomFieldBitBufSize = 0;
            listElem->bitBuf.doNotDisplay = FALSE;
            listElem->bitBuf.inputFileChanged = FALSE;
            status =
                OSA_quePut(&pObj->listElemQue[chId], (Int32) listElem,
                           OSA_TIMEOUT_NONE);
            OSA_assert(status == OSA_SOK);
        }
        pObj->bitBufState[chId] = IPCBITSOUTHLOS_BUFPOOL_STATE_CREATED;
    }
    return status;
}

static Int IpcBitsOutLink_deleteOutObj(IpcBitsOutLink_Obj * pObj)
{
    Int status = OSA_SOK;
    Int32 chId;
    IHeap_Handle srBitBufHeapHandle;
    const UInt32 srIndex = SYSTEM_IPC_SR_CACHED;

    srBitBufHeapHandle = System_ipcGetSRHeap(srIndex);
    OSA_assert(srBitBufHeapHandle != NULL);
    for (chId = 0; chId < pObj->numChn; chId++)
    {
        if (pObj->bitBufState[chId] == IPCBITSOUTHLOS_BUFPOOL_STATE_CREATED)
        {
            status = OSA_queDelete(&pObj->listElemQue[chId]);
            OSA_assert(status == OSA_SOK);
            OSA_assert(pObj->bitBufBasePtr[chId] != NULL);
            OSA_assert(pObj->bitBufTotalSize[chId] != 0);
            Memory_free(srBitBufHeapHandle,
                        pObj->bitBufBasePtr[chId], pObj->bitBufTotalSize[chId]);
            OSA_printf("IPC_BITSOUT:BitBuffer Free.chID:%d,Size:0x%X",
                        chId,pObj->bitBufTotalSize[chId]);
            pObj->bitBufBasePtr[chId] = NULL;
            pObj->bitBufTotalSize[chId] = 0;
            RingBufferDeInit(&pObj->ringBufHdnl[chId]);
        }
        else
        {
            OSA_assert(pObj->bitBufState[chId] == IPCBITSOUTHLOS_BUFPOOL_STATE_DELETED);
            OSA_assert(pObj->bitBufBasePtr[chId] == NULL);
            OSA_assert(pObj->bitBufTotalSize[chId] == 0);
        }
    }
    IpcBitsOutLink_deleteFreeListElemQue(pObj);
    return status;
}

Int32 IpcBitsOutLink_create(IpcBitsOutLink_Obj * pObj,
                            IpcBitsOutLinkHLOS_CreateParams * pPrm)
{
    Int32 status;
    UInt32 i;

#ifdef SYSTEM_DEBUG_IPC
    OSA_printf(" %d: IPC_BITS_OUT   : Create in progress !!!\n",
               OSA_getCurTimeInMsec());
#endif

    memcpy(&pObj->createArgs, pPrm, sizeof(pObj->createArgs));

    for (i=0; i<IPC_LINK_BITS_OUT_MAX_NUM_CHANNELS; i++)
    {
        if(pObj->createArgs.maxQueueDepth[i] == 0)
            pObj->createArgs.maxQueueDepth[i] =
                  IPC_BITS_OUT_MAX_BUFFERS_IN_QUEUE;
    }

    status = System_ipcListMPReset(pObj->listMPOutHndl, pObj->listMPInHndl);
    OSA_assert(status == OSA_SOK);

    IpcBitsOutLink_createOutObj(pObj);

    IpcBitsOutLink_initStats(pObj);

    pObj->startProcessing = FALSE;
    pObj->pDeleteBufMsg   = NULL;

    IpcBitsOutLink_createPrdObj(pObj);
#ifdef SYSTEM_DEBUG_IPC
    OSA_printf(" %d: IPC_BITS_OUT   : Create Done !!!\n",
               OSA_getCurTimeInMsec());
#endif

    return IPC_BITSOUT_LINK_S_SUCCESS;
}

Int32 IpcBitsOutLink_delete(IpcBitsOutLink_Obj * pObj)
{
    Int32 status = IPC_BITSOUT_LINK_S_SUCCESS;
#ifdef SYSTEM_DEBUG_IPC
    OSA_printf(" %d: IPC_BITS_OUT   : Delete in progress !!!\n",
               OSA_getCurTimeInMsec());
#endif
    IpcBitsOutLink_deletePrdObj(pObj);
    OSA_assert(NULL == pObj->pDeleteBufMsg);
    IPCBITSOUTLINK_INFO_LOG(pObj->tskId,
                            "RECV:%d\tFREE:%d,DROPPED:%d,AVGLATENCY:%d",
                            pObj->stats.recvCount,
                            pObj->stats.freeCount,
                            pObj->stats.droppedCount,
                    OSA_DIV(pObj->stats.totalRoundTrip ,
                             pObj->stats.freeCount));

    IpcBitsOutLink_deleteOutObj(pObj);
    status = System_ipcListMPReset(pObj->listMPOutHndl, pObj->listMPInHndl);
    OSA_assert(status == OSA_SOK);
#ifdef SYSTEM_DEBUG_IPC
    OSA_printf(" %d: IPC_BITS_OUT   : Delete Done !!!\n",
               OSA_getCurTimeInMsec());
#endif

    return IPC_BITSOUT_LINK_S_SUCCESS;
}

static
Int32 IpcBitsOutLink_doPrePutCacheOp(IpcBitsOutLink_Obj * pObj,
                                     SystemIpcBits_ListElem * pListElem)
{
    if (pListElem->bitBuf.fillLength)
    {
        Cache_wbInv(pListElem->bitBuf.addr,
                    pListElem->bitBuf.fillLength, Cache_Type_ALL, TRUE);
    }
    /* No cache ops done since pListElem is allocated from non-cached memory */
    UTILS_assert(SharedRegion_isCacheEnabled(SharedRegion_getId(pListElem)) ==
                 FALSE);
    return IPC_BITSOUT_LINK_S_SUCCESS;
}

static
Int32 IpcBitsOutLink_doPostGetCacheOp(IpcBitsOutLink_Obj * pObj,
                                      SystemIpcBits_ListElem * pListElem)
{
    /* No cache ops done since pListElem is allocated from non-cached memory */
    UTILS_assert(SharedRegion_isCacheEnabled(SharedRegion_getId(pListElem)) ==
                 FALSE);
    return IPC_BITSOUT_LINK_S_SUCCESS;
}

static
Int32 IpcBitsOutLink_listMPPut(IpcBitsOutLink_Obj * pObj,
                               SystemIpcBits_ListElem * pListElem)
{
    Int32 status = IPC_BITSOUT_LINK_S_SUCCESS;

    SYSTEM_IPC_BITS_SET_BUFSTATE(pListElem->bufState, IPC_BITBUF_STATE_OUTQUE);
    IpcBitsOutLink_doPrePutCacheOp(pObj, pListElem);
    status = ListMP_putTail(pObj->listMPOutHndl, (ListMP_Elem *) pListElem);
    OSA_assert(status == ListMP_S_SUCCESS);
    return IPC_BITSOUT_LINK_S_SUCCESS;
}



static
Int32 IpcBitsOutLink_getEmptyBufs(IpcBitsOutLink_Obj * pObj,
                                  Bitstream_BufList * pBufList,
                                  IpcBitsOutLinkHLOS_BitstreamBufReqInfo *reqInfo)
{
    Int32 status = IPC_BITSOUT_LINK_S_SUCCESS;
    Int chNum, bufId, bufs;
    SystemIpcBits_ListElem *pListElem;

    bufId = 0;
    bufs = 0;

    chNum = reqInfo->chNum;
    pBufList->bufs[bufId] = NULL;
   
    if (IPCBITSOUTHLOS_BUFPOOL_STATE_CREATED == pObj->bitBufState[chNum])
    {
        Void *writePtr = NULL; /* some non-NULL value */

        OSA_assert(reqInfo->bufSize > 0);
        reqInfo->bufSize = OSA_align(reqInfo->bufSize, RING_BUF_ALIGN_SIZE);

        writePtr = RingBufferWriterAcquire(&pObj->ringBufHdnl[chNum], 
                            reqInfo->bufSize);
        if (writePtr)
        {
            status =
                    OSA_queGet(&pObj->listElemQue[chNum],
                               (Int32 *) (&pListElem), OSA_TIMEOUT_NONE);
            if (status == OSA_SOK)
            {
                OSA_assert(pListElem != NULL);
                pBufList->bufs[bufId] = &pListElem->bitBuf;
                pBufList->bufs[bufId]->bufSize = reqInfo->bufSize;
                pBufList->bufs[bufId]->fillLength = 0;
                pBufList->bufs[bufId]->inputFileChanged = FALSE;
                pBufList->bufs[bufId]->mvDataFilledSize = 0;
                pBufList->bufs[bufId]->numTemporalLayerSetInCodec = 0;
                pBufList->bufs[bufId]->temporalId = 0;
                pBufList->bufs[bufId]->bottomFieldBitBufSize = 0;
                pBufList->bufs[bufId]->channelNum= chNum;
                pBufList->bufs[bufId]->seqId = SYSTEM_DISPLAY_SEQID_DEFAULT;
                pBufList->bufs[bufId]->addr = writePtr;
                pListElem->srBufPtr = SharedRegion_getSRPtr(writePtr,
                                           SYSTEM_IPC_SR_CACHED);

                if (pObj->bitBufPhyAddr[chNum])
                {
                    pBufList->bufs[bufId]->phyAddr =
                        (UInt32) ((UInt32) (pObj->bitBufPhyAddr[chNum]) +  
                                (((UInt32)writePtr)) - ((UInt32)pObj->bitBufBasePtr[chNum]) );
                }
                pBufList->bufs[bufId]->flushFrame = FALSE;
                bufs++;
                pObj->appAllocBufCnt[chNum] += 1;
            }
            else
            {
                /* release the ring buffer */
                RingBufferWriterCancel(&pObj->ringBufHdnl[chNum]);
            }
        }
    }
    else
    {
        #ifdef DEBUG_IPC_BITS
        OSA_printf("IPCBITSOUT:!!!WARNING.!!! Requested CH buffer not created:[%d]",
                   chNum);
        #endif
    }

    pBufList->numBufs = bufs;
    if (pBufList->numBufs == 0) {
        pObj->stats.numNoFreeBufCount++;
        if ((pObj->stats.numNoFreeBufCount % IPC_BITSOUT_STATS_WARN_INTERVAL) == 0)
        {
            #ifdef DEBUG_IPC_BITS
            OSA_printf("IPCBITSOUT:!!!WARNING.!!! NO FREE BUF AVAILABLE. OCCURENCE COUNT:[%d]",
                       pObj->stats.numNoFreeBufCount);
            #endif
        }
    }
    return IPC_BITSOUT_LINK_S_SUCCESS;
}


static
Void  IpcBitsOutLink_logTs(Bitstream_Buf *pBitBuf)
{
#ifdef SYSTEM_DEBUG_AVSYNC_DETAILED_LOGS
    UInt64 ts;

    ts = pBitBuf->upperTimeStamp & 0xFFFFFFFF;
    ts <<= 32;
    ts  |= pBitBuf->lowerTimeStamp & 0xFFFFFFFF;
    AvsyncLink_logIpcBitsOutTS(pBitBuf->channelNum,ts);
#else
    (Void)pBitBuf;
#endif
}


static
Int32 IpcBitsOutLink_putFullBufs(IpcBitsOutLink_Obj *pObj,
                                 Bitstream_BufList *pBufList)
{
    SystemIpcBits_ListElem *pListElem;
    Bitstream_Buf *pBitBuf;
    Bitstream_BufList freeBitBufList;
    Bool putDone = FALSE;
    Int32 bufId;
    Bool sendFlushFrame = FALSE;
    UInt32 flushFrameChId = ~(0u);

    freeBitBufList.numBufs = 0;
    for (bufId = 0; bufId < pBufList->numBufs; bufId++)
    {
        pBitBuf = pBufList->bufs[bufId];
        if (IPCBITSOUTHLOS_BUFPOOL_STATE_CREATED == pObj->bitBufState[pBitBuf->channelNum])
        {
            IpcBitsOutLink_logTs(pBitBuf);
            OSA_assert(pObj->appAllocBufCnt[pBitBuf->channelNum] > 0);
            pObj->appAllocBufCnt[pBitBuf->channelNum] -= 1;
            /* Atmost 1 buffer is allowed per channel. Assert if not */
            OSA_assert(pObj->appAllocBufCnt[pBitBuf->channelNum] == 0);
            /* Application must not set flush frame to false.
                        * Flush frame is only used internally to delete buffer pool
                        */
            pBitBuf->flushFrame = FALSE;
            pListElem = (SystemIpcBits_ListElem *)pBitBuf;
            OSA_assert(SharedRegion_getPtr(pListElem->srBufPtr) ==
                       pBitBuf->addr);
            if (0 == pBitBuf->fillLength)
            {
                /* filled length of 0 indicates application
                               * did not fill any data in this buffer.
                               * Free it immediately */
#if SYSTEM_DEBUG_IPC_RT
                OSA_printf(" IPC_OUT: Dropping bitbuf\n");
 #endif
                OSA_assert(freeBitBufList.numBufs <
                           VIDBITSTREAM_MAX_BITSTREAM_BUFS);
                freeBitBufList.bufs[freeBitBufList.numBufs] = pBitBuf;
                freeBitBufList.numBufs++;
                pObj->stats.droppedCount++;
                continue;
            }
            else
            {
				Int32 len;

                pObj->stats.recvCount++;
                OSA_assert(SYSTEM_IPC_BITS_GET_BUFSTATE(pListElem->bufState)
                           == IPC_BITBUF_STATE_FREE);
                OSA_assert(SYSTEM_IPC_BITS_GET_BUFOWNERPROCID(pListElem->bufState)
                           == System_getSelfProcId());
                SYSTEM_IPC_BITS_SET_BUFSTATE(pListElem->bufState,
                                             IPC_BITBUF_STATE_ALLOCED);
                IpcBitsOutLink_listMPPut(pObj, pListElem);
                len = OSA_align(pBitBuf->fillLength, RING_BUF_ALIGN_SIZE);

                RingBufferWriterRelease(
                            &pObj->ringBufHdnl[pBitBuf->allocPoolID], 
                            len);
                RingBufferWriterCancel(&pObj->ringBufHdnl[pBitBuf->allocPoolID]);
                putDone = TRUE;
            }
        }
        else
        {
            if (IPCBITSOUTHLOS_BUFPOOL_STATE_DELETEINPROGRESS_WAIT_APP_BUF_FREE ==
                pObj->bitBufState[pBitBuf->channelNum])
            {
                OSA_assert(pObj->appAllocBufCnt[pBitBuf->channelNum] > 0);
                pObj->appAllocBufCnt[pBitBuf->channelNum] -= 1;
                if (0 == pObj->appAllocBufCnt[pBitBuf->channelNum])
                {
                    OSA_assert(FALSE == sendFlushFrame);
                    sendFlushFrame = TRUE;
                    flushFrameChId = pBitBuf->channelNum;
                }
            }
            else
            {
                IPCBITSOUTLINK_INFO_LOG(pObj->tskId,"Warning!. Trying to putFullBuffer for chId[%d] in invalid state [%d]",
                                        pBitBuf->channelNum,
                                        pObj->bitBufState[pBitBuf->channelNum]);
            }
            /* filled length of 0 indicates application
                        * did not fill any data in this buffer.
                        * Free it immediately */
#if SYSTEM_DEBUG_IPC_RT
            OSA_printf(" IPC_OUT: Dropping bitbuf\n");
#endif
            OSA_assert(freeBitBufList.numBufs <
                       VIDBITSTREAM_MAX_BITSTREAM_BUFS);
            freeBitBufList.bufs[freeBitBufList.numBufs] = pBitBuf;
            freeBitBufList.numBufs++;
            pObj->stats.droppedCount++;
        }
    }
    if (freeBitBufList.numBufs)
    {
        IpcBitsOutLink_putEmptyBufs(pObj, &freeBitBufList);
    }
    if (sendFlushFrame)
    {
        OSA_assert(flushFrameChId < pObj->numChn);
        OSA_assert(IPCBITSOUTHLOS_BUFPOOL_STATE_DELETEINPROGRESS_WAIT_APP_BUF_FREE ==
                   pObj->bitBufState[flushFrameChId]);
        ipcbitsoutlink_send_flush_frame(pObj,flushFrameChId);
    }
    if (putDone && (pObj->createArgs.baseCreateParams.notifyNextLink))
    {
        System_ipcSendNotify(pObj->createArgs.baseCreateParams.outQueParams[0].
                             nextLink);
    }
    if (!putDone)
    {
        pObj->stats.numNoFullBufCount++;
        if ((pObj->stats.numNoFullBufCount % IPC_BITSOUT_STATS_WARN_INTERVAL) == 0)
        {
            #ifdef DEBUG_IPC_BITS
            OSA_printf("IPCBITSOUT:!!!WARNING.!!! NO FULL BUF AVAILABLE. OCCURENCE COUNT:[%d]",
                       pObj->stats.numNoFullBufCount);
            #endif
        }
    }
    return IPC_BITSOUT_LINK_S_SUCCESS;
}


static
Int32 IpcBitsOutLink_putEmptyBufs(IpcBitsOutLink_Obj * pObj,
                                  Bitstream_BufList * pBufList)
{
    Int32 status = IPC_BITSOUT_LINK_S_SUCCESS;
    Int32 bufId;
    SystemIpcBits_ListElem *pListElem;

    for (bufId = 0; bufId < pBufList->numBufs; bufId++)
    {
        pListElem = (SystemIpcBits_ListElem *) pBufList->bufs[bufId];
        OSA_assert(SharedRegion_getPtr(pListElem->srBufPtr) ==
                   pBufList->bufs[bufId]->addr);
        /* release ListElem back to queue */
        SYSTEM_IPC_BITS_SET_BUFOWNERPROCID(pListElem->bufState);
        SYSTEM_IPC_BITS_SET_BUFSTATE(pListElem->bufState,
                                     IPC_BITBUF_STATE_FREE);
        OSA_assert(pListElem->bitBuf.channelNum <
                   pObj->numChn);
        status = OSA_quePut(&(pObj->listElemQue[pListElem->bitBuf.channelNum]),
                            (Int32) pListElem, OSA_TIMEOUT_NONE);
        OSA_assert(status == OSA_SOK);
        
        RingBufferWriterCancel(&pObj->ringBufHdnl[pListElem->bitBuf.allocPoolID]);
    }
    return IPC_BITSOUT_LINK_S_SUCCESS;
}


static
Int32 ipcbitsoutlink_validate_delbuf_params(IpcBitsOutLink_Obj * pObj,
                                            IpcBitsOutHLOSLink_deleteChBufParams *bufDelPrms)
{
    Int32 status;

    OSA_assert(bufDelPrms != NULL);
    if (
        (bufDelPrms->chId < pObj->createArgs.inQueInfo.numCh)
        &&
        (bufDelPrms->chId < OSA_ARRAYSIZE(pObj->bitBufState))
        &&
        (pObj->pDeleteBufMsg == NULL)
        &&
        (pObj->bitBufState[bufDelPrms->chId] == IPCBITSOUTHLOS_BUFPOOL_STATE_CREATED))
    {
        status = IPC_BITSOUT_LINK_S_SUCCESS;
    }
    else
    {
        status = IPC_BITSOUT_LINK_E_INVALIDPARAM;
    }
    return status;
}

static
Void ipcbitsoutlink_do_delete_ch_buffer_pool(IpcBitsOutLink_Obj * pObj,
                                             UInt32 chId)
{
    IHeap_Handle srBitBufHeapHandle;
    const UInt32 srIndex = SYSTEM_IPC_SR_CACHED;
    Int status,i;
    SystemIpcBits_ListElem *pListElem;

    srBitBufHeapHandle = System_ipcGetSRHeap(srIndex);
    OSA_assert(srBitBufHeapHandle != NULL);

    OSA_assert(OSA_queGetQueuedCount(&pObj->listElemQue[chId]) ==
               pObj->queDepth[chId]);
    for (i = 0; i < pObj->queDepth[chId]; i++)
    {
        status = OSA_queGet(&pObj->listElemQue[chId],
                            (Int32 *)&pListElem,
                            OSA_TIMEOUT_NONE);
        OSA_assert(OSA_SOK == status);
        status = OSA_quePut(&pObj->listElemFreeQue,
                            (Int32)pListElem,
                            OSA_TIMEOUT_NONE);
        OSA_assert(OSA_SOK == status);
    }
    status = OSA_queDelete(&pObj->listElemQue[chId]);
    OSA_assert(status == OSA_SOK);
    OSA_assert(pObj->bitBufBasePtr[chId] != NULL);
    OSA_assert(pObj->bitBufTotalSize[chId] != 0);
    Memory_free(srBitBufHeapHandle,
                pObj->bitBufBasePtr[chId], pObj->bitBufTotalSize[chId]);
    OSA_printf("IPC_BITSOUT:BitBuffer Free.chID:%d,Size:0x%X",
                chId,pObj->bitBufTotalSize[chId]);
    RingBufferDeInit(&pObj->ringBufHdnl[chId]);
    pObj->bitBufBasePtr[chId] = NULL;
    pObj->bitBufTotalSize[chId] = 0;
    pObj->queDepth[chId] = 0;
}

static
Void ipcbitsoutlink_handle_flush_frame_free(IpcBitsOutLink_Obj * pObj,
                                            SystemIpcBits_ListElem *pListElem)

{
    UInt32 chId;
    OSA_MsgHndl *delBufAckMsg;
    Int status;

    OSA_assert(pListElem != NULL);
    IPCBITSOUTLINK_INFO_LOG(pObj->tskId,"Flush Frame Received for chId[%d]",
                            pListElem->bitBuf.channelNum);
    chId = pListElem->bitBuf.channelNum;
    OSA_assert(chId < pObj->numChn);
    OSA_assert(IPCBITSOUTHLOS_BUFPOOL_STATE_DELETEINPROGRESS_FLUSH_DONE ==
               pObj->bitBufState[chId]);
    OSA_assert(pObj->pDeleteBufMsg != NULL);
    delBufAckMsg = pObj->pDeleteBufMsg;
    pObj->pDeleteBufMsg = NULL;
    pObj->bitBufState[chId] = IPCBITSOUTHLOS_BUFPOOL_STATE_DELETED;
    OSA_assert((pListElem - pObj->listElem[0]) < OSA_ARRAYSIZE(pObj->listElem));
    ipcbitsoutlink_do_delete_ch_buffer_pool(pObj,chId);
    status = OSA_quePut(&pObj->listElemFreeQue,(Int32)pListElem,OSA_TIMEOUT_NONE);
    OSA_assert(status == OSA_SOK);
    IPCBITSOUTLINK_INFO_LOG(pObj->tskId,"Ch buffer delete completed for chId[%d].Total Time[%d]",
                            pListElem->bitBuf.channelNum,
                            (OSA_getCurTimeInMsec() - pObj->delMsgReceiveTime));
    OSA_tskAckOrFreeMsg(delBufAckMsg,IPC_BITSOUT_LINK_S_SUCCESS);
}


static
Int32 IpcBitsOutLink_initateDeleteChBuffer(IpcBitsOutLink_Obj * pObj,
                                           IpcBitsOutHLOSLink_deleteChBufParams *bufDelPrms,
                                           Bool *storeMsgForDelayedAck)
{
    Int32 status = IPC_BITSOUT_LINK_S_SUCCESS;

    status = ipcbitsoutlink_validate_delbuf_params(pObj,bufDelPrms);

    if (IPC_BITSOUT_LINK_S_SUCCESS == status)
    {
        UInt32 chId = bufDelPrms->chId;

        if (0 == pObj->appAllocBufCnt[chId])
        {
            /* As application does not have any allocated buffers, send flush buffer now */
            ipcbitsoutlink_send_flush_frame(pObj,chId);
        }
        else
        {
            pObj->bitBufState[chId] =
              IPCBITSOUTHLOS_BUFPOOL_STATE_DELETEINPROGRESS_WAIT_APP_BUF_FREE;
        }
        *storeMsgForDelayedAck = TRUE;
    }
    else
    {
        *storeMsgForDelayedAck = FALSE;
    }
    return status;
}

static
Int32 ipcbitsoutlink_validate_createbuf_params(IpcBitsOutLink_Obj * pObj,
                                               IpcBitsOutHLOSLink_createChBufParams *bufCreatePrms)
{
    Int32 status;

    OSA_assert(bufCreatePrms != NULL);
    if (
        (bufCreatePrms->chId < pObj->createArgs.inQueInfo.numCh)
        &&
        (bufCreatePrms->chId < OSA_ARRAYSIZE(pObj->bitBufState))
        &&
        (pObj->bitBufState[bufCreatePrms->chId] == IPCBITSOUTHLOS_BUFPOOL_STATE_DELETED))
    {
        status = IPC_BITSOUT_LINK_S_SUCCESS;
    }
    else
    {
        status = IPC_BITSOUT_LINK_E_INVALIDPARAM;
    }
    return status;
}

static
Int32 IpcBitsOutLink_doChBufCreate(IpcBitsOutLink_Obj * pObj,
                                   IpcBitsOutHLOSLink_createChBufParams *bufCreatePrms)
{
    IHeap_Handle srBitBufHeapHandle;
    const UInt32 srIndex = SYSTEM_IPC_SR_CACHED;
    Int status = OSA_SOK;
    UInt32 bufSize, numBufs, totBufSize, cacheLineSize;
    UInt32 bufId;
    UInt32 chId = bufCreatePrms->chId;
    Memory_Stats memstats;

    pObj->createArgs.maxQueueDepth[chId] = bufCreatePrms->maxQueueDepth;
    if(pObj->createArgs.maxQueueDepth[chId] == 0)
        pObj->createArgs.maxQueueDepth[chId] =
              IPC_BITS_OUT_MAX_BUFFERS_IN_QUEUE;

    cacheLineSize = SharedRegion_getCacheLineSize(srIndex);

    bufSize =
        OSA_align(bufCreatePrms->chMaxReqBufSize, cacheLineSize);
    numBufs = pObj->createArgs.maxQueueDepth[chId];
    
    totBufSize = OSA_align(bufCreatePrms->totalBitStreamBufferSize , cacheLineSize);

    srBitBufHeapHandle = System_ipcGetSRHeap(srIndex);
    OSA_assert(srBitBufHeapHandle != NULL);
    Memory_getStats(srBitBufHeapHandle,&memstats);
    if (memstats.largestFreeSize >= totBufSize)
    {
        pObj->bitBufBasePtr[chId] =
            Memory_alloc(srBitBufHeapHandle, totBufSize, cacheLineSize, NULL);
        OSA_assert(pObj->bitBufBasePtr[chId] != NULL);

        status = RingBufferInit(&pObj->ringBufHdnl[chId], pObj->bitBufBasePtr[chId], totBufSize);
        OSA_assert(status == OSA_SOK);

        pObj->bitBufSize[chId] = bufSize;
        pObj->queDepth[chId] = numBufs;
        pObj->bitBufTotalSize[chId] = totBufSize;
        pObj->appAllocBufCnt[chId] = 0;

        pObj->bitBufPhyAddr[chId] = IpcBitsOutLink_MapUsrVirt2Phy(pObj->bitBufBasePtr[chId]);

        OSA_printf ("\n###Bit buff of size from the SR # %d : %d / bufsize %d, maxQueueDepth %d \n", srIndex, totBufSize, bufSize, numBufs);
        OSA_printf ("###Bit buff BasePtr: %X / PhyAddr %X\n", (UInt32) pObj->bitBufBasePtr[chId], (UInt32) pObj->bitBufPhyAddr[chId]);

        status = OSA_queCreate(&pObj->listElemQue[chId],
                               SYSTEM_IPC_BITS_MAX_LIST_ELEM);
        OSA_assert(status == OSA_SOK);

        for (bufId = 0; bufId < numBufs; bufId++)
        {
            SystemIpcBits_ListElem *listElem;

            status = OSA_queGet(&pObj->listElemFreeQue,(Int32 *)&listElem,OSA_TIMEOUT_NONE);
            OSA_assert(OSA_SOK == status);
            SYSTEM_IPC_BITS_SET_BUFOWNERPROCID(listElem->bufState);
            SYSTEM_IPC_BITS_SET_BUFSTATE(listElem->bufState,
                                     IPC_BITBUF_STATE_FREE);

            listElem->bitBuf.allocPoolID = chId;
            listElem->bitBuf.bufSize = bufSize;
            listElem->bitBuf.fillLength = 0;
            listElem->bitBuf.mvDataFilledSize = 0;
            listElem->bitBuf.temporalId = 0;
            listElem->bitBuf.numTemporalLayerSetInCodec = 0;
            listElem->bitBuf.startOffset = 0;
            listElem->bitBuf.bottomFieldBitBufSize = 0;
            listElem->bitBuf.doNotDisplay = FALSE;
            listElem->bitBuf.inputFileChanged = FALSE;

            status =
                OSA_quePut(&pObj->listElemQue[chId], (Int32) listElem,
                           OSA_TIMEOUT_NONE);
            OSA_assert(status == OSA_SOK);
        }
    }
    else
    {
        IPCBITSOUTLINK_INFO_LOG(pObj->tskId,"WARNING:Channel Create Failed.!!!! "
                                            "ChId[%d],RequiredMemory:[%d],AvailableMemory:[%d]",
                                            bufCreatePrms->chId,
                                            totBufSize,
                                            memstats.largestFreeSize);
        status = OSA_EFAIL;
    }
    status = (OSA_SOK == status) ? IPC_BITSOUT_LINK_S_SUCCESS : IPC_BITSOUT_LINK_E_INVALIDPARAM;

    return status;
}

static
Int32 IpcBitsOutLink_CreateChBuffer(IpcBitsOutLink_Obj * pObj,
                                    IpcBitsOutHLOSLink_createChBufParams *bufCreatePrms)
{
    Int32 status = IPC_BITSOUT_LINK_S_SUCCESS;

    status = ipcbitsoutlink_validate_createbuf_params(pObj,bufCreatePrms);

    if (IPC_BITSOUT_LINK_S_SUCCESS == status)
    {
        UInt32 chId = bufCreatePrms->chId;

        status = IpcBitsOutLink_doChBufCreate(pObj,bufCreatePrms);
        if (IPC_BITSOUT_LINK_S_SUCCESS == status)
        {
            pObj->bitBufState[chId] = IPCBITSOUTHLOS_BUFPOOL_STATE_CREATED;
        }
    }
    return status;
}


Int32 IpcBitsOutLink_releaseBitBufs(IpcBitsOutLink_Obj * pObj)
{
    SystemIpcBits_ListElem *pListElem = NULL;
    Int32 status;
    UInt32 curTime, roundTripTime;

    curTime = OSA_getCurTimeInMsec();

    do
    {
        pListElem = ListMP_getHead(pObj->listMPInHndl);
        if (pListElem != NULL)
        {
            IpcBitsOutLink_doPostGetCacheOp(pObj, pListElem);
            if(SYSTEM_IPC_BITS_GET_BUFSTATE(pListElem->bufState)
               != IPC_BITBUF_STATE_INQUE)
            {
                OSA_printf("!!!WARNING.Bad ListElem\n");
                OSA_printf("ListElem:%p,State:%d,SharedRegionID:%d,SrPtr:%p",
                            pListElem,
                            SYSTEM_IPC_BITS_GET_BUFSTATE(pListElem->bufState),
                            SharedRegion_getId(pListElem),
                            (void *)pListElem->srBufPtr);
                OSA_printf("Stats:Recv:%d,Free:%d,Dropped:%d",
                           pObj->stats.recvCount,
                           pObj->stats.freeCount,
                           pObj->stats.droppedCount);
                OSA_assert(SYSTEM_IPC_BITS_GET_BUFSTATE(pListElem->bufState)
                           == IPC_BITBUF_STATE_INQUE);
            }
            if (curTime > ((UInt32) pListElem->ipcPrivData))
            {
                roundTripTime = curTime - ((UInt32) pListElem->ipcPrivData);
                pObj->stats.totalRoundTrip += roundTripTime;
            }
            pObj->stats.freeCount++;
            /* release ListElem back to queue */
            SYSTEM_IPC_BITS_SET_BUFOWNERPROCID(pListElem->bufState);
            SYSTEM_IPC_BITS_SET_BUFSTATE(pListElem->bufState,
                                         IPC_BITBUF_STATE_FREE);
            if (pListElem->bitBuf.flushFrame)
            {
                ipcbitsoutlink_handle_flush_frame_free(pObj,pListElem);
            }
            else
            {
				UInt32 len;

                OSA_assert(pListElem->bitBuf.channelNum <
                           pObj->numChn);

                len = OSA_align(pListElem->bitBuf.fillLength, RING_BUF_ALIGN_SIZE);

                /* Just simulate read operation as read APIs will not be called by decoder */
                RingBufferReaderAcquire(&pObj->ringBufHdnl[pListElem->bitBuf.channelNum], &len);
                if (len)
                {
                    RingBufferReaderRelease(&pObj->ringBufHdnl[pListElem->bitBuf.allocPoolID], len);
                }

                status =
                    OSA_quePut(&(pObj->listElemQue[pListElem->bitBuf.channelNum]),
                               (Int32) pListElem, OSA_TIMEOUT_NONE);
                OSA_assert(status == OSA_SOK);
            }

        }                                                  /* if (pListElem
                                                            * != NULL) */
    } while (pListElem != NULL);

    return IPC_BITSOUT_LINK_S_SUCCESS;
}

Int32 IpcBitsOutLink_CheckAllBuffersFreed(IpcBitsOutLink_Obj * pObj,
                                          IpcBitsOutHLOSLink_CheckBufferFreeParams *bufCreatePrms)
{
    Int i;
    Int32 status = IPC_BITSOUT_LINK_S_SUCCESS;

    IpcBitsOutLink_releaseBitBufs(pObj);
    for (i = 0; i < pObj->numChn;i++)
    {
        if (OSA_queGetQueuedCount(&pObj->listElemQue[i]) <
            pObj->queDepth[i])
        {
            break;
        }
    }
    if (i < pObj->numChn)
    {
        bufCreatePrms->allBufsFreedFlag = FALSE;
    }
    else
    {
        bufCreatePrms->allBufsFreedFlag = TRUE;
    }
    return status;
}

Int32 IpcBitsOutLink_getLinkInfo(OSA_TskHndl * pTsk, System_LinkInfo * info)
{
    System_LinkInfo linkInfo;
    IpcBitsOutLink_Obj *pObj = (IpcBitsOutLink_Obj *) pTsk->appData;

    linkInfo.numQue = 1;
    linkInfo.queInfo[0] = pObj->createArgs.inQueInfo;
    memcpy(info, &linkInfo, sizeof(*info));

    return IPC_BITSOUT_LINK_S_SUCCESS;
}

Int IpcBitsOutLink_tskMain(struct OSA_TskHndl * pTsk, OSA_MsgHndl * pMsg,
                           Uint32 curState)
{
    UInt32 cmd = OSA_msgGetCmd(pMsg);
    Bool ackMsg, done;
    Int32 status = IPC_BITSOUT_LINK_S_SUCCESS;
    IpcBitsOutLink_Obj *pObj = (IpcBitsOutLink_Obj *) pTsk->appData;
    char threadName[128];

    snprintf(threadName,sizeof(threadName),"%s_%x",__func__,pObj->tskId);
    threadName[sizeof(threadName) - 1] = 0;
    OSA_printTID(threadName);

    if (cmd != SYSTEM_CMD_CREATE)
    {
        OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
        return status;
    }

    status = IpcBitsOutLink_create(pObj, OSA_msgGetPrm(pMsg));

    OSA_tskAckOrFreeMsg(pMsg, status);

    if (status != OSA_SOK)
        return status;

    done = FALSE;
    ackMsg = FALSE;

    while (!done)
    {
        status = OSA_tskWaitMsg(pTsk, &pMsg);
        if (status != OSA_SOK)
            break;

        cmd = OSA_msgGetCmd(pMsg);

        OSA_mutexLock(&pObj->apiMutex);
        switch (cmd)
        {
            case SYSTEM_CMD_DELETE:
                done = TRUE;
                ackMsg = TRUE;
                break;

            case SYSTEM_IPC_CMD_RELEASE_FRAMES:
                OSA_tskAckOrFreeMsg(pMsg, status);

#ifdef SYSTEM_DEBUG_IPC_RT
                OSA_printf(" %d: IPC_BITS_OUT   : Received Notify !!!\n",
                           OSA_getCurTimeInMsec());
#endif
                if (pObj->createArgs.baseCreateParams.noNotifyMode)
                {
                    pObj->prd.numPendingCmd--;
                }
                IpcBitsOutLink_releaseBitBufs(pObj);
                break;

            case SYSTEM_CMD_START:
                IpcBitsOutLink_start(pObj);
                OSA_tskAckOrFreeMsg(pMsg, status);
                break;

            case SYSTEM_CMD_STOP:
                IpcBitsOutLink_stop(pObj);
                OSA_tskAckOrFreeMsg(pMsg, status);
                break;
            case IPCBITSOUT_LINK_CMD_PRINT_BUFFER_STATISTICS:
                IpcBitsOutLink_printBufferStats(pObj);
                OSA_tskAckOrFreeMsg(pMsg, status);
                break;
            case IPCBITSOUT_LINK_CMD_DELETE_CH_BUFFER:
            {
                Bool storeMsgForDelayedAck;

                status = IpcBitsOutLink_initateDeleteChBuffer(
                                                       pObj,
                                                       OSA_msgGetPrm(pMsg),
                                                       &storeMsgForDelayedAck);
                if (storeMsgForDelayedAck)
                {
                    OSA_assert(pObj->pDeleteBufMsg == NULL);
                    pObj->delMsgReceiveTime = OSA_getCurTimeInMsec();
                    pObj->pDeleteBufMsg = pMsg;
                }
                else
                {
                    OSA_tskAckOrFreeMsg(pMsg, status);
                }
                break;
            }
            case IPCBITSOUT_LINK_CMD_CREATE_CH_BUFFER:
            {
                status = IpcBitsOutLink_CreateChBuffer(
                                                      pObj,
                                                      OSA_msgGetPrm(pMsg));
                OSA_tskAckOrFreeMsg(pMsg, status);
                break;
            }
            case IPCBITSOUT_LINK_CMD_CHECK_ALL_BUFFERS_FREED:
            {
                status = IpcBitsOutLink_CheckAllBuffersFreed(
                                                      pObj,
                                                      OSA_msgGetPrm(pMsg));
                OSA_tskAckOrFreeMsg(pMsg, status);
                break;
            }
            default:
                OSA_tskAckOrFreeMsg(pMsg, status);
                break;
        }
        OSA_mutexUnlock(&pObj->apiMutex);
    }

    IpcBitsOutLink_delete(pObj);

#ifdef SYSTEM_DEBUG_IPC_BITS_OUT
    OSA_printf(" %d: IPC_BITS_OUT   : Delete Done !!!\n",
               OSA_getCurTimeInMsec());
#endif

    if (ackMsg && pMsg != NULL)
        OSA_tskAckOrFreeMsg(pMsg, status);

    return IPC_BITSOUT_LINK_S_SUCCESS;
}

Int32 IpcBitsOutLink_allocListElem(IpcBitsOutLink_Obj * pObj)
{
    UInt32 shAddr;
    UInt32 elemId;

    shAddr = System_ipcListMPAllocListElemMem(SYSTEM_IPC_SR_NON_CACHED_DEFAULT,
                                              sizeof(SystemIpcBits_ListElem) *
                                              SYSTEM_IPC_BITS_MAX_LIST_ELEM);

    for (elemId = 0; elemId < SYSTEM_IPC_BITS_MAX_LIST_ELEM; elemId++)
    {
        pObj->listElem[elemId] =
            (SystemIpcBits_ListElem *) (shAddr +
                                        elemId *
                                        sizeof(SystemIpcBits_ListElem));
    }

    return IPC_BITSOUT_LINK_S_SUCCESS;
}

Int32 IpcBitsOutLink_freeListElem(IpcBitsOutLink_Obj * pObj)
{
    System_ipcListMPFreeListElemMem(SYSTEM_IPC_SR_NON_CACHED_DEFAULT,
                                    (UInt32) pObj->listElem[0],
                                    sizeof(SystemIpcBits_ListElem) *
                                    SYSTEM_IPC_BITS_MAX_LIST_ELEM);

    return IPC_BITSOUT_LINK_S_SUCCESS;
}

Int32 IpcBitsOutLink_initListMP(IpcBitsOutLink_Obj * pObj)
{
    Int32 status;

    status = System_ipcListMPCreate(SYSTEM_IPC_SR_NON_CACHED_DEFAULT,
                                    pObj->tskId,
                                    &pObj->listMPOutHndl, &pObj->listMPInHndl,
                                    &pObj->gateMPOutHndl, &pObj->gateMPInHndl);
    OSA_assert(status == OSA_SOK);

    IpcBitsOutLink_allocListElem(pObj);

    return status;
}

Int32 IpcBitsOutLink_deInitListMP(IpcBitsOutLink_Obj * pObj)
{
    Int32 status;

    status = System_ipcListMPDelete(&pObj->listMPOutHndl, &pObj->listMPInHndl,
                                    &pObj->gateMPOutHndl, &pObj->gateMPInHndl);
    OSA_assert(status == OSA_SOK);

    IpcBitsOutLink_freeListElem(pObj);

    return status;
}


Int32 IpcBitsOutLink_init()
{
    Int32 status;
    System_LinkObj linkObj;
    UInt32 ipcBitsOutId;
    IpcBitsOutLink_Obj *pObj;
    char tskName[32];
    UInt32 procId = System_getSelfProcId();

    OSA_COMPILETIME_ASSERT(offsetof(SystemIpcBits_ListElem, bitBuf) == 0);
    OSA_COMPILETIME_ASSERT(offsetof(Bitstream_Buf, reserved) == 0);
    OSA_COMPILETIME_ASSERT(sizeof(((Bitstream_Buf *) 0)->reserved) ==
                           sizeof(ListMP_Elem));
    for (ipcBitsOutId = 0; ipcBitsOutId < IPC_BITS_OUT_LINK_OBJ_MAX;
         ipcBitsOutId++)
    {
        pObj = &gIpcBitsOutLink_obj[ipcBitsOutId];

        memset(pObj, 0, sizeof(*pObj));

        pObj->tskId =
            SYSTEM_MAKE_LINK_ID(procId,
                                SYSTEM_LINK_ID_IPC_BITS_OUT_0) + ipcBitsOutId;

        linkObj.pTsk = &pObj->tsk;
        linkObj.getLinkInfo = IpcBitsOutLink_getLinkInfo;

        System_registerLink(pObj->tskId, &linkObj);

        OSA_SNPRINTF(tskName, "IPC_BITS_OUT%d", ipcBitsOutId);

        System_ipcRegisterNotifyCb(pObj->tskId, IpcBitsOutLink_notifyCb);

        IpcBitsOutLink_initListMP(pObj);

        status = OSA_mutexCreate(&pObj->apiMutex);
        OSA_assert(status == OSA_SOK);

        status = OSA_tskCreate(&pObj->tsk,
                               IpcBitsOutLink_tskMain,
                               IPC_LINK_TSK_PRI,
                               IPC_LINK_TSK_STACK_SIZE, 0, pObj);
        OSA_assert(status == OSA_SOK);
    }

    return status;
}

Int32 IpcBitsOutLink_deInit()
{
    UInt32 ipcBitsOutId;
    IpcBitsOutLink_Obj *pObj;
    Int32 status;

    for (ipcBitsOutId = 0; ipcBitsOutId < IPC_BITS_OUT_LINK_OBJ_MAX;
         ipcBitsOutId++)
    {
        pObj = &gIpcBitsOutLink_obj[ipcBitsOutId];

        OSA_tskDelete(&pObj->tsk);

        IpcBitsOutLink_deInitListMP(pObj);

        status = OSA_mutexDelete(&pObj->apiMutex);
        OSA_assert(status == OSA_SOK);

    }
    return IPC_BITSOUT_LINK_S_SUCCESS;
}

Int32 IpcBitsOutLink_getEmptyVideoBitStreamBufs(UInt32 linkId,
                                                Bitstream_BufList *bufList,
                                                IpcBitsOutLinkHLOS_BitstreamBufReqInfo *reqInfo)
{
    OSA_TskHndl * pTsk;
    IpcBitsOutLink_Obj * pObj;
    Int status;

    OSA_assert(bufList != NULL);
    if (!((linkId  >= SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0)
          &&
          (linkId  < (SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0 + IPC_BITS_OUT_LINK_OBJ_MAX))))
    {
        return IPC_BITSOUT_LINK_E_INVALIDLINKID;
    }
    pTsk = System_getLinkTskHndl(linkId);
    pObj = pTsk->appData;
    OSA_mutexLock(&pObj->apiMutex);
    bufList->numBufs = 0;
    if (pObj->startProcessing)
    {
        IpcBitsOutLink_releaseBitBufs(pObj);
        status = IpcBitsOutLink_getEmptyBufs(pObj,bufList,reqInfo);
    }
    else
    {
        status = IPC_BITSOUT_LINK_S_SUCCESS;
    }
    OSA_mutexUnlock(&pObj->apiMutex);
    return status;
}


Int32 IpcBitsOutLink_putFullVideoBitStreamBufs(UInt32 linkId,
                                               Bitstream_BufList *bufList)
{
    OSA_TskHndl * pTsk;
    IpcBitsOutLink_Obj * pObj;
    Int status;

    OSA_assert(bufList != NULL);
    if (!((linkId  >= SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0)
          &&
          (linkId  < (SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0 + IPC_BITS_OUT_LINK_OBJ_MAX))))
    {
        return IPC_BITSOUT_LINK_E_INVALIDLINKID;
    }
    pTsk = System_getLinkTskHndl(linkId);
    pObj = pTsk->appData;
    OSA_mutexLock(&pObj->apiMutex);
    status = IpcBitsOutLink_putFullBufs(pObj,bufList);
    OSA_mutexUnlock(&pObj->apiMutex);
    return status;
}

Int32 IpcBitsOutLink_getInQueInfo(UInt32 linkId,
                                  System_LinkQueInfo *inQueInfo)
{
    OSA_TskHndl * pTsk;
    IpcBitsOutLink_Obj * pObj;
    Int status = IPC_BITSOUT_LINK_S_SUCCESS;

    OSA_assert(inQueInfo != NULL);
    if (!((linkId  >= SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0)
          &&
          (linkId  < (SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0 + IPC_BITS_OUT_LINK_OBJ_MAX))))
    {
        return IPC_BITSOUT_LINK_E_INVALIDLINKID;
    }
    pTsk = System_getLinkTskHndl(linkId);
    pObj = pTsk->appData;
    if (pObj->startProcessing)
    {
        *inQueInfo = pObj->createArgs.inQueInfo;
    }
    else
    {
        inQueInfo->numCh = 0;
    }
    return status;
}


