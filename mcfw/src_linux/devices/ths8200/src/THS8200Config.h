#ifndef THS8200CONFIG_INCLUDED_
#define THS8200CONFIG_INCLUDED_

#include "CSC_Config.h"

#define THS8200_ABS(a)   (((a) >= 0) ? (a) : (-(a))) //absolute value
#define THS8200_SIGN(a)  (((a) >= 0) ? (0) : (1))    //sign of value

#define THS8200_MAX_PCLK_DAC         205 //Maximum pixel clock frequency (MHz) supported by THS8200 DACs

#define THS8200_VESA_DMT_TIMING      0 //VESA-DMT timing parameters
#define THS8200_CEA_REVISED_TIMING   1 //CEA-861 timing parameters (revised for THS8200)
#define THS8200_CEA_TIMING           2 //CEA-861 timing parameters
#define THS8200_VESA_CVT_TIMING      3 //VESA-CVT timing parameters
#define THS8200_VESA_SMT_TIMING      4 //VESA-SMT timing parameters

#define THS8200_VALID_STD_FMT        0 //Valid standard format
#define THS8200_VALID_CUSTOM_FMT     1 //Valid custom format
#define THS8200_INVALID_FMT          2 //Invalid format

#define THS8200_LAST_FMT             0xFFFF //Format ID reserved for the last format in CEA_TIMINGS and VESA_TIMINGS arrays
#define THS8200_CUSTOM_FORMAT        -1 // Format ID reserved for a custom format


#define CSM_CTL_INPUT_RANGE_LMTD     0 // Limited range inputs
#define CSM_CTL_INPUT_RANGE_FULL     1 // Full range inputs

//Output modes
#define THS8200_OUTPUT_SYNCS_DIS     0 // Discrete output syncs disabled
#define THS8200_OUTPUT_SYNCS_EN      1 // Discrete output syncs enabled

#define THS8200_SOG_DISABLED         0 // Sync-On-Green disabled
#define THS8200_SOG_ENABLED          1 // Sync-On-Green enabled

//Timing modes
#define THS8200_PROGRESSIVE          0 // Progressive scan mode
#define THS8200_INTERLACED           1 // Interlaced scan mode

#define THS8200_VACTIVE_SDTV_480     480
#define THS8200_VACTIVE_SDTV_576     576
#define THS8200_VACTIVE_HDTV_720     720
#define THS8200_VACTIVE_HDTV_1080    1080

//DTG preset/generic modes
#define THS8200_DTG_PRESET_MODE      0 // Preset modes: HDTV (1080p, 1080i, 720p), SDTV (480i, 480p, 576i) and VESA preset modes
#define THS8200_DTG_GENERIC_MODE     1 // Generic modes: HDTV, SDTV (480p, 576p) and VESA generic modes

#define THS8200_DTG_TIMING_SDTV      0 // SDTV timing mode (bi-level syncs with equalization/serration pulses)
#define THS8200_DTG_TIMING_HDTV      1 // HDTV timing mode (tri-level syncs with equalization/serration pulses)
#define THS8200_DTG_TIMING_VESA      2 // VESA timing mode (bi-level syncs without equalization/serration pulses)

#define THS8200_DTG_HDTV_1080P       0 // ATSC mode 1080P (SMPTE 274M progressive) [HDTV]
#define THS8200_DTG_HDTV_1080I       1 // ATSC mode 1080I (SMPTE274M interlaced) [HDTV]
#define THS8200_DTG_HDTV_720P        2 // ATSC mode 720P (SMPTE296M progressive) [HDTV]
#define THS8200_DTG_HDTV_GENERIC     3 // Generic mode for HDTV [HDTV]
#define THS8200_DTG_SDTV_480I        4 // ATSC mode 480I (SDTV 525 lines interlaced) [SDTV]
#define THS8200_DTG_SDTV_480P        5 // ATSC mode 480P (SDTV 525 lines progressive) [SDTV]
#define THS8200_DTG_VESA_MASTER      6 // VESA master [VESA]
#define THS8200_DTG_VESA_SLAVE       7 // VESA slave [VESA]
#define THS8200_DTG_SDTV_576I        8 // 576I (SDTV 625 lines interlaced) [SDTV]
#define THS8200_DTG_SDTV_GENERIC     9 // Generic mode for SDTV [SDTV]

//DTG line type modes
#define THS8200_DTG_ACTIVE_VIDEO     0x0 // Active Video
#define THS8200_DTG_FULL_NTSP        0x1 // Full Normal Tri-Level Sync Pulse
#define THS8200_DTG_FULL_BTSP        0x2 // Full Broad Pulse and Tri-Level Sync Pulse
#define THS8200_DTG_NTSP_NTSP        0x3 // Normal Tri-Level Sync Pulse/Normal Tri-Level Sync Pulse
#define THS8200_DTG_BTSP_BTSP        0x4 // Broad Pulse and Tri-Level Sync Pulse/Broad Pulse and Tri-Level Sync Pulse
#define THS8200_DTG_NTSP_BTSP        0x5 // Normal Tri-Level Sync Pulse/Broad Pulse and Tri-Level Sync Pulse
#define THS8200_DTG_BTSP_NTSP        0x6 // Broad Pulse and Tri-Level Sync Pulse/Normal Tri-Level Sync Pulse
#define THS8200_DTG_ACTIVE_NEQ       0x7 // Active Video/Negative Equalization Pulse
#define THS8200_DTG_NSP_ACTIVE       0x8 // Normal Sync Pulse/Active Video
#define THS8200_DTG_FULL_NSP         0x9 // Full Normal Sync Pulse
#define THS8200_DTG_FULL_BSP         0xA // Full Broad Sync Pulse
#define THS8200_DTG_FULL_NEQ         0xB // Full Negative Equalization Pulse
#define THS8200_DTG_NEQ_NEQ          0xC // Negative Equalization Pulse/Negative Equalization Pulse
#define THS8200_DTG_BSP_BSP          0xD // Broad Sync Pulse/Broad Sync Pulse
#define THS8200_DTG_BSP_NEQ          0xE // Broad Sync Pulse/Negative Equalization Pulse
#define THS8200_DTG_NEQ_BSP          0xF // Negative Equalization Pulse/Broad Sync Pulse

//Misc parameters
//#define THS8200_BLANK_LEVEL          512 //512 = 0x200 => 350mV
//#define THS8200_SYNC_LOW_LEVEL        73 //512 - (1023 * 3 / 7) =  73 = 0x049 => 50mV
//#define THS8200_SYNC_HIGH_LEVEL      950 //512 + (1023 * 3 / 7) = 950 = 0x3B6 => 650mV

#define THS8200_HS_IN_ADVANCE_8      8 // HS_IN advance of 8

#define THS8200_HS_OUT_DELAY_0       0 // HS_OUT delay of 0
#define THS8200_HS_OUT_DELAY_8       8 // HS_OUT delay of 8

//#define THS8200_CNTL_REG_FIRST       0x03 //First THS8200 control register
//#define THS8200_CNTL_REG_LAST        0x85 //Last THS8200 control register

//#define THS8200_MAX_PCLK_CSC         150 //Maximum pixel clock frequency (MHz) supported by THS8200 CSC
//#define THS8200_MAX_PCLK_OSS          80 //Maximum pixel clock frequency (MHz) supported by 2x oversampling stage
#define THS8200_MAX_PCLK_DLL_HIGH     80 //Maximum pixel clock frequency (MHz) supported by DLL's high range
#define THS8200_MAX_PCLK_DLL_LOW      40 //Maximum pixel clock frequency (MHz) supported by DLL's low range

#define THS8200_FSADJ1                0
#define THS8200_FSADJ2                1

typedef struct
{
  //Structure to keep track of the state of the individual bit fields of the THS8200

//int reg00_reserved1;
//int reg01_reserved2;

  //System
//int reg02_version; //status register
  int reg03_vesa_clk;
  int reg03_dll_bypass;
  int reg03_vesa_color_bars;
  int reg03_dll_freq_sel;
  int reg03_dac_pwdn;
  int reg03_chip_pwdn;
  int reg03_chip_ms;
  int reg03_arst_func_n;

  //Color Space Conversion
  int reg04_csc_ric1;
  int reg04_csc_rfc1_msb;
  int reg05_csc_rfc1_lsb;
  int reg06_csc_ric2;
  int reg06_csc_rfc2_msb;
  int reg07_csc_rfc2_lsb;
  int reg08_csc_ric3;
  int reg08_csc_rfc3_msb;
  int reg09_csc_rfc3_lsb;
  int reg0A_csc_gic1;
  int reg0A_csc_gfc1_msb;
  int reg0B_csc_gfc1_lsb;
  int reg0C_csc_gic2;
  int reg0C_csc_gfc2_msb;
  int reg0D_csc_gfc2_lsb;
  int reg0E_csc_gic3;
  int reg0E_csc_gfc3_msb;
  int reg0F_csc_gfc3_lsb;
  int reg10_csc_bic1;
  int reg10_csc_bfc1_msb;
  int reg11_csc_bfc1_lsb;
  int reg12_csc_bic2;
  int reg12_csc_bfc2_msb;
  int reg13_csc_bfc2_lsb;
  int reg14_csc_bic3;
  int reg14_csc_bfc3_msb;
  int reg15_csc_bfc3_lsb;
  int reg16_csc_offset1_msb;
  int reg17_csc_offset1_lsb;
  int reg17_csc_offset2_msb;
  int reg18_csc_offset2_lsb;
  int reg18_csc_offset3_msb;
  int reg19_csc_offset3_lsb;
  int reg19_csc_bypass;
  int reg19_c_uof_cntl;

  //Test
  int reg1A_tst_digbpass;
  int reg1A_tst_offset;
  int reg1B_tst_ydelay;
  int reg1B_tst_fastramp;
  int reg1B_tst_slowramp;

  //Data Path
  int reg1C_data_clk656_on;
  int reg1C_data_fsadj;
  int reg1C_data_ifir12_bypass;
  int reg1C_data_ifir35_bypass;
  int reg1C_data_tristate656;
  int reg1C_data_dman_cntl;

  //Display Timing Generation, Part 1
  int reg1D_dtg1_y_blank_lsb;
  int reg1E_dtg1_y_sync_low_lsb;
  int reg1F_dtg1_y_sync_high_lsb;
  int reg20_dtg1_cbcr_blank_lsb;
  int reg21_dtg1_cbcr_sync_low_lsb;
  int reg22_dtg1_cbcr_sync_high_lsb;
  int reg23_dtg1_y_blank_msb;
  int reg23_dtg1_y_sync_low_msb;
  int reg23_dtg1_y_sync_high_msb;
  int reg24_dtg1_cbcr_blank_msb;
  int reg24_dtg1_cbcr_sync_low_msb;
  int reg24_dtg1_cbcr_sync_high_msb;
  int reg25_dtg1_spec_a;
  int reg26_dtg1_spec_b;
  int reg27_dtg1_spec_c;
  int reg28_dtg1_spec_d_lsb;
  int reg29_dtg1_spec_d1;
  int reg2A_dtg1_spec_e_lsb;
  int reg2B_dtg1_spec_d_msb;
  int reg2B_dtg1_spec_e_msb;
  int reg2B_dtg1_spec_h_msb;
  int reg2C_dtg1_spec_h_lsb;
  int reg2D_dtg1_spec_i_msb;
  int reg2E_dtg1_spec_i_lsb;
  int reg2F_dtg1_spec_k_lsb;
  int reg30_dtg1_spec_k_msb;
  int reg31_dtg1_spec_k1;
  int reg32_dtg1_spec_g_lsb;
  int reg33_dtg1_spec_g_msb;
  int reg34_dtg1_total_pixels_msb;
  int reg35_dtg1_total_pixels_lsb;
  int reg36_dtg1_field_flip;
  int reg36_dtg1_linecnt_msb;
  int reg37_dtg1_linecnt_lsb;
  int reg38_dtg1_on;
  int reg38_dtg1_pass_through;
  int reg38_dtg1_mode;
  int reg39_dtg1_frame_size_msb;
  int reg39_dtg1_field_size_msb;
  int reg3A_dtg1_frame_size_lsb;
  int reg3B_dtg1_field_size_lsb;
  int reg3C_dtg1_vesa_cbar_size;

  //DAC
  int reg3D_dac_i2c_cntl;
  int reg3D_dac1_cntl_msb;
  int reg3D_dac2_cntl_msb;
  int reg3D_dac3_cntl_msb;
  int reg3E_dac1_cntl_lsb;
  int reg3F_dac2_cntl_lsb;
  int reg40_dac3_cntl_lsb;

  //Clip/Shift/Multiplier
  int reg41_csm_clip_gy_low;
  int reg42_csm_clip_bcb_low;
  int reg43_csm_clip_rcr_low;
  int reg44_csm_clip_gy_high;
  int reg45_csm_clip_bcb_high;
  int reg46_csm_clip_rcr_high;
  int reg47_csm_shift_gy;
  int reg48_csm_shift_bcb;
  int reg49_csm_shift_rcr;
  int reg4A_csm_mult_gy_on;
  int reg4A_csm_shift_gy_on;
  int reg4A_csm_gy_high_clip_on;
  int reg4A_csm_gy_low_clip_on;
  int reg4A_csm_of_cntl;
  int reg4A_csm_mult_gy_msb;
  int reg4B_csm_mult_bcb_msb;
  int reg4B_csm_mult_rcr_msb;
  int reg4C_csm_mult_gy_lsb;
  int reg4D_csm_mult_bcb_lsb;
  int reg4E_csm_mult_rcr_lsb;
  int reg4F_csm_mult_rcr_on;
  int reg4F_csm_mult_bcb_on;
  int reg4F_csm_shift_rcr_on;
  int reg4F_csm_shift_bcb_on;
  int reg4F_csm_rcr_high_clip_on;
  int reg4F_csm_rcr_low_clip_on;
  int reg4F_csm_bcb_high_clip_on;
  int reg4F_csm_bcb_low_clip_on;

  //Display Timing Generation, Part 2
  int reg50_dtg2_bp1_msb;
  int reg50_dtg2_bp2_msb;
  int reg51_dtg2_bp3_msb;
  int reg51_dtg2_bp4_msb;
  int reg52_dtg2_bp5_msb;
  int reg52_dtg2_bp6_msb;
  int reg53_dtg2_bp7_msb;
  int reg53_dtg2_bp8_msb;
  int reg54_dtg2_bp9_msb;
  int reg54_dtg2_bp10_msb;
  int reg55_dtg2_bp11_msb;
  int reg55_dtg2_bp12_msb;
  int reg56_dtg2_bp13_msb;
  int reg56_dtg2_bp14_msb;
  int reg57_dtg2_bp15_msb;
  int reg57_dtg2_bp16_msb;
  int reg58_dtg2_bp1_lsb;
  int reg59_dtg2_bp2_lsb;
  int reg5A_dtg2_bp3_lsb;
  int reg5B_dtg2_bp4_lsb;
  int reg5C_dtg2_bp5_lsb;
  int reg5D_dtg2_bp6_lsb;
  int reg5E_dtg2_bp7_lsb;
  int reg5F_dtg2_bp8_lsb;
  int reg60_dtg2_bp9_lsb;
  int reg61_dtg2_bp10_lsb;
  int reg62_dtg2_bp11_lsb;
  int reg63_dtg2_bp12_lsb;
  int reg64_dtg2_bp13_lsb;
  int reg65_dtg2_bp14_lsb;
  int reg66_dtg2_bp15_lsb;
  int reg67_dtg2_bp16_lsb;
  int reg68_dtg2_linetype1;
  int reg68_dtg2_linetype2;
  int reg69_dtg2_linetype3;
  int reg69_dtg2_linetype4;
  int reg6A_dtg2_linetype5;
  int reg6A_dtg2_linetype6;
  int reg6B_dtg2_linetype7;
  int reg6B_dtg2_linetype8;
  int reg6C_dtg2_linetype9;
  int reg6C_dtg2_linetype10;
  int reg6D_dtg2_linetype11;
  int reg6D_dtg2_linetype12;
  int reg6E_dtg2_linetype13;
  int reg6E_dtg2_linetype14;
  int reg6F_dtg2_linetype15;
  int reg6F_dtg2_linetype16;
  int reg70_dtg2_hlength_lsb;
  int reg71_dtg2_hlength_msb;
  int reg71_dtg2_hdly_msb;
  int reg72_dtg2_hdly_lsb;
  int reg73_dtg2_vlength1_lsb;
  int reg74_dtg2_vlength1_msb;
  int reg74_dtg2_vdly1_msb;
  int reg75_dtg2_vdly1_lsb;
  int reg76_dtg2_vlength2_lsb;
  int reg77_dtg2_vlength2_msb;
  int reg77_dtg2_vdly2_msb;
  int reg78_dtg2_vdly2_lsb;
  int reg79_dtg2_hs_in_dly_msb;
  int reg7A_dtg2_hs_in_dly_lsb;
  int reg7B_dtg2_vs_in_dly_msb;
  int reg7C_dtg2_vs_in_dly_lsb;
//int reg7D_dtg2_pixel_cnt_msb; //status register
//int reg7E_dtg2_pixel_cnt_lsb; //status register
//int reg7F_dtg2_ip_fmt;        //status register
//int reg7F_dtg2_line_cnt_msb;  //status register
//int reg80_dtg2_line_cnt_lsb;  //status register
  int reg81_reserved3;          //reserved
  int reg82_dtg2_fid_de_cntl;
  int reg82_dtg2_rgb_mode_on;
  int reg82_dtg2_embedded_timing;
  int reg82_dtg2_vsout_pol;
  int reg82_dtg2_hsout_pol;
  int reg82_dtg2_fid_pol;
  int reg82_dtg2_vs_pol;
  int reg82_dtg2_hs_pol;

  //CGMS Control
  int reg83_cgms_en;
  int reg83_cgms_header;
  int reg84_cgms_payload_msb;
  int reg85_cgms_payload_lsb;
//int reg86_misc_ppl_lsb; //status register
//int reg87_misc_ppl_msb; //status register
//int reg88_misc_lpf_lsb; //status register
//int reg89_misc_lpf_msb; //status register
} THS8200_CFG;

typedef struct
{
  int    Y_BLANK;
  int    C_BLANK;
  int    Y_SYNC_LOW;
  int    C_SYNC_LOW;
  int    Y_SYNC_HIGH;
  int    C_SYNC_HIGH;
} THS8200_SOG;

typedef struct
{
  int    CLIP_GY_LOW;
  int    CLIP_BCB_LOW;
  int    CLIP_RCR_LOW;
  int    CLIP_GY_HIGH;
  int    CLIP_BCB_HIGH;
  int    CLIP_RCR_HIGH;
  int    SHIFT_GY;
  int    SHIFT_BCB;
  int    SHIFT_RCR;
  int    MULT_GY;
  int    MULT_BCB;
  int    MULT_RCR;
  char   CLIP_GY_LOW_ON;
  char   CLIP_BCB_LOW_ON;
  char   CLIP_RCR_LOW_ON;
  char   CLIP_GY_HIGH_ON;
  char   CLIP_BCB_HIGH_ON;
  char   CLIP_RCR_HIGH_ON;
  char   SHIFT_GY_ON;
  char   SHIFT_BCB_ON;
  char   SHIFT_RCR_ON;
  char   MULT_GY_ON;
  char   MULT_BCB_ON;
  char   MULT_RCR_ON;
} THS8200_CSM;

//prototypes
void THS8200SetConfig(THS8200_CONFIGURATION *, THS8200_REGS);
void THS8200SetCSC(THS8200_CFG *THS8200Cfg, CSC_COEFFICIENTS *CSC);
void THS8200ConfigSetDefaults(THS8200_CFG *THS8200Cfg);
void THS8200SetSOG(THS8200_CFG *THS8200Cfg, THS8200_SOG *SOG);

void THS8200SetCSM(THS8200_CFG *THS8200Cfg, THS8200_CSM *CSM);
void InitializeCSM(THS8200_CSM *CSM_Settings, THS8200_CONFIGURATION *THS8200);
void InitializeSOG(THS8200_SOG *SOG_Settings, THS8200_CONFIGURATION *THS8200);

void THS8200DisableSOG(THS8200_SOG *SOG_Settings);

#endif
