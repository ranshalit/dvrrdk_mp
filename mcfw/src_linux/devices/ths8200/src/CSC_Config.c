// Low level CSC configuration code

//Version 1.0 Updated March 15th 2012
//Changes
//

#include "CSC_Config.h"
#include <math.h> //for hue control only

                                      //min, mid,  max
const CSC_IO_RANGE Range4RGBFull    = {   0,   0, 1023,  //input/output range for GY
                                          0,   0, 1023,  //input/output range for BCb
                                          0,   0, 1023}; //input/output range for RCr

                                      //min, mid,  max
const CSC_IO_RANGE Range4RGBLmtd    = {  64,  64,  940,  //input/output range for GY
                                         64,  64,  940,  //input/output range for BCb
                                         64,  64,  940}; //input/output range for RCr

                                      //min, mid,  max
const CSC_IO_RANGE Range4YCbCrFull  = {   0,   0, 1023,  //input/output range for GY
                                          0, 512, 1023,  //input/output range for BCb
                                          0, 512, 1023}; //input/output range for RCr

                                      //min, mid,  max
const CSC_IO_RANGE Range4YCbCrLmtd  = {  64,  64,  940,  //input/output range for GY
                                         64, 512,  960,  //input/output range for BCb
                                         64, 512,  960}; //input/output range for RCr

//Note: The "mid" values are used for calculating the CSC offset terms.

//CSC coefficients
//Yd = G1*Gd + B1*Bd + R1*Rd + OFFS1
//Cb = G2*Gd + B2*Bd + R2*Rd + OFFS2
//Cr = G3*Gd + B3*Bd + R3*Rd + OFFS3
/*
                                      //      Gd,        Bd,        Rd
const CSC_COEFFICIENTS RGB2HDTV     = { 0.715169,  0.072192,  0.212639,     0,  //Yd
                                       -0.385408,  0.500000, -0.114592,     0,  //Cb
                                       -0.454156, -0.045844,  0.500000,     0}; //Cr

                                      //      Gd,        Bd,        Rd
const CSC_COEFFICIENTS RGB2SDTV     = { 0.587000,  0.114000,  0.299000,     0,  //Yd
                                       -0.331264,  0.500000, -0.168736,     0,  //Cb
                                       -0.418688, -0.081312,  0.500000,     0}; //Cr
*/
                                      //      Gd,        Bd,        Rd
const CSC_COEFFICIENTS RGB2HDTV     = { 0x000B7155, 0x000127B3, 0x000366F8, 0,  //Yd
                                        0xFFF9D560, 0x00080000, 0xFFFE2AA2, 0,  //Cb
                                        0xFFF8BBC8, 0xFFFF443A, 0x00080000, 0}; //Cr

                                      //      Gd,        Bd,        Rd
const CSC_COEFFICIENTS RGB2SDTV     = { 0x0009645A, 0x0001D2F2, 0x0004C8B4, 0,  //Yd
                                        0xFFFAB325, 0x00080000, 0xFFFD4CDD, 0,  //Cb
                                        0xFFF94D0F, 0xFFFEB2F3, 0x00080000, 0}; //Cr

//Gd = G1*Yd + B1*Cb + R1*Cr + OFFS1
//Bd = G2*Yd + B2*Cb + R2*Cr + OFFS2
//Rd = G3*Yd + B3*Cb + R3*Cr + OFFS3
/*
                                      //      Yd,        Cb,        Cr
const CSC_COEFFICIENTS HDTV2RGB     = { 1.000000, -0.187314, -0.468207,     0,  //Gd
                                        1.000000,  1.855615,  0.000000,     0,  //Bd
                                        1.000000,  0.000000,  1.574722,     0}; //Rd

                                      //      Yd,        Cb,        Cr
const CSC_COEFFICIENTS SDTV2RGB     = { 1.000000, -0.344136, -0.714136,     0,  //Gd
                                        1.000000,  1.772000,  0.000000,     0,  //Bd
                                        1.000000,  0.000000,  1.402000,     0}; //Rd
*/
                                      //      Yd,        Cb,        Cr
const CSC_COEFFICIENTS HDTV2RGB     = { 0x00100000, 0xFFFD00C4, 0xFFF8823A, 0,  //Gd
                                        0x00100000, 0x001DB09A, 0x00000000, 0,  //Bd
                                        0x00100000, 0x00000000, 0x00193210, 0}; //Rd

                                      //      Yd,        Cb,        Cr
const CSC_COEFFICIENTS SDTV2RGB     = { 0x00100000, 0xFFFA7E6C, 0xFFF492E7, 0,  //Gd
                                        0x00100000, 0x001C5A1D, 0x00000000, 0,  //Bd
                                        0x00100000, 0x00000000, 0x00166E98, 0}; //Rd

//Yd = G1*Yd + B1*Cb + R1*Cr + OFFS1
//Cb = G2*Yd + B2*Cb + R2*Cr + OFFS2
//Cr = G3*Yd + B3*Cb + R3*Cr + OFFS3
/*
                                      //      Yd,        Cb,        Cr
const CSC_COEFFICIENTS HDTV2SDTV    = { 1.000000,  0.101587,  0.196004,     0,  //Yd
                                        0.000000,  0.989858, -0.110612,     0,  //Cb
                                        0.000000, -0.072458,  0.983394,     0}; //Cr

                                      //      Yd,        Cb,        Cr
const CSC_COEFFICIENTS SDTV2HDTV    = { 1.000000, -0.118191, -0.212608,     0,  //Yd
                                        0.000000,  1.018633,  0.114575,     0,  //Cb
                                        0.000000,  0.075055,  1.025329,     0}; //Cr

                                      //   Gd/Yd,     Bd/Cb,     Rd/Cr
const CSC_COEFFICIENTS RGB2RGB      = { 1.000000,  0.000000,  0.000000,     0,  //Gd/Yd
                                        0.000000,  1.000000,  0.000000,     0,  //Bd/Cb
                                        0.000000,  0.000000,  1.000000,     0}; //Rd/Cr

                                      //   Gd/Yd,     Bd/Cb,     Rd/Cr
const CSC_COEFFICIENTS SDTV2SDTV    = { 1.000000,  0.000000,  0.000000,     0,  //Gd/Yd
                                        0.000000,  1.000000,  0.000000,     0,  //Bd/Cb
                                        0.000000,  0.000000,  1.000000,     0}; //Rd/Cr

                                      //   Gd/Yd,     Bd/Cb,     Rd/Cr
const CSC_COEFFICIENTS HDTV2HDTV    = { 1.000000,  0.000000,  0.000000,     0,  //Gd/Yd
                                        0.000000,  1.000000,  0.000000,     0,  //Bd/Cb
                                        0.000000,  0.000000,  1.000000,     0}; //Rd/Cr
*/
                                      //      Yd,        Cb,        Cr
const CSC_COEFFICIENTS HDTV2SDTV    = { 0x00100000, 0x0001A019, 0x000322D5, 0,  //Yd
                                        0x00000000, 0x000FD675, 0xFFFE3AF0, 0,  //Cb
                                        0x00000000, 0xFFFED737, 0x000FBBFB, 0}; //Cr

                                      //      Yd,        Cb,        Cr
const CSC_COEFFICIENTS SDTV2HDTV    = { 0x00100000, 0xFFFE1BE5, 0xFFFC9929, 0,  //Yd
                                        0x00000000, 0x00104C52, 0x0001D54D, 0,  //Cb
                                        0x00000000, 0x0001336D, 0x001067BF, 0}; //Cr

                                      //   Gd/Yd,     Bd/Cb,     Rd/Cr
const CSC_COEFFICIENTS RGB2RGB      = { 0x00100000, 0x00000000, 0x00000000, 0,  //Gd/Yd
                                        0x00000000, 0x00100000, 0x00000000, 0,  //Bd/Cb
                                        0x00000000, 0x00000000, 0x00100000, 0}; //Rd/Cr

                                      //   Gd/Yd,     Bd/Cb,     Rd/Cr
const CSC_COEFFICIENTS SDTV2SDTV    = { 0x00100000, 0x00000000, 0x00000000, 0,  //Gd/Yd
                                        0x00000000, 0x00100000, 0x00000000, 0,  //Bd/Cb
                                        0x00000000, 0x00000000, 0x00100000, 0}; //Rd/Cr

                                      //   Gd/Yd,     Bd/Cb,     Rd/Cr
const CSC_COEFFICIENTS HDTV2HDTV    = { 0x00100000, 0x00000000, 0x00000000, 0,  //Gd/Yd
                                        0x00000000, 0x00100000, 0x00000000, 0,  //Bd/Cb
                                        0x00000000, 0x00000000, 0x00100000, 0}; //Rd/Cr

//Swap CSC coefficients for B/Cb and R/Cr inputs/outputs
void CSC_CbCrSwap(CSC_COEFFICIENTS *CSC_Coef, CSC_CONTROL *CSC_Ctl)
{
  int Temp;

//Yd = G1*Yd + B1*Cb + R1*Cr + OFFS1
//Cb = G2*Yd + B2*Cb + R2*Cr + OFFS2
//Cr = G3*Yd + B3*Cb + R3*Cr + OFFS3

  if(CSC_Ctl->CbCrSwap == 1)      //Swap CSC coefficients for B/Cb and R/Cr inputs (DAC mode)
  {
    Temp         = CSC_Coef->B1;
    CSC_Coef->B1 = CSC_Coef->R1;
    CSC_Coef->R1 = Temp;

    Temp         = CSC_Coef->B2;
    CSC_Coef->B2 = CSC_Coef->R2;
    CSC_Coef->R2 = Temp;

    Temp         = CSC_Coef->B3;
    CSC_Coef->B3 = CSC_Coef->R3;
    CSC_Coef->R3 = Temp;
  }
  else if(CSC_Ctl->CbCrSwap == 2) //Swap CSC coefficients for B/Cb and R/Cr outputs (ADC mode)
  {
    Temp         = CSC_Coef->G2;
    CSC_Coef->G2 = CSC_Coef->G3;
    CSC_Coef->G3 = Temp;

    Temp         = CSC_Coef->B2;
    CSC_Coef->B2 = CSC_Coef->B3;
    CSC_Coef->B3 = Temp;

    Temp         = CSC_Coef->R2;
    CSC_Coef->R2 = CSC_Coef->R3;
    CSC_Coef->R3 = Temp;
  }
}

void ModifyCSC4ColorCtl(CSC_COEFFICIENTS *CSC_Coef, CSC_RANGE *Range, CSC_CONTROL *CSC_Ctl)
{
  double angle_radian;

  //Modify CSC coefficients for brightness, contrast, saturation and hue control
  //-128 <= CSC_CTL->Brightness <= +127 counts
  //   0 <= CSC_CTL->Contrast   <=  200 percent
  //   0 <= CSC_CTL->Saturation <=  200 percent
  // -30 <= CSC_CTL->Hue        <=  +30 degrees

  angle_radian = (CSC_CTL_PI / 180) * CSC_Ctl->Hue; //-30 to +30 degrees

  //contrast control
  CSC_Coef->G1 = CSC_Coef->G1 * CSC_Ctl->Contrast / 100;
  CSC_Coef->B1 = CSC_Coef->B1 * CSC_Ctl->Contrast / 100;
  CSC_Coef->R1 = CSC_Coef->R1 * CSC_Ctl->Contrast / 100;

  CSC_Coef->G2 = CSC_Coef->G2 * CSC_Ctl->Contrast / 100;
  CSC_Coef->B2 = CSC_Coef->B2 * CSC_Ctl->Contrast / 100;
  CSC_Coef->R2 = CSC_Coef->R2 * CSC_Ctl->Contrast / 100;

  CSC_Coef->G3 = CSC_Coef->G3 * CSC_Ctl->Contrast / 100;
  CSC_Coef->B3 = CSC_Coef->B3 * CSC_Ctl->Contrast / 100;
  CSC_Coef->R3 = CSC_Coef->R3 * CSC_Ctl->Contrast / 100;

  if(CSC_Ctl->OutputColor <= CSC_CTL_OUTPUT_COLOR_HDTV) //RGB to YCbCr, YCbCr to YCbCr
  {
    //saturation control
    CSC_Coef->G2 = CSC_Coef->G2 * CSC_Ctl->Saturation / 100;
    CSC_Coef->B2 = CSC_Coef->B2 * CSC_Ctl->Saturation / 100;
    CSC_Coef->R2 = CSC_Coef->R2 * CSC_Ctl->Saturation / 100;

    CSC_Coef->G3 = CSC_Coef->G3 * CSC_Ctl->Saturation / 100;
    CSC_Coef->B3 = CSC_Coef->B3 * CSC_Ctl->Saturation / 100;
    CSC_Coef->R3 = CSC_Coef->R3 * CSC_Ctl->Saturation / 100;
/*
    //hue control
    CSC_Coef->G2 = (float)(CSC_Coef->G2 * cos(angle_radian) + CSC_Coef->G3 * sin(angle_radian));
    CSC_Coef->B2 = (float)(CSC_Coef->B2 * cos(angle_radian) + CSC_Coef->B3 * sin(angle_radian));
    CSC_Coef->R2 = (float)(CSC_Coef->R2 * cos(angle_radian) + CSC_Coef->R3 * sin(angle_radian));

    CSC_Coef->G3 = (float)(CSC_Coef->G3 * cos(angle_radian) - CSC_Coef->G2 * sin(angle_radian));
    CSC_Coef->B3 = (float)(CSC_Coef->B3 * cos(angle_radian) - CSC_Coef->B2 * sin(angle_radian));
    CSC_Coef->R3 = (float)(CSC_Coef->R3 * cos(angle_radian) - CSC_Coef->R2 * sin(angle_radian));
*/
//  CSC_Coef->OFFS2 = (Range->Output.bcb_mid << 20) -
//                    Range->Input.gy_mid*CSC_Coef->G2 - Range->Input.bcb_mid*CSC_Coef->B2 - 
//                    Range->Input.rcr_mid*CSC_Coef->R2;

//  CSC_Coef->OFFS3 = (Range->Output.rcr_mid << 20) -
//                    Range->Input.gy_mid*CSC_Coef->G3 - Range->Input.bcb_mid*CSC_Coef->B3 - 
//                    Range->Input.rcr_mid*CSC_Coef->R3;
  }
  else //if(CSC_Ctl->OutputColor == CSC_CTL_OUTPUT_COLOR_RGB) // YCbCr to RGB, RGB to RGB
  {
    if(CSC_Ctl->InputColor <= CSC_CTL_INPUT_COLOR_HDTV) // YCbCr to RGB
    {
      //saturation control
      CSC_Coef->B1 = CSC_Coef->B1 * CSC_Ctl->Saturation / 100;
      CSC_Coef->B2 = CSC_Coef->B2 * CSC_Ctl->Saturation / 100;
      CSC_Coef->B3 = CSC_Coef->B3 * CSC_Ctl->Saturation / 100;

      CSC_Coef->R1 = CSC_Coef->R1 * CSC_Ctl->Saturation / 100;
      CSC_Coef->R2 = CSC_Coef->R2 * CSC_Ctl->Saturation / 100;
      CSC_Coef->R3 = CSC_Coef->R3 * CSC_Ctl->Saturation / 100;
/*
      //hue control
      CSC_Coef->B1 = (float)(CSC_Coef->B1 * cos(angle_radian) + CSC_Coef->R1 * sin(angle_radian));
      CSC_Coef->B2 = (float)(CSC_Coef->B2 * cos(angle_radian) + CSC_Coef->R2 * sin(angle_radian));
      CSC_Coef->B3 = (float)(CSC_Coef->B3 * cos(angle_radian) + CSC_Coef->R3 * sin(angle_radian));

      CSC_Coef->R1 = (float)(CSC_Coef->R1 * cos(angle_radian) - CSC_Coef->B1 * sin(angle_radian));
      CSC_Coef->R2 = (float)(CSC_Coef->R2 * cos(angle_radian) - CSC_Coef->B2 * sin(angle_radian));
      CSC_Coef->R3 = (float)(CSC_Coef->R3 * cos(angle_radian) - CSC_Coef->B3 * sin(angle_radian));
*/
    }

    CSC_Coef->OFFS2 = ((Range->Output.bcb_mid + CSC_Ctl->Brightness) << 20) -
                      Range->Input.gy_mid*CSC_Coef->G2 - Range->Input.bcb_mid*CSC_Coef->B2 - 
                      Range->Input.rcr_mid*CSC_Coef->R2;

    CSC_Coef->OFFS3 = ((Range->Output.rcr_mid + CSC_Ctl->Brightness) << 20) -
                      Range->Input.gy_mid*CSC_Coef->G3 - Range->Input.bcb_mid*CSC_Coef->B3 - 
                      Range->Input.rcr_mid*CSC_Coef->R3;
  }

  CSC_Coef->OFFS1 = ((Range->Output.gy_mid + CSC_Ctl->Brightness) << 20) - 
                    Range->Input.gy_mid*CSC_Coef->G1 - Range->Input.bcb_mid*CSC_Coef->B1 - 
                    Range->Input.rcr_mid*CSC_Coef->R1;
}

void ModifyCSC4IORange(CSC_COEFFICIENTS *CSC_Coef, CSC_RANGE *Range)
{
  //Modify CSC coefficients for input/output ranges being used
  CSC_Coef->G1 = CSC_Coef->G1 * (Range->Output.gy_max - Range->Output.gy_min)/
                                (Range->Input.gy_max  - Range->Input.gy_min );
  CSC_Coef->B1 = CSC_Coef->B1 * (Range->Output.gy_max - Range->Output.gy_min)/
                                (Range->Input.bcb_max - Range->Input.bcb_min);
  CSC_Coef->R1 = CSC_Coef->R1 * (Range->Output.gy_max - Range->Output.gy_min)/
                                (Range->Input.rcr_max - Range->Input.rcr_min);
  CSC_Coef->OFFS1 = (Range->Output.gy_mid << 20) - 
                    Range->Input.gy_mid*CSC_Coef->G1 - Range->Input.bcb_mid*CSC_Coef->B1 - 
                    Range->Input.rcr_mid*CSC_Coef->R1;

  CSC_Coef->G2 = CSC_Coef->G2 * (Range->Output.bcb_max - Range->Output.bcb_min)/
                                (Range->Input.gy_max   - Range->Input.gy_min );
  CSC_Coef->B2 = CSC_Coef->B2 * (Range->Output.bcb_max - Range->Output.bcb_min)/
                                (Range->Input.bcb_max  - Range->Input.bcb_min);
  CSC_Coef->R2 = CSC_Coef->R2 * (Range->Output.bcb_max - Range->Output.bcb_min)/
                                (Range->Input.rcr_max  - Range->Input.rcr_min);
  CSC_Coef->OFFS2 = (Range->Output.bcb_mid << 20) - 
                    Range->Input.gy_mid*CSC_Coef->G2 - Range->Input.bcb_mid*CSC_Coef->B2 - 
                    Range->Input.rcr_mid*CSC_Coef->R2;

  CSC_Coef->G3 = CSC_Coef->G3 * (Range->Output.rcr_max - Range->Output.rcr_min)/
                                (Range->Input.gy_max  - Range->Input.gy_min );
  CSC_Coef->B3 = CSC_Coef->B3 * (Range->Output.rcr_max - Range->Output.rcr_min)/
                                (Range->Input.bcb_max - Range->Input.bcb_min);
  CSC_Coef->R3 = CSC_Coef->R3 * (Range->Output.rcr_max - Range->Output.rcr_min)/
                                (Range->Input.rcr_max - Range->Input.rcr_min);
  CSC_Coef->OFFS3 = (Range->Output.rcr_mid << 20) - 
                    Range->Input.gy_mid*CSC_Coef->G3 - Range->Input.bcb_mid*CSC_Coef->B3 - 
                    Range->Input.rcr_mid*CSC_Coef->R3;
}

void InitializeCSC(CSC_COEFFICIENTS *CSC_Coef, CSC_RANGE *Range, CSC_CONTROL *CSC_Ctl)
{
  //Select which set of CSC coefficients to use
  if((CSC_Ctl->InputColor == CSC_CTL_INPUT_COLOR_RGB) && (CSC_Ctl->OutputColor == CSC_CTL_OUTPUT_COLOR_HDTV))
  {//RGB to HDTV YCbCr
    *CSC_Coef = RGB2HDTV;
  }
  else if((CSC_Ctl->InputColor == CSC_CTL_INPUT_COLOR_RGB) && (CSC_Ctl->OutputColor == CSC_CTL_OUTPUT_COLOR_SDTV))
  {//RGB to SDTV YCbCr
    *CSC_Coef = RGB2SDTV;
  }
  else if((CSC_Ctl->InputColor == CSC_CTL_INPUT_COLOR_HDTV) && (CSC_Ctl->OutputColor == CSC_CTL_OUTPUT_COLOR_RGB))
  {//HDTV YCbCr to RGB
    *CSC_Coef = HDTV2RGB;
  }
  else if((CSC_Ctl->InputColor == CSC_CTL_INPUT_COLOR_SDTV) && (CSC_Ctl->OutputColor == CSC_CTL_OUTPUT_COLOR_RGB))
  {//SDTV YCbCr to RGB
    *CSC_Coef = SDTV2RGB;
  }
  else if((CSC_Ctl->InputColor == CSC_CTL_INPUT_COLOR_HDTV) && (CSC_Ctl->OutputColor == CSC_CTL_OUTPUT_COLOR_SDTV))
  {//HDTV YCbCr to SDTV YCbCr
    *CSC_Coef = HDTV2SDTV;
  }
  else if((CSC_Ctl->InputColor == CSC_CTL_INPUT_COLOR_SDTV) && (CSC_Ctl->OutputColor == CSC_CTL_OUTPUT_COLOR_HDTV))
  {//SDTV YCbCr to HDTV YCbCr
    *CSC_Coef = SDTV2HDTV;
  }
  else //if(THS8200->InputColor == THS8200->OutputColor)
  {
    *CSC_Coef = RGB2RGB; //same as SDTV2SDTV and HDTV2HDTV
  }

  //Select which set of input/output range settings to use
  if(CSC_Ctl->InputRange == CSC_CTL_INPUT_RANGE_FULL)
  {
    if(CSC_Ctl->InputColor == CSC_CTL_INPUT_COLOR_RGB)
    {
      Range->Input = Range4RGBFull;
    }
    else
    {
      Range->Input = Range4YCbCrFull;
    }
  }
  else //if(CSC_Ctl->InputRange == CSC_CTL_INPUT_RANGE_LMTD)
  {
    if(CSC_Ctl->InputColor == CSC_CTL_INPUT_COLOR_RGB)
    {
      Range->Input = Range4RGBLmtd;
    }
    else
    {
      Range->Input = Range4YCbCrLmtd;
    }
  }

  if(CSC_Ctl->OutputRange == CSC_CTL_OUTPUT_RANGE_FULL)
  {
    if(CSC_Ctl->OutputColor == CSC_CTL_OUTPUT_COLOR_RGB)
    {
      Range->Output = Range4RGBFull;
    }
    else
    {
      Range->Output = Range4YCbCrFull;
    }
  }
  else //if(CSC_Ctl->OutputRange == CSC_CTL_OUTPUT_RANGE_LMTD)
  {
    if(CSC_Ctl->OutputColor == CSC_CTL_OUTPUT_COLOR_RGB)
    {
      Range->Output = Range4RGBLmtd;
    }
    else
    {
      Range->Output = Range4YCbCrLmtd;
    }
  }
}
