// THS8200.c : Defines the entry point for the console application.

//Version 1.0 Updated March 15th 2012
//Changes
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "THS8200_console_util.h"
#include "ths8200_drv.h"

//#define DEBUG //Used to enable code needed with the debugger

//Create WinVCC command file
void CreateWinVCC(THS8200_APPLICATION *THS8200App, THS8200_CONFIGURATION *THS8200, unsigned int RegSettings[THS8200_MAX_REGISTERS])
{
  FILE *fp;
  float HFreq, VFreq;
  int i, Temp;
  char fileName[256];

  Temp = THS8200->Timing.Hactive + THS8200->Timing.HFP + THS8200->Timing.HSW + THS8200->Timing.HBP;

  HFreq = (THS8200->Timing.PClk * 1000) / Temp;

  Temp = ((THS8200->Timing.Vactive >> THS8200->Timing.IP) + THS8200->Timing.VFP + THS8200->Timing.VSW +
           THS8200->Timing.VBP)*(THS8200->Timing.IP + 1) + THS8200->Timing.IP;

  VFreq = (HFreq * 1000) / Temp;

#ifdef DEBUG //for use with debugger only
  printf("///////////////////////////////////////////////////////////////////////////////\n");
  printf("\n");
  printf("BEGIN_DATASET //WinVCC *.cmd file created by THS8200 configuration software\n");
  printf("\n");
#if 0
  printf("DATASET_NAME,%c%dx%d%s @ %.3fHz, %.3fkHz, %.3fMHz, HS/VS %s/%s, %s %s %s %s>%s, THS8200%c\n",
          '"', THS8200->Timing.Hactive, THS8200->Timing.Vactive, THS8200VertScan[THS8200->Timing.IP], VFreq,
          HFreq, THS8200->Timing.PClk, THS8200SyncPol[THS8200->Timing.HPol], THS8200SyncPol[THS8200->Timing.VPol],
          THS8200InputWidths[THS8200->InputWidth], THS8200InputColor[THS8200->CSC.InputColor],
          THS8200ColorRatio[THS8200->InputRatio], THS8200SyncType[THS8200->InputSyncs],
          THS8200OutputColor[THS8200->CSC.OutputColor], '"');
#endif
  printf("\n");
  for (i = 0x03; i <= 0x85; i++)
  {
    if(RegSettings[i] != -1) //registers 0x7D to 0x81 are read only
    {
      if((THS8200App->PrintMode == 1) || (RegSettings[i] != THS8200RegDefaults[i]))
      {
        if(THS8200App->CommentMode == 0)
        {
          printf("WR_REG,THS8200,0x01,0x%02X,0x%02X // %s\n", i, RegSettings[i], THS8200RegNames[i]);
        }
        else //if(THS8200App->CommentMode == 1)
        {
          printf("WR_REG,THS8200,0x01,0x%02X,0x%02X //default: 0x%02X, %s\n", i, RegSettings[i], THS8200RegDefaults[i], THS8200RegNames[i]);
        }
      }
    }
  }

  printf("\n");
  printf("END_DATASET\n");
  printf("\n");
  printf("///////////////////////////////////////////////////////////////////////////////\n");
#endif

  if(THS8200->GenericDAC == 1) //WIP
  {
    Temp = 40;
  }
  else //if(THS8200->GenericDAC == 0)
  {
    Temp = (THS8200->InputWidth << 3) + ((THS8200->CSC.InputColor & 0x02) << 1) +
          (!THS8200->InputSyncs << 1) + (THS8200->CSC.OutputColor >> 1);

    if(Temp > 39)
    {
      Temp = 39;
    }
  }

  strcpy(fileName, THS8200FileNames[Temp]);

  /* override auto generated file name */
  if(THS8200App->FileName[0]!=0)
    strcpy(fileName, THS8200App->FileName);

  fp = fopen(fileName, THS8200FileModes[THS8200App->FileMode]);

  if(fp==NULL)
  {
    printf(" [THS8200] Unable to open file %s for writing!!!\n", fileName);
    return;
  }

  fprintf(fp,"///////////////////////////////////////////////////////////////////////////////\n");
  fprintf(fp,"\n");
  fprintf(fp,"BEGIN_DATASET\n");
  fprintf(fp,"\n");
  fprintf(fp,"DATASET_NAME,%c%dx%d%s @ %.3fHz, %.3fkHz, %.3fMHz, HS/VS %s/%s, %s %s %s %s>%s, THS8200%c\n",
          '"', THS8200->Timing.Hactive, THS8200->Timing.Vactive, THS8200VertScan[(int)THS8200->Timing.IP], VFreq,
          HFreq, THS8200->Timing.PClk, THS8200SyncPol[THS8200->Timing.HPol], THS8200SyncPol[THS8200->Timing.VPol],
          THS8200InputWidths[THS8200->InputWidth], THS8200InputColor[THS8200->CSC.InputColor],
          THS8200ColorRatio[THS8200->InputRatio], THS8200SyncType[THS8200->InputSyncs],
          THS8200OutputColor[THS8200->CSC.OutputColor], '"');
  fprintf(fp,"\n");
  for (i = 0x03; i <= 0x85; i++)
  {
    if(RegSettings[i] != -1) //registers 0x7D to 0x81 are read only
    {
      if((THS8200App->PrintMode == 1) || (RegSettings[i] != THS8200RegDefaults[i]))
      {
        if(THS8200App->CommentMode == 0)
        {
          fprintf(fp, "WR_REG,THS8200,0x01,0x%02X,0x%02X // %s\n", i, RegSettings[i], THS8200RegNames[i]);
        }
        else //if(THS8200App->CommentMode == 1)
        {
          fprintf(fp, "WR_REG,THS8200,0x01,0x%02X,0x%02X //default: 0x%02X, %s\n", i, RegSettings[i], THS8200RegDefaults[i], THS8200RegNames[i]);
        }
      }
    }
  }

  fprintf(fp,"\n");
  fprintf(fp,"END_DATASET\n");
  fprintf(fp,"\n");
  fprintf(fp,"///////////////////////////////////////////////////////////////////////////////\n");

  fclose (fp);
}

int GetIntParameter(int argc, char* argv[], char* Option, int Default) //Get program arguments
{
  int i;

  for (i = 1; i < argc; i+=2)
  {
    if (strcmp(Option, argv[i]) == 0)
    {
      if ((i + 1) >= argc)
      {
        printf("Missing value for argument %s\n", Option);
        return Default;
      }
      else
      {
        return (atoi(argv[i + 1]));
      }
    }
  }
  return Default;
}

int GetStringParameter(char *inputString, int argc, char* argv[], char* Option, char *Default) //Get program arguments
{
  int i;

  for (i = 1; i < argc; i+=2)
  {
    if (strcmp(Option, argv[i]) == 0)
    {
      if ((i + 1) >= argc)
      {
        printf("Missing value for argument %s\n", Option);
        strcpy( inputString, Default);
        return 0;
      }
      else
      {
        strcpy( inputString, argv[i + 1]);
        return 0;
      }
    }
  }

  strcpy( inputString, Default);
  return 0;
}

int THS8200_console_main(int argc, char* argv[])
{
  THS8200_CONFIGURATION Configuration;
  THS8200_APPLICATION Application;
  unsigned int Registers[THS8200_MAX_REGISTERS];
  int InputTiming, InputFormat;

  THS8200_setDefaultConfig(&Configuration);

  //Select desired video timing standard
  InputTiming = GetIntParameter(argc, argv, "-timing_std", -1); //see VideoTimings.c
  //0: VESA-DMT timing (default)
  //1: CEA-861 timing (supports 487i, 576i and 483p)
  //2: CEA-861 timing (supports 480i, 576i and 480p)
  //3: VESA-CVT timing (WIP)
  //4: VESA-SMT timing (reserved)

  InputFormat = GetIntParameter(argc, argv, "-format", -1); //see VideoTimings.c

  Configuration.Timing.Hactive = GetIntParameter(argc, argv, "-Hactive", -1);
  Configuration.Timing.Vactive = GetIntParameter(argc, argv, "-Vactive", -1);
  Configuration.Timing.PClk = (float)GetIntParameter(argc, argv, "-PClk", -1);
  Configuration.Timing.IP = GetIntParameter(argc, argv, "-IP", -1);
  Configuration.Timing.HFP = GetIntParameter(argc, argv, "-HFP", -1);
  Configuration.Timing.HSW = GetIntParameter(argc, argv, "-HSW", -1);
  Configuration.Timing.HBP = GetIntParameter(argc, argv, "-HBP", -1);
  Configuration.Timing.VFP = GetIntParameter(argc, argv, "-VFP", -1);
  Configuration.Timing.VSW = GetIntParameter(argc, argv, "-VSW", -1);
  Configuration.Timing.VBP = GetIntParameter(argc, argv, "-VBP", -1);
  Configuration.Timing.HPol = GetIntParameter(argc, argv, "-HPol", -1);
  Configuration.Timing.VPol = GetIntParameter(argc, argv, "-VPol", -1);

  //Primary Controls: The following input parameters need to be specified for most applications.

  Configuration.InputWidth = GetIntParameter(argc, argv, "-input_width", 0);
  //0: 30-bit YCbCr/RGB 4:4:4
  //1: 16-bit RGB 4:4:4
  //2: 15-bit RGB 4:4:4
  //3: 20-bit YCbCr 4:2:2
  //4: 10-bit YCbCr 4:2:2 (BT.656 mode)
  Configuration.CSC.OutputColor = GetIntParameter(argc, argv, "-output_color", 0);
  //0: SDTV (YCbCr) color space - 480i, 576i, 480p, 576p
  //1: HDTV (YCbCr) color space - 720p, 1080i, 1080p
  //2: RGB color space
  //3: Reserved
  Configuration.CSC.InputColor = GetIntParameter(argc, argv, "-input_color", -1);
  //0: SDTV (YCbCr) color space - 480i, 576i, 480p, 576p
  //1: HDTV (YCbCr) color space - 720p, 1080i, 1080p
  //2: RGB color space
  //3: Reserved
  Configuration.CSC.InputRange = GetIntParameter(argc, argv, "-input_range", -1);
  //0: Limited range (64 - 940)
  //1: Full range (0 - 1023)
  Configuration.CSC.OutputRange = GetIntParameter(argc, argv, "-output_range", 0); //CSC full-range output has never been used or tested
  //0: Limited range (64 - 940)
  //1: Full range (0 - 1023)
  Configuration.InputSyncs = GetIntParameter(argc, argv, "-input_syncs", -1);
  //0: Embedded Syncs
  //1: Discrete Syncs

  //Secondary Controls: The following input parameters should not need to be specified for most applications.

  Configuration.DTGMode = GetIntParameter(argc, argv, "-dtg_mode", 1);
  //0: Preset
  //1: Generic (default)
  Configuration.TimingMode = GetIntParameter(argc, argv, "-timing_mode", -1);
  //0: SDTV timing (supports bi-level syncs with equalization/serration pulses)
  //1: HDTV timing (supports tri-level syncs with equalization/serration pulses)
  //2: VESA timing (supports bi-level syncs without equalization/serration pulses)
  //3: Reserved
  Configuration.OutputSyncs = GetIntParameter(argc, argv, "-output_syncs", -1);
  //0: Discrete syncs disabled
  //1: Discrete syncs enabled
  Configuration.OutputSOG = GetIntParameter(argc, argv, "-sog", -1);
  //0: SOG disabled
  //1: SOG enabled
  //2: Sync on G, B and R enabled (not supported by software)
  Configuration.UpSampling = GetIntParameter(argc, argv, "-up_samp", 1); //for YPbPr outputs only
  //0: 2x up-sampling disabled
  //1: 2x up-sampling enabled (up to 80MHz) (default)
  Configuration.FID_DE_Cntl = GetIntParameter(argc, argv, "-fid_de_cntl", 0); //for discrete syncs only
  //0: Discrete FID input on Pin 47 (default)
  //1: DE input for VESA preset mode on Pin 47 or internal FID decode
  Configuration.InputSyncPol = GetIntParameter(argc, argv, "-sync_pol", 0); //specifies sync polarity expected from VESA/CEA timing standard
  //0: Normal   (0: Neg, 1: Pos) (default)
  //1: Inverted (0: Pos, 1: Neg)

  //Tertiary Controls: The following input parameters are intended for internal use only.

  Configuration.PowerMgmt = GetIntParameter(argc, argv, "-pwr_mgmt", 0);
  //0: Normal operation (default)
  //1: Digital logic power down
  //2: DAC power down
  //3: DAC and digital logic power down
  Configuration.FIDPolarity = GetIntParameter(argc, argv, "-fid_pol", 0);
  //0: Negative polarity, Field 1 = FID input low (default)
  //1: Positive polarity, Field 1 = FID input high
  Configuration.CSC.CbCrSwap = GetIntParameter(argc, argv, "-cbcr_swap", 0);
  //0: Disabled (default)
  //1: Swap CSC coefficients for B/Cb and R/Cr inputs  (DAC mode)
  //2: Swap CSC coefficients for B/Cb and R/Cr outputs (ADC mode)
  Configuration.BT656Output = GetIntParameter(argc, argv, "-bt656_out", 0); //for 10-bit input mode only
  //0: Disabled (default)
  //1: Enabled
  Configuration.MasterSlave = GetIntParameter(argc, argv, "-master_slave", 0); //for VESA preset mode only
  //0: Slave mode
  //1: Master mode
  Configuration.GenericDAC = GetIntParameter(argc, argv, "-gen_dac", 0); //for 30-bit VESA preset mode only
  //0: Disabled
  //1: Enabled (CSM multiply disabled, dtg1_on disabled)
  Configuration.VideoSyncRatio = GetIntParameter(argc, argv, "-sync_ratio", 0); //for 480i/p outputs only
  //0:  7:3 w/o pedestal (CEA 770.2)
  //1: 10:4 w/  pedestal (CEA 770.1) - obsolete standard
  Configuration.CGMSEnable = GetIntParameter(argc, argv, "-cgms_en", 0); //for SDTV generic 480p mode only
  //0: Disabled
  //1: Enabled

  Configuration.CSC.Brightness = GetIntParameter(argc, argv, "-brightness",   0); //-128 <= Brightness <= +127 counts
  Configuration.CSC.Contrast   = GetIntParameter(argc, argv, "-contrast"  , 100); //   0 <= Contrast   <=  200 percent
  Configuration.CSC.Saturation = GetIntParameter(argc, argv, "-saturation", 100); //   0 <= Saturation <=  200 percent
  Configuration.CSC.Hue        = GetIntParameter(argc, argv, "-hue"       ,   0); // -30 <= Hue        <=  +30 degrees

  //Work in progress: The following input parameters are not currently supported by software.

  Configuration.TestPattern = GetIntParameter(argc, argv, "-color_bars", 0); //for VESA preset mode only
  //0: Disabled
  //1: Enabled

  //Application Controls: The following input parameters affect what's written to the WinVCC *.cmd file.

  Application.FileMode = GetIntParameter(argc, argv, "-append_mode", 0);
  //0: Write, old file content discarded
  //1: Append, old file content preserved
  Application.PrintMode = GetIntParameter(argc, argv, "-print_mode", 0);
  //0: Exclude registers set to default values from command file
  //1: Include all registers in command file
  Application.CommentMode = GetIntParameter(argc, argv, "-add_defaults", 0);
  //0: Exclude register default settings in comments
  //1: Include register default settings in comments

  Application.I2cMode = GetIntParameter(argc, argv, "-i2c_mode", 0);
  // 0: Do not perform I2C write
  // 1: Write registers to I2C device

  Application.I2cDeviceAddr = GetIntParameter(argc, argv, "-i2c_dev_addr", THS8200_I2C_DEFAULT_DEVICE_ADDR);
  // I2C device address to use for THS8200

  Application.DvoConfigMode = GetIntParameter(argc, argv, "-dvo_config", 0);
  // 0: Do NOT config DVO based on THS8200 timings
  // 1: Config DM81xx DVO2 based on THS8200 timings

  GetStringParameter(Application.FileName, argc, argv, "-file_name", "");

  THS8200_getRegs(&Configuration, Registers, InputTiming, InputFormat);

  if(Application.FileMode!=2)
  {
    CreateWinVCC(&Application, &Configuration, Registers);
  }

  if(Application.I2cMode)
  {
    THS8200_i2cConfig(Registers, THS8200_I2C_DEFAULT_INST_ID, Application.I2cDeviceAddr);
  }

  if(Application.DvoConfigMode)
  {
    THS8200_dvoConfig(&Configuration, THS8200_TI81XX_DVO2);
  }

#ifdef DEBUG //for use with debugger only
  while(1);
#endif

  return 0;
}
