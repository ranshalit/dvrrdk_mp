
#include <ths8200_drv.h>
#include <features.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

//#define FAKE_I2C_OPERATION

int THS8200_i2cOpen(unsigned int instId)
{
    char deviceName[20];
    int fd;

    sprintf(deviceName, "/dev/i2c-%d", instId);

    fd = open(deviceName, O_RDWR);

    return fd;
}

int THS8200_i2cRead8(int fd, unsigned int devAddr, unsigned char *reg, unsigned char *value, unsigned int count)
{
    int i, j;
    int status = 0;
    struct i2c_msg * msgs = NULL;
    struct i2c_rdwr_ioctl_data data;

    msgs = (struct i2c_msg *) malloc(sizeof(struct i2c_msg) * count * 2);

    if(msgs==NULL)
    {
        printf(" THS8200 I2C (0x%02x): Malloc ERROR during Read !!! (reg[0x%02x], count = %d)\n", devAddr, reg[0], count);
        return -1;
    }

    for (i = 0, j = 0; i < count * 2; i+=2, j++)
    {
        msgs[i].addr  = devAddr;
        msgs[i].flags = 0;
        msgs[i].len   = 1;
        msgs[i].buf   = &reg[j];

        msgs[i+1].addr  = devAddr;
        msgs[i+1].flags = I2C_M_RD /* | I2C_M_REV_DIR_ADDR */;
        msgs[i+1].len   = 1;
        msgs[i+1].buf   = &value[j];
    }

    data.msgs = msgs;
    data.nmsgs = count * 2;

    status = ioctl(fd, I2C_RDWR, &data);
    if(status < 0)
    {
        status = -1;
        printf(" THS8200 I2C (0x%02x): Read ERROR !!! (reg[0x%02x], count = %d)\n", devAddr, reg[0], count);
    }
    else
        status = 0;

    free(msgs);

    return status;
}

int THS8200_i2cWrite8(int fd, unsigned int devAddr,  unsigned char *reg, unsigned char *value, unsigned int count)
{
    int i,j;
    unsigned char * bufAddr;
    int status = 0;

    struct i2c_msg * msgs = NULL;
    struct i2c_rdwr_ioctl_data data;

    msgs = (struct i2c_msg *) malloc(sizeof(struct i2c_msg) * count);

    if(msgs==NULL)
    {
        printf(" THS8200 I2C (0x%02x): Malloc ERROR during Write !!! (reg[0x%02x], count = %d)\n", devAddr, reg[0], count);
        return -1;
    }

    bufAddr = (unsigned char *) malloc(sizeof(unsigned char) * count * 2);

    if(bufAddr == NULL)
    {
        free(msgs);

        printf(" THS8200 I2C (0x%02x): Malloc ERROR during Write !!! (reg[0x%02x], count = %d)\n", devAddr, reg[0], count);
        return -1;

    }

    for (i = 0, j = 0; i < count; i++, j+=2)
    {
        bufAddr[j] = reg[i];
        bufAddr[j + 1] = value[i];

        msgs[i].addr  = devAddr;
        msgs[i].flags = 0;
        msgs[i].len   = 2;
        msgs[i].buf   = &bufAddr[j];

        #ifdef FAKE_I2C_OPERATION
        printf(" [THS8200] I2C WR (DEV 0x%02x) : 0x%02x = 0x%02x\n", devAddr, bufAddr[j], bufAddr[j+1]);
        #endif
    }
    data.msgs = msgs;
    data.nmsgs = count;

    #ifndef FAKE_I2C_OPERATION
    status = ioctl(fd, I2C_RDWR, &data);
    if(status < 0)
    {
        status = -1;
        printf(" THS8200 I2C (0x%02x): Write ERROR !!! (reg[0x%02x], count = %d)\n", devAddr, reg[0], count);
    }
    else
        status = 0;
    #endif

    free(msgs);
    free(bufAddr);

    return status;
}

int THS8200_i2cClose(int fd)
{
    return close(fd);
}

int THS8200_i2cConfig(THS8200_REGS regs, unsigned int i2cInstId, unsigned int deviceAddr)
{
    int fd, i, count, status;
    unsigned char regAddr[THS8200_MAX_REGS];
    unsigned char regValue[THS8200_MAX_REGS];

    fd = THS8200_i2cOpen(i2cInstId);
    if(fd<0)
    {
        printf(" THS8200: Unable to open i2c instance %d !!!\n", i2cInstId);
        return -1;
    }

    for(count = 0, i = 0; i < THS8200_MAX_REGS; i++)
    {
        if(regs[i]!=(unsigned int)-1)
        {
            regAddr[count] = i;
            regValue[count] = regs[i];
            count++;
        }
    }

    status = THS8200_i2cWrite8(fd, deviceAddr, regAddr, regValue, count);
    THS8200_i2cClose(i2cInstId);

    return status;
}

