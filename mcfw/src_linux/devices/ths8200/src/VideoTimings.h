#ifndef VIDEO_TIMINGS_INCLUDED_
#define VIDEO_TIMINGS_INCLUDED_

typedef struct 
{
  int     SyncType;
  int     PixelRepeat;
  int     Hactive;
  int     Vactive;
  char    IP;
  float   VFreq;
  float   PixelFreq;
  int     HFP;
  int     HSW;
  int     HBP;
  int     VFP;
  int     VSW;
  int     VBP;
  int     EQL;
  int     EQH;
  int     HPol;
  int     VPol;
  int     Format;
} VIDEO_TIMINGS;

extern VIDEO_TIMINGS CEA_Timings[];
extern VIDEO_TIMINGS CEA_REVISED_Timings[]; //revised for THS8200
extern VIDEO_TIMINGS VESA_DMT_Timings[];
extern VIDEO_TIMINGS VESA_CVT_Timings[];
extern VIDEO_TIMINGS VESA_SMT_Timings[];

#endif
