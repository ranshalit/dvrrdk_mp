
#include <ths8200_drv.h>
#include <stdio.h>
#include <string.h>

#define DVO_POLARITY_ACTIVE_HIGH    (0)
#define DVO_POLARITY_ACTIVE_LOW     (1)


int THS8200_dvoSysfsWrite(char *fileName, char *val)
{
    FILE *fp;
    int ret;

    printf(" [THS8200] DVO: %s = %s \n", fileName, val);

    fp = fopen(fileName, "w");
    if (fp == NULL) {
        printf(" [THS8200] Failed to open sysfs file [%s] for writing !!!\n", fileName);
        return -1;
    }
    ret = fwrite(val, strlen(val) + 1, 1, fp);
    if ( ret != 1) {
        printf(" [THS8200] Failed to write sysfs value [%s] to file [%s] !!!\n", fileName, val);
        fclose(fp);
        return -1;
    }
    fflush(fp);
    fclose(fp);
    return 0;
}

int THS8200_dvoConfig(THS8200_CONFIGURATION *cfg, unsigned int dvoInstId)
{
    char displaySysFsFileName[256];
    char sysfsFile[256];
    char sysfsValue[256];
    char busWidth[16];
    char dataFormat[16];
    int hsPol, vsPol, fidPol, dePol;

    if(dvoInstId==THS8200_TI81XX_DVO1)
        sprintf(displaySysFsFileName, "/sys/devices/platform/vpss/display0");
    else
        sprintf(displaySysFsFileName, "/sys/devices/platform/vpss/display1");

    /* disable display if enabled */
    sprintf(sysfsFile, "%s/enabled", displaySysFsFileName);
    THS8200_dvoSysfsWrite(sysfsFile, "0");

    /* set output mode */
    sprintf(sysfsFile, "%s/output", displaySysFsFileName);

    if(cfg->InputWidth==THS8200_INPUT_WIDTH_10 && cfg->InputSyncs==THS8200_INPUT_SYNCS_ES)
    {
        strcpy(busWidth, "single");
    }
    else
    if(cfg->InputWidth==THS8200_INPUT_WIDTH_20 && cfg->InputSyncs==THS8200_INPUT_SYNCS_ES)
    {
        strcpy(busWidth, "double");
    }
    else
    if(cfg->InputWidth==THS8200_INPUT_WIDTH_30 && cfg->InputSyncs==THS8200_INPUT_SYNCS_ES)
    {
        strcpy(busWidth, "triple");
    }
    else
    if(cfg->InputWidth==THS8200_INPUT_WIDTH_20 && cfg->InputSyncs==THS8200_INPUT_SYNCS_DS)
    {
        strcpy(busWidth, "doublediscrete");
    }
    else
    if(cfg->InputWidth==THS8200_INPUT_WIDTH_30 && cfg->InputSyncs==THS8200_INPUT_SYNCS_DS)
    {
        strcpy(busWidth, "triplediscrete");
    }
    else
    {
        printf(" [THS8200] DVO: output mode not supported, defaulting to 16-bit BT656 mode !!!\n");
        strcpy(busWidth, "double");
    }

    if(cfg->InputRatio==THS8200_INPUT_RATIO_422)
    {
        strcpy(dataFormat, "yuv422spuv");
    }
    else
    if(cfg->InputRatio==THS8200_INPUT_RATIO_444)
    {
        if(cfg->CSC.InputColor==CSC_CTL_INPUT_COLOR_RGB)
        {
            strcpy(dataFormat, "rgb888");
        }
        else
        if( cfg->CSC.InputColor==CSC_CTL_INPUT_COLOR_SDTV
                ||
            cfg->CSC.InputColor==CSC_CTL_INPUT_COLOR_HDTV
            )
        {
            strcpy(dataFormat, "yuv444p");
        }
        else
        {
            printf(" [THS8200] DVO: Unsupported input color !!!\n");
            strcpy(dataFormat, "rgb888");
        }

    }
    else
    {
        printf(" [THS8200] DVO: Unsupported input ratio, defaulting to YUV422 !!!\n");
        strcpy(dataFormat, "yuv422spuv");
    }

    dePol  = DVO_POLARITY_ACTIVE_HIGH;
    hsPol  = cfg->Timing.HPol^cfg->InputSyncPol;
    vsPol  = cfg->Timing.VPol^cfg->InputSyncPol;
    fidPol = cfg->FIDPolarity;

    sprintf(sysfsValue,"%s,%s,%d/%d/%d/%d",
        busWidth,
        dataFormat,
        dePol,
        fidPol,
        hsPol,
        vsPol
        );

    THS8200_dvoSysfsWrite(sysfsFile, sysfsValue);

    /* set timings */
    sprintf(sysfsFile, "%s/timings", displaySysFsFileName);

    sprintf(sysfsValue,"%d,%d/%d/%d/%d,%d/%d/%d/%d,%d",
        (unsigned int)(cfg->Timing.PClk*1000), /* MUST be specified in KHz */
        cfg->Timing.Hactive,
        cfg->Timing.HFP,
        cfg->Timing.HBP,
        cfg->Timing.HSW,
        cfg->Timing.Vactive,
        cfg->Timing.VFP,
        cfg->Timing.VBP,
        cfg->Timing.VSW,
        !cfg->Timing.IP /* 1 = progressive, 0 = interlaced @ sysfs */
        );

    THS8200_dvoSysfsWrite(sysfsFile, sysfsValue);

    return 0;
}
