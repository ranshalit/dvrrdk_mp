// Low level THS8200 configuration code

//Version 1.0 Updated March 15th 2012
//Changes
//

#include "THS8200Config.h"

//SOG blanking and sync levels (referred to DAC inputs)
//For 7:3 video:sync ratio without pedestal
//HDTV Peak Video: 1000 * (7 / 10) + 0.5 = 700mV (from blanking level)
//HDTV Sync Tip:   1000 * (3 / 10) + 0.5 = 300mV (from blanking level)

//HDTV Blanking:                         512 = 0x200 => 350mV
//HDTV Sync Low:  512 - (1023 * 3 / 7) =  73 = 0x049 =>  50mV
//HDTV Sync High: 512 + (1023 * 3 / 7) = 950 = 0x3B6 => 650mV

                                      //  Y,   C
const THS8200_SOG SyncLevels4SDTV   = { 512, 512,  //blank levels
                                         73, 512,  //sync low levels
                                        512, 512}; //sync high levels

                                      //  Y,   C
const THS8200_SOG SyncLevels4HDTV   = { 512, 512,  //blank levels
                                         73, 512,  //sync low levels
                                        950, 512}; //sync high levels

//For VESA preset mode
                                      //  Y,   C
const THS8200_SOG SyncLevels4VESA_P = {   0,   0,  //blank levels
                                          0,   0,  //sync low levels
                                          0,   0}; //sync high levels

//For VESA generic mode
                                      //  Y,   C
const THS8200_SOG SyncLevels4VESA_G = { 512,   0,  //blank levels
                                        512,   0,  //sync low levels
                                        512,   0}; //sync high levels

//For VESA generic mode (alternate, not used)
                                      //  Y,   C
const THS8200_SOG SyncLevels4VESA_G2 = {512, 512,  //blank levels
                                        512, 512,  //sync low levels
                                        512, 512}; //sync high levels

//For 10:4 video:sync ratio with pedestal
//SDTV Peak Video: 1000 * (100 / 140) + 0.5 = 714mV (from blanking level)
//SDTV Sync Tip:   1000 * ( 40 / 140) + 0.5 = 286mV (from blanking level)
//SDTV Pedestal:   1000 * (7.5 / 140) + 0.5 =  54mV (from blanking level)

//SDTV Sync Low:         1023 *  50 / 700 =  73 = 0x049 =>  50mV
//SDTV Blanking:  1023 * (286 + 50) / 700 = 491 = 0x1EB => 336mV
//SDTV Partial Pedestal: 350 - 336 = 14mV of 54mV

                                      //  Y,   C
const THS8200_SOG SyncLevels4SDTV_2 = { 491, 512,  //blank levels
                                         73, 512,  //sync low levels
                                        491, 512}; //sync high levels

//CSM settings
//For 7:3 video:sync ratio without pedestal
//YCbCrLmtd Y Gain:    1024*1023/(940-64) + 0.5 = 1196 = 0x4AC
//YCbCrLmtd CbCr Gain: 1024*1023/(960-64) + 0.5 = 1169 = 0x491
//YCbCr/RGBFull Gain:  1024*1023/(1023-0) + 0.5 = 1024 = 0x400 (unity gain)

                                      //G/Y, B/Cb, R/Cr
const THS8200_CSM CSMSet4YCbCrLmtd  = {  64,   64,   64,  //clip low settings (default)
                                         83,   63,   63,  //clip high settings (default), 1023 - 83 = 940, 1023 - 63 = 960
                                         64,   64,   64,  //shift downward settings
                                       1196, 1169, 1169,  //multiply settings
                                          1,    1,    1,  //clip low enable
                                          1,    1,    1,  //clip high enable
                                          1,    1,    1,  //shift enable
                                          1,    1,    1}; //multiply enable (pipeline delay increased by 1 pixel when on)

                                      //G/Y, B/Cb, R/Cr
const THS8200_CSM CSMSet4RGBLmtd    = {  64,   64,   64,  //clip low settings (default)
                                         83,   63,   63,  //clip high settings (default), 1023 - 83 = 940, 1023 - 63 = 960
                                         64,   64,   64,  //shift downward settings (default)
                                       1196, 1196, 1196,  //multiply settings
                                          1,    1,    1,  //clip low enable
                                          1,    1,    1,  //clip high enable
                                          1,    1,    1,  //shift enable
                                          1,    1,    1}; //multiply enable (pipeline delay increased by 1 pixel when on)

                                      //G/Y, B/Cb, R/Cr
const THS8200_CSM CSMSet4YCbYcFull  = {  64,   64,   64,  //clip low settings (default)
                                         83,   63,   63,  //clip high settings (default), 1023 - 83 = 940, 1023 - 63 = 960
                                         64,   64,   64,  //shift downward settings (default)
                                       1024, 1024, 1024,  //multiply settings (unity gain)
                                          0,    0,    0,  //clip low enable
                                          0,    0,    0,  //clip high enable
                                          0,    0,    0,  //shift enable
                                          1,    1,    1}; //multiply enable (pipeline delay increased by 1 pixel when on)

                                      //G/Y, B/Cb, R/Cr
const THS8200_CSM CSMSet4RGBFull    = {  64,   64,   64,  //clip low settings (default)
                                         83,   63,   63,  //clip high settings (default)
                                         64,   64,   64,  //shift downward settings (default)
                                       1024, 1024, 1024,  //multiply settings (unity gain)
                                          0,    0,    0,  //clip low enable
                                          0,    0,    0,  //clip high enable
                                          0,    0,    0,  //shift enable
                                          1,    1,    1}; //multiply enable (pipeline delay increased by 1 pixel when on)
//For Generic DAC mode
                                      //G/Y, B/Cb, R/Cr
const THS8200_CSM CSMSet4GenericDAC = {  64,   64,   64,  //clip low settings (default)
                                         83,   63,   63,  //clip high settings (default)
                                         64,   64,   64,  //shift downward settings (default)
                                       1024, 1024, 1024,  //multiply settings (unity gain)
                                          0,    0,    0,  //clip low enable
                                          0,    0,    0,  //clip high enable
                                          0,    0,    0,  //shift enable
                                          0,    0,    0}; //multiply enable (pipeline delay increased by 1 pixel when on)

//For 10:4 video:sync ratio with pedestal
//SDTV Peak Video: 1000 * (100 / 140) + 0.5 = 714mV (from blanking level)
//SDTV Sync Tip:   1000 * ( 40 / 140) + 0.5 = 286mV (from blanking level)
//SDTV Pedestal:   1000 * (7.5 / 140) + 0.5 =  54mV (from blanking level)
//SDTV Video Height:              714 -  54 = 660mV
//SDTV Partial Pedestal:          700 - 660 =  40mV of 54mV

//YCbCrLmtd Y Gain:  1024*(1023/700)*[(714-54)/(940-64)] + 0.5 = 1128 = 0x468
//YCbCrLmtd Y Shift: 64 - (1024/1128)*(1023/700)*(700-660) + 0.5 = 11 = 0x0B

                                      //G/Y, B/Cb, R/Cr
const THS8200_CSM CSMSet4YCbCrLmtd_2 = { 64,   64,   64,  //clip low settings (default),
                                         83,   63,   63,  //clip high settings (default), 1023 - 83 = 940, 1023 - 63 = 960
                                         11,   64,   64,  //shift downward settings
                                       1128, 1169, 1169,  //multiply settings
                                          1,    1,    1,  //clip low enable
                                          1,    1,    1,  //clip high enable
                                          1,    1,    1,  //shift enable
                                          1,    1,    1}; //multiply enable (always on)

//Default values to be used unless later modified
void THS8200ConfigSetDefaults(THS8200_CFG *THS8200Cfg)
{
  //System
  THS8200Cfg->reg03_vesa_clk                = 0x00; //bit 7 of Reg 0x03
  THS8200Cfg->reg03_dll_bypass              = 0x00; //bit 6 of Reg 0x03
  THS8200Cfg->reg03_vesa_color_bars         = 0x00; //bit 5 of Reg 0x03
  THS8200Cfg->reg03_dll_freq_sel            = 0x00; //bit 4 of Reg 0x03
  THS8200Cfg->reg03_dac_pwdn                = 0x00; //bit 3 of Reg 0x03
  THS8200Cfg->reg03_chip_pwdn               = 0x00; //bit 2 of Reg 0x03
  THS8200Cfg->reg03_chip_ms                 = 0x00; //bit 1 of Reg 0x03
  THS8200Cfg->reg03_arst_func_n             = 0x01; //bit 0 of Reg 0x03, active-low chip software reset

  //Color Space Conversion
  THS8200Cfg->reg04_csc_ric1                = 0x00;
  THS8200Cfg->reg04_csc_rfc1_msb            = 0x00;
  THS8200Cfg->reg05_csc_rfc1_lsb            = 0x00;
  THS8200Cfg->reg06_csc_ric2                = 0x00;
  THS8200Cfg->reg06_csc_rfc2_msb            = 0x00;
  THS8200Cfg->reg07_csc_rfc2_lsb            = 0x00;
  THS8200Cfg->reg08_csc_ric3                = 0x00;
  THS8200Cfg->reg08_csc_rfc3_msb            = 0x00;
  THS8200Cfg->reg09_csc_rfc3_lsb            = 0x00;
  THS8200Cfg->reg0A_csc_gic1                = 0x00;
  THS8200Cfg->reg0A_csc_gfc1_msb            = 0x00;
  THS8200Cfg->reg0B_csc_gfc1_lsb            = 0x00;
  THS8200Cfg->reg0C_csc_gic2                = 0x00;
  THS8200Cfg->reg0C_csc_gfc2_msb            = 0x00;
  THS8200Cfg->reg0D_csc_gfc2_lsb            = 0x00;
  THS8200Cfg->reg0E_csc_gic3                = 0x00;
  THS8200Cfg->reg0E_csc_gfc3_msb            = 0x00;
  THS8200Cfg->reg0F_csc_gfc3_lsb            = 0x00;
  THS8200Cfg->reg10_csc_bic1                = 0x00;
  THS8200Cfg->reg10_csc_bfc1_msb            = 0x00;
  THS8200Cfg->reg11_csc_bfc1_lsb            = 0x00;
  THS8200Cfg->reg12_csc_bic2                = 0x00;
  THS8200Cfg->reg12_csc_bfc2_msb            = 0x00;
  THS8200Cfg->reg13_csc_bfc2_lsb            = 0x00;
  THS8200Cfg->reg14_csc_bic3                = 0x00;
  THS8200Cfg->reg14_csc_bfc3_msb            = 0x00;
  THS8200Cfg->reg15_csc_bfc3_lsb            = 0x00;
  THS8200Cfg->reg16_csc_offset1_msb         = 0x00;
  THS8200Cfg->reg17_csc_offset1_lsb         = 0x00;
  THS8200Cfg->reg17_csc_offset2_msb         = 0x00;
  THS8200Cfg->reg18_csc_offset2_lsb         = 0x00;
  THS8200Cfg->reg18_csc_offset3_msb         = 0x00;
  THS8200Cfg->reg19_csc_offset3_lsb         = 0x00;
  THS8200Cfg->reg19_csc_bypass              = 0x01; //CSC bypassed
  THS8200Cfg->reg19_c_uof_cntl              = 0x01; //CSC under/overflow control enabled

  //Test
  THS8200Cfg->reg1A_tst_digbpass            = 0x00;
  THS8200Cfg->reg1A_tst_offset              = 0x00;
  THS8200Cfg->reg1B_tst_ydelay              = 0x00;
  THS8200Cfg->reg1B_tst_fastramp            = 0x00;
  THS8200Cfg->reg1B_tst_slowramp            = 0x00;

  //Data Path
  THS8200Cfg->reg1C_data_clk656_on          = 0x00; //bit 7 of Reg 0x1C
  THS8200Cfg->reg1C_data_fsadj              = 0x00; //bit 6 of Reg 0x1C
  THS8200Cfg->reg1C_data_ifir12_bypass      = 0x00; //bit 5 of Reg 0x1C, 4:2:2 to 4:4:4 conversion not bypassed
  THS8200Cfg->reg1C_data_ifir35_bypass      = 0x00; //bit 4 of Reg 0x1C, 2x up-sampling not bypassed
  THS8200Cfg->reg1C_data_tristate656        = 0x00; //bit 3 of Reg 0x1C
  THS8200Cfg->reg1C_data_dman_cntl          = 0x03; //bits 2:0 of Reg 0x1C, 20-bit YCbCr 4:2:2

  //Display Timing Generation, Part 1
  THS8200Cfg->reg1D_dtg1_y_blank_lsb        = 0x00;
  THS8200Cfg->reg1E_dtg1_y_sync_low_lsb     = 0x00;
  THS8200Cfg->reg1F_dtg1_y_sync_high_lsb    = 0x00;
  THS8200Cfg->reg20_dtg1_cbcr_blank_lsb     = 0x00;
  THS8200Cfg->reg21_dtg1_cbcr_sync_low_lsb  = 0x00;
  THS8200Cfg->reg22_dtg1_cbcr_sync_high_lsb = 0x00;
  THS8200Cfg->reg23_dtg1_y_blank_msb        = 0x02;
  THS8200Cfg->reg23_dtg1_y_sync_low_msb     = 0x00;
  THS8200Cfg->reg23_dtg1_y_sync_high_msb    = 0x03;
  THS8200Cfg->reg24_dtg1_cbcr_blank_msb     = 0x02;
  THS8200Cfg->reg24_dtg1_cbcr_sync_low_msb  = 0x00;
  THS8200Cfg->reg24_dtg1_cbcr_sync_high_msb = 0x03;
  THS8200Cfg->reg25_dtg1_spec_a             = 0x2C;
  THS8200Cfg->reg26_dtg1_spec_b             = 0x58;
  THS8200Cfg->reg27_dtg1_spec_c             = 0x2C;
  THS8200Cfg->reg28_dtg1_spec_d_lsb         = 0x84;
  THS8200Cfg->reg29_dtg1_spec_d1            = 0x00;
  THS8200Cfg->reg2A_dtg1_spec_e_lsb         = 0xC0;
  THS8200Cfg->reg2B_dtg1_spec_d_msb         = 0x00;
  THS8200Cfg->reg2B_dtg1_spec_e_msb         = 0x00;
  THS8200Cfg->reg2B_dtg1_spec_h_msb         = 0x00;
  THS8200Cfg->reg2C_dtg1_spec_h_lsb         = 0x00;
  THS8200Cfg->reg2D_dtg1_spec_i_msb         = 0x00;
  THS8200Cfg->reg2E_dtg1_spec_i_lsb         = 0x00;
  THS8200Cfg->reg2F_dtg1_spec_k_lsb         = 0x58;
  THS8200Cfg->reg30_dtg1_spec_k_msb         = 0x00;
  THS8200Cfg->reg31_dtg1_spec_k1            = 0x00;
  THS8200Cfg->reg32_dtg1_spec_g_lsb         = 0x58;
  THS8200Cfg->reg33_dtg1_spec_g_msb         = 0x00;
  THS8200Cfg->reg34_dtg1_total_pixels_msb   = 0x05;
  THS8200Cfg->reg35_dtg1_total_pixels_lsb   = 0x20;
  THS8200Cfg->reg36_dtg1_field_flip         = 0x00;
  THS8200Cfg->reg36_dtg1_linecnt_msb        = 0x00;
  THS8200Cfg->reg37_dtg1_linecnt_lsb        = 0x01;
  THS8200Cfg->reg38_dtg1_on                 = 0x01;
  THS8200Cfg->reg38_dtg1_pass_through       = 0x00;
  THS8200Cfg->reg38_dtg1_mode               = 0x06;
  THS8200Cfg->reg39_dtg1_frame_size_msb     = 0x03;
  THS8200Cfg->reg39_dtg1_field_size_msb     = 0x00;
  THS8200Cfg->reg3A_dtg1_frame_size_lsb     = 0x00;
  THS8200Cfg->reg3B_dtg1_field_size_lsb     = 0x20;
  THS8200Cfg->reg3C_dtg1_vesa_cbar_size     = 0x80;

  //DAC
  THS8200Cfg->reg3D_dac_i2c_cntl            = 0x00;
  THS8200Cfg->reg3D_dac1_cntl_msb           = 0x00;
  THS8200Cfg->reg3D_dac2_cntl_msb           = 0x00;
  THS8200Cfg->reg3D_dac3_cntl_msb           = 0x00;
  THS8200Cfg->reg3E_dac1_cntl_lsb           = 0x00;
  THS8200Cfg->reg3F_dac2_cntl_lsb           = 0x00;
  THS8200Cfg->reg40_dac3_cntl_lsb           = 0x00;

  //Clip/Shift/Multiplier
  THS8200Cfg->reg41_csm_clip_gy_low         = 0x40;
  THS8200Cfg->reg42_csm_clip_bcb_low        = 0x40;
  THS8200Cfg->reg43_csm_clip_rcr_low        = 0x40;
  THS8200Cfg->reg44_csm_clip_gy_high        = 0x53;
  THS8200Cfg->reg45_csm_clip_bcb_high       = 0x3F;
  THS8200Cfg->reg46_csm_clip_rcr_high       = 0x3F;
  THS8200Cfg->reg47_csm_shift_gy            = 0x40;
  THS8200Cfg->reg48_csm_shift_bcb           = 0x40;
  THS8200Cfg->reg49_csm_shift_rcr           = 0x40;
  THS8200Cfg->reg4A_csm_mult_gy_on          = 0x00; //bit 7 of Reg 0x4A
  THS8200Cfg->reg4A_csm_shift_gy_on         = 0x00; //bit 6 of Reg 0x4A
  THS8200Cfg->reg4A_csm_gy_high_clip_on     = 0x00; //bit 5 of Reg 0x4A
  THS8200Cfg->reg4A_csm_gy_low_clip_on      = 0x00; //bit 4 of Reg 0x4A
  THS8200Cfg->reg4A_csm_of_cntl             = 0x01; //bit 3 of Reg 0x4A, CSM overflow control enabled
  THS8200Cfg->reg4A_csm_mult_gy_msb         = 0x00; //bits 2:0 of Reg 0x4A
  THS8200Cfg->reg4B_csm_mult_bcb_msb        = 0x00;
  THS8200Cfg->reg4B_csm_mult_rcr_msb        = 0x00;
  THS8200Cfg->reg4C_csm_mult_gy_lsb         = 0x00;
  THS8200Cfg->reg4D_csm_mult_bcb_lsb        = 0x00;
  THS8200Cfg->reg4E_csm_mult_rcr_lsb        = 0x00;
  THS8200Cfg->reg4F_csm_mult_rcr_on         = 0x00; //bit 7 of Reg 0x4F
  THS8200Cfg->reg4F_csm_mult_bcb_on         = 0x00; //bit 6 of Reg 0x4F
  THS8200Cfg->reg4F_csm_shift_rcr_on        = 0x00; //bit 5 of Reg 0x4F
  THS8200Cfg->reg4F_csm_shift_bcb_on        = 0x00; //bit 4 of Reg 0x4F
  THS8200Cfg->reg4F_csm_rcr_high_clip_on    = 0x00; //bit 3 of Reg 0x4F
  THS8200Cfg->reg4F_csm_rcr_low_clip_on     = 0x00; //bit 2 of Reg 0x4F
  THS8200Cfg->reg4F_csm_bcb_high_clip_on    = 0x00; //bit 1 of Reg 0x4F
  THS8200Cfg->reg4F_csm_bcb_low_clip_on     = 0x00; //bit 0 of Reg 0x4F

  //Display Timing Generation, Part 2
  THS8200Cfg->reg50_dtg2_bp1_msb            = 0x00;
  THS8200Cfg->reg50_dtg2_bp2_msb            = 0x00;
  THS8200Cfg->reg51_dtg2_bp3_msb            = 0x00;
  THS8200Cfg->reg51_dtg2_bp4_msb            = 0x00;
  THS8200Cfg->reg52_dtg2_bp5_msb            = 0x00;
  THS8200Cfg->reg52_dtg2_bp6_msb            = 0x00;
  THS8200Cfg->reg53_dtg2_bp7_msb            = 0x00;
  THS8200Cfg->reg53_dtg2_bp8_msb            = 0x00;
  THS8200Cfg->reg54_dtg2_bp9_msb            = 0x00;
  THS8200Cfg->reg54_dtg2_bp10_msb           = 0x00;
  THS8200Cfg->reg55_dtg2_bp11_msb           = 0x00;
  THS8200Cfg->reg55_dtg2_bp12_msb           = 0x00;
  THS8200Cfg->reg56_dtg2_bp13_msb           = 0x00;
  THS8200Cfg->reg56_dtg2_bp14_msb           = 0x00;
  THS8200Cfg->reg57_dtg2_bp15_msb           = 0x00;
  THS8200Cfg->reg57_dtg2_bp16_msb           = 0x00;
  THS8200Cfg->reg58_dtg2_bp1_lsb            = 0x00;
  THS8200Cfg->reg59_dtg2_bp2_lsb            = 0x00;
  THS8200Cfg->reg5A_dtg2_bp3_lsb            = 0x00;
  THS8200Cfg->reg5B_dtg2_bp4_lsb            = 0x00;
  THS8200Cfg->reg5C_dtg2_bp5_lsb            = 0x00;
  THS8200Cfg->reg5D_dtg2_bp6_lsb            = 0x00;
  THS8200Cfg->reg5E_dtg2_bp7_lsb            = 0x00;
  THS8200Cfg->reg5F_dtg2_bp8_lsb            = 0x00;
  THS8200Cfg->reg60_dtg2_bp9_lsb            = 0x00;
  THS8200Cfg->reg61_dtg2_bp10_lsb           = 0x00;
  THS8200Cfg->reg62_dtg2_bp11_lsb           = 0x00;
  THS8200Cfg->reg63_dtg2_bp12_lsb           = 0x00;
  THS8200Cfg->reg64_dtg2_bp13_lsb           = 0x00;
  THS8200Cfg->reg65_dtg2_bp14_lsb           = 0x00;
  THS8200Cfg->reg66_dtg2_bp15_lsb           = 0x00;
  THS8200Cfg->reg67_dtg2_bp16_lsb           = 0x00;
  THS8200Cfg->reg68_dtg2_linetype1          = 0x00;
  THS8200Cfg->reg68_dtg2_linetype2          = 0x00;
  THS8200Cfg->reg69_dtg2_linetype3          = 0x00;
  THS8200Cfg->reg69_dtg2_linetype4          = 0x00;
  THS8200Cfg->reg6A_dtg2_linetype5          = 0x00;
  THS8200Cfg->reg6A_dtg2_linetype6          = 0x00;
  THS8200Cfg->reg6B_dtg2_linetype7          = 0x00;
  THS8200Cfg->reg6B_dtg2_linetype8          = 0x00;
  THS8200Cfg->reg6C_dtg2_linetype9          = 0x00;
  THS8200Cfg->reg6C_dtg2_linetype10         = 0x00;
  THS8200Cfg->reg6D_dtg2_linetype11         = 0x00;
  THS8200Cfg->reg6D_dtg2_linetype12         = 0x00;
  THS8200Cfg->reg6E_dtg2_linetype13         = 0x00;
  THS8200Cfg->reg6E_dtg2_linetype14         = 0x00;
  THS8200Cfg->reg6F_dtg2_linetype15         = 0x00;
  THS8200Cfg->reg6F_dtg2_linetype16         = 0x00;
  THS8200Cfg->reg70_dtg2_hlength_lsb        = 0x60;
  THS8200Cfg->reg71_dtg2_hlength_msb        = 0x00;
  THS8200Cfg->reg71_dtg2_hdly_msb           = 0x00;
  THS8200Cfg->reg72_dtg2_hdly_lsb           = 0x02; //use to align image on display
  THS8200Cfg->reg73_dtg2_vlength1_lsb       = 0x03;
  THS8200Cfg->reg74_dtg2_vlength1_msb       = 0x00;
  THS8200Cfg->reg74_dtg2_vdly1_msb          = 0x00;
  THS8200Cfg->reg75_dtg2_vdly1_lsb          = 0x03;
  THS8200Cfg->reg76_dtg2_vlength2_lsb       = 0x00; //must be set to 0x00 for progressive modes
  THS8200Cfg->reg77_dtg2_vlength2_msb       = 0x00; //must be set to 0x00 for progressive modes
  THS8200Cfg->reg77_dtg2_vdly2_msb          = 0x07; //must be set to 0x07 for progressive modes
  THS8200Cfg->reg78_dtg2_vdly2_lsb          = 0xFF; //must be set to 0xFF for progressive modes
  THS8200Cfg->reg79_dtg2_hs_in_dly_msb      = 0x00;
  THS8200Cfg->reg7A_dtg2_hs_in_dly_lsb      = 0x3D; //use to align image outside cropping interval
  THS8200Cfg->reg7B_dtg2_vs_in_dly_msb      = 0x00;
  THS8200Cfg->reg7C_dtg2_vs_in_dly_lsb      = 0x03;
//THS8200Cfg->reg7D_dtg2_pixel_cnt_msb      = 0x00; //status register
//THS8200Cfg->reg7E_dtg2_pixel_cnt_lsb      = 0x00; //status register
//THS8200Cfg->reg7F_dtg2_ip_fmt             = 0x00; //status register
//THS8200Cfg->reg7F_dtg2_line_cnt_msb       = 0x00; //status register
//THS8200Cfg->reg80_dtg2_line_cnt_lsb       = 0x00; //status register
//THS8200Cfg->reg81_reserved3               = 0x00; //reserved
  THS8200Cfg->reg82_dtg2_fid_de_cntl        = 0x00; //bit 7 of Reg 0x82
  THS8200Cfg->reg82_dtg2_rgb_mode_on        = 0x01; //bit 6 of Reg 0x82, RGB mode enabled
  THS8200Cfg->reg82_dtg2_embedded_timing    = 0x00; //bit 5 of Reg 0x82
  THS8200Cfg->reg82_dtg2_vsout_pol          = 0x01; //bit 4 of Reg 0x82, VSYNC output polarity = positive
  THS8200Cfg->reg82_dtg2_hsout_pol          = 0x01; //bit 3 of Reg 0x82, HSYNC output polarity = positive
  THS8200Cfg->reg82_dtg2_fid_pol            = 0x01; //bit 2 of Reg 0x82, FID input polarity = positive
  THS8200Cfg->reg82_dtg2_vs_pol             = 0x01; //bit 1 of Reg 0x82, VSYNC input polarity = positive
  THS8200Cfg->reg82_dtg2_hs_pol             = 0x01; //bit 0 of Reg 0x82, HSYNC input polarity = positive

  //CGMS Control
  THS8200Cfg->reg83_cgms_en                 = 0x00;
  THS8200Cfg->reg83_cgms_header             = 0x00;
  THS8200Cfg->reg84_cgms_payload_msb        = 0x00;
  THS8200Cfg->reg85_cgms_payload_lsb        = 0x00;
}

//Color Space Conversion

//Yd = G1*Gd + B1*Bd + R1*Rd + OFFS1
//Cb = G2*Gd + B2*Bd + R2*Rd + OFFS2
//Cr = G3*Gd + B3*Bd + R3*Rd + OFFS3

//Gd = G1*Yd + B1*Cb + R1*Cr + OFFS1
//Bd = G2*Yd + B2*Cb + R2*Cr + OFFS2
//Rd = G3*Yd + B3*Cb + R3*Cr + OFFS3

//Yd = G1*Yd + B1*Cb + R1*Cr + OFFS1
//Cb = G2*Yd + B2*Cb + R2*Cr + OFFS2
//Cr = G3*Yd + B3*Cb + R3*Cr + OFFS3

//CSC bit field nomenclature is: csc_<input><part>c<output> where:
//input : g = Gd/Yd, b = Bd/Cb, r = Rd/Cr
//part  : i = integer, f = fractional
//output: 1 = Yd/Gd, 2 = Cb/Bd, 3 = Cr/Rd

void THS8200SetCSC(THS8200_CFG *THS8200Cfg, CSC_COEFFICIENTS *CSC)
{
  int Temp;

  Temp = (THS8200_SIGN(CSC->R1) << 15) + (THS8200_ABS(CSC->R1 + 0x00000200) >> 10);

  THS8200Cfg->reg04_csc_ric1                = (Temp >> 10) & 0x3F;
  THS8200Cfg->reg04_csc_rfc1_msb            = (Temp >>  8) & 0x03;
  THS8200Cfg->reg05_csc_rfc1_lsb            = (Temp >>  0) & 0xFF;

  Temp = (THS8200_SIGN(CSC->R2) << 15) + (THS8200_ABS(CSC->R2 + 0x00000200) >> 10);

  THS8200Cfg->reg06_csc_ric2                = (Temp >> 10) & 0x3F;
  THS8200Cfg->reg06_csc_rfc2_msb            = (Temp >>  8) & 0x03;
  THS8200Cfg->reg07_csc_rfc2_lsb            = (Temp >>  0) & 0xFF;

  Temp = (THS8200_SIGN(CSC->R3) << 15) + (THS8200_ABS(CSC->R3 + 0x00000200) >> 10);

  THS8200Cfg->reg08_csc_ric3                = (Temp >> 10) & 0x3F;
  THS8200Cfg->reg08_csc_rfc3_msb            = (Temp >>  8) & 0x03;
  THS8200Cfg->reg09_csc_rfc3_lsb            = (Temp >>  0) & 0xFF;

  Temp = (THS8200_SIGN(CSC->G1) << 15) + (THS8200_ABS(CSC->G1 + 0x00000200) >> 10);

  THS8200Cfg->reg0A_csc_gic1                = (Temp >> 10) & 0x3F;
  THS8200Cfg->reg0A_csc_gfc1_msb            = (Temp >>  8) & 0x03;
  THS8200Cfg->reg0B_csc_gfc1_lsb            = (Temp >>  0) & 0xFF;

  Temp = (THS8200_SIGN(CSC->G2) << 15) + (THS8200_ABS(CSC->G2 + 0x00000200) >> 10);

  THS8200Cfg->reg0C_csc_gic2                = (Temp >> 10) & 0x3F;
  THS8200Cfg->reg0C_csc_gfc2_msb            = (Temp >>  8) & 0x03;
  THS8200Cfg->reg0D_csc_gfc2_lsb            = (Temp >>  0) & 0xFF;

  Temp = (THS8200_SIGN(CSC->G3) << 15) + (THS8200_ABS(CSC->G3 + 0x00000200) >> 10);

  THS8200Cfg->reg0E_csc_gic3                = (Temp >> 10) & 0x3F;
  THS8200Cfg->reg0E_csc_gfc3_msb            = (Temp >>  8) & 0x03;
  THS8200Cfg->reg0F_csc_gfc3_lsb            = (Temp >>  0) & 0xFF;

  Temp = (THS8200_SIGN(CSC->B1) << 15) + (THS8200_ABS(CSC->B1 + 0x00000200) >> 10);

  THS8200Cfg->reg10_csc_bic1                = (Temp >> 10) & 0x3F;
  THS8200Cfg->reg10_csc_bfc1_msb            = (Temp >>  8) & 0x03;
  THS8200Cfg->reg11_csc_bfc1_lsb            = (Temp >>  0) & 0xFF;

  Temp = (THS8200_SIGN(CSC->B2) << 15) + (THS8200_ABS(CSC->B2 + 0x00000200) >> 10);

  THS8200Cfg->reg12_csc_bic2                = (Temp >> 10) & 0x3F;
  THS8200Cfg->reg12_csc_bfc2_msb            = (Temp >>  8) & 0x03;
  THS8200Cfg->reg13_csc_bfc2_lsb            = (Temp >>  0) & 0xFF;

  Temp = (THS8200_SIGN(CSC->B3) << 15) + (THS8200_ABS(CSC->B3 + 0x00000200) >> 10);

  THS8200Cfg->reg14_csc_bic3                = (Temp >> 10) & 0x3F;
  THS8200Cfg->reg14_csc_bfc3_msb            = (Temp >>  8) & 0x03;
  THS8200Cfg->reg15_csc_bfc3_lsb            = (Temp >>  0) & 0xFF;

  Temp = (THS8200_SIGN(CSC->OFFS1) << 9) + (THS8200_ABS(CSC->OFFS1 + 0x00080000) >> 22);

  THS8200Cfg->reg16_csc_offset1_msb         = (Temp >>  2) & 0xFF;
  THS8200Cfg->reg17_csc_offset1_lsb         = (Temp >>  0) & 0x03;

  Temp = (THS8200_SIGN(CSC->OFFS2) << 9) + (THS8200_ABS(CSC->OFFS2 + 0x00080000) >> 22);

  THS8200Cfg->reg17_csc_offset2_msb         = (Temp >>  4) & 0x3F;
  THS8200Cfg->reg18_csc_offset2_lsb         = (Temp >>  0) & 0x0F;

  Temp = (THS8200_SIGN(CSC->OFFS3) << 9) + (THS8200_ABS(CSC->OFFS3 + 0x00080000) >> 22);

  THS8200Cfg->reg18_csc_offset3_msb         = (Temp >>  6) & 0x0F;
  THS8200Cfg->reg19_csc_offset3_lsb         = (Temp >>  0) & 0x3F;

//THS8200Cfg->reg19_csc_bypass              = 0x00; //CSC not bypassed
//THS8200Cfg->reg19_c_uof_cntl              = 0x01; //default
}

//Set blanking and syncs levels for SOG
void THS8200SetSOG(THS8200_CFG *THS8200Cfg, THS8200_SOG *SOG)
{
  THS8200Cfg->reg23_dtg1_y_blank_msb        = SOG->Y_BLANK     / 256;
  THS8200Cfg->reg1D_dtg1_y_blank_lsb        = SOG->Y_BLANK     % 256; //default

  THS8200Cfg->reg23_dtg1_y_sync_low_msb     = SOG->Y_SYNC_LOW  / 256; //default
  THS8200Cfg->reg1E_dtg1_y_sync_low_lsb     = SOG->Y_SYNC_LOW  % 256;

  THS8200Cfg->reg23_dtg1_y_sync_high_msb    = SOG->Y_SYNC_HIGH / 256;
  THS8200Cfg->reg1F_dtg1_y_sync_high_lsb    = SOG->Y_SYNC_HIGH % 256;

  THS8200Cfg->reg24_dtg1_cbcr_blank_msb     = SOG->C_BLANK     / 256;
  THS8200Cfg->reg20_dtg1_cbcr_blank_lsb     = SOG->C_BLANK     % 256; //default

  THS8200Cfg->reg24_dtg1_cbcr_sync_low_msb  = SOG->C_SYNC_LOW  / 256;
  THS8200Cfg->reg21_dtg1_cbcr_sync_low_lsb  = SOG->C_SYNC_LOW  % 256; //default

  THS8200Cfg->reg24_dtg1_cbcr_sync_high_msb = SOG->C_SYNC_HIGH / 256;
  THS8200Cfg->reg22_dtg1_cbcr_sync_high_lsb = SOG->C_SYNC_HIGH % 256; //default
}

//Disable SOG
void THS8200DisableSOG(THS8200_SOG *SOG)
{
  SOG->Y_SYNC_LOW  = SOG->Y_BLANK;
  SOG->Y_SYNC_HIGH = SOG->Y_BLANK;
}

//Initialize SOG settings
void InitializeSOG(THS8200_SOG *SOG_Settings, THS8200_CONFIGURATION *THS8200)
{
  if(THS8200->TimingMode == THS8200_DTG_TIMING_SDTV) //SDTV (bi-level syncs)
  {
    if((THS8200->VideoSyncRatio == 0) || (THS8200->Timing.Vactive == THS8200_VACTIVE_SDTV_576))
    {
      *SOG_Settings = SyncLevels4SDTV;   // 7:3 video:sync ratio w/o pedestal
    }
    else
    {
      *SOG_Settings = SyncLevels4SDTV_2; //10:4 video:sync ratio w/  pedestal
    }
  }
  else if(THS8200->TimingMode == THS8200_DTG_TIMING_HDTV) //HDTV (tri-level syncs)
  {
    *SOG_Settings = SyncLevels4HDTV;
  }
  else //if(THS8200->TimingMode == THS8200_DTG_TIMING_VESA)
  {
    //blanking level must be set to 0 when using DE input in the VESA preset mode
    if(THS8200->DTGMode == THS8200_DTG_PRESET_MODE) // && (THS8200->FID_DE_Cntl == 1))
    {
      *SOG_Settings = SyncLevels4VESA_P; //VESA preset mode
    }
    else //if(THS8200->DTGMode == THS8200_DTG_GENERIC_MODE)
    {
      *SOG_Settings = SyncLevels4VESA_G; //VESA generic mode
    }
  }
}

//Set CSM register settings
void THS8200SetCSM(THS8200_CFG *THS8200Cfg, THS8200_CSM *CSM)
{
  THS8200Cfg->reg41_csm_clip_gy_low         = CSM->CLIP_GY_LOW;   //default
  THS8200Cfg->reg42_csm_clip_bcb_low        = CSM->CLIP_BCB_LOW;  //default
  THS8200Cfg->reg43_csm_clip_rcr_low        = CSM->CLIP_RCR_LOW;  //default

  THS8200Cfg->reg44_csm_clip_gy_high        = CSM->CLIP_GY_HIGH;  //default
  THS8200Cfg->reg45_csm_clip_bcb_high       = CSM->CLIP_BCB_HIGH; //default
  THS8200Cfg->reg46_csm_clip_rcr_high       = CSM->CLIP_RCR_HIGH; //default

  THS8200Cfg->reg47_csm_shift_gy            = CSM->SHIFT_GY;      //default
  THS8200Cfg->reg48_csm_shift_bcb           = CSM->SHIFT_BCB;     //default
  THS8200Cfg->reg49_csm_shift_rcr           = CSM->SHIFT_RCR;     //default

  THS8200Cfg->reg4A_csm_mult_gy_msb         = CSM->MULT_GY  / 256;
  THS8200Cfg->reg4C_csm_mult_gy_lsb         = CSM->MULT_GY  % 256;

  THS8200Cfg->reg4B_csm_mult_bcb_msb        = CSM->MULT_BCB / 256;
  THS8200Cfg->reg4D_csm_mult_bcb_lsb        = CSM->MULT_BCB % 256;

  THS8200Cfg->reg4B_csm_mult_rcr_msb        = CSM->MULT_RCR / 256;
  THS8200Cfg->reg4E_csm_mult_rcr_lsb        = CSM->MULT_RCR % 256;

  THS8200Cfg->reg4A_csm_gy_low_clip_on      = CSM->CLIP_GY_LOW_ON;
  THS8200Cfg->reg4F_csm_bcb_low_clip_on     = CSM->CLIP_BCB_LOW_ON;
  THS8200Cfg->reg4F_csm_rcr_low_clip_on     = CSM->CLIP_RCR_LOW_ON;

  THS8200Cfg->reg4A_csm_gy_high_clip_on     = CSM->CLIP_GY_HIGH_ON;
  THS8200Cfg->reg4F_csm_bcb_high_clip_on    = CSM->CLIP_BCB_HIGH_ON;
  THS8200Cfg->reg4F_csm_rcr_high_clip_on    = CSM->CLIP_RCR_HIGH_ON;

  THS8200Cfg->reg4A_csm_shift_gy_on         = CSM->SHIFT_GY_ON;
  THS8200Cfg->reg4F_csm_shift_bcb_on        = CSM->SHIFT_BCB_ON;
  THS8200Cfg->reg4F_csm_shift_rcr_on        = CSM->SHIFT_RCR_ON;

  THS8200Cfg->reg4A_csm_mult_gy_on          = CSM->MULT_GY_ON;    //disabled in generic DAC mode only
  THS8200Cfg->reg4F_csm_mult_bcb_on         = CSM->MULT_BCB_ON;   //disabled in generic DAC mode only
  THS8200Cfg->reg4F_csm_mult_rcr_on         = CSM->MULT_RCR_ON;   //disabled in generic DAC mode only

//THS8200Cfg->reg4A_csm_of_cntl             = 0x01; //default
}

//Initialize CSM settings
void InitializeCSM(THS8200_CSM *CSM_Settings, THS8200_CONFIGURATION *THS8200)
{
//unsigned int CSMInputColor;
  unsigned int CSMInputRange;

//CSMInputColor = THS8200->CSC.OutputColor;

  if(THS8200->CSC.InputColor == THS8200->CSC.OutputColor)        //CSC bypassed
  {
    CSMInputRange = THS8200->CSC.InputRange;
  }
  else //if(THS8200->CSC.InputColor != THS8200->CSC.OutputColor) //CSC not bypassed
  {
    CSMInputRange = THS8200->CSC.OutputRange; //normally set to limited range
  }

  if(CSMInputRange == CSM_CTL_INPUT_RANGE_LMTD)
  {
    if(THS8200->CSC.OutputColor == CSC_CTL_OUTPUT_COLOR_RGB) //CSM input = limited-range RGB
    {
      *CSM_Settings = CSMSet4RGBLmtd;
    }
    else if(THS8200->CSC.OutputColor == CSC_CTL_OUTPUT_COLOR_HDTV) // CSM input = limited-range HDTV
    {
      *CSM_Settings = CSMSet4YCbCrLmtd;
    }
    else //if(THS8200->CSC.OutputColor == THS8200_OUTPUT_COLOR_SDTV) // CSM input = limited-range SDTV
    {
      if((THS8200->VideoSyncRatio == 0) || (THS8200->Timing.Vactive == THS8200_VACTIVE_SDTV_576))
      {
        *CSM_Settings = CSMSet4YCbCrLmtd;   // 7:3 video:sync ratio w/o pedestal
      }
      else
      {
        *CSM_Settings = CSMSet4YCbCrLmtd_2; //10:4 video:sync ratio w/  pedestal
      }
    }
  }
  else //if(CSMInputRange == CSM_CTL_INPUT_RANGE_FULL)
  {
    if(THS8200->GenericDAC == 1) //generic DAC mode enabled
    {
      *CSM_Settings = CSMSet4GenericDAC; //CSM clip, shift and multiply disabled
    }
    else //if(THS8200->GenericDAC == 0)
    {
      *CSM_Settings = CSMSet4RGBFull;    //CSM clip and shift disabled, CSM multiply enabled
//    *CSM_Settings = CSMSet4YCbCrFull;
    }
  }
}

void THS8200SetConfig(THS8200_CONFIGURATION *THS8200, THS8200_REGS reg)
{
  THS8200_CFG THS8200Cfg;
  CSC_COEFFICIENTS CSC_Coef;
  CSC_RANGE CSC_Range;
  THS8200_CSM CSM_Settings;
  THS8200_SOG SOG_Settings;
  unsigned int Temp;
  int hsout_delay, hs_in_delay;

  //Set bit fields to their default values
  THS8200ConfigSetDefaults(&THS8200Cfg);

  //Program bit fields that have common settings for each format

  //**********************************************************************
  //System Control (Sub-Address 0x03)
  //**********************************************************************

//  THS8200Cfg.reg03_dll_bypass              = 0; //default (must always be 0)
//  THS8200Cfg.reg03_vesa_color_bars         = 0; //default
//  THS8200Cfg.reg03_chip_ms                 = 0; //default
//  THS8200Cfg.reg03_arst_func_n             = 1; //default

  if((THS8200->InputRatio == THS8200_INPUT_RATIO_444) && (THS8200->UpSampling == 0))
  {
    THS8200Cfg.reg03_vesa_clk                = 1;
  }
//else
//{
//  THS8200Cfg.reg03_vesa_clk                = 0; //default
//}

  if(THS8200->Timing.PClk < THS8200_MAX_PCLK_DLL_LOW)
  {
    THS8200Cfg.reg03_dll_freq_sel            = 1;
  }
//else
//{
//  THS8200Cfg.reg03_dll_freq_sel            = 0; //default
//}

  THS8200Cfg.reg03_dac_pwdn                  = (THS8200->PowerMgmt >> 1) & 0x01;
  THS8200Cfg.reg03_chip_pwdn                 = (THS8200->PowerMgmt >> 0) & 0x01;

  //**********************************************************************
  //Color Space Conversion Control (Sub-Addresses 0x04 - 0x19)
  //**********************************************************************

//THS8200Cfg.reg19_csc_bypass = 1; //CSC bypassed (default)

  //Initialize CSC coefficients and input/output ranges
  InitializeCSC(&CSC_Coef, &CSC_Range, &THS8200->CSC);

  //Modify CSC coefficients for input/output ranges being used
//if(THS8200->InputColor != THS8200->OutputColor)
//{
    ModifyCSC4IORange(&CSC_Coef, &CSC_Range);
//}

  //Modify CSC coefficients for color controls (i.e. brightness, contrast, saturation and hue control)
//if(THS8200->CSC.OutputColor <= CSC_CTL_OUTPUT_COLOR_HDTV)
//{
    ModifyCSC4ColorCtl(&CSC_Coef, &CSC_Range, &THS8200->CSC);
//}

  //Swap CSC coefficients for B/Cb and R/Cr inputs
//if(THS8200->CSC.CbCrSwap == 1) //CbCr swap enabled
//{
    CSC_CbCrSwap(&CSC_Coef, &THS8200->CSC);
//}

  //Set THS8200 CSC registers
  THS8200SetCSC(&THS8200Cfg, &CSC_Coef);

  if((THS8200->CSC.InputColor != THS8200->CSC.OutputColor) ||
     (THS8200->CSC.CbCrSwap == 1))
  {
    THS8200Cfg.reg19_csc_bypass = 0; //CSC not bypassed
  }

  //**********************************************************************
  //Test Control (Sub-Addresses 0x1A - 0x1B)
  //**********************************************************************

//THS8200Cfg.reg1A_tst_digbpass             = 0x00; //default
//THS8200Cfg.reg1A_tst_offset               = 0x00; //default
//THS8200Cfg.reg1B_tst_ydelay               = 0x00; //default
//THS8200Cfg.reg1B_tst_fastramp             = 0x00; //default
//THS8200Cfg.reg1B_tst_slowramp             = 0x00; //default

  //**********************************************************************
  //Data Path Control (Sub-Address 0x1C)
  //**********************************************************************

  THS8200Cfg.reg1C_data_fsadj                = 0x01; //FSADJ1 terminal selected

  if(THS8200->InputRatio == THS8200_INPUT_RATIO_444)
  {
    THS8200Cfg.reg1C_data_ifir12_bypass      = 1; //4:2:2 to 4:4:4 conversion bypassed
  }
//else
//{
//  THS8200Cfg.reg1C_data_ifir12_bypass      = 0; //4:2:2 to 4:4:4 conversion not bypassed (default)
//}

  if(THS8200->UpSampling == 0)
  {
    THS8200Cfg.reg1C_data_ifir35_bypass      = 1; //2x up-sampling bypassed
  }
//else
//{
//  THS8200Cfg.reg1C_data_ifir35_bypass      = 0; //2x up-sampling not bypassed (default)
//}

  if(THS8200->InputWidth == THS8200_INPUT_WIDTH_10) //10-bit YCbCr 4:2:2
  {
    THS8200Cfg.reg1C_data_clk656_on          = THS8200->BT656Output;
    THS8200Cfg.reg1C_data_tristate656        = !THS8200->BT656Output;
  }
  else
  {
//  THS8200Cfg.reg1C_data_clk656_on          = 0; //BT.656 clock disabled (default)
    THS8200Cfg.reg1C_data_tristate656        = 1; //BT.656 data bus disabled
  }

  THS8200Cfg.reg1C_data_dman_cntl            = THS8200->InputWidth;

  //**********************************************************************
  //Display Timing Generation Control, Part 1 (Sub-Addresses 0x1D - 0x3C)
  //**********************************************************************

  //*****************************************************************
  //SOG blanking and sync levels control (Sub-Addresses 0x1D - 0x24)
  //*****************************************************************

  //Initialize SOG settings
  InitializeSOG(&SOG_Settings, THS8200);

  if(THS8200->OutputSOG == 0) //SOG disabled
  {
    THS8200DisableSOG(&SOG_Settings);
  }

  //Set SOG register settings
  THS8200SetSOG(&THS8200Cfg, &SOG_Settings);

  //*********************************************
  //DTG mode control (Sub-Addresses 0x36 - 0x38)
  //*********************************************

//THS8200Cfg.reg36_dtg1_linecnt_msb         = 0; //default

  if(THS8200->CGMSEnable == 1) //CGMS enabled
  {
    THS8200Cfg.reg38_dtg1_pass_through       = 1; //for FULL_NSP and FULL_NTSP line types only
  }
//else //if(THS8200->CGMSEnable == 0) //CGMS disabled
//{
//  THS8200Cfg.reg38_dtg1_pass_through       = 0; //default
//}

  if(THS8200->InputSyncs == THS8200_INPUT_SYNCS_DS)
  {
    if(THS8200->FID_DE_Cntl == 0) //Discrete FID signal used
    {
      THS8200Cfg.reg36_dtg1_field_flip       = !(THS8200->FIDPolarity);
    }
    else //if(THS8200->FID_DE_Cntl == 1) //DE operation in VESA preset mode or internal FID decode
    {
      if((THS8200->TimingMode == THS8200_DTG_TIMING_VESA) && //VESA preset mode
         (THS8200->DTGMode == THS8200_DTG_PRESET_MODE))
      {
        THS8200Cfg.reg36_dtg1_field_flip     = 1; //FID polarity must be set to 0
      }
//    else //if((THS8200->TimingMode != THS8200_DTG_TIMING_VESA) ||
//         //   (THS8200->DTGMode != THS8200_DTG_PRESET_MODE))
//    {
//      THS8200Cfg.reg36_dtg1_field_flip     = 0; //default
//    }
    }
  }
//else //if(THS8200->InputSyncs == THS8200_INPUT_SYNCS_ES)
//{
//  THS8200Cfg.reg36_dtg1_field_flip         = 0; //default
//}

  if(THS8200->GenericDAC == 1)
  {
    THS8200Cfg.reg38_dtg1_on                 = 0;
  }
//else //if(THS8200->GenericDAC == 0)
//{
//  THS8200Cfg.reg38_dtg1_on                 = 1; //default
//}

  if(THS8200->DTGMode == THS8200_DTG_PRESET_MODE)
  {
    if(THS8200->TimingMode == THS8200_DTG_TIMING_SDTV)
    {
      if(THS8200->Timing.Vactive == THS8200_VACTIVE_SDTV_576)
      {
//      THS8200Cfg.reg37_dtg1_linecnt_lsb    = 0x01; //default

        if(THS8200->Timing.IP == 1) //interlaced
        {
          THS8200Cfg.reg38_dtg1_mode         = THS8200_DTG_SDTV_576I; //SDTV 576i preset mode
        }
      }
      else //if(THS8200->Timing.Vactive == THS8200_VACTIVE_SDTV_480)
      {
        THS8200Cfg.reg37_dtg1_linecnt_lsb    = 0x04;

        if(THS8200->Timing.IP == 1) //interlaced
        {
          THS8200Cfg.reg38_dtg1_mode         = THS8200_DTG_SDTV_480I; //SDTV 480i preset mode
        }
        else //if(THS8200->Timing.IP == 0) //progressive
        {
          THS8200Cfg.reg38_dtg1_mode         = THS8200_DTG_SDTV_480P; //SDTV 480i preset mode
        }
      }
    }
    else if(THS8200->TimingMode == THS8200_DTG_TIMING_HDTV)
    {
//    THS8200Cfg.reg37_dtg1_linecnt_lsb      = 0x01; //default

      if(THS8200->Timing.Vactive == THS8200_VACTIVE_HDTV_1080)
      {
        if(THS8200->Timing.IP == 0)
        {
          THS8200Cfg.reg38_dtg1_mode         = THS8200_DTG_HDTV_1080P; //HDTV 1080p preset mode
        }
        else //if(THS8200->Timing.IP == 1)
        {
          THS8200Cfg.reg38_dtg1_mode         = THS8200_DTG_HDTV_1080I; //HDTV 1080i preset mode
        }
      }
      else //if(THS8200->Timing.Vactive == THS8200_VACTIVE_HDTV_720)
      {
        THS8200Cfg.reg38_dtg1_mode           = THS8200_DTG_HDTV_720P; //HDTV 720p preset mode
      }
    }
    else //if(THS8200->TimingMode == THS8200_DTG_TIMING_VESA)
    {
      if(THS8200->MasterSlave == 0) //slave mode
      {
        THS8200Cfg.reg38_dtg1_mode           = THS8200_DTG_VESA_SLAVE; //VESA preset mode (SOG not supported, no offset on Y)
      }
      else //if(THS8200->MasterSlave == 1) //master mode
      {
        THS8200Cfg.reg38_dtg1_mode           = THS8200_DTG_VESA_MASTER; //VESA preset mode (SOG not supported, no offset on Y)
      }
    }
  }
  else //if(THS8200->DTGMode == THS8200_DTG_GENERIC_MODE)
  {
//  THS8200Cfg.reg37_dtg1_linecnt_lsb        = 0x01; //default

    if(THS8200->TimingMode == THS8200_DTG_TIMING_HDTV)
    {
      THS8200Cfg.reg38_dtg1_mode             = THS8200_DTG_HDTV_GENERIC; //HDTV generic mode (SOG supported, 350mV offset on Y)
    }
    else //if(THS8200->TimingMode != THS8200_DTG_TIMING_HDTV)
    {
      THS8200Cfg.reg38_dtg1_mode             = THS8200_DTG_SDTV_GENERIC; //SDTV generic mode (SOG supported, 350mV offset on Y)
                                                                         //VESA generic mode (SOG supported, 350mV offset on Y)
    }
  }

  //**********************************************************************
  // DAC Control (Sub-Addresses 0x3D - 0x40)
  //**********************************************************************

//THS8200Cfg.reg3D_dac_i2c_cntl  = 0x00; //default
//THS8200Cfg.reg3D_dac1_cntl_msb = 0x00; //default
//THS8200Cfg.reg3D_dac2_cntl_msb = 0x00; //default
//THS8200Cfg.reg3D_dac3_cntl_msb = 0x00; //default
//THS8200Cfg.reg3E_dac1_cntl_lsb = 0x00; //default
//THS8200Cfg.reg3F_dac2_cntl_lsb = 0x00; //default
//THS8200Cfg.reg40_dac3_cntl_lsb = 0x00; //default

  //**********************************************************************
  //Clip/Shift/Multiplier Control (Sub-Addresses 0x41 - 0x4F)
  //**********************************************************************

  //Initialize CSM settings
  InitializeCSM(&CSM_Settings, THS8200);

  //Set CSM register settings
  THS8200SetCSM(&THS8200Cfg, &CSM_Settings);

  //**********************************************************************
  //Display Timing Generation Control, Part 2 (Sub-Addresses 0x50 - 0x82)
  //**********************************************************************

  //*****************************************
  //Discrete input/output syncs timing setup
  //*****************************************

  //Digital Process Delays
  //CSC = 8 pixel delay
  //CSM = 1 pixel delay
  //2x up-sampling (up to 80MHz) = 18 pixel delay
  //4:2:2 to 4:4:4 interpolation = 40 pixel delay
  //discrete input syncs = 4 pixel delay (compared to embedded syncs)
  //DLL bypass for VESA mode = 4 pixel delay (compared to not bypassed)

  hs_in_delay = 0; //initialize stack variable

  if(THS8200Cfg.reg03_vesa_clk == 1) //VESA clock mode, DLL bypassed
  {
    hs_in_delay += 4; //~3.5?
  }

  if(THS8200Cfg.reg19_csc_bypass == 0) //CSC not bypassed
  {
    hs_in_delay += 8;
  }

  if(THS8200Cfg.reg4A_csm_mult_gy_on == 1) //CSM multiply enabled
  {
    hs_in_delay += 1;
  }

  if(THS8200->InputRatio == THS8200_INPUT_RATIO_422) //4:2:2 to 4:4:4 conversion enabled
  {
    hs_in_delay += 40;
  }

  if(THS8200->UpSampling == 1) //2x up-sampling enabled (up to 80MHz, not used with VGA outputs)
  {
    hs_in_delay += 18;
  }

  if(THS8200->InputSyncs == THS8200_INPUT_SYNCS_ES)
  {
    hs_in_delay += THS8200->Timing.HFP;
  }
  else //if(THS8200->InputSyncs == THS8200_INPUT_SYNCS_DS)
  {
    hs_in_delay += 4;
  }

  hs_in_delay -= THS8200_HS_IN_ADVANCE_8;

  if(THS8200->OutputSyncs == 1)
  {
    hs_in_delay -= 1;
  }

  if((hs_in_delay >= THS8200_HS_OUT_DELAY_8) && (THS8200->OutputSyncs == 1))
  {
    hsout_delay = THS8200_HS_OUT_DELAY_8; //for CSC enabled
  }
  else
  {
    hsout_delay = THS8200_HS_OUT_DELAY_0; //for CSC disabled
  }

  hs_in_delay -= (int)hsout_delay;

  if(hs_in_delay < 0)
  {
    hs_in_delay = 0;
  }

  hsout_delay += 1;

//THS8200Cfg.reg71_dtg2_hdly_msb = hsout_delay / 256; //always 0 (default)
  THS8200Cfg.reg72_dtg2_hdly_lsb = hsout_delay % 256;

  hsout_delay += 1; //used in calculation of spec_b and spec_k

  THS8200Cfg.reg79_dtg2_hs_in_dly_msb = hs_in_delay / 256;
  THS8200Cfg.reg7A_dtg2_hs_in_dly_lsb = hs_in_delay % 256;

  if(THS8200->InputSyncs == THS8200_INPUT_SYNCS_ES)
  {
    Temp = THS8200->Timing.VFP; //vertical front porch

    THS8200Cfg.reg7B_dtg2_vs_in_dly_msb = Temp / 256;
    THS8200Cfg.reg7C_dtg2_vs_in_dly_lsb = Temp % 256;
  }
  else //if(THS8200->InputSyncs == THS8200_INPUT_SYNCS_DS)
  {
//  THS8200Cfg.reg7B_dtg2_vs_in_dly_msb = 0x00; //default
    THS8200Cfg.reg7C_dtg2_vs_in_dly_lsb = 0x00;
  }

//THS8200Cfg.reg82_dtg2_fid_de_cntl          = 0; //FID input (default)
//THS8200Cfg.reg82_dtg2_rgb_mode_on          = 1; //RGB mode on (default)
//THS8200Cfg.reg82_dtg2_embedded_timing      = 0; //embedded syncs disabled (default)

  THS8200Cfg.reg82_dtg2_fid_pol              = 0;

  if(THS8200->InputSyncs == THS8200_INPUT_SYNCS_DS)
  {
    THS8200Cfg.reg82_dtg2_fid_de_cntl        = THS8200->FID_DE_Cntl;

    if(THS8200->FID_DE_Cntl == 0) //Discrete FID signal used
    {
      THS8200Cfg.reg82_dtg2_fid_pol          = THS8200->FIDPolarity;
    }
  }
  else //if(THS8200->InputSyncs == THS8200_INPUT_SYNCS_ES)
  {
    THS8200Cfg.reg82_dtg2_embedded_timing    = 1; //embedded syncs enabled
  }

  if(THS8200->TimingMode <= THS8200_DTG_TIMING_HDTV) //SDTV/HDTV timing modes
  {
    THS8200Cfg.reg82_dtg2_rgb_mode_on        = 0; //RGB mode not on
  }
  else if(THS8200->DTGMode == THS8200_DTG_GENERIC_MODE) //VESA generic mode
  {
    THS8200Cfg.reg82_dtg2_rgb_mode_on        = 0; //RGB mode not on (Is this really the best setting here?)
  }
//else if(THS8200->DTGMode == THS8200_DTG_PRESET_MODE) //VESA preset mode
//{
//  THS8200Cfg.reg82_dtg2_rgb_mode_on        = 1; //RGB mode on (default), actually a don't care for VESA preset mode
//}

  //**********************************************************************
  //CGMS Control (Sub-Addresses 0x83-0x85)
  //**********************************************************************

//THS8200Cfg.reg83_cgms_en                  = 0x00; //default
//THS8200Cfg.reg83_cgms_header              = 0x00; //default
//THS8200Cfg.reg84_cgms_payload_msb         = 0x00; //default
//THS8200Cfg.reg85_cgms_payload_lsb         = 0x00; //default

  //Program bit fields that have unique settings for each format

  //****************************************
  //Control of horizontal timing parameters
  //****************************************

  //SDTV: uses spec_a, spec_b and spec_d
  //HDTV: uses spec_a, spec_b, spec_c, spec_d and spec_e

  Temp = THS8200->Timing.HSW; //horizontal sync width

  if(Temp > 255)
  {
    Temp = 255;
  }

  if(THS8200->TimingMode == THS8200_DTG_TIMING_SDTV)
  {
    //values tweaked slighty to make them closer to existing cnd files
    THS8200Cfg.reg25_dtg1_spec_a = Temp - 1;        //Negative Hsync width (SDTV)
    THS8200Cfg.reg27_dtg1_spec_c = (Temp + 1) >> 1; //Equalization pulse width (SDTV)
  }
  else if(THS8200->TimingMode == THS8200_DTG_TIMING_HDTV)
  {
    THS8200Cfg.reg25_dtg1_spec_a = Temp; //Negative Hsync width (HDTV)
    THS8200Cfg.reg27_dtg1_spec_c = Temp; //Positive Hsync width (HDTV)
  }
  else if(THS8200->DTGMode == THS8200_DTG_GENERIC_MODE) //VESA generic mode
  {
    THS8200Cfg.reg25_dtg1_spec_a = Temp; //Negative Hsync width (SDTV)
    THS8200Cfg.reg27_dtg1_spec_c = 0x00; //Equalization pulse width (SDTV)
  }
//else
//{
//  THS8200Cfg.reg25_dtg1_spec_a = 0x2C; //default
//  THS8200Cfg.reg27_dtg1_spec_c = 0x2C; //default
//}

  if((THS8200->TimingMode != THS8200_DTG_TIMING_VESA) || //not VESA preset mode
     (THS8200->DTGMode != THS8200_DTG_PRESET_MODE))
  {
    Temp = THS8200->Timing.HFP - hsout_delay;

    if(Temp > 1023)
    {
      Temp = 1023;
    }

    THS8200Cfg.reg30_dtg1_spec_k_msb = Temp / 256; //End of active video to sync (SDTV)/end of broad pulse to sync (HDTV)
    THS8200Cfg.reg2F_dtg1_spec_k_lsb = Temp % 256;

    if(Temp > 255)
    {
      Temp = 255;
    }

    THS8200Cfg.reg26_dtg1_spec_b = Temp; //End of active video to 0H (SDTV, HDTV)

    Temp = THS8200->Timing.HSW + THS8200->Timing.HBP + hsout_delay;

    if(Temp > 511)
    {
      Temp = 511;
    }

    THS8200Cfg.reg2B_dtg1_spec_d_msb = Temp / 256; //Sync to active video (SDTV)/sync to broad pulse (HDTV)
    THS8200Cfg.reg28_dtg1_spec_d_lsb = Temp % 256;
  }

//THS8200Cfg.reg2B_dtg1_spec_e_msb = 0x00; //default

  if(THS8200->TimingMode == THS8200_DTG_TIMING_HDTV)
  {
    THS8200Cfg.reg2B_dtg1_spec_e_msb = Temp / 256; //Sync to active video (HDTV)
    THS8200Cfg.reg2A_dtg1_spec_e_lsb = Temp % 256;
  }
  else if((THS8200->TimingMode == THS8200_DTG_TIMING_VESA) && //VESA preset mode
          (THS8200->DTGMode == THS8200_DTG_PRESET_MODE))
  {
    if(THS8200->TestPattern & 0x01) //Test pattern enabled
    {
      THS8200Cfg.reg2B_dtg1_spec_e_msb = 0x00; //Color bar start (VESA), WIP
      THS8200Cfg.reg2A_dtg1_spec_e_lsb = 0x00; //WIP
    }
  }

  Temp = THS8200->Timing.Hactive; //active pixels per line

  if(THS8200->TestPattern & 0x01)
  {
    THS8200Cfg.reg3C_dtg1_vesa_cbar_size = Temp >> 3; //width of each color bar in test pattern
  }

  Temp += THS8200->Timing.HFP + THS8200->Timing.HSW + THS8200->Timing.HBP; //total pixels per line

  if(Temp > 8191)
  {
    Temp = 8191;
  }

  THS8200Cfg.reg34_dtg1_total_pixels_msb = Temp / 256;
  THS8200Cfg.reg35_dtg1_total_pixels_lsb = Temp % 256;

  //dtg1_spec_g, dtg1_spec_h, dtg1_spec_i, dtg1_spec_k1 setup
  if(THS8200->TimingMode == THS8200_DTG_TIMING_SDTV)
  {
    THS8200Cfg.reg33_dtg1_spec_g_msb = (Temp >> 1) / 256; //half of line length (SDTV)
    THS8200Cfg.reg32_dtg1_spec_g_lsb = (Temp >> 1) % 256;

    Temp -= THS8200->Timing.HSW;

    if(Temp > 4095)
    {
      Temp = 4095;
    }

    THS8200Cfg.reg2D_dtg1_spec_i_msb = Temp / 256; //Full-line broad pulse duration (SDTV)
    THS8200Cfg.reg2E_dtg1_spec_i_lsb = Temp % 256;

    if(Temp > 2047)
    {
      Temp = 2047;
    }

    THS8200Cfg.reg2B_dtg1_spec_h_msb = (Temp >> 1) / 256; //Broad pulse duration (SDTV)
    THS8200Cfg.reg2C_dtg1_spec_h_lsb = (Temp >> 1) % 256;

    if(THS8200->Timing.IP == 1) //interlaced
    {
//    THS8200Cfg.reg29_dtg1_spec_d1 = 0x00; //default, why set to zero?
      THS8200Cfg.reg31_dtg1_spec_k1  = 0x0A; //480i, 576i
    }
//  else //if(THS8200->Timing.IP == 0) //progressive
//  {
//    THS8200Cfg.reg29_dtg1_spec_d1 = 0x00; //default
//    THS8200Cfg.reg31_dtg1_spec_k1  = 0x00; //default
//  }
  }
  else if((THS8200->TimingMode == THS8200_DTG_TIMING_VESA) && //VESA generic mode (optional?)
          (THS8200->DTGMode == THS8200_DTG_GENERIC_MODE))
  {
//  THS8200Cfg.reg33_dtg1_spec_g_msb = 0; //default
    THS8200Cfg.reg32_dtg1_spec_g_lsb = 0;

//  THS8200Cfg.reg2B_dtg1_spec_h_msb = 0; //default
//  THS8200Cfg.reg2C_dtg1_spec_h_lsb = 0; //default
  }

  //**************************************
  //Control of vertical timing parameters
  //**************************************

  //field and frame size setup
//if(THS8200->DTGMode == THS8200_DTG_GENERIC_MODE)
//{
    if(THS8200->Timing.IP == 0) //progressive
    {
      Temp = THS8200->Timing.Vactive + THS8200->Timing.VFP + THS8200->Timing.VSW + THS8200->Timing.VBP; //total lines per frame

      THS8200Cfg.reg39_dtg1_frame_size_msb = Temp / 256;
      THS8200Cfg.reg3A_dtg1_frame_size_lsb = Temp % 256;

      Temp++;

      THS8200Cfg.reg39_dtg1_field_size_msb = 0x07; //Temp / 256; //field_size should be greater than frame_size for progressive mode
      THS8200Cfg.reg3B_dtg1_field_size_lsb = 0xFF; //Temp % 256;
    }
    else //if(THS8200->Timing.IP == 1) //interlaced
    {
      Temp = ((THS8200->Timing.Vactive >> 1) + THS8200->Timing.VFP + THS8200->Timing.VSW + THS8200->Timing.VBP)*2 + 1; //total lines per frame

      THS8200Cfg.reg39_dtg1_frame_size_msb = Temp / 256;
      THS8200Cfg.reg3A_dtg1_frame_size_lsb = Temp % 256;

      Temp++;

      THS8200Cfg.reg39_dtg1_field_size_msb = (Temp >> 1) / 256;
      THS8200Cfg.reg3B_dtg1_field_size_lsb = (Temp >> 1) % 256;
    }
//}

  //breakpoint and line type setup for generic mode
  if((THS8200->DTGMode == THS8200_DTG_GENERIC_MODE) && (THS8200->Timing.IP == 0))
  {
    if(THS8200->TimingMode == THS8200_DTG_TIMING_SDTV) //480p, 576p
    {
      Temp = THS8200->Timing.VFP + 1;

//    THS8200Cfg.reg36_dtg1_linecnt_msb = Temp / 256; //always 0 (default)
      THS8200Cfg.reg37_dtg1_linecnt_lsb = Temp % 256;

//    THS8200Cfg.reg50_dtg2_bp1_msb = Temp / 256; //always 0 (default)
      THS8200Cfg.reg58_dtg2_bp1_lsb = Temp % 256;

      Temp += THS8200->Timing.VSW;

//    THS8200Cfg.reg50_dtg2_bp2_msb = Temp / 256; //always 0 (default)
      THS8200Cfg.reg59_dtg2_bp2_lsb = Temp % 256;

      Temp += THS8200->Timing.VBP;

//    THS8200Cfg.reg51_dtg2_bp3_msb = Temp / 256; //always 0 (default)
      THS8200Cfg.reg5A_dtg2_bp3_lsb = Temp % 256;

      Temp += THS8200->Timing.Vactive; //total lines per frame + 1

      THS8200Cfg.reg51_dtg2_bp4_msb = Temp / 256;
      THS8200Cfg.reg5B_dtg2_bp4_lsb = Temp % 256;

      THS8200Cfg.reg68_dtg2_linetype1 = THS8200_DTG_FULL_NSP; //Full Normal Sync Pulse
      THS8200Cfg.reg68_dtg2_linetype2 = THS8200_DTG_FULL_BSP; //Full Broad Sync Pulse
      THS8200Cfg.reg69_dtg2_linetype3 = THS8200_DTG_FULL_NSP; //Full Normal Sync Pulse (same as preset)
//    THS8200Cfg.reg69_dtg2_linetype4 = THS8200_DTG_ACTIVE_VIDEO; //Active Video, default
    }
    else if(THS8200->TimingMode == THS8200_DTG_TIMING_HDTV) //720p, 1080p
    {
//    THS8200Cfg.reg36_dtg1_linecnt_msb = 0; //default
//    THS8200Cfg.reg37_dtg1_linecnt_lsb = 1; //default

      Temp = THS8200->Timing.VSW + 1;

//    THS8200Cfg.reg50_dtg2_bp1_msb = Temp / 256; //always 0 (default)
      THS8200Cfg.reg58_dtg2_bp1_lsb = Temp % 256;

      Temp += THS8200->Timing.VBP;

//    THS8200Cfg.reg50_dtg2_bp2_msb = Temp / 256; //always 0 (default)
      THS8200Cfg.reg59_dtg2_bp2_lsb = Temp % 256;

      Temp += THS8200->Timing.Vactive;

      THS8200Cfg.reg51_dtg2_bp3_msb = Temp / 256;
      THS8200Cfg.reg5A_dtg2_bp3_lsb = Temp % 256;

      Temp += THS8200->Timing.VFP; //total lines per frame + 1

      THS8200Cfg.reg51_dtg2_bp4_msb = Temp / 256;
      THS8200Cfg.reg5B_dtg2_bp4_lsb = Temp % 256;

      THS8200Cfg.reg68_dtg2_linetype1 = THS8200_DTG_FULL_BTSP; //Full Broad Pulse and Tri-Level Sync Pulse
      THS8200Cfg.reg68_dtg2_linetype2 = THS8200_DTG_FULL_NTSP; //Full Normal Tri-Level Sync Pulse
//    THS8200Cfg.reg69_dtg2_linetype3 = THS8200_DTG_ACTIVE_VIDEO; //Active Video, default
      THS8200Cfg.reg69_dtg2_linetype4 = THS8200_DTG_FULL_NTSP; //Full Normal Tri-Level Sync Pulse
    }
    else if(THS8200->TimingMode == THS8200_DTG_TIMING_VESA) //VESA generic mode
    {
//    Temp = THS8200->Timing.Vactive + THS8200->Timing.VFP + THS8200->Timing.VSW + THS8200->Timing.VBP; //total lines per frame

//    THS8200Cfg.reg50_dtg2_bp1_msb = 0; //default
//    THS8200Cfg.reg58_dtg2_bp1_lsb = 0; //default

      THS8200Cfg.reg50_dtg2_bp2_msb = Temp / 256;
      THS8200Cfg.reg59_dtg2_bp2_lsb = Temp % 256;

//    THS8200Cfg.reg68_dtg2_linetype1 = THS8200_DTG_ACTIVE_VIDEO; //Active Video, default
//    THS8200Cfg.reg68_dtg2_linetype2 = THS8200_DTG_ACTIVE_VIDEO; //Active Video, default
    }
  }
  else if((THS8200->DTGMode == THS8200_DTG_GENERIC_MODE) && (THS8200->Timing.IP == 1))
  {
    if(THS8200->TimingMode == THS8200_DTG_TIMING_SDTV) //480i, 576i
    {
      if(THS8200->Timing.Vactive == THS8200_VACTIVE_SDTV_576)
      {
        Temp = THS8200->Timing.VFP + 1;

//      THS8200Cfg.reg36_dtg1_linecnt_msb = Temp / 256; //always 0 (default)
        THS8200Cfg.reg37_dtg1_linecnt_lsb = Temp % 256;

//      THS8200Cfg.reg50_dtg2_bp1_msb = Temp / 256; //always 0 (default)
        THS8200Cfg.reg58_dtg2_bp1_lsb = Temp % 256;

//      THS8200Cfg.reg50_dtg2_bp2_msb = (Temp + 1) / 256; //always 0 (default)
        THS8200Cfg.reg59_dtg2_bp2_lsb = (Temp + 1) % 256;

        Temp += THS8200->Timing.VSW;

//      THS8200Cfg.reg51_dtg2_bp3_msb = Temp / 256; //always 0 (default)
        THS8200Cfg.reg5A_dtg2_bp3_lsb = Temp % 256;

        Temp += (THS8200->Timing.VBP - THS8200->Timing.VFP);

//      THS8200Cfg.reg51_dtg2_bp4_msb = Temp / 256; //always 0 (default)
        THS8200Cfg.reg5B_dtg2_bp4_lsb = Temp % 256;

//      THS8200Cfg.reg52_dtg2_bp5_msb = (Temp + 1) / 256;
        THS8200Cfg.reg5C_dtg2_bp5_lsb = (Temp + 1) % 256;

        Temp += (THS8200->Timing.Vactive >> 1);

        THS8200Cfg.reg52_dtg2_bp6_msb = Temp / 256;
        THS8200Cfg.reg5D_dtg2_bp6_lsb = Temp % 256;

        Temp += THS8200->Timing.VFP;

        THS8200Cfg.reg53_dtg2_bp7_msb = Temp / 256;
        THS8200Cfg.reg5E_dtg2_bp7_lsb = Temp % 256;

        THS8200Cfg.reg53_dtg2_bp8_msb = (Temp + 1) / 256;
        THS8200Cfg.reg5F_dtg2_bp8_lsb = (Temp + 1) % 256;

        Temp += THS8200->Timing.VSW;

        THS8200Cfg.reg54_dtg2_bp9_msb = Temp / 256;
        THS8200Cfg.reg60_dtg2_bp9_lsb = Temp % 256;

        Temp += THS8200->Timing.VFP;

        THS8200Cfg.reg54_dtg2_bp10_msb = Temp / 256;
        THS8200Cfg.reg61_dtg2_bp10_lsb = Temp % 256;

        THS8200Cfg.reg55_dtg2_bp11_msb = (Temp + 1) / 256;
        THS8200Cfg.reg62_dtg2_bp11_lsb = (Temp + 1) % 256;

        Temp += (THS8200->Timing.VBP - THS8200->Timing.VFP);

        THS8200Cfg.reg55_dtg2_bp12_msb = (Temp + 1) / 256;
        THS8200Cfg.reg63_dtg2_bp12_lsb = (Temp + 1) % 256;

        Temp += (THS8200->Timing.Vactive >> 1);

        THS8200Cfg.reg56_dtg2_bp13_msb = Temp / 256;
        THS8200Cfg.reg64_dtg2_bp13_lsb = Temp % 256;

        THS8200Cfg.reg56_dtg2_bp14_msb = (Temp + 1) / 256;
        THS8200Cfg.reg65_dtg2_bp14_lsb = (Temp + 1) % 256;

        Temp += THS8200->Timing.VFP + 1; //total lines per frame + 1

        THS8200Cfg.reg57_dtg2_bp15_msb = Temp / 256;
        THS8200Cfg.reg66_dtg2_bp15_lsb = Temp % 256;

        THS8200Cfg.reg68_dtg2_linetype1  = THS8200_DTG_BSP_BSP;
        THS8200Cfg.reg68_dtg2_linetype2  = THS8200_DTG_BSP_NEQ;
        THS8200Cfg.reg69_dtg2_linetype3  = THS8200_DTG_NEQ_NEQ;
        THS8200Cfg.reg69_dtg2_linetype4  = THS8200_DTG_FULL_NSP;
        THS8200Cfg.reg6A_dtg2_linetype5  = THS8200_DTG_NSP_ACTIVE;
        THS8200Cfg.reg6A_dtg2_linetype6  = THS8200_DTG_ACTIVE_VIDEO; //default
        THS8200Cfg.reg6B_dtg2_linetype7  = THS8200_DTG_NEQ_NEQ;
        THS8200Cfg.reg6B_dtg2_linetype8  = THS8200_DTG_NEQ_BSP;
        THS8200Cfg.reg6C_dtg2_linetype9  = THS8200_DTG_BSP_BSP;
        THS8200Cfg.reg6C_dtg2_linetype10 = THS8200_DTG_NEQ_NEQ;
        THS8200Cfg.reg6D_dtg2_linetype11 = THS8200_DTG_FULL_NEQ;
        THS8200Cfg.reg6D_dtg2_linetype12 = THS8200_DTG_FULL_NSP;
        THS8200Cfg.reg6E_dtg2_linetype13 = THS8200_DTG_ACTIVE_VIDEO; //default
        THS8200Cfg.reg6E_dtg2_linetype14 = THS8200_DTG_ACTIVE_NEQ;
        THS8200Cfg.reg6F_dtg2_linetype15 = THS8200_DTG_NEQ_NEQ;
      }
      else //if(THS8200->Timing.Vactive == THS8200_VACTIVE_SDTV_480)
      {
        Temp = THS8200->Timing.VFP + 1;

//      THS8200Cfg.reg36_dtg1_linecnt_msb = Temp / 256; //always 0 (default)
        THS8200Cfg.reg37_dtg1_linecnt_lsb = Temp % 256;

//      THS8200Cfg.reg50_dtg2_bp1_msb = Temp / 256; //always 0 (default)
        THS8200Cfg.reg58_dtg2_bp1_lsb = Temp % 256;

        Temp += THS8200->Timing.VSW;

//      THS8200Cfg.reg50_dtg2_bp2_msb = Temp / 256; //always 0 (default)
        THS8200Cfg.reg59_dtg2_bp2_lsb = Temp % 256;

        Temp += THS8200->Timing.VFP;

//      THS8200Cfg.reg51_dtg2_bp3_msb = Temp / 256; //always 0 (default)
        THS8200Cfg.reg5A_dtg2_bp3_lsb = Temp % 256;

        Temp += (THS8200->Timing.VBP - THS8200->Timing.VFP);

//      THS8200Cfg.reg51_dtg2_bp4_msb = Temp / 256; //always 0 (default)
        THS8200Cfg.reg5B_dtg2_bp4_lsb = Temp % 256;

        Temp += (THS8200->Timing.Vactive >> 1);

        THS8200Cfg.reg52_dtg2_bp5_msb = Temp / 256;
        THS8200Cfg.reg5C_dtg2_bp5_lsb = Temp % 256;

        THS8200Cfg.reg52_dtg2_bp6_msb = (Temp + 1) / 256;
        THS8200Cfg.reg5D_dtg2_bp6_lsb = (Temp + 1) % 256;

        Temp += THS8200->Timing.VFP;

        THS8200Cfg.reg53_dtg2_bp7_msb = Temp / 256;
        THS8200Cfg.reg5E_dtg2_bp7_lsb = Temp % 256;

        THS8200Cfg.reg53_dtg2_bp8_msb = (Temp + 1) / 256;
        THS8200Cfg.reg5F_dtg2_bp8_lsb = (Temp + 1) % 256;

        Temp += THS8200->Timing.VSW;

        THS8200Cfg.reg54_dtg2_bp9_msb = Temp / 256;
        THS8200Cfg.reg60_dtg2_bp9_lsb = Temp % 256;

        THS8200Cfg.reg54_dtg2_bp10_msb = (Temp + 1) / 256;
        THS8200Cfg.reg61_dtg2_bp10_lsb = (Temp + 1) % 256;

        Temp += THS8200->Timing.VFP;

        THS8200Cfg.reg55_dtg2_bp11_msb = Temp / 256;
        THS8200Cfg.reg62_dtg2_bp11_lsb = Temp % 256;

        THS8200Cfg.reg55_dtg2_bp12_msb = (Temp + 1) / 256;
        THS8200Cfg.reg63_dtg2_bp12_lsb = (Temp + 1) % 256;

        Temp += (THS8200->Timing.VBP - THS8200->Timing.VFP);

        THS8200Cfg.reg56_dtg2_bp13_msb = Temp / 256;
        THS8200Cfg.reg64_dtg2_bp13_lsb = Temp % 256;

        THS8200Cfg.reg56_dtg2_bp14_msb = (Temp + 1) / 256;
        THS8200Cfg.reg65_dtg2_bp14_lsb = (Temp + 1) % 256;

        Temp += (THS8200->Timing.Vactive >> 1) + 1; //total lines per frame + 1

        THS8200Cfg.reg57_dtg2_bp15_msb = Temp / 256;
        THS8200Cfg.reg66_dtg2_bp15_lsb = Temp % 256;

        THS8200Cfg.reg68_dtg2_linetype1  = THS8200_DTG_NEQ_NEQ;
        THS8200Cfg.reg68_dtg2_linetype2  = THS8200_DTG_BSP_BSP;
        THS8200Cfg.reg69_dtg2_linetype3  = THS8200_DTG_NEQ_NEQ;
        THS8200Cfg.reg69_dtg2_linetype4  = THS8200_DTG_FULL_NSP;
        THS8200Cfg.reg6A_dtg2_linetype5  = THS8200_DTG_ACTIVE_VIDEO; //default
        THS8200Cfg.reg6A_dtg2_linetype6  = THS8200_DTG_ACTIVE_NEQ;
        THS8200Cfg.reg6B_dtg2_linetype7  = THS8200_DTG_NEQ_NEQ;
        THS8200Cfg.reg6B_dtg2_linetype8  = THS8200_DTG_NEQ_BSP;
        THS8200Cfg.reg6C_dtg2_linetype9  = THS8200_DTG_BSP_BSP;
        THS8200Cfg.reg6C_dtg2_linetype10 = THS8200_DTG_BSP_NEQ;
        THS8200Cfg.reg6D_dtg2_linetype11 = THS8200_DTG_NEQ_NEQ;
        THS8200Cfg.reg6D_dtg2_linetype12 = THS8200_DTG_FULL_NEQ;
        THS8200Cfg.reg6E_dtg2_linetype13 = THS8200_DTG_FULL_NSP;
        THS8200Cfg.reg6E_dtg2_linetype14 = THS8200_DTG_NSP_ACTIVE;
        THS8200Cfg.reg6F_dtg2_linetype15 = THS8200_DTG_ACTIVE_VIDEO; //default
      }
    }
    else if(THS8200->TimingMode == THS8200_DTG_TIMING_HDTV) //1080i
    {
      Temp = THS8200->Timing.VSW + 1;

//    THS8200Cfg.reg50_dtg2_bp1_msb = Temp / 256; //always 0 (default)
      THS8200Cfg.reg58_dtg2_bp1_lsb = Temp % 256;

//    THS8200Cfg.reg50_dtg2_bp2_msb = (Temp + 1) / 256; //always 0 (default)
      THS8200Cfg.reg59_dtg2_bp2_lsb = (Temp + 1) % 256;

      Temp += THS8200->Timing.VBP;

//    THS8200Cfg.reg51_dtg2_bp3_msb = Temp / 256; //always 0 (default)
      THS8200Cfg.reg5A_dtg2_bp3_lsb = Temp % 256;

      Temp += (THS8200->Timing.Vactive >> 1);

      THS8200Cfg.reg51_dtg2_bp4_msb = Temp / 256;
      THS8200Cfg.reg5B_dtg2_bp4_lsb = Temp % 256;

      Temp += THS8200->Timing.VFP;

      THS8200Cfg.reg52_dtg2_bp5_msb = Temp / 256;
      THS8200Cfg.reg5C_dtg2_bp5_lsb = Temp % 256;

      THS8200Cfg.reg52_dtg2_bp6_msb = (Temp + 1) / 256;
      THS8200Cfg.reg5D_dtg2_bp6_lsb = (Temp + 1) % 256;

      Temp += THS8200->Timing.VSW;

      THS8200Cfg.reg53_dtg2_bp7_msb = Temp / 256;
      THS8200Cfg.reg5E_dtg2_bp7_lsb = Temp % 256;

      THS8200Cfg.reg53_dtg2_bp8_msb = (Temp + 1) / 256;
      THS8200Cfg.reg5F_dtg2_bp8_lsb = (Temp + 1) % 256;

      Temp += THS8200->Timing.VBP + 1;

      THS8200Cfg.reg54_dtg2_bp9_msb = Temp / 256;
      THS8200Cfg.reg60_dtg2_bp9_lsb = Temp % 256;

      Temp += (THS8200->Timing.Vactive >> 1);

      THS8200Cfg.reg54_dtg2_bp10_msb = Temp / 256;
      THS8200Cfg.reg61_dtg2_bp10_lsb = Temp % 256;

      Temp += THS8200->Timing.VFP; //total lines per frame + 1

      THS8200Cfg.reg55_dtg2_bp11_msb = Temp / 256;
      THS8200Cfg.reg62_dtg2_bp11_lsb = Temp % 256;

      THS8200Cfg.reg68_dtg2_linetype1  = THS8200_DTG_BTSP_BTSP;
      THS8200Cfg.reg68_dtg2_linetype2  = THS8200_DTG_NTSP_NTSP;
      THS8200Cfg.reg69_dtg2_linetype3  = THS8200_DTG_FULL_NTSP;
      THS8200Cfg.reg69_dtg2_linetype4  = THS8200_DTG_ACTIVE_VIDEO; //default
      THS8200Cfg.reg6A_dtg2_linetype5  = THS8200_DTG_FULL_NTSP;
      THS8200Cfg.reg6A_dtg2_linetype6  = THS8200_DTG_NTSP_BTSP;
      THS8200Cfg.reg6B_dtg2_linetype7  = THS8200_DTG_BTSP_BTSP;
      THS8200Cfg.reg6B_dtg2_linetype8  = THS8200_DTG_BTSP_NTSP;
      THS8200Cfg.reg6C_dtg2_linetype9  = THS8200_DTG_FULL_NTSP;
      THS8200Cfg.reg6C_dtg2_linetype10 = THS8200_DTG_ACTIVE_VIDEO; //default
      THS8200Cfg.reg6D_dtg2_linetype11 = THS8200_DTG_FULL_NTSP;
    }
  }

  if(THS8200->OutputSyncs == 1) //Discrete output syncs enabled
  {
    Temp = THS8200->Timing.HSW; //horizontal sync width

    THS8200Cfg.reg71_dtg2_hlength_msb = Temp / 256;
    THS8200Cfg.reg70_dtg2_hlength_lsb = Temp % 256;

    Temp = THS8200->Timing.VSW + 1; //vertical sync width + 1

    THS8200Cfg.reg74_dtg2_vlength1_msb = Temp / 256;
    THS8200Cfg.reg73_dtg2_vlength1_lsb = Temp % 256;

    if(THS8200->Timing.IP == 1) //interlaced
    {
      THS8200Cfg.reg77_dtg2_vlength2_msb = Temp / 256;
      THS8200Cfg.reg76_dtg2_vlength2_lsb = Temp % 256;
    }
//  else //if(THS8200->Timing.IP == 0) //progressive
//  {
//    THS8200Cfg.reg77_dtg2_vlength2_msb = 0x00; //default
//    THS8200Cfg.reg76_dtg2_vlength2_lsb = 0x00; //default
//  }

//  THS8200Cfg.reg74_dtg2_vdly1_msb          = 0x00; //default
    THS8200Cfg.reg75_dtg2_vdly1_lsb          = 0x01;
//  THS8200Cfg.reg77_dtg2_vdly2_msb          = 0x07; //default
//  THS8200Cfg.reg78_dtg2_vdly2_lsb          = 0xFF; //default
  }
  else //if(THS8200->OutputSyncs == 0) //Discrete output syncs disabled
  {
    //set output sync durations to zero
//  THS8200Cfg.reg71_dtg2_hlength_msb        = 0x00; //default
    THS8200Cfg.reg70_dtg2_hlength_lsb        = 0x00;
//  THS8200Cfg.reg74_dtg2_vlength1_msb       = 0x00; //default
    THS8200Cfg.reg73_dtg2_vlength1_lsb       = 0x00;
//  THS8200Cfg.reg77_dtg2_vlength2_msb       = 0x00; //default
//  THS8200Cfg.reg76_dtg2_vlength2_lsb       = 0x00; //default

    //set output sync delays to maximum
//  THS8200Cfg.reg71_dtg2_hdly_msb           = 0x1F; //optional?
//  THS8200Cfg.reg72_dtg2_hdly_lsb           = 0xFF; //optional?
    THS8200Cfg.reg74_dtg2_vdly1_msb          = 0x07;
    THS8200Cfg.reg75_dtg2_vdly1_lsb          = 0xFF;
//  THS8200Cfg.reg77_dtg2_vdly2_msb          = 0x07; //default
//  THS8200Cfg.reg78_dtg2_vdly2_lsb          = 0xFF; //default
  }

  Temp = THS8200->Timing.VPol^THS8200->InputSyncPol; //vertical sync polority

  THS8200Cfg.reg82_dtg2_vsout_pol = Temp; //VSYNC output polarity
  THS8200Cfg.reg82_dtg2_vs_pol = Temp;    //VSYNC input polarity

  Temp = THS8200->Timing.HPol^THS8200->InputSyncPol; //horizontal sync polority

  THS8200Cfg.reg82_dtg2_hsout_pol = Temp; //HSYNC output polarity
  THS8200Cfg.reg82_dtg2_hs_pol = Temp;    //HSYNC input polarity

  //Copy to an array for quick programming

  {
    int i;

    for(i=0; i<THS8200_MAX_REGS; i++)
    {
        reg[i] = (unsigned int)-1;
    }
  }

  //System
  reg[0x03] = (THS8200Cfg.reg03_vesa_clk                << 7) | (THS8200Cfg.reg03_dll_bypass              << 6) |
              (THS8200Cfg.reg03_vesa_color_bars         << 5) | (THS8200Cfg.reg03_dll_freq_sel            << 4) |
              (THS8200Cfg.reg03_dac_pwdn                << 3) | (THS8200Cfg.reg03_chip_pwdn               << 2) |
              (THS8200Cfg.reg03_chip_ms                 << 1) | (THS8200Cfg.reg03_arst_func_n             << 0);

  //Color Space Conversion
  reg[0x04] = (THS8200Cfg.reg04_csc_ric1                << 2) | (THS8200Cfg.reg04_csc_rfc1_msb            << 0);
  reg[0x05] = (THS8200Cfg.reg05_csc_rfc1_lsb            << 0);
  reg[0x06] = (THS8200Cfg.reg06_csc_ric2                << 2) | (THS8200Cfg.reg06_csc_rfc2_msb            << 0);
  reg[0x07] = (THS8200Cfg.reg07_csc_rfc2_lsb            << 0);
  reg[0x08] = (THS8200Cfg.reg08_csc_ric3                << 2) | (THS8200Cfg.reg08_csc_rfc3_msb            << 0);
  reg[0x09] = (THS8200Cfg.reg09_csc_rfc3_lsb            << 0);
  reg[0x0A] = (THS8200Cfg.reg0A_csc_gic1                << 2) | (THS8200Cfg.reg0A_csc_gfc1_msb            << 0);
  reg[0x0B] = (THS8200Cfg.reg0B_csc_gfc1_lsb            << 0);
  reg[0x0C] = (THS8200Cfg.reg0C_csc_gic2                << 2) | (THS8200Cfg.reg0C_csc_gfc2_msb            << 0);
  reg[0x0D] = (THS8200Cfg.reg0D_csc_gfc2_lsb            << 0);
  reg[0x0E] = (THS8200Cfg.reg0E_csc_gic3                << 2) | (THS8200Cfg.reg0E_csc_gfc3_msb            << 0);
  reg[0x0F] = (THS8200Cfg.reg0F_csc_gfc3_lsb            << 0);
  reg[0x10] = (THS8200Cfg.reg10_csc_bic1                << 2) | (THS8200Cfg.reg10_csc_bfc1_msb            << 0);
  reg[0x11] = (THS8200Cfg.reg11_csc_bfc1_lsb            << 0);
  reg[0x12] = (THS8200Cfg.reg12_csc_bic2                << 2) | (THS8200Cfg.reg12_csc_bfc2_msb            << 0);
  reg[0x13] = (THS8200Cfg.reg13_csc_bfc2_lsb            << 0);
  reg[0x14] = (THS8200Cfg.reg14_csc_bic3                << 2) | (THS8200Cfg.reg14_csc_bfc3_msb            << 0);
  reg[0x15] = (THS8200Cfg.reg15_csc_bfc3_lsb            << 0);
  reg[0x16] = (THS8200Cfg.reg16_csc_offset1_msb         << 0);
  reg[0x17] = (THS8200Cfg.reg17_csc_offset1_lsb         << 6) | (THS8200Cfg.reg17_csc_offset2_msb         << 0);
  reg[0x18] = (THS8200Cfg.reg18_csc_offset2_lsb         << 4) | (THS8200Cfg.reg18_csc_offset3_msb         << 0);
  reg[0x19] = (THS8200Cfg.reg19_csc_offset3_lsb         << 2) | (THS8200Cfg.reg19_csc_bypass              << 1) |
              (THS8200Cfg.reg19_c_uof_cntl              << 0);

  //Test
  reg[0x1A] = (THS8200Cfg.reg1A_tst_digbpass            << 7) | (THS8200Cfg.reg1A_tst_offset              << 6);
  reg[0x1B] = (THS8200Cfg.reg1B_tst_ydelay              << 6) | (THS8200Cfg.reg1B_tst_fastramp            << 1) |
              (THS8200Cfg.reg1B_tst_slowramp            << 0);

  //Data Path
  reg[0x1C] = (THS8200Cfg.reg1C_data_clk656_on          << 7) | (THS8200Cfg.reg1C_data_fsadj              << 6) |
              (THS8200Cfg.reg1C_data_ifir12_bypass      << 5) | (THS8200Cfg.reg1C_data_ifir35_bypass      << 4) |
              (THS8200Cfg.reg1C_data_tristate656        << 3) | (THS8200Cfg.reg1C_data_dman_cntl          << 0);

  //Display Timing Generation, Part 1
  reg[0x1D] = (THS8200Cfg.reg1D_dtg1_y_blank_lsb        << 0);
  reg[0x1E] = (THS8200Cfg.reg1E_dtg1_y_sync_low_lsb     << 0);
  reg[0x1F] = (THS8200Cfg.reg1F_dtg1_y_sync_high_lsb    << 0);
  reg[0x20] = (THS8200Cfg.reg20_dtg1_cbcr_blank_lsb     << 0);
  reg[0x21] = (THS8200Cfg.reg21_dtg1_cbcr_sync_low_lsb  << 0);
  reg[0x22] = (THS8200Cfg.reg22_dtg1_cbcr_sync_high_lsb << 0);
  reg[0x23] = (THS8200Cfg.reg23_dtg1_y_blank_msb        << 4) | (THS8200Cfg.reg23_dtg1_y_sync_low_msb     << 2) |
              (THS8200Cfg.reg23_dtg1_y_sync_high_msb    << 0);
  reg[0x24] = (THS8200Cfg.reg24_dtg1_cbcr_blank_msb     << 4) | (THS8200Cfg.reg24_dtg1_cbcr_sync_low_msb  << 2) |
              (THS8200Cfg.reg24_dtg1_cbcr_sync_high_msb << 0);
  reg[0x25] = (THS8200Cfg.reg25_dtg1_spec_a             << 0);
  reg[0x26] = (THS8200Cfg.reg26_dtg1_spec_b             << 0);
  reg[0x27] = (THS8200Cfg.reg27_dtg1_spec_c             << 0);
  reg[0x28] = (THS8200Cfg.reg28_dtg1_spec_d_lsb         << 0);
  reg[0x29] = (THS8200Cfg.reg29_dtg1_spec_d1            << 0);
  reg[0x2A] = (THS8200Cfg.reg2A_dtg1_spec_e_lsb         << 0);
  reg[0x2B] = (THS8200Cfg.reg2B_dtg1_spec_d_msb         << 7) | (THS8200Cfg.reg2B_dtg1_spec_e_msb         << 6) |
              (THS8200Cfg.reg2B_dtg1_spec_h_msb         << 0);
  reg[0x2C] = (THS8200Cfg.reg2C_dtg1_spec_h_lsb         << 0);
  reg[0x2D] = (THS8200Cfg.reg2D_dtg1_spec_i_msb         << 0);
  reg[0x2E] = (THS8200Cfg.reg2E_dtg1_spec_i_lsb         << 0);
  reg[0x2F] = (THS8200Cfg.reg2F_dtg1_spec_k_lsb         << 0);
  reg[0x30] = (THS8200Cfg.reg30_dtg1_spec_k_msb         << 0);
  reg[0x31] = (THS8200Cfg.reg31_dtg1_spec_k1            << 0);
  reg[0x32] = (THS8200Cfg.reg32_dtg1_spec_g_lsb         << 0);
  reg[0x33] = (THS8200Cfg.reg33_dtg1_spec_g_msb         << 0);
  reg[0x34] = (THS8200Cfg.reg34_dtg1_total_pixels_msb   << 0);
  reg[0x35] = (THS8200Cfg.reg35_dtg1_total_pixels_lsb   << 0);
  reg[0x36] = (THS8200Cfg.reg36_dtg1_field_flip         << 7) | (THS8200Cfg.reg36_dtg1_linecnt_msb        << 0);
  reg[0x37] = (THS8200Cfg.reg37_dtg1_linecnt_lsb        << 0);
  reg[0x38] = (THS8200Cfg.reg38_dtg1_on                 << 7) | (THS8200Cfg.reg38_dtg1_pass_through       << 4) |
              (THS8200Cfg.reg38_dtg1_mode               << 0);
  reg[0x39] = (THS8200Cfg.reg39_dtg1_frame_size_msb     << 4) | (THS8200Cfg.reg39_dtg1_field_size_msb     << 0);
  reg[0x3A] = (THS8200Cfg.reg3A_dtg1_frame_size_lsb     << 0);
  reg[0x3B] = (THS8200Cfg.reg3B_dtg1_field_size_lsb     << 0);
  reg[0x3C] = (THS8200Cfg.reg3C_dtg1_vesa_cbar_size     << 0);

  //DAC
  reg[0x3D] = (THS8200Cfg.reg3D_dac_i2c_cntl            << 6) | (THS8200Cfg.reg3D_dac1_cntl_msb           << 4) |
              (THS8200Cfg.reg3D_dac2_cntl_msb           << 2) | (THS8200Cfg.reg3D_dac3_cntl_msb           << 0);
  reg[0x3E] = (THS8200Cfg.reg3E_dac1_cntl_lsb           << 0);
  reg[0x3F] = (THS8200Cfg.reg3F_dac2_cntl_lsb           << 0);
  reg[0x40] = (THS8200Cfg.reg40_dac3_cntl_lsb           << 0);

  //Clip/Shift/Multiplier
  reg[0x41] = (THS8200Cfg.reg41_csm_clip_gy_low         << 0);
  reg[0x42] = (THS8200Cfg.reg42_csm_clip_bcb_low        << 0);
  reg[0x43] = (THS8200Cfg.reg43_csm_clip_rcr_low        << 0);
  reg[0x44] = (THS8200Cfg.reg44_csm_clip_gy_high        << 0);
  reg[0x45] = (THS8200Cfg.reg45_csm_clip_bcb_high       << 0);
  reg[0x46] = (THS8200Cfg.reg46_csm_clip_rcr_high       << 0);
  reg[0x47] = (THS8200Cfg.reg47_csm_shift_gy            << 0);
  reg[0x48] = (THS8200Cfg.reg48_csm_shift_bcb           << 0);
  reg[0x49] = (THS8200Cfg.reg49_csm_shift_rcr           << 0);
  reg[0x4A] = (THS8200Cfg.reg4A_csm_mult_gy_on          << 7) | (THS8200Cfg.reg4A_csm_shift_gy_on         << 6) |
              (THS8200Cfg.reg4A_csm_gy_high_clip_on     << 5) | (THS8200Cfg.reg4A_csm_gy_low_clip_on      << 4) |
              (THS8200Cfg.reg4A_csm_of_cntl             << 3) | (THS8200Cfg.reg4A_csm_mult_gy_msb         << 0);
  reg[0x4B] = (THS8200Cfg.reg4B_csm_mult_bcb_msb        << 4) | (THS8200Cfg.reg4B_csm_mult_rcr_msb        << 0);
  reg[0x4C] = (THS8200Cfg.reg4C_csm_mult_gy_lsb         << 0);
  reg[0x4D] = (THS8200Cfg.reg4D_csm_mult_bcb_lsb        << 0);
  reg[0x4E] = (THS8200Cfg.reg4E_csm_mult_rcr_lsb        << 0);
  reg[0x4F] = (THS8200Cfg.reg4F_csm_mult_rcr_on         << 7) | (THS8200Cfg.reg4F_csm_mult_bcb_on         << 6) |
              (THS8200Cfg.reg4F_csm_shift_rcr_on        << 5) | (THS8200Cfg.reg4F_csm_shift_bcb_on        << 4) |
              (THS8200Cfg.reg4F_csm_rcr_high_clip_on    << 3) | (THS8200Cfg.reg4F_csm_rcr_low_clip_on     << 2) |
              (THS8200Cfg.reg4F_csm_bcb_high_clip_on    << 1) | (THS8200Cfg.reg4F_csm_bcb_low_clip_on     << 0);

  //Display Timing Generation, Part 2
  reg[0x50] = (THS8200Cfg.reg50_dtg2_bp1_msb            << 4) | (THS8200Cfg.reg50_dtg2_bp2_msb            << 0);
  reg[0x51] = (THS8200Cfg.reg51_dtg2_bp3_msb            << 4) | (THS8200Cfg.reg51_dtg2_bp4_msb            << 0);
  reg[0x52] = (THS8200Cfg.reg52_dtg2_bp5_msb            << 4) | (THS8200Cfg.reg52_dtg2_bp6_msb            << 0);
  reg[0x53] = (THS8200Cfg.reg53_dtg2_bp7_msb            << 4) | (THS8200Cfg.reg53_dtg2_bp8_msb            << 0);
  reg[0x54] = (THS8200Cfg.reg54_dtg2_bp9_msb            << 4) | (THS8200Cfg.reg54_dtg2_bp10_msb           << 0);
  reg[0x55] = (THS8200Cfg.reg55_dtg2_bp11_msb           << 4) | (THS8200Cfg.reg55_dtg2_bp12_msb           << 0);
  reg[0x56] = (THS8200Cfg.reg56_dtg2_bp13_msb           << 4) | (THS8200Cfg.reg56_dtg2_bp14_msb           << 0);
  reg[0x57] = (THS8200Cfg.reg57_dtg2_bp15_msb           << 4) | (THS8200Cfg.reg57_dtg2_bp16_msb           << 0);
  reg[0x58] = (THS8200Cfg.reg58_dtg2_bp1_lsb            << 0);
  reg[0x59] = (THS8200Cfg.reg59_dtg2_bp2_lsb            << 0);
  reg[0x5A] = (THS8200Cfg.reg5A_dtg2_bp3_lsb            << 0);
  reg[0x5B] = (THS8200Cfg.reg5B_dtg2_bp4_lsb            << 0);
  reg[0x5C] = (THS8200Cfg.reg5C_dtg2_bp5_lsb            << 0);
  reg[0x5D] = (THS8200Cfg.reg5D_dtg2_bp6_lsb            << 0);
  reg[0x5E] = (THS8200Cfg.reg5E_dtg2_bp7_lsb            << 0);
  reg[0x5F] = (THS8200Cfg.reg5F_dtg2_bp8_lsb            << 0);
  reg[0x60] = (THS8200Cfg.reg60_dtg2_bp9_lsb            << 0);
  reg[0x61] = (THS8200Cfg.reg61_dtg2_bp10_lsb           << 0);
  reg[0x62] = (THS8200Cfg.reg62_dtg2_bp11_lsb           << 0);
  reg[0x63] = (THS8200Cfg.reg63_dtg2_bp12_lsb           << 0);
  reg[0x64] = (THS8200Cfg.reg64_dtg2_bp13_lsb           << 0);
  reg[0x65] = (THS8200Cfg.reg65_dtg2_bp14_lsb           << 0);
  reg[0x66] = (THS8200Cfg.reg66_dtg2_bp15_lsb           << 0);
  reg[0x67] = (THS8200Cfg.reg67_dtg2_bp16_lsb           << 0);
  reg[0x68] = (THS8200Cfg.reg68_dtg2_linetype1          << 4) | (THS8200Cfg.reg68_dtg2_linetype2          << 0);
  reg[0x69] = (THS8200Cfg.reg69_dtg2_linetype3          << 4) | (THS8200Cfg.reg69_dtg2_linetype4          << 0);
  reg[0x6A] = (THS8200Cfg.reg6A_dtg2_linetype5          << 4) | (THS8200Cfg.reg6A_dtg2_linetype6          << 0);
  reg[0x6B] = (THS8200Cfg.reg6B_dtg2_linetype7          << 4) | (THS8200Cfg.reg6B_dtg2_linetype8          << 0);
  reg[0x6C] = (THS8200Cfg.reg6C_dtg2_linetype9          << 4) | (THS8200Cfg.reg6C_dtg2_linetype10         << 0);
  reg[0x6D] = (THS8200Cfg.reg6D_dtg2_linetype11         << 4) | (THS8200Cfg.reg6D_dtg2_linetype12         << 0);
  reg[0x6E] = (THS8200Cfg.reg6E_dtg2_linetype13         << 4) | (THS8200Cfg.reg6E_dtg2_linetype14         << 0);
  reg[0x6F] = (THS8200Cfg.reg6F_dtg2_linetype15         << 4) | (THS8200Cfg.reg6F_dtg2_linetype16         << 0);
  reg[0x70] = (THS8200Cfg.reg70_dtg2_hlength_lsb        << 0);
  reg[0x71] = (THS8200Cfg.reg71_dtg2_hlength_msb        << 6) | (THS8200Cfg.reg71_dtg2_hdly_msb           << 0);
  reg[0x72] = (THS8200Cfg.reg72_dtg2_hdly_lsb           << 0);
  reg[0x73] = (THS8200Cfg.reg73_dtg2_vlength1_lsb       << 0);
  reg[0x74] = (THS8200Cfg.reg74_dtg2_vlength1_msb       << 6) | (THS8200Cfg.reg74_dtg2_vdly1_msb          << 0);
  reg[0x75] = (THS8200Cfg.reg75_dtg2_vdly1_lsb          << 0);
  reg[0x76] = (THS8200Cfg.reg76_dtg2_vlength2_lsb       << 0);
  reg[0x77] = (THS8200Cfg.reg77_dtg2_vlength2_msb       << 6) | (THS8200Cfg.reg77_dtg2_vdly2_msb          << 0);
  reg[0x78] = (THS8200Cfg.reg78_dtg2_vdly2_lsb          << 0);
  reg[0x79] = (THS8200Cfg.reg79_dtg2_hs_in_dly_msb      << 0);
  reg[0x7A] = (THS8200Cfg.reg7A_dtg2_hs_in_dly_lsb      << 0);
  reg[0x7B] = (THS8200Cfg.reg7B_dtg2_vs_in_dly_msb      << 0);
  reg[0x7C] = (THS8200Cfg.reg7C_dtg2_vs_in_dly_lsb      << 0);
  reg[0x7D] = -1; //status register
  reg[0x7E] = -1; //status register
  reg[0x7F] = -1; //status register
  reg[0x80] = -1; //status register
  reg[0x81] = -1; //reserved
  reg[0x82] = (THS8200Cfg.reg82_dtg2_fid_de_cntl        << 7) | (THS8200Cfg.reg82_dtg2_rgb_mode_on        << 6) |
              (THS8200Cfg.reg82_dtg2_embedded_timing    << 5) | (THS8200Cfg.reg82_dtg2_vsout_pol          << 4) |
              (THS8200Cfg.reg82_dtg2_hsout_pol          << 3) | (THS8200Cfg.reg82_dtg2_fid_pol            << 2) |
              (THS8200Cfg.reg82_dtg2_vs_pol             << 1) | (THS8200Cfg.reg82_dtg2_hs_pol             << 0);

  //CGMS Control
  reg[0x83] = (THS8200Cfg.reg83_cgms_en                 << 6) | (THS8200Cfg.reg83_cgms_header             << 0);
  reg[0x84] = (THS8200Cfg.reg84_cgms_payload_msb        << 0);
  reg[0x85] = (THS8200Cfg.reg85_cgms_payload_lsb        << 0);
}
