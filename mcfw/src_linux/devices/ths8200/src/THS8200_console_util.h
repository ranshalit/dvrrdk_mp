#ifndef THS8200_INCLUDED_
#define THS8200_INCLUDED_

#define THS8200_MAX_REGISTERS        0x86 //Total number of THS8200 registers (0x00 to 0x85)
#define CSC_MAX_COEFFICIENTS         12

static unsigned int THS8200RegDefaults[THS8200_MAX_REGISTERS] =
{
  //THS8200 Regiser Defaults - used as comments in WinVCC command files

  0x00, //reg 0x00, reserved1, reserved
  0x00, //reg 0x01, reserved2, reserved

  //System
  0x00, //reg 0x02, version, status_register
  0x01, //reg 0x03, chip_ctl

  //Color Space Conversion
  0x00, //reg 0x04, csc_r11
  0x00, //reg 0x05, csc_r12
  0x00, //reg 0x06, csc_r21
  0x00, //reg 0x07, csc_r22
  0x00, //reg 0x08, csc_r31
  0x00, //reg 0x09, csc_r32
  0x00, //reg 0x0A, csc_g11
  0x00, //reg 0x0B, csc_g12
  0x00, //reg 0x0C, csc_g21
  0x00, //reg 0x0D, csc_g22
  0x00, //reg 0x0E, csc_g31
  0x00, //reg 0x0F, csc_g32
  0x00, //reg 0x10, csc_b11
  0x00, //reg 0x11, csc_b12
  0x00, //reg 0x12, csc_b21
  0x00, //reg 0x13, csc_b22
  0x00, //reg 0x14, csc_b31
  0x00, //reg 0x15, csc_b32
  0x00, //reg 0x16, csc_offs1
  0x00, //reg 0x17, csc_offs12
  0x00, //reg 0x18, csc_offs23
  0x03, //reg 0x19, csc_offs3

  //Test
  0x00, //reg 0x1A, tst_cntl1
  0x00, //reg 0x1B, tst_cntl2

  //Data Path
  0x03, //reg 0x1C, data_cntl

  //Display Timing Generation, Part 1
  0x00, //reg 0x1D, dtg1_y_sync1_lsb
  0x00, //reg 0x1E, dtg1_y_sync2_lsb
  0x00, //reg 0x1F, dtg1_y_sync3_lsb
  0x00, //reg 0x20, dtg1_cbcr_sync1_lsb
  0x00, //reg 0x21, dtg1_cbcr_sync2_lsb
  0x00, //reg 0x22, dtg1_cbcr_sync3_lsb
  0x23, //reg 0x23, dtg1_y_sync_msb
  0x23, //reg 0x24, dtg1_cbcr_sync_msb
  0x2C, //reg 0x25, dtg1_spec_a
  0x58, //reg 0x26, dtg1_spec_b
  0x2C, //reg 0x27, dtg1_spec_c
  0x84, //reg 0x28, dtg1_spec_d_lsb
  0x00, //reg 0x29, dtg1_spec_d1
  0xC0, //reg 0x2A, dtg1_spec_e_lsb
  0x00, //reg 0x2B, dtg1_spec_deh_msb
  0x00, //reg 0x2C, dtg1_spec_h_lsb
  0x00, //reg 0x2D, dtg1_spec_i_msb
  0x00, //reg 0x2E, dtg1_spec_i_lsb
  0x58, //reg 0x2F, dtg1_spec_k_lsb
  0x00, //reg 0x30, dtg1_spec_k_msb
  0x00, //reg 0x31, dtg1_spec_k1
  0x58, //reg 0x32, dtg1_speg_g_lsb
  0x00, //reg 0x33, dtg1_speg_g_msb
  0x05, //reg 0x34, dtg1_total_pixels_msb
  0x20, //reg 0x35, dtg1_total_pixels_lsb
  0x00, //reg 0x36, dtg1_fieldflip_linecnt_msb
  0x01, //reg 0x37, dtg1_linecnt_lsb
  0x86, //reg 0x38, dtg1_mode
  0x30, //reg 0x39, dtg1_frame_field_size_msb
  0x00, //reg 0x3A, dtg1_frame_size_lsb
  0x20, //reg 0x3B, dtg1_field_size_lsb
  0x80, //reg 0x3C, dtg1_vesa_cbar_size

  //DAC
  0x00, //reg 0x3D, dac_cntl_msb
  0x00, //reg 0x3E, dac1_cntl_lsb
  0x00, //reg 0x3F, dac2_cntl_lsb
  0x00, //reg 0x40, dac3_cntl_lsb

  //Clip/Shift/Multiplier
  0x40, //reg 0x41, csm_clip_gy_low
  0x40, //reg 0x42, csm_clip_bcb_low
  0x40, //reg 0x43, csm_clip_rcr_low
  0x53, //reg 0x44, csm_clip_gy_high
  0x3F, //reg 0x45, csm_clip_bcb_high
  0x3F, //reg 0x46, csm_clip_rcr_high
  0x40, //reg 0x47, csm_shift_gy
  0x40, //reg 0x48, csm_shift_bcb
  0x40, //reg 0x49, csm_shift_rcr
  0x08, //reg 0x4A, csm_gy_ctrl_mult_msb
  0x00, //reg 0x4B, csm_mult_bcb_rcr_msb
  0x00, //reg 0x4C, csm_mult_gy_lsb
  0x00, //reg 0x4D, csm_mult_bcb_lsb
  0x00, //reg 0x4E, csm_mult_rcr_lsb
  0x00, //reg 0x4F, csm_rcr_bcb_ctrl

  //Display Timing Generation, Part 2
  0x00, //reg 0x50, dtg2_bp1_2_msb
  0x00, //reg 0x51, dtg2_bp3_4_msb
  0x00, //reg 0x52, dtg2_bp5_6_msb
  0x00, //reg 0x53, dtg2_bp7_8_msb
  0x00, //reg 0x54, dtg2_bp9_10_msb
  0x00, //reg 0x55, dtg2_bp11_12_msb
  0x00, //reg 0x56, dtg2_bp13_14_msb
  0x00, //reg 0x57, dtg2_bp15_16_msb
  0x00, //reg 0x58, dtg2_bp1_lsb
  0x00, //reg 0x59, dtg2_bp2_lsb
  0x00, //reg 0x5A, dtg2_bp3_lsb
  0x00, //reg 0x5B, dtg2_bp4_lsb
  0x00, //reg 0x5C, dtg2_bp5_lsb
  0x00, //reg 0x5D, dtg2_bp6_lsb
  0x00, //reg 0x5E, dtg2_bp7_lsb
  0x00, //reg 0x5F, dtg2_bp8_lsb
  0x00, //reg 0x60, dtg2_bp9_lsb
  0x00, //reg 0x61, dtg2_bp10_lsb
  0x00, //reg 0x62, dtg2_bp11_lsb
  0x00, //reg 0x63, dtg2_bp12_lsb
  0x00, //reg 0x64, dtg2_bp13_lsb
  0x00, //reg 0x65, dtg2_bp14_lsb
  0x00, //reg 0x66, dtg2_bp15_lsb
  0x00, //reg 0x67, dtg2_bp16_lsb
  0x00, //reg 0x68, dtg2_linetype1
  0x00, //reg 0x69, dtg2_linetype2
  0x00, //reg 0x6A, dtg2_linetype3
  0x00, //reg 0x6B, dtg2_linetype4
  0x00, //reg 0x6C, dtg2_linetype5
  0x00, //reg 0x6D, dtg2_linetype6
  0x00, //reg 0x6E, dtg2_linetype7
  0x00, //reg 0x6F, dtg2_linetype8
  0x60, //reg 0x70, dtg2_hlength_lsb
  0x00, //reg 0x71, dtg2_hlength_msb_hdly_msb
  0x02, //reg 0x72, dtg2_hdly_lsb
  0x03, //reg 0x73, dtg2_vlength1_lsb
  0x00, //reg 0x74, dtg2_vlength1_msb_vdly1_msb
  0x03, //reg 0x75, dtg2_vdly1_lsb
  0x00, //reg 0x76, dtg2_vlength2_lsb
  0x07, //reg 0x77, dtg2_vlength2_msb_vdly2_msb
  0xFF, //reg 0x78, dtg2_vdly2_lsb
  0x00, //reg 0x79, dtg2_hs_in_dly_msb
  0x3D, //reg 0x7A, dtg2_hs_in_dly_lsb
  0x00, //reg 0x7B, dtg2_vs_in_dly_msb
  0x03, //reg 0x7C, dtg2_vs_in_dly_lsb
  0x00, //reg 0x7D, dtg2_pixel_cnt_msb, status register
  0x00, //reg 0x7E, dtg2_pixel_cnt_lsb, status register
  0x00, //reg 0x7F, dtg2_line_cnt_msb,  status register
  0x00, //reg 0x80, dtg2_line_cnt_lsb,  status register
  0x00, //reg 0x81, reserved3, reserved
  0x5F, //reg 0x82, dtg2_cntl

  //CGMS Control
  0x00, //reg 0x83, cgms_header
  0x00, //reg 0x84, cgms_payload_msb
  0x00  //reg 0x85, cgms_payload_lsb

//0x00, //reg 0x85, misc_ppl_lsb, status register
//0x00, //reg 0x85, misc_ppl_msb, status register
//0x00, //reg 0x85, misc_lpf_lsb, status register
//0x00, //reg 0x85, misc_lpf_msb, status register
};

static char *THS8200RegNames[THS8200_MAX_REGISTERS] =
{
  //THS8200 Regiser Names - used as comments in WinVCC command files

  "reserved1",                    //0x00, reserved
  "reserved2",                    //0x01, reserved

  //System
  "version",                      //0x02, status_register
  "chip_ctl",                     //0x03

  //Color Space Conversion
  "csc_r11",                      //0x04
  "csc_r12",                      //0x05
  "csc_r21",                      //0x06
  "csc_r22",                      //0x07
  "csc_r31",                      //0x08
  "csc_r32",                      //0x09
  "csc_g11",                      //0x0A
  "csc_g12",                      //0x0B
  "csc_g21",                      //0x0C
  "csc_g22",                      //0x0D
  "csc_g31",                      //0x0E
  "csc_g32",                      //0x0F
  "csc_b11",                      //0x10
  "csc_b12",                      //0x11
  "csc_b21",                      //0x12
  "csc_b22",                      //0x13
  "csc_b31",                      //0x14
  "csc_b32",                      //0x15
  "csc_offs1",                    //0x16
  "csc_offs12",                   //0x17
  "csc_offs23",                   //0x18
  "csc_offs3",                    //0x19

  //Test
  "tst_cntl1",                    //0x1A
  "tst_cntl2",                    //0x1B

  //Data Path
  "data_cntl",                    //0x1C

  //Display Timing Generation, Part 1
  "dtg1_y_sync1_lsb",             //0x1D
  "dtg1_y_sync2_lsb",             //0x1E
  "dtg1_y_sync3_lsb",             //0x1F
  "dtg1_cbcr_sync1_lsb",          //0x20
  "dtg1_cbcr_sync2_lsb",          //0x21
  "dtg1_cbcr_sync3_lsb",          //0x22
  "dtg1_y_sync_msb",              //0x23
  "dtg1_cbcr_sync_msb",           //0x24
  "dtg1_spec_a",                  //0x25
  "dtg1_spec_b",                  //0x26
  "dtg1_spec_c",                  //0x27
  "dtg1_spec_d_lsb",              //0x28
  "dtg1_spec_d1",                 //0x29
  "dtg1_spec_e_lsb",              //0x2A
  "dtg1_spec_deh_msb",            //0x2B
  "dtg1_spec_h_lsb",              //0x2C
  "dtg1_spec_i_msb",              //0x2D
  "dtg1_spec_i_lsb",              //0x2E
  "dtg1_spec_k_lsb",              //0x2F
  "dtg1_spec_k_msb",              //0x30
  "dtg1_spec_k1",                 //0x31
  "dtg1_speg_g_lsb",              //0x32
  "dtg1_speg_g_msb",              //0x33
  "dtg1_total_pixels_msb",        //0x34
  "dtg1_total_pixels_lsb",        //0x35
  "dtg1_fieldflip_linecnt_msb",   //0x36
  "dtg1_linecnt_lsb",             //0x37
  "dtg1_mode",                    //0x38
  "dtg1_frame_field_size_msb",    //0x39
  "dtg1_frame_size_lsb",          //0x3A
  "dtg1_field_size_lsb",          //0x3B
  "dtg1_vesa_cbar_size",          //0x3C

  //DAC
  "dac_cntl_msb",                 //0x3D
  "dac1_cntl_lsb",                //0x3E
  "dac2_cntl_lsb",                //0x3F
  "dac3_cntl_lsb",                //0x40

  //Clip/Shift/Multiplier
  "csm_clip_gy_low",              //0x41
  "csm_clip_bcb_low",             //0x42
  "csm_clip_rcr_low",             //0x43
  "csm_clip_gy_high",             //0x44
  "csm_clip_bcb_high",            //0x45
  "csm_clip_rcr_high",            //0x46
  "csm_shift_gy",                 //0x47
  "csm_shift_bcb",                //0x48
  "csm_shift_rcr",                //0x49
  "csm_gy_ctrl_mult_msb",         //0x4A
  "csm_mult_bcb_rcr_msb",         //0x4B
  "csm_mult_gy_lsb",              //0x4C
  "csm_mult_bcb_lsb",             //0x4D
  "csm_mult_rcr_lsb",             //0x4E
  "csm_rcr_bcb_ctrl",             //0x4F

  //Display Timing Generation, Part 2
  "dtg2_bp1_2_msb",               //0x50
  "dtg2_bp3_4_msb",               //0x51
  "dtg2_bp5_6_msb",               //0x52
  "dtg2_bp7_8_msb",               //0x53
  "dtg2_bp9_10_msb",              //0x54
  "dtg2_bp11_12_msb",             //0x55
  "dtg2_bp13_14_msb",             //0x56
  "dtg2_bp15_16_msb",             //0x57
  "dtg2_bp1_lsb",                 //0x58
  "dtg2_bp2_lsb",                 //0x59
  "dtg2_bp3_lsb",                 //0x5A
  "dtg2_bp4_lsb",                 //0x5B
  "dtg2_bp5_lsb",                 //0x5C
  "dtg2_bp6_lsb",                 //0x5D
  "dtg2_bp7_lsb",                 //0x5E
  "dtg2_bp8_lsb",                 //0x5F
  "dtg2_bp9_lsb",                 //0x60
  "dtg2_bp10_lsb",                //0x61
  "dtg2_bp11_lsb",                //0x62
  "dtg2_bp12_lsb",                //0x63
  "dtg2_bp13_lsb",                //0x64
  "dtg2_bp14_lsb",                //0x65
  "dtg2_bp15_lsb",                //0x66
  "dtg2_bp16_lsb",                //0x67
  "dtg2_linetype1",               //0x68
  "dtg2_linetype2",               //0x69
  "dtg2_linetype3",               //0x6A
  "dtg2_linetype4",               //0x6B
  "dtg2_linetype5",               //0x6C
  "dtg2_linetype6",               //0x6D
  "dtg2_linetype7",               //0x6E
  "dtg2_linetype8",               //0x6F
  "dtg2_hlength_lsb",             //0x70
  "dtg2_hlength_msb_hdly_msb",    //0x71
  "dtg2_hdly_lsb",                //0x72
  "dtg2_vlength1_lsb",            //0x73
  "dtg2_vlength1_msb_vdly1_msb",  //0x74
  "dtg2_vdly1_lsb",               //0x75
  "dtg2_vlength2_lsb",            //0x76
  "dtg2_vlength2_msb_vdly2_msb",  //0x77
  "dtg2_vdly2_lsb",               //0x78
  "dtg2_hs_in_dly_msb",           //0x79
  "dtg2_hs_in_dly_lsb",           //0x7A
  "dtg2_vs_in_dly_msb",           //0x7B
  "dtg2_vs_in_dly_lsb",           //0x7C
  "dtg2_pixel_cnt_msb",           //0x7D, status register
  "dtg2_pixel_cnt_lsb",           //0x7E, status register
  "dtg2_line_cnt_msb",            //0x7F, status register
  "dtg2_line_cnt_lsb",            //0x80, status register
  "reserved3",                    //0x81, reserved
  "dtg2_cntl",                    //0x82

  //CGMS Control
  "cgms_header",                  //0x83
  "cgms_payload_msb",             //0x84
  "cgms_payload_lsb"              //0x85
//"misc_ppl_lsb",                 //0x86, status register
//"misc_ppl_msb",                 //0x87, status register
//"misc_lpf_lsb",                 //0x88, status register
//"misc_lpf_msb",                 //0x89, status register
};

static char *THS8200FileNames[41] =
{
  "THS8200_30bit_YCbCr_444_DS_to_YPbPr.txt",  //00h
  "THS8200_30bit_YCbCr_444_DS_to_VGA.txt",
  "THS8200_30bit_YCbCr_444_ES_to_YPbPr.txt",
  "THS8200_30bit_YCbCr_444_ES_to_VGA.txt",

  "THS8200_30bit_RGB_444_DS_to_YPbPr.txt",    //04h
  "THS8200_30bit_RGB_444_DS_to_VGA.txt",
  "THS8200_30bit_RGB_444_ES_to_YPbPr.txt", //not supported by software
  "THS8200_30bit_RGB_444_ES_to_VGA.txt",   //not supported by software

  "THS8200_command_file.txt", //08h
  "THS8200_command_file.txt",
  "THS8200_command_file.txt",
  "THS8200_command_file.txt",

  "THS8200_16bit_RGB_444_DS_to_YPbPr.txt", //0Ch
  "THS8200_16bit_RGB_444_DS_to_VGA.txt",
  "THS8200_16bit_RGB_444_ES_to_YPbPr.txt", //not supported by software
  "THS8200_16bit_RGB_444_ES_to_VGA.txt",   //not supported by software

  "THS8200_command_file.txt", //10h
  "THS8200_command_file.txt",
  "THS8200_command_file.txt",
  "THS8200_command_file.txt",

  "THS8200_15bit_RGB_444_DS_to_YPbPr.txt", //14h
  "THS8200_15bit_RGB_444_DS_to_VGA.txt",
  "THS8200_15bit_RGB_444_ES_to_YPbPr.txt", //not supported by software
  "THS8200_15bit_RGB_444_ES_to_VGA.txt",   //not supported by software

  "THS8200_20bit_YCbCr_422_DS_to_YPbPr.txt", //not supported by H/W, 18h
  "THS8200_20bit_YCbCr_422_DS_to_VGA.txt",   //not supported by H/W
  "THS8200_20bit_YCbCr_422_ES_to_YPbPr.txt",
  "THS8200_20bit_YCbCr_422_ES_to_VGA.txt",

  "THS8200_command_file.txt",
  "THS8200_command_file.txt",
  "THS8200_command_file.txt",
  "THS8200_command_file.txt",

  "THS8200_10bit_YCbCr_422_DS_to_YPbPr.txt",
  "THS8200_10bit_YCbCr_422_DS_to_VGA.txt",
  "THS8200_10bit_YCbCr_422_ES_to_YPbPr.txt",
  "THS8200_10bit_YCbCr_422_ES_to_VGA.txt",

  "THS8200_command_file.txt",
  "THS8200_command_file.txt",
  "THS8200_command_file.txt",
  "THS8200_command_file.txt",

  "THS8200_generic_dac_setup.txt" //WIP
};

char *CSCCoefNames[CSC_MAX_COEFFICIENTS] =
{
  //CSC Coefficient Names - used as comments in WinVCC command files

  "CSC_Coef.G1", //0
  "CSC_Coef.B1", //1
  "CSC_Coef.R1", //2
  "CSC_Coef.O1", //3
  "CSC_Coef.G2", //4
  "CSC_Coef.B2", //5
  "CSC_Coef.R2", //6
  "CSC_Coef.O2", //7
  "CSC_Coef.G3", //8
  "CSC_Coef.B3", //9
  "CSC_Coef.R3", //10
  "CSC_Coef.O3"  //11
};

static char *THS8200InputWidths[5] =
{
  "30-bit",
  "16-bit",
  "15-bit",
  "20-bit",
  "10-bit"
};

static char *THS8200InputColor[3] =
{
  "YCbCr",
  "YCbCr",
  "RGB"
};

static char *THS8200OutputColor[3] =
{
  "YPbPr SOY",
  "YPbPr SOY",
  "5-wire VGA"
};

static char *THS8200ColorRatio[2] =
{
  "4:4:4",
  "4:2:2"
};

static char *THS8200SyncType[2] =
{
  "ES", //embedded syncs
  "DS"  //discrete syncs
};

static char *THS8200SyncPol[2] =
{
  "-", //negative sync
  "+"  //positive sync
};

static char *THS8200VertScan[2] =
{
  "p", //progressive scan
  "i"  //interlaced scan
};

static char *THS8200FileModes[2] =
{
  "w", //write mode
  "a"  //append mode
};

typedef struct
{
  unsigned char       FileMode;
                      //0: Write, old file content discarded
                      //1: Append, old file content preserved
                      //2: Disable, file write
  unsigned char       PrintMode;
                      //0: Exclude regisers set to default values from command file
                      //1: Include all registers in command file
  unsigned char       CommentMode;
                      //0: Exclude register default settings in comments
                      //1: Include register default settings in comments

           char       FileName[256];
                      // if not specified then this filename is used instead of auto generated filename

  unsigned char       I2cMode;
                      // 0: Do not perform I2C write
                      // 1: Write registers to I2C device

  unsigned char       I2cDeviceAddr;
                      // I2C device address to use for THS8200
  unsigned char       DvoConfigMode;
                      // 0: Do NOT config DVO based on THS8200 timings
                      // 1: Config DM81xx DVO2 based on THS8200 timings


}THS8200_APPLICATION;

#endif
