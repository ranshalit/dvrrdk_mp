#ifndef CSC_CONFIG_INCLUDED_
#define CSC_CONFIG_INCLUDED_

#include <ths8200_drv.h>


#define CSC_CTL_PI                   3.14159265358979323846
#define CSC_MAX_COEFS                12

typedef struct
{
  int  G1;
  int  B1;
  int  R1;
  int  OFFS1;
  int  G2;
  int  B2;
  int  R2;
  int  OFFS2;
  int  G3;
  int  B3;
  int  R3;
  int  OFFS3;
} CSC_COEFFICIENTS;

typedef struct
{
  int  gy_min;
  int  gy_mid;
  int  gy_max;
  int  bcb_min;
  int  bcb_mid;
  int  bcb_max;
  int  rcr_min;
  int  rcr_mid;
  int  rcr_max;
} CSC_IO_RANGE;

typedef struct
{
  CSC_IO_RANGE Input;
  CSC_IO_RANGE Output;
} CSC_RANGE;


typedef float CSC_COEFS[CSC_MAX_COEFS];

//prototypes
void InitializeCSC(CSC_COEFFICIENTS *CSC_Coef, CSC_RANGE *Range, CSC_CONTROL *CSC_Ctl);
void ModifyCSC4IORange(CSC_COEFFICIENTS *CSC_Coef, CSC_RANGE *Range);
void ModifyCSC4ColorCtl(CSC_COEFFICIENTS *CSC_Coef, CSC_RANGE *Range, CSC_CONTROL *CSC_Ctl);
void CSC_CbCrSwap(CSC_COEFFICIENTS *CSC_Coef, CSC_CONTROL *CSC_Ctl);

#endif
