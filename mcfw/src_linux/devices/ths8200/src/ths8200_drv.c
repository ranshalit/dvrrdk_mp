
#include <ths8200_drv.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "THS8200Config.h"
#include "VideoTimings.h"



int THS8200_setDefaultConfig(THS8200_CONFIGURATION *configuration)
{
  VIDEO_TIMINGS CurrentInfo;

  CurrentInfo = VESA_SMT_Timings[1]; //720x480 @ 60Hz, HS/VS -/-

  configuration->Timing.Hactive = CurrentInfo.Hactive;
  configuration->Timing.Vactive = CurrentInfo.Vactive;
  configuration->Timing.PClk = CurrentInfo.PixelFreq;
  configuration->Timing.IP = CurrentInfo.IP;
  configuration->Timing.HFP = CurrentInfo.HFP;
  configuration->Timing.HSW = CurrentInfo.HSW;
  configuration->Timing.HBP = CurrentInfo.HBP;
  configuration->Timing.VFP = CurrentInfo.VFP;
  configuration->Timing.VSW = CurrentInfo.VSW;
  configuration->Timing.VBP = CurrentInfo.VBP;
  configuration->Timing.HPol = CurrentInfo.HPol;
  configuration->Timing.VPol = CurrentInfo.VPol;

  //Primary Controls: The following input parameters need to be specified for most applications.

  configuration->InputWidth = 0;
  //0: 30-bit YCbCr/RGB 4:4:4
  //1: 16-bit RGB 4:4:4
  //2: 15-bit RGB 4:4:4
  //3: 20-bit YCbCr 4:2:2
  //4: 10-bit YCbCr 4:2:2 (BT.656 mode)

  configuration->CSC.OutputColor = 0;
  //0: SDTV (YCbCr) color space - 480i, 576i, 480p, 576p
  //1: HDTV (YCbCr) color space - 720p, 1080i, 1080p
  //2: RGB color space
  //3: Reserved

  configuration->CSC.InputColor = -1;
  configuration->CSC.InputRange = -1;

  configuration->CSC.OutputRange = 0; //CSC full-range output has never been used or tested
  //0: Limited range (64 - 940)
  //1: Full range (0 - 1023)

  configuration->InputSyncs = -1;

  //Secondary Controls: The following input parameters should not need to be specified for most applications.

  configuration->DTGMode = 1;
  //0: Preset
  //1: Generic (default)

  configuration->TimingMode = -1;
  configuration->OutputSyncs = -1;
  configuration->OutputSOG = -1;

  configuration->UpSampling = 1; //for YPbPr outputs only
  //0: 2x up-sampling disabled
  //1: 2x up-sampling enabled (up to 80MHz) (default)

  configuration->FID_DE_Cntl = 0; //for discrete syncs only
  //0: Discrete FID input on Pin 47 (default)
  //1: DE input for VESA preset mode on Pin 47 or internal FID decode

  configuration->InputSyncPol = 0; //specifies sync polarity expected from VESA/CEA timing standard
  //0: Normal   (0: Neg, 1: Pos) (default)
  //1: Inverted (0: Pos, 1: Neg)

  //Tertiary Controls: The following input parameters are intended for internal use only.

  configuration->PowerMgmt = 0;
  //0: Normal operation (default)
  //1: Digital logic power down
  //2: DAC power down
  //3: DAC and digital logic power down

  configuration->FIDPolarity = 0;
  //0: Negative polarity, Field 1 = FID input low (default)
  //1: Positive polarity, Field 1 = FID input high

  configuration->CSC.CbCrSwap = 0;
  //0: Disabled (default)
  //1: Swap CSC coefficients for B/Cb and R/Cr inputs  (DAC mode)
  //2: Swap CSC coefficients for B/Cb and R/Cr outputs (ADC mode)

  configuration->BT656Output = 0; //for 10-bit input mode only
  //0: Disabled (default)
  //1: Enabled

  configuration->MasterSlave = 0; //for VESA preset mode only
  //0: Slave mode
  //1: Master mode

  configuration->GenericDAC = 0; //for 30-bit VESA preset mode only
  //0: Disabled
  //1: Enabled (CSM multiply disabled, dtg1_on disabled)

  configuration->VideoSyncRatio = 0; //for 480i/p outputs only
  //0:  7:3 w/o pedestal (CEA 770.2)
  //1: 10:4 w/  pedestal (CEA 770.1) - obsolete standard

  configuration->CGMSEnable = 0; //for SDTV generic 480p mode only
  //0: Disabled
  //1: Enabled

  configuration->CSC.Brightness = 0; //-128 <= Brightness <= +127 counts
  configuration->CSC.Contrast   = 100; //   0 <= Contrast   <=  200 percent
  configuration->CSC.Saturation = 100; //   0 <= Saturation <=  200 percent
  configuration->CSC.Hue        = 0; // -30 <= Hue        <=  +30 degrees

  //Work in progress: The following input parameters are not currently supported by software.

  configuration->TestPattern = 0; //for VESA preset mode only

  return 0;
}


int THS8200_getRegs(THS8200_CONFIGURATION *configuration, THS8200_REGS regs, int InputTiming, int InputFormat)
{
  VIDEO_TIMINGS CurrentInfo;
  int FormatMode, FormatID;
  int FormatMSB, FormatLSB;
  int i;

  CurrentInfo = VESA_SMT_Timings[1]; //720x480 @ 60Hz, HS/VS -/-

  if(InputTiming==-1)
    InputTiming = 0;

  //Select desired timing format from selected video timing standard
  if(InputFormat != -1)
  {
    FormatID = 0;  //initialize stack variable
    FormatMSB = 0; //initialize stack variable (for CEA timing formats with a 16:9 aspect ratio)
    FormatLSB = 0; //initialize stack variable (for CEA timing formats with a  4:3 aspect ratio and VESA timing formats)

    for (i = 0; (FormatMSB != InputFormat) && (FormatLSB != InputFormat) && (FormatID != THS8200_LAST_FMT); i++)
    {
      if(InputTiming == THS8200_VESA_DMT_TIMING)
      {
        CurrentInfo = VESA_DMT_Timings[i]; //VESA-DMT timing
      }
      else if(InputTiming == THS8200_CEA_REVISED_TIMING)
      {
        CurrentInfo = CEA_REVISED_Timings[i]; //CEA-861 timing (supports 487i, 576i and 483p)
      }
      else if(InputTiming == THS8200_CEA_TIMING)
      {
        CurrentInfo = CEA_Timings[i]; //CEA-861 timing (supports 480i, 576i and 480p)
      }
      else if(InputTiming == THS8200_VESA_CVT_TIMING)
      {
        CurrentInfo = VESA_CVT_Timings[i]; //VESA-CVT timing
      }
      else //if(InputTiming == THS8200_VESA_SMT_TIMING)
      {
        CurrentInfo = VESA_SMT_Timings[i]; //VESA-SMT timing
      }

      FormatID = CurrentInfo.Format;
      FormatMSB = CurrentInfo.Format >> 8;     //for CEA timing (MSB:16:9, LSB:4:3)
      FormatLSB = CurrentInfo.Format & 0x00FF;
    }

    if((FormatID != THS8200_LAST_FMT) && (CurrentInfo.PixelFreq <= THS8200_MAX_PCLK_DAC))
    {
      FormatMode = THS8200_VALID_STD_FMT;
    }
    else
    {
      FormatMode = THS8200_INVALID_FMT; //Specified input format not supported
    }
  }
  else
  {
    if(configuration->Timing.Hactive != -1 &&
       configuration->Timing.Vactive != -1 &&
       configuration->Timing.PClk != -1 &&
       configuration->Timing.IP   != -1 &&
       configuration->Timing.HFP  != -1 &&
       configuration->Timing.HSW  != -1 &&
       configuration->Timing.HBP  != -1 &&
       configuration->Timing.VFP  != -1 &&
       configuration->Timing.VSW  != -1 &&
       configuration->Timing.VBP  != -1 &&
       configuration->Timing.HPol != -1 &&
       configuration->Timing.VPol != -1)
    {
      FormatMode = THS8200_VALID_CUSTOM_FMT;
    }
    else
    {
      FormatMode = THS8200_INVALID_FMT; //Missing one or more required timing parameters
    }
  }

  if(FormatMode == THS8200_VALID_CUSTOM_FMT)
  {

  }
  else
  {
    if(FormatMode == THS8200_INVALID_FMT) //Use safe-mode timing formats when missing any required timing parameters
    {
      if((InputTiming == THS8200_VESA_DMT_TIMING) || (InputTiming == THS8200_VESA_CVT_TIMING))
      {
        CurrentInfo = VESA_SMT_Timings[0]; //640x480 @ 60Hz, HS/VS -/-
      }
      else //if((InputTiming == CEA_TIMING) || (InputTiming == CEA_REVISED_TIMING))
      {
        CurrentInfo = VESA_SMT_Timings[1]; //720x480 @ 60Hz, HS/VS -/-
      }
    }

    configuration->Timing.Hactive = CurrentInfo.Hactive;
    configuration->Timing.Vactive = CurrentInfo.Vactive;
    configuration->Timing.PClk = CurrentInfo.PixelFreq;
    configuration->Timing.IP = CurrentInfo.IP;
    configuration->Timing.HFP = CurrentInfo.HFP;
    configuration->Timing.HSW = CurrentInfo.HSW;
    configuration->Timing.HBP = CurrentInfo.HBP;
    configuration->Timing.VFP = CurrentInfo.VFP;
    configuration->Timing.VSW = CurrentInfo.VSW;
    configuration->Timing.VBP = CurrentInfo.VBP;
    configuration->Timing.HPol = CurrentInfo.HPol;
    configuration->Timing.VPol = CurrentInfo.VPol;
  }


  if(configuration->CSC.InputColor==-1)
    configuration->CSC.InputColor = configuration->CSC.OutputColor;

  if(configuration->CSC.InputRange==-1)
    configuration->CSC.InputRange = configuration->CSC.InputColor >> 1;

  if(configuration->InputSyncs==-1)
    configuration->InputSyncs = (configuration->CSC.InputColor >> 1);
  //0: Embedded Syncs
  //1: Discrete Syncs

  //Implement don't care conditions to handle invalid input formats
  //For example, the InputColor parameter is treated as a don't care condition for
  //15/16-bit inputs since the THS8200 only supports RGB inputs for this bus width.
  if(configuration->InputWidth == THS8200_INPUT_WIDTH_30)      //30-bit YCbCr/RGB 4:4:4
  {
//  configuration->CSC.InputColor = configuration->CSC.InputColor;
    configuration->InputRatio = THS8200_INPUT_RATIO_444;
//  configuration->InputSyncs = configuration->InputSyncs;
  }
  else if(configuration->InputWidth == THS8200_INPUT_WIDTH_20) //20-bit YCbCr 4:2:2
  {
    configuration->CSC.InputColor = configuration->CSC.InputColor & 0x01; //SDTV or HDTV
    configuration->InputRatio = THS8200_INPUT_RATIO_422;
//  configuration->InputSyncs = configuration->InputSyncs;
  }
  else if(configuration->InputWidth == THS8200_INPUT_WIDTH_10) //10-bit YCbCr 4:2:2
  {
    configuration->CSC.InputColor = CSC_CTL_INPUT_COLOR_SDTV; //SDTV only
    configuration->InputRatio = THS8200_INPUT_RATIO_422;
    configuration->InputSyncs = THS8200_INPUT_SYNCS_ES;
  }
  else                                                   //15/16-bit RGB 4:4:4
  {
    configuration->CSC.InputColor = CSC_CTL_INPUT_COLOR_RGB;
    configuration->InputRatio = THS8200_INPUT_RATIO_444;
    configuration->InputSyncs = THS8200_INPUT_SYNCS_DS;
  }

  if(configuration->TimingMode == THS8200_DTG_TIMING_SDTV)
  {
    if(configuration->Timing.Vactive == THS8200_VACTIVE_SDTV_576) //576p
    {
      configuration->DTGMode = THS8200_DTG_GENERIC_MODE; //SDTV generic mode required
    }
  }
  else if(configuration->TimingMode == THS8200_DTG_TIMING_VESA)
  {
    if(configuration->OutputSOG == 1) //SOG enabled
    {
      configuration->DTGMode = THS8200_DTG_GENERIC_MODE; //VESA generic mode required
    }

//  if(configuration->InputSyncs == THS8200_INPUT_SYNCS_ES)
//  {
//    configuration->DTGMode = THS8200_DTG_GENERIC_MODE; //VESA generic mode highly recommended
//  }

//  if((configuration->InputSyncs == THS8200_INPUT_SYNCS_DS) && (THS8200->FID_DE_Cntl == 1)) //DE operation in VESA preset mode or internal FID decode
//  {
//    configuration->DTGMode = THS8200_DTG_PRESET_MODE; //VESA preset mode most likely preferred
//  }
  }

  if(configuration->TimingMode==-1)
    configuration->TimingMode = configuration->CSC.OutputColor;
  //0: SDTV timing (supports bi-level syncs with equalization/serration pulses)
  //1: HDTV timing (supports tri-level syncs with equalization/serration pulses)
  //2: VESA timing (supports bi-level syncs without equalization/serration pulses)
  //3: Reserved

  if(configuration->OutputSyncs==-1)
    configuration->OutputSyncs = configuration->CSC.OutputColor >> 1;
  //0: Discrete syncs disabled
  //1: Discrete syncs enabled

  if(configuration->OutputSOG==-1)
    configuration->OutputSOG = !(configuration->CSC.OutputColor >> 1);
  //0: SOG disabled
  //1: SOG enabled
  //2: Sync on G, B and R enabled (not supported by software)
  //forces preset or generic mode when required

  if(configuration->TimingMode == THS8200_DTG_TIMING_VESA) //VESA preset/generic modes
  {
    configuration->UpSampling = 0; //2x up-sampling disabled
  }
  else if(configuration->Timing.PClk > THS8200_MAX_PCLK_DLL_HIGH) //1080p60, 1080p50 only
  {
    configuration->UpSampling = 0; //2x up-sampling disabled
  }

  THS8200SetConfig(configuration, regs);

  return 0;
}

#ifdef _HOST_BUILD_
int THS8200_i2cConfig(THS8200_REGS regs, unsigned int i2cInstId, unsigned int deviceAddr)
{
    return 0;
}

int THS8200_dvoConfig(THS8200_CONFIGURATION *cfg, unsigned int dvoInstId)
{
    return 0;
}
#endif