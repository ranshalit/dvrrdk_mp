

#ifndef _THS8200_DRV_H_
#define _THS8200_DRV_H_


#define THS8200_I2C_DEFAULT_INST_ID  (0)

//Input data manager modes (reg1C_data_dman_cntl)
#define THS8200_INPUT_WIDTH_30       0 // 30-bit YCbCr/RGB 4:4:4
#define THS8200_INPUT_WIDTH_16       1 // 16-bit RGB 4:4:4
#define THS8200_INPUT_WIDTH_15       2 // 15-bit RGB 4:4:4
#define THS8200_INPUT_WIDTH_20       3 // 20-bit YCbCr 4:2:2
#define THS8200_INPUT_WIDTH_10       4 // 10-bit YCbCr 4:2:2 (BT.656 mode)

//Input modes
#define THS8200_INPUT_RATIO_444      0 // 4:4:4 chroma subsampling ratio
#define THS8200_INPUT_RATIO_422      1 // 4:2:2 chroma subsampling ratio

#define THS8200_INPUT_SYNCS_ES       0 // Embedded syncs
#define THS8200_INPUT_SYNCS_DS       1 // Discrete syncs

#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
#undef THS8200_I2C_DEFAULT_INST_ID
#define THS8200_I2C_DEFAULT_INST_ID  (3)
#endif

#ifdef TI_816X_BUILD
#undef THS8200_I2C_DEFAULT_INST_ID
#define THS8200_I2C_DEFAULT_INST_ID  (2)
#endif

#define THS8200_I2C_DEFAULT_DEVICE_ADDR     (0x20)


//Input modes
#define CSC_CTL_INPUT_RANGE_LMTD     0 // Limited range inputs
#define CSC_CTL_INPUT_RANGE_FULL     1 // Full range inputs

#define CSC_CTL_INPUT_COLOR_SDTV     0 // SDTV (YCbCr) color space (BT.601 compliant)
#define CSC_CTL_INPUT_COLOR_HDTV     1 // HDTV (YCbCr) color space (BT.709 compliant)
#define CSC_CTL_INPUT_COLOR_RGB      2 // RGB color space

//Output modes
#define CSC_CTL_OUTPUT_RANGE_LMTD    0 // Limited range inputs
#define CSC_CTL_OUTPUT_RANGE_FULL    1 // Full range inputs

#define CSC_CTL_OUTPUT_COLOR_SDTV    0 // SDTV (YCbCr) color space (BT.601 compliant)
#define CSC_CTL_OUTPUT_COLOR_HDTV    1 // HDTV (YCbCr) color space (BT.709 compliant)
#define CSC_CTL_OUTPUT_COLOR_RGB     2 // RGB color space


#define THS8200_TI81XX_DVO1     (1)
#define THS8200_TI81XX_DVO2     (2)


#define THS8200_MAX_REGS             0x86 //Total number of THS8200 registers (0x00 to 0x85)

typedef struct
{
  int  InputColor;    /* CSC_CTL_INPUT_COLOR_xxx */
  int  InputRange;    /* CSC_CTL_INPUT_RANGE_xxx */
  int  OutputColor;   /* CSC_CTL_OUTPUT_COLOR_xxx */
  int  OutputRange;   /* CSC_CTL_OUTPUT_RANGE_xxx */
  int  Brightness;
  int  Contrast;
  int  Saturation;
  int  Hue;
  int  CbCrSwap;
} CSC_CONTROL;

typedef struct
{
  int    Hactive;
  int    Vactive;
  float  PClk;
  char   IP;
  int    HFP;
  int    HSW;
  int    HBP;
  int    VFP;
  int    VSW;
  int    VBP;
  int    HPol;
  int    VPol;
} THS8200_TIMING;

typedef struct
{
  //Primary Controls
  int       InputWidth;
                      //0: 30-bit YCbCr/RGB 4:4:4
                      //1: 16-bit RGB 4:4:4
                      //2: 15-bit RGB 4:4:4
                      //3: 20-bit YCbCr 4:2:2
                      //4: 10-bit YCbCr 4:2:2 (BT.656 mode)
                      //5-7: Reserved
  int       InputSyncs; //for 20-bit and 30-bit YCbCr inputs only
                      //0: Embedded Syncs
                      //1: Discrete Syncs
  int       InputRatio;
                      //0: 4:4:4
                      //1: 4:2:2 (YCbCr)

  //Secondary Controls
  int       DTGMode;
                      //0: Preset
                      //1: Generic
  int       TimingMode; //controls DTG modes
                      //0: SDTV timing (supports bi-level syncs with equalization/serration pulses)
                      //1: HDTV timing (supports tri-level syncs with equalization/serration pulses)
                      //2: VESA timing (supports bi-level syncs without equalization/serration pulses)
                      //3: Reserved
  int       OutputSyncs;
                      //0: Discrete syncs disabled
                      //1: Discrete syncs enabled
  int       OutputSOG; //controls CSM settings
                      //0: SOG disabled
                      //1: SOG enabled
                      //2: Sync on G, B and R enabled (not supported by software)
  int       UpSampling; //for YPbPr outputs only
                      //0: 2x up-sampling disabled
                      //1: 2x up-sampling enabled (up to 80MHz)
  int       FID_DE_Cntl; //for discrete syncs only, (bit 7 of register 0x82)
                      //0: Discrete FID input on Pin 47
                      //1: DE input for VESA preset mode on Pin 47 or internal FID decode
  int       InputSyncPol; //specifies sync polarity expected from VESA/CEA timing standard
                      //0: Normal   (0: Neg, 1: Pos)
                      //1: Inverted (0: Pos, 1: Neg)

  //Tertiary Controls
  int       PowerMgmt;
                      //0: Normal operation
                      //1: Digital logic power down
                      //2: DAC power down
                      //3: DAC and digital logic power down
  int       FIDPolarity; //for VGA outputs only
                      //0: Negative polarity, Field 1 = FID input low
                      //1: Positive polarity, Field 1 = FID input high

  int       BT656Output; //for 10-bit input mode only
                      //0: Disabled
                      //1: Enabled
  int       MasterSlave; //for VESA preset mode only
                      //0: Slave mode
                      //1: Master mode
  int       GenericDAC; //for 30-bit VESA preset mode only
                      //0: Disabled
                      //1: Enabled (CSM multiply disabled, dtg1_on disabled)
  int       VideoSyncRatio; //for SDTV YPbPr outputs only
                      //0:  7:3 w/o pedestal (CEA 770.2)
                      //1: 10:4 w/  pedestal (CEA 770.1) - obsolete standard
  int       CGMSEnable; //for SDTV generic 480p mode only
                      //0: Disabled
                      //1: Enabled

  //Work in progress
  int       TestPattern; //for VESA preset mode only
                      //0: Disabled
                      //1: Enabled

  THS8200_TIMING      Timing;
  CSC_CONTROL         CSC;
}THS8200_CONFIGURATION;

typedef unsigned int THS8200_REGS[THS8200_MAX_REGS];

/*
    Set THS8200_CONFIGURATION to default values
*/
int THS8200_setDefaultConfig(THS8200_CONFIGURATION *cfg);


/*
    Based on THS8200_CONFIGURATION get THS8200 register values
    THS8200 I2C programming does not happen here
*/
int THS8200_getRegs(THS8200_CONFIGURATION *configuration, THS8200_REGS regs, int InputTiming, int InputFormat);

/*
    Based on device register config program the THS8200 device via i2c
*/
int THS8200_i2cConfig(THS8200_REGS regs, unsigned int i2cInstId, unsigned int deviceAddr);

/*
    Based on THS8200_CONFIGURATION program the Digital video output for TI81xx devices

    dvoInstId = THS8200_TI81XX_DVOx
*/
int THS8200_dvoConfig(THS8200_CONFIGURATION *cfg, unsigned int dvoInstId);

#endif

