# THS8200 Generic DAC setup, 30-bit 4:4:4 full-range (0 - 1023) input to full-range (0 - 700mV) output
# Output File: c:\\THS8200_generic_dac_setup.txt

# For use without THS8200 input/output syncs

# For Generic DAC mode, the input controls need to be set as follows.
# -input_width = 0 (30-bit 4:4:4 input)
# -input_color = -output_color (CSC bypassed)
# -cbcr_swap   = 0 (CSC bypassed)
# -input_range = 1 (full-range 0-1023)
# -timing      = 2 (VESA timing mode)
# -csm_bypass  = 1 (CSM bypassed)

# Generic DAC
./ths8200_config.out -timing_std 0 -input_width 0 -input_range 1 -input_color 2 -input_syncs 1 -output_color 2 -append_mode 0 -print_mode 0 -dtg_mode 0 -output_syncs 0 -fid_de_cntl 0 -gen_dac 1

# Generic DAC with DE Blanking Enabled
./ths8200_config.out -timing_std 0 -input_width 0 -input_range 1 -input_color 2 -input_syncs 1 -output_color 2 -append_mode 1 -print_mode 0 -dtg_mode 0 -output_syncs 0 -fid_de_cntl 1 -gen_dac 1

# Definition of input parameters
# 
# -timing_std
#  0: VESA-DMT timing standard
#  1: CEA-861 timing standard
# 
# -format
#  xx: Desired format ID from selected timing standard (decimal numbers only)
#  Example: Format ID of SXGA @ 60Hz is 35d
# 
# -input_width
#  0: 30-bit
#  1: 16-bit
#  2: 15-bit
#  3: 20-bit
#  4: 10-bit
# 
# -input_range
#  0: Limited range (64 - 940)
#  1: Full range (0 - 1023)
# 
# -input_color
#  0: SDTV (YCbCr) color space (ITU-R BT.601 compliant)
#  1: HDTV (YCbCr) color space (ITU-R BT.709 compliant)
#  2: RGB color space
#  3: Reserved
# 
# -input_syncs
#  0: Embedded syncs
#  1: Discrete syncs
# 
# -output_color
#  0: SDTV (YCbCr) color space (ITU-R BT.601 compliant)
#  1: HDTV (YCbCr) color space (ITU-R BT.709 compliant)
#  2: RGB color space
#  3: Reserved
# 
# -append_mode
#  0: Write, old file content discarded
#  1: Append, old file content preserved
# 
# -print_mode
#  0: Exclude regisers set to default values from command file
#  1: Include all registers in command file
# 
# -dtg_mode
#  0: Preset
#  1: Generic (default)
# 
# -output_syncs
#  0: Discrete syncs disabled
#  1: Discrete syncs enabled
# 
# -fid_de_cntl # for discrete syncs only
#  0: Discrete FID input on Pin 47 (default)
#  1: DE input for VESA preset mode on Pin 47 or internal FID decode
# 
# -gen_dac # for 30-bit VESA preset mode only
#  0: Disabled (default)
#  1: Enabled (CSM multiply disabled, dtg1_on disabled)
# 
# See THS8200_ReadMe.txt file for additional input parameters.