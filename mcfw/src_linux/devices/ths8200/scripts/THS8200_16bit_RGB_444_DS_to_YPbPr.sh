# 16-bit full-range RGB 4:4:4 input with discrete syncs to YPbPr output with SOG
# Output File: c:\\THS8200_16bit_RGB_444_DS_to_YPbPr.txt

# 480i60
./ths8200_config.out -timing_std 1 -format 6 -input_width 1 -input_range 1 -input_color 2 -input_syncs 1 -output_color 0 -append_mode 0 -print_mode 1

# 576i50
./ths8200_config.out -timing_std 1 -format 21 -input_width 1 -input_range 1 -input_color 2 -input_syncs 1 -output_color 0 -append_mode 1 -print_mode 1

# 480p60
./ths8200_config.out -timing_std 1 -format 2 -input_width 1 -input_range 1 -input_color 2 -input_syncs 1 -output_color 0 -append_mode 1 -print_mode 1

# 576p50
./ths8200_config.out -timing_std 1 -format 17 -input_width 1 -input_range 1 -input_color 2 -input_syncs 1 -output_color 0 -append_mode 1 -print_mode 1

# 720p60
./ths8200_config.out -timing_std 1 -format 4 -input_width 1 -input_range 1 -input_color 2 -input_syncs 1 -output_color 1 -append_mode 1 -print_mode 1

# 720p50
./ths8200_config.out -timing_std 1 -format 19 -input_width 1 -input_range 1 -input_color 2 -input_syncs 1 -output_color 1 -append_mode 1 -print_mode 1

# 1080i60
./ths8200_config.out -timing_std 1 -format 5 -input_width 1 -input_range 1 -input_color 2 -input_syncs 1 -output_color 1 -append_mode 1 -print_mode 1

# 1080i50
./ths8200_config.out -timing_std 1 -format 20 -input_width 1 -input_range 1 -input_color 2 -input_syncs 1 -output_color 1 -append_mode 1 -print_mode 1

# 1080p60
./ths8200_config.out -timing_std 1 -format 16 -input_width 1 -input_range 1 -input_color 2 -input_syncs 1 -output_color 1 -append_mode 1 -print_mode 1

# 1080p50
./ths8200_config.out -timing_std 1 -format 31 -input_width 1 -input_range 1 -input_color 2 -input_syncs 1 -output_color 1 -append_mode 1 -print_mode 1

# Definition of input parameters
# 
# -timing_std
#  0: VESA-DMT timing standard
#  1: CEA-861 timing standard
# 
# -format
#  xx: Desired format ID from selected timing standard (decimal numbers only)
#  Example: Format ID of SXGA @ 60Hz is 35d
# 
# -input_width
#  0: 30-bit
#  1: 16-bit
#  2: 15-bit
#  3: 20-bit
#  4: 10-bit
# 
# -input_range
#  0: Limited range (64 - 940)
#  1: Full range (0 - 1023)
# 
# -input_color
#  0: SDTV (YCbCr) color space (ITU-R BT.601 compliant)
#  1: HDTV (YCbCr) color space (ITU-R BT.709 compliant)
#  2: RGB color space
#  3: Reserved
# 
# -input_syncs
#  0: Embedded syncs
#  1: Discrete syncs
# 
# -output_color
#  0: SDTV (YCbCr) color space (ITU-R BT.601 compliant)
#  1: HDTV (YCbCr) color space (ITU-R BT.709 compliant)
#  2: RGB color space
#  3: Reserved
# 
# -append_mode
#  0: Write, old file content discarded
#  1: Append, old file content preserved
# 
# -print_mode
#  0: Exclude regisers set to default values from command file
#  1: Include all registers in command file
# 
# See THS8200_ReadMe.txt file for additional input parameters.