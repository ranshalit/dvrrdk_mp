/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#ifndef _DEVICE_TW2968_PRIV_H_
#define _DEVICE_TW2968_PRIV_H_

#include <osa_sem.h>
#include <osa_i2c.h>

#include "ti_vcap_common_def.h"

#define DEVICE_TW2968_ENABLE_960H_MODE
//#define DEVICE_TW2968_DEBUG

#define DEVICE_TW2968_CH_PER_DEVICE_MAX    (8)

#define DEVICE_TW2968_REG_PAGE      (0x40)

#define DEVICE_TW2968_REG_BASE_VIN1 (0x00)
#define DEVICE_TW2968_REG_BASE_VIN2 (0x10)
#define DEVICE_TW2968_REG_BASE_VIN3 (0x20)
#define DEVICE_TW2968_REG_BASE_VIN4 (0x30)

#define DEVICE_TW2968_REG_BASE_VIN5 (0x00)
#define DEVICE_TW2968_REG_BASE_VIN6 (0x10)
#define DEVICE_TW2968_REG_BASE_VIN7 (0x20)
#define DEVICE_TW2968_REG_BASE_VIN8 (0x30)

#define DEVICE_TW2968_REG_OFF_VID_STATUS   (0x0)
#define DEVICE_TW2968_REG_OFF_BRIGHTNESS   (0x1)
#define DEVICE_TW2968_REG_OFF_CONTRAST     (0x2)
#define DEVICE_TW2968_REG_OFF_SHARPNESS    (0x3)
#define DEVICE_TW2968_REG_OFF_SAT_U        (0x4)
#define DEVICE_TW2968_REG_OFF_SAT_V        (0x5)
#define DEVICE_TW2968_REG_OFF_HUE          (0x6)
#define DEVICE_TW2968_REG_OFF_CROP_HACTVIE_HIGH     (0x7)
#define DEVICE_TW2968_REG_OFF_VDELAY_LOW            (0x8)
#define DEVICE_TW2968_REG_OFF_HACTIVE_LOW           (0xB)
#define DEVICE_TW2968_REG_OFF_CHIP_STATUS2 (0xD)
#define DEVICE_TW2968_REG_OFF_STD_SEL      (0xE)

/* Page 0 registers */
#define DEVICE_TW2968_REG_VID_CLK_SEL               (0x61)
#define DEVICE_TW2968_REG_CHANNEL_ID_12             (0x63)
#define DEVICE_TW2968_REG_CHANNEL_ID_34             (0x64)
#define DEVICE_TW2968_REG_CHANNEL_ID_56             (0x65)
#define DEVICE_TW2968_REG_CHANNEL_ID_78             (0x66)
#define DEVICE_TW2968_REG_VID_BUS_TRI_STATE_CTRL    (0x6F)
#define DEVICE_TW2968_REG_SOFT_RESET                (0x80) /* also in PAGE1 */
#define DEVICE_TW2968_REG_BGCTL_MISC_CTL2           (0x96)
#define DEVICE_TW2968_REG_NOVID                     (0x9E)
#define DEVICE_TW2968_REG_CLK_OUT_DELAY             (0x9F)
#define DEVICE_TW2968_REG_960HEN                    (0xCC)
#define DEVICE_TW2968_REG_O36M                      (0xCD)
#define DEVICE_TW2968_REG_VID_OUT_MODE              (0xE7)
#define DEVICE_TW2968_REG_VID_CH_SEL_VD1_12         (0xE8)
#define DEVICE_TW2968_REG_VID_CH_SEL_VD1_34         (0xE9)
#define DEVICE_TW2968_REG_VID_CH_SEL_VD2_12         (0xEA)
#define DEVICE_TW2968_REG_VID_CH_SEL_VD2_34         (0xEB)
#define DEVICE_TW2968_REG_VID_CH_SEL_VD3_12         (0xEC)
#define DEVICE_TW2968_REG_VID_CH_SEL_VD3_34         (0xED)
#define DEVICE_TW2968_REG_VID_CH_SEL_VD4_12         (0xEE)
#define DEVICE_TW2968_REG_VID_CH_SEL_VD4_34         (0xEF)
#define DEVICE_TW2968_REG_VID_MISC_CTRL             (0xF9)
#define DEVICE_TW2968_REG_OUT_ENABLE                (0xFA)
#define DEVICE_TW2968_REG_CLK_POL_CTRL              (0xFB) /* can be used for inverting clock polarity */
#define DEVICE_TW2968_REG_DEVICE_ID                 (0xFE)
#define DEVICE_TW2968_REG_REV_ID                    (0xFF)



#define DEVICE_TW2968_VID_DETECT_MASK               ((1<<6)|(1<<5)|(1<<3))
#define DEVICE_TW2968_VID_LOSS_DETECT_MASK          (1<<7)
#define DEVICE_TW2968_VID_50HZ_DETECT_MASK          (1<<0)

#define DEVICE_TW2968_NTSC_PAL_WIDTH_960H           (960)
#define DEVICE_TW2968_NTSC_PAL_WIDTH_720H           (720)

#define DEVICE_TW2968_NTSC_HEIGHT                   (240)
#define DEVICE_TW2968_PAL_HEIGHT                    (288)

#define DEVICE_TW2968_VID_STD_NTSC_MJ               (0)
#define DEVICE_TW2968_VID_STD_PAL_BDGHIN            (1)
#define DEVICE_TW2968_VID_STD_NTSC_4_43             (3)
#define DEVICE_TW2968_VID_STD_PAL_M                 (4)
#define DEVICE_TW2968_VID_STD_PAL_COMB_N            (5)
#define DEVICE_TW2968_VID_STD_PAL_60                (6)

#define DEVICE_TW2968_CHIP_ID                       (0x001E)

/*
  TW2968 Object
*/
typedef struct
{
    OSA_I2cHndl i2cHandle;

    Device_VideoDecoderVideoModeParams videoModeParams;
    /* video mode params */

    Device_VideoDecoderCreateParams createArgs;
    /* create time arguments */

    Bool enable960H;

} Device_Tw2968Obj;



Int32 Device_tw2968GetChipId ( Device_Tw2968Obj * pObj,
                             Device_VideoDecoderChipIdParams * pPrm,
                             Device_VideoDecoderChipIdStatus * pStatus );

Int32 Device_tw2968GetVideoStatus ( Device_Tw2968Obj * pObj,
                                     VCAP_VIDEO_SOURCE_STATUS_PARAMS_S * pPrm,
                                     VCAP_VIDEO_SOURCE_CH_STATUS_S     * pStatus );

Int32 Device_tw2968Reset ( Device_Tw2968Obj * pObj );

Int32 Device_tw2968SetVideoMode ( Device_Tw2968Obj * pObj,
                                Device_VideoDecoderVideoModeParams * pPrm );

Int32 Device_tw2968Start ( Device_Tw2968Obj * pObj );

Int32 Device_tw2968Stop ( Device_Tw2968Obj * pObj );

Int32 Device_tw2968SetVideoColor ( Device_Tw2968Obj * pObj,
                                 Device_VideoDecoderColorParams * pPrm );

Int32 Device_tw2968RegWrite ( Device_Tw2968Obj * pObj,
                            Device_VideoDecoderRegRdWrParams * pPrm );

Int32 Device_tw2968RegRead ( Device_Tw2968Obj * pObj,
                           Device_VideoDecoderRegRdWrParams * pPrm );


#endif /*  _DEVICE_TW2968_PRIV_H_  */
