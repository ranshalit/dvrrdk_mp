/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include "ti_media_std.h"
#include "ti_vsys_common_def.h"
#include <device.h>
#include <device_videoDecoder.h>
#include <tw2968_priv.h>
#include <osa_i2c.h>


Int32 Device_tw2968SelectPage ( Device_Tw2968Obj * pObj, UInt32 pageNum)
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;

    pCreateArgs = &pObj->createArgs;

    numRegs = 0;

    regAddr[numRegs] = DEVICE_TW2968_REG_PAGE;
    regValue[numRegs] = pageNum & 0x3;
    numRegs++;

    status = OSA_i2cWrite8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], regAddr, regValue, numRegs);

    if ( status < 0 )
        return -1;

    #ifdef DEVICE_TW2968_DEBUG
    printf(" DEVICE: TW2968: Page %d selected (0x%02x=0x%02x) !!!\n",
        pageNum,
        regAddr[0], regValue[0]
    );
    #endif

    return status;
}

Int32 Device_tw2968SetVdelay(Device_Tw2968Obj * pObj,
                              UInt32 chId, UInt32 vdelay )
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs, regBase;


    pCreateArgs = &pObj->createArgs;

    if(chId<(DEVICE_TW2968_CH_PER_DEVICE_MAX/2))
        Device_tw2968SelectPage(pObj, 0);
    else
        Device_tw2968SelectPage(pObj, 1);

    chId = chId % (DEVICE_TW2968_CH_PER_DEVICE_MAX/2);

    regBase = DEVICE_TW2968_REG_BASE_VIN1 + chId*0x10;

    numRegs = 0;

    /* assuming VDELAY HI is always 0 and only setting VDELAY LO */

    regAddr[numRegs] = regBase + DEVICE_TW2968_REG_OFF_VDELAY_LOW;
    regValue[numRegs] = vdelay & 0xFF;
    numRegs++;

    status = OSA_i2cWrite8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], regAddr, regValue, numRegs);

    if ( status < 0 )
        return -1;

    #ifdef DEVICE_TW2968_DEBUG
    printf(" DEVICE: TW2968: CH%d: VDELAY (%d): 0x%02x=0x%02x, 0x%02x=0x%02x!!!\n",
                chId,
                vdelay,
                regAddr[0], regValue[0],
                regAddr[1], regValue[1]
          );
    #endif

    return status;
}

Int32 Device_tw2968SetHactive(Device_Tw2968Obj * pObj,
                              UInt32 chId, UInt32 hactive )
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs, regBase;


    pCreateArgs = &pObj->createArgs;

    if(chId<(DEVICE_TW2968_CH_PER_DEVICE_MAX/2))
        Device_tw2968SelectPage(pObj, 0);
    else
        Device_tw2968SelectPage(pObj, 1);

    chId = chId % (DEVICE_TW2968_CH_PER_DEVICE_MAX/2);

    regBase = DEVICE_TW2968_REG_BASE_VIN1 + chId*0x10;

    numRegs = 0;

    regAddr[numRegs] = regBase + DEVICE_TW2968_REG_OFF_CROP_HACTVIE_HIGH;
    regValue[numRegs] = 0x10 | ((hactive >> 8) & 0x3);
    numRegs++;

    regAddr[numRegs] = regBase + DEVICE_TW2968_REG_OFF_HACTIVE_LOW;
    regValue[numRegs] = hactive & 0xFF;
    numRegs++;

    status = OSA_i2cWrite8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], regAddr, regValue, numRegs);

    if ( status < 0 )
        return -1;

    #ifdef DEVICE_TW2968_DEBUG
    printf(" DEVICE: TW2968: CH%d: HACTIVE (%d): 0x%02x=0x%02x, 0x%02x=0x%02x!!!\n",
                chId,
                hactive,
                regAddr[0], regValue[0],
                regAddr[1], regValue[1]
          );
    #endif

    return status;
}


Int32 Device_tw2968GetChipId ( Device_Tw2968Obj * pObj,
                             Device_VideoDecoderChipIdParams * pPrm,
                             Device_VideoDecoderChipIdStatus * pStatus )
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;

    if ( pPrm == NULL )
        return -1;

    pCreateArgs = &pObj->createArgs;

    Device_tw2968SelectPage(pObj, 0);

    numRegs = 0;

    regAddr[numRegs] = DEVICE_TW2968_REG_DEVICE_ID;
    regValue[numRegs] = 0;
    numRegs++;

    regAddr[numRegs] = DEVICE_TW2968_REG_REV_ID;
    regValue[numRegs] = 0;
    numRegs++;

    status = OSA_i2cRead8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], regAddr, regValue, numRegs);

    if ( status < 0 )
        return -1;

    pStatus->chipId =  (((UInt32) (regValue[0]>>6)&0x3) << 5) | ( (regValue[1] >> 3) & 0x1F ) ;
    pStatus->chipRevision = regValue[1] & 0x7;
    pStatus->firmwareVersion = 0;

    #ifdef DEVICE_TW2968_DEBUG
    printf(" DEVICE: TW2968: Chip ID: 0x%02x=0x%02x, 0x%02x=0x%02x!!!\n",
                regAddr[0], regValue[0],
                regAddr[1], regValue[1]
          );
    #endif

    return status;
}


Int32 Device_tw2968GetVideoStatus ( Device_Tw2968Obj * pObj,
                                     VCAP_VIDEO_SOURCE_STATUS_PARAMS_S * pPrm,
                                     VCAP_VIDEO_SOURCE_CH_STATUS_S     * pStatus )
                                     {
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;
    UInt32 chId, std, regBase;


    if ( pPrm == NULL )
        return -1;

    pCreateArgs = &pObj->createArgs;

    chId = pPrm->channelNum % DEVICE_TW2968_CH_PER_DEVICE_MAX;

    if(chId<(DEVICE_TW2968_CH_PER_DEVICE_MAX/2))
        Device_tw2968SelectPage(pObj, 0);
    else
        Device_tw2968SelectPage(pObj, 1);

    chId = chId % (DEVICE_TW2968_CH_PER_DEVICE_MAX/2);

    regBase = DEVICE_TW2968_REG_BASE_VIN1 + chId*0x10;

    numRegs = 0;

    regAddr[numRegs] = regBase + DEVICE_TW2968_REG_OFF_VID_STATUS;
    regValue[numRegs] = 0;
    numRegs++;

    regAddr[numRegs] = regBase + DEVICE_TW2968_REG_OFF_CHIP_STATUS2;
    regValue[numRegs] = 0;
    numRegs++;

    regAddr[numRegs] = regBase + DEVICE_TW2968_REG_OFF_STD_SEL;
    regValue[numRegs] = 0;
    numRegs++;

    status = OSA_i2cRead8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], regAddr, regValue, numRegs);

    if ( status < 0 )
        return -1;

    #ifdef DEVICE_TW2968_DEBUG
    printf(" DEVICE: TW2968: Video Status: 0x%02x=0x%02x, 0x%02x=0x%02x, 0x%02x=0x%02x !!!\n",
                regAddr[0], regValue[0],
                regAddr[1], regValue[1],
                regAddr[2], regValue[2]
          );
    #endif

    pStatus->isVideoDetect = FALSE;
    pStatus->frameWidth    = 0;
    pStatus->frameHeight   = 0;
    pStatus->frameInterval = 0;
    pStatus->isInterlaced  = 0;

    if((regValue[0] & DEVICE_TW2968_VID_DETECT_MASK)==DEVICE_TW2968_VID_DETECT_MASK)
        pStatus->isVideoDetect = TRUE;

    if ( pStatus->isVideoDetect )
    {
        /*
         * since input to TW2968 is always interlaced
         */
        pStatus->isInterlaced = ( ((regValue[1] >> 3) & 1)==0 ? TRUE : FALSE ) ;

        /*
         * 60Hz, i.e 16.667msec per field
         */
        pStatus->frameInterval = 16667;

        if ( ( regValue[0] & DEVICE_TW2968_VID_50HZ_DETECT_MASK ) == DEVICE_TW2968_VID_50HZ_DETECT_MASK)    /* is 50Hz or 60Hz ? */
        {
            /*
             * 50Hz, i.e 20msec per field
             */
            pStatus->frameInterval = 20000;
        }

        /*
         * frame width is always fixed
         */
        if(pObj->enable960H)
            pStatus->frameWidth = DEVICE_TW2968_NTSC_PAL_WIDTH_960H;
        else
            pStatus->frameWidth = DEVICE_TW2968_NTSC_PAL_WIDTH_720H;

        pStatus->frameHeight = 0;

        std = ( (regValue[2] >> 4) & 0x7 );   /* video standard */

        if ( std == DEVICE_TW2968_VID_STD_PAL_BDGHIN  /* PAL (B,D,G,H,I,N) */
             || std == DEVICE_TW2968_VID_STD_PAL_M    /* PAL (M) */
             || std == DEVICE_TW2968_VID_STD_PAL_COMB_N   /* PAL (Combination-N) */
             || std == DEVICE_TW2968_VID_STD_PAL_60   /* PAL 60  */
             )
        {
            /*
             * PAL standard
             */
            pStatus->frameHeight = DEVICE_TW2968_PAL_HEIGHT;
        }
        if ( std == DEVICE_TW2968_VID_STD_NTSC_MJ /* NTSC (M,J) */
             || std == DEVICE_TW2968_VID_STD_NTSC_4_43    /* NTSC 4.43 */
             )
        {
            /*
             * NTSC standard
             */
            pStatus->frameHeight = DEVICE_TW2968_NTSC_HEIGHT;
        }
    }
    return status;
}


Int32 Device_tw2968Reset ( Device_Tw2968Obj * pObj )
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;

    pCreateArgs = &pObj->createArgs;

    Device_tw2968SelectPage(pObj, 0);

    numRegs = 0;

    regAddr[numRegs]  = DEVICE_TW2968_REG_SOFT_RESET;
    regValue[numRegs] = (1<<7) | (0x3F);
    numRegs++;

    status = OSA_i2cWrite8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], regAddr, regValue, numRegs);

    if ( status < 0 )
        return -1;

    Device_tw2968SelectPage(pObj, 1);

    numRegs = 0;

    regAddr[numRegs]  = DEVICE_TW2968_REG_SOFT_RESET;
    regValue[numRegs] = (0xF);
    numRegs++;

    status = OSA_i2cWrite8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], regAddr, regValue, numRegs);

    if ( status < 0 )
        return -1;

    OSA_waitMsecs(100);

    #ifdef DEVICE_TW2968_DEBUG
    printf(" DEVICE: TW2968: Soft Reset Done (0x%02x=0x%02x) !!!\n",
        regAddr[0], regValue[0]
        );
    #endif

    return status;
}


Int32 Device_tw2968SetVideoMode ( Device_Tw2968Obj * pObj,
                                Device_VideoDecoderVideoModeParams * pPrm )
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;
    UInt8 regAddr[32];
    UInt8 regValue[32];
    UInt8 numRegs;
    UInt32 hactive, chId, vdelay;

    if ( pPrm == NULL )
        return -1;

    pCreateArgs = &pObj->createArgs;

    Device_tw2968SelectPage(pObj, 0);

    numRegs = 0;

    regAddr[numRegs]  = DEVICE_TW2968_REG_CHANNEL_ID_56;
    regValue[numRegs] = (0) | (1<<4);
    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_CHANNEL_ID_78;
    regValue[numRegs] = (2) | (3<<4);
    numRegs++;

    /* The Netra 960H DVR uses video ports VD2 and VD3. */
    regAddr[numRegs]  = DEVICE_TW2968_REG_VID_BUS_TRI_STATE_CTRL;
    if(pObj->enable960H)
        regValue[numRegs] = 0xF0 | 0x9;
    else
        regValue[numRegs] = 0x00 | 0x9;

    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_BGCTL_MISC_CTL2;
    regValue[numRegs] = 0xE0 | (1<<2) | (1<<1); /* enable blue background on video loss */
    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_NOVID;
    regValue[numRegs] = 0x42 | (3<<4); /* embed CHID in EAV/SAV as well as H-blank */
    numRegs++;


    regAddr[numRegs]  = DEVICE_TW2968_REG_CLK_OUT_DELAY;
    regValue[numRegs] = (3) | (3<<4);   /* This value depends on boards. Intersil 
                                           TW2968 EVM uses 0x33 for this register. 
                                           The same value works on VVDN board. */
    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_960HEN;
    if(pObj->enable960H)
        regValue[numRegs] = 0xFF;
    else
        regValue[numRegs] = 0x00;
    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_O36M;
    if(pObj->enable960H)
        regValue[numRegs] = 0xFF;
    else
        regValue[numRegs] = 0x00;
    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_VID_OUT_MODE;
    regValue[numRegs] = (2<<6) | (2<<4) | (2<<2) | (2<<0) ; /* 4CH pixel mux mode */
    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_VID_MISC_CTRL;
    if(pObj->enable960H)
        regValue[numRegs] = 0x40 | 0x3; /* output 36Mhz clock */
    else
        regValue[numRegs] = 0x40 | 0x0; /* output 27Mhz clock */
    numRegs++;

    /* For the Netra 960H DVR, video port VD2 uses the CLKPO output and video port VD3 uses the CLKNO output.
        The polarity of the CLKNO output needs to be inverted so that it has the same polarity as the CLKPO output.
    */
    regAddr[numRegs]  = DEVICE_TW2968_REG_CLK_POL_CTRL;
    regValue[numRegs] = 0x0F | (1<<7) | (0<<6) ;
    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_VID_CH_SEL_VD2_12;
    regValue[numRegs] = (0) | (1 << 4);
    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_VID_CH_SEL_VD2_34;
    regValue[numRegs] = (2) | (3 << 4);
    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_VID_CH_SEL_VD3_12;
    regValue[numRegs] = (4) | (5 << 4);
    numRegs++;

    regAddr[numRegs]  = DEVICE_TW2968_REG_VID_CH_SEL_VD3_34;
    regValue[numRegs] = (6) | (7 << 4);
    numRegs++;

    status = OSA_i2cWrite8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], regAddr, regValue, numRegs);

    if ( status < 0 )
        return -1;

    if(pObj->enable960H)
    {
        hactive = DEVICE_TW2968_NTSC_PAL_WIDTH_960H;
        vdelay  = 0x18; /* modified for correct vertical alignment */
    }
    else
    {
        hactive = DEVICE_TW2968_NTSC_PAL_WIDTH_720H;
        vdelay  = 0x12; /* default */
    }



    for(chId=0; chId<DEVICE_TW2968_CH_PER_DEVICE_MAX; chId++)
    {
        Device_tw2968SetHactive(pObj, chId, hactive);
        Device_tw2968SetVdelay(pObj, chId, vdelay);
    }

    #ifdef DEVICE_TW2968_DEBUG
    printf(" DEVICE: TW2968: Set Video Mode Done !!!\n");
    #endif

    return status;
}


Int32 Device_tw2968Start ( Device_Tw2968Obj * pObj )
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;

    pCreateArgs = &pObj->createArgs;

    Device_tw2968SelectPage(pObj, 0);

    numRegs = 0;

    regAddr[numRegs]  = DEVICE_TW2968_REG_OUT_ENABLE;
    regValue[numRegs] = (1<<6) | (2<<2) | (2<<0); /* enable outputs and output 4x clock for 4x pixel mux mode */
    numRegs++;

    status = OSA_i2cWrite8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], regAddr, regValue, numRegs);

    if ( status < 0 )
        return -1;

    #ifdef DEVICE_TW2968_DEBUG
    printf(" DEVICE: TW2968: Start Done (0x%02x=0x%02x) !!!\n",
        regAddr[0], regValue[0]
        );
    #endif

    return status;
}


Int32 Device_tw2968Stop ( Device_Tw2968Obj * pObj )
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;

    pCreateArgs = &pObj->createArgs;

    Device_tw2968SelectPage(pObj, 0);

    numRegs = 0;

    regAddr[numRegs]  = DEVICE_TW2968_REG_OUT_ENABLE;
    regValue[numRegs] = 0x30;
    numRegs++;

    status = OSA_i2cWrite8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], regAddr, regValue, numRegs);

    if ( status < 0 )
        return -1;

    #ifdef DEVICE_TW2968_DEBUG
    printf(" DEVICE: TW2968: Stop Done (0x%02x=0x%02x) !!!\n",
        regAddr[0], regValue[0]
        );
    #endif

    return status;
}


Int32 Device_tw2968SetVideoColor ( Device_Tw2968Obj * pObj,
                                 Device_VideoDecoderColorParams * pPrm )
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;

    if ( pPrm == NULL )
        return -1;

    pCreateArgs = &pObj->createArgs;


    return status;
}


/* write to I2C registers */
Int32 Device_tw2968RegWrite ( Device_Tw2968Obj * pObj,
                            Device_VideoDecoderRegRdWrParams * pPrm )
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;

    if ( pPrm == NULL )
        return -1;

    pCreateArgs = &pObj->createArgs;

    status = OSA_i2cWrite8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], pPrm->regAddr, pPrm->regValue8, pPrm->numRegs );

    return status;
}

/* read from I2C registers */
Int32 Device_tw2968RegRead ( Device_Tw2968Obj * pObj,
                           Device_VideoDecoderRegRdWrParams * pPrm )
{
    Int32 status = 0;
    Device_VideoDecoderCreateParams *pCreateArgs;

    if ( pPrm == NULL )
        return -1;

    pCreateArgs = &pObj->createArgs;

    status = OSA_i2cRead8 (&pObj->i2cHandle, pCreateArgs->deviceI2cAddr[0], pPrm->regAddr, pPrm->regValue8, pPrm->numRegs);

    return status;
}


