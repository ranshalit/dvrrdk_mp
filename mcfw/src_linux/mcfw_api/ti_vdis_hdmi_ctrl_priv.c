/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/


#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ti81xxhdmi.h>
#include <linux/edid.h>

#include <osa_thr.h>
#include <osa_mbx.h>
#include <ti_vsys_common_def.h>
#include "ti_vsys_priv.h"
#include "ti_vdis_hdmi_ctrl_priv.h"
#include "ti_vsys_priv.h"

/* TO DO
    1. Check for VESA resolution in case of HDMI
    2. Use an array instead of resolution1 in VDIS_SCREEN_INFO_S
*/
/* =============================================================================
 * Local defines
 * =============================================================================
 */
/* If set to 1, debug messages would be printed. */
#define HDMI_CTRL_DEBUG_IS_ENABLE       (0)

/*
 * Next Action the control thread should perform
 */
#define NO_ACTION_REQUIRED              (0)

#define STOP_HDMI_TRANSMITTER           (1)
#define START_HDMI_TRANSMITTER          (2)

#define READ_PARSED_EDID                (8)
#define APPLY_PARSED_EDID               (16)
#define CORRUPT_EDID                    (32)
#define EDID_READ_IN_PROGRESS           (64)

#define EXIT_CONTROL_THREAD             (0xFF)
#define CONTINUE_CONTROL_THREAD         (0x11)

/* Standard Error string */
#define HDMI_CTRL_ERROR             " [host] HDMI Ctrl : ERROR :"
#define HDMI_CTRL_WARNING           " [host] HDMI Ctrl : Warning :"
#define HDMI_CTRL_STATUS            " [host] HDMI Ctrl :"
#define HDMI_CTRL_DEBUG             " [host] HDMI Debug :"

/* Standard VIC code for different resolution as per CEA861 D */
#define HDMI_CEA_1080P60    (16)
#define HDMI_CEA_1080P50    (31)
#define HDMI_CEA_720P60     (19)

/* =============================================================================
 * Local Datatypes
 * =============================================================================
 */
/*
 * HDMI Controller configurations / status of connected sink
 */
typedef struct _VDIS_HDMI_CONTROL {

    int exitCtrlthread;
    OSA_ThrHndl ctrlThread;

    Bool    isHdmi;
    Bool    isSinkConnected;
    /* A sink might not be connected for HDMI to start streaming */
    Bool    isStreaming;
    int     edidAction;
    int     validEdid;
    char edid[HDMI_EDID_MAX_LENGTH];

    int     hdmiFd;


    VSYS_VIDEO_STANDARD_E   vidCurrRes;
    int     audioCurrSampleRate;

    struct ti81xxhdmi_sink_edid_parsed p_edid;
    VSYS_VIDEO_STANDARD_E pref1;
    VSYS_VIDEO_STANDARD_E pref2;
    VSYS_VIDEO_STANDARD_E pref3;

    Bool    lPcmSupport;
    UInt32  sampleRates;
    UInt32  width;
    UInt32  numChan;

    OSA_MbxHndl ctrlMbx;
    OSA_MbxHndl ctrlStart;
    OSA_MbxHndl ctrlStop;

}VDIS_HDMI_CNTRL_INST;

/* =============================================================================
 * Globals
 * =============================================================================
 */

/*
 * HDMI device node
 */
static char gHdmiDevName[30] = {"/dev/TI81XX_HDMI"};

/*
 * HDMI Transmitter state and other configurations
 */
VDIS_HDMI_CNTRL_INST gCtrlInst = {EXIT_CONTROL_THREAD};

/* =============================================================================
 * Vdis internal APIs prototype
 * =============================================================================
 */
static int Vdis_readParsedEdid(VDIS_HDMI_CNTRL_INST *pCtrlInst);
static int Vdis_getSupportedCeaResolutions(VDIS_HDMI_CNTRL_INST *pCtrlInst);
static int Vdis_getSupportedLPcmAudioCfg(VDIS_HDMI_CNTRL_INST *pCtrlInst);
static int Vdis_getSupportedVesaResolutions(VDIS_HDMI_CNTRL_INST *pCtrlInst);
static void * Vdis_hdmiControlThread(void * ctrlInst);

/* =============================================================================
 * Vdis module APIs
 * =============================================================================
 */
/*
 * Initialize the state and configurations
 */
Int32 Vdis_hdmiCtrl_init(UInt32 currResolution)
{
    int status = OSA_SOK;

    gCtrlInst.audioCurrSampleRate = -1;
    gCtrlInst.validEdid = FALSE;
    gCtrlInst.edidAction = NO_ACTION_REQUIRED;
    gCtrlInst.isHdmi = FALSE;
    gCtrlInst.isSinkConnected = DEMO_HDMI_CTRL_NO_SINK;
    gCtrlInst.isStreaming = DEMO_HDMI_CTRL_NOT_STREAMING;
    gCtrlInst.exitCtrlthread = EXIT_CONTROL_THREAD;

    gCtrlInst.pref1 = VSYS_STD_MAX;
    gCtrlInst.pref2 = VSYS_STD_MAX;
    gCtrlInst.pref3 = VSYS_STD_MAX;
    gCtrlInst.lPcmSupport = FALSE;
    gCtrlInst.sampleRates = 0;
    gCtrlInst.width = 0;
    gCtrlInst.numChan = 0;
    gCtrlInst.vidCurrRes = currResolution;

    while (status == OSA_SOK)
    {
        printf(HDMI_CTRL_STATUS "Initializing\n");
        status = OSA_mbxCreate(&gCtrlInst.ctrlMbx);
        if (status != OSA_SOK)
        {
            printf(HDMI_CTRL_ERROR "Could not create HDMI control thread mail box\n");
            break;
        }
        status = OSA_mbxCreate(&gCtrlInst.ctrlStart);
        if (status != OSA_SOK)
        {
            OSA_mbxDelete(&gCtrlInst.ctrlMbx);
            printf(HDMI_CTRL_ERROR "Could not create HDMI control start mail box\n");
            break;
        }
        status = OSA_mbxCreate(&gCtrlInst.ctrlStop);
        if (status != OSA_SOK)
        {
            OSA_mbxDelete(&gCtrlInst.ctrlStart);
            OSA_mbxDelete(&gCtrlInst.ctrlMbx);
            printf(HDMI_CTRL_ERROR "Could not create HDMI control stop mail box\n");
            break;
        }

        /* Create the handler thread */
        gCtrlInst.exitCtrlthread = CONTINUE_CONTROL_THREAD;
        memset(&gCtrlInst.ctrlThread, 0, sizeof(gCtrlInst.ctrlThread));

        status = OSA_thrCreate(&gCtrlInst.ctrlThread,
                      Vdis_hdmiControlThread,
                      OSA_THR_PRI_DEFAULT, OSA_THR_STACK_SIZE_DEFAULT,
                      (void *) &gCtrlInst);
        if (status != OSA_SOK)
        {
            OSA_mbxDelete(&gCtrlInst.ctrlStop);
            OSA_mbxDelete(&gCtrlInst.ctrlStart);
            OSA_mbxDelete(&gCtrlInst.ctrlMbx);
            printf(HDMI_CTRL_ERROR "Could not create handler thread - Exiting\n");
        }
        break;
    }

    return status;
}

/*
 * De Initializes the HDMI control thread.
 *      Initializes the exit procedure for the control thread.
 */
Int32 Vdis_hdmiCtrl_deInit(void)
{
    if (gCtrlInst.exitCtrlthread != EXIT_CONTROL_THREAD)
    {
        gCtrlInst.exitCtrlthread = EXIT_CONTROL_THREAD;
    }

    return 0;
}

/*
 * Stop HDMI transmitter.
 *  Requests the control thread to stop HDMI transmitter. This is a blocking
 *  call and could potentially take upto 2 X DEMO_HDMI_CTRL_HPD_POLL_INTERVAL
 *  time period.
 */
Int32 Vdis_hdmiCtrl_stop(void)
{
    int status = OSA_EFAIL;
    if (gCtrlInst.exitCtrlthread == CONTINUE_CONTROL_THREAD)
    {
        status = OSA_mbxSendMsg(&gCtrlInst.ctrlMbx, &gCtrlInst.ctrlStop,
                                STOP_HDMI_TRANSMITTER, NULL, OSA_MBX_WAIT_ACK);
        if (status != OSA_SOK)
        {
            printf(HDMI_CTRL_ERROR "Could not send STOP command\n");
        }
        else
        {
            gCtrlInst.vidCurrRes = VSYS_STD_MAX;
#if (HDMI_CTRL_DEBUG_IS_ENABLE == 1)
            printf(HDMI_CTRL_DEBUG "Stopped \n");
#endif /* HDMI_CTRL_DEBUG_IS_ENABLE */
        }
    }
    return status;
}


/*
 * Start HDMI transmitter.
 *  Requests the control thread to start HDMI transmitter. This is a blocking
 *  call and could potentially take upto 2 X DEMO_HDMI_CTRL_HPD_POLL_INTERVAL
 *  time period.
 */
Int32 Vdis_hdmiCtrl_start(UInt32 currResolution)
{
    int status = OSA_EFAIL;
    if (gCtrlInst.exitCtrlthread == CONTINUE_CONTROL_THREAD)
    {
        status = OSA_mbxSendMsg(&gCtrlInst.ctrlMbx, &gCtrlInst.ctrlStart,
                                START_HDMI_TRANSMITTER, NULL, OSA_MBX_WAIT_ACK);
        if (status != OSA_SOK)
        {
            printf(HDMI_CTRL_ERROR "Could not send START command\n");
        }
        else
        {
            gCtrlInst.vidCurrRes = currResolution;
#if (HDMI_CTRL_DEBUG_IS_ENABLE == 1)
            printf(HDMI_CTRL_DEBUG "Started \n");
            printf(HDMI_CTRL_DEBUG "Current resolution is %d\n", currResolution);
#endif /* HDMI_CTRL_DEBUG_IS_ENABLE */
        }
    }
    return status;
}


Int32 Vdis_getScreenInfo(VDIS_DEV vdDevId,
                         VDIS_SCREEN_INFO_S *scrInfo)
{
    Int32 status = OSA_EFAIL;
    while (((gCtrlInst.exitCtrlthread == CONTINUE_CONTROL_THREAD) &&
            (vdDevId == VDIS_DEV_HDMI)) &&
           (scrInfo != NULL))
    {
        scrInfo->isSinkConnected = FALSE;
        scrInfo->isStreaming = FALSE;

        if (gCtrlInst.isSinkConnected == DEMO_HDMI_CTRL_NO_SINK)
        {
            status = OSA_SOK;
            break;
        }

        scrInfo->isSinkConnected = TRUE;
        if (gCtrlInst.isStreaming == DEMO_HDMI_CTRL_STREAMING)
        {
            scrInfo->isStreaming = TRUE;
        }
        scrInfo->currentResolution = gCtrlInst.vidCurrRes;

        if (gCtrlInst.validEdid == TRUE)
        {
            /* Do not worry about sink being disconnected at this point.
               as an EVENT will also be generated and hence apps will know the
               correct status */
            if (gCtrlInst.isHdmi == TRUE)
            {
                scrInfo->isHdmi = TRUE;
                scrInfo->resolution1 = gCtrlInst.pref1;
                scrInfo->resolution2 = gCtrlInst.pref2;
                scrInfo->resolution3 = gCtrlInst.pref3;

                scrInfo->supportLPCM = gCtrlInst.lPcmSupport;
                scrInfo->numChannels = gCtrlInst.numChan;
                scrInfo->widthOfSamples = gCtrlInst.width;
                scrInfo->samplingFrequency = gCtrlInst.sampleRates;
            }
            else
            {
                scrInfo->isHdmi = FALSE;
                scrInfo->resolution1 = gCtrlInst.pref1;
                scrInfo->resolution2 = gCtrlInst.pref2;

                scrInfo->supportLPCM = FALSE;
            }
            status = OSA_SOK;
        }

        break;
    }
    return status;
}

Int32 Vdis_printScreenDetailedTimings(VDIS_DEV vdDevId)
{
    UInt32 i;

    Int32 status = OSA_EFAIL;
    while ((gCtrlInst.exitCtrlthread == CONTINUE_CONTROL_THREAD) &&
           (vdDevId == VDIS_DEV_HDMI))
    {
        if (gCtrlInst.isSinkConnected == DEMO_HDMI_CTRL_NO_SINK)
        {
            printf(HDMI_CTRL_STATUS "No Sink detected !!!\n");
            status = OSA_SOK;
            break;
        }

        if (gCtrlInst.validEdid == TRUE)
        {
            if (gCtrlInst.p_edid.num_dt <= 0)
            {
                printf(HDMI_CTRL_STATUS "Detailed Timings Info not found !!!\n");
                break;
            }

            printf("| X-Res         | Y-Res         | H Pulse Width | H Front Porch | H Back Porch  | V Pulse Width | V Front Porch | V Back Porch  | Pixel Clock   |\n");
            for (i = 0; i < gCtrlInst.p_edid.num_dt; i++)
            {
                printf("| %-14d| %-14d| %-14d| %-14d| %-14d| %-14d| %-14d| %-14d| %-14d|\n",
                    gCtrlInst.p_edid.detailed_timings[i].x_res,
                    gCtrlInst.p_edid.detailed_timings[i].y_res,
                    gCtrlInst.p_edid.detailed_timings[i].hsw,
                    gCtrlInst.p_edid.detailed_timings[i].hfp,
                    gCtrlInst.p_edid.detailed_timings[i].hbp,
                    gCtrlInst.p_edid.detailed_timings[i].vsw,
                    gCtrlInst.p_edid.detailed_timings[i].vfp,
                    gCtrlInst.p_edid.detailed_timings[i].vbp,
                    gCtrlInst.p_edid.detailed_timings[i].pixel_clock);
            }
        }
        break;
    }
    return status;
}


/*
 * Debug Interface - Prints out the detected sink details.
 */
void Vdis_hdmiCtrl_printDetectedDetails(void)
{
    int i;
    VSYS_VIDEO_STANDARD_E res;
    if (gCtrlInst.isSinkConnected == DEMO_HDMI_CTRL_NO_SINK)
    {
        printf(HDMI_CTRL_STATUS "No Sink detected \n");
    }
    else
    {
        printf(HDMI_CTRL_STATUS "Current Resolution is %d\n", gCtrlInst.vidCurrRes);
        if (gCtrlInst.isStreaming == DEMO_HDMI_CTRL_STREAMING)
            printf(HDMI_CTRL_STATUS "Sink detected & streaming \n");
        else
            printf(HDMI_CTRL_STATUS "Sink detected & NOT streaming \n");

        if (gCtrlInst.validEdid == TRUE)
        {
            if (gCtrlInst.p_edid.is_hdmi_supported != 0)
            {
                printf(HDMI_CTRL_STATUS "HDMI Interface\n");
                for (i = 0; i < 3; i++)
                {
                    if (i == 0)
                        res = gCtrlInst.pref1;
                    else if (i == 1)
                        res = gCtrlInst.pref2;
                    else
                        res = gCtrlInst.pref3;

                    if (res == VSYS_STD_1080P_60)
                        printf(HDMI_CTRL_STATUS "Supports 1080P60 Resolution\n");
                    if (res == VSYS_STD_1080P_50)
                        printf(HDMI_CTRL_STATUS "Supports 1080P50 Resolution\n");
                    if (res == VSYS_STD_720P_60)
                        printf(HDMI_CTRL_STATUS "Supports 720P60 Resolution\n");
                }

                i = 0;
                if (gCtrlInst.lPcmSupport == TRUE)
                {
                    printf(HDMI_CTRL_STATUS "LPCM supported for following sample rates\n");
                    if ((gCtrlInst.sampleRates & DEMO_HDMI_32KHZ_SAMPLE_RATE) != 0)
                    {
                        printf("%d - 32 KHZ\n", i);
                        i++;
                    }
                    if ((gCtrlInst.sampleRates & DEMO_HDMI_44_1KHZ_SAMPLE_RATE) != 0)
                    {
                        printf("%d - 44.1 KHZ\n", i);
                        i++;
                    }
                    if ((gCtrlInst.sampleRates & DEMO_HDMI_48KHZ_SAMPLE_RATE) != 0)
                    {
                        printf("%d - 48 KHZ\n", i);
                        i++;
                    }
                    if ((gCtrlInst.sampleRates & DEMO_HDMI_96KHZ_SAMPLE_RATE) != 0)
                    {
                        printf("%d - 96 KHZ\n", i);
                        i++;
                    }
                    if ((gCtrlInst.sampleRates & DEMO_HDMI_192KHZ_SAMPLE_RATE) != 0)
                    {
                        printf("%d - 192 KHZ\n", i);
                        i++;
                    }
                }
                else
                    printf(HDMI_CTRL_STATUS "LPCM is not supported\n");


            }
            else
            {
                printf(HDMI_CTRL_STATUS "DVI Interface\n");
                if (gCtrlInst.pref1 == VSYS_STD_XGA_60)
                    printf(HDMI_CTRL_STATUS "Supports XGA Resolution\n");
                if (gCtrlInst.pref2 == VSYS_STD_SXGA_60)
                    printf(HDMI_CTRL_STATUS "Supports SXGA Resolution\n");
                printf(HDMI_CTRL_STATUS "No Audio supported\n");
            }
        }
        else
            printf(HDMI_CTRL_STATUS "EDID not yet parsed \n");
    }
}

/* Reads the EDID that was parsed by the driver */
static int Vdis_readParsedEdid(VDIS_HDMI_CNTRL_INST *pCtrlInst)
{
    int status;

    /* Driver ensures no re-reads. If EDID has been read once, its just copied
        */
    status = ioctl(pCtrlInst->hdmiFd, TI81XXHDMI_READ_EDID, &pCtrlInst->edid);
    if (status < 0)
    {
        printf(HDMI_CTRL_WARNING "Could not get SINKS EDID \n");
        return status;
    }

    status = ioctl(pCtrlInst->hdmiFd, TI81XXHDMI_GET_PARSED_EDID_INFO,
                    &pCtrlInst->p_edid);
    if (status < 0)
    {
        printf(HDMI_CTRL_WARNING "Could not get SINKS Parsed EDID \n");
        return status;
    }
    return 0;
}

/* Determines if supported HDMI resolutions are supported by the connectedsink*/
static int Vdis_getSupportedCeaResolutions( VDIS_HDMI_CNTRL_INST *pCtrlInst)
{
    struct image_format *pCea = &pCtrlInst->p_edid.supported_cea_vic;
    int i, j, VSYS_STD;

    j = 0;
    VSYS_STD = 0;
    pCtrlInst->pref1 = VSYS_STD_MAX;
    pCtrlInst->pref2 = VSYS_STD_MAX;
    pCtrlInst->pref3 = VSYS_STD_MAX;

    /* We are interstead in 3 CEA resolutions, hence limiting j to 3 */
    for (i = 0; (i < pCea->length && j < 3); i++)
    {
        VSYS_STD = VSYS_STD_MAX;
        if (pCea->fmt[i].code == HDMI_CEA_1080P60)
        {
            VSYS_STD = VSYS_STD_1080P_60;
        }
        else if (pCea->fmt[i].code == HDMI_CEA_1080P50)
        {
            VSYS_STD = VSYS_STD_1080P_50;
        }
        else if (pCea->fmt[i].code == HDMI_CEA_720P60)
        {
            VSYS_STD = VSYS_STD_720P_60;
        }
        else
        {
            /* look for next resolution */
        }

        if ((j == 0) && (VSYS_STD != VSYS_STD_MAX))
        {
            pCtrlInst->pref1 = VSYS_STD;
            j++;
        }
        else if ((j == 1) && (VSYS_STD != VSYS_STD_MAX))
        {
            pCtrlInst->pref2 = VSYS_STD;
            j++;
        }
        else if ((j == 2) && (VSYS_STD != VSYS_STD_MAX))
        {
            pCtrlInst->pref3 = VSYS_STD;
            j++;
        }
    }
    return 0;
}

/* Determines if LPCM stereo audio is supported by the connected sink */
static int Vdis_getSupportedLPcmAudioCfg(VDIS_HDMI_CNTRL_INST *pCtrlInst)
{
    int i, freq, supportFreq;
    pCtrlInst->lPcmSupport = FALSE;
    pCtrlInst->sampleRates = 0;

    supportFreq = DEMO_HDMI_32KHZ_SAMPLE_RATE | DEMO_HDMI_44_1KHZ_SAMPLE_RATE;
    supportFreq |= DEMO_HDMI_48KHZ_SAMPLE_RATE | DEMO_HDMI_96KHZ_SAMPLE_RATE;

    for (i = 0; i < pCtrlInst->p_edid.audio_support.length; i++)
    {
        pCtrlInst->width =
                pCtrlInst->p_edid.audio_support.fmt[i].width;
        pCtrlInst->numChan =
                pCtrlInst->p_edid.audio_support.fmt[i].num_of_ch;
        /* We support LPCM 2 channels only with 16 bits / channel */
        if ((pCtrlInst->p_edid.audio_support.fmt[i].format == 1) &&
            (pCtrlInst->p_edid.audio_support.fmt[i].num_of_ch == 2))
        {
            freq = pCtrlInst->p_edid.audio_support.fmt[i].freq;
            if (((freq & supportFreq) != 0) &&
                ((pCtrlInst->p_edid.audio_support.fmt[i].width & 0x1) != 0))
            {
                pCtrlInst->sampleRates = freq;
                pCtrlInst->lPcmSupport = TRUE;
            }
        }
    }
    return 0;
}

/* Determines if supported VESA resolutions is supported by the connected sink*/
static int Vdis_getSupportedVesaResolutions(VDIS_HDMI_CNTRL_INST *pCtrlInst)
{
    int i, l, h, ratio, fps, mul, div;

    pCtrlInst->pref1 = VSYS_STD_MAX;
    pCtrlInst->pref2 = VSYS_STD_MAX;
    pCtrlInst->pref3 = VSYS_STD_MAX;

    /*  If third bit of byte 2 of estabilished timings is set, then XGA is
        supported */
    if ((pCtrlInst->p_edid.established_timings[1] & 0x8) != 0)
    {
        pCtrlInst->pref1 = VSYS_STD_XGA_60;
    }

    /* Refer VESA standard timings */
    for (i = 0; i < 8; i += 2)
    {
        l = (pCtrlInst->p_edid.standard_timings[i] + 31) * 8;

        fps = pCtrlInst->p_edid.standard_timings[i + 1] & 0x3F;

        ratio = (pCtrlInst->p_edid.standard_timings[i + 1] & 0x3F) >> 6;
        if (ratio == 0){
            mul = 10;
            div = 16;
        } else if (ratio == 1) {
            mul = 3;
            div = 4;
        } else if (ratio == 2) {
            mul = 4;
            div = 5;
        } else if (ratio == 3) {
            mul = 9;
            div = 16;
        } else {
            printf(HDMI_CTRL_ERROR "DVI : EDID : Unable to ");
            printf("determine multiplication factor \n");
            mul = 1;
            break;
        }

        h = (l * mul)/div;

        /* Check if SXGA is supported */
        if ((l == 1280) && (h = 1024) && (fps == 00))
        {
            pCtrlInst->pref2 = VSYS_STD_SXGA_60;
        }
    }

    if (pCtrlInst->pref2 == VSYS_STD_SXGA_60)
    {
        pCtrlInst->pref2 = pCtrlInst->pref1;
        pCtrlInst->pref1 = VSYS_STD_SXGA_60;
    }

    return 0;
}

/* Main Control thread */
static void *Vdis_hdmiControlThread(void * ctrlInst)
{
    VDIS_HDMI_CNTRL_INST *pCtrlInst = (VDIS_HDMI_CNTRL_INST *) ctrlInst;
    int status = OSA_SOK;
    struct ti81xxhdmi_status hdmiStatus;
    OSA_MsgHndl *pMsg;
    int sendEvent;
    char                  threadName[128];

    snprintf(threadName,sizeof(threadName),"%s_%x",__func__,0);
    threadName[sizeof(threadName) - 1] = 0;
    OSA_printTID(threadName);

    hdmiStatus.is_hpd_detected = 0x0;
    hdmiStatus.is_hdmi_streaming = 0x0;
    if (pCtrlInst == NULL)
    {
        printf(HDMI_CTRL_ERROR "Illegal arguments\n");
        return pCtrlInst;
    }

    /* Initialization */
    pCtrlInst->hdmiFd = open(gHdmiDevName, O_RDWR);
    if (pCtrlInst->hdmiFd < 0)
    {
        printf(HDMI_CTRL_ERROR "Could not open HDMI driver\n");
        pCtrlInst->exitCtrlthread = EXIT_CONTROL_THREAD;
        return NULL;
    }

    if ((status == OSA_SOK) &&
           (pCtrlInst->exitCtrlthread == CONTINUE_CONTROL_THREAD))
    {
        printf(HDMI_CTRL_STATUS "Initialized\n");
    }

    sendEvent = 0;

    /* Main Loop */
    while ((status == OSA_SOK) &&
           (pCtrlInst->exitCtrlthread == CONTINUE_CONTROL_THREAD))
    {

        /* Get the connection status */
        status = ioctl(pCtrlInst->hdmiFd, TI81XXHDMI_GET_STATUS, &hdmiStatus);
        if (status != 0)
        {
            printf(HDMI_CTRL_ERROR "Could not get sink connection status\n");
            printf(HDMI_CTRL_ERROR "Terminating HDMI control demo.\n");
            status = -1;
            pCtrlInst->exitCtrlthread = EXIT_CONTROL_THREAD;
            break;
        }

        if (hdmiStatus.is_hpd_detected != 0x0)
        {
            /* Check if this was due to re-connect */
            if (pCtrlInst->isSinkConnected == DEMO_HDMI_CTRL_NO_SINK)
            {
                /* Check we require to read the EDID */
                if (pCtrlInst->isStreaming == DEMO_HDMI_CTRL_STREAMING)
                {
                    /* Read the EDID, parse */
#if (HDMI_CTRL_DEBUG_IS_ENABLE == 1)
                    printf(HDMI_CTRL_DEBUG "Setting up read EDID 0\n");
#endif /* HDMI_CTRL_DEBUG_IS_ENABLE */

                    pCtrlInst->edidAction = READ_PARSED_EDID;
                }
                pCtrlInst->isSinkConnected = DEMO_HDMI_CTRL_SINK_CONNECTED;
                /* Generate an local event (host) */
                sendEvent = VSYS_EVENT_HDMI_TV_CONNECTED;
                printf(HDMI_CTRL_STATUS "Sink Connected\n");
            }
            else if (pCtrlInst->validEdid == FALSE)
            {
                if (pCtrlInst->isStreaming == DEMO_HDMI_CTRL_STREAMING)
                {
                    /* Condition,
                       Where START was called after Sink was detected
                       Read the EDID, parse */
                    pCtrlInst->edidAction = READ_PARSED_EDID;
#if (HDMI_CTRL_DEBUG_IS_ENABLE == 1)
                    printf(HDMI_CTRL_DEBUG "Setting up read EDID 1\n");
#endif /* HDMI_CTRL_DEBUG_IS_ENABLE */
                }
            }
        }
        else
        {
            if (pCtrlInst->isSinkConnected != DEMO_HDMI_CTRL_NO_SINK)
            {
                sendEvent = VSYS_EVENT_HDMI_TV_DISCONNECTED;
                printf(HDMI_CTRL_STATUS "Sink Disconnected\n");
            }
            pCtrlInst->isSinkConnected = DEMO_HDMI_CTRL_NO_SINK;
            pCtrlInst->validEdid = FALSE;

        }

        /* Check if have to/can read the EDID */
        if (((pCtrlInst->isStreaming == DEMO_HDMI_CTRL_STREAMING) &&
             (pCtrlInst->edidAction == READ_PARSED_EDID)) &&
            (pCtrlInst->isSinkConnected == DEMO_HDMI_CTRL_SINK_CONNECTED))
        {
            pCtrlInst->validEdid = EDID_READ_IN_PROGRESS;
            pCtrlInst->isHdmi = FALSE;
            /* Read EDID here and update the edid flag */
            status = Vdis_readParsedEdid(pCtrlInst);
            if (status != 0)
            {
                pCtrlInst->validEdid = CORRUPT_EDID;
                printf(HDMI_CTRL_WARNING "Could not get parsed EDID\n");
                /* Do not bother continue displaying */
                status = OSA_SOK;
            }
            else
            {
#if (HDMI_CTRL_DEBUG_IS_ENABLE == 1)
                printf(HDMI_CTRL_DEBUG "EVENT TV Connected\n");
#endif /* HDMI_CTRL_DEBUG_IS_ENABLE */
                /* In cases where we have stopped and TV is connected
                    and re-started. This will result in no change in HPD
                    however, but we would require to let apps know the EDID
                    has been read and apps could use it.
                    */
                sendEvent = VSYS_EVENT_HDMI_TV_CONNECTED;

                /* Check if interface is HDMI / DVI */
                if (pCtrlInst->p_edid.is_hdmi_supported != 0)
                {
                    /* HDMI interface */
                    pCtrlInst->isHdmi = TRUE;
                    Vdis_getSupportedCeaResolutions(pCtrlInst);
                    Vdis_getSupportedLPcmAudioCfg(pCtrlInst);
                }
                else
                {
                    /* DVI */
                    pCtrlInst->isHdmi = FALSE;
                    Vdis_getSupportedVesaResolutions(pCtrlInst);
                }
                pCtrlInst->validEdid = TRUE;
            }
            pCtrlInst->edidAction = NO_ACTION_REQUIRED;
        }

        pMsg = NULL;
        status = OSA_mbxCheckMsg(&pCtrlInst->ctrlMbx, &pMsg);
        if (status == OSA_SOK)
        {
            /* Actions required */
            switch (OSA_msgGetCmd(pMsg))
            {
                case STOP_HDMI_TRANSMITTER:
                    /* Perform stop */
                    status = ioctl(pCtrlInst->hdmiFd, TI81XXHDMI_STOP);
                    if (status != 0)
                    {
                        printf(HDMI_CTRL_ERROR "Could not stop HDMI\n");
                        status = 0;
                    }
                    else
                    {
                        pCtrlInst->isStreaming = DEMO_HDMI_CTRL_NOT_STREAMING;
                    }
                    OSA_mbxAckOrFreeMsg(pMsg, OSA_SOK);
                break;

                case START_HDMI_TRANSMITTER:
                    /* Perform start */
                    status = ioctl(pCtrlInst->hdmiFd, TI81XXHDMI_START);
                    if (status != 0)
                    {
                        printf(HDMI_CTRL_ERROR "Could not start HDMI\n");
                        status = 0;
                    }
                    else
                    {
                        pCtrlInst->isStreaming = DEMO_HDMI_CTRL_STREAMING;
                    }
                    OSA_mbxAckOrFreeMsg(pMsg, OSA_SOK);
                break;

                default :
                    /* Un-Recoganised command, nack */
                    OSA_mbxAckOrFreeMsg(pMsg, OSA_EFAIL);
                    printf(HDMI_CTRL_ERROR "UnRecoganized command received\n");
                break;

            }
        }
        else
        {
            /* No Messages */
            status = OSA_SOK;
        }

        /* Check if apps requires to be notfied */
        if (sendEvent != 0)
        {
            /* Check if there was indeed an event handler, else wait.
                If the event is updated, no issues, as the last event is all
                the application is interstead in */
            if (Vsys_isEventHandlerRegistered () != FALSE)
            {

#if (HDMI_CTRL_DEBUG_IS_ENABLE == 1)
                printf(HDMI_CTRL_DEBUG "Sending Event %d\n", sendEvent);
#endif /* HDMI_CTRL_DEBUG_IS_ENABLE */
                System_linkControl(SYSTEM_LINK_ID_HOST, sendEvent,
                                   NULL, 0, FALSE);
                sendEvent = 0;
            }
        }


        OSA_waitMsecs(DEMO_HDMI_CTRL_HPD_POLL_INTERVAL);

    }

    printf(HDMI_CTRL_STATUS "Exiting control thread...");
    pCtrlInst->exitCtrlthread = EXIT_CONTROL_THREAD;
    status = ioctl(pCtrlInst->hdmiFd, TI81XXHDMI_STOP);
    if (status != 0)
    {
        printf(HDMI_CTRL_ERROR "Could not stop HDMI\n");
        status = 0;
    }
    OSA_mbxDelete(&pCtrlInst->ctrlMbx);
    OSA_mbxDelete(&pCtrlInst->ctrlStart);
    OSA_mbxDelete(&pCtrlInst->ctrlStop);

    close(pCtrlInst->hdmiFd);
    printf("[Done] \n");
    return (NULL);
}
