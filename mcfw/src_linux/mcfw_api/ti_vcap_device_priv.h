#ifndef __TI_VCAP_DEVICE_PRIV_H__
#define __TI_VCAP_DEVICE_PRIV_H__

#include "ti_vcap.h"
#include <device.h>
#include <device_videoDecoder.h>

typedef struct {

    UInt32 vipInstId;
    UInt32 deviceId;
    UInt32 numChInDevice;
    Device_VideoDecoderVideoModeParams modeParams;

} VCAP_DEVICE_CREATE_PARAM_S;


Int32 Vcap_deviceCreate(VCAP_DEVICE_CREATE_PARAM_S *pPrm, UInt32 numDevices);
Int32 Vcap_deviceDelete();
Int32 Vcap_deviceStart();
Int32 Vcap_deviceStop();
Int32 Vcap_deviceSetAudioParams(UInt32 numChannels, UInt32 samplingHz, UInt32 audioVolume);
Int32 Vcap_deviceGetSourceStatus(VCAP_VIDEO_SOURCE_STATUS_S *pPrm);
Int32 Vcap_deviceSetColor(Int32 contrast, Int32 brightness, Int32 saturation, Int32 hue, Int32 chId);
Bool  Vcap_deviceIsPalMode();

#endif

