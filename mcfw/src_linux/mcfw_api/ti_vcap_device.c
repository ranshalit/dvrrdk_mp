/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include "ti_vsys_priv.h"
#include "ti_vcap_device_priv.h"

#include <tvp5158.h>
#include <tw2968.h>

typedef struct {

    Device_Handle               deviceHandle;
    Bool                        isPalMode;
    VCAP_DEVICE_CREATE_PARAM_S  createPrm;

} VCAP_DEVICE_CONTEXT_S;

typedef struct {
    VCAP_DEVICE_CONTEXT_S       devInfo[VCAP_DEV_MAX];
    UInt32                      numDevices;

} VCAP_DEVICE_MODULE_CONTEXT_S;


VCAP_DEVICE_MODULE_CONTEXT_S gVcapDeviceModuleContext;

Int32 Vcap_deviceControl(UInt32 deviceNum, UInt32 cmd, Ptr inArgs, Ptr outArgs)
{
    Int32 status=-1;
    VCAP_DEVICE_CONTEXT_S  *pDevice;

    OSA_assert(deviceNum<gVcapDeviceModuleContext.numDevices);

    pDevice = &gVcapDeviceModuleContext.devInfo[deviceNum];

    if(pDevice->deviceHandle)
    {
        if(pDevice->createPrm.deviceId==DEVICE_VID_DEC_TVP5158_DRV)
        {
                status = Device_tvp5158Control(pDevice->deviceHandle,
                                   cmd,
                                   inArgs,
                                   outArgs);
        }
        if(pDevice->createPrm.deviceId==DEVICE_VID_DEC_TW2968_DRV)
        {
                status = Device_tw2968Control(pDevice->deviceHandle,
                                   cmd,
                                   inArgs,
                                   outArgs);
        }
    }
    return status;
}

Int32 Vcap_deviceCreate(VCAP_DEVICE_CREATE_PARAM_S *pPrm, UInt32 numDevices)
{
    Int32 status = 0;
    Device_VideoDecoderChipIdParams      vidDecChipIdArgs;
    Device_VideoDecoderChipIdStatus      vidDecChipIdStatus;
    VCAP_VIDEO_SOURCE_STATUS_PARAMS_S    videoStatusArgs;
    VCAP_VIDEO_SOURCE_CH_STATUS_S        videoStatus;
    Int32                                devId;
    Device_VideoDecoderCreateParams      createArgs;
    Device_VideoDecoderCreateStatus      createStatusArgs;
    Device_VideoDecoderVideoModeParams   vidDecVideoModeArgs;
    UInt32                               numCh;
    VCAP_DEVICE_CONTEXT_S               *pDevice;

    memset(&gVcapDeviceModuleContext, 0, sizeof(gVcapDeviceModuleContext));

    numCh = 0;

    gVcapDeviceModuleContext.numDevices = numDevices;

    for(devId = 0; devId < numDevices; devId++)
    {
        memset(&createArgs, 0, sizeof(Device_VideoDecoderCreateParams));

        #if defined (TI_814X_BUILD) || defined (TI_8107_BUILD)
        createArgs.deviceI2cInstId    = 2;
        #endif
        #ifdef TI_816X_BUILD
        createArgs.deviceI2cInstId    = 1;
        #endif

        numCh += pPrm[devId].numChInDevice;

        createArgs.numDevicesAtPort   = 1;
        createArgs.deviceI2cAddr[0]   = Device_getVidDecI2cAddr(
                                                         pPrm[devId].deviceId,
                                                         pPrm[devId].vipInstId);

        createArgs.deviceResetGpio[0] = DEVICE_VIDEO_DECODER_GPIO_NONE;

        vidDecChipIdArgs.deviceNum = 0;
        videoStatusArgs.channelNum = 0;

        pDevice = &gVcapDeviceModuleContext.devInfo[devId];

        pDevice->createPrm = pPrm[devId];

        if(pDevice->createPrm.deviceId==DEVICE_VID_DEC_TVP5158_DRV)
        {
            pDevice->deviceHandle = Device_tvp5158Create(
                                                         pDevice->createPrm.deviceId,
                                                         devId, // instId - need to change
                                                         &createArgs,
                                                         &createStatusArgs);

        }

        if(pDevice->createPrm.deviceId==DEVICE_VID_DEC_TW2968_DRV)
        {
            pDevice->deviceHandle = Device_tw2968Create(
                                                         pDevice->createPrm.deviceId,
                                                         devId, // instId - need to change
                                                         &createArgs,
                                                         &createStatusArgs);
        }

        OSA_assert(pDevice->deviceHandle!=NULL);

        status = Vcap_deviceControl(devId,
                                    IOCTL_DEVICE_VIDEO_DECODER_GET_CHIP_ID,
                                    &vidDecChipIdArgs,
                                    &vidDecChipIdStatus);

        if (status >= 0)
        {
            printf(" VCAP: DEVICE-%d (0x%02x): Chip ID 0x%04x, Rev 0x%04x, Firmware 0x%04x !!!\n",
                       devId,
                       createArgs.deviceI2cAddr[0],
                       vidDecChipIdStatus.chipId,
                       vidDecChipIdStatus.chipRevision,
                       vidDecChipIdStatus.firmwareVersion
                  );

            status = Vcap_deviceControl(devId,
                                        IOCTL_DEVICE_VIDEO_DECODER_GET_VIDEO_STATUS,
                                        &videoStatusArgs, &videoStatus);

            if (videoStatus.isVideoDetect)
            {
                printf(" VCAP: DEVICE-%d (0x%02x): Detected video (%dx%d@%dHz, %d) !!!\n",
                           devId,
                           createArgs.deviceI2cAddr[0],
                           videoStatus.frameWidth,
                           videoStatus.frameHeight,
                           1000000 / videoStatus.frameInterval,
                           videoStatus.isInterlaced);

                /* Assumption here is height width of video remains same across channels */
                if (videoStatus.frameHeight == 288)
                    pDevice->isPalMode = TRUE;

            }
            else
            {
                printf(" VCAP: DEVICE-%d (0x%02x):  NO Video Detected !!!\n", devId, createArgs.deviceI2cAddr[0]);
            }
        }
        else
        {
            printf(" VCAP: DEVICE-%d (0x%02x): Device not found !!!\n", devId, createArgs.deviceI2cAddr[0]);
        }
    }

    #ifdef SYSTEM_ENABLE_AUDIO
    Vcap_deviceSetAudioParams(numCh, 16000, 5);
    #endif

    /* Configure video decoder */

    for(devId = 0; devId < numDevices; devId++)
    {
        vidDecVideoModeArgs = pPrm[devId].modeParams;

        status = Vcap_deviceControl(devId,
                                    IOCTL_DEVICE_VIDEO_DECODER_SET_VIDEO_MODE,
                                    &vidDecVideoModeArgs,
                                    NULL);
    }
    return status;
}

Int32 Vcap_deviceDelete()
{
    UInt32 devId;
    Int32 status = 0;
    VCAP_DEVICE_CONTEXT_S  *pDevice;

    for(devId = 0; devId < gVcapDeviceModuleContext.numDevices; devId++)
    {
        pDevice = &gVcapDeviceModuleContext.devInfo[devId];

        if(pDevice->deviceHandle)
        {
            if(pDevice->createPrm.deviceId==DEVICE_VID_DEC_TVP5158_DRV)
            {
                status |= Device_tvp5158Delete(pDevice->deviceHandle,
                                   NULL);
            }

            if(pDevice->createPrm.deviceId==DEVICE_VID_DEC_TW2968_DRV)
            {
                status |= Device_tw2968Delete(pDevice->deviceHandle,
                                   NULL);
            }

            pDevice->deviceHandle = NULL;
        }
    }

    return status;
}


Int32 Vcap_deviceStart()
{
    UInt32 devId;
    Int32 status = 0;

    for(devId = 0; devId < gVcapDeviceModuleContext.numDevices; devId++)
    {
        status |= Vcap_deviceControl(devId, DEVICE_CMD_START, NULL, NULL);
    }

    return status;
}

Int32 Vcap_deviceStop()
{
    UInt32 devId;
    Int32 status = 0;

    for(devId = 0; devId < gVcapDeviceModuleContext.numDevices; devId++)
    {
        status |= Vcap_deviceControl(devId, DEVICE_CMD_STOP, NULL, NULL);
    }

    return status;
}

Int32 Vcap_deviceSetAudioParams(UInt32 numChannels, UInt32 samplingHz,UInt32 audioVolume)
{
    UInt32 devId = 0;
    Int32  status = 0;
    Device_AudioModeParams   audArgs;
    Int32 cascadeStage = 0;
    VCAP_DEVICE_CONTEXT_S  *pDevice;

    /* Fixed Setting for video device */
    audArgs.deviceNum           = 0; /* Should not changed */
    audArgs.samplingHz          = samplingHz;
    audArgs.masterModeEnable    = 1;
    audArgs.dspModeEnable       = 0;
    audArgs.ulawEnable          = 0;
    audArgs.audioVolume         = audioVolume;//High value will create gliches and noise in Captured Audio
    audArgs.numAudioChannels    = numChannels;

    for(devId=0;devId<gVcapDeviceModuleContext.numDevices;devId++)
    {
        switch (numChannels)
        {
            case 4:
                audArgs.tdmChannelNum       = 1;
                audArgs.cascadeStage        = 0;
                break;
            case 8:
                audArgs.tdmChannelNum       = 2;
                audArgs.cascadeStage        = cascadeStage;
                cascadeStage++; /* In case of Cascading the cascadeStage  should be incremented by 1 */
                break;
            case 16:
                audArgs.tdmChannelNum       = 4;
                audArgs.cascadeStage        = cascadeStage;
                cascadeStage++; /* In case of Cascading the cascadeStage  should be incremented by 1 */
                break;

            default:
                break;
        }

        pDevice = &gVcapDeviceModuleContext.devInfo[devId];

        status = Vcap_deviceControl(devId,
                                   IOCTL_DEVICE_SET_AUDIO_MODE,
                                   &audArgs,
                                   NULL);
    }

    return status;
}

Int32 Vcap_deviceGetSourceStatus(VCAP_VIDEO_SOURCE_STATUS_S *pPrm)
{
    VCAP_VIDEO_SOURCE_STATUS_PARAMS_S  videoStatusArgs;
    VCAP_VIDEO_SOURCE_CH_STATUS_S      videoStatus;
    UInt32 chId, devId, deviceChId;
    Int32 status = 0;
    VCAP_DEVICE_CONTEXT_S  *pDevice;
    VCAP_VIDEO_SOURCE_CH_STATUS_S *pVidStatus;

    chId = 0;

    for(devId=0; devId<gVcapDeviceModuleContext.numDevices; devId++)
    {
        pDevice = &gVcapDeviceModuleContext.devInfo[devId];

        for(deviceChId=0; deviceChId<pDevice->createPrm.numChInDevice; deviceChId++)
        {
            pVidStatus = &pPrm->chStatus[chId];

            videoStatusArgs.channelNum = deviceChId;

            memset(&videoStatus, 0, sizeof(videoStatus));

            status |= Vcap_deviceControl(devId,
                                    IOCTL_DEVICE_VIDEO_DECODER_GET_VIDEO_STATUS,
                                    &videoStatusArgs,
                                    &videoStatus
                                );

            pVidStatus->isVideoDetect = videoStatus.isVideoDetect;
            pVidStatus->frameWidth    = videoStatus.frameWidth;
            pVidStatus->frameHeight   = videoStatus.frameHeight;
            pVidStatus->frameInterval = videoStatus.frameInterval;
            pVidStatus->isInterlaced  = videoStatus.isInterlaced;
            pVidStatus->vipInstId = devId;
            pVidStatus->chId = deviceChId;

            chId++;
        }
   }

   pPrm->numChannels = chId;

   return status;
}

Int32 Vcap_deviceSetColor(Int32 contrast, Int32 brightness, Int32 saturation, Int32 hue, Int32 chId)
{
    Int32 status = 0, curChId, devId, deviceChId;
    VCAP_DEVICE_CONTEXT_S  *pDevice;
    Device_VideoDecoderColorParams colorPrm;

    if(brightness < 0)
        brightness = DEVICE_VIDEO_DECODER_NO_CHANGE;
    if(brightness > 255)
        brightness = DEVICE_VIDEO_DECODER_NO_CHANGE;

    if(contrast < 0)
        contrast = DEVICE_VIDEO_DECODER_NO_CHANGE;
    if(contrast > 255)
        contrast = DEVICE_VIDEO_DECODER_NO_CHANGE;

    if(saturation < 0)
        saturation = DEVICE_VIDEO_DECODER_NO_CHANGE;
    if(saturation > 255)
        saturation = DEVICE_VIDEO_DECODER_NO_CHANGE;

    if(hue < 0)
        hue = DEVICE_VIDEO_DECODER_NO_CHANGE;
    if(hue > 255)
        hue = DEVICE_VIDEO_DECODER_NO_CHANGE;

    colorPrm.videoBrightness = brightness;
    colorPrm.videoContrast   = contrast;
    colorPrm.videoSaturation = saturation;
    colorPrm.videoHue        = hue;
    colorPrm.videoSharpness  = DEVICE_VIDEO_DECODER_NO_CHANGE;

    curChId = 0;

    for(devId=0; devId<gVcapDeviceModuleContext.numDevices; devId++)
    {
        pDevice = &gVcapDeviceModuleContext.devInfo[devId];

        if(chId < curChId+pDevice->createPrm.numChInDevice)
        {
            /* channel maps to this device handle, get device chId */
            deviceChId = chId - curChId;

            colorPrm.channelNum      = deviceChId;

            status = Vcap_deviceControl(devId, IOCTL_DEVICE_VIDEO_DECODER_SET_VIDEO_COLOR, &colorPrm, NULL);
            break;
        }
        curChId+=pDevice->createPrm.numChInDevice;
    }

    return status;
}

Bool Vcap_deviceIsPalMode()
{
    VCAP_DEVICE_CONTEXT_S  *pDevice;

    pDevice = &gVcapDeviceModuleContext.devInfo[0];

    return pDevice->isPalMode;
}
