/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include "ti_vsys_priv.h"
#include "ti_vcap_priv.h"

#include <device.h>
#include <device_videoDecoder.h>


#define VCAP_TRACE_ENABLE_FXN_ENTRY_EXIT           (0)
#define VCAP_TRACE_INFO_PRINT_INTERVAL             (8192)


#if VCAP_TRACE_ENABLE_FXN_ENTRY_EXIT
#define VCAP_TRACE_FXN(str,...)                    do {                           \
                                                     static Int printInterval = 0;\
                                                     if ((printInterval % VCAP_TRACE_INFO_PRINT_INTERVAL) == 0) \
                                                     {                                                          \
                                                         OSA_printf("TI_VCAP:%s function:%s",str,__func__);     \
                                                         OSA_printf(__VA_ARGS__);                               \
                                                     }                                                          \
                                                     printInterval++;                                           \
                                                   } while (0)
#define VCAP_TRACE_FXN_ENTRY(...)                  VCAP_TRACE_FXN("Entered",__VA_ARGS__)
#define VCAP_TRACE_FXN_EXIT(...)                   VCAP_TRACE_FXN("Leaving",__VA_ARGS__)
#else
#define VCAP_TRACE_FXN_ENTRY(...)
#define VCAP_TRACE_FXN_EXIT(...)
#endif

static
Int32 Vcap_setWrbkCaptScParamsDefault(UInt32 inW, UInt32 inH, UInt32 outW, UInt32 outH);
static
Int32 Vcap_startWrbkCaptDefault();
static
Int32 Vcap_stopWrbkCaptDefault();

/* =============================================================================
 * Globals
 * =============================================================================
 */

VCAP_MODULE_CONTEXT_S gVcapModuleContext;

/* =============================================================================
 * Vcap module APIs
 * =============================================================================
 */


Void Vcap_params_init(VCAP_PARAMS_S * pContext)
{
    UInt16 devId, chnId, strmId;

    memset(pContext, 0, sizeof(VCAP_PARAMS_S));
    for(devId = 0; devId < VCAP_DEV_MAX; devId++)
    {
        pContext->deviceParams[devId].portEnable = FALSE;
        pContext->deviceParams[devId].portMode   = VCAP_MODE_BT656_8BIT_YUV422;
        pContext->deviceParams[devId].dataFormat = VCAP_MULTICHN_OPTION_4D1;
        pContext->deviceParams[devId].signalType = VS_AUTO_DETECT;
    }
    for(chnId = 0; chnId < VCAP_CHN_MAX; chnId++)
    {
        for(strmId=0; strmId<VCAP_STRM_MAX; strmId++)
        {
            pContext->channelParams[chnId].strmEnable[strmId]             = FALSE;
            pContext->channelParams[chnId].strmFormat[strmId]             = VF_YUV422I_UYVY;
            pContext->channelParams[chnId].strmResolution[strmId].start_X = 0;
            pContext->channelParams[chnId].strmResolution[strmId].start_Y = 0;
            pContext->channelParams[chnId].strmResolution[strmId].width   = 0;
            pContext->channelParams[chnId].strmResolution[strmId].height  = 0;
        }
        pContext->channelParams[chnId].deviceId                   = 0;
        pContext->channelParams[chnId].dynamicParams.contrast     = 0;
        pContext->channelParams[chnId].dynamicParams.satauration  = 0;
        pContext->channelParams[chnId].dynamicParams.brightness   = 0;
        pContext->channelParams[chnId].dynamicParams.hue          = 0;
    }

    pContext->enableConfigExtVideoDecoder = TRUE;
    printf("pContext->enableConfigExtVideoDecoder is TRUE\n");


}


Int32 Vcap_init(VCAP_PARAMS_S * pContext)
{
    //UInt16 devId,
    UInt16 chnId, strmId, linkId;

    /* Mark all links related to capture as invalid by default, they will be setup with valid IDs later */
    gVcapModuleContext.captureId             = SYSTEM_LINK_ID_INVALID;
    for(linkId=0; linkId<MAX_NSF_LINK; linkId++)
    {
        gVcapModuleContext.nsfId[linkId]                 = SYSTEM_LINK_ID_INVALID;
    }
    gVcapModuleContext.capSwMsId             = SYSTEM_LINK_ID_INVALID;
    for(linkId=0; linkId<MAX_SCLR_LINK; linkId++)
    {
        gVcapModuleContext.sclrId[linkId]             = SYSTEM_LINK_ID_INVALID;
    }
    for(linkId=0; linkId<MAX_ALG_LINK; linkId++)
    {
        gVcapModuleContext.dspAlgId[linkId]           = SYSTEM_LINK_ID_INVALID;
    }
    for(linkId=0; linkId<MAX_IPC_FRAMES_LINK; linkId++)
    {
        gVcapModuleContext.ipcFramesOutVpssId[linkId]    = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.ipcFramesInDspId[linkId]      = SYSTEM_LINK_ID_INVALID;
    }
    for(linkId=0; linkId<MAX_DEI_LINK; linkId++)
    {
        gVcapModuleContext.deiId[linkId] = SYSTEM_LINK_ID_INVALID;
    }
    gVcapModuleContext.nullSrcId                = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.ipcFramesOutVpssToHostId = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.ipcFramesInHostId        = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.ipcBitsInHLOSId          = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.callbackFxn.newDataAvailableCb       = NULL;
    gVcapModuleContext.callbackArg                          = NULL;
    gVcapModuleContext.bitscallbackFxn.newDataAvailableCb   = NULL;
    gVcapModuleContext.bitscallbackArg                      = NULL;

    /* initialize counters */
    gVcapModuleContext.numChannels = 0;

    if(pContext==NULL)
    {
        Vcap_params_init(&gVcapModuleContext.vcapConfig);
    }
    else
    {

        for (chnId = 0; chnId < VCAP_CHN_MAX; chnId++)
        {
            for(strmId=0; strmId<VCAP_STRM_MAX; strmId++)
            {
                if(pContext->channelParams[chnId].strmEnable[strmId])
                {
                    gVcapModuleContext.numChannels++;
                }
            }
        }

        memcpy(&gVcapModuleContext.vcapConfig, pContext, sizeof(VCAP_PARAMS_S));

        gVcapModuleContext.numChannels = pContext->numChn;

        gVcapModuleContext.isPalMode = FALSE;
        gVcapModuleContext.isWrbkCaptEnable = FALSE;

    }
    gVcapModuleContext.setWrbkCaptScParamsFxn = Vcap_setWrbkCaptScParamsDefault;
    gVcapModuleContext.startWrbkCaptFxn       = Vcap_startWrbkCaptDefault;
    gVcapModuleContext.stopWrbkCaptFxn        = Vcap_stopWrbkCaptDefault;


    return 0;
}


/* Init done once; do not uninit video device handle */
Int32 Vcap_reInit(VCAP_PARAMS_S * pContext)
{
    //UInt16 devId,
    UInt16 chnId, strmId, linkId;

    /* Mark all links related to capture as invalid by default, they will be setup with valid IDs later */
    gVcapModuleContext.captureId             = SYSTEM_LINK_ID_INVALID;
    for(linkId=0; linkId<MAX_NSF_LINK; linkId++)
    {
        gVcapModuleContext.nsfId[linkId]                 = SYSTEM_LINK_ID_INVALID;
    }
    gVcapModuleContext.capSwMsId             = SYSTEM_LINK_ID_INVALID;
    for(linkId=0; linkId<MAX_SCLR_LINK; linkId++)
    {
        gVcapModuleContext.sclrId[linkId]             = SYSTEM_LINK_ID_INVALID;
    }
    for(linkId=0; linkId<MAX_ALG_LINK; linkId++)
    {
        gVcapModuleContext.dspAlgId[linkId]           = SYSTEM_LINK_ID_INVALID;
    }
    for(linkId=0; linkId<MAX_IPC_FRAMES_LINK; linkId++)
    {
        gVcapModuleContext.ipcFramesOutVpssId[linkId]    = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.ipcFramesInDspId[linkId]      = SYSTEM_LINK_ID_INVALID;
    }
    for(linkId=0; linkId<MAX_DEI_LINK; linkId++)
    {
        gVcapModuleContext.deiId[linkId] = SYSTEM_LINK_ID_INVALID;
    }
    gVcapModuleContext.nullSrcId                = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.ipcFramesOutVpssToHostId = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.ipcFramesInHostId        = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.ipcBitsInHLOSId          = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.callbackFxn.newDataAvailableCb       = NULL;
    gVcapModuleContext.callbackArg                          = NULL;
    gVcapModuleContext.bitscallbackFxn.newDataAvailableCb   = NULL;
    gVcapModuleContext.bitscallbackArg                      = NULL;

    /* initialize counters */
    gVcapModuleContext.numChannels = 0;

    if(pContext==NULL)
    {
        Vcap_params_init(&gVcapModuleContext.vcapConfig);
    }
    else
    {

        for (chnId = 0; chnId < VCAP_CHN_MAX; chnId++)
        {
            for(strmId=0; strmId<VCAP_STRM_MAX; strmId++)
            {
                if(pContext->channelParams[chnId].strmEnable[strmId])
                {
                    gVcapModuleContext.numChannels++;
                }
            }
        }

        memcpy(&gVcapModuleContext.vcapConfig, pContext, sizeof(VCAP_PARAMS_S));

        gVcapModuleContext.numChannels = pContext->numChn;

        gVcapModuleContext.isPalMode = FALSE;

    }
    return 0;
}

Void Vcap_exit()
{
    /* Empty for now */
}


Int32 Vcap_start()
{
    UInt32 linkId;
    Int32  status = 0;

    status = Vcap_deviceStart();

    /* start can happen in any order, except its recommended to start capture Link the last */
    if(gVcapModuleContext.ipcFramesOutVpssToHostId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStart(gVcapModuleContext.ipcFramesOutVpssToHostId);
    }

    if(gVcapModuleContext.ipcFramesInHostId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStart(gVcapModuleContext.ipcFramesInHostId);
    }

    if(gVcapModuleContext.ipcBitsInHLOSId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStart(gVcapModuleContext.ipcBitsInHLOSId);
    }
    /* start can happen in any order, except its recommended to start capture Link the last */
    for(linkId=0; linkId<MAX_NSF_LINK; linkId++)
    {
        if(gVcapModuleContext.nsfId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkStart(gVcapModuleContext.nsfId[linkId]);
        }
    }

    /* start can happen in any order, except its recommended to start capture Link the last */
    for(linkId = 0; linkId < MAX_IPC_FRAMES_LINK; linkId++)
    {
        if(gVcapModuleContext.ipcFramesOutVpssId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkStart(gVcapModuleContext.ipcFramesOutVpssId[linkId]);
        }
        if(gVcapModuleContext.ipcFramesInDspId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkStart(gVcapModuleContext.ipcFramesInDspId[linkId]);
        }
    }

    for(linkId = 0; linkId < MAX_DEI_LINK; linkId++)
    {
        if(gVcapModuleContext.deiId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkStart(gVcapModuleContext.deiId[linkId]);
        }
    }

    for(linkId=0; linkId<MAX_SCLR_LINK; linkId++)
    {
        if(gVcapModuleContext.sclrId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkStart(gVcapModuleContext.sclrId[linkId]);
        }
    }

    if(gVcapModuleContext.nullSrcId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStart(gVcapModuleContext.nullSrcId);
    }

    /* Start taking CPU load just before starting of capture link */
    MultiCh_prfLoadCalcEnable(TRUE, FALSE, FALSE);

    if(gVcapModuleContext.capSwMsId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStart(gVcapModuleContext.capSwMsId);
    }

    if(gVcapModuleContext.captureId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStart(gVcapModuleContext.captureId);
    }

    return 0;
}


Int32 Vcap_stop()
{
    UInt32 linkId;
    Int32 status = 0;

    status = Vcap_deviceStop();

    /* stop needs to be in the reseverse order of create */

    if(gVcapModuleContext.captureId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStop(gVcapModuleContext.captureId);
    }

    if(gVcapModuleContext.nullSrcId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStop(gVcapModuleContext.nullSrcId);
    }

    if(gVcapModuleContext.capSwMsId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStop(gVcapModuleContext.capSwMsId);
    }

    for(linkId = 0; linkId < MAX_IPC_FRAMES_LINK; linkId++)
    {
        if(gVcapModuleContext.ipcFramesOutVpssId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
           System_linkStop(gVcapModuleContext.ipcFramesOutVpssId[linkId]);
        }
        if(gVcapModuleContext.ipcFramesInDspId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkStop(gVcapModuleContext.ipcFramesInDspId[linkId]);
        }
    }

    for(linkId = 0;linkId < MAX_DEI_LINK;linkId++)
    {
        if(gVcapModuleContext.deiId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkStop(gVcapModuleContext.deiId[linkId]);
        }
    }

    for(linkId=0; linkId<MAX_NSF_LINK; linkId++)
    {
        if(gVcapModuleContext.nsfId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkStop(gVcapModuleContext.nsfId[linkId]);
        }
    }

    for(linkId=0; linkId<MAX_SCLR_LINK; linkId++)
    {
        if(gVcapModuleContext.sclrId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkStop(gVcapModuleContext.sclrId[linkId]);
        }
    }

    if(gVcapModuleContext.ipcFramesOutVpssToHostId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStop(gVcapModuleContext.ipcFramesOutVpssToHostId);
    }

    if(gVcapModuleContext.ipcFramesInHostId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStop(gVcapModuleContext.ipcFramesInHostId);
    }

    if(gVcapModuleContext.ipcBitsInHLOSId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkStop(gVcapModuleContext.ipcBitsInHLOSId);
    }

    return 0;
}


/*
    Select the channel in capture link for whom extra buffers are needed
*/
Int32 Vcap_setExtraFramesChId(UInt32 chId)
{
    CaptureLink_ExtraFramesChId  prm;
    Int32 status;

    /* currently in all McFW use-case only QUE0 of capture is used */
    prm.queId = 0;
    prm.chId  = chId;

    status = System_linkControl(
            gVcapModuleContext.captureId,
            CAPTURE_LINK_CMD_SET_EXTRA_FRAMES_CH_ID,
            &prm,
            sizeof(prm),
            TRUE
            );

    return status;
}

/**
 * \brief:
 *      Get capture channels enabled
 * \input:
 *      NA
 * \output:
 *      NA
 * \return
*       Number of capture channels
*/
Int32 Vcap_getNumChannels(Void)
{
    return gVcapModuleContext.vcapConfig.numChn;
}

Int32 Vcap_setParamDevice(VCAP_DEV vcDevId, VCAP_DEV_PARAM_S *psvcDevParam, VCAP_PARAMS_E paramId)
{
    memcpy(&gVcapModuleContext.vcapConfig.deviceParams[vcDevId], psvcDevParam, sizeof(VCAP_DEV_PARAM_S));
    return 0;
}


Int32 Vcap_getParamDevice(VCAP_DEV vcDevId, VCAP_DEV_PARAM_S *psvcDevParam, VCAP_PARAMS_E paramId)
{
    memcpy(psvcDevParam,&gVcapModuleContext.vcapConfig.deviceParams[vcDevId],sizeof(VCAP_DEV_PARAM_S));
    return 0;
}

Int32 Vcap_enableDevice(VCAP_DEV vcDevId)
{
    return 0;
}

Int32 Vcap_disableDevice(VCAP_DEV vcDevId)
{
    return 0;
}

Int32 Vcap_setParamChn(VCAP_CHN vcChnId, VCAP_CHN_PARAM_S *psCapChnParam, VCAP_PARAMS_E paramId)
{
    memcpy(&gVcapModuleContext.vcapConfig.channelParams[vcChnId], psCapChnParam, sizeof(VCAP_CHN_PARAM_S));
    return 0;
}

Int32 Vcap_getParamChn(VCAP_CHN vcChnId, VCAP_CHN_PARAM_S *psCapChnParam, VCAP_PARAMS_E paramId)
{
    memcpy(psCapChnParam, &gVcapModuleContext.vcapConfig.channelParams[vcChnId], sizeof(VCAP_CHN_PARAM_S));
    return 0;
}

Int32 Vcap_setDynamicParamChn(VCAP_CHN vcChnId, VCAP_CHN_DYNAMIC_PARAM_S *psCapChnDynaParam, VCAP_PARAMS_E paramId)
{
    Int32 status = ERROR_NONE;

    DeiLink_chDynamicSetOutRes params = {0};
    SclrLink_chDynamicSetOutRes prms = {0};
    CaptureLink_SetResolution vcapSetRes = {0};
    UInt32 deiId = 0;
    UInt32 scdAlgLinkId;


#ifdef TI_816X_BUILD
    if((gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) ||
        (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_VCAP_VENC) ||
        (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_SD_VCAP_VENC)
    )
         scdAlgLinkId = gVcapModuleContext.dspAlgId[1];
    else
#endif
         scdAlgLinkId = gVcapModuleContext.dspAlgId[0];


    if(gVcapModuleContext.captureId==SYSTEM_LINK_ID_INVALID)
        return ERROR_FAIL;

    switch(paramId)
    {
        case VCAP_FORMAT:
            break;
        case VCAP_RESOLUTION:
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
            if(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF)
            {
                return MultiCh_progressive16ChVcapVencVdecVdis_setCapDynamicParamChn(vcChnId, psCapChnDynaParam, paramId);
            }
            if(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_16CH_NRT)
            {
                return MultiCh_progressive16ChNrtVcapVencVdecVdisSetOutRes(vcChnId, psCapChnDynaParam);
            }
#endif
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
            if ((gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH)||
                (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_8CH))
            {
                return MultiCh_progressive8ChVcapVencVdecVdis_setCapDynamicParamChn(vcChnId, psCapChnDynaParam, paramId);
            }
#endif
            /* ranran */
#if defined(TI_814X_BUILD)
            if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
            {
                return CommitCh_hdDvrSetCapDynamicParamChn(vcChnId, psCapChnDynaParam, paramId);
            }
#endif
#if defined(TI_816X_BUILD)
            if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_VCAP_VENC)
            {
                return MultiCh_hdDvrSetCapDynamicParamChn(vcChnId, psCapChnDynaParam, paramId);
            }
            if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_SD_VCAP_VENC)
            {
                return MultiCh_hdSdDvrSetCapDynamicParamChn(vcChnId, psCapChnDynaParam, paramId);
            }
#endif

            {
               params.chId = vcChnId;
               params.queId = (UInt32)psCapChnDynaParam->chDynamicRes.pathId;

#ifdef TI_816X_BUILD
               deiId = (vcChnId < 8)? 0: 1;
               params.chId = (vcChnId >= 8)? (vcChnId-8): vcChnId;
#endif
               if(((gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_VCAP_VENC)
                   ||(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_8CH))
                   &&(params.queId == 2)) /* secondary channel, control SC5 for enable/disable */
               {
                prms.chId = vcChnId;
                status = System_linkControl(
                                           gVcapModuleContext.sclrId[0],
                                           SCLR_LINK_CMD_GET_OUTPUTRESOLUTION,
                                           &(prms),
                                           sizeof(prms),
                                           TRUE
                                           );
                prms.width = SystemUtils_align(psCapChnDynaParam->chDynamicRes.width, 16);
                prms.height = SystemUtils_align(psCapChnDynaParam->chDynamicRes.height, 1);
                status = System_linkControl(
                                           gVcapModuleContext.sclrId[0],
                                           SCLR_LINK_CMD_SET_OUTPUTRESOLUTION,
                                           &(prms),
                                           sizeof(prms),
                                           TRUE
                                           );
               }
               else
               {
                status = System_linkControl(
                                            gVcapModuleContext.deiId[deiId],
                                            DEI_LINK_CMD_GET_OUTPUTRESOLUTION,
                                            &(params),
                                            sizeof(params),
                                            TRUE
                                            );
                params.width = SystemUtils_align(psCapChnDynaParam->chDynamicRes.width, 16);
                params.height = SystemUtils_align(psCapChnDynaParam->chDynamicRes.height, 1);
                status = System_linkControl(
                                            gVcapModuleContext.deiId[deiId],
                                            DEI_LINK_CMD_SET_OUTPUTRESOLUTION,
                                            &(params),
                                            sizeof(params),
                                            TRUE
                                            );
               }
#ifdef TI_816X_BUILD
               if( (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC)
               && (psCapChnDynaParam->chDynamicRes.pathId == 3))
                {
                    prms.chId = vcChnId;
                    status = System_linkControl(
                                                gVcapModuleContext.sclrId[0],
                                                SCLR_LINK_CMD_GET_OUTPUTRESOLUTION,
                                                &(prms),
                                                sizeof(prms),
                                                TRUE
                                                );
                    prms.width = SystemUtils_align(psCapChnDynaParam->chDynamicRes.width, 16);
                    prms.height = SystemUtils_align(psCapChnDynaParam->chDynamicRes.height, 1);
                    status = System_linkControl(
                                                gVcapModuleContext.sclrId[0],
                                                SCLR_LINK_CMD_SET_OUTPUTRESOLUTION,
                                                &(prms),
                                                sizeof(prms),
                                                TRUE
                                                );
                }
#endif
            }
            break;
        case VCAP_CONTRAST:
            status = Vcap_setColor(psCapChnDynaParam->contrast,
                          -1,
                          -1,
                          -1,
                          vcChnId);
            break;
        case VCAP_SATURATION:
            status = Vcap_setColor(
                          -1,
                          -1,
                          psCapChnDynaParam->satauration,
                          -1,
                          vcChnId);

            break;
        case VCAP_BRIGHTNESS:
            status = Vcap_setColor(
                          -1,
                          psCapChnDynaParam->brightness,
                          -1,
                          -1,
                          vcChnId);
            break;
        case VCAP_HUE:
            status = Vcap_setColor(
                          -1,
                          -1,
                          -1,
                          psCapChnDynaParam->hue,
                          vcChnId);

            break;
        case VCAP_PORTMODE:
            break;
        case VCAP_SIGNALTYPE:
            break;
        case VCAP_OSDWINPRM:
            status = System_linkControl(
                                        gVcapModuleContext.dspAlgId[0],
                                        ALG_LINK_OSD_CMD_SET_CHANNEL_WIN_PRM,
                                        psCapChnDynaParam->osdChWinPrm,
                                        sizeof(AlgLink_OsdChWinParams),
                                        TRUE
                                        );
            break;
        case VCAP_OSDBLINDWINPRM:
            #if defined(TI_8107_BUILD)
            status = System_linkControl(
                                        gVcapModuleContext.dspAlgId[0],
                                        ALG_LINK_OSD_CMD_SET_CHANNEL_BLIND_WIN_PRM,
                                        psCapChnDynaParam->osdChBlindWinPrm,
                                        sizeof(AlgLink_OsdChBlindWinParams),
                                        TRUE
                                        );
            #endif
            break;

        case VCAP_SCDMODE:
            status = System_linkControl(
                                        scdAlgLinkId,
                                        ALG_LINK_SCD_CMD_SET_CHANNEL_MODE,
                                        &(psCapChnDynaParam->scdChPrm),
                                        sizeof(psCapChnDynaParam->scdChPrm),
                                        TRUE
                                        );
            break;
        case VCAP_SCDSENSITIVITY:
            status = System_linkControl(
                                        scdAlgLinkId,
                                        ALG_LINK_SCD_CMD_SET_CHANNEL_SENSITIVITY,
                                        &(psCapChnDynaParam->scdChPrm),
                                        sizeof(psCapChnDynaParam->scdChPrm),
                                        TRUE
                                        );
            break;

        case VCAP_IGNORELIGHTSOFF:
            status = System_linkControl(
                                        scdAlgLinkId,
                                        ALG_LINK_SCD_CMD_SET_CHANNEL_IGNORELIGHTSOFF,
                                        &(psCapChnDynaParam->scdChPrm),
                                        sizeof(psCapChnDynaParam->scdChPrm),
                                        TRUE
                                        );
            break;
        case VCAP_SCDBLOCKCONFIG:
                status = System_linkControl(
                                            scdAlgLinkId,
                                            ALG_LINK_SCD_CMD_SET_CHANNEL_BLOCKCONFIG,
                                            &(psCapChnDynaParam->scdChBlkPrm),
                                            sizeof(psCapChnDynaParam->scdChBlkPrm),
                                            TRUE
                                            );
            break;

        case VCAP_SCDVACONFIG:
                status = System_linkControl(
                                            scdAlgLinkId,
                                            ALG_LINK_SCD_CMD_SET_CHANNEL_VA_PARAMS,
                                            &(psCapChnDynaParam->scdChVaPrm),
                                            sizeof(psCapChnDynaParam->scdChVaPrm),
                                            TRUE
                                            );
            break;
        case VCAP_IGNORELIGHTSON:
            status = System_linkControl(
                                        scdAlgLinkId,
                                        ALG_LINK_SCD_CMD_SET_CHANNEL_IGNORELIGHTSON,
                                        &(psCapChnDynaParam->scdChPrm),
                                        sizeof(psCapChnDynaParam->scdChPrm),
                                        TRUE
                                        );
            break;
        case VCAP_BLINDAREACONFIG:
            status = System_linkControl(
                                        gVcapModuleContext.captureId,
                                        CAPTURE_LINK_CMD_CONFIGURE_BLIND_AREA,
                                        &(psCapChnDynaParam->captureBlindInfo),
                                        sizeof(psCapChnDynaParam->captureBlindInfo),
                                        TRUE
                                        );
            break;
        case VCAP_INPUT_RESOLUTION:
            vcapSetRes.queId  = 0;
            vcapSetRes.chId   = psCapChnDynaParam->inputResolution.chId;
            vcapSetRes.width  = psCapChnDynaParam->inputResolution.width;
            vcapSetRes.height = psCapChnDynaParam->inputResolution.height;
            status = System_linkControl(
                                        gVcapModuleContext.captureId,
                                        CAPTURE_LINK_CMD_SET_RESOLUTION,
                                        &(vcapSetRes),
                                        sizeof(vcapSetRes),
                                        TRUE
                                        );
            break;
        case VCAP_ALL:
            status = Vcap_setColor(
                          psCapChnDynaParam->contrast,
                          psCapChnDynaParam->brightness,
                          psCapChnDynaParam->satauration,
                          psCapChnDynaParam->hue,
                          vcChnId);
            break;
        default:
            break;

    }

    return status;
}

Int32 Vcap_setAudioModeParam(UInt32 numChannels, UInt32 samplingHz, UInt32 audioVolume)
{
    return Vcap_deviceSetAudioParams(numChannels, samplingHz, audioVolume);
}

/**
 * \brief:
 *      Get capture Channel dynamic parameters
 * \input:
 *      vcChnId             -- capture Channel id
 *      psCapChnDynaParam   -- Device dynamic parameter structure
 * \output:
 *      NA
 * \return
*       ERROR_NONE    --  while success
*       ERROR_CODE          --  refer for err defination
*/
Int32 Vcap_getDynamicParamChn(VCAP_CHN vcChnId, VCAP_CHN_DYNAMIC_PARAM_S *psCapChnDynaParam, VCAP_PARAMS_E paramId)
{
    Int32 status = ERROR_NONE;

    DeiLink_chDynamicSetOutRes params = {0};
    SclrLink_chDynamicSetOutRes prms = {0};
    UInt32 deiId = 0;
    UInt32 scdAlgLinkId;

#ifdef TI_816X_BUILD
    if((gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) ||
        (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_VCAP_VENC) ||
        (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_SD_VCAP_VENC)
    )
         scdAlgLinkId = gVcapModuleContext.dspAlgId[1];
    else
#endif
         scdAlgLinkId = gVcapModuleContext.dspAlgId[0];


    if(gVcapModuleContext.captureId==SYSTEM_LINK_ID_INVALID)
        return ERROR_FAIL;

    switch(paramId)
    {
        case VCAP_RESOLUTION:
            #if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
            if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF)
            {
                return MultiCh_progressive16ChVcapVencVdecVdis_getCapDynamicParamChn(vcChnId, psCapChnDynaParam, paramId);
            }
            if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_16CH_NRT)
            {
                return MultiCh_progressive16ChNrtVcapVencVdecVdisGetOutRes(vcChnId, psCapChnDynaParam);
            }
            #endif
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
            if ((gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH)||
                (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_8CH))
            {
                return MultiCh_progressive8ChVcapVencVdecVdis_getCapDynamicParamChn(vcChnId, psCapChnDynaParam, paramId);
            }
#endif
#if defined(TI_814X_BUILD)
            if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
            {
                return CommitCh_hdDvrGetCapDynamicParamChn(vcChnId, psCapChnDynaParam, paramId);
            }
#endif
#if defined(TI_816X_BUILD)
            if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_VCAP_VENC)
            {
                return MultiCh_hdDvrGetCapDynamicParamChn(vcChnId, psCapChnDynaParam, paramId);
            }
            if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_SD_VCAP_VENC)
            {
                return MultiCh_hdSdDvrGetCapDynamicParamChn(vcChnId, psCapChnDynaParam, paramId);
            }
#endif
            params.chId = vcChnId;
            params.queId = (UInt32)psCapChnDynaParam->chDynamicRes.pathId;
            #ifdef TI_816X_BUILD
            deiId = (vcChnId < 8)? 0: 1;
            params.chId = (vcChnId >= 8)? (vcChnId-8): vcChnId;
            #endif
            if(((gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_VCAP_VENC)
                ||(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_8CH))
                &&(params.queId == 2)) /* secondary channel, control SC5 for enable/disable */
            {
                prms.chId = vcChnId;
                status = System_linkControl(
                                       gVcapModuleContext.sclrId[0],
                                       SCLR_LINK_CMD_GET_OUTPUTRESOLUTION,
                                       &(prms),
                                       sizeof(prms),
                                       TRUE
                                       );
                psCapChnDynaParam->chDynamicRes.width = params.width;
                psCapChnDynaParam->chDynamicRes.height = params.height;
            }
            else
            {
                status = System_linkControl(
                                        gVcapModuleContext.deiId[deiId],
                                        DEI_LINK_CMD_GET_OUTPUTRESOLUTION,
                                        &(params),
                                        sizeof(params),
                                        TRUE
                                        );
                psCapChnDynaParam->chDynamicRes.width = params.width;
                psCapChnDynaParam->chDynamicRes.height = params.height;
            }
            break;

        case VCAP_SCDGETALLCHFRAMESTATUS:

            psCapChnDynaParam->scdAllChFrameStatus.numCh = 0;

            status = System_linkControl(
                                        scdAlgLinkId,
                                        ALG_LINK_SCD_CMD_GET_ALL_CHANNEL_FRAME_STATUS,
                                        &(psCapChnDynaParam->scdAllChFrameStatus),
                                        sizeof(psCapChnDynaParam->scdAllChFrameStatus),
                                        TRUE
                                        );
            break;

        case VCAP_SCDPRINTVACHCONFIG:
                status = System_linkControl(
                                            scdAlgLinkId,
                                            ALG_LINK_SCD_CMD_PRINT_ALLCHANNEL_VA_PARAMS,
                                            NULL,
                                            0,
                                            TRUE
                                            );
            break;

        default:
            status = ERROR_FAIL;
            break;
    }

    return status;
}


/**
 * \brief:
 *      Skip any specific FID type. This is an additional control in capture side itself; is really useful for secondary stream <CIF>.
 *      Stream 0 is D1 & Stream 1 is CIF.
 * \input:
 *      vcChnId             -- capture Channel id
 *      fidType             -- TOP/BOTTOM field
 * \output:
 *      NA
 * \return
*       TI_MEDIA_SUCCESS    --  while success
*       ERROR_CODE          --  refer for err defination
*/
Int32 Vcap_skipFidType(VCAP_CHN vcChnId, Int32 fidType)
{
    Int32 status = ERROR_NONE;
    SclrLink_chDynamicSkipFidType params;

    if(gVcapModuleContext.sclrId[0]!=SYSTEM_LINK_ID_INVALID)
    {
        params.chId = vcChnId;
        params.fidType = fidType;
        status = System_linkControl(
                                gVcapModuleContext.sclrId[0],
                                SCLR_LINK_CMD_SKIP_FID_TYPE,
                                &(params),
                                sizeof(params),
                                TRUE
                               );
    }
    return status;
}

/**
 * \brief:
 *      Set capture frame rate. This is an additional control in capture side itself; is really useful for secondary stream <CIF>.
 *      Stream 0 is D1 & Stream 1 is CIF.
 * \input:
 *      vcChnId             -- capture Channel id
 *      vStrmId             -- Stream Id
 *      frameRate          -- Frame Rate
 * \output:
 *      NA
 * \return
*       TI_MEDIA_SUCCESS    --  while success
*       ERROR_CODE          --  refer for err defination
*/
Int32 Vcap_setFrameRate(VCAP_CHN vcChnId, VCAP_STRM vStrmId, Int32 inputFrameRate, Int32 outputFrameRate)
{
    DeiLink_ChFpsParams params;
    SclrLink_ChFpsParams sclrParams;
    NsfLink_ChFpsParams nsfParams;
    Int32 status = ERROR_NONE;
    UInt32 noOfDEIChan,deiId;

#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
    if(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF)
    {
        return MultiCh_progressive16ChVcapVencVdecVdisSetCapFrameRate(vcChnId, vStrmId, inputFrameRate, outputFrameRate);
    }
    if(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_16CH_NRT)
    {
        return MultiCh_progressive16ChNrtVcapVencVdecVdisSetCapFrameRate(vcChnId, vStrmId, inputFrameRate, outputFrameRate);
    }
#endif
#if defined(TI_8107_BUILD)
    if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH)
    {
        return MultiCh_progressive8ChVcapVencVdecVdisSetCapFrameRate(vcChnId, vStrmId, inputFrameRate, outputFrameRate);
    }

#endif
#if defined(TI_814X_BUILD)
    if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_8CH)
    {
        return MultiCh_progressive8ChVcapVencVdecVdisSetCapFrameRate(vcChnId, vStrmId, inputFrameRate, outputFrameRate);
    }
#endif
    /* ranran */
#if defined(TI_814X_BUILD)
    if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
    {
        return CommitCh_hdDvrSetCapFrameRate(vcChnId, vStrmId, inputFrameRate, outputFrameRate);
    }
#endif
#if defined(TI_816X_BUILD)
    if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_VCAP_VENC)
    {
        return MultiCh_hdDvrSetCapFrameRate(vcChnId, vStrmId, inputFrameRate, outputFrameRate);
    }
    if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_SD_VCAP_VENC)
    {
        return MultiCh_hdSdDvrSetCapFrameRate(vcChnId, vStrmId, inputFrameRate, outputFrameRate);
    }
#endif
    {
       noOfDEIChan = DEI_LINK_MAX_CH;
       deiId = vcChnId/noOfDEIChan;
    }

    if(vcChnId>=255 || deiId >= MAX_DEI_LINK)
    {
        /* invalid parameter */
        return -1;
    }

    params.chId = vcChnId%noOfDEIChan;
    params.streamId = vStrmId;

    params.inputFrameRate = inputFrameRate;
    params.outputFrameRate = outputFrameRate;

    {
        if ((gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_VCAP_VENC)
           ||(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_8CH))
        {
            if (vStrmId == 2) /* secondary stream */
            {
                sclrParams.chId = vcChnId;
                sclrParams.inputFrameRate = inputFrameRate;
                sclrParams.outputFrameRate = outputFrameRate;

                status = System_linkControl(gVcapModuleContext.sclrId[0], SCLR_LINK_CMD_SET_FRAME_RATE,
                                   &sclrParams, sizeof(sclrParams), TRUE);
                return status;
            }
            if (vStrmId == 3) /* MJPEG stream */
            {
                params.streamId = 2;
            }
        }
        if(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC)
        {
            if (vStrmId == 4) /* secondary stream */
            {
                sclrParams.chId = vcChnId;
                sclrParams.inputFrameRate = inputFrameRate;
                sclrParams.outputFrameRate = outputFrameRate;

                status = System_linkControl(gVcapModuleContext.sclrId[0], SCLR_LINK_CMD_SET_FRAME_RATE,
                                   &sclrParams, sizeof(sclrParams), TRUE);
                return status;
            }
        }
        if(gVcapModuleContext.deiId[deiId]!=SYSTEM_LINK_ID_INVALID)
        {
            status = System_linkControl(gVcapModuleContext.deiId[deiId], DEI_LINK_CMD_SET_FRAME_RATE,
                           &params, sizeof(params), TRUE);

   #if defined(TI_814X_BUILD) && defined(TI_8107_BUILD)
            if(vStrmId==0)
            {
                /* since JPEG stream is generated from preview stream, if preview stream
                     FPS changes we need to change the input FPS for JPEG stream
                     output FPS is still kept to 1

                     ONLY applicable for 4D1 814x progressive use-case
                 */
                if ((gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC) &&
                    (FALSE == gVsysModuleContext.vsysConfig.enableOptBwMode))
                {
                     if(gVcapModuleContext.nsfId[0]!=SYSTEM_LINK_ID_INVALID)
                     {
                         NsfLink_ChFpsParams nsfParams;

                         nsfParams.chId = params.chId;
                         nsfParams.inputFrameRate = params.outputFrameRate;
                         nsfParams.outputFrameRate = 1;

                         status = System_linkControl(gVcapModuleContext.nsfId[0], NSF_LINK_CMD_SET_FRAME_RATE,
                                    &nsfParams, sizeof(nsfParams), TRUE);

                     }
                }
            }
   #endif
        }

   }
   if(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_INTERLACED_VCAP_VDIS_VENC_VDEC)

   {
        if(vStrmId == 0)
        {
            sclrParams.chId = vcChnId;

            sclrParams.inputFrameRate = inputFrameRate;
            sclrParams.outputFrameRate = outputFrameRate;
            status = System_linkControl(gVcapModuleContext.sclrId[0], SCLR_LINK_CMD_SET_FRAME_RATE,
                               &sclrParams, sizeof(sclrParams), TRUE);
        }

        if(vStrmId == 1)
        {
            nsfParams.chId = vcChnId;
            nsfParams.inputFrameRate  = inputFrameRate;
            nsfParams.outputFrameRate = outputFrameRate;
            status = System_linkControl(gVcapModuleContext.nsfId[0], NSF_LINK_CMD_SET_FRAME_RATE,
                           &nsfParams, sizeof(nsfParams), TRUE);
        }
   }
    return status;
}

/**
 * \brief:
 *      Get capture frame rate. Not available now
 * \input:
 *      vcChnId             -- capture Channel id
 *      vStrmId             -- Stream Id
 *      frameRate          -- Frame Rate
 * \output:
 *      NA
 * \return
*       TI_MEDIA_SUCCESS    --  while success
*       ERROR_CODE          --  refer for err defination
*/
Int32 Vcap_getFrameRate(VCAP_CHN vcChnId, VCAP_STRM vStrmId)
{
    return 0;
}


Int32 Vcap_enableDisableChn(VCAP_CHN vcChnId, VCAP_STRM vcStrmId, Bool enableChn)
{
    char *onOffName[] = { "OFF ", "ON" };
    Int32 status = ERROR_FAIL;
    DeiLink_ChannelInfo channelInfo;
    SclrLink_ChannelInfo SclrchannelInfo;
    UInt32 cmdId, deiId;
    UInt32 noOfDEIChan;

#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
    if(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF)
    {
        return MultiCh_progressive16ChVcapVencVdecVdis_enableDisableCapChn(vcChnId, vcStrmId, enableChn);
    }
    if(gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_16CH_NRT)
    {
        status = MultiCh_progressive16ChNrtVcapVencVdecVdisEnableDisableCapChn(vcChnId, vcStrmId, enableChn);
        if (status == ERROR_NONE)
            printf(" VCAP: CH%d STRM%d = [%s]\n", vcChnId, vcChnId, onOffName[enableChn]);
        return status;
    }
#endif
#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
    if ((gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_D1_AND_CIF_8CH) ||
        (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_PROGRESSIVE_VCAP_VDIS_VENC_VDEC_8CH) )
    {
        return MultiCh_progressive8ChVcapVencVdecVdis_enableDisableCapChn(vcChnId, vcStrmId, enableChn);
    }
#endif
    /* ranran */
#if defined(TI_814X_BUILD)
    if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_COMMIT_VCAP_VENC_VDIS)
    {
        return CommitCh_hdDvrEnableDisableCapChn(vcChnId, vcStrmId, enableChn);
    }
#endif
#if defined(TI_816X_BUILD)
    if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_VCAP_VENC)
    {
        return MultiCh_hdDvrEnableDisableCapChn(vcChnId, vcStrmId, enableChn);
    }
    if (gVsysModuleContext.vsysConfig.systemUseCase == VSYS_USECASE_MULTICHN_HD_SD_VCAP_VENC)
    {
        return MultiCh_hdSdDvrEnableDisableCapChn(vcChnId, vcStrmId, enableChn);
    }
#endif
    {
      noOfDEIChan = DEI_LINK_MAX_CH;
    }

    channelInfo.channelId = vcChnId%noOfDEIChan;
    channelInfo.streamId  = vcStrmId;
    channelInfo.enable    = enableChn;

    deiId = vcChnId/noOfDEIChan;

    if(enableChn)
        cmdId = DEI_LINK_CMD_ENABLE_CHANNEL;
    else
        cmdId = DEI_LINK_CMD_DISABLE_CHANNEL;


    if(channelInfo.streamId == 2)
    {
        /* secondary channel, control SC5 for enable/disable */
        SclrchannelInfo.channelId = vcChnId;
        SclrchannelInfo.enable    = enableChn;

        if(enableChn)
            cmdId = SCLR_LINK_CMD_ENABLE_CHANNEL;
        else
            cmdId = SCLR_LINK_CMD_DISABLE_CHANNEL;

        if(gVcapModuleContext.sclrId[0]!=SYSTEM_LINK_ID_INVALID)
        {
            status = System_linkControl(
                                        gVcapModuleContext.sclrId[0],
                                        cmdId,
                                        &(SclrchannelInfo),
                                        sizeof(SclrLink_ChannelInfo),
                                        TRUE
                                        );
        }
    }
    else if(channelInfo.streamId == 3)
    {
        /* secondary channel, control SC5 for enable/disable */
        SclrchannelInfo.channelId = vcChnId;
        SclrchannelInfo.enable    = enableChn;

        if(enableChn)
            cmdId = SCLR_LINK_CMD_ENABLE_CHANNEL;
        else
            cmdId = SCLR_LINK_CMD_DISABLE_CHANNEL;

        if(gVcapModuleContext.sclrId[1]!=SYSTEM_LINK_ID_INVALID)
        {
            status = System_linkControl(
                                        gVcapModuleContext.sclrId[1],
                                        cmdId,
                                        &(SclrchannelInfo),
                                        sizeof(SclrLink_ChannelInfo),
                                        TRUE
                                        );
        }
    }
    else
    {
        if(deiId < MAX_DEI_LINK)
        {
            if(gVcapModuleContext.deiId[deiId]!=SYSTEM_LINK_ID_INVALID)
            {
                status = System_linkControl(
                                            gVcapModuleContext.deiId[deiId],
                                            cmdId,
                                            &(channelInfo),
                                            sizeof(DeiLink_ChannelInfo),
                                            TRUE
                                            );
            }
        }
    }

    printf(" VCAP: CH%d STRM%d = [%s]\n", vcChnId, vcStrmId, onOffName[enableChn]);

    return status;

}

Int32 Vcap_enableChn(VCAP_CHN vcChnId, VCAP_STRM vcStrmId)
{
    return Vcap_enableDisableChn(vcChnId, vcStrmId, TRUE);
}


Int32 Vcap_disableChn(VCAP_CHN vcChnId, VCAP_STRM vcStrmId)
{
    return Vcap_enableDisableChn(vcChnId, vcStrmId, FALSE);
}


Int32 Vcap_lock2DisplayChn( VCAP_CHN vcChnId,  VDIS_CHN vdChn) // need discuss for intput/output
{
    return 0;
}

Int32 Vcap_unLock2DisplayChn(VCAP_CHN vcChnId)
{
    return 0;
}

Int32 Vcap_getChnBufferBlocking(VCAP_CHN vcChnId, UInt8 *pChnBuffer, UInt32 uiTimeoutMs)       // consider later
{
    return 0;
}

Int32 Vcap_getChnBufferNonBlocking(VCAP_CHN vcChnId, UInt8 *pChnBuffer)        // consider later
{
    return 0;
}

Int32 Vcap_releaseChnBuffer(VCAP_CHN vcChnId, UInt8 *pChnBuffer)   // consider later
{
    return 0;
}

Int32 Vcap_delete()
{
    UInt32 linkId;
    /* delete can be done in any order */

    if(gVcapModuleContext.captureId!=SYSTEM_LINK_ID_INVALID)
        System_linkDelete(gVcapModuleContext.captureId);

    if(gVcapModuleContext.capSwMsId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkDelete(gVcapModuleContext.capSwMsId);
    }

    for(linkId=0; linkId<MAX_IPC_FRAMES_LINK; linkId++)
    {
        if(gVcapModuleContext.ipcFramesOutVpssId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
           System_linkDelete(gVcapModuleContext.ipcFramesOutVpssId[linkId]);
        }

        if(gVcapModuleContext.ipcFramesInDspId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkDelete(gVcapModuleContext.ipcFramesInDspId[linkId]);
        }
    }

    for(linkId=0; linkId<MAX_ALG_LINK; linkId++)
    {
        if(gVcapModuleContext.dspAlgId[linkId]!=SYSTEM_LINK_ID_INVALID)
        {
            System_linkDelete(gVcapModuleContext.dspAlgId[linkId]);
        }
    }

    for(linkId=0; linkId<MAX_DEI_LINK; linkId++)
    {
        if(gVcapModuleContext.deiId[linkId]!=SYSTEM_LINK_ID_INVALID)
            System_linkDelete(gVcapModuleContext.deiId[linkId]);
    }

    for(linkId=0; linkId<MAX_NSF_LINK; linkId++)
    {
        if(gVcapModuleContext.nsfId[linkId]!=SYSTEM_LINK_ID_INVALID)
            System_linkDelete(gVcapModuleContext.nsfId[linkId]);
    }

    for(linkId=0; linkId<MAX_SCLR_LINK; linkId++)
    {
        if(gVcapModuleContext.sclrId[linkId]!=SYSTEM_LINK_ID_INVALID)
            System_linkDelete(gVcapModuleContext.sclrId[linkId]);
    }

    if(gVcapModuleContext.ipcFramesOutVpssToHostId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkDelete(gVcapModuleContext.ipcFramesOutVpssToHostId);
    }

    if(gVcapModuleContext.ipcFramesInHostId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkDelete(gVcapModuleContext.ipcFramesInHostId);
    }
    if(gVcapModuleContext.ipcBitsInHLOSId!=SYSTEM_LINK_ID_INVALID)
    {
        System_linkDelete(gVcapModuleContext.ipcBitsInHLOSId);
    }

    Vcap_deviceDelete();

    return 0;
}

/**
 * \brief:
 *              register call back which will post the message
 * \input:
 *              callback                -- callback function
 * \output:
 *              NA
 * \return
*               TI_MEDIA_SUCCESS        --      while success
*               ERROR_CODE                      --      refer for err defination
*/
Int32 Vcap_registerCallback(VCAP_CALLBACK_S * callback, Ptr arg)
{
    gVcapModuleContext.callbackFxn = *callback;
    gVcapModuleContext.callbackArg = arg;

    return 0;
}

/**
 * \brief:
 *              register bits call back which will post the message
 * \input:
 *              callback                -- callback function
 * \output:
 *              NA
 * \return
*               TI_MEDIA_SUCCESS        --      while success
*               ERROR_CODE                      --      refer for err defination
*/
Int32 Vcap_registerBitsCallback(VCAP_CALLBACK_S * callback, Ptr arg)
{
    gVcapModuleContext.bitscallbackFxn = *callback;
    gVcapModuleContext.bitscallbackArg = arg;

    return 0;
}

/**
    \brief Returns Bits Callback info registered by the application

    \param callback            [OUT] Pointer to User specified callbacks

    \param arg                 [OUT] Pointer to Callback context

    \return ERROR_NONE on success
*/
Int32 Vcap_getBitsCallbackInfo(VCAP_CALLBACK_S ** callback, Ptr *arg)
{
    *callback = &gVcapModuleContext.bitscallbackFxn;
    *arg      = gVcapModuleContext.bitscallbackArg;

    return 0;
}

/**
 * \brief:
 *              MCFW layer call back which will invoke the app registered callback
 * \input:
 *              callback                -- callback function
 * \output:
 *              NA
 * \return
*               TI_MEDIA_SUCCESS        --      while success
*               ERROR_CODE                      --      refer for err defination
*/
Void Vcap_ipcFramesInCbFxn(Ptr cbCtx)
{
    OSA_assert(cbCtx == &gVcapModuleContext);

    VCAP_TRACE_FXN_ENTRY("Vcap_ipcFramesInCbFxn");

    if (gVcapModuleContext.callbackFxn.newDataAvailableCb)
    {
        gVcapModuleContext.callbackFxn.newDataAvailableCb(gVcapModuleContext.callbackArg);
    }
}

static Void Vcap_copyVidFrameInfoLink2McFw(VIDEO_FRAMEBUF_S *dstBuf,
                                           VIDFrame_Buf    *srcBuf)
{
    Int i,j;
    OSA_assert(VIDEO_MAX_FIELDS == VIDFRAME_MAX_FIELDS);
    OSA_assert(VIDEO_MAX_PLANES == VIDFRAME_MAX_PLANES);

    for (i = 0; i < VIDEO_MAX_FIELDS; i++)
    {
        for (j = 0; j < VIDEO_MAX_PLANES; j++)
        {
            dstBuf->addr[i][j] = srcBuf->addr[i][j];
            dstBuf->phyAddr[i][j] = srcBuf->phyAddr[i][j];
        }
    }
    dstBuf->channelNum  = srcBuf->channelNum;
    dstBuf->fid         = srcBuf->fid;
    dstBuf->frameWidth  = srcBuf->frameWidth;
    dstBuf->frameHeight = srcBuf->frameHeight;
    dstBuf->linkPrivate = srcBuf->linkPrivate;
    dstBuf->timeStamp   = srcBuf->timeStamp;
    dstBuf->framePitch[0] = srcBuf->framePitch[0];
    dstBuf->framePitch[1] = srcBuf->framePitch[1];
    dstBuf->startX        = srcBuf->startX;
    dstBuf->startY        = srcBuf->startY;

    VCAP_TRACE_FXN_EXIT("VidFrameInfo:"
                         "virt[0][0]:%p,"
                         "phy[0][0]:%p,"
                         "channelNum:%d,"
                         "fid:%d,"
                         "frameWidth:%d,"
                         "frameHeight:%d,"
                         "framePitch[0]:%d,"
                         "framePitch[1]:%d,"
                         "timeStamp:%d,"
                         "startX:%d",
                         "startY:%d",
                         dstBuf->addr[0][0],
                         dstBuf->phyAddr[0][0],
                         dstBuf->channelNum,
                         dstBuf->fid,
                         dstBuf->frameWidth,
                         dstBuf->frameHeight,
                         dstBuf->framePitch[0],
                         dstBuf->framePitch[1],
                         dstBuf->timeStamp,
                         dstBuf->startX,
                         dstBuf->startY);
}

static Void Vcap_copyVidFrameInfoMcFw2Link(VIDFrame_Buf *dstBuf,
                                           VIDEO_FRAMEBUF_S    *srcBuf)
{
    Int i,j;
    OSA_assert(VIDEO_MAX_FIELDS == VIDFRAME_MAX_FIELDS);
    OSA_assert(VIDEO_MAX_PLANES == VIDFRAME_MAX_PLANES);

    for (i = 0; i < VIDEO_MAX_FIELDS; i++)
    {
        for (j = 0; j < VIDEO_MAX_PLANES; j++)
        {
            dstBuf->addr[i][j] = srcBuf->addr[i][j];
            dstBuf->phyAddr[i][j] = srcBuf->phyAddr[i][j];
        }
    }
    dstBuf->channelNum  = srcBuf->channelNum;
    dstBuf->fid         = srcBuf->fid;
    dstBuf->frameWidth  = srcBuf->frameWidth;
    dstBuf->frameHeight = srcBuf->frameHeight;
    dstBuf->linkPrivate = srcBuf->linkPrivate;
    dstBuf->timeStamp   = srcBuf->timeStamp;
    dstBuf->framePitch[0] = srcBuf->framePitch[0];
    dstBuf->framePitch[1] = srcBuf->framePitch[1];
    dstBuf->startX        = srcBuf->startX;
    dstBuf->startY        = srcBuf->startY;

    VCAP_TRACE_FXN_EXIT("VidFrameInfo:"
                         "virt[0][0]:%p,"
                         "phy[0][0]:%p,"
                         "channelNum:%d,"
                         "fid:%d,"
                         "frameWidth:%d,"
                         "frameHeight:%d,"
                         "framePitch[0]:%d,"
                         "framePitch[1]:%d,"
                         "timeStamp:%d,"
                         "startX:%d,"
                         "startY:%d,"
                         dstBuf->addr[0][0],
                         dstBuf->phyAddr[0][0],
                         dstBuf->channelNum,
                         dstBuf->fid,
                         dstBuf->frameWidth,
                         dstBuf->frameHeight,
                         dstBuf->framePitch[0],
                         dstBuf->framePitch[1],
                         dstBuf->timeStamp,
                         dstBuf->startX,
                         dstBuf->startY);
}

static Void Vcap_copyBitBufInfoLink2McFw(VALG_FRAMERESULTBUF_S *dstBuf,
                                         Bitstream_Buf    *srcBuf)
{
    dstBuf->reserved       = (UInt32)srcBuf;
    dstBuf->bufVirtAddr    = srcBuf->addr;
    dstBuf->bufSize        = srcBuf->bufSize;
    dstBuf->chnId          = srcBuf->channelNum;
    dstBuf->filledBufSize  = srcBuf->fillLength;
    dstBuf->timestamp      = srcBuf->timeStamp;
    dstBuf->upperTimeStamp = srcBuf->upperTimeStamp;
    dstBuf->lowerTimeStamp = srcBuf->lowerTimeStamp;
    dstBuf->bufPhysAddr    = (Void *)srcBuf->phyAddr;
    dstBuf->frameWidth     = srcBuf->frameWidth;
    dstBuf->frameHeight    = srcBuf->frameHeight;

    VCAP_TRACE_FXN_EXIT("BitBufInfo:"
                         "virt:%p,"
                         "bufSize:%d,"
                         "chnId:%d,"
                         "filledBufSize:%d,"
                         "timeStamp:%d,"
                         "phy:%p,"
                         "width:%d"
                         "height:%d",
                         dstBuf->bufVirtAddr,
                         dstBuf->bufSize,
                         dstBuf->chnId,
                         dstBuf->filledBufSize,
                         dstBuf->timestamp,
                         dstBuf->bufPhysAddr,
                         dstBuf->frameWidth,
                         dstBuf->frameHeight);
}

/**
    \brief Get encoded buffers from McFW

    \param pBitsBufList [OUT]   List of Bistream Buffers returned by the function
    \param timeout      [IN]    VSYS_WAIT_FOREVER or VSYS_NO_WAIT or timeout in units of msec

    \return SUCCESS or FAIL
 */
Int32 Vcap_getAlgResultBuffer(VALG_FRAMERESULTBUF_LIST_S *pBitsBufList, UInt32 timeout)
{
    Bitstream_BufList ipcBufList;
    Bitstream_Buf *pInBuf;
    VALG_FRAMERESULTBUF_S *pOutBuf;
    UInt32 i;

    VCAP_TRACE_FXN_ENTRY();
    pBitsBufList->numBufs = 0;
    ipcBufList.numBufs = 0;

    IpcBitsInLink_getFullVideoBitStreamBufs(gVcapModuleContext.ipcBitsInHLOSId,
                                            &ipcBufList);

    pBitsBufList->numBufs = ipcBufList.numBufs;
    for (i = 0; i < ipcBufList.numBufs; i++)
    {
        pOutBuf = &pBitsBufList->bitsBuf[i];
        pInBuf = ipcBufList.bufs[i];

        Vcap_copyBitBufInfoLink2McFw(pOutBuf,pInBuf);
    }

    VCAP_TRACE_FXN_EXIT("NumBufs Received:%d",pBitsBufList->numBufs);
    return 0;
}
/**
    \brief Release encoded buffers to McFW

    Buffers returned by Vcap_getBitstreamBuffer() are returned to the framework
    for resue after user is done using the encoded bitstreams

    \param pBitsBufList [IN]   List of Bistream Buffers

    \return SUCCESS or FAIL
 */
Int32 Vcap_releaseAlgResultBuffer(VALG_FRAMERESULTBUF_LIST_S *pBitsBufList)
{
    VALG_FRAMERESULTBUF_S *pOutBuf;
    Bitstream_BufList ipcBufList;
    UInt32 i;
    Int status = 0;

    VCAP_TRACE_FXN_ENTRY("Num bufs released:%d",pBitsBufList->numBufs);
    ipcBufList.numBufs = pBitsBufList->numBufs;
    for (i = 0; i < ipcBufList.numBufs; i++)
    {
        pOutBuf = &pBitsBufList->bitsBuf[i];
        ipcBufList.bufs[i] = (Bitstream_Buf*)pBitsBufList->bitsBuf[i].reserved;
    }
    if (ipcBufList.numBufs)
    {
        status =
        IpcBitsInLink_putEmptyVideoBitStreamBufs(gVcapModuleContext.ipcBitsInHLOSId,
                                                 &ipcBufList);
    }
    VCAP_TRACE_FXN_EXIT("Buf release status:%d",status);
    return 0;
}


/**
    \brief Request filled video buffers from framework

    User calls this API to get full video frames from the framework.
    After getting the video frames, user will
    - consume the video frames
    - and then call Vcap_putEmptyVideoFrames() to free the video frames back to the framework

    \param pFrameBufList    [OUT]  List of video frames returned by the framework
    \param timeout          [IN]   TIMEOUT_WAIT_FOREVER or TIMEOUT_NO_WAIT or timeout in msecs

    \return ERROR_NONE on success
*/
Int32 Vcap_getFullVideoFrames(VIDEO_FRAMEBUF_LIST_S *pFrameBufList, UInt32 timeout)
{
    VIDFrame_BufList  vidBufList;
    VIDFrame_Buf     *pInBuf;
    VIDEO_FRAMEBUF_S *pOutBuf;
    UInt32 i;

    VCAP_TRACE_FXN_ENTRY();
    pFrameBufList->numFrames = 0;
    vidBufList.numFrames = 0;
    IpcFramesInLink_getFullVideoFrames(gVcapModuleContext.ipcFramesInHostId,
                                       &vidBufList);

    pFrameBufList->numFrames = vidBufList.numFrames;
    for (i = 0; i < vidBufList.numFrames; i++)
    {
        pOutBuf = &pFrameBufList->frames[i];
        pInBuf = &vidBufList.frames[i];

        Vcap_copyVidFrameInfoLink2McFw(pOutBuf,pInBuf);
    }

    VCAP_TRACE_FXN_EXIT("NumFrames Received:%d",pFrameBufList->numFrames);
    return 0;
}
/**
    \brief Give consumed video frames back to the application to be freed

    Buffers that are were previously got from Vcap_getFullVideoFrames can be
    freed back to the framework by invoking this API.

    \param pFrameBufList [IN]   List of video frames

    \return ERROR_NONE on success
*/
Int32 Vcap_putEmptyVideoFrames(VIDEO_FRAMEBUF_LIST_S *pFrameBufList)
{
    VIDEO_FRAMEBUF_S *pSrcBuf;
    VIDFrame_Buf     *pDstBuf;
    VIDFrame_BufList  vidBufList;
    UInt32 i;
    Int status = 0;

    VCAP_TRACE_FXN_ENTRY("Num bufs released:%d",pFrameBufList->numFrames);
    vidBufList.numFrames = pFrameBufList->numFrames;
    for (i = 0; i < vidBufList.numFrames; i++)
    {
        pSrcBuf = &pFrameBufList->frames[i];
        pDstBuf = &vidBufList.frames[i];
        Vcap_copyVidFrameInfoMcFw2Link(pDstBuf,pSrcBuf);
    }
    if (vidBufList.numFrames)
    {
        status =
        IpcFramesInLink_putEmptyVideoFrames(gVcapModuleContext.ipcFramesInHostId,
                                            &vidBufList);
    }
    VCAP_TRACE_FXN_EXIT("VIDFrame release status:%d",status);
    return 0;
}
/**
    \brief Detects video per channel at the video input.

    \param pFrameBufList [IN]   List of video frames

    \return ERROR_NONE on success
*/

Int32 Vcap_detectVideo()
{

    Int status = 0;

    if((   gVcapModuleContext.vcapConfig.enableConfigExtVideoDecoder)
       && (gVcapModuleContext.captureId!=SYSTEM_LINK_ID_INVALID))
    {
        status = System_linkControl(gVcapModuleContext.captureId,
                                CAPTURE_LINK_CMD_DETECT_VIDEO,
                                NULL,
                                0,
                                TRUE);
    }
    return status;

}

Int32 Vcap_getVideoSourceStatus(VCAP_VIDEO_SOURCE_STATUS_S *pStatus)
{
    Int32 status = 0;

    if(gVcapModuleContext.vcapConfig.enableConfigExtVideoDecoder)
    {
        status = Vcap_deviceGetSourceStatus(&gVcapModuleContext.videoStatus);
    }

    if(pStatus != NULL)
    {
        memcpy(pStatus,
               &gVcapModuleContext.videoStatus,
               sizeof(*pStatus));
    }

    return status;
}


Int32 Vcap_setVideoSourceStatus(VCAP_VIDEO_SOURCE_STATUS_S *pStatus)
{

    if(pStatus != NULL)
    {
        memcpy(&gVcapModuleContext.videoStatus,
               pStatus,
               sizeof(gVcapModuleContext.videoStatus));

        /* Assumption here is height width of video remains same across channels */
        if(pStatus->chStatus[0].frameHeight == 288)
            gVcapModuleContext.isPalMode = TRUE;

    }

    return 0;
}

Int32 Vcap_configVideoDecoder(VCAP_DEVICE_CREATE_PARAM_S * deviceCreatePrm, UInt32 numDevices)
{
    Int32 status = 0;
    if(gVcapModuleContext.vcapConfig.enableConfigExtVideoDecoder)
    {
        Vcap_deviceCreate(deviceCreatePrm, numDevices);
    }

    return status;
}


Int32 Vcap_deleteVideoDecoder()
{
    return Vcap_deviceDelete();
}

Int32 Vcap_setColor(Int32 contrast, Int32 brightness, Int32 saturation, Int32 hue, Int32 chId)
{
    if(gVcapModuleContext.vcapConfig.enableConfigExtVideoDecoder)
    {
        return Vcap_deviceSetColor(contrast, brightness, saturation, hue, chId);
    }
    return 0;
}

Bool Vcap_isPalMode()
{
#ifdef  USE_CAMERA
    return 1;
#else
    return 0;
#endif

    if(gVcapModuleContext.vcapConfig.enableConfigExtVideoDecoder)
    {
        return Vcap_deviceIsPalMode();
    }
    else
    {
        return (gVcapModuleContext.isPalMode);
    }
}

Int32 Vcap_detectVideoInfo()
{
    Int32 i, status = 0;

    if(gVcapModuleContext.vcapConfig.enableConfigExtVideoDecoder)
    {
        status = Vcap_deviceGetSourceStatus(&gVcapModuleContext.videoStatus);
    }

    for(i=0; i< gVcapModuleContext.videoStatus.numChannels;i++)
    {
        if(gVcapModuleContext.videoStatus.chStatus[i].isVideoDetect == TRUE)
        {
            if(gVcapModuleContext.videoStatus.chStatus[i].frameHeight == 288)
            {
                return VSYS_STD_PAL;
            }
            else if(gVcapModuleContext.videoStatus.chStatus[i].frameHeight == 240)
            {
                return VSYS_STD_NTSC;
            }
            break;

        }

    }

    return VSYS_STD_NTSC;

}


Int32 Vcap_setWrbkCaptScParamsDefault(UInt32 inW, UInt32 inH, UInt32 outW, UInt32 outH)
{
    int status = 0;
    CaptureLink_ScParams scPrms;

    scPrms.queId = 1;
    scPrms.chId = 0;
    scPrms.scEnable = TRUE;
    scPrms.inWidth = inW;
    scPrms.inHeight = inH;
    scPrms.outWidth = outW;
    scPrms.outHeight = outH;

    if ((gVcapModuleContext.captureId != SYSTEM_LINK_ID_INVALID) &&
        (TRUE == gVcapModuleContext.isWrbkCaptEnable))
    {
        status = System_linkControl(
                    gVcapModuleContext.captureId,
                    CAPTURE_LINK_CMD_SET_SC_PARAMS,
                    &scPrms,
                    sizeof(scPrms),
                    TRUE);
    }

    return (status);
}

Int32 Vcap_setWrbkCaptScParams(UInt32 inW, UInt32 inH, UInt32 outW, UInt32 outH)
{
    OSA_assert(gVcapModuleContext.setWrbkCaptScParamsFxn != NULL);
    return gVcapModuleContext.setWrbkCaptScParamsFxn( inW, inH,outW, outH);
}

/**
    \brief This API starts the Write back capture driver

    This API is used to start write back capture driver.
    Write back capture is tied with the display and it could
    have scalar in the path. Input and output resolution parameter
    are used for configuring scarar.

    \param inResolution    [IN] Input resolution to the scalar
    \param outResolution   [IN] Output Resolution from Scalar

*/
static
Int32 Vcap_startWrbkCaptDefault()
{
    int status = 0;
    CaptureLink_CtrlWrbkCapt wrbkCapt;

    /* Assumption Write back capture is on Queue ID 1 and there is only one channel */
    wrbkCapt.queId = 1;
    wrbkCapt.chId = 0;
    wrbkCapt.enable = TRUE;

    if ((gVcapModuleContext.captureId != SYSTEM_LINK_ID_INVALID) &&
        (TRUE == gVcapModuleContext.isWrbkCaptEnable))
    {
        status = System_linkControl(
                    gVcapModuleContext.captureId,
                    CAPTURE_LINK_CMD_ENABLE_WRBK_CAPT,
                    &wrbkCapt,
                    sizeof(wrbkCapt),
                    TRUE);
    }

    return (status);
}

Int32 Vcap_startWrbkCapt()
{
    OSA_assert(gVcapModuleContext.startWrbkCaptFxn != NULL);
    return gVcapModuleContext.startWrbkCaptFxn();
}


/**
    \brief This API is used to stop the write back capture driver.
*/
static
Int32 Vcap_stopWrbkCaptDefault()
{
    int status = 0;
    CaptureLink_CtrlWrbkCapt wrbkCapt;

    /* Assumption Write back capture is on Queue ID 1 and there is only one channel */
    wrbkCapt.queId = 1;
    wrbkCapt.chId = 0;
    wrbkCapt.enable = FALSE;

    if ((gVcapModuleContext.captureId != SYSTEM_LINK_ID_INVALID) &&
        (TRUE == gVcapModuleContext.isWrbkCaptEnable))
    {
        status = System_linkControl(
                    gVcapModuleContext.captureId,
                    CAPTURE_LINK_CMD_ENABLE_WRBK_CAPT,
                    &wrbkCapt,
                    sizeof(wrbkCapt),
                    TRUE);
    }

    return (status);
}

Int32 Vcap_stopWrbkCapt()
{
    OSA_assert(gVcapModuleContext.stopWrbkCaptFxn != NULL);
    return gVcapModuleContext.stopWrbkCaptFxn();
}

