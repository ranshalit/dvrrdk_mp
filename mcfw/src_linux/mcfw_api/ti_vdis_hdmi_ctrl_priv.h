/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/


#ifndef _DEMO_HDMI_CTRL_H_
#define _DEMO_HDMI_CTRL_H_

#include <stdio.h>
#include <sys/types.h>


/* State of HDMI controller */
#define DEMO_HDMI_CTRL_NO_SINK          (0)
#define DEMO_HDMI_CTRL_SINK_CONNECTED   (1)
#define DEMO_HDMI_CTRL_NOT_STREAMING    (2)
#define DEMO_HDMI_CTRL_STREAMING        (3)

/* HDMI Audio Sampling rates */
#define DEMO_HDMI_32KHZ_SAMPLE_RATE         (1)
#define DEMO_HDMI_44_1KHZ_SAMPLE_RATE       (2)
#define DEMO_HDMI_48KHZ_SAMPLE_RATE         (4)
#define DEMO_HDMI_96KHZ_SAMPLE_RATE         (16)
#define DEMO_HDMI_192KHZ_SAMPLE_RATE        (64)

/* Period between HPD poll - period between poll for sink connect/disconnect.
    Units is in milliseconds */
#define DEMO_HDMI_CTRL_HPD_POLL_INTERVAL    (1000)

/* Initializes the HDMI control thread and monitors for TV plug out & plug in.
    Reads the EDID, parse it */
int Vdis_hdmiCtrl_init(UInt32 currResolution);
/* Exists the HDMI control thread */
int Vdis_hdmiCtrl_deInit(void);

/* Stop the HDMI transmitter */
int Vdis_hdmiCtrl_stop(void);
/* Start the HDMI transmitter */
int Vdis_hdmiCtrl_start(UInt32 currResolution);
/* To be used only for DEBUG */
void Vdis_hdmiCtrl_printDetectedDetails(void);
#endif /* _DEMO_HDMI_CTRL_H_ */
