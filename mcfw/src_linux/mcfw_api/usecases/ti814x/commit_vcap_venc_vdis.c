/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/
/* ranran new file */
/*
                    Capture (YUV422I) 1CH 720P 60fps
                      |
                      |
                      |            
        DEIH (DEI-SC YUV422I )     
                      |           
                      |
                      |
                     DUP
                     | |
     +---------  ----  NSF (YUV420)-----IPC-OUT(VPSS)---IPC-IN(VIDEO-M3)---ENC--IPCBITS-OUT(M3)---IPCBITS-IN(Host)
     |                                      
     |                                       
  SW Mosaic          
 (SC5 YUV422I)  
     |                          
On-Chip HDMI    
  1080p60         
*/

#include "mcfw/src_linux/mcfw_api/usecases/multich_common.h"
#include "mcfw/src_linux/mcfw_api/usecases/multich_ipcbits.h"
#include "mcfw/src_linux/mcfw_api/ti_vdis_priv.h"
#include "mcfw/src_linux/devices/tw2968/src/tw2968_priv.h"
#include "mcfw/src_linux/devices/tvp5158/src/tvp5158_priv.h"
#include "mcfw/interfaces/link_api/system_tiler.h"
#include "mcfw/interfaces/link_api/avsync_hlos.h"
#include "mcfw/interfaces/ti_vdis_timings.h"


/* ranran #define ENABLE_FAKE_HD_MODE */


/* =============================================================================
 * Use case code
 * =============================================================================
 */


static
Void MultiCh_singlewin_layoutprm(SwMsLink_LayoutPrm *layoutInfo,VSYS_VIDEO_STANDARD_E outRes)
{
    SwMsLink_LayoutWinInfo *winInfo;
    UInt32 outWidth, outHeight, winId, widthAlign, heightAlign;

    MultiCh_swMsGetOutSize(outRes, &outWidth, &outHeight);
    widthAlign = 8;
    heightAlign = 1;

    /* init to known default */
    memset(layoutInfo, 0, sizeof(*layoutInfo));

    layoutInfo->onlyCh2WinMapChanged = FALSE;
    layoutInfo->outputFPS = 60;
    layoutInfo->numWin = 1;
    winId = 0;

    winInfo = &layoutInfo->winInfo[winId];

    winInfo->width  = SystemUtils_floor(outWidth, widthAlign);
    winInfo->height = SystemUtils_floor(outHeight, heightAlign);
    winInfo->startX = 0;
    winInfo->startY = 0;
    winInfo->bypass = FALSE;
 	winInfo->channelNum = 0;
}

#define MAX_NUM_CAPTURE_DEVICES     (4)
VCAP_DEVICE_CREATE_PARAM_S        vidDecVideoModeArgs[MAX_NUM_CAPTURE_DEVICES];

#ifdef ENABLE_FAKE_HD_MODE
static
Void multich_hddvr_configure_extvideodecoder_prm()
{
    int i;
    VCAP_VIDEO_SOURCE_STATUS_S vidSourceStatus;
    UInt32 deviceId;
    UInt32 numCaptureDevices;

    deviceId = DEVICE_VID_DEC_TVP5158_DRV;

    if (deviceId == DEVICE_VID_DEC_TW2968_DRV)
    {
        numCaptureDevices = 2;
    }
    else
    {
        numCaptureDevices = 4;
    }


    for(i = 0; i < numCaptureDevices; i++)
    {
        vidDecVideoModeArgs[i].deviceId         = deviceId;
        if(deviceId == DEVICE_VID_DEC_TW2968_DRV)
        {
            /* consider first device connected to VIP0 Port A and second device connected to VIP1 PortA */
            vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i*2;
            vidDecVideoModeArgs[i].numChInDevice    = 1;
        }
        else
        {
            /* consider first device connected to VIP0 Port A and second device connected to VIP1 PortA */
            vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i;
            vidDecVideoModeArgs[i].numChInDevice    = 1;
        }
        vidDecVideoModeArgs[i].modeParams.videoIfMode        = DEVICE_CAPT_VIDEO_IF_MODE_8BIT;
        vidDecVideoModeArgs[i].modeParams.videoDataFormat    = SYSTEM_DF_YUV422P;
        vidDecVideoModeArgs[i].modeParams.standard           = SYSTEM_STD_D1;
        vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
                    DEVICE_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_EMBEDDED_SYNC;
        vidDecVideoModeArgs[i].modeParams.videoSystem        =
                 DEVICE_VIDEO_DECODER_VIDEO_SYSTEM_AUTO_DETECT;
        vidDecVideoModeArgs[i].modeParams.videoCropEnable    = FALSE;
        vidDecVideoModeArgs[i].modeParams.videoAutoDetectTimeout = -1;
    }
    Vcap_configVideoDecoder(&vidDecVideoModeArgs[0],
                            numCaptureDevices);
    Vcap_getVideoSourceStatus(&vidSourceStatus);
    Vcap_setVideoSourceStatus(&vidSourceStatus);
}
#endif

#define     NUM_CAPTURE_DEVICES          1

Void CommitCh_createHdDvr()
{
    CaptureLink_CreateParams    capturePrm;
    DeiLink_CreateParams        deiPrm;
    DupLink_CreateParams        dupPrm; /* ranran , dupPrm1; */
    SwMsLink_CreateParams       swMsPrm;
    DisplayLink_CreateParams    displayPrm;
    NsfLink_CreateParams        nsfPrm;

    CaptureLink_VipInstParams *pCaptureInstPrm;
    CaptureLink_OutParams     *pCaptureOutPrm;

    EncLink_ChCreateParams *pLinkChPrm;
    EncLink_ChDynamicParams *pLinkDynPrm;
    VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
    VENC_CHN_PARAMS_S *pChPrm;
    IpcLink_CreateParams            ipcOutVpssPrm;
    IpcLink_CreateParams            ipcInVideoPrm;
    EncLink_CreateParams            encPrm;
    IpcBitsOutLinkRTOS_CreateParams ipcBitsOutVideoPrm;
    IpcBitsInLinkHLOS_CreateParams  ipcBitsInHostPrm;
   /* MergeLink_CreateParams      mergePrm; ranran */
    
    UInt32 grpxId[VDIS_DEV_MAX];
    UInt32 dupId;
    /*ranran UInt32 mergeId, dupId1;*/
    UInt32 deiOutQue;
    UInt32 ipcOutVpssId;
    UInt32 ipcInVideoId;
    UInt32 chId;
#ifndef ELBIT_BOARD
    UInt32 i;
#endif
    MULTICH_INIT_STRUCT(CaptureLink_CreateParams ,capturePrm);
    MULTICH_INIT_STRUCT(DeiLink_CreateParams ,deiPrm);
    MULTICH_INIT_STRUCT(NsfLink_CreateParams ,nsfPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams ,ipcInVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams,ipcBitsOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,ipcBitsInHostPrm);
    MULTICH_INIT_STRUCT(DisplayLink_CreateParams,displayPrm);
    MULTICH_INIT_STRUCT(SwMsLink_CreateParams ,swMsPrm);
    MULTICH_INIT_STRUCT(EncLink_CreateParams, encPrm);



    MultiCh_detectBoard();

    System_linkControl(
        SYSTEM_LINK_ID_M3VPSS,
        SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES,
        NULL,
        0,
        TRUE
        );
    //ranran
#ifdef USE_CAMERA
    gVcapModuleContext.isPalMode = TRUE;
#else
    gVcapModuleContext.isPalMode = FALSE;
#endif

    capturePrm.isPalMode = Vcap_isPalMode();
  //  printf("ranran pal mode = %d (0-ntsc, 1 - pal)\n", capturePrm.isPalMode);
    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_CAPTURE;
    gVcapModuleContext.nsfId[0]     = SYSTEM_LINK_ID_NSF_0;
    gVcapModuleContext.deiId[0]     = SYSTEM_LINK_ID_DEI_0;
    dupId                           = SYSTEM_VPSS_LINK_ID_DUP_0;
   /* ranran dupId1                          = SYSTEM_VPSS_LINK_ID_DUP_1;
    mergeId                         = SYSTEM_VPSS_LINK_ID_MERGE_0; */

    gVdisModuleContext.swMsId[0]    = SYSTEM_LINK_ID_SW_MS_MULTI_INST_0;
    swMsPrm.numSwMsInst             = 1;
    swMsPrm.swMsInstId[0]           = SYSTEM_SW_MS_SC_INST_SC5; /* ranran was SYSTEM_SW_MS_SC_INST_DEI_SC */

    gVdisModuleContext.displayId[0] = SYSTEM_LINK_ID_DISPLAY_0; // ON CHIP HDMI
    grpxId[0]                       = SYSTEM_LINK_ID_GRPX_0;

    ipcOutVpssId                    = SYSTEM_VPSS_LINK_ID_IPC_OUT_M3_0;
    ipcInVideoId                    = SYSTEM_VIDEO_LINK_ID_IPC_IN_M3_0;
    gVencModuleContext.encId                    = SYSTEM_LINK_ID_VENC_0;
    gVencModuleContext.ipcBitsInHLOSId          = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;
    gVencModuleContext.ipcBitsOutRTOSId         = SYSTEM_VIDEO_LINK_ID_IPC_BITS_OUT_0;

    capturePrm.numVipInst    = 1;
    capturePrm.outQueParams[0].nextLink = gVcapModuleContext.deiId[0];

    capturePrm.tilerEnable              = FALSE;
    capturePrm.enableSdCrop             = FALSE;

#ifdef ENABLE_FAKE_HD_MODE
    capturePrm.fakeHdMode                  = TRUE;
#else
    capturePrm.fakeHdMode                  = FALSE;
#endif


    pCaptureInstPrm                     = &capturePrm.vipInst[0];
    //ranran
#ifndef ELBIT_BOARD
    pCaptureInstPrm->vipInstId          = SYSTEM_CAPTURE_INST_VIP0_PORTA;
#else
  //  printf("ranran xxxx\n");
#ifdef NEW_PORT
    pCaptureInstPrm->vipInstId          = SYSTEM_CAPTURE_INST_VIP0_PORTA;
#else

    pCaptureInstPrm->vipInstId          = SYSTEM_CAPTURE_INST_VIP1_PORTA;
#endif
#endif
    pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];

#ifdef ENABLE_FAKE_HD_MODE
    gVcapModuleContext.isWrbkCaptEnable = TRUE;
    pCaptureInstPrm->enableTimestampInInterrupt = TRUE;
    pCaptureInstPrm->vipInstIdnsf          = SYSTEM_CAPTURE_INST_SC5_WB2;
    pCaptureInstPrm->videoDecoderId     = 0;
    pCaptureInstPrm->inScanFormat       = SYSTEM_SF_PROGRESSIVE;
    pCaptureInstPrm->inDataFormat       = SYSTEM_DF_RGB24_888;
    pCaptureInstPrm->standard           = SYSTEM_STD_1080P_60;

    pCaptureOutPrm->scOutWidth          = 1920;
    pCaptureOutPrm->scOutHeight         = 1080;
#else
    pCaptureInstPrm->inScanFormat       = SYSTEM_SF_INTERLACED;
    pCaptureInstPrm->videoDecoderId     = SYSTEM_DEVICE_VID_DEC_TVP5158_DRV; /* ranran set to tvp5158 */ /* DONT set it to TVP5158 */
    pCaptureInstPrm->standard           = SYSTEM_STD_D1; /* ranran changed from 1080I */
    pCaptureInstPrm->inDataFormat       = SYSTEM_DF_YUV422P;
    pCaptureOutPrm->scOutWidth          = 0;
    pCaptureOutPrm->scOutHeight         = 0;
#endif

    pCaptureInstPrm->videoCaptureMode   = DEVICE_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_EMBEDDED_SYNC;
    pCaptureInstPrm->videoIfMode        = SYSTEM_CAPT_VIDEO_IF_MODE_8BIT; /* ranran changed from 16 */
    pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV422I_YUYV;

    pCaptureInstPrm->numOutput          = 1;

    pCaptureOutPrm->scEnable            = FALSE;
    pCaptureOutPrm->outQueId            = 0;

/* use ELBIT_BOARD when required to work with EVM instead of readl board.
 * put define in component.mk
 */
#ifndef ELBIT_BOARD
/* ranran added *******************************/
    for(i = 0; i < NUM_CAPTURE_DEVICES; i++)
    {
        vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i;
        vidDecVideoModeArgs[i].deviceId         = DEVICE_VID_DEC_TVP5158_DRV;
        vidDecVideoModeArgs[i].numChInDevice    = 4;

        vidDecVideoModeArgs[i].modeParams.videoIfMode        = DEVICE_CAPT_VIDEO_IF_MODE_8BIT;
        vidDecVideoModeArgs[i].modeParams.videoDataFormat    = SYSTEM_DF_YUV422P;
        vidDecVideoModeArgs[i].modeParams.standard           = SYSTEM_STD_D1;
        vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
        		DEVICE_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_EMBEDDED_SYNC;
        vidDecVideoModeArgs[i].modeParams.videoSystem        =
                                      DEVICE_VIDEO_DECODER_VIDEO_SYSTEM_AUTO_DETECT;
        vidDecVideoModeArgs[i].modeParams.videoCropEnable    = FALSE;
        vidDecVideoModeArgs[i].modeParams.videoAutoDetectTimeout = -1;
    }


    /* ranran added *******************************/
    Vcap_configVideoDecoder(vidDecVideoModeArgs, NUM_CAPTURE_DEVICES);
/*#ifdef ENABLE_FAKE_HD_MODE
    multich_hddvr_configure_extvideodecoder_prm();
#else*/
#else
   // printf("ranran gVcapModuleContext.vcapConfig.enableConfigExtVideoDecoder is FALSE\n");
    gVcapModuleContext.vcapConfig.enableConfigExtVideoDecoder = FALSE;
#endif


    deiOutQue                = DEI_LINK_OUT_QUE_DEI_SC;
    deiPrm.inQueParams.prevLinkId                        = gVcapModuleContext.captureId;
    deiPrm.inQueParams.prevLinkQueId                     = 0;
    deiPrm.outQueParams[deiOutQue].nextLink              = dupId;
    deiPrm.enableOut[deiOutQue]                          = TRUE;
    deiPrm.tilerEnable[deiOutQue]                        = FALSE;

    deiPrm.comprEnable                                   = FALSE;
    deiPrm.setVipScYuv422Format                          = FALSE;

    deiPrm.enableDeiForceBypass                          = FALSE; /* ranran */
//    deiPrm.interlacedBypassMode                          = TRUE;
#ifdef USE_CAMERA
    deiPrm.inputFrameRate[deiOutQue] = 50;/*ranran was 60 */
    deiPrm.outputFrameRate[deiOutQue] = 25;
#else
    deiPrm.inputFrameRate[deiOutQue] = 60;/*ranran was 60 */
    deiPrm.outputFrameRate[deiOutQue] = 30;
#endif
    /* VIP Scalar ratio is 1:1 */
    deiPrm.outScaleFactor[deiOutQue][0].scaleMode = DEI_SCALE_MODE_RATIO;
    deiPrm.outScaleFactor[deiOutQue][0].ratio.widthRatio.numerator    = 1;
    deiPrm.outScaleFactor[deiOutQue][0].ratio.widthRatio.denominator  = 1;
    deiPrm.outScaleFactor[deiOutQue][0].ratio.heightRatio.numerator   = 1;
    deiPrm.outScaleFactor[deiOutQue][0].ratio.heightRatio.denominator     = 1;
    for (chId=1; chId < DEI_LINK_MAX_CH; chId++)
        deiPrm.outScaleFactor[deiOutQue][chId] = deiPrm.outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0];

    dupPrm.inQueParams.prevLinkId         = gVcapModuleContext.deiId[0];
    dupPrm.inQueParams.prevLinkQueId      = deiOutQue;
    dupPrm.numOutQue                      = 2;
    dupPrm.outQueParams[0].nextLink       = gVdisModuleContext.swMsId[0];
    dupPrm.outQueParams[1].nextLink       = gVcapModuleContext.nsfId[0];
    dupPrm.notifyNextLink                 = TRUE;

    nsfPrm.bypassNsf                 = TRUE; /* ranran was false */
    nsfPrm.tilerEnable               = FALSE;
    nsfPrm.inQueParams.prevLinkId    = dupId;
    nsfPrm.inQueParams.prevLinkQueId = 1;
    nsfPrm.numOutQue                 = 1;
    nsfPrm.outQueParams[0].nextLink  = ipcOutVpssId; /* ranran gVdisModuleContext.swMsId[0]; */
    /*ranran

    dupPrm1.inQueParams.prevLinkId         = gVcapModuleContext.nsfId[0];
    dupPrm1.inQueParams.prevLinkQueId      = 0;
    dupPrm1.numOutQue                      = 2;
    dupPrm1.outQueParams[0].nextLink       = mergeId;
    dupPrm1.outQueParams[1].nextLink       = mergeId;
    dupPrm1.notifyNextLink                 = TRUE;

    mergePrm.numInQue                     = 2;
    mergePrm.inQueParams[0].prevLinkId    = dupId1;
    mergePrm.inQueParams[0].prevLinkQueId = 0;
    mergePrm.inQueParams[1].prevLinkId    = dupId1;
    mergePrm.inQueParams[1].prevLinkQueId = 1;
    mergePrm.outQueParams.nextLink        = ipcOutVpssId;
    mergePrm.notifyNextLink               = TRUE;
*/
    swMsPrm.inQueParams.prevLinkId    = dupId;
    swMsPrm.inQueParams.prevLinkQueId = 0;
    swMsPrm.outQueParams.nextLink     = gVdisModuleContext.displayId[0];
    swMsPrm.maxInputQueLen            = SYSTEM_SW_MS_DEFAULT_INPUT_QUE_LEN;
    swMsPrm.maxOutRes                 = VSYS_STD_1080P_60;
    swMsPrm.initOutRes                = VSYS_STD_1080P_60;
    swMsPrm.lineSkipMode = FALSE;

    swMsPrm.enableLayoutGridDraw = gVdisModuleContext.vdisConfig.enableLayoutGridDraw;

    MultiCh_singlewin_layoutprm(&swMsPrm.layoutPrm,swMsPrm.initOutRes);

    displayPrm.inQueParams[0].prevLinkId    = gVdisModuleContext.swMsId[0];
    displayPrm.inQueParams[0].prevLinkQueId = 0;
    displayPrm.displayRes                   = swMsPrm.initOutRes;

    ipcOutVpssPrm.inQueParams.prevLinkId    = gVcapModuleContext.nsfId[0];
    ipcOutVpssPrm.inQueParams.prevLinkQueId = 0;
    ipcOutVpssPrm.numOutQue                 = 1;
    ipcOutVpssPrm.outQueParams[0].nextLink  = ipcInVideoId;
    ipcOutVpssPrm.notifyNextLink            = TRUE; /* ranran was FALSE; */
    ipcOutVpssPrm.notifyPrevLink            = TRUE;
    ipcOutVpssPrm.noNotifyMode              = FALSE; /* ranran was TRUE; */

    ipcInVideoPrm.inQueParams.prevLinkId    = ipcOutVpssId;
    ipcInVideoPrm.inQueParams.prevLinkQueId = 0;
    ipcInVideoPrm.numOutQue                 = 1;
    ipcInVideoPrm.outQueParams[0].nextLink  = gVencModuleContext.encId;
    ipcInVideoPrm.notifyNextLink            = TRUE;
    ipcInVideoPrm.notifyPrevLink            = TRUE; /* ranran was FALSE; */
    ipcInVideoPrm.noNotifyMode              = FALSE; /* was TRUE; */


    ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.encId;
    ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsOutVideoPrm.baseCreateParams.numOutQue                 = 1;
    ipcBitsOutVideoPrm.baseCreateParams.outQueParams[0].nextLink   = gVencModuleContext.ipcBitsInHLOSId;
    MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&ipcBitsOutVideoPrm, TRUE);

    ipcBitsInHostPrm.baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.ipcBitsOutRTOSId;
    ipcBitsInHostPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsInHostPrm.baseCreateParams.numOutQue                 = 1;
    ipcBitsInHostPrm.baseCreateParams.outQueParams[0].nextLink  = SYSTEM_LINK_ID_INVALID;
    MultiCh_ipcBitsInitCreateParams_BitsInHLOS(&ipcBitsInHostPrm);
    EncLink_CreateParams_Init(&encPrm); /* ranran added */
    encPrm.inQueParams.prevLinkId   = ipcInVideoId;
    encPrm.inQueParams.prevLinkQueId= 0;
    encPrm.outQueParams.nextLink    = gVencModuleContext.ipcBitsOutRTOSId;

    pLinkChPrm  = &encPrm.chCreateParams[0];
    pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

    pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[0];
    pDynPrm     = &pChPrm->dynamicParam;

    encPrm.numBufPerCh[0] = 6;
    encPrm.numBufPerCh[1] = 6;
    encPrm.numBufPerCh[2] = 6;

    pLinkChPrm->format                  = IVIDEO_H264HP;
    pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[0];
    pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
    pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
    pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
    pLinkChPrm->enableWaterMarking      = pChPrm->enableWaterMarking;
    pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
    pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
    pLinkChPrm->rateControlPreset       = pChPrm->rcType;
    pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;
    pLinkChPrm->numTemporalLayer        = pChPrm->numTemporalLayer;

    pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
    pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
    pLinkDynPrm->interFrameInterval     = 1;
    pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
    pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
    pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
    pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
    pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
    pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
    pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
    pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;


    /* same params for 2nd channel */
    encPrm.chCreateParams[1] = encPrm.chCreateParams[0];
 
    System_linkCreate (gVcapModuleContext.captureId, &capturePrm, sizeof(capturePrm));
#ifdef ENABLE_FAKE_HD_MODE
    {
        CaptureLink_SkipOddFields captureSkipOddFields;

        captureSkipOddFields.queId = 0;
        captureSkipOddFields.skipOddFieldsChBitMask = 0xFFFFFFFF; /* all CHs */
        captureSkipOddFields.oddFieldSkipRatio      = CAPTURE_LINK_ODD_FIELD_SKIP_ALL;

        System_linkControl(
            gVcapModuleContext.captureId,
            CAPTURE_LINK_CMD_SKIP_ODD_FIELDS,
            &captureSkipOddFields,
            sizeof(captureSkipOddFields),
            TRUE);
    }
#endif

    System_linkCreate(gVcapModuleContext.deiId[0]  , &deiPrm, sizeof(deiPrm));
    System_linkCreate(dupId, &dupPrm, sizeof(dupPrm));

    System_linkCreate(gVcapModuleContext.nsfId[0] , &nsfPrm, sizeof(nsfPrm));
/*
 ranran
     System_linkCreate(dupId1, &dupPrm1, sizeof(dupPrm1));
    System_linkCreate(mergeId   , &mergePrm  , sizeof(mergePrm));
*/
    System_linkCreate(ipcOutVpssId , &ipcOutVpssPrm , sizeof(ipcOutVpssPrm) );
    System_linkCreate(ipcInVideoId , &ipcInVideoPrm , sizeof(ipcInVideoPrm) );

    System_linkCreate(gVencModuleContext.encId, &encPrm, sizeof(encPrm));
    System_linkCreate(gVencModuleContext.ipcBitsOutRTOSId, &ipcBitsOutVideoPrm, sizeof(ipcBitsOutVideoPrm));
    System_linkCreate(gVencModuleContext.ipcBitsInHLOSId, &ipcBitsInHostPrm, sizeof(ipcBitsInHostPrm));

    System_linkCreate(gVdisModuleContext.swMsId[0]  , &swMsPrm, sizeof(swMsPrm));

    System_linkCreate(gVdisModuleContext.displayId[0], &displayPrm, sizeof(displayPrm));

    MultiCh_memPrintHeapStatus();

    Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX0, VDIS_OFF);
    Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX1, VDIS_OFF);
}



Void CommitCh_deleteHdDvr()
{
    UInt32 grpxId[VDIS_DEV_MAX];
    UInt32 dupId; /* ranran , dupId1, mergeId;*/
    UInt32 ipcOutVpssId;
    UInt32 ipcInVideoId;



    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_CAPTURE;
    gVcapModuleContext.nsfId[0]     = SYSTEM_LINK_ID_NSF_0;
    gVcapModuleContext.deiId[0]     = SYSTEM_LINK_ID_DEI_0;
    dupId                           = SYSTEM_VPSS_LINK_ID_DUP_0;
   /*ranran dupId1                          = SYSTEM_VPSS_LINK_ID_DUP_1;
    mergeId                         = SYSTEM_VPSS_LINK_ID_MERGE_0;*/

    gVdisModuleContext.swMsId[0]    = SYSTEM_LINK_ID_SW_MS_MULTI_INST_0;

    gVdisModuleContext.displayId[0] = SYSTEM_LINK_ID_DISPLAY_0; // ON CHIP HDMI
    grpxId[0]                       = SYSTEM_LINK_ID_GRPX_0;

    ipcOutVpssId       = SYSTEM_VPSS_LINK_ID_IPC_OUT_M3_0;
    ipcInVideoId       = SYSTEM_VIDEO_LINK_ID_IPC_IN_M3_0;
    gVencModuleContext.encId                    = SYSTEM_LINK_ID_VENC_0;
    gVencModuleContext.ipcBitsInHLOSId          = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;
    gVencModuleContext.ipcBitsOutRTOSId         = SYSTEM_VIDEO_LINK_ID_IPC_BITS_OUT_0;

    System_linkDelete(gVcapModuleContext.captureId);

    System_linkDelete(gVcapModuleContext.deiId[0]);

    System_linkDelete(dupId);
   /*ranran System_linkDelete(dupId1);
    System_linkDelete(mergeId);*/

    System_linkDelete(gVdisModuleContext.swMsId[0] );

    System_linkDelete(gVdisModuleContext.displayId[0]);


    System_linkDelete(gVcapModuleContext.nsfId[0]);
    System_linkDelete(ipcOutVpssId);
    System_linkDelete(ipcInVideoId);
    System_linkDelete(gVencModuleContext.encId);
    System_linkDelete(gVencModuleContext.ipcBitsInHLOSId);
    System_linkDelete(gVencModuleContext.ipcBitsOutRTOSId);

    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    MultiCh_prfLoadCalcEnable(FALSE, TRUE, FALSE);

}


/* Dummy functions */
Int32 CommitCh_hdDvrSetCapDynamicParamChn(VCAP_CHN vcChnId, VCAP_CHN_DYNAMIC_PARAM_S *psCapChnDynaParam, VCAP_PARAMS_E paramId)
{
    vcChnId = vcChnId;
    psCapChnDynaParam = psCapChnDynaParam;
    paramId = paramId;
    return 0;
}
Int32 CommitCh_hdDvrGetCapDynamicParamChn(VCAP_CHN vcChnId, VCAP_CHN_DYNAMIC_PARAM_S *psCapChnDynaParam, VCAP_PARAMS_E paramId)
{
    vcChnId = vcChnId;
    psCapChnDynaParam = psCapChnDynaParam;
    paramId = paramId;
    return 0;
}

Int32 CommitCh_hdDvrSetCapFrameRate(VCAP_CHN vcChnId, VCAP_STRM vcStrmId, Int32 inputFrameRate, Int32 outputFrameRate)
{
    vcChnId = vcChnId;
    vcStrmId = vcStrmId;
    inputFrameRate = outputFrameRate;
    return 0;
}

Int32 CommitCh_hdDvrEnableDisableCapChn(VCAP_CHN vcChnId, VCAP_STRM vcStrmId, Bool enableChn)
{
    vcChnId = vcChnId;
    vcStrmId = vcStrmId;
    enableChn = enableChn;
    return 0;
}
void  CommitCh_hdDvrSwmsChReMap(VDIS_MOSAIC_S *psVdMosaicParam)
{
    psVdMosaicParam = psVdMosaicParam;
}


