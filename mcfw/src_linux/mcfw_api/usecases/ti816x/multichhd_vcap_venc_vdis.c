/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include "mcfw/src_linux/mcfw_api/usecases/multich_common.h"
#include "mcfw/src_linux/mcfw_api/usecases/multich_ipcbits.h"
#include "mcfw/src_linux/mcfw_api/ti_vdis_priv.h"
#include "mcfw/src_linux/devices/tw2968/src/tw2968_priv.h"
#include "mcfw/src_linux/devices/tvp5158/src/tvp5158_priv.h"
#include "mcfw/interfaces/link_api/system_tiler.h"
#include "mcfw/interfaces/link_api/avsync_hlos.h"
#include "mcfw/interfaces/ti_vdis_timings.h"

/*

All DEI in bypass mode

DUP0 - DUP_FULL_RES_CAPTURE   MERGE0 - MERGE_Q_RES_CAPTURE      NSF0 - NSF_MJPEG
DUP1 - DUP_Q_RES_CAPTURE      MERGE1 - MERGE_DISPLAY            NSF1 - NSF_Q_RES_CAPTURE
DUP2 - DUP_DISPLAY            MERGE2 - MERGE_ENCODE             NSF2 - NSF_SCD

SELECT - SELECT_FULL_RES_CAPTURE

SWMS
CH 0, 1, 2, 3   - LOW RES LIVE CHs    - 540p30
CH 4, 5, 6, 7   - PLAYBACK CHs        - 1080p30
CH 8, 9, 10, 11 - HIGH RES LIVE CHs   - 1080p30

SWMS0 - HDMI
- Supports
    - 1x1 LIVE      - HIGH RES LIVE CH
    - 2x2 LIVE      - LOW RES LIVE CH
    - 1x1, 2x2 PLAYBACK
    - Other layouts not supported
    - LIVE+PLAY mix of CHs not supported, playback CHs not shown in this case

SWMS1 - SDTV
- Supports
    - 1x1 LIVE      - LOW RES LIVE CH
    - 2x2 LIVE      - LOW RES LIVE CH
    - 1x1 PLAYBACK
    - Other layouts not supported
    - LIVE+PLAY mix of CHs not supported, playback CHs not shown in this case

                                                                             Capture
                                                                                |
                                                                                |  4CH 1080p30
                                                                                |  YUV422
                                                                                |
                                                                              DUP0
                                                                              |  | |
                                                (for live preview)            |  | +---------------------------------------------+
                                    +-----------------------------------------+  +-----+                                         |
                                    |                                                  |                                        NSF0
                                    |                                                 SELECT                                     |  4CH 1080p 1fps
                                    |                                                  |                                         |   YUV420
                                    |                                             +------------------+                           |
    IPC BITS OUT (A8)               |                                 2CH 1080p30 |                  | 2CH 1080p30               |
        |                           |                                  YUV422     |                  |  YUV422                   |
    IPC BITS IN  (Video M3)         |                                             |                  |                           |
        |                           |                                           DEIHQ0               DEI0                        |
    Decode (1CH 1080p30 h264)       |                                           |     |             |     |                      |
        |                           |                                           |     |2CH 540p30   |     | 2CH 540p30           |
    IPC M3 OUT (Video M3)           |                                           |     | YUV422      |     |  YUV422              |
        |                           |                                         (VIP-SC)|           (VIP-SC)|                      |
    IPC M3 IN  (VPSS M3)            |                                           |     |             |     |                      |
        |                           |                                           |     |             |     |                      |
        |                           |                                           |    (DEI-SC)       |    (DEI-SC)                |(for MJPEG)
        |                           |                                           |     |             |     |                      |
        |                           |                          +----------------]-----+             |     |                      |
        |                           |                          |                |                   |     |                      |
        |                           |                          | +--------------]-------------------]-----+                      |
        |                           |                          | |              |                   |                            |
        |                           |                          | |              |                   |                            |
        |                           |                          | |              |                   |                            |
        |                           |                          | |   2CH 1080p30|                   | 2CH 1080p30                |
        |                           |                          | |     YUV420   |                   |  YUV420                    |
        |                           |                          MERGE0           |                   |                            |
        |                           |                           |               |                   |                            |
        |                           |                          NSF1             |                   |                            |
        +-------------------------+ |                           |  4CH 540p30   |                   +----------+   +-------------+
            (for display)         | |                           |   YUV420      |    (for primary encode)      |   |
                                  | |                          DUP1             | (for primary encode)         |   |
                                  | |    (for live preview)    | |              +----------------------------+ |   |
                                +-|-]--------------------------+ |                                           | |   |
                                | | |                            |                                           | |   |
                                MERGE1                           |                                           | |   |
                                    |                            |                                           | |   |
                                    |                         +--+                                           | |   |
                                   DUP2           4CH 540p30  |                                              | |   |
                                   | |             YUV420     |                                              | |   |
                                   | |                        |                                              | |   |
                              +----+ +---+                  DEIHQ1                                           | |   |
                              |          |                  |     |                                          | |   |
                            SWMS0       SWMS1               |     |4CH 240p15                                | |   |
                             (SC5)      (SC2)               |     | YUV422                                   | |   |
                              |          |                (VIP-SC)|                                          | |   |
                   HDDAC --- HDMI       SDDAC               |     |                                          | |   |
                      (tied VENC)                           |     |                                          | |   |
                                                            |    (DEI-SC)                                    | |   |
                                                            |     |                                          | |   |
                                                            |     |                                          | |   |
                                                            |     |                                          | |   |
                                                            |     +------------------------------------------]-]---]----+
                                                            |                                                | |   |    |
                                               4CH 480p30   |                                                | |   |    |
                                                YUV420      |                                                | |   |    |
                                                            |                                                | |   |    |
                                                            |                                                | |   |    |
                                                            |                                                | |   |   NSF2
                                                            |                                                | |   |    | 4CH 240p 15
                                                            |                                                | |   |    | YUV420
                                                            |                                                | |   |    |
                                                            +------------------------------------------------]-]-+ |  IPC Frames Out0 (M3)--<<process link>>--IPC Frames In(DSP)--ALGLINK SCD
                                                                     (for secondary encode)                  | | | |
                                                                                                             MERGE3
                                                                                                               |
                                                                                                               |
                                                                                                      IPC Frames Out0 (M3)--<<process link>>--IPC Frames In(DSP)--ALGLINK SWOSD
                                                                                                               |
                                                                                                               |
                                                                                       +-----------------------+
                                                                                       |
                                                                                   IPC OUT (M3)
                                                                                       |
                                                                                   IPC IN  (M3)
                                                                                       |
                                                                                   Encode (4CH 1080p30 h264 + 4CH 480p30 h264 + 4CH 1080p1 MJPEG)
                                                                                       |
                                                                                   IPC BITS OUT (M3)
                                                                                       |
                                                                                   IPC BITS IN  (A8)
*/


#define ENABLE_FAKE_HD_MODE

#define TILER_ENABLE_DECODE      (TRUE)
#define TILER_ENABLE_ENCODE      (TRUE)


/*
    Encode
    CH 0, 1,  2,  3 - 1080p30 H264
    CH 4, 5,  6,  7 -  480p30 H264
    CH 8, 9, 10, 11 - 1080p1  MJPEG

    Decode
    CH 0, 1,  2,  3 - 1080p30 H264
*/
static SystemVideo_Ivahd2ChMap_Tbl systemVid_encDecIvaChMapTbl =
{
    .isPopulated = 1,
    .ivaMap[0] =
    {
        .EncNumCh  = 2,
        .EncChList = {0, 1 },
        .DecNumCh  = 1,
        .DecChList = {3 },
    },
    .ivaMap[1] =
    {
        .EncNumCh  = 10,
        .EncChList = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11},
        .DecNumCh  = 0,
    },
    .ivaMap[2] =
    {
        .EncNumCh  = 0,
        .DecNumCh  = 3,
        .DecChList = {0, 1, 2 },
    },
};


#define MULTICH_HD_DVR_USECASE_MAX_NUM_LINKS    (64)

#define DUP_FULL_RES_CAPTURE        (0)
#define DUP_Q_RES_CAPTURE           (1)
#define DUP_DISPLAY                 (2)
#define NUM_DUP_LINK                (3)

#define MERGE_Q_RES_CAPTURE         (0)
#define MERGE_DISPLAY               (1)
#define MERGE_ENCODE                (2)
#define NUM_MERGE_LINK              (3)

#define SELECT_FULL_RES_CAPTURE     (0)
#define NUM_SELECT_LINK             (1)

#define NSF_MJPEG                   (0)
#define NSF_Q_RES_CAPTURE           (1)
#define NSF_SCD                     (2)
#define NUM_NSF_LINK                (3)

#define DEIHQ_FULL_RES              (0)
#define DEIHQ_Q_RES                 (1)
#define DEI_FULL_RES                (2)
#define NUM_DEI_LINK                (3)

#define MAX_NUM_CAPTURE_DEVICES     (4)
#define MAX_NUM_DECODE_CHANNELS     (4)
#define MAX_NUM_CAPTURE_CHANNELS    (4)

#define MAX_PRIMARY_WIDTH           (1920)
#define MAX_PRIMARY_HEIGHT          (1080)

#define MAX_SECONDARY_WIDTH         (704)
#define MAX_SECONDARY_HEIGHT        (480)


#define     NUM_BUFS_PER_CH_CAPTURE              (4)
#define     NUM_BUFS_PER_CH_DEI                  (4)
#define     NUM_BUFS_PER_CH_DEC                  (4)
#define     NUM_BUFS_PER_CH_SWMS                 (4)
#define     NUM_BUFS_PER_CH_ENC_PRI              (4)
#define     NUM_BUFS_PER_CH_ENC_SEC              (4)
#define     NUM_BUFS_PER_CH_BITSOUT              (50)
#define     NUM_BUFS_PER_CH_BITSOUT_SCD          (2)
#define     NUM_BUFS_PER_CH_NSF_Q_RES_CAPTURE    (4)
#define     NUM_BUFS_PER_CH_NSF_SCD              (4)
#define     NUM_BUFS_PER_CH_NSF_MJPEG            (2)

#define     ENC_LINK_SECONDARY_STREAM_POOL_ID    (0)
#define     ENC_LINK_PRIMARY_STREAM_POOL_ID      (1)

#define     BIT_BUF_LENGTH_LIMIT_FACTOR_HD      (5)

#define     MAX_BUFFERING_QUEUE_LEN_PER_CH           (6)

typedef struct {

    UInt32 ipcBitsOutDSPId;

    UInt32 mergeId[NUM_MERGE_LINK];
    UInt32 dupId[NUM_DUP_LINK];

    UInt32 grpxId[VDIS_DEV_MAX];

    UInt32 ipcOutVpssId;
    UInt32 ipcInVpssId;
    UInt32 ipcOutVideoId;
    UInt32 ipcInVideoId;

    UInt32 selectId[NUM_SELECT_LINK];

    UInt32 createdLinkCount;
    UInt32 createdLinks[MULTICH_HD_DVR_USECASE_MAX_NUM_LINKS];

    CaptureLink_CreateParams          capturePrm;
    NsfLink_CreateParams              nsfPrm[NUM_NSF_LINK];
    DeiLink_CreateParams              deiPrm[NUM_DEI_LINK];
    SwMsLink_CreateParams             swMsPrm[VDIS_DEV_MAX];
    AvsyncLink_LinkSynchConfigParams  avsyncCfg[2];
    DisplayLink_CreateParams          displayPrm[VDIS_DEV_MAX];

    SelectLink_CreateParams           selectPrm[NUM_SELECT_LINK];
    MergeLink_CreateParams            mergePrm[NUM_MERGE_LINK];
    DupLink_CreateParams              dupPrm[NUM_DUP_LINK];

    IpcLink_CreateParams              ipcOutVideoPrm;
    IpcLink_CreateParams              ipcInVpssPrm;
    IpcLink_CreateParams              ipcOutVpssPrm;
    IpcLink_CreateParams              ipcInVideoPrm;
    IpcBitsOutLinkHLOS_CreateParams   ipcBitsOutHostPrm;
    IpcBitsInLinkRTOS_CreateParams    ipcBitsInVideoPrm;
    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutVideoPrm;
    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutDspPrm;
    IpcBitsInLinkHLOS_CreateParams    ipcBitsInHostPrm[2];
    IpcFramesOutLinkRTOS_CreateParams ipcFramesOutVpssPrm[2];
    IpcFramesInLinkRTOS_CreateParams  ipcFramesInDspPrm[2];

    EncLink_CreateParams              encPrm;
    DecLink_CreateParams              decPrm;

    AlgLink_CreateParams              dspAlgPrm[2];

    VCAP_DEVICE_CREATE_PARAM_S        vidDecVideoModeArgs[MAX_NUM_CAPTURE_DEVICES];

    SwMsLink_LayoutPrm                swmsLayoutPrm[VDIS_DEV_MAX];

} MultiChHdDvr_Context;


MultiChHdDvr_Context gHdDvrUsecaseContext =
{
    .createdLinkCount           = 0
};


static
Void multich_hddvr_register_created_link(MultiChHdDvr_Context *pContext,
                                                    UInt32 linkID)
{
    OSA_assert(pContext->createdLinkCount < OSA_ARRAYSIZE(pContext->createdLinks));
    pContext->createdLinks[pContext->createdLinkCount] = linkID;
    pContext->createdLinkCount++;
}

#define MULTICH_HDDVR_CREATE_LINK(linkID,createPrm)                \
    do                                                             \
    {                                                              \
        System_linkCreate(linkID,&createPrm,sizeof(createPrm));    \
        multich_hddvr_register_created_link(&gHdDvrUsecaseContext, \
                                                linkID);           \
    } while (0)

static
Void multich_hddvr_reset_link_prms()
{
    int i;

    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHdDvrUsecaseContext.ipcOutVpssPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHdDvrUsecaseContext.ipcInVideoPrm);

    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHdDvrUsecaseContext.ipcOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHdDvrUsecaseContext.ipcInVpssPrm);

    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams,gHdDvrUsecaseContext.ipcBitsOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,gHdDvrUsecaseContext.ipcBitsInHostPrm[0]);

    IpcBitsOutLinkHLOS_CreateParams_Init(&gHdDvrUsecaseContext.ipcBitsOutHostPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,gHdDvrUsecaseContext.ipcBitsInHostPrm[1]);

    IpcBitsOutLinkRTOS_CreateParams_Init(&gHdDvrUsecaseContext.ipcBitsOutDspPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkRTOS_CreateParams,gHdDvrUsecaseContext.ipcBitsInVideoPrm);

    MULTICH_INIT_STRUCT(IpcFramesOutLinkRTOS_CreateParams,gHdDvrUsecaseContext.ipcFramesOutVpssPrm[0]);
    MULTICH_INIT_STRUCT(IpcFramesInLinkRTOS_CreateParams,gHdDvrUsecaseContext.ipcFramesInDspPrm[0]);

    MULTICH_INIT_STRUCT(IpcFramesOutLinkRTOS_CreateParams,gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1]);
    MULTICH_INIT_STRUCT(IpcFramesInLinkRTOS_CreateParams,gHdDvrUsecaseContext.ipcFramesInDspPrm[1]);

    CaptureLink_CreateParams_Init(&gHdDvrUsecaseContext.capturePrm);

    for (i = 0; i < OSA_ARRAYSIZE(gHdDvrUsecaseContext.nsfPrm);i++)
    {
        NsfLink_CreateParams_Init(&gHdDvrUsecaseContext.nsfPrm[i]);
    }

    for (i = 0; i < OSA_ARRAYSIZE(gHdDvrUsecaseContext.deiPrm);i++)
    {
        DeiLink_CreateParams_Init(&gHdDvrUsecaseContext.deiPrm[i]);
    }

    for (i = 0; i < OSA_ARRAYSIZE(gHdDvrUsecaseContext.swMsPrm);i++)
    {
        SwMsLink_CreateParams_Init(&gHdDvrUsecaseContext.swMsPrm[i]);
    }

    for (i = 0; i < OSA_ARRAYSIZE(gHdDvrUsecaseContext.avsyncCfg);i++)
    {
        AvsyncLink_LinkSynchConfigParams_Init(&gHdDvrUsecaseContext.avsyncCfg[i]);
    }

    for (i = 0; i < OSA_ARRAYSIZE(gHdDvrUsecaseContext.displayPrm);i++)
    {
        DisplayLink_CreateParams_Init(&gHdDvrUsecaseContext.displayPrm[i]);
    }

    DecLink_CreateParams_Init(&gHdDvrUsecaseContext.decPrm);
    EncLink_CreateParams_Init(&gHdDvrUsecaseContext.encPrm);

    for (i = 0; i < OSA_ARRAYSIZE(gHdDvrUsecaseContext.dspAlgPrm);i++)
    {
        AlgLink_CreateParams_Init(&gHdDvrUsecaseContext.dspAlgPrm[i]);
    }

    for (i = 0; i < OSA_ARRAYSIZE(gHdDvrUsecaseContext.selectPrm);i++)
    {
        SelectLink_CreateParams_Init(&gHdDvrUsecaseContext.selectPrm[i]);
    }
}

static
Void multich_hddvr_set_capture_prm(CaptureLink_CreateParams *capturePrm)
{
    CaptureLink_VipInstParams         *pCaptureInstPrm;
    CaptureLink_OutParams             *pCaptureOutPrm;
    UInt32 vipInstId;

    capturePrm->isPalMode                   = FALSE;
    capturePrm->numVipInst                  = 4;
    capturePrm->tilerEnable                 = FALSE;
    #ifdef ENABLE_FAKE_HD_MODE
    capturePrm->fakeHdMode                  = TRUE;
    #else
    capturePrm->fakeHdMode                  = FALSE;
    #endif
    capturePrm->enableSdCrop                = FALSE;
    capturePrm->doCropInCapture             = FALSE;
    capturePrm->numBufsPerCh                = NUM_BUFS_PER_CH_CAPTURE;
    capturePrm->numExtraBufs                = 0;
    capturePrm->maxBlindAreasPerCh          = 0;
    capturePrm->overrideNumBufsInInstPrms   = 0;

    for(vipInstId=0; vipInstId<capturePrm->numVipInst; vipInstId++)
    {
        pCaptureInstPrm                     = &capturePrm->vipInst[vipInstId];
        pCaptureInstPrm->vipInstId          = (SYSTEM_CAPTURE_INST_VIP0_PORTA+
                                              vipInstId)%SYSTEM_CAPTURE_INST_MAX;
        #ifdef ENABLE_FAKE_HD_MODE
        pCaptureInstPrm->videoDecoderId     = SYSTEM_DEVICE_VID_DEC_TVP5158_DRV;
        pCaptureInstPrm->standard           = SYSTEM_STD_D1;
        #else
        pCaptureInstPrm->videoDecoderId     = 0; /* DONT set it to TVP5158 */
        pCaptureInstPrm->standard           = SYSTEM_STD_1080P_30;
        #endif
        pCaptureInstPrm->inDataFormat       = SYSTEM_DF_YUV422P;
        pCaptureInstPrm->numOutput          = 1;

        pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];
        pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV422I_YUYV;
        pCaptureOutPrm->scEnable            = FALSE;
        pCaptureOutPrm->scOutWidth          = 0;
        pCaptureOutPrm->scOutHeight         = 0;
        pCaptureOutPrm->outQueId            = 0;
    }
}

static
Void multich_hddvr_set_nsf_prm(NsfLink_CreateParams * nsfPrm, UInt32 numBufPerCh, UInt32 outputFrameRate)
{
    nsfPrm->numOutQue    = 1;
    nsfPrm->bypassNsf    = TRUE;
    nsfPrm->tilerEnable  = FALSE;
    nsfPrm->numBufsPerCh = numBufPerCh;
    nsfPrm->inputFrameRate = 30;
    nsfPrm->outputFrameRate = outputFrameRate;
}

static
UInt32 multich_hddvr_get_videodecoder_device_id()
{
    OSA_I2cHndl i2cHandle;
    Int32 status;
    UInt32 twl_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TW2968_DRV,0);
    UInt32 tvp_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TVP5158_DRV,0);
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;
    UInt32 chipId;
    UInt32 deviceId = ~0u;

    status = OSA_i2cOpen(&i2cHandle, I2C_DEFAULT_INST_ID);
    OSA_assert(status==0);

    numRegs = 0;
    regAddr[numRegs] = DEVICE_TW2968_REG_DEVICE_ID;
    regValue[numRegs] = 0;
    numRegs++;

    regAddr[numRegs] = DEVICE_TW2968_REG_REV_ID;
    regValue[numRegs] = 0;
    numRegs++;

    status = OSA_i2cRead8(&i2cHandle,twl_i2c_addr,regAddr,regValue,numRegs);
    if (status == 0)
    {
        chipId =  (((UInt32) (regValue[0]>>6)&0x3) << 5) | ( (regValue[1] >> 3) & 0x1F ) ;
        printf("\nTWL_CHIP_ID_READ:0x%x\n",chipId);
        if (chipId == DEVICE_TW2968_CHIP_ID)
        {
            deviceId = DEVICE_VID_DEC_TW2968_DRV;
        }
    }
    if (deviceId == ~0u)
    {
        numRegs = 0;
        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_MSB;
        regValue[numRegs] = 0;
        numRegs++;

        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_LSB;
        regValue[numRegs] = 0;
        numRegs++;

        status = OSA_i2cRead8(&i2cHandle,tvp_i2c_addr,regAddr,regValue,numRegs);
        if (status == 0)
        {
            chipId = ( ( UInt32 ) regValue[0] << 8 ) | regValue[1];
            printf("\nTVP_CHIP_ID_READ:0x%x\n",chipId);
            if (DEVICE_TVP5158_CHIP_ID == chipId)
            {
                deviceId = DEVICE_VID_DEC_TVP5158_DRV;
            }
        }
    }
    OSA_assert(deviceId != ~0u);
    status = OSA_i2cClose(&i2cHandle);
    OSA_assert(status==0);
    return deviceId;
}

static
Void multich_hddvr_configure_extvideodecoder_prm()
{
    int i;
    VCAP_VIDEO_SOURCE_STATUS_S vidSourceStatus;
    UInt32 deviceId;
    UInt32 numCaptureDevices;

    deviceId = multich_hddvr_get_videodecoder_device_id();

    if (deviceId == DEVICE_VID_DEC_TW2968_DRV)
    {
        numCaptureDevices = 2;
    }
    else
    {
        numCaptureDevices = 4;
    }


    for(i = 0; i < numCaptureDevices; i++)
    {
        gHdDvrUsecaseContext.vidDecVideoModeArgs[i].deviceId         = deviceId;
        if(deviceId == DEVICE_VID_DEC_TW2968_DRV)
        {
            /* consider first device connected to VIP0 Port A and second device connected to VIP1 PortA */
            gHdDvrUsecaseContext.vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i*2;
            gHdDvrUsecaseContext.vidDecVideoModeArgs[i].numChInDevice    = 1;
        }
        else
        {
            /* consider first device connected to VIP0 Port A and second device connected to VIP1 PortA */
            gHdDvrUsecaseContext.vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i;
            gHdDvrUsecaseContext.vidDecVideoModeArgs[i].numChInDevice    = 1;
        }
        gHdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoIfMode        = DEVICE_CAPT_VIDEO_IF_MODE_8BIT;
        gHdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoDataFormat    = SYSTEM_DF_YUV422P;
        gHdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.standard           = SYSTEM_STD_D1;
        gHdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
                    DEVICE_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_EMBEDDED_SYNC;
        gHdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoSystem        =
                                      DEVICE_VIDEO_DECODER_VIDEO_SYSTEM_AUTO_DETECT;
        gHdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCropEnable    = FALSE;
        gHdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoAutoDetectTimeout = -1;
    }
    Vcap_configVideoDecoder(&gHdDvrUsecaseContext.vidDecVideoModeArgs[0],
                            numCaptureDevices);
    Vcap_getVideoSourceStatus(&vidSourceStatus);
    Vcap_setVideoSourceStatus(&vidSourceStatus);
}

static
Void multich_hddvr_set_dei_prm(DeiLink_CreateParams *deiPrm, DeiLink_OutputScaleFactor *outScalerFactorDeiSc, DeiLink_OutputScaleFactor *outScalerFactorVipSc)
{
    UInt32 i, outId, chId;
    UInt32 outIdList[] = { DEI_LINK_OUT_QUE_DEI_SC, DEI_LINK_OUT_QUE_VIP_SC };

    deiPrm->comprEnable                 = FALSE;
    deiPrm->setVipScYuv422Format        = FALSE;
    deiPrm->inputDeiFrameRate           = 30;
    deiPrm->outputDeiFrameRate          = 30;
    deiPrm->enableDeiForceBypass        = TRUE;
    deiPrm->enableForceInterlacedInput  = FALSE;
    deiPrm->enableLineSkipSc            = FALSE;
    deiPrm->enableDualVipOut            = FALSE;
    deiPrm->enableInputFrameRateUpscale = FALSE;

    for(i=0; i<OSA_ARRAYSIZE(outIdList); i++)
    {
        outId = outIdList[i];

        deiPrm->enableOut[outId] = TRUE;

        deiPrm->tilerEnable[outId] = FALSE;
        if(outId==DEI_LINK_OUT_QUE_VIP_SC)
            deiPrm->tilerEnable[outId] = TILER_ENABLE_ENCODE;

        for(chId=0; chId<DEI_LINK_MAX_CH; chId++)
        {
            if(outId==DEI_LINK_OUT_QUE_VIP_SC)
                deiPrm->outScaleFactor[outId][chId] = *outScalerFactorVipSc;
            if(outId==DEI_LINK_OUT_QUE_DEI_SC)
                deiPrm->outScaleFactor[outId][chId] = *outScalerFactorDeiSc;
        }

        deiPrm->inputFrameRate[outId]   = 30;
        deiPrm->outputFrameRate[outId]  = 30;

        deiPrm->numBufsPerCh[outId]     = NUM_BUFS_PER_CH_DEI;
        deiPrm->generateBlankOut[outId] = FALSE;
    }
}

static
Void multich_hddvr_set_enclink_prm(EncLink_CreateParams *encPrm)
{
    int i,j;
    EncLink_ChCreateParams *pLinkChPrm;
    EncLink_ChDynamicParams *pLinkDynPrm;
    VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
    VENC_CHN_PARAMS_S *pChPrm;

    encPrm->numBufPerCh[ENC_LINK_SECONDARY_STREAM_POOL_ID] = NUM_BUFS_PER_CH_ENC_SEC;
    encPrm->numBufPerCh[ENC_LINK_PRIMARY_STREAM_POOL_ID] = NUM_BUFS_PER_CH_ENC_PRI;

    /* Primary Stream Params - D1 */
    for (i=0; i < gVencModuleContext.vencConfig.numPrimaryChn; i++)
    {
        pLinkChPrm  = &encPrm->chCreateParams[i];
        pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

        pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
        pDynPrm     = &pChPrm->dynamicParam;

        pLinkChPrm->format                  = IVIDEO_H264HP;
        pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
        pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->enableAnalyticinfo      = FALSE;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
        pLinkChPrm->rateControlPreset       = pChPrm->rcType;
        pLinkChPrm->enableHighSpeed         = TRUE;
        pLinkChPrm->numTemporalLayer        = VENC_TEMPORAL_LAYERS_2;
        pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;

        pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
        pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
        pLinkDynPrm->interFrameInterval     = 1;
        pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
        pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
        pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
        pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
        pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
        pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
        pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
        pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
    }

    /* Secondary Out <CIF> Params */
    for (i =  gVencModuleContext.vencConfig.numPrimaryChn,
         j =  VENC_PRIMARY_CHANNELS;
         i < (gVencModuleContext.vencConfig.numPrimaryChn
              + gVencModuleContext.vencConfig.numSecondaryChn);
         i++, j++)
    {
        pLinkChPrm  = &encPrm->chCreateParams[i];
        pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

        pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[j];
        pDynPrm     = &pChPrm->dynamicParam;

        pLinkChPrm->format                  = IVIDEO_H264HP;
        pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
        pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
        pLinkChPrm->rateControlPreset       = pChPrm->rcType;
        pLinkChPrm->enableHighSpeed         = TRUE;
        pLinkChPrm->numTemporalLayer        = VENC_TEMPORAL_LAYERS_2;
        pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;


        pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
        pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
        pLinkDynPrm->interFrameInterval     = 1;
        pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
        pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
        pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
        pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
        pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
        pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
        pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
    }
    if (gVsysModuleContext.vsysConfig.enableMjpegEnc == TRUE)
    {
        for (i=gVencModuleContext.vencConfig.numPrimaryChn + gVencModuleContext.vencConfig.numSecondaryChn;
             i<(gVencModuleContext.vencConfig.numPrimaryChn + gVencModuleContext.vencConfig.numSecondaryChn + gVencModuleContext.vencConfig.numPrimaryChn);
             i++)
        {
            pLinkChPrm  = &encPrm->chCreateParams[i];
            pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

            pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
            pDynPrm     = &pChPrm->dynamicParam;

            pLinkChPrm->format                 = IVIDEO_MJPEG;
            pLinkChPrm->profile                = 0;
            pLinkChPrm->dataLayout             = VCODEC_FIELD_SEPARATED;
            pLinkChPrm->fieldMergeEncodeEnable = FALSE;
            pLinkChPrm->enableAnalyticinfo     = 0;
            pLinkChPrm->enableWaterMarking     = 0;
            pLinkChPrm->maxBitRate             = 0;
            pLinkChPrm->encodingPreset         = 0;
            pLinkChPrm->rateControlPreset      = 0;
            pLinkChPrm->enableSVCExtensionFlag = 0;
            pLinkChPrm->numTemporalLayer       = 0;

            pLinkDynPrm->intraFrameInterval    = 0;
            pLinkDynPrm->targetBitRate         = 100*1000;
            pLinkDynPrm->interFrameInterval    = 0;
            pLinkDynPrm->mvAccuracy            = 0;
            pLinkDynPrm->inputFrameRate        = 1;
            pLinkDynPrm->qpMin                 = 0;
            pLinkDynPrm->qpMax                 = 0;
            pLinkDynPrm->qpInit                = -1;
            pLinkDynPrm->vbrDuration           = 0;
            pLinkDynPrm->vbrSensitivity        = 0;
        }
    }
}

static
Void multich_hddvr_set_swms_default_layout(SwMsLink_CreateParams *swMsPrm, UInt32 devId)
{
    SwMsLink_LayoutPrm *layoutInfo;
    SwMsLink_LayoutWinInfo *winInfo;
    UInt32 outWidth, outHeight, winId, widthAlign, heightAlign;
    UInt32 rowMax,colMax,row,col;

    MultiCh_swMsGetOutSize(swMsPrm->initOutRes, &outWidth, &outHeight);

    widthAlign = 8;
    heightAlign = 1;

    if(devId>=VDIS_DEV_MAX)
        devId = VDIS_DEV_HDMI;

    rowMax = 2;
    colMax = 2;

    layoutInfo = &swMsPrm->layoutPrm;

    /* init to known default */
    memset(layoutInfo, 0, sizeof(*layoutInfo));

    layoutInfo->onlyCh2WinMapChanged = FALSE;
    layoutInfo->outputFPS            = 30;
    layoutInfo->numWin               = rowMax * colMax;

    for(row=0; row<rowMax; row++)
    {
        for(col=0; col<colMax; col++)
        {
            winId = row*colMax+col;

            winInfo = &layoutInfo->winInfo[winId];

            winInfo->width  = VsysUtils_floor(outWidth/colMax    , widthAlign );
            winInfo->height = VsysUtils_floor(outHeight/rowMax   , heightAlign);
            winInfo->startX = VsysUtils_floor(winInfo->width*col , widthAlign );
            winInfo->startY = VsysUtils_floor(winInfo->height*row, heightAlign);

            if (col == colMax - 1) /* the last col */
            {
                winInfo->width = outWidth - winInfo->width * (colMax - 1);
            }

            if(devId == VDIS_DEV_SD)
            {
                winInfo->channelNum = winId; /* show live 540p res CHs */
            }
            else
            {
                winInfo->channelNum = winId + MAX_NUM_CAPTURE_CHANNELS; /* show live 540p res CHs */
            }

            winInfo->bypass = FALSE;
        }
    }
}


static
Void multich_hddvr_set_swms_prm(SwMsLink_CreateParams *swMsPrm,
                                    UInt32 swMsIdx)
{
    UInt32 devId, maxOutRes;

    swMsPrm->numSwMsInst = 1;

    if (swMsIdx == 1)
    {
        swMsPrm->swMsInstId[0] = SYSTEM_SW_MS_SC_INST_DEI_SC_NO_DEI;
        swMsPrm->includeVipScInDrvPath = TRUE;

        maxOutRes  = VSYS_STD_PAL;
        devId      = VDIS_DEV_SD;
    }
    else
    {
        swMsPrm->swMsInstId[0] = SYSTEM_SW_MS_SC_INST_SC5;

        maxOutRes  = VSYS_STD_1080P_60;
        devId      = VDIS_DEV_HDMI;
    }

    swMsPrm->maxOutRes                   = maxOutRes;
	swMsPrm->initOutRes                  = gVdisModuleContext.vdisConfig.deviceParams[devId].resolution;
    swMsPrm->lineSkipMode                = FALSE;
    swMsPrm->enableLayoutGridDraw        = gVdisModuleContext.vdisConfig.enableLayoutGridDraw;
    swMsPrm->maxInputQueLen              = SYSTEM_SW_MS_DEFAULT_INPUT_QUE_LEN + 6;
    swMsPrm->numOutBuf                   = NUM_BUFS_PER_CH_SWMS;
    swMsPrm->enableOuputDup              = TRUE;
    swMsPrm->enableProcessTieWithDisplay = TRUE;
    swMsPrm->outDataFormat               = SYSTEM_DF_YUV422I_YUYV;
    swMsPrm->outputBufModified           = TRUE;

    multich_hddvr_set_swms_default_layout(swMsPrm, devId);
}

static
Void multich_hddvr_set_avsync_vidque_prm(Avsync_SynchConfigParams *queCfg,
                                            Int chnum,
                                            UInt32 avsStartChNum,
                                            UInt32 avsEndChNum)
{
    queCfg->chNum = chnum;
    queCfg->audioPresent = FALSE;
    if ((queCfg->chNum >= avsStartChNum)
        &&
        (queCfg->chNum <= avsEndChNum)
        &&
        (gVsysModuleContext.vsysConfig.enableAVsync))
    {
        queCfg->avsyncEnable = FALSE;
    }
    else
    {
        queCfg->avsyncEnable = FALSE;
    }
    queCfg->clkAdjustPolicy.refClkType = AVSYNC_REFCLKADJUST_NONE;
    queCfg->playTimerStartTimeout = 0;
    queCfg->playStartMode = AVSYNC_PLAYBACK_START_MODE_WAITSYNCH;
    queCfg->ptsInitMode   = AVSYNC_PTS_INIT_MODE_APP;
    queCfg->clkAdjustPolicy.clkAdjustLead = AVSYNC_VIDEO_TIMEBASESHIFT_MAX_LEAD_MS;
    queCfg->clkAdjustPolicy.clkAdjustLag = AVSYNC_VIDEO_TIMEBASESHIFT_MAX_LAG_MS;
    queCfg->vidSynchPolicy.playMaxLag    = 200;
}

static
Void multich_hddvr_set_avsync_prm(AvsyncLink_LinkSynchConfigParams *avsyncPrm,
                                     UInt32 swMsIdx,
                                     UInt32 prevLinkID,
                                     UInt32 prevLinkQueId)
{
    System_LinkInfo                   swmsInLinkInfo;
    Int i;
    Int32 status;

    if (0 == swMsIdx)
    {
        Vdis_getAvsyncConfig(VDIS_DEV_HDMI,avsyncPrm);
        avsyncPrm->displayLinkID        = Vdis_getDisplayId(VDIS_DEV_HDMI);
        avsyncPrm->videoSynchLinkID = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)];
    }
    else
    {
        Vdis_getAvsyncConfig(VDIS_DEV_SD,avsyncPrm);
        avsyncPrm->displayLinkID        = Vdis_getDisplayId(VDIS_DEV_SD);
        avsyncPrm->videoSynchLinkID = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)];
    }
    System_linkGetInfo(prevLinkID,&swmsInLinkInfo);
    OSA_assert(swmsInLinkInfo.numQue > prevLinkQueId);

    avsyncPrm->numCh            = swmsInLinkInfo.queInfo[prevLinkQueId].numCh;
    avsyncPrm->syncMasterChnum = AVSYNC_INVALID_CHNUM;
    for (i = 0; i < avsyncPrm->numCh;i++)
    {
        multich_hddvr_set_avsync_vidque_prm(&avsyncPrm->queCfg[i],
                                               i,
                                               gVcapModuleContext.vcapConfig.numChn,
                                               (gVcapModuleContext.vcapConfig.numChn + (gVdecModuleContext.vdecConfig.numChn - 1)));
    }
    if (0 == swMsIdx)
    {
        Vdis_setAvsyncConfig(VDIS_DEV_HDMI,avsyncPrm);
    }
    else
    {
        Vdis_setAvsyncConfig(VDIS_DEV_SD,avsyncPrm);
    }

    status = Avsync_configSyncConfigInfo(avsyncPrm);
    OSA_assert(status == 0);

}

static
Void multich_hddvr_set_osd_prm(AlgLink_CreateParams *dspAlgPrm)
{
    int chId;

    dspAlgPrm->enableOSDAlg = TRUE;
    dspAlgPrm->enableSCDAlg = FALSE;
    dspAlgPrm->outQueParams[ALG_LINK_SCD_OUT_QUE].nextLink = SYSTEM_LINK_ID_INVALID;

    for(chId = 0; chId < ALG_LINK_OSD_MAX_CH; chId++)
    {
        AlgLink_OsdChWinParams * chWinPrm =
          &dspAlgPrm->osdChCreateParams[chId].chDefaultParams;
        /* set osd window max width and height */
        dspAlgPrm->osdChCreateParams[chId].maxWidth  = EXAMPLE_OSD_WIN_MAX_WIDTH;
        dspAlgPrm->osdChCreateParams[chId].maxHeight = EXAMPLE_OSD_WIN_MAX_HEIGHT;

        chWinPrm->chId       = chId;
        chWinPrm->numWindows = 0;
    }
}

static
Void multich_hddvr_set_scd_prm(AlgLink_CreateParams *dspAlgPrm)
{
    Int32   numBlksInFrame;
    Int32   numHorzBlks, numVertBlks, chIdx;
    UInt32  x, y, i;

    dspAlgPrm->enableOSDAlg = FALSE;
    dspAlgPrm->enableSCDAlg = TRUE;
    dspAlgPrm->scdCreateParams.maxWidth               = 352;
    dspAlgPrm->scdCreateParams.maxHeight              = 240;
    dspAlgPrm->scdCreateParams.maxStride              = 352;
    dspAlgPrm->scdCreateParams.numValidChForSCD       = 4;

    dspAlgPrm->scdCreateParams.numSecs2WaitB4Init     = 3;
    dspAlgPrm->scdCreateParams.numSecs2WaitB4FrmAlert = 1;
    dspAlgPrm->scdCreateParams.inputFrameRate         = 2;
    dspAlgPrm->scdCreateParams.outputFrameRate        = 2;

    /*
        Each block is fixed to be 32x10 in size when height is 240
        Each block is fixed to be 32x12 in size when height is 288
    */
    numHorzBlks    = dspAlgPrm->scdCreateParams.maxWidth / 32;
    if(dspAlgPrm->scdCreateParams.maxHeight == 240)
       numVertBlks    = dspAlgPrm->scdCreateParams.maxHeight / 10;
    else
       numVertBlks    = dspAlgPrm->scdCreateParams.maxHeight / 12;

    numBlksInFrame = numHorzBlks * numVertBlks;

    for(chIdx = 0; chIdx < dspAlgPrm->scdCreateParams.numValidChForSCD; chIdx++)
    {
       AlgLink_ScdChParams * chPrm = &dspAlgPrm->scdCreateParams.chDefaultParams[chIdx];

       chPrm->blkNumBlksInFrame = numBlksInFrame;
       chPrm->chId              = chIdx;
       chPrm->mode              = ALG_LINK_SCD_DETECTMODE_MONITOR_BLOCKS_AND_FRAME;
       chPrm->frmIgnoreLightsON = FALSE;
       chPrm->frmIgnoreLightsOFF= FALSE;
       chPrm->frmSensitivity    = ALG_LINK_SCD_SENSITIVITY_HIGH;
       chPrm->frmEdgeThreshold  = 100;
       i = 0;
       for(y = 0; y < numVertBlks; y++)
       {
         for(x = 0; x < numHorzBlks; x++)
         {
           chPrm->blkConfig[i].sensitivity = ALG_LINK_SCD_SENSITIVITY_LOW;
           chPrm->blkConfig[i].monitored   = 0;
           i++;
         }
       }
    }
    dspAlgPrm->scdCreateParams.numBufPerCh = NUM_BUFS_PER_CH_BITSOUT_SCD;
}

static
Void multich_hddvr_set_declink_prms(DecLink_CreateParams *decPrm)
{
    int i;

    gVdecModuleContext.vdecConfig.numChn = MAX_NUM_DECODE_CHANNELS;
    for (i=0; i<gVdecModuleContext.vdecConfig.numChn; i++)
    {
        decPrm->chCreateParams[i].format                 = IVIDEO_H264HP;
        decPrm->chCreateParams[i].profile                = IH264VDEC_PROFILE_ANY;
        decPrm->chCreateParams[i].processCallLevel       = VDEC_FRAMELEVELPROCESSCALL;
        decPrm->chCreateParams[i].targetMaxWidth         = 1920;
        decPrm->chCreateParams[i].targetMaxHeight        = 1080;
        decPrm->chCreateParams[i].numBufPerCh            = NUM_BUFS_PER_CH_DEC;
        decPrm->chCreateParams[i].tilerEnable            = TILER_ENABLE_DECODE;
        decPrm->chCreateParams[i].defaultDynamicParams.targetFrameRate = gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.frameRate;
        decPrm->chCreateParams[i].defaultDynamicParams.targetBitRate   = gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.targetBitRate;
    }
}

static
Void multich_hddvr_set_ipcbitsout_hlos_prms(IpcBitsOutLinkHLOS_CreateParams * ipcBitsOutHostPrm)
{
    int i;

    for (i = 0;
         i < (MAX_NUM_DECODE_CHANNELS);
         i++)
    {
        System_LinkChInfo *pChInfo;

        pChInfo = &ipcBitsOutHostPrm->inQueInfo.chInfo[i];

        pChInfo->bufType        = 0; // NOT USED
        pChInfo->codingformat   = 0; // NOT USED
        pChInfo->dataFormat     = 0; // NOT USED
        pChInfo->memType        = 0; // NOT USED
        pChInfo->startX         = 0; // NOT USED
        pChInfo->startY         = 0; // NOT USED
        pChInfo->width          = 1920;
        pChInfo->height         = 1080;
        pChInfo->pitch[0]       = 0; // NOT USED
        pChInfo->pitch[1]       = 0; // NOT USED
        pChInfo->pitch[2]       = 0; // NOT USED
        pChInfo->scanFormat     = SYSTEM_SF_PROGRESSIVE;

        ipcBitsOutHostPrm->maxQueueDepth[i] =
            MAX_BUFFERING_QUEUE_LEN_PER_CH;
        ipcBitsOutHostPrm->chMaxReqBufSize[i] = (pChInfo->width * pChInfo->height);
        ipcBitsOutHostPrm->totalBitStreamBufferSize [i] =
                (ipcBitsOutHostPrm->chMaxReqBufSize[i] * BIT_BUF_LENGTH_LIMIT_FACTOR_HD);
    }
    ipcBitsOutHostPrm->baseCreateParams.noNotifyMode   = FALSE;
    ipcBitsOutHostPrm->baseCreateParams.notifyNextLink = TRUE;
    ipcBitsOutHostPrm->baseCreateParams.numOutQue      = 1;
    ipcBitsOutHostPrm->inQueInfo.numCh                 = MAX_NUM_DECODE_CHANNELS;
}

static
Void multich_hddvr_set_display_prms(DisplayLink_CreateParams *displayPrm,
                                        UInt32 maxOutRes)
{
    displayPrm->displayRes = maxOutRes;
}

static
Void multich_hddvr_set_link_ids()
{
    Bool   enableOsdAlgLink = gVsysModuleContext.vsysConfig.enableOsd;
    Bool   enableScdAlgLink = gVsysModuleContext.vsysConfig.enableScd;
    int    i;

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_CAPTURE;

    for (i = 0; i < NUM_NSF_LINK;i++)
    {
        gVcapModuleContext.nsfId[i]         = SYSTEM_LINK_ID_NSF_0 + i;
    }

    gVcapModuleContext.deiId[DEIHQ_FULL_RES]     = SYSTEM_LINK_ID_DEI_HQ_0;
    gVcapModuleContext.deiId[DEIHQ_Q_RES]        = SYSTEM_LINK_ID_DEI_0;
    gVcapModuleContext.deiId[DEI_FULL_RES]       = SYSTEM_LINK_ID_DEI_HQ_1;

    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]    = SYSTEM_LINK_ID_SW_MS_MULTI_INST_0;
    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_1;

    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)] = SYSTEM_LINK_ID_DISPLAY_0; /* ON CHIP HDMI */
    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)]   = SYSTEM_LINK_ID_DISPLAY_2; /* SD HDMI */

    gHdDvrUsecaseContext.grpxId[0]                       = SYSTEM_LINK_ID_GRPX_0;
    gHdDvrUsecaseContext.grpxId[1]                       = SYSTEM_LINK_ID_GRPX_1;

    for (i = 0; i < NUM_MERGE_LINK;i++)
    {
        gHdDvrUsecaseContext.mergeId[i] = SYSTEM_VPSS_LINK_ID_MERGE_0 + i;
    }
    for (i = 0; i < NUM_DUP_LINK;i++)
    {
        gHdDvrUsecaseContext.dupId[i] = SYSTEM_VPSS_LINK_ID_DUP_0 + i;
    }
    for (i = 0; i < NUM_SELECT_LINK;i++)
    {
        gHdDvrUsecaseContext.selectId[i]   = SYSTEM_VPSS_LINK_ID_SELECT_0 + i;
    }

    if(enableOsdAlgLink)
    {
        gVcapModuleContext.ipcFramesOutVpssId[0] = SYSTEM_VPSS_LINK_ID_IPC_FRAMES_OUT_0;
        gVcapModuleContext.ipcFramesInDspId[0]   = SYSTEM_DSP_LINK_ID_IPC_FRAMES_IN_0;
        gVcapModuleContext.dspAlgId[0]           = SYSTEM_LINK_ID_ALG_0;
    }

    if(enableScdAlgLink)
    {
        gVcapModuleContext.ipcFramesOutVpssId[1] = SYSTEM_VPSS_LINK_ID_IPC_FRAMES_OUT_1;
        gVcapModuleContext.ipcFramesInDspId[1]   = SYSTEM_DSP_LINK_ID_IPC_FRAMES_IN_1;
        gVcapModuleContext.dspAlgId[1]           = SYSTEM_LINK_ID_ALG_1;
        gHdDvrUsecaseContext.ipcBitsOutDSPId     = SYSTEM_DSP_LINK_ID_IPC_BITS_OUT_0;
        gVcapModuleContext.ipcBitsInHLOSId       = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;
    }

    gVencModuleContext.encId        = SYSTEM_LINK_ID_VENC_0;
    gVdecModuleContext.decId        = SYSTEM_LINK_ID_VDEC_0;

    gHdDvrUsecaseContext.ipcOutVpssId = SYSTEM_VPSS_LINK_ID_IPC_OUT_M3_0;
    gHdDvrUsecaseContext.ipcInVideoId = SYSTEM_VIDEO_LINK_ID_IPC_IN_M3_0;

    gHdDvrUsecaseContext.ipcOutVideoId= SYSTEM_VIDEO_LINK_ID_IPC_OUT_M3_0;
    gHdDvrUsecaseContext.ipcInVpssId  = SYSTEM_VPSS_LINK_ID_IPC_IN_M3_0;

    gVencModuleContext.ipcBitsOutRTOSId     = SYSTEM_VIDEO_LINK_ID_IPC_BITS_OUT_0;
    if(enableScdAlgLink)
       gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_1;
    else
       gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;

    gVdecModuleContext.ipcBitsOutHLOSId     = SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0;
    gVdecModuleContext.ipcBitsInRTOSId      = SYSTEM_VIDEO_LINK_ID_IPC_BITS_IN_0;

    gVdisModuleContext.setMosaicFxn         = MultiCh_hdDvrSetMosaicParams;
}

static
Void multich_hddvr_reset_link_ids()
{
    Bool   enableOsdAlgLink = gVsysModuleContext.vsysConfig.enableOsd;
    Bool   enableScdAlgLink = gVsysModuleContext.vsysConfig.enableScd;
    int    i;

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_INVALID;

    for (i = 0; i < NUM_NSF_LINK;i++)
    {
        gVcapModuleContext.nsfId[i]         = SYSTEM_LINK_ID_INVALID;
    }
    for(i=0; i<NUM_DEI_LINK; i++)
    {
        gVcapModuleContext.deiId[i]     = SYSTEM_LINK_ID_INVALID;
    }

    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]      = SYSTEM_LINK_ID_INVALID;
    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)]      = SYSTEM_LINK_ID_INVALID;

    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)] = SYSTEM_LINK_ID_INVALID; /* ON CHIP HDMI */
    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)] = SYSTEM_LINK_ID_INVALID; /* SD HDMI */

    gHdDvrUsecaseContext.grpxId[0]                       = SYSTEM_LINK_ID_INVALID;
    gHdDvrUsecaseContext.grpxId[1]                       = SYSTEM_LINK_ID_INVALID;

    for (i = 0; i < NUM_MERGE_LINK;i++)
    {
        gHdDvrUsecaseContext.mergeId[i] = SYSTEM_LINK_ID_INVALID;
    }
    for (i = 0; i < NUM_DUP_LINK;i++)
    {
        gHdDvrUsecaseContext.dupId[i] = SYSTEM_LINK_ID_INVALID;
    }
    for (i = 0; i < NUM_SELECT_LINK;i++)
    {
        gHdDvrUsecaseContext.selectId[i] = SYSTEM_LINK_ID_INVALID;
    }

    if(enableOsdAlgLink)
    {
        gVcapModuleContext.ipcFramesOutVpssId[0] = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.ipcFramesInDspId[0] = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.dspAlgId[0] = SYSTEM_LINK_ID_INVALID;
    }
    if(enableScdAlgLink)
    {
        gVcapModuleContext.ipcFramesOutVpssId[1] = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.ipcFramesInDspId[1]   = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.dspAlgId[1]           = SYSTEM_LINK_ID_INVALID;
        gHdDvrUsecaseContext.ipcBitsOutDSPId = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.ipcBitsInHLOSId       = SYSTEM_LINK_ID_INVALID;
    }
    gVencModuleContext.encId        = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.decId        = SYSTEM_LINK_ID_INVALID;

    gHdDvrUsecaseContext.ipcOutVpssId = SYSTEM_LINK_ID_INVALID;
    gHdDvrUsecaseContext.ipcInVideoId = SYSTEM_LINK_ID_INVALID;

    gHdDvrUsecaseContext.ipcOutVideoId= SYSTEM_LINK_ID_INVALID;
    gHdDvrUsecaseContext.ipcInVpssId  = SYSTEM_LINK_ID_INVALID;

    gVencModuleContext.ipcBitsOutRTOSId  = SYSTEM_LINK_ID_INVALID;
    gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_LINK_ID_INVALID;

    gVdecModuleContext.ipcBitsOutHLOSId  = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.ipcBitsInRTOSId  = SYSTEM_LINK_ID_INVALID;

    gVdisModuleContext.setMosaicFxn     = NULL;
}

static
Void multich_hddvr_connect_osd_links()
{
    /* IpcFramesOut Link for OSD */
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.inQueParams.prevLinkId     = gHdDvrUsecaseContext.mergeId[MERGE_ENCODE];
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.inQueParams.prevLinkQueId  = 0;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.numOutQue                  = 1;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.notifyNextLink             = TRUE;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.outQueParams[0].nextLink   = gHdDvrUsecaseContext.ipcOutVpssId;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.processLink                = gVcapModuleContext.ipcFramesInDspId[0];
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.notifyProcessLink          = TRUE;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.noNotifyMode               = FALSE;

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.ipcFramesOutVpssId[0],
        gHdDvrUsecaseContext.ipcFramesOutVpssPrm[0]
    );


    /* IpcFramesInDsp ---Q0--- dspAlg0(SCD) */
    gHdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.inQueParams.prevLinkId       = gVcapModuleContext.ipcFramesOutVpssId[0];
    gHdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.inQueParams.prevLinkQueId    = 0;
    gHdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.numOutQue                    = 1;
    gHdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.outQueParams[0].nextLink     = gVcapModuleContext.dspAlgId[0];
    gHdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.notifyPrevLink               = TRUE;
    gHdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.notifyNextLink               = TRUE;
    gHdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.noNotifyMode                 = FALSE;

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.ipcFramesInDspId[0],
        gHdDvrUsecaseContext.ipcFramesInDspPrm[0]
    );

    multich_hddvr_set_osd_prm(&gHdDvrUsecaseContext.dspAlgPrm[0]);
    gHdDvrUsecaseContext.dspAlgPrm[0].inQueParams.prevLinkId    = gVcapModuleContext.ipcFramesInDspId[0];
    gHdDvrUsecaseContext.dspAlgPrm[0].inQueParams.prevLinkQueId = 0;

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.dspAlgId[0],
        gHdDvrUsecaseContext.dspAlgPrm[0]
    );
}

static
Void multich_hddvr_connect_scd_links()
{
    /* NSF link
        to convert YUV422 data to YUV420 for SCD input
    */
    multich_hddvr_set_nsf_prm(&gHdDvrUsecaseContext.nsfPrm[NSF_SCD], NUM_BUFS_PER_CH_NSF_SCD, 5);

    gHdDvrUsecaseContext.nsfPrm[NSF_SCD].inQueParams.prevLinkId    = gVcapModuleContext.deiId[DEIHQ_Q_RES];
    gHdDvrUsecaseContext.nsfPrm[NSF_SCD].inQueParams.prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC;
    gHdDvrUsecaseContext.nsfPrm[NSF_SCD].outQueParams[0].nextLink  = gVcapModuleContext.ipcFramesOutVpssId[1];

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.nsfId[NSF_SCD],
        gHdDvrUsecaseContext.nsfPrm[NSF_SCD]
    );

    /* ipcFramesOutVpssId[1] ---Q2---> ipcFramesInDspId[1] */
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.inQueParams.prevLinkId = gVcapModuleContext.nsfId[NSF_SCD];
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.notifyPrevLink         = TRUE;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.inputFrameRate             = 10;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.outputFrameRate            = 10;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.numOutQue                  = 1;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.outQueParams[0].nextLink   = gVcapModuleContext.ipcFramesInDspId[1];
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.notifyNextLink             = TRUE;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.processLink                = SYSTEM_LINK_ID_INVALID;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.notifyProcessLink          = FALSE;
    gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.noNotifyMode               = FALSE;

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.ipcFramesOutVpssId[1],
        gHdDvrUsecaseContext.ipcFramesOutVpssPrm[1]
    );

    gHdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.inQueParams.prevLinkId       = gVcapModuleContext.ipcFramesOutVpssId[1];
    gHdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.inQueParams.prevLinkQueId    = 0;
    gHdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.notifyPrevLink               = TRUE;
    gHdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.numOutQue                    = 1;
    gHdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.outQueParams[0].nextLink     = gVcapModuleContext.dspAlgId[1];
    gHdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.notifyNextLink               = TRUE;
    gHdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.noNotifyMode                 = FALSE;

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.ipcFramesInDspId[1],
        gHdDvrUsecaseContext.ipcFramesInDspPrm[1]
    );

    /* dspAlgId[1] ---Q0---> ipcBitsOutDsp */
    multich_hddvr_set_scd_prm(&gHdDvrUsecaseContext.dspAlgPrm[1]);
    gHdDvrUsecaseContext.dspAlgPrm[1].inQueParams.prevLinkId                        = gVcapModuleContext.ipcFramesInDspId[1];
    gHdDvrUsecaseContext.dspAlgPrm[1].inQueParams.prevLinkQueId                     = 0;
    gHdDvrUsecaseContext.dspAlgPrm[1].outQueParams[ALG_LINK_SCD_OUT_QUE].nextLink   = gHdDvrUsecaseContext.ipcBitsOutDSPId;
    gHdDvrUsecaseContext.ipcBitsOutDspPrm.baseCreateParams.inQueParams.prevLinkId   = gVcapModuleContext.dspAlgId[1];
    gHdDvrUsecaseContext.ipcBitsOutDspPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.dspAlgId[1],
        gHdDvrUsecaseContext.dspAlgPrm[1]
    );

    /* ipcBitsOutDsp ---Q0---> ipcBitsInHlos */
    gHdDvrUsecaseContext.ipcBitsOutDspPrm.baseCreateParams.numOutQue                 = 1;
    gHdDvrUsecaseContext.ipcBitsOutDspPrm.baseCreateParams.outQueParams[0].nextLink  = gVcapModuleContext.ipcBitsInHLOSId;
    MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&gHdDvrUsecaseContext.ipcBitsOutDspPrm,
                                                TRUE);
    gHdDvrUsecaseContext.ipcBitsInHostPrm[1].baseCreateParams.inQueParams.prevLinkId = gHdDvrUsecaseContext.ipcBitsOutDSPId;
    gHdDvrUsecaseContext.ipcBitsInHostPrm[1].baseCreateParams.inQueParams.prevLinkQueId = 0;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.ipcBitsOutDSPId,
        gHdDvrUsecaseContext.ipcBitsOutDspPrm
    );

    gHdDvrUsecaseContext.ipcBitsInHostPrm[1].baseCreateParams.numOutQue                 = 1;
    gHdDvrUsecaseContext.ipcBitsInHostPrm[1].baseCreateParams.outQueParams[0].nextLink  = SYSTEM_LINK_ID_INVALID;
    MultiCh_ipcBitsInitCreateParams_BitsInHLOSVcap(&gHdDvrUsecaseContext.ipcBitsInHostPrm[1]);

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.ipcBitsInHLOSId,
        gHdDvrUsecaseContext.ipcBitsInHostPrm[1]
    );
}

static
Void multich_hddvr_connect_encode_links()
{
    /* ipcOutVpssIdisOutVpssId ---Q0---> ipcInVideoId */
    gHdDvrUsecaseContext.ipcOutVpssPrm.inQueParams.prevLinkId    = gVcapModuleContext.ipcFramesOutVpssId[0];
    gHdDvrUsecaseContext.ipcOutVpssPrm.inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.ipcOutVpssPrm.outQueParams[0].nextLink  = gHdDvrUsecaseContext.ipcInVideoId;
    gHdDvrUsecaseContext.ipcOutVpssPrm.notifyNextLink            = FALSE;
    gHdDvrUsecaseContext.ipcOutVpssPrm.notifyPrevLink            = TRUE;
    gHdDvrUsecaseContext.ipcOutVpssPrm.noNotifyMode              = TRUE;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.ipcOutVpssId,
        gHdDvrUsecaseContext.ipcOutVpssPrm
    );


    /* ipcInVideoId ---Q0---> encId */
    gHdDvrUsecaseContext.ipcInVideoPrm.inQueParams.prevLinkId    = gHdDvrUsecaseContext.ipcOutVpssId;
    gHdDvrUsecaseContext.ipcInVideoPrm.inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.ipcInVideoPrm.numOutQue                 = 1;
    gHdDvrUsecaseContext.ipcInVideoPrm.outQueParams[0].nextLink  = gVencModuleContext.encId;
    gHdDvrUsecaseContext.ipcInVideoPrm.notifyNextLink            = TRUE;
    gHdDvrUsecaseContext.ipcInVideoPrm.notifyPrevLink            = FALSE;
    gHdDvrUsecaseContext.ipcInVideoPrm.noNotifyMode              = TRUE;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.ipcInVideoId,
        gHdDvrUsecaseContext.ipcInVideoPrm
    );


    /* encId ---Q0---> ipcBitsOutRTOSId */
    multich_hddvr_set_enclink_prm(&gHdDvrUsecaseContext.encPrm);
    gHdDvrUsecaseContext.encPrm.inQueParams.prevLinkId    = gHdDvrUsecaseContext.ipcInVideoId;
    gHdDvrUsecaseContext.encPrm.inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.encPrm.outQueParams.nextLink     = gVencModuleContext.ipcBitsOutRTOSId;

    MULTICH_HDDVR_CREATE_LINK(
        gVencModuleContext.encId,
        gHdDvrUsecaseContext.encPrm
    );

    /* ipcBitsOutVideoId ---Q0---> ipcBitsInHostId */
    gHdDvrUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.encId;
    gHdDvrUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.numOutQue                 = 1;
    gHdDvrUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.outQueParams[0].nextLink  = gVencModuleContext.ipcBitsInHLOSId;
    MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&gHdDvrUsecaseContext.ipcBitsOutVideoPrm,
                                               TRUE);
    gHdDvrUsecaseContext.ipcBitsInHostPrm[0].baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.ipcBitsOutRTOSId;
    gHdDvrUsecaseContext.ipcBitsInHostPrm[0].baseCreateParams.inQueParams.prevLinkQueId = 0;

    MULTICH_HDDVR_CREATE_LINK(
        gVencModuleContext.ipcBitsOutRTOSId,
        gHdDvrUsecaseContext.ipcBitsOutVideoPrm
    );

    MultiCh_ipcBitsInitCreateParams_BitsInHLOS(&gHdDvrUsecaseContext.ipcBitsInHostPrm[0]);
    MULTICH_HDDVR_CREATE_LINK(
        gVencModuleContext.ipcBitsInHLOSId,
        gHdDvrUsecaseContext.ipcBitsInHostPrm[0]
    );
}

static
Void multich_hddvr_connect_decode_links()
{
    /* ipcBitsOutHostId ---Q0---> ipcBitsInRtosId */
    multich_hddvr_set_ipcbitsout_hlos_prms(&gHdDvrUsecaseContext.ipcBitsOutHostPrm);
    gHdDvrUsecaseContext.ipcBitsOutHostPrm.baseCreateParams.outQueParams[0].nextLink    = gVdecModuleContext.ipcBitsInRTOSId;

    MULTICH_HDDVR_CREATE_LINK(
        gVdecModuleContext.ipcBitsOutHLOSId,
        gHdDvrUsecaseContext.ipcBitsOutHostPrm
    );

    /* ipcBitsInRtosId ---Q0---> decId */
    gHdDvrUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkId      = gVdecModuleContext.ipcBitsOutHLOSId;
    gHdDvrUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkQueId   = 0;
    gHdDvrUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.numOutQue                    = 1;
    gHdDvrUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.outQueParams[0].nextLink     = gVdecModuleContext.decId;
    MultiCh_ipcBitsInitCreateParams_BitsInRTOS(&gHdDvrUsecaseContext.ipcBitsInVideoPrm, TRUE);

    MULTICH_HDDVR_CREATE_LINK(
        gVdecModuleContext.ipcBitsInRTOSId,
        gHdDvrUsecaseContext.ipcBitsInVideoPrm
    );

    /* decId---Q0--->ipcOutVideoId*/
    multich_hddvr_set_declink_prms(&gHdDvrUsecaseContext.decPrm);
    gHdDvrUsecaseContext.decPrm.inQueParams.prevLinkId    = gVdecModuleContext.ipcBitsInRTOSId;
    gHdDvrUsecaseContext.decPrm.inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.decPrm.outQueParams.nextLink  = gHdDvrUsecaseContext.ipcOutVideoId;

    MULTICH_HDDVR_CREATE_LINK(
        gVdecModuleContext.decId,
        gHdDvrUsecaseContext.decPrm
    );

    /*ipcOutVideoId---Q0-->ipcInVpssId*/
    gHdDvrUsecaseContext.ipcOutVideoPrm.inQueParams.prevLinkId    = gVdecModuleContext.decId;
    gHdDvrUsecaseContext.ipcOutVideoPrm.inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.ipcOutVideoPrm.numOutQue                 = 1;
    gHdDvrUsecaseContext.ipcOutVideoPrm.outQueParams[0].nextLink  = gHdDvrUsecaseContext.ipcInVpssId;
    gHdDvrUsecaseContext.ipcOutVideoPrm.notifyNextLink            = FALSE;
    gHdDvrUsecaseContext.ipcOutVideoPrm.notifyPrevLink            = TRUE;
    gHdDvrUsecaseContext.ipcOutVideoPrm.noNotifyMode              = TRUE;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.ipcOutVideoId,
        gHdDvrUsecaseContext.ipcOutVideoPrm
    );

    /*ipcInVpssId---Q0--> mergeId[MERGE_DISPLAY] */
    gHdDvrUsecaseContext.ipcInVpssPrm.inQueParams.prevLinkId    = gHdDvrUsecaseContext.ipcOutVideoId;
    gHdDvrUsecaseContext.ipcInVpssPrm.inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.ipcInVpssPrm.numOutQue                 = 1;
    gHdDvrUsecaseContext.ipcInVpssPrm.outQueParams[0].nextLink  = gHdDvrUsecaseContext.mergeId[MERGE_DISPLAY];
    gHdDvrUsecaseContext.ipcInVpssPrm.notifyNextLink            = TRUE;
    gHdDvrUsecaseContext.ipcInVpssPrm.notifyPrevLink            = FALSE;
    gHdDvrUsecaseContext.ipcInVpssPrm.noNotifyMode              = TRUE;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.ipcInVpssId,
        gHdDvrUsecaseContext.ipcInVpssPrm
    );
}

static
Void multich_hddvr_connect_display_links()
{
    /* MERGE Link
        to merge input for display from low res live, playback and high res live
    */
    gHdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].numInQue = 3;
    gHdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[0].prevLinkId    = gHdDvrUsecaseContext.dupId[DUP_Q_RES_CAPTURE];
    gHdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[0].prevLinkQueId = 0;
    gHdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[1].prevLinkId    = gHdDvrUsecaseContext.ipcInVpssId;
    gHdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[1].prevLinkQueId = 0;
    gHdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[2].prevLinkId    = gHdDvrUsecaseContext.dupId[DUP_FULL_RES_CAPTURE];
    gHdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[2].prevLinkQueId = 0;
    gHdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].outQueParams.nextLink        = gHdDvrUsecaseContext.dupId[DUP_DISPLAY];
    gHdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].notifyNextLink               = TRUE;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.mergeId[MERGE_DISPLAY],
        gHdDvrUsecaseContext.mergePrm[MERGE_DISPLAY]
    );

    /*dupId[DUP_DISPLAY]---Q0--> swMsId[0] */
    /*dupId[DUP_DISPLAY]---Q1--> swMsId[1] */
    gHdDvrUsecaseContext.dupPrm[DUP_DISPLAY].inQueParams.prevLinkId       = gHdDvrUsecaseContext.mergeId[MERGE_DISPLAY];
    gHdDvrUsecaseContext.dupPrm[DUP_DISPLAY].inQueParams.prevLinkQueId      = 0;
    gHdDvrUsecaseContext.dupPrm[DUP_DISPLAY].numOutQue                      = 2;
    gHdDvrUsecaseContext.dupPrm[DUP_DISPLAY].outQueParams[0].nextLink       = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)];
    gHdDvrUsecaseContext.dupPrm[DUP_DISPLAY].outQueParams[1].nextLink       = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)];
    gHdDvrUsecaseContext.dupPrm[DUP_DISPLAY].notifyNextLink                 = TRUE;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.dupId[DUP_DISPLAY],
        gHdDvrUsecaseContext.dupPrm[DUP_DISPLAY]
    );

    /* Avsync configuration for SwMs[0] */
    multich_hddvr_set_avsync_prm(&gHdDvrUsecaseContext.avsyncCfg[0],
                                0,
                                gHdDvrUsecaseContext.dupId[DUP_DISPLAY],
                                0);

    /*swMsId[0]---Q0--> displayId[VDIS_DEV_HDMI] */
    multich_hddvr_set_swms_prm(&gHdDvrUsecaseContext.swMsPrm[0], 0);
    gHdDvrUsecaseContext.swMsPrm[0].inQueParams.prevLinkId    = gHdDvrUsecaseContext.dupId[DUP_DISPLAY];
    gHdDvrUsecaseContext.swMsPrm[0].inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.swMsPrm[0].outQueParams.nextLink     = Vdis_getDisplayId(VDIS_DEV_HDMI);

    MULTICH_HDDVR_CREATE_LINK(
        gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)],
        gHdDvrUsecaseContext.swMsPrm[0]
    );

    gHdDvrUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)] = gHdDvrUsecaseContext.swMsPrm[0].layoutPrm;

    /* Avsync configuration for SwMs[1] */
    multich_hddvr_set_avsync_prm(&gHdDvrUsecaseContext.avsyncCfg[1],
                                1,
                                gHdDvrUsecaseContext.dupId[DUP_DISPLAY],
                                1);

    /*swMsId[1]---Q1--> displayId[VDIS_DEV_SD] */
    multich_hddvr_set_swms_prm(&gHdDvrUsecaseContext.swMsPrm[1], 1);
    gHdDvrUsecaseContext.swMsPrm[1].inQueParams.prevLinkId    = gHdDvrUsecaseContext.dupId[DUP_DISPLAY];
    gHdDvrUsecaseContext.swMsPrm[1].inQueParams.prevLinkQueId = 1;
    gHdDvrUsecaseContext.swMsPrm[1].outQueParams.nextLink     = Vdis_getDisplayId(VDIS_DEV_SD);

    MULTICH_HDDVR_CREATE_LINK(
        gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)],
        gHdDvrUsecaseContext.swMsPrm[1]
    );

    gHdDvrUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)] = gHdDvrUsecaseContext.swMsPrm[1].layoutPrm;

    multich_hddvr_set_display_prms(
        &gHdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)],
        gHdDvrUsecaseContext.swMsPrm[0].initOutRes
    );
    gHdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)].numInputQueues = 1;
    gHdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)].inQueParams[0].prevLinkId    = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)];
    gHdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)].inQueParams[0].prevLinkQueId = 0;

    MULTICH_HDDVR_CREATE_LINK(
        Vdis_getDisplayId(VDIS_DEV_HDMI),
        gHdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]
    );

    multich_hddvr_set_display_prms(
        &gHdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)],
        gHdDvrUsecaseContext.swMsPrm[1].initOutRes
    );
    gHdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)].numInputQueues = 1;
    gHdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)].inQueParams[0].prevLinkId    = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)];
    gHdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)].inQueParams[0].prevLinkQueId = 0;

    MULTICH_HDDVR_CREATE_LINK(
        Vdis_getDisplayId(VDIS_DEV_SD),
        gHdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)]
    );
}

static
Void multich_hddvr_connect_links(Bool   enableScdAlgLink)
{
    DeiLink_OutputScaleFactor outScaleFactorDeiSc, outScaleFactorVipSc;


    #ifdef ENABLE_FAKE_HD_MODE
    multich_hddvr_configure_extvideodecoder_prm();
    #else
    gVcapModuleContext.vcapConfig.enableConfigExtVideoDecoder = FALSE;
    #endif

    /* Capture Link */
    multich_hddvr_set_capture_prm(&gHdDvrUsecaseContext.capturePrm);
    gHdDvrUsecaseContext.capturePrm.outQueParams[0].nextLink = gHdDvrUsecaseContext.dupId[DUP_FULL_RES_CAPTURE];

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.captureId,
        gHdDvrUsecaseContext.capturePrm
    );

    #ifdef ENABLE_FAKE_HD_MODE
    {
        CaptureLink_SkipOddFields captureSkipOddFields;

        captureSkipOddFields.queId = 0;
        captureSkipOddFields.skipOddFieldsChBitMask = 0xFFFFFFFF; /* all CHs */
        captureSkipOddFields.oddFieldSkipRatio      = CAPTURE_LINK_ODD_FIELD_SKIP_ALL;

        System_linkControl(
            gVcapModuleContext.captureId,
            CAPTURE_LINK_CMD_SKIP_ODD_FIELDS,
            &captureSkipOddFields,
            sizeof(captureSkipOddFields),
            TRUE);
    }
    #endif

    /* DUP Full resolution capture
        1st DUP for high res live preview
        2nd DUP for H264 encode scaling, low res live preview scaling
        3rd DUP for MJPEG encode
    */
    gHdDvrUsecaseContext.dupPrm[DUP_FULL_RES_CAPTURE].inQueParams.prevLinkId    = gVcapModuleContext.captureId;
    gHdDvrUsecaseContext.dupPrm[DUP_FULL_RES_CAPTURE].inQueParams.prevLinkQueId = 0;

    gHdDvrUsecaseContext.dupPrm[DUP_FULL_RES_CAPTURE].numOutQue                 = 3;
    gHdDvrUsecaseContext.dupPrm[DUP_FULL_RES_CAPTURE].outQueParams[0].nextLink  = gHdDvrUsecaseContext.mergeId[MERGE_DISPLAY];
    gHdDvrUsecaseContext.dupPrm[DUP_FULL_RES_CAPTURE].outQueParams[1].nextLink  = gHdDvrUsecaseContext.selectId[SELECT_FULL_RES_CAPTURE];
    gHdDvrUsecaseContext.dupPrm[DUP_FULL_RES_CAPTURE].outQueParams[2].nextLink  = gVcapModuleContext.nsfId[NSF_MJPEG];

    gHdDvrUsecaseContext.dupPrm[DUP_FULL_RES_CAPTURE].notifyNextLink            = TRUE;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.dupId[DUP_FULL_RES_CAPTURE],
        gHdDvrUsecaseContext.dupPrm[DUP_FULL_RES_CAPTURE]
    );

    /* SELECT link
        to split 4CH high res into two paths of 2CHs high res each
    */
    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].numOutQue = 2;

    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].inQueParams.prevLinkId    = gHdDvrUsecaseContext.dupId[DUP_FULL_RES_CAPTURE];
    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].inQueParams.prevLinkQueId = 1;

    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].outQueParams[0].nextLink  = gVcapModuleContext.deiId[DEIHQ_FULL_RES];
    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].outQueParams[1].nextLink  = gVcapModuleContext.deiId[DEI_FULL_RES];

    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].outQueChInfo[0].outQueId  = 0;
    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].outQueChInfo[0].numOutCh  = 2;
    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].outQueChInfo[0].inChNum[0]= 0;
    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].outQueChInfo[0].inChNum[1]= 1;

    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].outQueChInfo[1].outQueId  = 1;
    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].outQueChInfo[1].numOutCh  = 2;
    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].outQueChInfo[1].inChNum[0]= 2;
    gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE].outQueChInfo[1].inChNum[1]= 3;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.selectId[SELECT_FULL_RES_CAPTURE],
        gHdDvrUsecaseContext.selectPrm[SELECT_FULL_RES_CAPTURE]
    );

    /* DEIHQ, DEI link create
        used in bypass mode as scalar
        1st output is through VIP-SC YUV420 and is used for high res encode
        2nd output is Quater of high res input and is used
                as low res input for live preview
                as well as secondary encode scaling
    */

    /* 1/2 scaling */
    outScaleFactorDeiSc.scaleMode = DEI_SCALE_MODE_RATIO;
    outScaleFactorDeiSc.ratio.widthRatio.numerator     = 1;
    outScaleFactorDeiSc.ratio.widthRatio.denominator   = 2;
    outScaleFactorDeiSc.ratio.heightRatio.numerator    = 1;
    outScaleFactorDeiSc.ratio.heightRatio.denominator  = 2;

    /* 1:1 scaling */
    outScaleFactorVipSc.scaleMode = DEI_SCALE_MODE_RATIO;
    outScaleFactorVipSc.ratio.widthRatio.numerator     = 1;
    outScaleFactorVipSc.ratio.widthRatio.denominator   = 1;
    outScaleFactorVipSc.ratio.heightRatio.numerator    = 1;
    outScaleFactorVipSc.ratio.heightRatio.denominator  = 1;

    /* DEIHQ */
    multich_hddvr_set_dei_prm(&gHdDvrUsecaseContext.deiPrm[DEIHQ_FULL_RES], &outScaleFactorDeiSc, &outScaleFactorVipSc);

    gHdDvrUsecaseContext.deiPrm[DEIHQ_FULL_RES].inQueParams.prevLinkId    = gHdDvrUsecaseContext.selectId[SELECT_FULL_RES_CAPTURE];
    gHdDvrUsecaseContext.deiPrm[DEIHQ_FULL_RES].inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.deiPrm[DEIHQ_FULL_RES].outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink = gHdDvrUsecaseContext.mergeId[MERGE_Q_RES_CAPTURE];
    gHdDvrUsecaseContext.deiPrm[DEIHQ_FULL_RES].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = gHdDvrUsecaseContext.mergeId[MERGE_ENCODE];

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.deiId[DEIHQ_FULL_RES],
        gHdDvrUsecaseContext.deiPrm[DEIHQ_FULL_RES]
    );

    /* DEI */
    multich_hddvr_set_dei_prm(&gHdDvrUsecaseContext.deiPrm[DEI_FULL_RES]  , &outScaleFactorDeiSc, &outScaleFactorVipSc);

    gHdDvrUsecaseContext.deiPrm[DEI_FULL_RES].inQueParams.prevLinkId    = gHdDvrUsecaseContext.selectId[SELECT_FULL_RES_CAPTURE];
    gHdDvrUsecaseContext.deiPrm[DEI_FULL_RES].inQueParams.prevLinkQueId = 1;
    gHdDvrUsecaseContext.deiPrm[DEI_FULL_RES].outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink = gHdDvrUsecaseContext.mergeId[MERGE_Q_RES_CAPTURE];
    gHdDvrUsecaseContext.deiPrm[DEI_FULL_RES].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = gHdDvrUsecaseContext.mergeId[MERGE_ENCODE];

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.deiId[DEI_FULL_RES],
        gHdDvrUsecaseContext.deiPrm[DEI_FULL_RES]
    );

    /* MERGE Link
        to merge output DEI-SC low res capture into a single path so that it can be
        given for scaling for live preview and secondary channel encode
    */
    gHdDvrUsecaseContext.mergePrm[MERGE_Q_RES_CAPTURE].numInQue = 2;
    gHdDvrUsecaseContext.mergePrm[MERGE_Q_RES_CAPTURE].inQueParams[0].prevLinkId    = gVcapModuleContext.deiId[DEIHQ_FULL_RES];
    gHdDvrUsecaseContext.mergePrm[MERGE_Q_RES_CAPTURE].inQueParams[0].prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC;
    gHdDvrUsecaseContext.mergePrm[MERGE_Q_RES_CAPTURE].inQueParams[1].prevLinkId    = gVcapModuleContext.deiId[DEI_FULL_RES];
    gHdDvrUsecaseContext.mergePrm[MERGE_Q_RES_CAPTURE].inQueParams[1].prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC;
    gHdDvrUsecaseContext.mergePrm[MERGE_Q_RES_CAPTURE].outQueParams.nextLink        = gVcapModuleContext.nsfId[NSF_Q_RES_CAPTURE];
    gHdDvrUsecaseContext.mergePrm[MERGE_Q_RES_CAPTURE].notifyNextLink               = TRUE;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.mergeId[MERGE_Q_RES_CAPTURE],
        gHdDvrUsecaseContext.mergePrm[MERGE_Q_RES_CAPTURE]
    );

    /* NSF link
        to convert YUV422 data to YUV420, this is to save DDR BW (double read issue with YUV422 input) at
        subsequent steps of scaling
    */
    multich_hddvr_set_nsf_prm(&gHdDvrUsecaseContext.nsfPrm[NSF_Q_RES_CAPTURE], NUM_BUFS_PER_CH_NSF_Q_RES_CAPTURE, 30);

    gHdDvrUsecaseContext.nsfPrm[NSF_Q_RES_CAPTURE].inQueParams.prevLinkId    = gHdDvrUsecaseContext.mergeId[MERGE_Q_RES_CAPTURE];
    gHdDvrUsecaseContext.nsfPrm[NSF_Q_RES_CAPTURE].inQueParams.prevLinkQueId = 0;
    gHdDvrUsecaseContext.nsfPrm[NSF_Q_RES_CAPTURE].outQueParams[0].nextLink  = gHdDvrUsecaseContext.dupId[DUP_Q_RES_CAPTURE];

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.nsfId[NSF_Q_RES_CAPTURE],
        gHdDvrUsecaseContext.nsfPrm[NSF_Q_RES_CAPTURE]
    );

    /* DUP low resoluation scaled capture CHs
        1st DUP for live preview
        2nd DUP for H264 encode scaling, SCD scaling
    */
    gHdDvrUsecaseContext.dupPrm[DUP_Q_RES_CAPTURE].inQueParams.prevLinkId    = gVcapModuleContext.nsfId[NSF_Q_RES_CAPTURE];
    gHdDvrUsecaseContext.dupPrm[DUP_Q_RES_CAPTURE].inQueParams.prevLinkQueId = 0;

    gHdDvrUsecaseContext.dupPrm[DUP_Q_RES_CAPTURE].numOutQue                 = 2;
    gHdDvrUsecaseContext.dupPrm[DUP_Q_RES_CAPTURE].outQueParams[0].nextLink  = gHdDvrUsecaseContext.mergeId[MERGE_DISPLAY];
    gHdDvrUsecaseContext.dupPrm[DUP_Q_RES_CAPTURE].outQueParams[1].nextLink  = gVcapModuleContext.deiId[DEIHQ_Q_RES];

    gHdDvrUsecaseContext.dupPrm[DUP_Q_RES_CAPTURE].notifyNextLink            = TRUE;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.dupId[DUP_Q_RES_CAPTURE],
        gHdDvrUsecaseContext.dupPrm[DUP_Q_RES_CAPTURE]
    );

    /* DEIHQ
        used in bypass mode as scalar
        1st output is through VIP-SC YUV420 and is used for secondary stream D1 encode
        2nd output is CIF and is used for SCD
    */

    /* CIF scaling */
    outScaleFactorDeiSc.scaleMode = DEI_SCALE_MODE_ABSOLUTE;
    outScaleFactorDeiSc.absoluteResolution.outWidth  = 352;
    outScaleFactorDeiSc.absoluteResolution.outHeight = 240;

    /* D1 scaling */
    outScaleFactorVipSc.scaleMode = DEI_SCALE_MODE_ABSOLUTE;
    outScaleFactorVipSc.absoluteResolution.outWidth  = MAX_SECONDARY_WIDTH;
    outScaleFactorVipSc.absoluteResolution.outHeight = MAX_SECONDARY_HEIGHT;

    /* DEIHQ */
    multich_hddvr_set_dei_prm(&gHdDvrUsecaseContext.deiPrm[DEIHQ_Q_RES], &outScaleFactorDeiSc, &outScaleFactorVipSc);

    gHdDvrUsecaseContext.deiPrm[DEIHQ_Q_RES].inQueParams.prevLinkId    = gHdDvrUsecaseContext.dupId[DUP_Q_RES_CAPTURE];
    gHdDvrUsecaseContext.deiPrm[DEIHQ_Q_RES].inQueParams.prevLinkQueId = 1;
    gHdDvrUsecaseContext.deiPrm[DEIHQ_Q_RES].outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink = gVcapModuleContext.nsfId[NSF_SCD];
    gHdDvrUsecaseContext.deiPrm[DEIHQ_Q_RES].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = gHdDvrUsecaseContext.mergeId[MERGE_ENCODE];

    if(!enableScdAlgLink)
    {
        /* disable DEI_SC output if SCD is not enabled  */
        gHdDvrUsecaseContext.deiPrm[DEIHQ_Q_RES].enableOut[DEI_LINK_OUT_QUE_DEI_SC] = FALSE;
    }

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.deiId[DEIHQ_Q_RES],
        gHdDvrUsecaseContext.deiPrm[DEIHQ_Q_RES]
    );

    /* NSF link
        to convert YUV422 data to YUV420 for MJPEG encode, @ 1fps
    */
    multich_hddvr_set_nsf_prm(&gHdDvrUsecaseContext.nsfPrm[NSF_MJPEG], NUM_BUFS_PER_CH_NSF_MJPEG, 1);

    gHdDvrUsecaseContext.nsfPrm[NSF_MJPEG].inQueParams.prevLinkId    = gHdDvrUsecaseContext.dupId[DUP_FULL_RES_CAPTURE];
    gHdDvrUsecaseContext.nsfPrm[NSF_MJPEG].inQueParams.prevLinkQueId = 2;
    gHdDvrUsecaseContext.nsfPrm[NSF_MJPEG].outQueParams[0].nextLink  = gHdDvrUsecaseContext.mergeId[MERGE_ENCODE];

    MULTICH_HDDVR_CREATE_LINK(
        gVcapModuleContext.nsfId[NSF_MJPEG],
        gHdDvrUsecaseContext.nsfPrm[NSF_MJPEG]
    );

    /* MERGE Link
        to merge input for H264 primary encode, H264 secondary encode, MJPEG encode
    */
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].numInQue = 4;
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[0].prevLinkId    = gVcapModuleContext.deiId[DEIHQ_FULL_RES];
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[0].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[1].prevLinkId    = gVcapModuleContext.deiId[DEI_FULL_RES];
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[1].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[2].prevLinkId    = gVcapModuleContext.deiId[DEIHQ_Q_RES];
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[2].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[3].prevLinkId    = gVcapModuleContext.nsfId[NSF_MJPEG];
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[3].prevLinkQueId = 0;
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].outQueParams.nextLink        = gVcapModuleContext.ipcFramesOutVpssId[0];
    gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE].notifyNextLink               = TRUE;

    MULTICH_HDDVR_CREATE_LINK(
        gHdDvrUsecaseContext.mergeId[MERGE_ENCODE],
        gHdDvrUsecaseContext.mergePrm[MERGE_ENCODE]
    );

    multich_hddvr_connect_osd_links();

    if(enableScdAlgLink)
    {
        multich_hddvr_connect_scd_links();
    }

    multich_hddvr_connect_encode_links();
    multich_hddvr_connect_decode_links();
    multich_hddvr_connect_display_links();
}

static
void multich_hddvr_disable_all_play_ch_in_swms(VDIS_DEV vdDevId, VDIS_MOSAIC_S *psVdMosaicParam, SwMsLink_LayoutPrm *vdisLayoutPrm)
{
    UInt32 winId, chId;

    for(winId=0; winId<vdisLayoutPrm->numWin; winId++)
    {
        chId = psVdMosaicParam->chnMap[winId];

        if(chId >= MAX_NUM_CAPTURE_CHANNELS && chId < (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS))
        {
            Vdis_setChn2WinMap(vdDevId, chId, SYSTEM_SW_MS_INVALID_ID);

            vdisLayoutPrm->winInfo[winId].channelNum = SYSTEM_SW_MS_INVALID_ID;
        }
    }
}

static
int multich_hddvr_decode_ch_enable(UInt32 chId, Bool enable)
{
    UInt32 cmdId;
    DecLink_ChannelInfo decChInfo;
    Int32 status;

    if(chId >= MAX_NUM_DECODE_CHANNELS)
        return -1;

    if(enable)
    {
        cmdId = DEC_LINK_CMD_ENABLE_CHANNEL;
    }
    else
    {
        cmdId = DEC_LINK_CMD_DISABLE_CHANNEL;
    }

    decChInfo.chId     = chId;

    status = System_linkControl(gVdecModuleContext.decId, cmdId, &decChInfo, sizeof(decChInfo), TRUE);
    OSA_assert(status == 0);

    return status;
}

/*
    assume's playback channels visible in only one display
*/
static
void multich_hddvr_enable_visible_decode_channels(SwMsLink_LayoutPrm *vdisLayoutPrm)
{
    UInt32 winId, chId;
    UInt32 decodeChEnable[MAX_NUM_DECODE_CHANNELS];

    for(chId=0; chId<MAX_NUM_DECODE_CHANNELS; chId++)
    {
        decodeChEnable[chId] = FALSE;
    }

    for(winId=0; winId<vdisLayoutPrm->numWin; winId++)
    {
        chId = vdisLayoutPrm->winInfo[winId].channelNum;

        if(chId >= MAX_NUM_CAPTURE_CHANNELS && chId < (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS))
        {
            decodeChEnable[chId%MAX_NUM_DECODE_CHANNELS] = TRUE;
        }
    }

    for(chId=0; chId<MAX_NUM_DECODE_CHANNELS; chId++)
    {
        multich_hddvr_decode_ch_enable(chId, decodeChEnable[chId]);
    }
}

Void MultiCh_createHdDvr()
{

    multich_hddvr_reset_link_prms();
    multich_hddvr_set_link_ids();
    printf("\n********* Entered usecase HdDvr <816x> Cap/Enc/Dec/Dis \n\n");

    MultiCh_detectBoard();

    System_linkControl(
        SYSTEM_LINK_ID_M3VPSS,
        SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES,
        NULL,
        0,
        TRUE
        );

    System_linkControl(
        SYSTEM_LINK_ID_M3VIDEO,
        SYSTEM_COMMON_CMD_SET_CH2IVAHD_MAP_TBL,
        &systemVid_encDecIvaChMapTbl,
        sizeof(SystemVideo_Ivahd2ChMap_Tbl),
        TRUE
    );

    if ((FALSE == TILER_ENABLE_ENCODE) && (FALSE == TILER_ENABLE_DECODE))
    {
        SystemTiler_disableAllocator();
    }
    multich_hddvr_connect_links(gVsysModuleContext.vsysConfig.enableScd);

    multich_hddvr_enable_visible_decode_channels(
        &gHdDvrUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]
    );
}

Void MultiCh_deleteHdDvr()
{
    UInt32 i;

    for (i = 0; i < gHdDvrUsecaseContext.createdLinkCount; i++)
    {
        System_linkDelete (gHdDvrUsecaseContext.createdLinks[i]);
    }
    gHdDvrUsecaseContext.createdLinkCount = 0;
    multich_hddvr_reset_link_ids();

    Vcap_deleteVideoDecoder();
    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    MultiCh_prfLoadCalcEnable(FALSE, TRUE, FALSE);

    if ((FALSE == TILER_ENABLE_ENCODE) && (FALSE == TILER_ENABLE_DECODE))
    {
        SystemTiler_enableAllocator();
    }
}


Int32 MultiCh_hdDvrSetMosaicParams(VDIS_DEV vdDevId, VDIS_MOSAIC_S *psVdMosaicParam )
{
    UInt32 swMsId;
    Int32 retVal = -1;
    Uint32 numLiveCh=0, numPlayCh=0, numHighResCh=0;
    SwMsLink_LayoutPrm *vdisLayoutPrm;
    UInt32 winId, chId;

    swMsId = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(vdDevId)];
    if(swMsId==SYSTEM_LINK_ID_INVALID)
        return -1;

    vdisLayoutPrm = &gHdDvrUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(vdDevId)];

    /* Get display resolution and coordinates */
    vdisLayoutPrm->numWin = psVdMosaicParam->numberOfWindows;
    vdisLayoutPrm->onlyCh2WinMapChanged = psVdMosaicParam->onlyCh2WinMapChanged;
    vdisLayoutPrm->outputFPS = psVdMosaicParam->outputFPS;

    /* Assign each windows coordinates, size and mapping */
    for(winId=0; winId<vdisLayoutPrm->numWin; winId++)
    {
        chId = psVdMosaicParam->chnMap[winId];

        vdisLayoutPrm->winInfo[winId].channelNum         = chId;
        vdisLayoutPrm->winInfo[winId].bufAddrOffset[0u]  = -1;
        vdisLayoutPrm->winInfo[winId].bufAddrOffset[1u]  = -1;
        vdisLayoutPrm->winInfo[winId].width              = psVdMosaicParam->winList[winId].width;
        vdisLayoutPrm->winInfo[winId].height             = psVdMosaicParam->winList[winId].height;
        vdisLayoutPrm->winInfo[winId].startX             = psVdMosaicParam->winList[winId].start_X;
        vdisLayoutPrm->winInfo[winId].startY             = psVdMosaicParam->winList[winId].start_Y;
        vdisLayoutPrm->winInfo[winId].bypass             = FALSE;

        if(chId >= (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS))
        {
            /* not a valid CH ID, mark it as invalid */
            vdisLayoutPrm->winInfo[winId].channelNum = SYSTEM_SW_MS_INVALID_ID;
            continue;
        }

        if(chId < gVdisModuleContext.vdisConfig.numChannels)
        {
            Vdis_setChn2WinMap(vdDevId, chId,winId);

            if(Vdis_isEnableChn(vdDevId,chId) == FALSE)
            {
                vdisLayoutPrm->winInfo[winId].channelNum = SYSTEM_SW_MS_INVALID_ID;
            }
        }

        if(chId >= MAX_NUM_CAPTURE_CHANNELS && chId < (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS))
            numPlayCh++;
        if(chId < MAX_NUM_CAPTURE_CHANNELS )
        {
            numLiveCh++;
            if( vdisLayoutPrm->winInfo[winId].width > (MAX_PRIMARY_WIDTH/2)
                &&
                vdisLayoutPrm->winInfo[winId].height > (MAX_PRIMARY_HEIGHT/2)
                )
            {
                if(numHighResCh==0)
                {
                    /* switch to high res channel */
                    /* can have only one high res channel in a layout */
                    vdisLayoutPrm->winInfo[winId].channelNum = chId + (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS);
                }

                numHighResCh++;
            }
        }
    }

    if(vdDevId==VDIS_DEV_SD)
    {
        /*
            Cannot have playback in SDTV, so mark all playback CHs as invalid in SDTV
        */
        if(numPlayCh>0)
        {
            multich_hddvr_disable_all_play_ch_in_swms(vdDevId, psVdMosaicParam, vdisLayoutPrm);
        }
    }
    if(vdDevId==VDIS_DEV_HDMI)
    {
        /* Cannot have mix of live and playback CHs
            disable playback CHs in this case
        */
        if(numPlayCh>0 && numLiveCh>0)
        {
            multich_hddvr_disable_all_play_ch_in_swms(vdDevId, psVdMosaicParam, vdisLayoutPrm);
        }
    }

    Vdis_swMs_PrintLayoutParams(vdDevId, vdisLayoutPrm);
    System_linkControl(swMsId, SYSTEM_SW_MS_LINK_CMD_SWITCH_LAYOUT, (vdisLayoutPrm), sizeof(*vdisLayoutPrm), TRUE);

    if(vdDevId==VDIS_DEV_HDMI)
    {
        /* since only HDTV can show playback channels */
        multich_hddvr_enable_visible_decode_channels(vdisLayoutPrm);
    }

    return retVal;
}

void  MultiCh_hdDvrSwmsChReMap(VDIS_MOSAIC_S *psVdMosaicParam)
{
    Int32 winId, chId;

    /*
        Convert from SW Moisac Ch ID  to McFW CH ID

        SWMS Ch ID                            McFW CH ID
        0..3   (low res live CHs in SWMS)     0..3        Live channels in McFW
        4..7   ( playback CHs in SWMS)        4..7        Playback channels in McFW
        8..11  (high res live CHs in SWMS)    0..3        Live channels in McFW
     */

    for (winId = 0; winId < psVdMosaicParam->numberOfWindows; winId++)
    {
        chId = psVdMosaicParam->chnMap[winId];

        if ( chId >= (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS) )
        {
            if(chId != (UInt32)(VDIS_CHN_INVALID) )
            {
                chId -= (MAX_NUM_DECODE_CHANNELS+MAX_NUM_CAPTURE_CHANNELS);
                chId = (chId % MAX_NUM_CAPTURE_CHANNELS);
            }
        }

        psVdMosaicParam->chnMap[winId] = chId;
    }
}

Int32 MultiCh_hdDvrSetCapFrameRate(VCAP_CHN vcChnId, VCAP_STRM vcStrmId, Int32 inputFrameRate, Int32 outputFrameRate)
{
    DeiLink_ChFpsParams chInfo;
    UInt32 linkId;
    Int32 retVal = -1;
    NsfLink_ChFpsParams nsfFpsInfo;

    if(vcChnId >= MAX_NUM_CAPTURE_CHANNELS)
        return -1;

    /*
        0: Primary
        1: Secondary
        2: MJPEG
    */

    if(vcStrmId >= 3)
        return -1;

    /* primary stream */
    if(vcStrmId==0)
    {
        if(vcChnId < (MAX_NUM_CAPTURE_CHANNELS/2))
            linkId = gVcapModuleContext.deiId[DEIHQ_FULL_RES];
        else
            linkId = gVcapModuleContext.deiId[DEI_FULL_RES];

        chInfo.chId             = vcChnId % (MAX_NUM_CAPTURE_CHANNELS/2);
        chInfo.streamId         = DEI_LINK_OUT_QUE_VIP_SC;
        chInfo.inputFrameRate   = inputFrameRate;
        chInfo.outputFrameRate  = outputFrameRate;

        retVal = System_linkControl(linkId, DEI_LINK_CMD_SET_FRAME_RATE, &chInfo, sizeof(chInfo), TRUE);
    }
    /* secondary stream */
    if(vcStrmId==1)
    {
        linkId = gVcapModuleContext.deiId[DEIHQ_Q_RES];

        chInfo.chId             = vcChnId;
        chInfo.streamId         = DEI_LINK_OUT_QUE_VIP_SC;
        chInfo.inputFrameRate   = inputFrameRate;
        chInfo.outputFrameRate  = outputFrameRate;

        retVal = System_linkControl(linkId, DEI_LINK_CMD_SET_FRAME_RATE, &chInfo, sizeof(chInfo), TRUE);
    }
    /* MJPEG stream */
    if(vcStrmId==2)
    {
        linkId = gVcapModuleContext.nsfId[NSF_MJPEG];

        nsfFpsInfo.chId            = vcChnId;
        nsfFpsInfo.inputFrameRate  = inputFrameRate;
        nsfFpsInfo.outputFrameRate = outputFrameRate;

        retVal = System_linkControl(linkId, NSF_LINK_CMD_SET_FRAME_RATE, &nsfFpsInfo, sizeof(nsfFpsInfo), TRUE);
    }

    return retVal;
}

Int32 MultiCh_hdDvrEnableDisableCapChn(VCAP_CHN vcChnId, VCAP_STRM vcStrmId, Bool enableChn)
{
    DeiLink_ChannelInfo chInfo;
    UInt32 linkId, cmdId;
    Int32 retVal = -1;
    NsfLink_ChFpsParams nsfFpsInfo;

    if(vcChnId >= MAX_NUM_CAPTURE_CHANNELS)
        return -1;

    /*
        0: Primary
        1: Secondary
        2: MJPEG
    */

    if(vcStrmId >= 3)
        return -1;

    /* primary stream */
    if(vcStrmId==0)
    {
        if(vcChnId < (MAX_NUM_CAPTURE_CHANNELS/2))
            linkId = gVcapModuleContext.deiId[DEIHQ_FULL_RES];
        else
            linkId = gVcapModuleContext.deiId[DEI_FULL_RES];

        if(enableChn)
            cmdId = DEI_LINK_CMD_ENABLE_CHANNEL;
        else
            cmdId = DEI_LINK_CMD_DISABLE_CHANNEL;

        chInfo.channelId = vcChnId % (MAX_NUM_CAPTURE_CHANNELS/2);
        chInfo.streamId  = DEI_LINK_OUT_QUE_VIP_SC;
        chInfo.enable    = enableChn;

        retVal = System_linkControl(linkId, cmdId, &chInfo, sizeof(chInfo), TRUE);
    }
    /* secondary stream */
    if(vcStrmId==1)
    {
        linkId = gVcapModuleContext.deiId[DEIHQ_Q_RES];

        if(enableChn)
            cmdId = DEI_LINK_CMD_ENABLE_CHANNEL;
        else
            cmdId = DEI_LINK_CMD_DISABLE_CHANNEL;

        chInfo.channelId = vcChnId;
        chInfo.streamId  = DEI_LINK_OUT_QUE_VIP_SC;
        chInfo.enable    = enableChn;

        retVal = System_linkControl(linkId, cmdId, &chInfo, sizeof(chInfo), TRUE);
    }
    /* MJPEG stream */
    if(vcStrmId==2)
    {
        /* disable channel by setting FPS to 0 */

        linkId = gVcapModuleContext.nsfId[NSF_MJPEG];

        cmdId = NSF_LINK_CMD_SET_FRAME_RATE;

        nsfFpsInfo.chId = vcChnId;
        nsfFpsInfo.inputFrameRate = 30;
        if(enableChn)
            nsfFpsInfo.outputFrameRate = 1; /* 1 fps */
        else
            nsfFpsInfo.outputFrameRate = 0; /* 0 fps */

        retVal = System_linkControl(linkId, cmdId, &nsfFpsInfo, sizeof(nsfFpsInfo), TRUE);
    }

    return retVal;
}

Int32 MultiCh_hdDvrSetCapDynamicParamChn(VCAP_CHN vcChnId, VCAP_CHN_DYNAMIC_PARAM_S *psCapChnDynaParam, VCAP_PARAMS_E paramId)
{
    UInt32 linkId;
    Int32 retVal = -1;
    Bool applyOnPrimary, applyOnSecondary;
    DeiLink_chDynamicSetOutRes deiOutRes;

    if(vcChnId >= MAX_NUM_CAPTURE_CHANNELS)
        return -1;

    /*
        VCAP_PATH_PREVIEW - resolution change not supported
        VCAP_PATH_PRIMARY_STREAM - Primary stream (full res)
        VCAP_PATH_SECONDARY_STREAM - Secondary stream (low res)

        resolution change not supported on MJPEG stream
    */

    if(paramId!=VCAP_RESOLUTION)
        return -1;

    applyOnPrimary = FALSE;
    applyOnSecondary = FALSE;

    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_PRIMARY_STREAM)
    {
        applyOnPrimary = TRUE;
    } else
    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_SECONDARY_STREAM)
    {
        applyOnSecondary = TRUE;
    } else
    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_ALL)
    {
        applyOnPrimary = TRUE;
        applyOnSecondary = TRUE;
    }

    if(applyOnPrimary)
    {
        if(vcChnId < (MAX_NUM_CAPTURE_CHANNELS/2))
            linkId = gVcapModuleContext.deiId[DEIHQ_FULL_RES];
        else
            linkId = gVcapModuleContext.deiId[DEI_FULL_RES];

        deiOutRes.queId = DEI_LINK_OUT_QUE_VIP_SC;
        deiOutRes.chId  = vcChnId % (MAX_NUM_CAPTURE_CHANNELS/2);

        retVal = System_linkControl(linkId, DEI_LINK_CMD_GET_OUTPUTRESOLUTION, &deiOutRes, sizeof(deiOutRes), TRUE);

        if(retVal!=0)
            return retVal;

        deiOutRes.width  = psCapChnDynaParam->chDynamicRes.width;
        deiOutRes.height = psCapChnDynaParam->chDynamicRes.height;

        if(deiOutRes.width > MAX_PRIMARY_WIDTH)
            deiOutRes.width = MAX_PRIMARY_WIDTH;
        if(deiOutRes.height > MAX_PRIMARY_HEIGHT)
            deiOutRes.height = MAX_PRIMARY_HEIGHT;

        retVal = System_linkControl(linkId, DEI_LINK_CMD_SET_OUTPUTRESOLUTION, &deiOutRes, sizeof(deiOutRes), TRUE);
    }
    if(applyOnSecondary)
    {
        linkId = gVcapModuleContext.deiId[DEIHQ_Q_RES];

        deiOutRes.queId = DEI_LINK_OUT_QUE_VIP_SC;
        deiOutRes.chId  = vcChnId;

        retVal = System_linkControl(linkId, DEI_LINK_CMD_GET_OUTPUTRESOLUTION, &deiOutRes, sizeof(deiOutRes), TRUE);

        if(retVal!=0)
            return retVal;

        deiOutRes.width  = psCapChnDynaParam->chDynamicRes.width;
        deiOutRes.height = psCapChnDynaParam->chDynamicRes.height;

        if(deiOutRes.width > MAX_SECONDARY_WIDTH)
            deiOutRes.width = MAX_SECONDARY_WIDTH;
        if(deiOutRes.height > MAX_SECONDARY_HEIGHT)
            deiOutRes.height = MAX_SECONDARY_HEIGHT;

        retVal = System_linkControl(linkId, DEI_LINK_CMD_SET_OUTPUTRESOLUTION, &deiOutRes, sizeof(deiOutRes), TRUE);
    }

    return retVal;
}

Int32 MultiCh_hdDvrGetCapDynamicParamChn(VCAP_CHN vcChnId, VCAP_CHN_DYNAMIC_PARAM_S *psCapChnDynaParam, VCAP_PARAMS_E paramId)
{
    UInt32 linkId;
    Int32 retVal = -1;
    Bool applyOnPrimary, applyOnSecondary;
    DeiLink_chDynamicSetOutRes deiOutRes;

    psCapChnDynaParam->chDynamicRes.width  = 0;
    psCapChnDynaParam->chDynamicRes.height = 0;

    if(vcChnId >= MAX_NUM_CAPTURE_CHANNELS)
        return -1;

    /*
        VCAP_PATH_PREVIEW - resolution get not supported
        VCAP_PATH_PRIMARY_STREAM - Primary stream (full res)
        VCAP_PATH_SECONDARY_STREAM - Secondary stream (low res)

        resolution get not supported on MJPEG stream
    */

    if(paramId!=VCAP_RESOLUTION)
        return -1;

    applyOnPrimary = FALSE;
    applyOnSecondary = FALSE;

    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_PRIMARY_STREAM)
    {
        applyOnPrimary = TRUE;
    } else
    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_SECONDARY_STREAM)
    {
        applyOnSecondary = TRUE;
    } else
    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_ALL)
    {
        /* get cannot be on 'all' */
        return -1;
    }

    if(applyOnPrimary)
    {
        if(vcChnId < (MAX_NUM_CAPTURE_CHANNELS/2))
            linkId = gVcapModuleContext.deiId[DEIHQ_FULL_RES];
        else
            linkId = gVcapModuleContext.deiId[DEI_FULL_RES];

        deiOutRes.queId = DEI_LINK_OUT_QUE_VIP_SC;
        deiOutRes.chId  = vcChnId % (MAX_NUM_CAPTURE_CHANNELS/2);
    }
    else
    if(applyOnSecondary)
    {
        linkId = gVcapModuleContext.deiId[DEIHQ_Q_RES];

        deiOutRes.queId = DEI_LINK_OUT_QUE_VIP_SC;
        deiOutRes.chId  = vcChnId;
    }
    else
    {
        return -1;
    }

    retVal = System_linkControl(linkId, DEI_LINK_CMD_GET_OUTPUTRESOLUTION, &deiOutRes, sizeof(deiOutRes), TRUE);
    if(retVal!=0)
        return retVal;

    psCapChnDynaParam->chDynamicRes.width = deiOutRes.width;
    psCapChnDynaParam->chDynamicRes.height = deiOutRes.height;

    return retVal;
}
