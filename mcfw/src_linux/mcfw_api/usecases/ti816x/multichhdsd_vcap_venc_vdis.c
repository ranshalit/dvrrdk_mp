/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

#include "mcfw/src_linux/mcfw_api/usecases/multich_common.h"
#include "mcfw/src_linux/mcfw_api/usecases/multich_ipcbits.h"
#include "mcfw/src_linux/mcfw_api/ti_vdis_priv.h"
#include "mcfw/src_linux/devices/tw2968/src/tw2968_priv.h"
#include "mcfw/src_linux/devices/tvp5158/src/tvp5158_priv.h"
#include "mcfw/interfaces/link_api/system_tiler.h"
#include "mcfw/interfaces/link_api/avsync_hlos.h"
#include "mcfw/interfaces/ti_vdis_timings.h"

/*

                                                                                Capture
                                                                                  |
                                                                                  |  2CH 1080p30 + 8CH D1
                                                                                  |  YUV422
                                                                                SELECT_HD_SD
                                                                        2 1080p |    | 8D1
                                                                                |    |
                                                                              DUP_HD +---------------+
                                                                              | | |                  |
                                                (for live preview CH0, 1)     | | +------------------]---------------------------+
                                    +-----------------------------------------+ |                    |                           |
                                    |                                           |                    |                           |
                                    |                                           |                    |                           |
                                    |                                           |                    |                           |
                                    |                                           |                    |                           |
    IPC BITS OUT (A8)               |                                2CH 1080p30|                    | 8CH D1                    |
        |                           |                                 YUV422    |                    |  YUV422                   |
    IPC BITS IN  (Video M3)         |                                           |                    |                           |
        |                           |                                           DEI_HD               DEI_SD                      |
    Decode (N CH 1080p30 h264)      |                                           |     |             ============                 |
        |                           |                                           |     |2CH 540p30   | |   | |  |                 |
    IPC M3 OUT (Video M3)           |                                           |     | YUV422      | |   | |  |                 |
        |                           |                                         (VIP-SC)|           (VIP-SC)| |  |                 |
    IPC M3 IN  (VPSS M3)            |                                           |     |             | |   | |  |                 |
        |                           |                                           |     |             | |   | |  |                 |
        |                           |                                           |    (DEI-SC)       | |  (DEI-SC)                |(for MJPEG)
        |                           |                                           |     |             | |   | |  |                 |
        |                           |                          +----------------]-----+             | |   | |  | 8CH 1fps 480p   |
        |                           |  (live preview CH2..9    |                |                   | |   | |  | MJPEG           |
        |                           | +------------------------]----------------]-------------------]-]---+ |  +--------------+  |
        |                           | |                        |                |   8CH 480p30      | |     |                 |  |
        |                           | |                        |                |    YUV422         | |     |                 MERGE_MJPEG
        |                           | |                        |                |                   | |     +-------------+     |
        |                           | |                        |                |                   | |                   |   NSF_MJPEG
        |                           | |                        |     2CH 1080p30|          8CH 480p30 | 8CH 240p30        |    |  2CH 1080p + 8CH D1 1fps
        |                           | |                        |       YUV420   |          YUV420   | | YUV420            |    |   YUV420
        |                           | |                        |                |                   | | (for secondary encode) |
        |                           | |                        |                |                   | +---------+         |    |
        |                           | |                        NSF_LOW_RES      |                   |           |         |    |
        +-------------------------+ | |                         |  2CH 540p30   |                   +----------+|  +------]----+
            (for display)         | | |                         |   YUV420      |    (for primary encode)      ||  |      |
                                  | | |                        DUP_LOW_RES      | (for primary encode)         ||  |      |
                                  | | |  (for live preview)    | |              +----------------------------+ ||  |      |
                                +-|-]-]------------------------+ |                                           | ||  |      |
                                | | | |                          |                                           | ||  |      |
                                MERGE_DISPLAY                    |                                           | ||  |      |
                                    |                            |                                           | ||  |      |
                                    |                         +--+                                           | ||  |      |
                                   DUP_DISPLAY    2CH 540p30  |                                              | ||  |      |
                                   | |             YUV420     |                                              | ||  |      |
                                   | |                        |                                              | ||  |      |
                              +----+ +---+                  DEI_LOW_RES                                      | ||  |      |
                              |          |                  |     |                                          | ||  |      |
                            SWMS0       SWMS1               |     |2CH 240p15                                | ||  |      |
                             (SC5)      (SC2)               |     | YUV422                                   | ||  |      |
                              |          |                (VIP-SC)|                                          | ||  |      |
                   HDDAC --- HDMI       SDDAC               |     |                                          | ||  |      |
                      (tied VENC)                           |     |                                          | ||  |      |
                                                            |    (DEI-SC)                                    | ||  |      | 8CH 240p 15
                                                            |     |                                          | ||  |      | YUV420
                                                            |     |                                          | ||  |      |
                                                            |     |                                          | ||  |      |
                                                            |     +------------------------------------------]-]]--]----+ |
                                                            |                                                | ||  |    | |
                                               2CH 480p30   |                                                | ||  |    | |
                                                YUV420      |                                                | ||  |  MERGE_SCD
                                                            |                                                | ||  |    |
                                                            |                                                | ||  |    |
                                                            |                                                | ||  |   NSF_SCD
                                                            |                                                | ||  |    | 10CH 240p 15
                                                            |                                                | ||  |    | YUV420
                                                            |                                                | ||  |    |
                                                            +------------------------------------------------]-]|+ |  IPC Frames Out0 (M3)--<<process link>>--IPC Frames In(DSP)--ALGLINK SCD
                                                                     (for secondary encode)                  | ||| |
                                                                                                             MERGE_ENCODE
                                                                                                               |
                                                                                                               |
                                                                                                      IPC Frames Out0 (M3)--<<process link>>--IPC Frames In(DSP)--ALGLINK SWOSD
                                                                                                               |
                                                                                                               |
                                                                                       +-----------------------+
                                                                                       |
                                                                                   IPC OUT (M3)
                                                                                       |
                                                                                   IPC IN  (M3)
                                                                                       |
                                                                                   Encode (10CH primary + 10CH secondary + 10CH MJPEG)
                                                                                       |
                                                                                   IPC BITS OUT (M3)
                                                                                       |
                                                                                   IPC BITS IN  (A8)
*/


#define ENABLE_FAKE_HD_MODE

#define TILER_ENABLE_DECODE      (TRUE)
#define TILER_ENABLE_ENCODE      (TRUE)


/*
    Encode
    CH  0,  1,                        - 1080p30 H264 Primary CH
    CH  2,  3,  4,  5,  6,  7,  8,  9 -  480p30 H264 Primary CH
    CH 10, 11,                        -  480p30 H264 Secondary CH
    CH 12, 13, 14, 15, 16, 17, 18, 19 -  240p30 H264 Secondary CH
    CH 20, 21                         - 1080p1  MJPEG
    CH 22, 23, 24, 25, 26, 27, 28, 29 -  480p1  MJPEG

    Decode
    CH  0,  1,                        - 1080p30/480p H264
    CH  2,  3,  4,  5,  6,  7         - 480p H264
*/
static SystemVideo_Ivahd2ChMap_Tbl systemVid_encDecIvaChMapTbl =
{
    .isPopulated = 1,
    .ivaMap[0] =
    {
        .EncNumCh  = 2,
        .EncChList = {0, 1 },
        .DecNumCh  = 0,
        .DecChList = {3 },
    },
    .ivaMap[1] =
    {
        .EncNumCh  = 10,
        .EncChList = {2, 3, 4, 5,
                      6, 7, 8, 9,
                      10, 11
                     },
        .DecNumCh  = 0,
    },
    .ivaMap[2] =
    {
        .EncNumCh  = 18,
        .EncChList = { 12, 13, 14, 15,
                       16, 17, 18, 19,
                       20, 21, 22, 23,
                       24, 25, 26, 27,
                       28, 29
                     },
        .DecNumCh  = 7,
        .DecChList = { 1, 2, 3, 4,
                       5, 6, 7
                     },
    },
};


#define MULTICH_HDSD_DVR_USECASE_MAX_NUM_LINKS    (64)

#define DUP_HD                      (0)
#define DUP_LOW_RES                 (1)
#define DUP_DISPLAY                 (2)
#define NUM_DUP_LINK                (3)

#define MERGE_MJPEG                 (0)
#define MERGE_SCD                   (1)
#define MERGE_DISPLAY               (2)
#define MERGE_ENCODE                (3)
#define NUM_MERGE_LINK              (4)

#define SELECT_HD_SD                (0)
#define NUM_SELECT_LINK             (1)

#define NSF_MJPEG                   (0)
#define NSF_LOW_RES                 (1)
#define NSF_SCD                     (2)
#define NUM_NSF_LINK                (3)

#define DEI_HD                      (0)
#define DEI_LOW_RES                 (1)
#define DEI_SD                      (2)
#define NUM_DEI_LINK                (3)

#define MAX_NUM_CAPTURE_DEVICES     (4)
#define MAX_NUM_DECODE_CHANNELS     (8)
#define MAX_NUM_CAPTURE_CHANNELS    (10)
#define MAX_NUM_HD_CAPTURE_CHANNELS (2)

#define MAX_HD_PRIMARY_WIDTH           (1920)
#define MAX_HD_PRIMARY_HEIGHT          (1080)

#define MAX_HD_SECONDARY_WIDTH         (720)
#define MAX_HD_SECONDARY_HEIGHT        (480)

#define MAX_SD_PRIMARY_WIDTH           (720)
#define MAX_SD_PRIMARY_HEIGHT          (480)

#define MAX_SD_SECONDARY_WIDTH         (352)
#define MAX_SD_SECONDARY_HEIGHT        (240)


#define     NUM_BUFS_PER_CH_CAPTURE              (4)
#define     NUM_BUFS_PER_CH_DEI                  (4)
#define     NUM_BUFS_PER_CH_DEC                  (4)
#define     NUM_BUFS_PER_CH_SWMS                 (4)
#define     NUM_BUFS_PER_CH_ENC_PRI              (4)
#define     NUM_BUFS_PER_CH_ENC_SEC              (4)
#define     NUM_BUFS_PER_CH_BITSOUT              (50)
#define     NUM_BUFS_PER_CH_BITSOUT_SCD          (2)
#define     NUM_BUFS_PER_CH_NSF_LOW_RES          (4)
#define     NUM_BUFS_PER_CH_NSF_SCD              (4)
#define     NUM_BUFS_PER_CH_NSF_MJPEG            (2)

#define     ENC_LINK_HD_STREAM_POOL_ID           (0)
#define     ENC_LINK_SD_STREAM_POOL_ID           (1)
#define     ENC_LINK_CIF_STREAM_POOL_ID          (2)

#define     BIT_BUF_LENGTH_LIMIT_FACTOR_HD       (5)

#define     MAX_BUFFERING_QUEUE_LEN_PER_CH       (6)

typedef struct {

    UInt32 ipcBitsOutDSPId;

    UInt32 mergeId[NUM_MERGE_LINK];
    UInt32 dupId[NUM_DUP_LINK];

    UInt32 grpxId[VDIS_DEV_MAX];

    UInt32 ipcOutVpssId;
    UInt32 ipcInVpssId;
    UInt32 ipcOutVideoId;
    UInt32 ipcInVideoId;

    UInt32 selectId[NUM_SELECT_LINK];

    UInt32 createdLinkCount;
    UInt32 createdLinks[MULTICH_HDSD_DVR_USECASE_MAX_NUM_LINKS];

    CaptureLink_CreateParams          capturePrm;
    NsfLink_CreateParams              nsfPrm[NUM_NSF_LINK];
    DeiLink_CreateParams              deiPrm[NUM_DEI_LINK];
    SwMsLink_CreateParams             swMsPrm[VDIS_DEV_MAX];
    AvsyncLink_LinkSynchConfigParams  avsyncCfg[2];
    DisplayLink_CreateParams          displayPrm[VDIS_DEV_MAX];

    SelectLink_CreateParams           selectPrm[NUM_SELECT_LINK];
    MergeLink_CreateParams            mergePrm[NUM_MERGE_LINK];
    DupLink_CreateParams              dupPrm[NUM_DUP_LINK];

    IpcLink_CreateParams              ipcOutVideoPrm;
    IpcLink_CreateParams              ipcInVpssPrm;
    IpcLink_CreateParams              ipcOutVpssPrm;
    IpcLink_CreateParams              ipcInVideoPrm;
    IpcBitsOutLinkHLOS_CreateParams   ipcBitsOutHostPrm;
    IpcBitsInLinkRTOS_CreateParams    ipcBitsInVideoPrm;
    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutVideoPrm;
    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutDspPrm;
    IpcBitsInLinkHLOS_CreateParams    ipcBitsInHostPrm[2];
    IpcFramesOutLinkRTOS_CreateParams ipcFramesOutVpssPrm[2];
    IpcFramesInLinkRTOS_CreateParams  ipcFramesInDspPrm[2];

    EncLink_CreateParams              encPrm;
    DecLink_CreateParams              decPrm;

    AlgLink_CreateParams              dspAlgPrm[2];

    VCAP_DEVICE_CREATE_PARAM_S        vidDecVideoModeArgs[MAX_NUM_CAPTURE_DEVICES];

    SwMsLink_LayoutPrm                swmsLayoutPrm[VDIS_DEV_MAX];

} MultiChHdSdDvr_Context;


MultiChHdSdDvr_Context gHdSdDvrUsecaseContext =
{
    .createdLinkCount           = 0
};


static
Void multich_hdsddvr_register_created_link(MultiChHdSdDvr_Context *pContext,
                                                    UInt32 linkID)
{
    OSA_assert(pContext->createdLinkCount < OSA_ARRAYSIZE(pContext->createdLinks));
    pContext->createdLinks[pContext->createdLinkCount] = linkID;
    pContext->createdLinkCount++;
}

#define MULTICH_HDSDDVR_CREATE_LINK(linkID,createPrm)                \
    do                                                             \
    {                                                              \
        System_linkCreate(linkID,&createPrm,sizeof(createPrm));    \
        multich_hdsddvr_register_created_link(&gHdSdDvrUsecaseContext, \
                                                linkID);           \
    } while (0)

static
Void multich_hdsddvr_reset_link_prms()
{
    int i;

    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHdSdDvrUsecaseContext.ipcOutVpssPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHdSdDvrUsecaseContext.ipcInVideoPrm);

    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHdSdDvrUsecaseContext.ipcOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHdSdDvrUsecaseContext.ipcInVpssPrm);

    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams,gHdSdDvrUsecaseContext.ipcBitsOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,gHdSdDvrUsecaseContext.ipcBitsInHostPrm[0]);

    IpcBitsOutLinkHLOS_CreateParams_Init(&gHdSdDvrUsecaseContext.ipcBitsOutHostPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkRTOS_CreateParams,gHdSdDvrUsecaseContext.ipcBitsInVideoPrm);

    IpcBitsOutLinkRTOS_CreateParams_Init(&gHdSdDvrUsecaseContext.ipcBitsOutDspPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,gHdSdDvrUsecaseContext.ipcBitsInHostPrm[1]);

    MULTICH_INIT_STRUCT(IpcFramesOutLinkRTOS_CreateParams,gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[0]);
    MULTICH_INIT_STRUCT(IpcFramesInLinkRTOS_CreateParams,gHdSdDvrUsecaseContext.ipcFramesInDspPrm[0]);

    MULTICH_INIT_STRUCT(IpcFramesOutLinkRTOS_CreateParams,gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1]);
    MULTICH_INIT_STRUCT(IpcFramesInLinkRTOS_CreateParams,gHdSdDvrUsecaseContext.ipcFramesInDspPrm[1]);

    CaptureLink_CreateParams_Init(&gHdSdDvrUsecaseContext.capturePrm);

    for (i = 0; i < OSA_ARRAYSIZE(gHdSdDvrUsecaseContext.nsfPrm);i++)
    {
        NsfLink_CreateParams_Init(&gHdSdDvrUsecaseContext.nsfPrm[i]);
    }

    for (i = 0; i < OSA_ARRAYSIZE(gHdSdDvrUsecaseContext.deiPrm);i++)
    {
        DeiLink_CreateParams_Init(&gHdSdDvrUsecaseContext.deiPrm[i]);
    }

    for (i = 0; i < OSA_ARRAYSIZE(gHdSdDvrUsecaseContext.swMsPrm);i++)
    {
        SwMsLink_CreateParams_Init(&gHdSdDvrUsecaseContext.swMsPrm[i]);
    }

    for (i = 0; i < OSA_ARRAYSIZE(gHdSdDvrUsecaseContext.avsyncCfg);i++)
    {
        AvsyncLink_LinkSynchConfigParams_Init(&gHdSdDvrUsecaseContext.avsyncCfg[i]);
    }

    for (i = 0; i < OSA_ARRAYSIZE(gHdSdDvrUsecaseContext.displayPrm);i++)
    {
        DisplayLink_CreateParams_Init(&gHdSdDvrUsecaseContext.displayPrm[i]);
    }

    DecLink_CreateParams_Init(&gHdSdDvrUsecaseContext.decPrm);
    EncLink_CreateParams_Init(&gHdSdDvrUsecaseContext.encPrm);

    for (i = 0; i < OSA_ARRAYSIZE(gHdSdDvrUsecaseContext.dspAlgPrm);i++)
    {
        AlgLink_CreateParams_Init(&gHdSdDvrUsecaseContext.dspAlgPrm[i]);
    }

    for (i = 0; i < OSA_ARRAYSIZE(gHdSdDvrUsecaseContext.selectPrm);i++)
    {
        SelectLink_CreateParams_Init(&gHdSdDvrUsecaseContext.selectPrm[i]);
    }
}

static
Void multich_hdsddvr_set_capture_prm(CaptureLink_CreateParams *capturePrm)
{
    CaptureLink_VipInstParams         *pCaptureInstPrm;
    CaptureLink_OutParams             *pCaptureOutPrm;
    UInt32 vipInstId;

    capturePrm->isPalMode                   = FALSE;
    capturePrm->numVipInst                  = 4;
    capturePrm->tilerEnable                 = FALSE;
    #ifdef ENABLE_FAKE_HD_MODE
    capturePrm->fakeHdMode                  = TRUE;
    #else
    capturePrm->fakeHdMode                  = FALSE;
    #endif
    capturePrm->enableSdCrop                = FALSE;
    capturePrm->doCropInCapture             = FALSE;
    capturePrm->numBufsPerCh                = NUM_BUFS_PER_CH_CAPTURE;
    capturePrm->numExtraBufs                = 0;
    capturePrm->maxBlindAreasPerCh          = 0;
    capturePrm->overrideNumBufsInInstPrms   = 0;

    for(vipInstId=0; vipInstId<capturePrm->numVipInst; vipInstId++)
    {
        pCaptureInstPrm                     = &capturePrm->vipInst[vipInstId];
        pCaptureInstPrm->vipInstId          = SYSTEM_CAPTURE_INST_VIP0_PORTA+vipInstId;

        if(vipInstId<MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            #ifdef ENABLE_FAKE_HD_MODE
            pCaptureInstPrm->videoDecoderId     = SYSTEM_DEVICE_VID_DEC_TVP5158_DRV;
            pCaptureInstPrm->standard           = SYSTEM_STD_D1;
            #else
            pCaptureInstPrm->videoDecoderId     = 0; /* DONT set it to TVP5158 */
            pCaptureInstPrm->standard           = SYSTEM_STD_1080P_30;
            #endif
        }
        else
        {
            pCaptureInstPrm->videoDecoderId     = SYSTEM_DEVICE_VID_DEC_TVP5158_DRV;
            pCaptureInstPrm->standard           = SYSTEM_STD_MUX_4CH_D1;
        }
        pCaptureInstPrm->inDataFormat       = SYSTEM_DF_YUV422P;
        pCaptureInstPrm->numOutput          = 1;

        pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];
        pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV422I_YUYV;
        pCaptureOutPrm->scEnable            = FALSE;
        pCaptureOutPrm->scOutWidth          = 0;
        pCaptureOutPrm->scOutHeight         = 0;
        pCaptureOutPrm->outQueId            = 0;
    }
}

static
Void multich_hdsddvr_set_nsf_prm(NsfLink_CreateParams * nsfPrm, UInt32 numBufPerCh)
{
    nsfPrm->numOutQue    = 1;
    nsfPrm->bypassNsf    = TRUE;
    nsfPrm->tilerEnable  = FALSE;
    nsfPrm->numBufsPerCh = numBufPerCh;
    nsfPrm->inputFrameRate = 30;
    nsfPrm->outputFrameRate = 30;
}

static
UInt32 multich_hdsddvr_get_videodecoder_device_id()
{
    OSA_I2cHndl i2cHandle;
    Int32 status;
    UInt32 twl_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TW2968_DRV,0);
    UInt32 tvp_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TVP5158_DRV,0);
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;
    UInt32 chipId;
    UInt32 deviceId = ~0u;

    status = OSA_i2cOpen(&i2cHandle, I2C_DEFAULT_INST_ID);
    OSA_assert(status==0);

    numRegs = 0;
    regAddr[numRegs] = DEVICE_TW2968_REG_DEVICE_ID;
    regValue[numRegs] = 0;
    numRegs++;

    regAddr[numRegs] = DEVICE_TW2968_REG_REV_ID;
    regValue[numRegs] = 0;
    numRegs++;

    status = OSA_i2cRead8(&i2cHandle,twl_i2c_addr,regAddr,regValue,numRegs);
    if (status == 0)
    {
        chipId =  (((UInt32) (regValue[0]>>6)&0x3) << 5) | ( (regValue[1] >> 3) & 0x1F ) ;
        printf("\nTWL_CHIP_ID_READ:0x%x\n",chipId);
        if (chipId == DEVICE_TW2968_CHIP_ID)
        {
            deviceId = DEVICE_VID_DEC_TW2968_DRV;
        }
    }
    if (deviceId == ~0u)
    {
        numRegs = 0;
        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_MSB;
        regValue[numRegs] = 0;
        numRegs++;

        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_LSB;
        regValue[numRegs] = 0;
        numRegs++;

        status = OSA_i2cRead8(&i2cHandle,tvp_i2c_addr,regAddr,regValue,numRegs);
        if (status == 0)
        {
            chipId = ( ( UInt32 ) regValue[0] << 8 ) | regValue[1];
            printf("\nTVP_CHIP_ID_READ:0x%x\n",chipId);
            if (DEVICE_TVP5158_CHIP_ID == chipId)
            {
                deviceId = DEVICE_VID_DEC_TVP5158_DRV;
            }
        }
    }
    OSA_assert(deviceId != ~0u);
    status = OSA_i2cClose(&i2cHandle);
    OSA_assert(status==0);
    return deviceId;
}

static
Void multich_hdsddvr_configure_extvideodecoder_prm()
{
    int i;
    VCAP_VIDEO_SOURCE_STATUS_S vidSourceStatus;
    UInt32 deviceId;
    UInt32 numCaptureDevices;

    deviceId = multich_hdsddvr_get_videodecoder_device_id();

    if (deviceId == DEVICE_VID_DEC_TW2968_DRV)
    {
        numCaptureDevices = 2;
    }
    else
    {
        numCaptureDevices = 4;
    }


    for(i = 0; i < numCaptureDevices; i++)
    {
        gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].deviceId         = deviceId;
        if(deviceId == DEVICE_VID_DEC_TW2968_DRV)
        {
            /* consider first device connected to VIP0 Port A and second device connected to VIP1 PortA */
            gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i*2;

            if(i<MAX_NUM_HD_CAPTURE_CHANNELS/2)
            {
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].numChInDevice       = 1;
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
                    DEVICE_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_EMBEDDED_SYNC;
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.standard = VSYS_STD_D1;
            }
            else
            {
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].numChInDevice       = 8;
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
                    DEVICE_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_PIXEL_MUX_EMBEDDED_SYNC;
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.standard = VSYS_STD_MUX_4CH_D1;
            }
        }
        else
        {
            /* consider first device connected to VIP0 Port A and second device connected to VIP1 PortA */
            gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i;

            if(i<MAX_NUM_HD_CAPTURE_CHANNELS)
            {
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].numChInDevice       = 1;
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
                    DEVICE_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_EMBEDDED_SYNC;
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.standard = VSYS_STD_D1;
            }
            else
            {
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].numChInDevice       = 4;
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
                    DEVICE_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_PIXEL_MUX_EMBEDDED_SYNC;
                gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.standard = VSYS_STD_MUX_4CH_D1;
            }
        }

        gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoIfMode        = DEVICE_CAPT_VIDEO_IF_MODE_8BIT;
        gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoDataFormat    = SYSTEM_DF_YUV422P;
        gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoSystem        =
                                      DEVICE_VIDEO_DECODER_VIDEO_SYSTEM_AUTO_DETECT;
        gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCropEnable    = FALSE;
        gHdSdDvrUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoAutoDetectTimeout = -1;
    }
    Vcap_configVideoDecoder(&gHdSdDvrUsecaseContext.vidDecVideoModeArgs[0],
                            numCaptureDevices);
    Vcap_getVideoSourceStatus(&vidSourceStatus);
    Vcap_setVideoSourceStatus(&vidSourceStatus);
}

static
Void multich_hdsddvr_set_hd_dei_prm(DeiLink_CreateParams *deiPrm, DeiLink_OutputScaleFactor *outScalerFactorDeiSc, DeiLink_OutputScaleFactor *outScalerFactorVipSc)
{
    UInt32 i, outId, chId;
    UInt32 outIdList[] = { DEI_LINK_OUT_QUE_DEI_SC, DEI_LINK_OUT_QUE_VIP_SC };

    deiPrm->comprEnable                 = FALSE;
    deiPrm->setVipScYuv422Format        = FALSE;
    deiPrm->inputDeiFrameRate           = 30;
    deiPrm->outputDeiFrameRate          = 30;
    deiPrm->enableDeiForceBypass        = TRUE;
    deiPrm->enableForceInterlacedInput  = FALSE;
    deiPrm->enableLineSkipSc            = FALSE;
    deiPrm->enableDualVipOut            = FALSE;
    deiPrm->enableInputFrameRateUpscale = FALSE;

    for(i=0; i<OSA_ARRAYSIZE(outIdList); i++)
    {
        outId = outIdList[i];

        deiPrm->enableOut[outId] = TRUE;

        deiPrm->tilerEnable[outId] = FALSE;
        if(outId==DEI_LINK_OUT_QUE_VIP_SC)
            deiPrm->tilerEnable[outId] = TILER_ENABLE_ENCODE;

        for(chId=0; chId<DEI_LINK_MAX_CH; chId++)
        {
            if(outId==DEI_LINK_OUT_QUE_VIP_SC)
                deiPrm->outScaleFactor[outId][chId] = *outScalerFactorVipSc;
            if(outId==DEI_LINK_OUT_QUE_DEI_SC)
                deiPrm->outScaleFactor[outId][chId] = *outScalerFactorDeiSc;
        }

        deiPrm->inputFrameRate[outId]   = 30;
        deiPrm->outputFrameRate[outId]  = 30;

        deiPrm->numBufsPerCh[outId]     = NUM_BUFS_PER_CH_DEI;
        deiPrm->generateBlankOut[outId] = FALSE;
    }
}

static
Void multich_hdsddvr_set_sd_dei_prm(DeiLink_CreateParams *deiPrm)
{
    UInt32 i, outId, chId;
    UInt32 outIdList[] = { DEI_LINK_OUT_QUE_DEI_SC,
                           DEI_LINK_OUT_QUE_VIP_SC,
                           DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT,
                           DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT,
                           DEI_LINK_OUT_QUE_DEI_SC_TERTIARY_OUT
                        };
    DeiLink_OutputScaleFactor outScaleFactorSc;

    deiPrm->comprEnable                 = FALSE;
    deiPrm->setVipScYuv422Format        = FALSE;
    deiPrm->inputDeiFrameRate           = 60;
    deiPrm->outputDeiFrameRate          = 60;
    deiPrm->enableDeiForceBypass        = FALSE;
    deiPrm->enableForceInterlacedInput  = FALSE;
    deiPrm->enableLineSkipSc            = FALSE;
    deiPrm->enableDualVipOut            = FALSE;
    deiPrm->enableInputFrameRateUpscale = FALSE;

    for(i=0; i<OSA_ARRAYSIZE(outIdList); i++)
    {
        outId = outIdList[i];

        deiPrm->enableOut[outId] = TRUE;

        deiPrm->tilerEnable[outId] = FALSE;
        if(outId==DEI_LINK_OUT_QUE_VIP_SC)
            deiPrm->tilerEnable[outId] = TILER_ENABLE_ENCODE;
        if(outId==DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT)
            deiPrm->tilerEnable[outId] = TILER_ENABLE_ENCODE;

        if(outId==DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT /* CIF encode */
                ||
           outId==DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT /* SCD */
            )
        {
            /* 1/2 scaling */
            outScaleFactorSc.scaleMode = DEI_SCALE_MODE_RATIO;
            outScaleFactorSc.ratio.widthRatio.numerator     = 1;
            outScaleFactorSc.ratio.widthRatio.denominator   = 2;
            outScaleFactorSc.ratio.heightRatio.numerator    = 1;
            outScaleFactorSc.ratio.heightRatio.denominator  = 2;
        }
        else
        {
            /* 1:1 scaling */
            outScaleFactorSc.scaleMode = DEI_SCALE_MODE_RATIO;
            outScaleFactorSc.ratio.widthRatio.numerator     = 1;
            outScaleFactorSc.ratio.widthRatio.denominator   = 1;
            outScaleFactorSc.ratio.heightRatio.numerator    = 1;
            outScaleFactorSc.ratio.heightRatio.denominator  = 1;
        }

        for(chId=0; chId<DEI_LINK_MAX_CH; chId++)
        {
           deiPrm->outScaleFactor[outId][chId] = outScaleFactorSc;
        }

        deiPrm->inputFrameRate[outId]   = 30;
        deiPrm->outputFrameRate[outId]  = 30;
        deiPrm->numBufsPerCh[outId]     = NUM_BUFS_PER_CH_DEI;
        deiPrm->generateBlankOut[outId] = FALSE;

        if(outId==DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT)
        {
            deiPrm->inputFrameRate[outId]   = 15;
            deiPrm->outputFrameRate[outId]  = 5;
        }
        else
        if(outId==DEI_LINK_OUT_QUE_DEI_SC_TERTIARY_OUT)
        {
            deiPrm->inputFrameRate[outId]   = 15;
            deiPrm->outputFrameRate[outId]  = 1;
        }
    }
}

static
Void multich_hdsddvr_set_enclink_prm(EncLink_CreateParams *encPrm)
{
    int i,j;
    EncLink_ChCreateParams *pLinkChPrm;
    EncLink_ChDynamicParams *pLinkDynPrm;
    VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
    VENC_CHN_PARAMS_S *pChPrm;

    encPrm->numBufPerCh[ENC_LINK_CIF_STREAM_POOL_ID] = NUM_BUFS_PER_CH_ENC_SEC;
    encPrm->numBufPerCh[ENC_LINK_SD_STREAM_POOL_ID] = NUM_BUFS_PER_CH_ENC_SEC;
    encPrm->numBufPerCh[ENC_LINK_HD_STREAM_POOL_ID] = NUM_BUFS_PER_CH_ENC_PRI;

    /* Primary Stream Params - D1 */
    for (i=0; i < gVencModuleContext.vencConfig.numPrimaryChn; i++)
    {
        pLinkChPrm  = &encPrm->chCreateParams[i];
        pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

        pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
        pDynPrm     = &pChPrm->dynamicParam;

        pLinkChPrm->format                  = IVIDEO_H264HP;
        pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
        pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->enableAnalyticinfo      = FALSE;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
        pLinkChPrm->rateControlPreset       = pChPrm->rcType;
        pLinkChPrm->enableHighSpeed         = TRUE;
        pLinkChPrm->numTemporalLayer        = VENC_TEMPORAL_LAYERS_2;
        pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;

        pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
        pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
        pLinkDynPrm->interFrameInterval     = 1;
        pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
        pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
        pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
        pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
        pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
        pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
        pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
        pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
    }

    /* Secondary Out <CIF> Params */
    for (i =  gVencModuleContext.vencConfig.numPrimaryChn,
         j =  VENC_PRIMARY_CHANNELS;
         i < (gVencModuleContext.vencConfig.numPrimaryChn
              + gVencModuleContext.vencConfig.numSecondaryChn);
         i++, j++)
    {
        pLinkChPrm  = &encPrm->chCreateParams[i];
        pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

        pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[j];
        pDynPrm     = &pChPrm->dynamicParam;

        pLinkChPrm->format                  = IVIDEO_H264HP;
        pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[j];
        pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
        pLinkChPrm->rateControlPreset       = pChPrm->rcType;
        pLinkChPrm->enableHighSpeed         = TRUE;
        pLinkChPrm->numTemporalLayer        = VENC_TEMPORAL_LAYERS_2;
        pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;


        pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
        pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
        pLinkDynPrm->interFrameInterval     = 1;
        pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
        pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
        pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
        pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
        pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
        pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
        pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
    }
    if (gVsysModuleContext.vsysConfig.enableMjpegEnc == TRUE)
    {
        for (i=gVencModuleContext.vencConfig.numPrimaryChn + gVencModuleContext.vencConfig.numSecondaryChn;
             i<(gVencModuleContext.vencConfig.numPrimaryChn + gVencModuleContext.vencConfig.numSecondaryChn + gVencModuleContext.vencConfig.numPrimaryChn);
             i++)
        {
            pLinkChPrm  = &encPrm->chCreateParams[i];
            pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

            pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
            pDynPrm     = &pChPrm->dynamicParam;

            pLinkChPrm->format                 = IVIDEO_MJPEG;
            pLinkChPrm->profile                = 0;
            pLinkChPrm->dataLayout             = VCODEC_FIELD_SEPARATED;
            pLinkChPrm->fieldMergeEncodeEnable = FALSE;
            pLinkChPrm->enableAnalyticinfo     = 0;
            pLinkChPrm->enableWaterMarking     = 0;
            pLinkChPrm->maxBitRate             = 0;
            pLinkChPrm->encodingPreset         = 0;
            pLinkChPrm->rateControlPreset      = 0;
            pLinkChPrm->enableSVCExtensionFlag = 0;
            pLinkChPrm->numTemporalLayer       = 0;

            pLinkDynPrm->intraFrameInterval    = 0;
            pLinkDynPrm->targetBitRate         = 100*1000;
            pLinkDynPrm->interFrameInterval    = 0;
            pLinkDynPrm->mvAccuracy            = 0;
            pLinkDynPrm->inputFrameRate        = 1;
            pLinkDynPrm->qpMin                 = 0;
            pLinkDynPrm->qpMax                 = 0;
            pLinkDynPrm->qpInit                = -1;
            pLinkDynPrm->vbrDuration           = 0;
            pLinkDynPrm->vbrSensitivity        = 0;
        }
    }
}

static
Void multich_hdsddvr_set_swms_default_layout(SwMsLink_CreateParams *swMsPrm, UInt32 devId)
{
    SwMsLink_LayoutPrm *layoutInfo;
    SwMsLink_LayoutWinInfo *winInfo;
    UInt32 outWidth, outHeight, winId, widthAlign, heightAlign;
    UInt32 rowMax,colMax,row,col;

    MultiCh_swMsGetOutSize(swMsPrm->initOutRes, &outWidth, &outHeight);

    widthAlign = 8;
    heightAlign = 1;

    if(devId>=VDIS_DEV_MAX)
        devId = VDIS_DEV_HDMI;

    rowMax = 2;
    colMax = 2;

    layoutInfo = &swMsPrm->layoutPrm;

    /* init to known default */
    memset(layoutInfo, 0, sizeof(*layoutInfo));

    layoutInfo->onlyCh2WinMapChanged = FALSE;
    layoutInfo->outputFPS            = 30;
    layoutInfo->numWin               = rowMax * colMax;

    for(row=0; row<rowMax; row++)
    {
        for(col=0; col<colMax; col++)
        {
            winId = row*colMax+col;

            winInfo = &layoutInfo->winInfo[winId];

            winInfo->width  = VsysUtils_floor(outWidth/colMax    , widthAlign );
            winInfo->height = VsysUtils_floor(outHeight/rowMax   , heightAlign);
            winInfo->startX = VsysUtils_floor(winInfo->width*col , widthAlign );
            winInfo->startY = VsysUtils_floor(winInfo->height*row, heightAlign);

            if (col == colMax - 1) /* the last col */
            {
                winInfo->width = outWidth - winInfo->width * (colMax - 1);
            }

            if(devId == VDIS_DEV_SD)
            {
                winInfo->channelNum = winId;
            }
            else
            {
                /* show playback channels on HDTV by default */
                winInfo->channelNum = (winId + MAX_NUM_CAPTURE_CHANNELS);
            }

            winInfo->bypass = FALSE;
        }
    }
}


static
Void multich_hdsddvr_set_swms_prm(SwMsLink_CreateParams *swMsPrm,
                                    UInt32 swMsIdx)
{
    UInt32 devId, maxOutRes;

    swMsPrm->numSwMsInst = 1;

    if (swMsIdx == 1)
    {
        swMsPrm->swMsInstId[0] = SYSTEM_SW_MS_SC_INST_SC5;

        maxOutRes  = VSYS_STD_PAL;
        devId      = VDIS_DEV_SD;
    }
    else
    {
        swMsPrm->swMsInstId[0] = SYSTEM_SW_MS_SC_INST_SC5;

        maxOutRes  = VSYS_STD_1080P_60;
        devId      = VDIS_DEV_HDMI;
    }

    swMsPrm->maxOutRes                   = maxOutRes;
	swMsPrm->initOutRes                  = gVdisModuleContext.vdisConfig.deviceParams[devId].resolution;
    swMsPrm->lineSkipMode                = FALSE;
    swMsPrm->enableLayoutGridDraw        = gVdisModuleContext.vdisConfig.enableLayoutGridDraw;
    swMsPrm->maxInputQueLen              = SYSTEM_SW_MS_DEFAULT_INPUT_QUE_LEN + 6;
    swMsPrm->numOutBuf                   = NUM_BUFS_PER_CH_SWMS;
    swMsPrm->enableOuputDup              = TRUE;
    swMsPrm->enableProcessTieWithDisplay = TRUE;
    swMsPrm->outDataFormat               = SYSTEM_DF_YUV422I_YUYV;
    swMsPrm->outputBufModified           = TRUE;

    multich_hdsddvr_set_swms_default_layout(swMsPrm, devId);
}

static
Void multich_hdsddvr_set_avsync_vidque_prm(Avsync_SynchConfigParams *queCfg,
                                            Int chnum,
                                            UInt32 avsStartChNum,
                                            UInt32 avsEndChNum)
{
    queCfg->chNum = chnum;
    queCfg->audioPresent = FALSE;
    if ((queCfg->chNum >= avsStartChNum)
        &&
        (queCfg->chNum <= avsEndChNum)
        &&
        (gVsysModuleContext.vsysConfig.enableAVsync))
    {
        queCfg->avsyncEnable = FALSE;
    }
    else
    {
        queCfg->avsyncEnable = FALSE;
    }
    queCfg->clkAdjustPolicy.refClkType = AVSYNC_REFCLKADJUST_NONE;
    queCfg->playTimerStartTimeout = 0;
    queCfg->playStartMode = AVSYNC_PLAYBACK_START_MODE_WAITSYNCH;
    queCfg->ptsInitMode   = AVSYNC_PTS_INIT_MODE_APP;
    queCfg->clkAdjustPolicy.clkAdjustLead = AVSYNC_VIDEO_TIMEBASESHIFT_MAX_LEAD_MS;
    queCfg->clkAdjustPolicy.clkAdjustLag = AVSYNC_VIDEO_TIMEBASESHIFT_MAX_LAG_MS;
    queCfg->vidSynchPolicy.playMaxLag    = 200;
}

static
Void multich_hdsddvr_set_avsync_prm(AvsyncLink_LinkSynchConfigParams *avsyncPrm,
                                     UInt32 swMsIdx,
                                     UInt32 prevLinkID,
                                     UInt32 prevLinkQueId)
{
    System_LinkInfo                   swmsInLinkInfo;
    Int i;
    Int32 status;

    if (0 == swMsIdx)
    {
        Vdis_getAvsyncConfig(VDIS_DEV_HDMI,avsyncPrm);
        avsyncPrm->displayLinkID        = Vdis_getDisplayId(VDIS_DEV_HDMI);
        avsyncPrm->videoSynchLinkID = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)];
    }
    else
    {
        Vdis_getAvsyncConfig(VDIS_DEV_SD,avsyncPrm);
        avsyncPrm->displayLinkID        = Vdis_getDisplayId(VDIS_DEV_SD);
        avsyncPrm->videoSynchLinkID = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)];
    }
    System_linkGetInfo(prevLinkID,&swmsInLinkInfo);
    OSA_assert(swmsInLinkInfo.numQue > prevLinkQueId);

    avsyncPrm->numCh            = swmsInLinkInfo.queInfo[prevLinkQueId].numCh;
    avsyncPrm->syncMasterChnum = AVSYNC_INVALID_CHNUM;
    for (i = 0; i < avsyncPrm->numCh;i++)
    {
        multich_hdsddvr_set_avsync_vidque_prm(&avsyncPrm->queCfg[i],
                                               i,
                                               gVcapModuleContext.vcapConfig.numChn,
                                               (gVcapModuleContext.vcapConfig.numChn + (gVdecModuleContext.vdecConfig.numChn - 1)));
    }
    if (0 == swMsIdx)
    {
        Vdis_setAvsyncConfig(VDIS_DEV_HDMI,avsyncPrm);
    }
    else
    {
        Vdis_setAvsyncConfig(VDIS_DEV_SD,avsyncPrm);
    }

    status = Avsync_configSyncConfigInfo(avsyncPrm);
    OSA_assert(status == 0);

}

static
Void multich_hdsddvr_set_osd_prm(AlgLink_CreateParams *dspAlgPrm)
{
    int chId;

    dspAlgPrm->enableOSDAlg = TRUE;
    dspAlgPrm->enableSCDAlg = FALSE;
    dspAlgPrm->outQueParams[ALG_LINK_SCD_OUT_QUE].nextLink = SYSTEM_LINK_ID_INVALID;

    for(chId = 0; chId < ALG_LINK_OSD_MAX_CH; chId++)
    {
        AlgLink_OsdChWinParams * chWinPrm =
          &dspAlgPrm->osdChCreateParams[chId].chDefaultParams;
        /* set osd window max width and height */
        dspAlgPrm->osdChCreateParams[chId].maxWidth  = EXAMPLE_OSD_WIN_MAX_WIDTH;
        dspAlgPrm->osdChCreateParams[chId].maxHeight = EXAMPLE_OSD_WIN_MAX_HEIGHT;

        chWinPrm->chId       = chId;
        chWinPrm->numWindows = 0;
    }
}

static
Void multich_hdsddvr_set_scd_prm(AlgLink_CreateParams *dspAlgPrm)
{
    Int32   numBlksInFrame;
    Int32   numHorzBlks, numVertBlks, chIdx;
    UInt32  x, y, i;

    dspAlgPrm->enableOSDAlg = FALSE;
    dspAlgPrm->enableSCDAlg = TRUE;
    dspAlgPrm->scdCreateParams.maxWidth               = 352;
    dspAlgPrm->scdCreateParams.maxHeight              = 240;
    dspAlgPrm->scdCreateParams.maxStride              = 352;
    dspAlgPrm->scdCreateParams.numValidChForSCD       = 10;

    dspAlgPrm->scdCreateParams.numSecs2WaitB4Init     = 3;
    dspAlgPrm->scdCreateParams.numSecs2WaitB4FrmAlert = 1;
    dspAlgPrm->scdCreateParams.inputFrameRate         = 2;
    dspAlgPrm->scdCreateParams.outputFrameRate        = 2;

    /*
        Each block is fixed to be 32x10 in size when height is 240
        Each block is fixed to be 32x12 in size when height is 288
    */
    numHorzBlks    = dspAlgPrm->scdCreateParams.maxWidth / 32;
    if(dspAlgPrm->scdCreateParams.maxHeight == 240)
       numVertBlks    = dspAlgPrm->scdCreateParams.maxHeight / 10;
    else
       numVertBlks    = dspAlgPrm->scdCreateParams.maxHeight / 12;

    numBlksInFrame = numHorzBlks * numVertBlks;

    for(chIdx = 0; chIdx < dspAlgPrm->scdCreateParams.numValidChForSCD; chIdx++)
    {
       AlgLink_ScdChParams * chPrm = &dspAlgPrm->scdCreateParams.chDefaultParams[chIdx];

       chPrm->blkNumBlksInFrame = numBlksInFrame;
       chPrm->chId              = chIdx;
       chPrm->mode              = ALG_LINK_SCD_DETECTMODE_MONITOR_BLOCKS_AND_FRAME;
       chPrm->frmIgnoreLightsON = FALSE;
       chPrm->frmIgnoreLightsOFF= FALSE;
       chPrm->frmSensitivity    = ALG_LINK_SCD_SENSITIVITY_HIGH;
       chPrm->frmEdgeThreshold  = 100;
       i = 0;
       for(y = 0; y < numVertBlks; y++)
       {
         for(x = 0; x < numHorzBlks; x++)
         {
           chPrm->blkConfig[i].sensitivity = ALG_LINK_SCD_SENSITIVITY_LOW;
           chPrm->blkConfig[i].monitored   = 0;
           i++;
         }
       }
    }
    dspAlgPrm->scdCreateParams.numBufPerCh = NUM_BUFS_PER_CH_BITSOUT_SCD;
}

static
Void multich_hdsddvr_set_declink_prms(DecLink_CreateParams *decPrm)
{
    int i;

    gVdecModuleContext.vdecConfig.numChn = MAX_NUM_DECODE_CHANNELS;
    for (i=0; i<gVdecModuleContext.vdecConfig.numChn; i++)
    {
        decPrm->chCreateParams[i].format                 = IVIDEO_H264HP;
        decPrm->chCreateParams[i].profile                = IH264VDEC_PROFILE_ANY;
        decPrm->chCreateParams[i].processCallLevel       = VDEC_FRAMELEVELPROCESSCALL;
        if(i<MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            decPrm->chCreateParams[i].targetMaxWidth         = MAX_HD_PRIMARY_WIDTH;
            decPrm->chCreateParams[i].targetMaxHeight        = MAX_HD_PRIMARY_HEIGHT;
        }
        else
        {
            decPrm->chCreateParams[i].targetMaxWidth         = MAX_SD_PRIMARY_WIDTH;
            decPrm->chCreateParams[i].targetMaxHeight        = MAX_SD_PRIMARY_HEIGHT;
        }
        decPrm->chCreateParams[i].numBufPerCh            = NUM_BUFS_PER_CH_DEC;
        decPrm->chCreateParams[i].tilerEnable            = TILER_ENABLE_DECODE;
        decPrm->chCreateParams[i].defaultDynamicParams.targetFrameRate = gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.frameRate;
        decPrm->chCreateParams[i].defaultDynamicParams.targetBitRate   = gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.targetBitRate;
    }
}

static
Void multich_hdsddvr_set_ipcbitsout_hlos_prms(IpcBitsOutLinkHLOS_CreateParams * ipcBitsOutHostPrm)
{
    int i;

    for (i = 0;
         i < (MAX_NUM_DECODE_CHANNELS);
         i++)
    {
        System_LinkChInfo *pChInfo;

        pChInfo = &ipcBitsOutHostPrm->inQueInfo.chInfo[i];

        pChInfo->bufType        = 0; // NOT USED
        pChInfo->codingformat   = 0; // NOT USED
        pChInfo->dataFormat     = 0; // NOT USED
        pChInfo->memType        = 0; // NOT USED
        pChInfo->startX         = 0; // NOT USED
        pChInfo->startY         = 0; // NOT USED
        if(i<MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            pChInfo->width         = MAX_HD_PRIMARY_WIDTH;
            pChInfo->height        = MAX_HD_PRIMARY_HEIGHT;
        }
        else
        {
            pChInfo->width         = MAX_SD_PRIMARY_WIDTH;
            pChInfo->height        = MAX_SD_PRIMARY_HEIGHT;
        }
        pChInfo->pitch[0]       = 0; // NOT USED
        pChInfo->pitch[1]       = 0; // NOT USED
        pChInfo->pitch[2]       = 0; // NOT USED
        pChInfo->scanFormat     = SYSTEM_SF_PROGRESSIVE;

        ipcBitsOutHostPrm->maxQueueDepth[i] =
            MAX_BUFFERING_QUEUE_LEN_PER_CH;
        ipcBitsOutHostPrm->chMaxReqBufSize[i] = (pChInfo->width * pChInfo->height);
        ipcBitsOutHostPrm->totalBitStreamBufferSize [i] =
                (ipcBitsOutHostPrm->chMaxReqBufSize[i] * BIT_BUF_LENGTH_LIMIT_FACTOR_HD);
    }
    ipcBitsOutHostPrm->baseCreateParams.noNotifyMode   = FALSE;
    ipcBitsOutHostPrm->baseCreateParams.notifyNextLink = TRUE;
    ipcBitsOutHostPrm->baseCreateParams.numOutQue      = 1;
    ipcBitsOutHostPrm->inQueInfo.numCh                 = MAX_NUM_DECODE_CHANNELS;
}

static
Void multich_hdsddvr_set_display_prms(DisplayLink_CreateParams *displayPrm,
                                        UInt32 maxOutRes)
{
    displayPrm->displayRes = maxOutRes;
}

static
Void multich_hdsddvr_set_link_ids()
{
    Bool   enableOsdAlgLink = gVsysModuleContext.vsysConfig.enableOsd;
    Bool   enableScdAlgLink = gVsysModuleContext.vsysConfig.enableScd;
    int    i;

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_CAPTURE;

    for (i = 0; i < NUM_NSF_LINK;i++)
    {
        gVcapModuleContext.nsfId[i]         = SYSTEM_LINK_ID_NSF_0 + i;
    }

    gVcapModuleContext.deiId[DEI_HD]             = SYSTEM_LINK_ID_DEI_0;
    gVcapModuleContext.deiId[DEI_LOW_RES]        = SYSTEM_LINK_ID_DEI_1;
    gVcapModuleContext.deiId[DEI_SD]             = SYSTEM_LINK_ID_DEI_HQ_0;

    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]    = SYSTEM_LINK_ID_SW_MS_MULTI_INST_0;
    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_1;

    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)] = SYSTEM_LINK_ID_DISPLAY_0; /* ON CHIP HDMI */
    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)]   = SYSTEM_LINK_ID_DISPLAY_2; /* SD HDMI */

    gHdSdDvrUsecaseContext.grpxId[0]                       = SYSTEM_LINK_ID_GRPX_0;
    gHdSdDvrUsecaseContext.grpxId[1]                       = SYSTEM_LINK_ID_GRPX_1;

    for (i = 0; i < NUM_MERGE_LINK;i++)
    {
        gHdSdDvrUsecaseContext.mergeId[i] = SYSTEM_VPSS_LINK_ID_MERGE_0 + i;
    }
    for (i = 0; i < NUM_DUP_LINK;i++)
    {
        gHdSdDvrUsecaseContext.dupId[i] = SYSTEM_VPSS_LINK_ID_DUP_0 + i;
    }
    for (i = 0; i < NUM_SELECT_LINK;i++)
    {
        gHdSdDvrUsecaseContext.selectId[i]   = SYSTEM_VPSS_LINK_ID_SELECT_0 + i;
    }

    if(enableOsdAlgLink)
    {
        gVcapModuleContext.ipcFramesOutVpssId[0] = SYSTEM_VPSS_LINK_ID_IPC_FRAMES_OUT_0;
        gVcapModuleContext.ipcFramesInDspId[0]   = SYSTEM_DSP_LINK_ID_IPC_FRAMES_IN_0;
        gVcapModuleContext.dspAlgId[0]           = SYSTEM_LINK_ID_ALG_0;
    }

    if(enableScdAlgLink)
    {
        gVcapModuleContext.ipcFramesOutVpssId[1] = SYSTEM_VPSS_LINK_ID_IPC_FRAMES_OUT_1;
        gVcapModuleContext.ipcFramesInDspId[1]   = SYSTEM_DSP_LINK_ID_IPC_FRAMES_IN_1;
        gVcapModuleContext.dspAlgId[1]           = SYSTEM_LINK_ID_ALG_1;
        gHdSdDvrUsecaseContext.ipcBitsOutDSPId     = SYSTEM_DSP_LINK_ID_IPC_BITS_OUT_0;
        gVcapModuleContext.ipcBitsInHLOSId       = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;
    }

    gVencModuleContext.encId        = SYSTEM_LINK_ID_VENC_0;
    gVdecModuleContext.decId        = SYSTEM_LINK_ID_VDEC_0;

    gHdSdDvrUsecaseContext.ipcOutVpssId = SYSTEM_VPSS_LINK_ID_IPC_OUT_M3_0;
    gHdSdDvrUsecaseContext.ipcInVideoId = SYSTEM_VIDEO_LINK_ID_IPC_IN_M3_0;

    gHdSdDvrUsecaseContext.ipcOutVideoId= SYSTEM_VIDEO_LINK_ID_IPC_OUT_M3_0;
    gHdSdDvrUsecaseContext.ipcInVpssId  = SYSTEM_VPSS_LINK_ID_IPC_IN_M3_0;

    gVencModuleContext.ipcBitsOutRTOSId     = SYSTEM_VIDEO_LINK_ID_IPC_BITS_OUT_0;
    if(enableScdAlgLink)
       gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_1;
    else
       gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;

    gVdecModuleContext.ipcBitsOutHLOSId     = SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0;
    gVdecModuleContext.ipcBitsInRTOSId      = SYSTEM_VIDEO_LINK_ID_IPC_BITS_IN_0;

    gVdisModuleContext.setMosaicFxn         = MultiCh_hdSdDvrSetMosaicParams;
}

static
Void multich_hdsddvr_reset_link_ids()
{
    Bool   enableOsdAlgLink = gVsysModuleContext.vsysConfig.enableOsd;
    Bool   enableScdAlgLink = gVsysModuleContext.vsysConfig.enableScd;
    int    i;

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_INVALID;

    for (i = 0; i < NUM_NSF_LINK;i++)
    {
        gVcapModuleContext.nsfId[i]         = SYSTEM_LINK_ID_INVALID;
    }
    for(i=0; i<NUM_DEI_LINK; i++)
    {
        gVcapModuleContext.deiId[i]     = SYSTEM_LINK_ID_INVALID;
    }

    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]      = SYSTEM_LINK_ID_INVALID;
    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)]      = SYSTEM_LINK_ID_INVALID;

    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)] = SYSTEM_LINK_ID_INVALID; /* ON CHIP HDMI */
    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)] = SYSTEM_LINK_ID_INVALID; /* SD HDMI */

    gHdSdDvrUsecaseContext.grpxId[0]                       = SYSTEM_LINK_ID_INVALID;
    gHdSdDvrUsecaseContext.grpxId[1]                       = SYSTEM_LINK_ID_INVALID;

    for (i = 0; i < NUM_MERGE_LINK;i++)
    {
        gHdSdDvrUsecaseContext.mergeId[i] = SYSTEM_LINK_ID_INVALID;
    }
    for (i = 0; i < NUM_DUP_LINK;i++)
    {
        gHdSdDvrUsecaseContext.dupId[i] = SYSTEM_LINK_ID_INVALID;
    }
    for (i = 0; i < NUM_SELECT_LINK;i++)
    {
        gHdSdDvrUsecaseContext.selectId[i] = SYSTEM_LINK_ID_INVALID;
    }

    if(enableOsdAlgLink)
    {
        gVcapModuleContext.ipcFramesOutVpssId[0] = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.ipcFramesInDspId[0] = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.dspAlgId[0] = SYSTEM_LINK_ID_INVALID;
    }
    if(enableScdAlgLink)
    {
        gVcapModuleContext.ipcFramesOutVpssId[1] = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.ipcFramesInDspId[1]   = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.dspAlgId[1]           = SYSTEM_LINK_ID_INVALID;
        gHdSdDvrUsecaseContext.ipcBitsOutDSPId = SYSTEM_LINK_ID_INVALID;
        gVcapModuleContext.ipcBitsInHLOSId       = SYSTEM_LINK_ID_INVALID;
    }
    gVencModuleContext.encId        = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.decId        = SYSTEM_LINK_ID_INVALID;

    gHdSdDvrUsecaseContext.ipcOutVpssId = SYSTEM_LINK_ID_INVALID;
    gHdSdDvrUsecaseContext.ipcInVideoId = SYSTEM_LINK_ID_INVALID;

    gHdSdDvrUsecaseContext.ipcOutVideoId= SYSTEM_LINK_ID_INVALID;
    gHdSdDvrUsecaseContext.ipcInVpssId  = SYSTEM_LINK_ID_INVALID;

    gVencModuleContext.ipcBitsOutRTOSId  = SYSTEM_LINK_ID_INVALID;
    gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_LINK_ID_INVALID;

    gVdecModuleContext.ipcBitsOutHLOSId  = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.ipcBitsInRTOSId  = SYSTEM_LINK_ID_INVALID;

    gVdisModuleContext.setMosaicFxn     = NULL;
}

static
Void multich_hdsddvr_connect_osd_links()
{
    /* IpcFramesOut Link for OSD */
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.inQueParams.prevLinkId     = gHdSdDvrUsecaseContext.mergeId[MERGE_ENCODE];
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.inQueParams.prevLinkQueId  = 0;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.numOutQue                  = 1;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.notifyNextLink             = TRUE;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.outQueParams[0].nextLink   = gHdSdDvrUsecaseContext.ipcOutVpssId;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.processLink                = gVcapModuleContext.ipcFramesInDspId[0];
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.notifyProcessLink          = TRUE;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[0].baseCreateParams.noNotifyMode               = FALSE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.ipcFramesOutVpssId[0],
        gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[0]
    );


    /* IpcFramesInDsp ---Q0--- dspAlg0(SCD) */
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.inQueParams.prevLinkId       = gVcapModuleContext.ipcFramesOutVpssId[0];
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.inQueParams.prevLinkQueId    = 0;
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.numOutQue                    = 1;
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.outQueParams[0].nextLink     = gVcapModuleContext.dspAlgId[0];
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.notifyPrevLink               = TRUE;
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.notifyNextLink               = TRUE;
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[0].baseCreateParams.noNotifyMode                 = FALSE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.ipcFramesInDspId[0],
        gHdSdDvrUsecaseContext.ipcFramesInDspPrm[0]
    );

    multich_hdsddvr_set_osd_prm(&gHdSdDvrUsecaseContext.dspAlgPrm[0]);
    gHdSdDvrUsecaseContext.dspAlgPrm[0].inQueParams.prevLinkId    = gVcapModuleContext.ipcFramesInDspId[0];
    gHdSdDvrUsecaseContext.dspAlgPrm[0].inQueParams.prevLinkQueId = 0;

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.dspAlgId[0],
        gHdSdDvrUsecaseContext.dspAlgPrm[0]
    );
}

static
Void multich_hdsddvr_connect_scd_links()
{
    /* MERGE Link
        to merge input for SCD
    */
    gHdSdDvrUsecaseContext.mergePrm[MERGE_SCD].numInQue = 2;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_SCD].inQueParams[0].prevLinkId    = gVcapModuleContext.deiId[DEI_LOW_RES];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_SCD].inQueParams[0].prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_SCD].inQueParams[1].prevLinkId    = gVcapModuleContext.deiId[DEI_SD];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_SCD].inQueParams[1].prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_SCD].outQueParams.nextLink        = gVcapModuleContext.nsfId[NSF_SCD];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_SCD].notifyNextLink               = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.mergeId[MERGE_SCD],
        gHdSdDvrUsecaseContext.mergePrm[MERGE_SCD]
    );

    /* NSF link
        to convert YUV422 data to YUV420 for SCD input
    */
    multich_hdsddvr_set_nsf_prm(&gHdSdDvrUsecaseContext.nsfPrm[NSF_SCD], NUM_BUFS_PER_CH_NSF_SCD);

    gHdSdDvrUsecaseContext.nsfPrm[NSF_SCD].inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.mergeId[MERGE_SCD];
    gHdSdDvrUsecaseContext.nsfPrm[NSF_SCD].inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.nsfPrm[NSF_SCD].outQueParams[0].nextLink  = gVcapModuleContext.ipcFramesOutVpssId[1];

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.nsfId[NSF_SCD],
        gHdSdDvrUsecaseContext.nsfPrm[NSF_SCD]
    );

    /* ipcFramesOutVpssId[1] ---Q2---> ipcFramesInDspId[1] */
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.inQueParams.prevLinkId = gVcapModuleContext.nsfId[NSF_SCD];
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.notifyPrevLink         = TRUE;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.inputFrameRate             = 10;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.outputFrameRate            = 10;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.numOutQue                  = 1;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.outQueParams[0].nextLink   = gVcapModuleContext.ipcFramesInDspId[1];
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.notifyNextLink             = TRUE;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.processLink                = SYSTEM_LINK_ID_INVALID;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.notifyProcessLink          = FALSE;
    gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1].baseCreateParams.noNotifyMode               = FALSE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.ipcFramesOutVpssId[1],
        gHdSdDvrUsecaseContext.ipcFramesOutVpssPrm[1]
    );

    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.inQueParams.prevLinkId       = gVcapModuleContext.ipcFramesOutVpssId[1];
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.inQueParams.prevLinkQueId    = 0;
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.notifyPrevLink               = TRUE;
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.numOutQue                    = 1;
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.outQueParams[0].nextLink     = gVcapModuleContext.dspAlgId[1];
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.notifyNextLink               = TRUE;
    gHdSdDvrUsecaseContext.ipcFramesInDspPrm[1].baseCreateParams.noNotifyMode                 = FALSE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.ipcFramesInDspId[1],
        gHdSdDvrUsecaseContext.ipcFramesInDspPrm[1]
    );

    /* dspAlgId[1] ---Q0---> ipcBitsOutDsp */
    multich_hdsddvr_set_scd_prm(&gHdSdDvrUsecaseContext.dspAlgPrm[1]);
    gHdSdDvrUsecaseContext.dspAlgPrm[1].inQueParams.prevLinkId                        = gVcapModuleContext.ipcFramesInDspId[1];
    gHdSdDvrUsecaseContext.dspAlgPrm[1].inQueParams.prevLinkQueId                     = 0;
    gHdSdDvrUsecaseContext.dspAlgPrm[1].outQueParams[ALG_LINK_SCD_OUT_QUE].nextLink   = gHdSdDvrUsecaseContext.ipcBitsOutDSPId;
    gHdSdDvrUsecaseContext.ipcBitsOutDspPrm.baseCreateParams.inQueParams.prevLinkId   = gVcapModuleContext.dspAlgId[1];
    gHdSdDvrUsecaseContext.ipcBitsOutDspPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.dspAlgId[1],
        gHdSdDvrUsecaseContext.dspAlgPrm[1]
    );

    /* ipcBitsOutDsp ---Q0---> ipcBitsInHlos */
    gHdSdDvrUsecaseContext.ipcBitsOutDspPrm.baseCreateParams.numOutQue                 = 1;
    gHdSdDvrUsecaseContext.ipcBitsOutDspPrm.baseCreateParams.outQueParams[0].nextLink  = gVcapModuleContext.ipcBitsInHLOSId;
    MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&gHdSdDvrUsecaseContext.ipcBitsOutDspPrm,
                                                TRUE);
    gHdSdDvrUsecaseContext.ipcBitsInHostPrm[1].baseCreateParams.inQueParams.prevLinkId = gHdSdDvrUsecaseContext.ipcBitsOutDSPId;
    gHdSdDvrUsecaseContext.ipcBitsInHostPrm[1].baseCreateParams.inQueParams.prevLinkQueId = 0;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.ipcBitsOutDSPId,
        gHdSdDvrUsecaseContext.ipcBitsOutDspPrm
    );

    gHdSdDvrUsecaseContext.ipcBitsInHostPrm[1].baseCreateParams.numOutQue                 = 1;
    gHdSdDvrUsecaseContext.ipcBitsInHostPrm[1].baseCreateParams.outQueParams[0].nextLink  = SYSTEM_LINK_ID_INVALID;
    MultiCh_ipcBitsInitCreateParams_BitsInHLOSVcap(&gHdSdDvrUsecaseContext.ipcBitsInHostPrm[1]);

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.ipcBitsInHLOSId,
        gHdSdDvrUsecaseContext.ipcBitsInHostPrm[1]
    );
}

static
Void multich_hdsddvr_connect_encode_links()
{
    /* ipcOutVpssIdisOutVpssId ---Q0---> ipcInVideoId */
    gHdSdDvrUsecaseContext.ipcOutVpssPrm.inQueParams.prevLinkId    = gVcapModuleContext.ipcFramesOutVpssId[0];
    gHdSdDvrUsecaseContext.ipcOutVpssPrm.inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.ipcOutVpssPrm.outQueParams[0].nextLink  = gHdSdDvrUsecaseContext.ipcInVideoId;
    gHdSdDvrUsecaseContext.ipcOutVpssPrm.notifyNextLink            = FALSE;
    gHdSdDvrUsecaseContext.ipcOutVpssPrm.notifyPrevLink            = TRUE;
    gHdSdDvrUsecaseContext.ipcOutVpssPrm.noNotifyMode              = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.ipcOutVpssId,
        gHdSdDvrUsecaseContext.ipcOutVpssPrm
    );


    /* ipcInVideoId ---Q0---> encId */
    gHdSdDvrUsecaseContext.ipcInVideoPrm.inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.ipcOutVpssId;
    gHdSdDvrUsecaseContext.ipcInVideoPrm.inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.ipcInVideoPrm.numOutQue                 = 1;
    gHdSdDvrUsecaseContext.ipcInVideoPrm.outQueParams[0].nextLink  = gVencModuleContext.encId;
    gHdSdDvrUsecaseContext.ipcInVideoPrm.notifyNextLink            = TRUE;
    gHdSdDvrUsecaseContext.ipcInVideoPrm.notifyPrevLink            = FALSE;
    gHdSdDvrUsecaseContext.ipcInVideoPrm.noNotifyMode              = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.ipcInVideoId,
        gHdSdDvrUsecaseContext.ipcInVideoPrm
    );


    /* encId ---Q0---> ipcBitsOutRTOSId */
    multich_hdsddvr_set_enclink_prm(&gHdSdDvrUsecaseContext.encPrm);
    gHdSdDvrUsecaseContext.encPrm.inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.ipcInVideoId;
    gHdSdDvrUsecaseContext.encPrm.inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.encPrm.outQueParams.nextLink     = gVencModuleContext.ipcBitsOutRTOSId;

    MULTICH_HDSDDVR_CREATE_LINK(
        gVencModuleContext.encId,
        gHdSdDvrUsecaseContext.encPrm
    );

    /* ipcBitsOutVideoId ---Q0---> ipcBitsInHostId */
    gHdSdDvrUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.encId;
    gHdSdDvrUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.numOutQue                 = 1;
    gHdSdDvrUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.outQueParams[0].nextLink  = gVencModuleContext.ipcBitsInHLOSId;
    MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&gHdSdDvrUsecaseContext.ipcBitsOutVideoPrm,
                                               TRUE);
    gHdSdDvrUsecaseContext.ipcBitsInHostPrm[0].baseCreateParams.inQueParams.prevLinkId    = gVencModuleContext.ipcBitsOutRTOSId;
    gHdSdDvrUsecaseContext.ipcBitsInHostPrm[0].baseCreateParams.inQueParams.prevLinkQueId = 0;

    MULTICH_HDSDDVR_CREATE_LINK(
        gVencModuleContext.ipcBitsOutRTOSId,
        gHdSdDvrUsecaseContext.ipcBitsOutVideoPrm
    );

    MultiCh_ipcBitsInitCreateParams_BitsInHLOS(&gHdSdDvrUsecaseContext.ipcBitsInHostPrm[0]);
    MULTICH_HDSDDVR_CREATE_LINK(
        gVencModuleContext.ipcBitsInHLOSId,
        gHdSdDvrUsecaseContext.ipcBitsInHostPrm[0]
    );
}

static
Void multich_hdsddvr_connect_decode_links()
{
    /* ipcBitsOutHostId ---Q0---> ipcBitsInRtosId */
    multich_hdsddvr_set_ipcbitsout_hlos_prms(&gHdSdDvrUsecaseContext.ipcBitsOutHostPrm);
    gHdSdDvrUsecaseContext.ipcBitsOutHostPrm.baseCreateParams.outQueParams[0].nextLink    = gVdecModuleContext.ipcBitsInRTOSId;

    MULTICH_HDSDDVR_CREATE_LINK(
        gVdecModuleContext.ipcBitsOutHLOSId,
        gHdSdDvrUsecaseContext.ipcBitsOutHostPrm
    );

    /* ipcBitsInRtosId ---Q0---> decId */
    gHdSdDvrUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkId      = gVdecModuleContext.ipcBitsOutHLOSId;
    gHdSdDvrUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkQueId   = 0;
    gHdSdDvrUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.numOutQue                    = 1;
    gHdSdDvrUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.outQueParams[0].nextLink     = gVdecModuleContext.decId;
    MultiCh_ipcBitsInitCreateParams_BitsInRTOS(&gHdSdDvrUsecaseContext.ipcBitsInVideoPrm, TRUE);

    MULTICH_HDSDDVR_CREATE_LINK(
        gVdecModuleContext.ipcBitsInRTOSId,
        gHdSdDvrUsecaseContext.ipcBitsInVideoPrm
    );

    /* decId---Q0--->ipcOutVideoId*/
    multich_hdsddvr_set_declink_prms(&gHdSdDvrUsecaseContext.decPrm);
    gHdSdDvrUsecaseContext.decPrm.inQueParams.prevLinkId    = gVdecModuleContext.ipcBitsInRTOSId;
    gHdSdDvrUsecaseContext.decPrm.inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.decPrm.outQueParams.nextLink  = gHdSdDvrUsecaseContext.ipcOutVideoId;

    MULTICH_HDSDDVR_CREATE_LINK(
        gVdecModuleContext.decId,
        gHdSdDvrUsecaseContext.decPrm
    );

    /*ipcOutVideoId---Q0-->ipcInVpssId*/
    gHdSdDvrUsecaseContext.ipcOutVideoPrm.inQueParams.prevLinkId    = gVdecModuleContext.decId;
    gHdSdDvrUsecaseContext.ipcOutVideoPrm.inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.ipcOutVideoPrm.numOutQue                 = 1;
    gHdSdDvrUsecaseContext.ipcOutVideoPrm.outQueParams[0].nextLink  = gHdSdDvrUsecaseContext.ipcInVpssId;
    gHdSdDvrUsecaseContext.ipcOutVideoPrm.notifyNextLink            = FALSE;
    gHdSdDvrUsecaseContext.ipcOutVideoPrm.notifyPrevLink            = TRUE;
    gHdSdDvrUsecaseContext.ipcOutVideoPrm.noNotifyMode              = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.ipcOutVideoId,
        gHdSdDvrUsecaseContext.ipcOutVideoPrm
    );

    /*ipcInVpssId---Q0--> mergeId[MERGE_DISPLAY] */
    gHdSdDvrUsecaseContext.ipcInVpssPrm.inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.ipcOutVideoId;
    gHdSdDvrUsecaseContext.ipcInVpssPrm.inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.ipcInVpssPrm.numOutQue                 = 1;
    gHdSdDvrUsecaseContext.ipcInVpssPrm.outQueParams[0].nextLink  = gHdSdDvrUsecaseContext.mergeId[MERGE_DISPLAY];
    gHdSdDvrUsecaseContext.ipcInVpssPrm.notifyNextLink            = TRUE;
    gHdSdDvrUsecaseContext.ipcInVpssPrm.notifyPrevLink            = FALSE;
    gHdSdDvrUsecaseContext.ipcInVpssPrm.noNotifyMode              = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.ipcInVpssId,
        gHdSdDvrUsecaseContext.ipcInVpssPrm
    );
}

static
Void multich_hdsddvr_connect_display_links()
{
    /* MERGE Link
        to merge input for display from low res live, playback and high res live
    */
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].numInQue = 4;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[0].prevLinkId    = gHdSdDvrUsecaseContext.dupId[DUP_LOW_RES];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[0].prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[1].prevLinkId    = gVcapModuleContext.deiId[DEI_SD];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[1].prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[2].prevLinkId    = gHdSdDvrUsecaseContext.ipcInVpssId;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[2].prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[3].prevLinkId    = gHdSdDvrUsecaseContext.dupId[DUP_HD];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].inQueParams[3].prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].outQueParams.nextLink        = gHdSdDvrUsecaseContext.dupId[DUP_DISPLAY];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY].notifyNextLink               = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.mergeId[MERGE_DISPLAY],
        gHdSdDvrUsecaseContext.mergePrm[MERGE_DISPLAY]
    );

    /*dupId[DUP_DISPLAY]---Q0--> swMsId[0] */
    /*dupId[DUP_DISPLAY]---Q1--> swMsId[1] */
    gHdSdDvrUsecaseContext.dupPrm[DUP_DISPLAY].inQueParams.prevLinkId       = gHdSdDvrUsecaseContext.mergeId[MERGE_DISPLAY];
    gHdSdDvrUsecaseContext.dupPrm[DUP_DISPLAY].inQueParams.prevLinkQueId      = 0;
    gHdSdDvrUsecaseContext.dupPrm[DUP_DISPLAY].numOutQue                      = 2;
    gHdSdDvrUsecaseContext.dupPrm[DUP_DISPLAY].outQueParams[0].nextLink       = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)];
    gHdSdDvrUsecaseContext.dupPrm[DUP_DISPLAY].outQueParams[1].nextLink       = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)];
    gHdSdDvrUsecaseContext.dupPrm[DUP_DISPLAY].notifyNextLink                 = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.dupId[DUP_DISPLAY],
        gHdSdDvrUsecaseContext.dupPrm[DUP_DISPLAY]
    );

    /* Avsync configuration for SwMs[0] */
    multich_hdsddvr_set_avsync_prm(&gHdSdDvrUsecaseContext.avsyncCfg[0],
                                0,
                                gHdSdDvrUsecaseContext.dupId[DUP_DISPLAY],
                                0);

    /*swMsId[0]---Q0--> displayId[VDIS_DEV_HDMI] */
    multich_hdsddvr_set_swms_prm(&gHdSdDvrUsecaseContext.swMsPrm[0], 0);
    gHdSdDvrUsecaseContext.swMsPrm[0].inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.dupId[DUP_DISPLAY];
    gHdSdDvrUsecaseContext.swMsPrm[0].inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.swMsPrm[0].outQueParams.nextLink     = Vdis_getDisplayId(VDIS_DEV_HDMI);

    MULTICH_HDSDDVR_CREATE_LINK(
        gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)],
        gHdSdDvrUsecaseContext.swMsPrm[0]
    );

    gHdSdDvrUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)] = gHdSdDvrUsecaseContext.swMsPrm[0].layoutPrm;

    /* Avsync configuration for SwMs[1] */
    multich_hdsddvr_set_avsync_prm(&gHdSdDvrUsecaseContext.avsyncCfg[1],
                                1,
                                gHdSdDvrUsecaseContext.dupId[DUP_DISPLAY],
                                1);

    /*swMsId[1]---Q1--> displayId[VDIS_DEV_SD] */
    multich_hdsddvr_set_swms_prm(&gHdSdDvrUsecaseContext.swMsPrm[1], 1);
    gHdSdDvrUsecaseContext.swMsPrm[1].inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.dupId[DUP_DISPLAY];
    gHdSdDvrUsecaseContext.swMsPrm[1].inQueParams.prevLinkQueId = 1;
    gHdSdDvrUsecaseContext.swMsPrm[1].outQueParams.nextLink     = Vdis_getDisplayId(VDIS_DEV_SD);

    MULTICH_HDSDDVR_CREATE_LINK(
        gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)],
        gHdSdDvrUsecaseContext.swMsPrm[1]
    );

    gHdSdDvrUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)] = gHdSdDvrUsecaseContext.swMsPrm[1].layoutPrm;

    multich_hdsddvr_set_display_prms(
        &gHdSdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)],
        gHdSdDvrUsecaseContext.swMsPrm[0].initOutRes
    );
    gHdSdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)].numInputQueues = 1;
    gHdSdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)].inQueParams[0].prevLinkId    = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)];
    gHdSdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)].inQueParams[0].prevLinkQueId = 0;

    MULTICH_HDSDDVR_CREATE_LINK(
        Vdis_getDisplayId(VDIS_DEV_HDMI),
        gHdSdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]
    );

    multich_hdsddvr_set_display_prms(
        &gHdSdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)],
        gHdSdDvrUsecaseContext.swMsPrm[1].initOutRes
    );
    gHdSdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)].numInputQueues = 1;
    gHdSdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)].inQueParams[0].prevLinkId    = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)];
    gHdSdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)].inQueParams[0].prevLinkQueId = 0;

    MULTICH_HDSDDVR_CREATE_LINK(
        Vdis_getDisplayId(VDIS_DEV_SD),
        gHdSdDvrUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_SD)]
    );
}

static
Void multich_hdsddvr_connect_links(Bool   enableScdAlgLink)
{
    DeiLink_OutputScaleFactor outScaleFactorDeiSc, outScaleFactorVipSc;


    #ifdef ENABLE_FAKE_HD_MODE
    multich_hdsddvr_configure_extvideodecoder_prm();
    #else
    gVcapModuleContext.vcapConfig.enableConfigExtVideoDecoder = FALSE;
    #endif

    /* Capture Link */
    multich_hdsddvr_set_capture_prm(&gHdSdDvrUsecaseContext.capturePrm);
    gHdSdDvrUsecaseContext.capturePrm.outQueParams[0].nextLink = gHdSdDvrUsecaseContext.selectId[SELECT_HD_SD];

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.captureId,
        gHdSdDvrUsecaseContext.capturePrm
    );

    #ifdef ENABLE_FAKE_HD_MODE
    {
        CaptureLink_SkipOddFields captureSkipOddFields;

        captureSkipOddFields.queId = 0;
        captureSkipOddFields.skipOddFieldsChBitMask = 0x3; /* Ch0, Ch1 */
        captureSkipOddFields.oddFieldSkipRatio      = CAPTURE_LINK_ODD_FIELD_SKIP_ALL;

        System_linkControl(
            gVcapModuleContext.captureId,
            CAPTURE_LINK_CMD_SKIP_ODD_FIELDS,
            &captureSkipOddFields,
            sizeof(captureSkipOddFields),
            TRUE);
    }
    #endif

    /* SELECT link
        to split 10CH into two paths of 2CHs HD + 8CH SD
    */
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].numOutQue = 2;

    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].inQueParams.prevLinkId    = gVcapModuleContext.captureId;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].inQueParams.prevLinkQueId = 0;

    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueParams[0].nextLink  = gHdSdDvrUsecaseContext.dupId[DUP_HD];
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueParams[1].nextLink  = gVcapModuleContext.deiId[DEI_SD];

    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[0].outQueId  = 0;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[0].numOutCh  = 2;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[0].inChNum[0]= 0;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[0].inChNum[1]= 1;

    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[1].outQueId  = 1;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[1].numOutCh  = 8;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[1].inChNum[0]= 2;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[1].inChNum[1]= 3;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[1].inChNum[2]= 4;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[1].inChNum[3]= 5;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[1].inChNum[4]= 6;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[1].inChNum[5]= 7;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[1].inChNum[6]= 8;
    gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD].outQueChInfo[1].inChNum[7]= 9;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.selectId[SELECT_HD_SD],
        gHdSdDvrUsecaseContext.selectPrm[SELECT_HD_SD]
    );

    /* DUP Full resolution HD capture
        1st DUP for high res live preview
        2nd DUP for H264 encode scaling, low res live preview scaling
        3rd DUP for MJPEG encode
    */
    gHdSdDvrUsecaseContext.dupPrm[DUP_HD].inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.selectId[SELECT_HD_SD];
    gHdSdDvrUsecaseContext.dupPrm[DUP_HD].inQueParams.prevLinkQueId = 0;

    gHdSdDvrUsecaseContext.dupPrm[DUP_HD].numOutQue                 = 3;
    gHdSdDvrUsecaseContext.dupPrm[DUP_HD].outQueParams[0].nextLink  = gHdSdDvrUsecaseContext.mergeId[MERGE_DISPLAY];
    gHdSdDvrUsecaseContext.dupPrm[DUP_HD].outQueParams[1].nextLink  = gVcapModuleContext.deiId[DEI_HD];
    gHdSdDvrUsecaseContext.dupPrm[DUP_HD].outQueParams[2].nextLink  = gHdSdDvrUsecaseContext.mergeId[MERGE_MJPEG];

    gHdSdDvrUsecaseContext.dupPrm[DUP_HD].notifyNextLink            = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.dupId[DUP_HD],
        gHdSdDvrUsecaseContext.dupPrm[DUP_HD]
    );

    /* DEI HD link create
        used in bypass mode as scalar
        1st output is through VIP-SC YUV420 and is used for high res encode
        2nd output is Quater of high res input and is used
                as low res input for live preview
                as well as secondary encode scaling
    */

    /* 1/2 scaling */
    outScaleFactorDeiSc.scaleMode = DEI_SCALE_MODE_RATIO;
    outScaleFactorDeiSc.ratio.widthRatio.numerator     = 1;
    outScaleFactorDeiSc.ratio.widthRatio.denominator   = 2;
    outScaleFactorDeiSc.ratio.heightRatio.numerator    = 1;
    outScaleFactorDeiSc.ratio.heightRatio.denominator  = 2;

    /* 1:1 scaling */
    outScaleFactorVipSc.scaleMode = DEI_SCALE_MODE_RATIO;
    outScaleFactorVipSc.ratio.widthRatio.numerator     = 1;
    outScaleFactorVipSc.ratio.widthRatio.denominator   = 1;
    outScaleFactorVipSc.ratio.heightRatio.numerator    = 1;
    outScaleFactorVipSc.ratio.heightRatio.denominator  = 1;

    /* DEI HD */
    multich_hdsddvr_set_hd_dei_prm(&gHdSdDvrUsecaseContext.deiPrm[DEI_HD], &outScaleFactorDeiSc, &outScaleFactorVipSc);

    gHdSdDvrUsecaseContext.deiPrm[DEI_HD].inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.dupId[DUP_HD];
    gHdSdDvrUsecaseContext.deiPrm[DEI_HD].inQueParams.prevLinkQueId = 1;
    gHdSdDvrUsecaseContext.deiPrm[DEI_HD].outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink = gVcapModuleContext.nsfId[NSF_LOW_RES];
    gHdSdDvrUsecaseContext.deiPrm[DEI_HD].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = gHdSdDvrUsecaseContext.mergeId[MERGE_ENCODE];

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.deiId[DEI_HD],
        gHdSdDvrUsecaseContext.deiPrm[DEI_HD]
    );

    /* DEI */
    multich_hdsddvr_set_sd_dei_prm(&gHdSdDvrUsecaseContext.deiPrm[DEI_SD]);

    gHdSdDvrUsecaseContext.deiPrm[DEI_SD].inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.selectId[SELECT_HD_SD];
    gHdSdDvrUsecaseContext.deiPrm[DEI_SD].inQueParams.prevLinkQueId = 1;
    gHdSdDvrUsecaseContext.deiPrm[DEI_SD].outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink = gHdSdDvrUsecaseContext.mergeId[MERGE_DISPLAY];
    gHdSdDvrUsecaseContext.deiPrm[DEI_SD].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = gHdSdDvrUsecaseContext.mergeId[MERGE_ENCODE];
    gHdSdDvrUsecaseContext.deiPrm[DEI_SD].outQueParams[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT].nextLink = gHdSdDvrUsecaseContext.mergeId[MERGE_ENCODE];
    gHdSdDvrUsecaseContext.deiPrm[DEI_SD].outQueParams[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT].nextLink = gHdSdDvrUsecaseContext.mergeId[MERGE_SCD];
    gHdSdDvrUsecaseContext.deiPrm[DEI_SD].outQueParams[DEI_LINK_OUT_QUE_DEI_SC_TERTIARY_OUT].nextLink = gHdSdDvrUsecaseContext.mergeId[MERGE_MJPEG];

    if(!enableScdAlgLink)
    {
        /* disable DEI_SC output if SCD is not enabled  */
        gHdSdDvrUsecaseContext.deiPrm[DEI_SD].enableOut[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT] = FALSE;
    }

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.deiId[DEI_SD],
        gHdSdDvrUsecaseContext.deiPrm[DEI_SD]
    );


    /* NSF link
        to convert YUV422 data to YUV420, this is to save DDR BW (double read issue with YUV422 input) at
        subsequent steps of scaling
    */
    multich_hdsddvr_set_nsf_prm(&gHdSdDvrUsecaseContext.nsfPrm[NSF_LOW_RES], NUM_BUFS_PER_CH_NSF_LOW_RES);

    gHdSdDvrUsecaseContext.nsfPrm[NSF_LOW_RES].inQueParams.prevLinkId    = gVcapModuleContext.deiId[DEI_HD];
    gHdSdDvrUsecaseContext.nsfPrm[NSF_LOW_RES].inQueParams.prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC;
    gHdSdDvrUsecaseContext.nsfPrm[NSF_LOW_RES].outQueParams[0].nextLink  = gHdSdDvrUsecaseContext.dupId[DUP_LOW_RES];

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.nsfId[NSF_LOW_RES],
        gHdSdDvrUsecaseContext.nsfPrm[NSF_LOW_RES]
    );

    /* DUP low resoluation scaled capture CHs
        1st DUP for live preview
        2nd DUP for H264 encode scaling, SCD scaling
    */
    gHdSdDvrUsecaseContext.dupPrm[DUP_LOW_RES].inQueParams.prevLinkId    = gVcapModuleContext.nsfId[NSF_LOW_RES];
    gHdSdDvrUsecaseContext.dupPrm[DUP_LOW_RES].inQueParams.prevLinkQueId = 0;

    gHdSdDvrUsecaseContext.dupPrm[DUP_LOW_RES].numOutQue                 = 2;
    gHdSdDvrUsecaseContext.dupPrm[DUP_LOW_RES].outQueParams[0].nextLink  = gHdSdDvrUsecaseContext.mergeId[MERGE_DISPLAY];
    gHdSdDvrUsecaseContext.dupPrm[DUP_LOW_RES].outQueParams[1].nextLink  = gVcapModuleContext.deiId[DEI_LOW_RES];

    gHdSdDvrUsecaseContext.dupPrm[DUP_LOW_RES].notifyNextLink            = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.dupId[DUP_LOW_RES],
        gHdSdDvrUsecaseContext.dupPrm[DUP_LOW_RES]
    );

    /* DEI LOW RES
        used in bypass mode as scalar
        1st output is through VIP-SC YUV420 and is used for secondary stream D1 encode
        2nd output is CIF and is used for SCD
    */

    /* CIF scaling */
    outScaleFactorDeiSc.scaleMode = DEI_SCALE_MODE_ABSOLUTE;
    outScaleFactorDeiSc.absoluteResolution.outWidth  = 352;
    outScaleFactorDeiSc.absoluteResolution.outHeight = 240;

    /* D1 scaling */
    outScaleFactorVipSc.scaleMode = DEI_SCALE_MODE_ABSOLUTE;
    outScaleFactorVipSc.absoluteResolution.outWidth  = MAX_HD_SECONDARY_WIDTH;
    outScaleFactorVipSc.absoluteResolution.outHeight = MAX_HD_SECONDARY_HEIGHT;

    /* DEI */
    multich_hdsddvr_set_hd_dei_prm(&gHdSdDvrUsecaseContext.deiPrm[DEI_LOW_RES], &outScaleFactorDeiSc, &outScaleFactorVipSc);

    gHdSdDvrUsecaseContext.deiPrm[DEI_LOW_RES].inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.dupId[DUP_LOW_RES];
    gHdSdDvrUsecaseContext.deiPrm[DEI_LOW_RES].inQueParams.prevLinkQueId = 1;
    gHdSdDvrUsecaseContext.deiPrm[DEI_LOW_RES].outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink = gHdSdDvrUsecaseContext.mergeId[MERGE_SCD];
    gHdSdDvrUsecaseContext.deiPrm[DEI_LOW_RES].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = gHdSdDvrUsecaseContext.mergeId[MERGE_ENCODE];

    /* SCD at 5fps */
    gHdSdDvrUsecaseContext.deiPrm[DEI_LOW_RES].inputFrameRate[DEI_LINK_OUT_QUE_DEI_SC]   = 30;
    gHdSdDvrUsecaseContext.deiPrm[DEI_LOW_RES].outputFrameRate[DEI_LINK_OUT_QUE_DEI_SC]  =  5;


    if(!enableScdAlgLink)
    {
        /* disable DEI_SC output if SCD is not enabled  */
        gHdSdDvrUsecaseContext.deiPrm[DEI_LOW_RES].enableOut[DEI_LINK_OUT_QUE_DEI_SC] = FALSE;
    }

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.deiId[DEI_LOW_RES],
        gHdSdDvrUsecaseContext.deiPrm[DEI_LOW_RES]
    );

    /* MERGE Link
        to merge input for HD MJPEG encode, SD MJPEG encode
    */
    gHdSdDvrUsecaseContext.mergePrm[MERGE_MJPEG].numInQue = 2;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_MJPEG].inQueParams[0].prevLinkId    = gHdSdDvrUsecaseContext.dupId[DUP_HD];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_MJPEG].inQueParams[0].prevLinkQueId = 2;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_MJPEG].inQueParams[1].prevLinkId    = gVcapModuleContext.deiId[DEI_SD];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_MJPEG].inQueParams[1].prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC_TERTIARY_OUT;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_MJPEG].outQueParams.nextLink        = gVcapModuleContext.nsfId[NSF_MJPEG];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_MJPEG].notifyNextLink               = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.mergeId[MERGE_MJPEG],
        gHdSdDvrUsecaseContext.mergePrm[MERGE_MJPEG]
    );

    /* NSF link
        to convert YUV422 data to YUV420 for MJPEG encode, @ 1fps
    */
    multich_hdsddvr_set_nsf_prm(&gHdSdDvrUsecaseContext.nsfPrm[NSF_MJPEG], NUM_BUFS_PER_CH_NSF_MJPEG);

    gHdSdDvrUsecaseContext.nsfPrm[NSF_MJPEG].inQueParams.prevLinkId    = gHdSdDvrUsecaseContext.mergeId[MERGE_MJPEG];
    gHdSdDvrUsecaseContext.nsfPrm[NSF_MJPEG].inQueParams.prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.nsfPrm[NSF_MJPEG].outQueParams[0].nextLink  = gHdSdDvrUsecaseContext.mergeId[MERGE_ENCODE];

    MULTICH_HDSDDVR_CREATE_LINK(
        gVcapModuleContext.nsfId[NSF_MJPEG],
        gHdSdDvrUsecaseContext.nsfPrm[NSF_MJPEG]
    );

    /* CH0, 1 set to 1fps */
    {
        NsfLink_ChFpsParams nsfFps;
        int i;

        for(i=0; i<MAX_NUM_HD_CAPTURE_CHANNELS; i++)
        {
            nsfFps.chId = i;
            nsfFps.inputFrameRate = 30;
            nsfFps.outputFrameRate = 1;

            System_linkControl(
                gVcapModuleContext.nsfId[NSF_MJPEG],
                NSF_LINK_CMD_SET_FRAME_RATE,
                &nsfFps,
                sizeof(nsfFps),
                TRUE);
        }
    }

    /* MERGE Link
        to merge input for H264 primary encode, H264 secondary encode, MJPEG encode
    */
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].numInQue = 5;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[0].prevLinkId    = gVcapModuleContext.deiId[DEI_HD];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[0].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[1].prevLinkId    = gVcapModuleContext.deiId[DEI_SD];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[1].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[2].prevLinkId    = gVcapModuleContext.deiId[DEI_LOW_RES];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[2].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[3].prevLinkId    = gVcapModuleContext.deiId[DEI_SD];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[3].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[4].prevLinkId    = gVcapModuleContext.nsfId[NSF_MJPEG];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].inQueParams[4].prevLinkQueId = 0;
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].outQueParams.nextLink        = gVcapModuleContext.ipcFramesOutVpssId[0];
    gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE].notifyNextLink               = TRUE;

    MULTICH_HDSDDVR_CREATE_LINK(
        gHdSdDvrUsecaseContext.mergeId[MERGE_ENCODE],
        gHdSdDvrUsecaseContext.mergePrm[MERGE_ENCODE]
    );

    multich_hdsddvr_connect_osd_links();

    if(enableScdAlgLink)
    {
        multich_hdsddvr_connect_scd_links();
    }

    multich_hdsddvr_connect_encode_links();
    multich_hdsddvr_connect_decode_links();
    multich_hdsddvr_connect_display_links();
}

static
void multich_hdsddvr_disable_all_play_ch_in_swms(VDIS_DEV vdDevId, VDIS_MOSAIC_S *psVdMosaicParam, SwMsLink_LayoutPrm *vdisLayoutPrm)
{
    UInt32 winId, chId;

    for(winId=0; winId<vdisLayoutPrm->numWin; winId++)
    {
        chId = psVdMosaicParam->chnMap[winId];

        if(chId >= MAX_NUM_CAPTURE_CHANNELS && chId < (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS))
        {
            Vdis_setChn2WinMap(vdDevId, chId, SYSTEM_SW_MS_INVALID_ID);

            vdisLayoutPrm->winInfo[winId].channelNum = SYSTEM_SW_MS_INVALID_ID;
        }
    }
}

static
int multich_hdsddvr_decode_ch_enable(UInt32 chId, Bool enable)
{
    UInt32 cmdId;
    DecLink_ChannelInfo decChInfo;
    Int32 status;

    if(chId >= MAX_NUM_DECODE_CHANNELS)
        return -1;

    if(enable)
    {
        cmdId = DEC_LINK_CMD_ENABLE_CHANNEL;
    }
    else
    {
        cmdId = DEC_LINK_CMD_DISABLE_CHANNEL;
    }

    decChInfo.chId     = chId;

    status = System_linkControl(gVdecModuleContext.decId, cmdId, &decChInfo, sizeof(decChInfo), TRUE);
    OSA_assert(status == 0);

    return status;
}

/*
    assume's playback channels visible in only one display
*/
static
void multich_hdsddvr_enable_visible_decode_channels(SwMsLink_LayoutPrm *vdisLayoutPrm)
{
    UInt32 winId, chId;
    UInt32 decodeChEnable[MAX_NUM_DECODE_CHANNELS];

    for(chId=0; chId<MAX_NUM_DECODE_CHANNELS; chId++)
    {
        decodeChEnable[chId] = FALSE;
    }

    for(winId=0; winId<vdisLayoutPrm->numWin; winId++)
    {
        chId = vdisLayoutPrm->winInfo[winId].channelNum;

        if(chId >= MAX_NUM_CAPTURE_CHANNELS && chId < (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS))
        {
            decodeChEnable[chId-MAX_NUM_CAPTURE_CHANNELS] = TRUE;
        }
    }

    for(chId=0; chId<MAX_NUM_DECODE_CHANNELS; chId++)
    {
        multich_hdsddvr_decode_ch_enable(chId, decodeChEnable[chId]);
    }
}

Void MultiCh_createHdSdDvr()
{

    multich_hdsddvr_reset_link_prms();
    multich_hdsddvr_set_link_ids();
    printf("\n********* Entered usecase HdSdDvr <816x> Cap/Enc/Dec/Dis \n\n");

    MultiCh_detectBoard();

    System_linkControl(
        SYSTEM_LINK_ID_M3VPSS,
        SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES,
        NULL,
        0,
        TRUE
        );

    System_linkControl(
        SYSTEM_LINK_ID_M3VIDEO,
        SYSTEM_COMMON_CMD_SET_CH2IVAHD_MAP_TBL,
        &systemVid_encDecIvaChMapTbl,
        sizeof(SystemVideo_Ivahd2ChMap_Tbl),
        TRUE
    );

    if ((FALSE == TILER_ENABLE_ENCODE) && (FALSE == TILER_ENABLE_DECODE))
    {
        SystemTiler_disableAllocator();
    }
    multich_hdsddvr_connect_links(gVsysModuleContext.vsysConfig.enableScd);

    multich_hdsddvr_enable_visible_decode_channels(
        &gHdSdDvrUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]
    );
}

Void MultiCh_deleteHdSdDvr()
{
    UInt32 i;

    for (i = 0; i < gHdSdDvrUsecaseContext.createdLinkCount; i++)
    {
        System_linkDelete (gHdSdDvrUsecaseContext.createdLinks[i]);
    }
    gHdSdDvrUsecaseContext.createdLinkCount = 0;
    multich_hdsddvr_reset_link_ids();

    Vcap_deleteVideoDecoder();
    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    MultiCh_prfLoadCalcEnable(FALSE, TRUE, FALSE);

    if ((FALSE == TILER_ENABLE_ENCODE) && (FALSE == TILER_ENABLE_DECODE))
    {
        SystemTiler_enableAllocator();
    }
}


Int32 MultiCh_hdSdDvrSetMosaicParams(VDIS_DEV vdDevId, VDIS_MOSAIC_S *psVdMosaicParam )
{
    UInt32 swMsId;
    Int32 retVal = -1;
    Uint32 numLiveCh=0, numPlayCh=0, numHighResCh=0;
    SwMsLink_LayoutPrm *vdisLayoutPrm;
    UInt32 winId, chId;

    swMsId = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(vdDevId)];
    if(swMsId==SYSTEM_LINK_ID_INVALID)
        return -1;

    vdisLayoutPrm = &gHdSdDvrUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(vdDevId)];

    /* Get display resolution and coordinates */
    vdisLayoutPrm->numWin = psVdMosaicParam->numberOfWindows;
    vdisLayoutPrm->onlyCh2WinMapChanged = psVdMosaicParam->onlyCh2WinMapChanged;
    vdisLayoutPrm->outputFPS = psVdMosaicParam->outputFPS;

    /* Assign each windows coordinates, size and mapping */
    for(winId=0; winId<vdisLayoutPrm->numWin; winId++)
    {
        chId = psVdMosaicParam->chnMap[winId];

        vdisLayoutPrm->winInfo[winId].channelNum         = chId;
        vdisLayoutPrm->winInfo[winId].bufAddrOffset[0u]  = -1;
        vdisLayoutPrm->winInfo[winId].bufAddrOffset[1u]  = -1;
        vdisLayoutPrm->winInfo[winId].width              = psVdMosaicParam->winList[winId].width;
        vdisLayoutPrm->winInfo[winId].height             = psVdMosaicParam->winList[winId].height;
        vdisLayoutPrm->winInfo[winId].startX             = psVdMosaicParam->winList[winId].start_X;
        vdisLayoutPrm->winInfo[winId].startY             = psVdMosaicParam->winList[winId].start_Y;
        vdisLayoutPrm->winInfo[winId].bypass             = FALSE;

        if(chId >= (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS))
        {
            /* not a valid CH ID, mark it as invalid */
            vdisLayoutPrm->winInfo[winId].channelNum = SYSTEM_SW_MS_INVALID_ID;
            continue;
        }

        if(chId < gVdisModuleContext.vdisConfig.numChannels)
        {
            Vdis_setChn2WinMap(vdDevId, chId,winId);

            if(Vdis_isEnableChn(vdDevId,chId) == FALSE)
            {
                vdisLayoutPrm->winInfo[winId].channelNum = SYSTEM_SW_MS_INVALID_ID;
            }
        }

        if(chId >= MAX_NUM_CAPTURE_CHANNELS && chId < (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS))
            numPlayCh++;
        if(chId < MAX_NUM_HD_CAPTURE_CHANNELS )
        {
            numLiveCh++;
            if( vdisLayoutPrm->winInfo[winId].width > (MAX_HD_PRIMARY_WIDTH/2)
                &&
                vdisLayoutPrm->winInfo[winId].height > (MAX_HD_PRIMARY_HEIGHT/2)
                )
            {
                if(numHighResCh==0)
                {
                    /* switch to high res channel */
                    /* can have only one high res channel in a layout */
                    vdisLayoutPrm->winInfo[winId].channelNum = chId + (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS);
                }

                numHighResCh++;
            }
        }
    }

    if(vdDevId==VDIS_DEV_SD)
    {
        /*
            Cannot have playback in SDTV, so mark all playback CHs as invalid in SDTV
        */
        if(numPlayCh>0)
        {
            multich_hdsddvr_disable_all_play_ch_in_swms(vdDevId, psVdMosaicParam, vdisLayoutPrm);
        }
    }
    if(vdDevId==VDIS_DEV_HDMI)
    {
        /* Cannot have mix of live and playback CHs
            disable playback CHs in this case
        */
        if(numPlayCh>0 && numLiveCh>0)
        {
            multich_hdsddvr_disable_all_play_ch_in_swms(vdDevId, psVdMosaicParam, vdisLayoutPrm);
        }
    }

    Vdis_swMs_PrintLayoutParams(vdDevId, vdisLayoutPrm);
    System_linkControl(swMsId, SYSTEM_SW_MS_LINK_CMD_SWITCH_LAYOUT, (vdisLayoutPrm), sizeof(*vdisLayoutPrm), TRUE);

    if(vdDevId==VDIS_DEV_HDMI)
    {
        /* since only HDTV can show playback channels */
        multich_hdsddvr_enable_visible_decode_channels(vdisLayoutPrm);
    }

    return retVal;
}

void  MultiCh_hdSdDvrSwmsChReMap(VDIS_MOSAIC_S *psVdMosaicParam)
{
    Int32 winId, chId;

    /*
        Convert from SW Moisac Ch ID  to McFW CH ID

        SWMS Ch ID                             McFW CH ID
        0..9    (low res live CHs in SWMS)     0..9       Live channels in McFW
        10..17  ( playback CHs in SWMS)        10..17     Playback channels in McFW
        18..19  (high res live CHs in SWMS)    0..1       Live channels in McFW
     */

    for (winId = 0; winId < psVdMosaicParam->numberOfWindows; winId++)
    {
        chId = psVdMosaicParam->chnMap[winId];

        if ( chId >= (MAX_NUM_CAPTURE_CHANNELS+MAX_NUM_DECODE_CHANNELS) )
        {
            if(chId != (UInt32)(VDIS_CHN_INVALID) )
            {
                chId -= (MAX_NUM_DECODE_CHANNELS+MAX_NUM_CAPTURE_CHANNELS);
                chId = (chId % MAX_NUM_HD_CAPTURE_CHANNELS); /* only HD channels are high res */
            }
        }

        psVdMosaicParam->chnMap[winId] = chId;
    }
}

Int32 MultiCh_hdSdDvrSetCapFrameRate(VCAP_CHN vcChnId, VCAP_STRM vcStrmId, Int32 inputFrameRate, Int32 outputFrameRate)
{
    DeiLink_ChFpsParams chInfo;
    UInt32 linkId;
    Int32 retVal = -1;
    NsfLink_ChFpsParams nsfFpsInfo;

    if(vcChnId >= MAX_NUM_CAPTURE_CHANNELS)
        return -1;

    /*
        0: Primary
        1: Secondary
        2: MJPEG
    */

    if(vcStrmId >= 3)
        return -1;

    /* primary stream */
    if(vcStrmId==0)
    {
        if(vcChnId < MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            linkId          = gVcapModuleContext.deiId[DEI_HD];
            chInfo.chId     = vcChnId;
        }
        else
        {
            linkId = gVcapModuleContext.deiId[DEI_SD];
            chInfo.chId     = vcChnId - MAX_NUM_HD_CAPTURE_CHANNELS;
        }

        chInfo.streamId         = DEI_LINK_OUT_QUE_VIP_SC;
        chInfo.inputFrameRate   = inputFrameRate;
        chInfo.outputFrameRate  = outputFrameRate;

        retVal = System_linkControl(linkId, DEI_LINK_CMD_SET_FRAME_RATE, &chInfo, sizeof(chInfo), TRUE);
    }
    /* secondary stream */
    if(vcStrmId==1)
    {
        if(vcChnId < MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            linkId          = gVcapModuleContext.deiId[DEI_LOW_RES];
            chInfo.chId     = vcChnId;
            chInfo.streamId = DEI_LINK_OUT_QUE_VIP_SC;
        }
        else
        {
            linkId          = gVcapModuleContext.deiId[DEI_SD];
            chInfo.chId     = vcChnId - MAX_NUM_HD_CAPTURE_CHANNELS;
            chInfo.streamId = DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT;
        }

        chInfo.inputFrameRate   = inputFrameRate;
        chInfo.outputFrameRate  = outputFrameRate;

        retVal = System_linkControl(linkId, DEI_LINK_CMD_SET_FRAME_RATE, &chInfo, sizeof(chInfo), TRUE);
    }
    /* MJPEG stream */
    if(vcStrmId==2)
    {
        if(vcChnId < MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            linkId = gVcapModuleContext.nsfId[NSF_MJPEG];

            nsfFpsInfo.chId            = vcChnId;
            nsfFpsInfo.inputFrameRate  = inputFrameRate;
            nsfFpsInfo.outputFrameRate = outputFrameRate;

            retVal = System_linkControl(linkId, NSF_LINK_CMD_SET_FRAME_RATE, &nsfFpsInfo, sizeof(nsfFpsInfo), TRUE);
        }
        else
        {
            linkId                  = gVcapModuleContext.deiId[DEI_SD];
            chInfo.chId             = vcChnId - MAX_NUM_HD_CAPTURE_CHANNELS;
            chInfo.streamId         = DEI_LINK_OUT_QUE_DEI_SC_TERTIARY_OUT;
            chInfo.inputFrameRate   = inputFrameRate;
            chInfo.outputFrameRate  = outputFrameRate;

            retVal = System_linkControl(linkId, DEI_LINK_CMD_SET_FRAME_RATE, &chInfo, sizeof(chInfo), TRUE);
        }
    }

    return retVal;
}

Int32 MultiCh_hdSdDvrEnableDisableCapChn(VCAP_CHN vcChnId, VCAP_STRM vcStrmId, Bool enableChn)
{
    DeiLink_ChannelInfo chInfo;
    UInt32 linkId, cmdId;
    Int32 retVal = -1;
    NsfLink_ChFpsParams nsfFpsInfo;

    if(vcChnId >= MAX_NUM_CAPTURE_CHANNELS)
        return -1;

    /*
        0: Primary
        1: Secondary
        2: MJPEG
    */

    if(vcStrmId >= 3)
        return -1;

    /* primary stream */
    if(vcStrmId==0)
    {
        if(vcChnId < MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            linkId           = gVcapModuleContext.deiId[DEI_HD];
            chInfo.channelId = vcChnId;
        }
        else
        {
            linkId           = gVcapModuleContext.deiId[DEI_SD];
            chInfo.channelId = vcChnId - MAX_NUM_HD_CAPTURE_CHANNELS;
        }

        if(enableChn)
            cmdId = DEI_LINK_CMD_ENABLE_CHANNEL;
        else
            cmdId = DEI_LINK_CMD_DISABLE_CHANNEL;

        chInfo.streamId  = DEI_LINK_OUT_QUE_VIP_SC;
        chInfo.enable    = enableChn;

        retVal = System_linkControl(linkId, cmdId, &chInfo, sizeof(chInfo), TRUE);
    }
    /* secondary stream */
    if(vcStrmId==1)
    {
        if(vcChnId < MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            linkId          = gVcapModuleContext.deiId[DEI_LOW_RES];
            chInfo.channelId= vcChnId;
            chInfo.streamId = DEI_LINK_OUT_QUE_VIP_SC;
        }
        else
        {
            linkId          = gVcapModuleContext.deiId[DEI_SD];
            chInfo.channelId= vcChnId - MAX_NUM_HD_CAPTURE_CHANNELS;
            chInfo.streamId = DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT;
        }

        if(enableChn)
            cmdId = DEI_LINK_CMD_ENABLE_CHANNEL;
        else
            cmdId = DEI_LINK_CMD_DISABLE_CHANNEL;

        chInfo.enable    = enableChn;

        retVal = System_linkControl(linkId, cmdId, &chInfo, sizeof(chInfo), TRUE);
    }
    /* MJPEG stream */
    if(vcStrmId==2)
    {
        /* disable channel by setting FPS to 0 */
        linkId = gVcapModuleContext.nsfId[NSF_MJPEG];

        cmdId = NSF_LINK_CMD_SET_FRAME_RATE;

        nsfFpsInfo.chId = vcChnId;
        nsfFpsInfo.inputFrameRate = 30;
        if(enableChn)
            nsfFpsInfo.outputFrameRate = 1; /* 1 fps */
        else
            nsfFpsInfo.outputFrameRate = 0; /* 0 fps */

        retVal = System_linkControl(linkId, cmdId, &nsfFpsInfo, sizeof(nsfFpsInfo), TRUE);
    }

    return retVal;
}

Int32 MultiCh_hdSdDvrSetCapDynamicParamChn(VCAP_CHN vcChnId, VCAP_CHN_DYNAMIC_PARAM_S *psCapChnDynaParam, VCAP_PARAMS_E paramId)
{
    UInt32 linkId, maxWidth, maxHeight;
    Int32 retVal = -1;
    Bool applyOnPrimary, applyOnSecondary;
    DeiLink_chDynamicSetOutRes deiOutRes;

    if(vcChnId >= MAX_NUM_CAPTURE_CHANNELS)
        return -1;

    /*
        VCAP_PATH_PREVIEW - resolution change not supported
        VCAP_PATH_PRIMARY_STREAM - Primary stream (full res)
        VCAP_PATH_SECONDARY_STREAM - Secondary stream (low res)

        resolution change not supported on MJPEG stream
    */

    if(paramId!=VCAP_RESOLUTION)
        return -1;

    applyOnPrimary = FALSE;
    applyOnSecondary = FALSE;

    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_PRIMARY_STREAM)
    {
        applyOnPrimary = TRUE;
    } else
    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_SECONDARY_STREAM)
    {
        applyOnSecondary = TRUE;
    } else
    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_ALL)
    {
        applyOnPrimary = TRUE;
        applyOnSecondary = TRUE;
    }

    if(applyOnPrimary)
    {
        if(vcChnId < MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            linkId = gVcapModuleContext.deiId[DEI_HD];
            deiOutRes.chId  = vcChnId;
            maxWidth = MAX_HD_PRIMARY_WIDTH;
            maxHeight = MAX_HD_PRIMARY_HEIGHT;
        }
        else
        {
            linkId = gVcapModuleContext.deiId[DEI_SD];
            deiOutRes.chId  = vcChnId - MAX_NUM_HD_CAPTURE_CHANNELS;
            maxWidth = MAX_SD_PRIMARY_WIDTH;
            maxHeight = MAX_SD_PRIMARY_HEIGHT;
        }

        deiOutRes.queId = DEI_LINK_OUT_QUE_VIP_SC;

        retVal = System_linkControl(linkId, DEI_LINK_CMD_GET_OUTPUTRESOLUTION, &deiOutRes, sizeof(deiOutRes), TRUE);

        if(retVal!=0)
            return retVal;

        deiOutRes.width  = psCapChnDynaParam->chDynamicRes.width;
        deiOutRes.height = psCapChnDynaParam->chDynamicRes.height;

        if(deiOutRes.width > maxWidth)
            deiOutRes.width = maxWidth;
        if(deiOutRes.height > maxHeight)
            deiOutRes.height = maxHeight;

        retVal = System_linkControl(linkId, DEI_LINK_CMD_SET_OUTPUTRESOLUTION, &deiOutRes, sizeof(deiOutRes), TRUE);
    }
    if(applyOnSecondary)
    {
        if(vcChnId < MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            linkId          = gVcapModuleContext.deiId[DEI_LOW_RES];
            deiOutRes.chId  = vcChnId;
            deiOutRes.queId = DEI_LINK_OUT_QUE_VIP_SC;
            maxWidth = MAX_HD_SECONDARY_WIDTH;
            maxHeight = MAX_HD_SECONDARY_HEIGHT;
        }
        else
        {
            linkId          = gVcapModuleContext.deiId[DEI_SD];
            deiOutRes.chId  = vcChnId - MAX_NUM_HD_CAPTURE_CHANNELS;
            deiOutRes.queId = DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT;
            maxWidth = MAX_SD_SECONDARY_WIDTH;
            maxHeight = MAX_SD_SECONDARY_HEIGHT;
        }

        retVal = System_linkControl(linkId, DEI_LINK_CMD_GET_OUTPUTRESOLUTION, &deiOutRes, sizeof(deiOutRes), TRUE);

        if(retVal!=0)
            return retVal;

        deiOutRes.width  = psCapChnDynaParam->chDynamicRes.width;
        deiOutRes.height = psCapChnDynaParam->chDynamicRes.height;

        if(deiOutRes.width > maxWidth)
            deiOutRes.width = maxWidth;
        if(deiOutRes.height > maxHeight)
            deiOutRes.height = maxHeight;

        retVal = System_linkControl(linkId, DEI_LINK_CMD_SET_OUTPUTRESOLUTION, &deiOutRes, sizeof(deiOutRes), TRUE);
    }

    return retVal;
}

Int32 MultiCh_hdSdDvrGetCapDynamicParamChn(VCAP_CHN vcChnId, VCAP_CHN_DYNAMIC_PARAM_S *psCapChnDynaParam, VCAP_PARAMS_E paramId)
{
    UInt32 linkId;
    Int32 retVal = -1;
    Bool applyOnPrimary, applyOnSecondary;
    DeiLink_chDynamicSetOutRes deiOutRes;

    psCapChnDynaParam->chDynamicRes.width  = 0;
    psCapChnDynaParam->chDynamicRes.height = 0;

    if(vcChnId >= MAX_NUM_CAPTURE_CHANNELS)
        return -1;

    /*
        VCAP_PATH_PREVIEW - resolution get not supported
        VCAP_PATH_PRIMARY_STREAM - Primary stream (full res)
        VCAP_PATH_SECONDARY_STREAM - Secondary stream (low res)

        resolution get not supported on MJPEG stream
    */

    if(paramId!=VCAP_RESOLUTION)
        return -1;

    applyOnPrimary = FALSE;
    applyOnSecondary = FALSE;

    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_PRIMARY_STREAM)
    {
        applyOnPrimary = TRUE;
    } else
    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_SECONDARY_STREAM)
    {
        applyOnSecondary = TRUE;
    } else
    if(psCapChnDynaParam->chDynamicRes.pathId==VCAP_PATH_ALL)
    {
        /* get cannot be on 'all' */
        return -1;
    }

    if(applyOnPrimary)
    {
        if(vcChnId < MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            linkId = gVcapModuleContext.deiId[DEI_HD];
            deiOutRes.chId  = vcChnId;
        }
        else
        {
            linkId = gVcapModuleContext.deiId[DEI_SD];
            deiOutRes.chId  = vcChnId - MAX_NUM_HD_CAPTURE_CHANNELS;
        }

        deiOutRes.queId = DEI_LINK_OUT_QUE_VIP_SC;
    }
    else
    if(applyOnSecondary)
    {
        if(vcChnId < MAX_NUM_HD_CAPTURE_CHANNELS)
        {
            linkId          = gVcapModuleContext.deiId[DEI_LOW_RES];
            deiOutRes.chId  = vcChnId;
            deiOutRes.queId = DEI_LINK_OUT_QUE_VIP_SC;
        }
        else
        {
            linkId          = gVcapModuleContext.deiId[DEI_SD];
            deiOutRes.chId  = vcChnId - MAX_NUM_HD_CAPTURE_CHANNELS;
            deiOutRes.queId = DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT;
        }
    }
    else
    {
        return -1;
    }

    retVal = System_linkControl(linkId, DEI_LINK_CMD_GET_OUTPUTRESOLUTION, &deiOutRes, sizeof(deiOutRes), TRUE);
    if(retVal!=0)
        return retVal;

    psCapChnDynaParam->chDynamicRes.width = deiOutRes.width;
    psCapChnDynaParam->chDynamicRes.height = deiOutRes.height;

    return retVal;
}
