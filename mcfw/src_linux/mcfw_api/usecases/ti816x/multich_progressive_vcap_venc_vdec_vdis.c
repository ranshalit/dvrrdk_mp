/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/


#include "mcfw/src_linux/mcfw_api/usecases/multich_common.h"
#include "mcfw/src_linux/mcfw_api/usecases/multich_ipcbits.h"
#include "mcfw/interfaces/link_api/avsync_hlos.h"
#include "mcfw/src_linux/devices/tw2968/src/tw2968_priv.h"
#include "mcfw/src_linux/devices/tvp5158/src/tvp5158_priv.h"
#include "mcfw/interfaces/link_api/system_tiler.h"

#ifdef MIN
#undef MIN
#endif


#define MIN(a,b) (((a) < (b)) ? (a) : (b))

/* =============================================================================
 * Externs
 * =============================================================================
 */

static UInt8 SCDChannelMonitor[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
/* =============================================================================
 * Use case code
 * =============================================================================
 */

/**
                        +----------------------+
                        |   Capture (YUV422I)  |
                        |                      |
                        |   16 CH D1 60fps     |
                        +----------+-----------+
                                   |
                                   |
                                   v
                        +----------------------+
                        | CAPTURE_DISPLAY_DUP  |
                        +--+------------+------+
       +-------------------+            |
       |                                |
       |                                v            <<Process
       |                           +--------------+    Link>>    +---------------+      +---------+
       |                           +IPC Frames Out+------------->|IPC Frames In  +----->| AlgLink |
       |                           |    (M3)      <--------------+    (DSP)      <------+  (SWOSD)|
       |                           +---+-------+--+              +---------------+      +---------+
       |                               |       |
       |                               |       |
       |                               |       |8CH D1
       |                         8CH D1|       |YUV422
       |                         YUV422|       |
       |                               |       +---------------------+
       |                               |                             |
       |                               |                             |
       |                               v                             v
       |                         +-------------+              +--------------+
       |                         |   DEIH      |              |    DEI       |
       |                         |             |              |              |
       |                         |DEI_SC VIP_SC|              |DEI_SC  VIP_SC|
       |                         +-------------+              +--------------+
       |                      8CH D1|    |  |8CH D1          8CH D1|   |  |8CH D1
       |                      YUV422|    |  |YUV420          YUV422|   |  |YUV420
       |                            |    |  |                      |   |  +---------------+
       |                            |    |  +----------------------|---|-------+          |
       |                            |    |                         |   |       |          |
       |                            |    |8CH CIF               8CH|CIF|       |          |
       |                  +---------+    |YUV420                YUV|420|      +v----------v----+
       |                  |              |                         |   |      |MERGE_VIP_SC_D1 |
       |                  |     +----------------------------------+   |      |                |
       |                  |     |        |                             |      +---------+------+
       |                  |     |        +---------+    +--------------+                |16CH D1
       |                  |     |                  |    |                               v
       |                  |     |                  |    |                      +---------------+
       |                  |     |                  |    |                      |   DUP_D1_LINK |
       |                  |     |                  v    v                      |               |
       |                  |     |            +----------------+                +-------+-----+-+
       |                  |     |            |MERGE_VIP_SC_   |                 16CH D1|     |16CH D1
       |                  |     |            |SECONDARY_OUT   |                  YUV420|     |YUV420
       |                  |     |            +-------+--------+                  PRI   |     |MJPEG
       |                  |     |                    |                                 |     |
       |                  |     |                    |16CH CIF                         |     |
       |                  |     |                    |YUV420        +--------------+   |     |
       |                  |     |                    v              |    16CH CIF  |   |     |
       |                  |     |            +-----------------+    |    YUV420    |   |     |
       |                  |     |            |  CIF DUP LINK   |    |              v   v     v
       |                  |     |            |                 |    |             +----------------+
       |                  |     |            +--+----+------+--+    |             | MERGE_D1_CIF   |
       |                  |     |               |    |      |       |             |                |
       |                  |     |               |    |      +-------+             +---------+------+
       |                  |     |    +----------+    |                                      |            48CH YUV420
       |            8CH   | 8CH |    |               |                                      +-------------------+
       +            D1    |  D1 |    |16CH           v                                                          |
       v                  |     |    |CIF       +-------------+       +------------+        +----------+        |
  +----------+            v     v    v          |IPC FramesOut+------>|IPC FramesIn+------->| AlgLink  |        |
  |SDTV Disp |         +--------------+         |   (M3)      <------+|   (DSP)    |<------+|  (SCD)   |        |
  |          |         |              |<---+    +-------------+       +------------+        +----------+        |
  +----------+         | LIVE_DECODE  | 16CH                                                                    |
                       |  MERGE       | D1 |                                                                    |
                       |              |Play|     +------------+          +--------+         +--------+          |
                       +-----+--------+    |     |            |          |IPCBits |         |IPCBits |          |
                             |             +-----+ DecLink    |<---------|Out (A8)|         |In(A8)  |          |
                             |                   +------------+  IPCBits +--------+         +--------+          v
                             |48CH                               In(M3)                         ^        +------------+
                             |                                                           IPCBits|        |  IPCM3OUT  |
                             |                                                           Out(M3)|        +------+-----+
                             v                                                              +---+-----+         |
                       +--------------+                                                     |EncLink  |         |
                       | LIVE_DECODE  |                                                     +---------+         v
                       |  DUP         |                                                          ^       +------------+
                       +-+----------+-+                                                          |       |  IPCM3IN   |
                         |          |                                                            |       +------+-----+
                         |          |                                                            |              |
                         v          v                                                            +--------------+
                  +---------+    +-----------+
                  |SWMS0    |    |SWMS1      |
                  |         |    |           |
                  +----+----+    +----+------+
                       |              |
                       v              v
                   On-chip          Off-chip  HDDAC
                   HDMI             HDMI     + (VGA)
                   (HDMI0)          (HDMI1)

*/

#define     MAX_NUM_CAPTURE_DEVICES          4
#define     NUM_IPC_OUT_BUFFERS              20

#define     MAX_BUFFERING_QUEUE_LEN_PER_CH           (50)

#define     BIT_BUF_LENGTH_LIMIT_FACTOR_SD            (6)

/* =============================================================================
 * Externs
 * =============================================================================
 */


/* =============================================================================
 * Use case code
 * =============================================================================
 */
static SystemVideo_Ivahd2ChMap_Tbl systemVid_encDecIvaChMapTbl =
{
    .isPopulated = 1,
    .ivaMap[0] =
    {
        .EncNumCh  = 17,
        .EncChList = {0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 31, 33, 36, 39, 42, 45 },
        .DecNumCh  = 5,
        .DecChList = {0, 3, 6, 9, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    },
    .ivaMap[1] =
    {
        .EncNumCh  = 16,
        .EncChList = {1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 34, 37, 40, 43, 46, 47 },
        .DecNumCh  = 5,
        .DecChList = {1, 4, 7, 10, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    },
    .ivaMap[2] =
    {
        .EncNumCh  = 15,
        .EncChList = {2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44 },
        .DecNumCh  = 6,
        .DecChList = {2, 5, 8, 11, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    },
};
#define     NUM_NSF_LINK                           2
#define     NUM_MERGE_LINK                         5
#define     DEI_SC_CIF_MERGE_LINK_IDX              0

/**  DEI_VIP_D1_MERGE_LINK
*   DEI0 ---D1_CIF_MERGE_LINK_DEI0_VIP_SC_PRI_QIDX-->Q0--|
*                                                        |
*   DEI1 ---D1_CIF_MERGE_LINK_DEI1_VIP_SC_PRI_QIDX-->Q1--|
*                                                        |
*   CIF_DUP ---------------------------------------->Q2--|-DEI_VIP_D1_MERGE_LINK
*                                                        |
*   DEI0 ---D1_CIF_MERGE_LINK_DEI0_DEI_SC_SEC_QIDX-->Q3--|
*                                                        |
*   DEI1 ---D1_CIF_MERGE_LINK_DEI1_VIP_SC_PRI_QIDX-->Q4--|
*/
#define     D1_CIF_MERGE_LINK_IDX                    1
#define     D1_CIF_MERGE_LINK_NUM_IN_QUE             4
#define     D1_CIF_MERGE_LINK_DEI0_VIP_SC_PRI_QIDX   0
#define     D1_CIF_MERGE_LINK_DEI1_VIP_SC_PRI_QIDX   1
#define     D1_CIF_MERGE_LINK_CIF_DUP_QIDX           2
#define     D1_CIF_MERGE_LINK_DEI0_DEI_SC_SEC_QIDX   3
#define     D1_CIF_MERGE_LINK_DEI1_DEI_SC_SEC_QIDX   4



#define     LIVE_DECODE_MERGE_LINK_IDX             2
#define     DEI_VIP_D1_MERGE_LINK                  3

/**  DEI_PREVIEW_CIF_MERGE_LINK_IDX
*   DEI0 ---DEI_DEI_SC_PRI_Q-->Q0--|
*                                  |
*   DEI1 ---DEI_DEI_SC_PRI_Q-->Q1--|-DEI_VIP_D1_MERGE_LINK
*                                  |
*   CIF_DUP ------------------>Q1--|
*
*/
#define     DEI_PREVIEW_CIF_MERGE_LINK_IDX              3
#define     DEI_PREVIEW_CIF_MERGE_LINK_NUM_INQUE       (3)
#define     DEI_PREVIEW_CIF_MERGE_LINK_DEI0_D1_QIDX    (0)
#define     DEI_PREVIEW_CIF_MERGE_LINK_DEI1_D1_QIDX    (1)
#define     DEI_PREVIEW_CIF_MERGE_LINK_CIF_DUP_QIDX    (2)

#define     DEI_SC_MJPEG_MERGE_LINK_QIDX                  (4)

#define     NUM_DUP_LINK                           3
#define     LIVE_DECODE_DUP_LINK_IDX               0
#define     CIF_DUP_LINK_IDX                       1
#define     CAPTURE_DISPLAY_DUP_LINK_IDX           2

#define     NUM_DEI_CHAN                           8
#define     NUM_BUFS_PER_CH_DEI_VIP_SC_PRIMARY    (5)
#define     NUM_BUFS_PER_CH_DEI_VIP_SC_SECONDARY  (7)
#define     NUM_BUFS_PER_CH_DEI_DEI_SC_SECONDARY  (4)

#define     MULTICH_PROGRESSIVE_DVR_USECASE_MAX_NUM_LINKS (64)

#define     MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_LIVE_CHCOUNT_THRESHOLD     (12)
#define     MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_PLAYBACK_CHCOUNT_THRESHOLD (14)
#define     MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_LIVE_CHCOUNT_DISABLE       (16)
#define     MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_MAX_CHANNELS (MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_LIVE_CHCOUNT_DISABLE)

typedef struct MultichProgressiveDVR_Context
{
    UInt32 createdLinkCount;
    UInt32 createdLinks[MULTICH_PROGRESSIVE_DVR_USECASE_MAX_NUM_LINKS];
    MergeLink_InLinkChInfo                 mergeChMap[2];
    SwMsLink_LayoutPrm                     swmsLayoutPrm[VDIS_DEV_MAX];
    System_LinkInfo                        deiLinkInfo[2];
    VDIS_MOSAIC_S                          vdisMosaicPrms[VDIS_DEV_MAX];
    UInt32                                 invisbleDecChannelList[DEC_LINK_MAX_CH];
    UInt32                                 visbleDecChannelList[DEC_LINK_MAX_CH];
    UInt32                                 captureFps;
    UInt32                                 swmsSelectId;
    SelectLink_CreateParams                swmsSelectPrm;
    UInt32                                 cifSelectStartCh;
    UInt32                                 numChPerDei;
}  MultichProgressiveDVR_Context;

MultichProgressiveDVR_Context gProgressiveDVRUsecaseContext;

static Void multich_progressivedvr_register_created_link(MultichProgressiveDVR_Context *pContext,
                                                         UInt32 linkID)
{
    OSA_assert(pContext->createdLinkCount < OSA_ARRAYSIZE(pContext->createdLinks));
    pContext->createdLinks[pContext->createdLinkCount] = linkID;
    pContext->createdLinkCount++;
}

#define MULTICH_PROGRESSIVEDVR_CREATE_LINK(linkID,createPrm,createPrmSize)      \
    do                                                                          \
    {                                                                           \
        System_linkCreate(linkID,createPrm,createPrmSize);                      \
        multich_progressivedvr_register_created_link(&gProgressiveDVRUsecaseContext,  \
                                                     linkID);                   \
    } while (0)

#define     MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(linkID)   (((linkID) == SYSTEM_LINK_ID_DEI_HQ_0) ? 0 : 1)
#define     MULTICH_PROGRESSIVEDVR_GET_CAPTURE_FIELDS_PER_SEC() (gProgressiveDVRUsecaseContext.captureFps)
#define     MULTICH_PROGRESSIVEDVR_GET_CAPTURE_FRAMES_PER_SEC() (gProgressiveDVRUsecaseContext.captureFps/2)

#ifdef MAX
#undef MAX
#endif

#define MAX(a,b) ((a) > (b) ? (a) : (b))

static
Int32 MultiCh_progressiveDVRSetMosaicParams(VDIS_DEV vdDevId, VDIS_MOSAIC_S *psVdMosaicParam );
static
Void  multich_progressive_get_decode_visibility_channels_list(UInt32 *invisbleChannelList,
                                                           UInt32 *numInvisibleCh,UInt32 maxInvisibleCh,
                                                           UInt32 *visbleChannelList,
                                                           UInt32 *numVisibleCh,UInt32 maxVisibleCh);

static
Void mulich_progressive_set_avsync_vidque_prm(Avsync_SynchConfigParams *queCfg,
                                            Int chnum,
                                            UInt32 avsStartChNum,
                                            UInt32 avsEndChNum,
                                            VDIS_DEV vdDevId)
{
    queCfg->chNum = chnum;
    queCfg->audioPresent = FALSE;
    if ((queCfg->chNum >= avsStartChNum)
        &&
        (queCfg->chNum <= avsEndChNum)
        &&
        (gVsysModuleContext.vsysConfig.enableAVsync)
        &&
        (VDIS_DEV_SD != vdDevId))
    {
        queCfg->avsyncEnable = TRUE;
    }
    else
    {
        queCfg->avsyncEnable = FALSE;
    }
    queCfg->clkAdjustPolicy.refClkType = AVSYNC_REFCLKADJUST_NONE;
    queCfg->playTimerStartTimeout = 0;
    queCfg->playStartMode =     AVSYNC_PLAYBACK_START_MODE_WAITSYNCH;
    queCfg->ptsInitMode   = AVSYNC_PTS_INIT_MODE_AUTO;
}

static
Void mulich_progressive_set_avsync_prm(AvsyncLink_LinkSynchConfigParams *avsyncPrm,
                                     UInt32 swMsIdx,
                                     VDIS_DEV vdDevId)
{
    Int i;
    Int32 status;

    Vdis_getAvsyncConfig(vdDevId,avsyncPrm);
    avsyncPrm->displayLinkID        = Vdis_getDisplayId(vdDevId);
    avsyncPrm->videoSynchLinkID = gVdisModuleContext.swMsId[swMsIdx];
    avsyncPrm->numCh            = gVdecModuleContext.vdecConfig.numChn;
    avsyncPrm->syncMasterChnum =  AVSYNC_INVALID_CHNUM;
    for (i = 0; i < avsyncPrm->numCh;i++)
    {
        mulich_progressive_set_avsync_vidque_prm(&avsyncPrm->queCfg[i],
                                               i+16,
                                               16,
                                               (16 + gVdecModuleContext.vdecConfig.numChn - 1),
                                               vdDevId);
    }
    if (0 == swMsIdx)
    {
        Vdis_setAvsyncConfig(VDIS_DEV_HDMI,avsyncPrm);
    }
    else
    {
        Vdis_setAvsyncConfig(VDIS_DEV_DVO2,avsyncPrm);
    }

    status = Avsync_configSyncConfigInfo(avsyncPrm);
    OSA_assert(status == 0);
}

static
void multich_progressive_set_dec2disp_chmap(UInt32 ipcInVpssFromDecodeId,
                                            UInt32 swmsPreMergeId)
{
    MergeLink_InLinkChInfo inChInfo;

    MergeLink_InLinkChInfo_Init(&inChInfo);
    inChInfo.inLinkID = ipcInVpssFromDecodeId;
    System_linkControl(swmsPreMergeId,
                       MERGE_LINK_CMD_GET_INPUT_LINK_CHINFO,
                       &inChInfo,
                       sizeof(inChInfo),
                       TRUE);
    OSA_assert(inChInfo.numCh == gVdecModuleContext.vdecConfig.numChn);

    MultiCh_setDec2DispMap(VDIS_DEV_HDMI,gVdecModuleContext.vdecConfig.numChn,0,inChInfo.startChNum);
    MultiCh_setDec2DispMap(VDIS_DEV_DVO2,gVdecModuleContext.vdecConfig.numChn,0,inChInfo.startChNum);
}

static
void multich_progressive_set_dei2disp_chmap(UInt32 swmsPreMergeId)
{
    Int i;

    OSA_COMPILETIME_ASSERT(OSA_ARRAYSIZE(gProgressiveDVRUsecaseContext.mergeChMap) ==
                           OSA_ARRAYSIZE(gProgressiveDVRUsecaseContext.deiLinkInfo));
    for (i = 0; i < OSA_ARRAYSIZE(gProgressiveDVRUsecaseContext.mergeChMap);i++)
    {
        MergeLink_InLinkChInfo_Init(&gProgressiveDVRUsecaseContext.mergeChMap[i]);
        gProgressiveDVRUsecaseContext.mergeChMap[i].inLinkID = gVcapModuleContext.deiId[i];
        System_linkControl(swmsPreMergeId,
                           MERGE_LINK_CMD_GET_INPUT_LINK_CHINFO,
                           &gProgressiveDVRUsecaseContext.mergeChMap[i],
                           sizeof(gProgressiveDVRUsecaseContext.mergeChMap[i]),
                           TRUE);
        System_linkGetInfo(gVcapModuleContext.deiId[i],&gProgressiveDVRUsecaseContext.deiLinkInfo[i]);
    }
}

static
UInt32 multich_progressive_get_videodecoder_device_id()
{
    OSA_I2cHndl i2cHandle;
    Int32 status;
    UInt32 twl_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TW2968_DRV,0);
    UInt32 tvp_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TVP5158_DRV,0);
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;
    UInt32 chipId;
    UInt32 deviceId = ~0u;

    status = OSA_i2cOpen(&i2cHandle, I2C_DEFAULT_INST_ID);
    OSA_assert(status==0);

    numRegs = 0;
    regAddr[numRegs] = DEVICE_TW2968_REG_DEVICE_ID;
    regValue[numRegs] = 0;
    numRegs++;

    regAddr[numRegs] = DEVICE_TW2968_REG_REV_ID;
    regValue[numRegs] = 0;
    numRegs++;

    status = OSA_i2cRead8(&i2cHandle,twl_i2c_addr,regAddr,regValue,numRegs);
    if (status == 0)
    {
        chipId =  (((UInt32) (regValue[0]>>6)&0x3) << 5) | ( (regValue[1] >> 3) & 0x1F ) ;
        printf("\nTWL_CHIP_ID_READ:0x%x\n",chipId);
        if (chipId == DEVICE_TW2968_CHIP_ID)
        {
            deviceId = DEVICE_VID_DEC_TW2968_DRV;
        }
    }
    if (deviceId == ~0u)
    {
        numRegs = 0;
        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_MSB;
        regValue[numRegs] = 0;
        numRegs++;

        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_LSB;
        regValue[numRegs] = 0;
        numRegs++;

        status = OSA_i2cRead8(&i2cHandle,tvp_i2c_addr,regAddr,regValue,numRegs);
        if (status == 0)
        {
            chipId = ( ( UInt32 ) regValue[0] << 8 ) | regValue[1];
            printf("\nTVP_CHIP_ID_READ:0x%x\n",chipId);
            if (DEVICE_TVP5158_CHIP_ID == chipId)
            {
                deviceId = DEVICE_VID_DEC_TVP5158_DRV;
            }
        }
    }
    OSA_assert(deviceId != ~0u);
    status = OSA_i2cClose(&i2cHandle);
    OSA_assert(status==0);
    return deviceId;
}

static
Void multich_progressive_set_capture_fps(UInt32 *captureFps)
{
    Bool isPal = Vcap_isPalMode();

    if (isPal)
    {
        *captureFps = 50;
    }
    else
    {
        *captureFps = 60;
    }
}

static
Void multich_progressive_configure_cifdup_delay(UInt32 dupId,UInt32 delayOutQueId)
{
    DupLink_DelayParams delayPrms;
    Int32 status;

    delayPrms.outQueId = delayOutQueId;

    status =
    System_linkControl(dupId,
                       DUP_LINK_CMD_SET_DELAY_OUTPUT_QUE_PARAMS,
                       &delayPrms,
                       sizeof(delayPrms),
                       TRUE);
    OSA_assert(status == 0);
}

static
Void multich_progressive_set_preswms_select_chnl_prms(UInt32 swmsPreMergeId,
                                                      UInt32 cifDupLinkId,
                                                      SelectLink_CreateParams *swmsSelectPrm)
{
    System_LinkInfo        liveDecodeMergeInfo;
    MergeLink_InLinkChInfo inChInfo;
    UInt32 totalMergeChannels;
    UInt i;

    System_linkGetInfo(swmsPreMergeId,&liveDecodeMergeInfo);
    UTILS_assert(liveDecodeMergeInfo.numQue == 1);
    totalMergeChannels = liveDecodeMergeInfo.queInfo[0].numCh;

    MergeLink_InLinkChInfo_Init(&inChInfo);
    inChInfo.inLinkID = cifDupLinkId;
    System_linkControl(swmsPreMergeId,
                       MERGE_LINK_CMD_GET_INPUT_LINK_CHINFO,
                       &inChInfo,
                       sizeof(inChInfo),
                       TRUE);
    OSA_assert(inChInfo.numCh < totalMergeChannels);
    swmsSelectPrm->numOutQue = 1;
    swmsSelectPrm->outQueChInfo[0].numOutCh = totalMergeChannels - inChInfo.numCh;
    OSA_assert(inChInfo.startChNum == swmsSelectPrm->outQueChInfo[0].numOutCh);
    swmsSelectPrm->outQueChInfo[0].outQueId = 0;
    for (i = 0; i < swmsSelectPrm->outQueChInfo[0].numOutCh;i++)
    {
        swmsSelectPrm->outQueChInfo[0].inChNum[i] = i;
    }
    gProgressiveDVRUsecaseContext.cifSelectStartCh = inChInfo.startChNum;
    gProgressiveDVRUsecaseContext.numChPerDei = NUM_DEI_CHAN;
}

static
Void multich_progressive_disable_mjpeg_chnl()
{
    UInt i;

    /* Secondary Out <CIF> Params */
    for ( i = VENC_PRIMARY_CHANNELS * 2;
          i < (VENC_PRIMARY_CHANNELS * 3);
          i++)
    {
        Venc_disableChn(i);
    }
}

static
Void multich_progressive_disable_secondary_chnl()
{
    UInt i;

    /* Secondary Out <CIF> Params */
    for ( i = VENC_PRIMARY_CHANNELS;
          i < (VENC_PRIMARY_CHANNELS + gVencModuleContext.vencConfig.numSecondaryChn);
          i++)
    {
        Venc_disableChn(i);
    }
}


Void MultiCh_createProgressiveVcapVencVdecVdis()
{
    CaptureLink_CreateParams    capturePrm;
    NsfLink_CreateParams            nsfParam[NUM_NSF_LINK];
    SclrLink_CreateParams           sclrPrm;
    SelectLink_CreateParams selectPrm;
    DeiLink_CreateParams        deiPrm[MAX_DEI_LINK];
    MergeLink_CreateParams      mergePrm[NUM_MERGE_LINK];
    DupLink_CreateParams        dupPrm[NUM_DUP_LINK];
    static SwMsLink_CreateParams       swMsPrm[VDIS_DEV_MAX];
    DisplayLink_CreateParams    displayPrm[VDIS_DEV_MAX];
    IpcLink_CreateParams        ipcOutVpssPrm;
    IpcLink_CreateParams        ipcInVpssPrm;
    IpcLink_CreateParams        ipcOutVideoPrm;
    IpcLink_CreateParams        ipcInVideoPrm;
    EncLink_CreateParams        encPrm;
    DecLink_CreateParams        decPrm;
    IpcBitsOutLinkHLOS_CreateParams   ipcBitsOutHostPrm;
    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutVideoPrm;
    IpcBitsInLinkHLOS_CreateParams    ipcBitsInHostPrm[2];
    IpcBitsInLinkRTOS_CreateParams    ipcBitsInVideoPrm;
    CaptureLink_VipInstParams         *pCaptureInstPrm;
    CaptureLink_OutParams             *pCaptureOutPrm;
    IpcFramesInLinkRTOS_CreateParams  ipcFramesInDspPrm[2];
    IpcFramesOutLinkRTOS_CreateParams  ipcFramesOutVpssPrm[2];
    AlgLink_CreateParams                dspAlgPrm[2];

    System_LinkInfo                   bitsProducerLinkInfo;
    UInt32 mergeId[NUM_MERGE_LINK];
    UInt32 dupId[NUM_DUP_LINK];
    UInt32 nullId;
    UInt32 grpxId[VDIS_DEV_MAX];
    UInt32 ipcOutVpssId, ipcInVpssId;
    UInt32 ipcOutVideoId, ipcInVideoId;
    AvsyncLink_LinkSynchConfigParams   avsyncCfg[VDIS_DEV_MAX];

    VCAP_DEVICE_CREATE_PARAM_S         vidDecVideoModeArgs[MAX_NUM_CAPTURE_DEVICES];
    UInt32 vipInstId;

    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutDspPrm;
    UInt32 ipcBitsOutDSPId;


    UInt32 selectId;
    UInt32 i,j, chId;
    Bool   enableOsdAlgLink = gVsysModuleContext.vsysConfig.enableOsd;
    Bool   enableScdAlgLink = gVsysModuleContext.vsysConfig.enableScd;
    UInt32 deviceId;
    UInt32 numCaptureDevices;
    VCAP_VIDEO_SOURCE_STATUS_S vidSourceStatus;
    SystemTiler_BucketGeometry_t bucketRes;


    MULTICH_INIT_STRUCT(IpcLink_CreateParams,ipcOutVpssPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,ipcInVpssPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,ipcOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,ipcInVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsOutLinkHLOS_CreateParams,ipcBitsOutHostPrm);
    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams,ipcBitsOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,ipcBitsInHostPrm[0]);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,ipcBitsInHostPrm[1]);
    MULTICH_INIT_STRUCT(IpcBitsInLinkRTOS_CreateParams,ipcBitsInVideoPrm);
    MULTICH_INIT_STRUCT(IpcFramesInLinkRTOS_CreateParams,ipcFramesInDspPrm[0]);
    MULTICH_INIT_STRUCT(IpcFramesInLinkRTOS_CreateParams,ipcFramesInDspPrm[1]);
    MULTICH_INIT_STRUCT(IpcFramesOutLinkRTOS_CreateParams,ipcFramesOutVpssPrm[0]);
    MULTICH_INIT_STRUCT(IpcFramesOutLinkRTOS_CreateParams,ipcFramesOutVpssPrm[1]);
    MULTICH_INIT_STRUCT(DecLink_CreateParams, decPrm);
    MULTICH_INIT_STRUCT(EncLink_CreateParams, encPrm);
    MULTICH_INIT_STRUCT(AlgLink_CreateParams, dspAlgPrm[0]);
    MULTICH_INIT_STRUCT(AlgLink_CreateParams, dspAlgPrm[1]);
    for(i = 0; i< NUM_NSF_LINK; i++)
       MULTICH_INIT_STRUCT(NsfLink_CreateParams,nsfParam[i]);

    for (i = 0; i < VDIS_DEV_MAX;i++)
    {
        MULTICH_INIT_STRUCT(DisplayLink_CreateParams ,displayPrm[i]);
        MULTICH_INIT_STRUCT(SwMsLink_CreateParams ,swMsPrm[i]);
    }
    printf("\n********* Entered usecase 16CH Progressive <816x> Cap/Enc/Dec/Dis \n\n");
    gProgressiveDVRUsecaseContext.createdLinkCount = 0;

     MultiCh_detectBoard();

    System_linkControl(
        SYSTEM_LINK_ID_M3VPSS,
        SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES,
        NULL,
        0,
        TRUE
        );

    System_linkControl(
        SYSTEM_LINK_ID_M3VIDEO,
        SYSTEM_COMMON_CMD_SET_CH2IVAHD_MAP_TBL,
        &systemVid_encDecIvaChMapTbl,
        sizeof(SystemVideo_Ivahd2ChMap_Tbl),
        TRUE
    );

    bucketRes.minResolution = SYSTEM_TILER_RESOLUTION_CIF;
    bucketRes.setSingleBucketGeometry = TRUE;
    SystemTiler_setBucketGeometry(&bucketRes);

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_CAPTURE;
    selectId                        = SYSTEM_VPSS_LINK_ID_SELECT_0;
    gProgressiveDVRUsecaseContext.swmsSelectId = SYSTEM_VPSS_LINK_ID_SELECT_1;

    gVcapModuleContext.dspAlgId[0] = SYSTEM_LINK_ID_ALG_0;
    gVcapModuleContext.ipcFramesOutVpssId[0] = SYSTEM_VPSS_LINK_ID_IPC_FRAMES_OUT_0;
    gVcapModuleContext.ipcFramesInDspId[0] = SYSTEM_DSP_LINK_ID_IPC_FRAMES_IN_0;
    gVcapModuleContext.nsfId[0]     = SYSTEM_LINK_ID_NSF_0;
    gVcapModuleContext.nsfId[1]     = SYSTEM_LINK_ID_NSF_1;
    gVcapModuleContext.sclrId[0]       = SYSTEM_LINK_ID_SCLR_INST_0;
    gVcapModuleContext.ipcFramesOutVpssId[1] = SYSTEM_VPSS_LINK_ID_IPC_FRAMES_OUT_1;
    gVcapModuleContext.ipcFramesInDspId[1]   = SYSTEM_DSP_LINK_ID_IPC_FRAMES_IN_1;
    gVcapModuleContext.dspAlgId[1]           = SYSTEM_LINK_ID_ALG_1;
    ipcBitsOutDSPId                          = SYSTEM_DSP_LINK_ID_IPC_BITS_OUT_0;
    gVcapModuleContext.ipcBitsInHLOSId       = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;
    gVcapModuleContext.deiId[0]     = SYSTEM_LINK_ID_DEI_HQ_0;
    gVcapModuleContext.deiId[1]     = SYSTEM_LINK_ID_DEI_0;
    gVencModuleContext.encId        = SYSTEM_LINK_ID_VENC_0;
    gVdecModuleContext.decId        = SYSTEM_LINK_ID_VDEC_0;

    gVdisModuleContext.swMsId[0]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_0;
    gVdisModuleContext.swMsId[1]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_1;
    swMsPrm[0].numSwMsInst = 1;
    swMsPrm[1].numSwMsInst = 1;
    swMsPrm[0].swMsInstId[0] = SYSTEM_SW_MS_SC_INST_SC5;
    swMsPrm[1].swMsInstId[0] = SYSTEM_SW_MS_SC_INST_SC5;

    gVdisModuleContext.displayId[0] = SYSTEM_LINK_ID_DISPLAY_0; /* ON CHIP HDMI */
    gVdisModuleContext.displayId[1] = SYSTEM_LINK_ID_DISPLAY_1; /* OFF CHIP HDMI */
    gVdisModuleContext.displayId[2] = SYSTEM_LINK_ID_DISPLAY_2; /* OFF CHIP HDMI */

    mergeId[LIVE_DECODE_MERGE_LINK_IDX] = SYSTEM_VPSS_LINK_ID_MERGE_0;
    mergeId[DEI_SC_CIF_MERGE_LINK_IDX]      = SYSTEM_VPSS_LINK_ID_MERGE_1;
    mergeId[D1_CIF_MERGE_LINK_IDX]          = SYSTEM_VPSS_LINK_ID_MERGE_2;
    mergeId[DEI_PREVIEW_CIF_MERGE_LINK_IDX]      = SYSTEM_VPSS_LINK_ID_MERGE_3;
    mergeId[DEI_SC_MJPEG_MERGE_LINK_QIDX]           = SYSTEM_VPSS_LINK_ID_MERGE_4;

    nullId                              = SYSTEM_VPSS_LINK_ID_NULL_0;
    dupId[LIVE_DECODE_DUP_LINK_IDX]     = SYSTEM_VPSS_LINK_ID_DUP_0;
    dupId[CIF_DUP_LINK_IDX]             = SYSTEM_VPSS_LINK_ID_DUP_1;
    dupId[CAPTURE_DISPLAY_DUP_LINK_IDX] = SYSTEM_VPSS_LINK_ID_DUP_2;
    grpxId[0]                       = SYSTEM_LINK_ID_GRPX_0;
    grpxId[1]                       = SYSTEM_LINK_ID_GRPX_1;


    ipcOutVpssId = SYSTEM_VPSS_LINK_ID_IPC_OUT_M3_0;
    ipcInVideoId = SYSTEM_VIDEO_LINK_ID_IPC_IN_M3_0;
    ipcOutVideoId= SYSTEM_VIDEO_LINK_ID_IPC_OUT_M3_0;
    ipcInVpssId  = SYSTEM_VPSS_LINK_ID_IPC_IN_M3_0;

    gVencModuleContext.ipcBitsOutRTOSId  = SYSTEM_VIDEO_LINK_ID_IPC_BITS_OUT_0;
    gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_1;
    gVdecModuleContext.ipcBitsOutHLOSId  = SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0;
    gVdecModuleContext.ipcBitsInRTOSId  = SYSTEM_VIDEO_LINK_ID_IPC_BITS_IN_0;

    CaptureLink_CreateParams_Init(&capturePrm);
    capturePrm.outQueParams[0].nextLink   = dupId[CAPTURE_DISPLAY_DUP_LINK_IDX];

    dupPrm[CAPTURE_DISPLAY_DUP_LINK_IDX].notifyNextLink = TRUE;
    dupPrm[CAPTURE_DISPLAY_DUP_LINK_IDX].inQueParams.prevLinkId = gVcapModuleContext.captureId;
    dupPrm[CAPTURE_DISPLAY_DUP_LINK_IDX].inQueParams.prevLinkQueId = 0;


    dupPrm[CAPTURE_DISPLAY_DUP_LINK_IDX].numOutQue = 2;
    dupPrm[CAPTURE_DISPLAY_DUP_LINK_IDX].outQueParams[0].nextLink = selectId;
    dupPrm[CAPTURE_DISPLAY_DUP_LINK_IDX].outQueParams[1].nextLink = gVdisModuleContext.displayId[2];

    selectPrm.numOutQue = 2;

    selectPrm.inQueParams.prevLinkId = dupId[CAPTURE_DISPLAY_DUP_LINK_IDX];
    selectPrm.inQueParams.prevLinkQueId  = 0;

    selectPrm.outQueParams[0].nextLink = gVcapModuleContext.deiId[0];
    selectPrm.outQueParams[1].nextLink = gVcapModuleContext.deiId[1];

    selectPrm.outQueChInfo[0].outQueId = 0;
    selectPrm.outQueChInfo[0].numOutCh = NUM_DEI_CHAN;

    selectPrm.outQueChInfo[1].outQueId = 1;
    selectPrm.outQueChInfo[1].numOutCh = NUM_DEI_CHAN;

    for(chId=0; chId<NUM_DEI_CHAN; chId++)
    {
        selectPrm.outQueChInfo[0].inChNum[chId] = chId;
        selectPrm.outQueChInfo[1].inChNum[chId] = NUM_DEI_CHAN + chId;
    }

    capturePrm.numVipInst                 = 4;

    capturePrm.tilerEnable                = FALSE;
    capturePrm.numBufsPerCh               = 9;
    capturePrm.numExtraBufs               = 6;
    capturePrm.maxBlindAreasPerCh       = 4;

    for(vipInstId=0; vipInstId<capturePrm.numVipInst; vipInstId++)
    {
        pCaptureInstPrm                     = &capturePrm.vipInst[vipInstId];
        pCaptureInstPrm->vipInstId          = (SYSTEM_CAPTURE_INST_VIP0_PORTA+
                                              vipInstId)%SYSTEM_CAPTURE_INST_MAX;
        pCaptureInstPrm->videoDecoderId     = SYSTEM_DEVICE_VID_DEC_TVP5158_DRV;
        pCaptureInstPrm->inDataFormat       = SYSTEM_DF_YUV422P;
        pCaptureInstPrm->standard           = SYSTEM_STD_MUX_4CH_D1;
        pCaptureInstPrm->numOutput          = 1;

        pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];
        pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV422I_YUYV;
        pCaptureOutPrm->scEnable            = FALSE;
        pCaptureOutPrm->scOutWidth          = 0;
        pCaptureOutPrm->scOutHeight         = 0;
        pCaptureOutPrm->outQueId          = 0;
    }

    deviceId = multich_progressive_get_videodecoder_device_id();
    if (deviceId == DEVICE_VID_DEC_TW2968_DRV)
    {
        numCaptureDevices = 2;
    }
    else
    {
        numCaptureDevices = 4;
    }

    for(i = 0; i < numCaptureDevices; i++)
    {
        if(DEVICE_VID_DEC_TW2968_DRV == deviceId)
        {
            /* consider first device connected to VIP0 Port A and second device connected to VIP1 PortA */
            vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i*2;
            vidDecVideoModeArgs[i].deviceId         = DEVICE_VID_DEC_TW2968_DRV;
            vidDecVideoModeArgs[i].numChInDevice    = 8;
        }
        else
        {
            vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i;
            vidDecVideoModeArgs[i].deviceId         = DEVICE_VID_DEC_TVP5158_DRV;
            vidDecVideoModeArgs[i].numChInDevice    = 4;
        }
        vidDecVideoModeArgs[i].modeParams.videoIfMode        = DEVICE_CAPT_VIDEO_IF_MODE_8BIT;
        vidDecVideoModeArgs[i].modeParams.videoDataFormat    = SYSTEM_DF_YUV422P;
        vidDecVideoModeArgs[i].modeParams.standard           = SYSTEM_STD_MUX_4CH_D1;
        vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
        		DEVICE_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_PIXEL_MUX_EMBEDDED_SYNC;
        vidDecVideoModeArgs[i].modeParams.videoSystem        =
                                      DEVICE_VIDEO_DECODER_VIDEO_SYSTEM_AUTO_DETECT;
        vidDecVideoModeArgs[i].modeParams.videoCropEnable    = FALSE;
        vidDecVideoModeArgs[i].modeParams.videoAutoDetectTimeout = -1;
    }

    Vcap_configVideoDecoder(vidDecVideoModeArgs, numCaptureDevices);
    Vcap_getVideoSourceStatus(&vidSourceStatus);
    Vcap_setVideoSourceStatus(&vidSourceStatus);
    /**After Capture is created set capture fps */
    multich_progressive_set_capture_fps(&gProgressiveDVRUsecaseContext.captureFps);


    for(i=0; i<2; i++)
    {
        DeiLink_CreateParams_Init(&deiPrm[i]);
        deiPrm[i].inQueParams.prevLinkId                      = selectId;
        deiPrm[i].inQueParams.prevLinkQueId                     = i;

        /* Set Output Scaling at DEI based on ratio */
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].scaleMode = DEI_SCALE_MODE_RATIO;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.widthRatio.numerator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.widthRatio.denominator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.heightRatio.numerator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.heightRatio.denominator = 1;
        for (chId=1; chId < DEI_LINK_MAX_CH; chId++)
            deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][chId] = deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0];

        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0].scaleMode = DEI_SCALE_MODE_RATIO;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0].ratio.widthRatio.numerator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0].ratio.widthRatio.denominator = 2;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0].ratio.heightRatio.numerator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0].ratio.heightRatio.denominator = 2;
        for (chId=1; chId < DEI_LINK_MAX_CH; chId++)
            deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][chId] = deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0];
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].scaleMode = DEI_SCALE_MODE_RATIO;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.heightRatio.numerator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.heightRatio.denominator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.widthRatio.numerator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.widthRatio.denominator = 1;
        for (chId=1; chId < DEI_LINK_MAX_CH; chId++)
            deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId] = deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0];

        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT][0].scaleMode = DEI_SCALE_MODE_RATIO;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT][0].ratio.widthRatio.numerator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT][0].ratio.widthRatio.denominator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT][0].ratio.heightRatio.numerator = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT][0].ratio.heightRatio.denominator = 1;
        for (chId=1; chId < DEI_LINK_MAX_CH; chId++)
            deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT][chId] = deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT][0];

        deiPrm[i].enableOut[DEI_LINK_OUT_QUE_DEI_SC]                        = TRUE;
        deiPrm[i].outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink            = mergeId[DEI_PREVIEW_CIF_MERGE_LINK_IDX];
        mergePrm[DEI_PREVIEW_CIF_MERGE_LINK_IDX].numInQue = DEI_PREVIEW_CIF_MERGE_LINK_NUM_INQUE;
        mergePrm[DEI_PREVIEW_CIF_MERGE_LINK_IDX].inQueParams[DEI_PREVIEW_CIF_MERGE_LINK_DEI0_D1_QIDX + i].prevLinkId = gVcapModuleContext.deiId[i];
        mergePrm[DEI_PREVIEW_CIF_MERGE_LINK_IDX].inQueParams[DEI_PREVIEW_CIF_MERGE_LINK_DEI0_D1_QIDX + i].prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC;
        deiPrm[i].inputFrameRate[DEI_LINK_OUT_QUE_DEI_SC]                   = MULTICH_PROGRESSIVEDVR_GET_CAPTURE_FRAMES_PER_SEC();
        deiPrm[i].outputFrameRate[DEI_LINK_OUT_QUE_DEI_SC]                  = MULTICH_PROGRESSIVEDVR_GET_CAPTURE_FRAMES_PER_SEC();

        deiPrm[i].enableOut[DEI_LINK_OUT_QUE_VIP_SC]                        = TRUE;
        deiPrm[i].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink            = mergeId[D1_CIF_MERGE_LINK_IDX];
        mergePrm[D1_CIF_MERGE_LINK_IDX].numInQue                            = D1_CIF_MERGE_LINK_NUM_IN_QUE;
        mergePrm[D1_CIF_MERGE_LINK_IDX].inQueParams[D1_CIF_MERGE_LINK_DEI0_VIP_SC_PRI_QIDX +  i].prevLinkId     = gVcapModuleContext.deiId[i];
        mergePrm[D1_CIF_MERGE_LINK_IDX].inQueParams[D1_CIF_MERGE_LINK_DEI0_VIP_SC_PRI_QIDX +  i].prevLinkQueId  = DEI_LINK_OUT_QUE_VIP_SC;
        deiPrm[i].tilerEnable[DEI_LINK_OUT_QUE_VIP_SC] = TRUE;
        deiPrm[i].numBufsPerCh[DEI_LINK_OUT_QUE_VIP_SC] = NUM_BUFS_PER_CH_DEI_VIP_SC_PRIMARY;
        deiPrm[i].inputFrameRate[DEI_LINK_OUT_QUE_VIP_SC]                   = MULTICH_PROGRESSIVEDVR_GET_CAPTURE_FRAMES_PER_SEC();
        deiPrm[i].outputFrameRate[DEI_LINK_OUT_QUE_VIP_SC]                  = MULTICH_PROGRESSIVEDVR_GET_CAPTURE_FRAMES_PER_SEC();

        deiPrm[i].enableOut[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT]    = TRUE;
        deiPrm[i].outQueParams[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT].nextLink   = mergeId[DEI_SC_CIF_MERGE_LINK_IDX];
        mergePrm[DEI_SC_CIF_MERGE_LINK_IDX].numInQue                      = 2;
        mergePrm[DEI_SC_CIF_MERGE_LINK_IDX].inQueParams[i].prevLinkId     = gVcapModuleContext.deiId[i];
        mergePrm[DEI_SC_CIF_MERGE_LINK_IDX].inQueParams[i].prevLinkQueId  = DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT;
        deiPrm[i].tilerEnable[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT] = TRUE;
        deiPrm[i].numBufsPerCh[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT] = NUM_BUFS_PER_CH_DEI_VIP_SC_SECONDARY;
        deiPrm[i].inputFrameRate[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT]                   = MULTICH_PROGRESSIVEDVR_GET_CAPTURE_FRAMES_PER_SEC();
        deiPrm[i].outputFrameRate[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT]                  = MULTICH_PROGRESSIVEDVR_GET_CAPTURE_FRAMES_PER_SEC();


        deiPrm[i].enableOut[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT] = TRUE;
        deiPrm[i].outQueParams[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT].nextLink = mergeId[DEI_SC_MJPEG_MERGE_LINK_QIDX];

        mergePrm[DEI_SC_MJPEG_MERGE_LINK_QIDX].numInQue                      = 2;
        mergePrm[DEI_SC_MJPEG_MERGE_LINK_QIDX].inQueParams[i].prevLinkId     = gVcapModuleContext.deiId[i];
        mergePrm[DEI_SC_MJPEG_MERGE_LINK_QIDX].inQueParams[i].prevLinkQueId  = DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT;

        deiPrm[i].tilerEnable[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT] = FALSE;
        deiPrm[i].numBufsPerCh[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT] = NUM_BUFS_PER_CH_DEI_DEI_SC_SECONDARY;
        deiPrm[i].inputFrameRate[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT]  = MULTICH_PROGRESSIVEDVR_GET_CAPTURE_FRAMES_PER_SEC();
        deiPrm[i].outputFrameRate[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT] = 1;


        deiPrm[i].comprEnable                                   = FALSE;
        deiPrm[i].setVipScYuv422Format                          = FALSE;
    }
    mergePrm[DEI_SC_MJPEG_MERGE_LINK_QIDX].outQueParams.nextLink         = gVcapModuleContext.nsfId[1];
    mergePrm[DEI_SC_MJPEG_MERGE_LINK_QIDX].notifyNextLink                = TRUE;
    NsfLink_CreateParams_Init(&nsfParam[1]);
    nsfParam[1].bypassNsf       = TRUE;
    nsfParam[1].inputFrameRate  = 1;
    nsfParam[1].outputFrameRate = 1;
    nsfParam[1].tilerEnable     = FALSE;
    nsfParam[1].inQueParams.prevLinkId    = mergeId[DEI_SC_MJPEG_MERGE_LINK_QIDX];
    nsfParam[1].inQueParams.prevLinkQueId = 0;
    nsfParam[1].numOutQue                 = 1;
    nsfParam[1].outQueParams[0].nextLink  = mergeId[D1_CIF_MERGE_LINK_IDX];
    nsfParam[1].numBufsPerCh              = 3;

    mergePrm[D1_CIF_MERGE_LINK_IDX].inQueParams[D1_CIF_MERGE_LINK_DEI0_DEI_SC_SEC_QIDX].prevLinkId     = gVcapModuleContext.nsfId[1];
    mergePrm[D1_CIF_MERGE_LINK_IDX].inQueParams[D1_CIF_MERGE_LINK_DEI0_DEI_SC_SEC_QIDX].prevLinkQueId  = 0;


    mergePrm[DEI_SC_CIF_MERGE_LINK_IDX].outQueParams.nextLink         = dupId[CIF_DUP_LINK_IDX];
    mergePrm[DEI_SC_CIF_MERGE_LINK_IDX].notifyNextLink                = TRUE;
    dupPrm[CIF_DUP_LINK_IDX].inQueParams.prevLinkId = mergeId[DEI_SC_CIF_MERGE_LINK_IDX];
    dupPrm[CIF_DUP_LINK_IDX].inQueParams.prevLinkQueId = 0;

    dupPrm[CIF_DUP_LINK_IDX].numOutQue = 2;

    dupPrm[CIF_DUP_LINK_IDX].outQueParams[0].nextLink = mergeId[D1_CIF_MERGE_LINK_IDX];
    mergePrm[D1_CIF_MERGE_LINK_IDX].numInQue                            = D1_CIF_MERGE_LINK_NUM_IN_QUE;
    mergePrm[D1_CIF_MERGE_LINK_IDX].inQueParams[D1_CIF_MERGE_LINK_CIF_DUP_QIDX].prevLinkId     = dupId[CIF_DUP_LINK_IDX];;
    mergePrm[D1_CIF_MERGE_LINK_IDX].inQueParams[D1_CIF_MERGE_LINK_CIF_DUP_QIDX].prevLinkQueId  = 0;

    dupPrm[CIF_DUP_LINK_IDX].outQueParams[1].nextLink = mergeId[DEI_PREVIEW_CIF_MERGE_LINK_IDX];
    mergePrm[DEI_PREVIEW_CIF_MERGE_LINK_IDX].numInQue = DEI_PREVIEW_CIF_MERGE_LINK_NUM_INQUE;
    mergePrm[DEI_PREVIEW_CIF_MERGE_LINK_IDX].inQueParams[DEI_PREVIEW_CIF_MERGE_LINK_CIF_DUP_QIDX].prevLinkId = dupId[CIF_DUP_LINK_IDX];;
    mergePrm[DEI_PREVIEW_CIF_MERGE_LINK_IDX].inQueParams[DEI_PREVIEW_CIF_MERGE_LINK_CIF_DUP_QIDX].prevLinkQueId = 1;
    dupPrm[CIF_DUP_LINK_IDX].notifyNextLink = TRUE;


    dupPrm[CAPTURE_DISPLAY_DUP_LINK_IDX].numOutQue = 3;
    dupPrm[CAPTURE_DISPLAY_DUP_LINK_IDX].outQueParams[2].nextLink = gVcapModuleContext.sclrId[0];

    SclrLink_CreateParams_Init(&sclrPrm);
    sclrPrm.inQueParams.prevLinkId             = dupId[CAPTURE_DISPLAY_DUP_LINK_IDX];
    sclrPrm.inQueParams.prevLinkQueId          = 2;
    sclrPrm.outQueParams.nextLink              = gVcapModuleContext.nsfId[0];
    sclrPrm.tilerEnable                        = FALSE;
    sclrPrm.enableLineSkipSc                   = TRUE;//FALSE;
    sclrPrm.inputFrameRate                     = 30;
    sclrPrm.outputFrameRate                    = 2;

    sclrPrm.scaleMode                                    = DEI_SCALE_MODE_RATIO;
    sclrPrm.outScaleFactor.ratio.widthRatio.numerator    = 1;
    sclrPrm.outScaleFactor.ratio.widthRatio.denominator  = 2;
    sclrPrm.outScaleFactor.ratio.heightRatio.numerator   = 1;
    sclrPrm.outScaleFactor.ratio.heightRatio.denominator = 1;
    sclrPrm.numBufsPerCh                                 = 2;

    NsfLink_CreateParams_Init(&nsfParam[0]);
    nsfParam[0].bypassNsf       = TRUE;
    nsfParam[0].inputFrameRate  = 1;
    nsfParam[0].outputFrameRate = 1;
    nsfParam[0].tilerEnable     = FALSE;
    nsfParam[0].inQueParams.prevLinkId    = gVcapModuleContext.sclrId[0];
    nsfParam[0].inQueParams.prevLinkQueId = 0;
    nsfParam[0].numOutQue                 = 1;
    nsfParam[0].outQueParams[0].nextLink  = gVcapModuleContext.ipcFramesOutVpssId[1];
    nsfParam[0].numBufsPerCh              = 2;

    ipcFramesOutVpssPrm[1].baseCreateParams.inQueParams.prevLinkId    = gVcapModuleContext.nsfId[0];
    ipcFramesOutVpssPrm[1].baseCreateParams.inQueParams.prevLinkQueId = 0;

    ipcFramesOutVpssPrm[1].baseCreateParams.inputFrameRate  = 30;
    ipcFramesOutVpssPrm[1].baseCreateParams.outputFrameRate = 30;
    ipcFramesOutVpssPrm[1].baseCreateParams.notifyPrevLink = TRUE;

    ipcFramesOutVpssPrm[1].baseCreateParams.numOutQue = 1;
    ipcFramesOutVpssPrm[1].baseCreateParams.outQueParams[0].nextLink = gVcapModuleContext.ipcFramesInDspId[1];//SYSTEM_LINK_ID_INVALID;
    ipcFramesOutVpssPrm[1].baseCreateParams.processLink = SYSTEM_LINK_ID_INVALID;//gVcapModuleContext.ipcFramesInDspId[1];

    /* Disable notification to DSP (notifyNextLink = FALSE ) as random hangs inside Notify_sendEvent 
     * observed in some H/W setups
     * DSP IPC Frames In Link should run in periodic mode -> noNotifyMode = TRUE.
     * DSP doesnt notify previous link (VPSS) -> notifyPrevLink = FALSE;
     * VPSS Frames Out Link should run in periodic mode -> noNotifyMode = TRUE.  
     */
    ipcFramesOutVpssPrm[1].baseCreateParams.notifyNextLink = FALSE;
    ipcFramesOutVpssPrm[1].baseCreateParams.notifyProcessLink = FALSE;
    ipcFramesInDspPrm[1].baseCreateParams.noNotifyMode   = TRUE;
    ipcFramesInDspPrm[1].baseCreateParams.notifyPrevLink = FALSE;
    ipcFramesOutVpssPrm[1].baseCreateParams.noNotifyMode = TRUE;

    ipcFramesInDspPrm[1].baseCreateParams.inQueParams.prevLinkId = gVcapModuleContext.ipcFramesOutVpssId[1];
    ipcFramesInDspPrm[1].baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcFramesInDspPrm[1].baseCreateParams.numOutQue   = 1;
    ipcFramesInDspPrm[1].baseCreateParams.outQueParams[0].nextLink = gVcapModuleContext.dspAlgId[1];
    ipcFramesInDspPrm[1].baseCreateParams.notifyNextLink = TRUE;

    dspAlgPrm[1].inQueParams.prevLinkId = gVcapModuleContext.ipcFramesInDspId[1];
    dspAlgPrm[1].inQueParams.prevLinkQueId = 0;

    mergePrm[D1_CIF_MERGE_LINK_IDX].numInQue                        = D1_CIF_MERGE_LINK_NUM_IN_QUE;
    mergePrm[D1_CIF_MERGE_LINK_IDX].outQueParams.nextLink           = ipcOutVpssId;
    mergePrm[D1_CIF_MERGE_LINK_IDX].notifyNextLink                  = TRUE;
    mergePrm[D1_CIF_MERGE_LINK_IDX].outQueParams.nextLink           = gVcapModuleContext.ipcFramesOutVpssId[0];

    ipcFramesOutVpssPrm[0].baseCreateParams.inQueParams.prevLinkId = mergeId[D1_CIF_MERGE_LINK_IDX];
    ipcFramesOutVpssPrm[0].baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcFramesOutVpssPrm[0].baseCreateParams.notifyPrevLink = TRUE;

    ipcFramesOutVpssPrm[0].baseCreateParams.numOutQue = 1;
    ipcFramesOutVpssPrm[0].baseCreateParams.outQueParams[0].nextLink = ipcOutVpssId;
    ipcFramesOutVpssPrm[0].baseCreateParams.notifyNextLink = TRUE;

    ipcFramesOutVpssPrm[0].baseCreateParams.processLink = gVcapModuleContext.ipcFramesInDspId[0];

    /* Disable notification to DSP (notifyProcessLink = FALSE ) as random hangs inside Notify_sendEvent 
     * observed in some H/W setups
     * DSP IPC Frames In Link should run in periodic mode -> noNotifyMode = TRUE.
     * DSP doesnt notify previous link (VPSS) -> notifyPrevLink = FALSE;
     * VPSS Frames Out Link should run in periodic mode -> noNotifyMode = TRUE.  
     */
    ipcFramesOutVpssPrm[0].baseCreateParams.notifyProcessLink = FALSE;
    ipcFramesInDspPrm[0].baseCreateParams.noNotifyMode   = TRUE;
    ipcFramesInDspPrm[0].baseCreateParams.notifyPrevLink = FALSE;
    ipcFramesOutVpssPrm[0].baseCreateParams.noNotifyMode = TRUE;

    //prevLink->processLink->nextLink
    ipcFramesInDspPrm[0].baseCreateParams.inQueParams.prevLinkId = gVcapModuleContext.ipcFramesOutVpssId[0];
    ipcFramesInDspPrm[0].baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcFramesInDspPrm[0].baseCreateParams.numOutQue   = 1;
    ipcFramesInDspPrm[0].baseCreateParams.outQueParams[0].nextLink = gVcapModuleContext.dspAlgId[0];
    ipcFramesInDspPrm[0].baseCreateParams.notifyNextLink = TRUE;

    dspAlgPrm[0].inQueParams.prevLinkId = gVcapModuleContext.ipcFramesInDspId[0];
    dspAlgPrm[0].inQueParams.prevLinkQueId = 0;

    ipcOutVpssPrm.inQueParams.prevLinkId    = gVcapModuleContext.ipcFramesOutVpssId[0];
    ipcOutVpssPrm.inQueParams.prevLinkQueId = 0;
    ipcOutVpssPrm.outQueParams[0].nextLink     = ipcInVideoId;
    ipcOutVpssPrm.notifyNextLink            = FALSE;
    ipcOutVpssPrm.notifyPrevLink            = TRUE;
    ipcOutVpssPrm.noNotifyMode              = TRUE;

    ipcInVideoPrm.inQueParams.prevLinkId    = ipcOutVpssId;
    ipcInVideoPrm.inQueParams.prevLinkQueId = 0;
    ipcInVideoPrm.numOutQue                 = 1;
    ipcInVideoPrm.outQueParams[0].nextLink  = gVencModuleContext.encId;
    ipcInVideoPrm.notifyNextLink            = TRUE;
    ipcInVideoPrm.notifyPrevLink            = FALSE;
    ipcInVideoPrm.noNotifyMode              = TRUE;

    encPrm.numBufPerCh[0] = 6; //D1
    encPrm.numBufPerCh[1] = 6; //CIF
    /* available buffers per channel with CIF and MJPEG encoder support is less*/
    if (gVsysModuleContext.vsysConfig.enableMjpegEnc == TRUE)
    {
        encPrm.numBufPerCh[0] = 6;
        encPrm.numBufPerCh[1] = 6;
        encPrm.numBufPerCh[2] = 6;
        encPrm.numBufPerCh[3] = 6;
    }

    {
        EncLink_ChCreateParams *pLinkChPrm;
        EncLink_ChDynamicParams *pLinkDynPrm;
        VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
        VENC_CHN_PARAMS_S *pChPrm;

        /* Primary Stream Params - D1 */
        for (i=0; i<gVencModuleContext.vencConfig.numPrimaryChn; i++)
        {
            pLinkChPrm  = &encPrm.chCreateParams[i];
            pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

            pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
            pDynPrm     = &pChPrm->dynamicParam;

            pLinkChPrm->format                  = IVIDEO_H264HP;
            pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
            pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
            pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
            pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
            pLinkChPrm->enableWaterMarking      = pChPrm->enableWaterMarking;
            pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
            pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
            pLinkChPrm->rateControlPreset       = pChPrm->rcType;
            pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;
            pLinkChPrm->numTemporalLayer        = pChPrm->numTemporalLayer;

            pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
            pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
            pLinkDynPrm->interFrameInterval     = pDynPrm->interFrameInterval;
            pLinkChPrm->enableHighSpeed         = TRUE;
            pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
            pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
            pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
            pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
            pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
            pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
            pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
            pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
        }

        {
            /* Secondary Out <CIF> Params */
            for (i=gVencModuleContext.vencConfig.numPrimaryChn, j=VENC_PRIMARY_CHANNELS;
                  i<(gVencModuleContext.vencConfig.numPrimaryChn
                           + gVencModuleContext.vencConfig.numSecondaryChn);
                    i++, j++)
            {
                pLinkChPrm  = &encPrm.chCreateParams[i];
                pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

                pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[j];
                pDynPrm     = &pChPrm->dynamicParam;

                pLinkChPrm->format                  = IVIDEO_H264HP;
                pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
                pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
                pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
                pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
                pLinkChPrm->enableWaterMarking      = pChPrm->enableWaterMarking;
                pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
                pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
                pLinkChPrm->rateControlPreset       = pChPrm->rcType;
                pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;
                pLinkChPrm->numTemporalLayer        = pChPrm->numTemporalLayer;

                pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
                pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
                pLinkDynPrm->interFrameInterval     = pDynPrm->interFrameInterval;
                pLinkChPrm->enableHighSpeed         = TRUE;
                pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
                pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
                pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
                pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
                pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
                pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
                pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
            }
        }

        {
            for (i=gVencModuleContext.vencConfig.numPrimaryChn + gVencModuleContext.vencConfig.numSecondaryChn;
                      i<(VENC_CHN_MAX); i++)
            {
                pLinkChPrm  = &encPrm.chCreateParams[i];
                pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

                pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
                pDynPrm     = &pChPrm->dynamicParam;

                pLinkChPrm->format                 = IVIDEO_MJPEG;
                pLinkChPrm->profile                = 0;
                pLinkChPrm->dataLayout             = VCODEC_FIELD_SEPARATED;
                pLinkChPrm->fieldMergeEncodeEnable = FALSE;
                pLinkChPrm->enableAnalyticinfo     = 0;
                pLinkChPrm->enableWaterMarking     = 0;
                pLinkChPrm->maxBitRate             = 0;
                pLinkChPrm->encodingPreset         = 0;
                pLinkChPrm->rateControlPreset      = 0;
                pLinkChPrm->enableSVCExtensionFlag = 0;
                pLinkChPrm->numTemporalLayer       = 0;

                pLinkDynPrm->intraFrameInterval    = 0;
                pLinkDynPrm->targetBitRate         = 100*1000;
                pLinkDynPrm->interFrameInterval    = 0;
                pLinkDynPrm->mvAccuracy            = 0;
                pLinkDynPrm->inputFrameRate        = pDynPrm->inputFrameRate;
                pLinkDynPrm->qpMin                 = 0;
                pLinkDynPrm->qpMax                 = 0;
                pLinkDynPrm->qpInit                = -1;
                pLinkDynPrm->vbrDuration           = 0;
                pLinkDynPrm->vbrSensitivity        = 0;
            }
        }

        encPrm.inQueParams.prevLinkId    = ipcInVideoId;
        encPrm.inQueParams.prevLinkQueId = 0;
        encPrm.outQueParams.nextLink     = gVencModuleContext.ipcBitsOutRTOSId;
    }


    ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkId = gVencModuleContext.encId;
    ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsOutVideoPrm.baseCreateParams.numOutQue                 = 1;
    ipcBitsOutVideoPrm.baseCreateParams.outQueParams[0].nextLink = gVencModuleContext.ipcBitsInHLOSId;
    MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&ipcBitsOutVideoPrm,
                                               TRUE);

    ipcBitsInHostPrm[0].baseCreateParams.inQueParams.prevLinkId = gVencModuleContext.ipcBitsOutRTOSId;
    ipcBitsInHostPrm[0].baseCreateParams.inQueParams.prevLinkQueId = 0;
    MultiCh_ipcBitsInitCreateParams_BitsInHLOS(&ipcBitsInHostPrm[0]);

    dspAlgPrm[0].enableOSDAlg = enableOsdAlgLink;
    dspAlgPrm[0].enableSCDAlg = FALSE;
    dspAlgPrm[0].outQueParams[ALG_LINK_SCD_OUT_QUE].nextLink = SYSTEM_LINK_ID_INVALID;


    for(chId = 0; chId < ALG_LINK_OSD_MAX_CH; chId++)
    {
        AlgLink_OsdChWinParams * chWinPrm = &dspAlgPrm[0].osdChCreateParams[chId].chDefaultParams;

        /* set osd window max width and height */
        dspAlgPrm[0].osdChCreateParams[chId].maxWidth  = EXAMPLE_OSD_WIN_MAX_WIDTH;
        dspAlgPrm[0].osdChCreateParams[chId].maxHeight = EXAMPLE_OSD_WIN_MAX_HEIGHT;

        chWinPrm->chId = chId;
        chWinPrm->numWindows = 0;
    }

    {
        Int32   numBlksInFrame;
        Int32   numHorzBlks, numVertBlks, chIdx;
        Uint32  x, y, i;

        dspAlgPrm[1].enableOSDAlg = FALSE;
        dspAlgPrm[1].enableSCDAlg = enableScdAlgLink;
        dspAlgPrm[1].outQueParams[ALG_LINK_SCD_OUT_QUE].nextLink     = ipcBitsOutDSPId;

        dspAlgPrm[1].scdCreateParams.maxWidth               = 352;
        if(Vcap_isPalMode())
           dspAlgPrm[1].scdCreateParams.maxHeight           = 288;
        else
           dspAlgPrm[1].scdCreateParams.maxHeight           = 240;
        dspAlgPrm[1].scdCreateParams.maxStride              = 352;
        dspAlgPrm[1].scdCreateParams.numValidChForSCD       = 16;
        dspAlgPrm[1].scdCreateParams.maxNumVaChan           = 1;
        dspAlgPrm[1].scdCreateParams.numSecs2WaitB4Init     = 3;
        dspAlgPrm[1].scdCreateParams.numSecs2WaitB4FrmAlert = 1;
        dspAlgPrm[1].scdCreateParams.inputFrameRate         = 2;
        dspAlgPrm[1].scdCreateParams.outputFrameRate        = 2;
        dspAlgPrm[1].scdCreateParams.numSecs2WaitAfterFrmAlert = 2;
        dspAlgPrm[1].scdCreateParams.numBufPerCh               = 2;

        dspAlgPrm[1].scdCreateParams.enableMotionNotify    = FALSE;
        dspAlgPrm[1].scdCreateParams.enableTamperNotify    = FALSE;
        dspAlgPrm[1].scdCreateParams.enableVaEvntNotify    = FALSE;
       // Configure array to monitor scene changes in all frame blocks, i.e., motion detection.
       // Each block is fixed to be 32x10 in size when height is 240,
       // Each block is fixed to be 32x11 in size when height is 288
        numHorzBlks    = dspAlgPrm[1].scdCreateParams.maxWidth / 32;
        if((dspAlgPrm[1].scdCreateParams.maxHeight%10) == 0)
           numVertBlks    = dspAlgPrm[1].scdCreateParams.maxHeight / 10;
        else   /* For 288 Block height becomes 12 */
           numVertBlks    = dspAlgPrm[1].scdCreateParams.maxHeight / 12;

        numBlksInFrame = numHorzBlks * numVertBlks;

        for(chIdx = 0; chIdx < dspAlgPrm[1].scdCreateParams.numValidChForSCD; chIdx++)
        {
           AlgLink_ScdChParams * chPrm = &dspAlgPrm[1].scdCreateParams.chDefaultParams[chIdx];

           chPrm->blkNumBlksInFrame = numBlksInFrame;
           chPrm->chId               = SCDChannelMonitor[chIdx];
           chPrm->mode               = ALG_LINK_SCD_DETECTMODE_MONITOR_BLOCKS_AND_FRAME;
           if(chIdx < dspAlgPrm[1].scdCreateParams.maxNumVaChan)
           {
               chPrm->inputFrameRate     = 10;
               chPrm->outputFrameRate    = 10;
               chPrm->mode               = ALG_LINK_SCD_DETECTMODE_MONITOR_BLOCKS_AND_FRAME | ALG_LINK_SCD_DETECTMODE_TRIPZONE;
           }
           else
           {
               chPrm->inputFrameRate     = 2;
               chPrm->outputFrameRate    = 2;
           }
           chPrm->frmIgnoreLightsON  = FALSE;
           chPrm->frmIgnoreLightsOFF = FALSE;
           chPrm->frmSensitivity     = ALG_LINK_SCD_SENSITIVITY_HIGH;//ALG_LINK_SCD_SENSITIVITY_MID;
           chPrm->frmEdgeThreshold   = 100;
           i = 0;
           for(y = 0; y < numVertBlks; y++)
           {
             for(x = 0; x < numHorzBlks; x++)
             {
               chPrm->blkConfig[i].sensitivity = ALG_LINK_SCD_SENSITIVITY_LOW;
               chPrm->blkConfig[i].monitored     = 0;
               i++;
             }
           }
        }

        ipcBitsOutDspPrm.baseCreateParams.inQueParams.prevLinkId    = gVcapModuleContext.dspAlgId[1];
        ipcBitsOutDspPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
        ipcBitsOutDspPrm.baseCreateParams.numOutQue                 = 1;
        ipcBitsOutDspPrm.baseCreateParams.outQueParams[0].nextLink  = gVcapModuleContext.ipcBitsInHLOSId;
        MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&ipcBitsOutDspPrm,
                                                   TRUE);
        ipcBitsOutDspPrm.baseCreateParams.notifyNextLink              = TRUE;
        ipcBitsOutDspPrm.baseCreateParams.noNotifyMode                = FALSE;

        ipcBitsInHostPrm[1].baseCreateParams.inQueParams.prevLinkId = ipcBitsOutDSPId;
        ipcBitsInHostPrm[1].baseCreateParams.inQueParams.prevLinkQueId = 0;
        ipcBitsInHostPrm[1].baseCreateParams.numOutQue                 = 1;
        ipcBitsInHostPrm[1].baseCreateParams.outQueParams[0].nextLink   = SYSTEM_LINK_ID_INVALID;
        MultiCh_ipcBitsInitCreateParams_BitsInHLOSVcap(&ipcBitsInHostPrm[1]);
        ipcBitsInHostPrm[1].baseCreateParams.notifyPrevLink         = TRUE;
        ipcBitsInHostPrm[1].baseCreateParams.noNotifyMode              = FALSE;

    }



    capturePrm.isPalMode = Vcap_isPalMode();
    MULTICH_PROGRESSIVEDVR_CREATE_LINK (gVcapModuleContext.captureId, &capturePrm, sizeof(capturePrm));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK (dupId[CAPTURE_DISPLAY_DUP_LINK_IDX], &dupPrm[CAPTURE_DISPLAY_DUP_LINK_IDX], sizeof(dupPrm[CAPTURE_DISPLAY_DUP_LINK_IDX]));

    MULTICH_PROGRESSIVEDVR_CREATE_LINK (selectId, &selectPrm, sizeof(selectPrm));


    for(i=0; i<2; i++)
        MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.deiId[i]  , &deiPrm[i], sizeof(deiPrm[i]));

    MULTICH_PROGRESSIVEDVR_CREATE_LINK(mergeId[DEI_SC_CIF_MERGE_LINK_IDX], &mergePrm[DEI_SC_CIF_MERGE_LINK_IDX], sizeof(mergePrm[DEI_SC_CIF_MERGE_LINK_IDX]));

    MULTICH_PROGRESSIVEDVR_CREATE_LINK(dupId[CIF_DUP_LINK_IDX], &dupPrm[CIF_DUP_LINK_IDX], sizeof(dupPrm[CIF_DUP_LINK_IDX]));
    multich_progressive_configure_cifdup_delay(dupId[CIF_DUP_LINK_IDX],0);

    MULTICH_PROGRESSIVEDVR_CREATE_LINK(mergeId[DEI_SC_MJPEG_MERGE_LINK_QIDX], &mergePrm[DEI_SC_MJPEG_MERGE_LINK_QIDX], sizeof(mergePrm[DEI_SC_MJPEG_MERGE_LINK_QIDX]));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.nsfId[1], &nsfParam[1], sizeof(nsfParam[1]));

    MULTICH_PROGRESSIVEDVR_CREATE_LINK(mergeId[D1_CIF_MERGE_LINK_IDX], &mergePrm[D1_CIF_MERGE_LINK_IDX], sizeof(mergePrm[D1_CIF_MERGE_LINK_IDX]));
    mergePrm[DEI_PREVIEW_CIF_MERGE_LINK_IDX].notifyNextLink = TRUE;
    mergePrm[DEI_PREVIEW_CIF_MERGE_LINK_IDX].outQueParams.nextLink = gProgressiveDVRUsecaseContext.swmsSelectId;
    gProgressiveDVRUsecaseContext.swmsSelectPrm.inQueParams.prevLinkId = mergeId[DEI_PREVIEW_CIF_MERGE_LINK_IDX];
    gProgressiveDVRUsecaseContext.swmsSelectPrm.inQueParams.prevLinkQueId = 0;
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(mergeId[DEI_PREVIEW_CIF_MERGE_LINK_IDX], &mergePrm[DEI_PREVIEW_CIF_MERGE_LINK_IDX], sizeof(mergePrm[DEI_PREVIEW_CIF_MERGE_LINK_IDX]));

    multich_progressive_set_preswms_select_chnl_prms(mergeId[DEI_PREVIEW_CIF_MERGE_LINK_IDX],dupId[CIF_DUP_LINK_IDX],&gProgressiveDVRUsecaseContext.swmsSelectPrm);
    gProgressiveDVRUsecaseContext.swmsSelectPrm.outQueParams[0].nextLink = mergeId[LIVE_DECODE_MERGE_LINK_IDX];
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gProgressiveDVRUsecaseContext.swmsSelectId,&gProgressiveDVRUsecaseContext.swmsSelectPrm,sizeof(gProgressiveDVRUsecaseContext.swmsSelectPrm));

    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.ipcFramesOutVpssId[0], &ipcFramesOutVpssPrm[0], sizeof(ipcFramesOutVpssPrm[0]));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.ipcFramesInDspId[0], &ipcFramesInDspPrm[0], sizeof(ipcFramesInDspPrm[0]));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.dspAlgId[0] , &dspAlgPrm[0], sizeof(dspAlgPrm[0]));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.sclrId[0], &sclrPrm, sizeof(sclrPrm));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.nsfId[0], &nsfParam[0], sizeof(nsfParam[0]));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.ipcFramesOutVpssId[1], &ipcFramesOutVpssPrm[1], sizeof(ipcFramesOutVpssPrm[1]));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.ipcFramesInDspId[1], &ipcFramesInDspPrm[1], sizeof(ipcFramesInDspPrm[1]));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.dspAlgId[1] , &dspAlgPrm[1], sizeof(dspAlgPrm[1]));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(ipcBitsOutDSPId, &ipcBitsOutDspPrm, sizeof(ipcBitsOutDspPrm));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVcapModuleContext.ipcBitsInHLOSId, &ipcBitsInHostPrm[1], sizeof(ipcBitsInHostPrm[1]));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(ipcOutVpssId , &ipcOutVpssPrm , sizeof(ipcOutVpssPrm) );
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(ipcInVideoId , &ipcInVideoPrm , sizeof(ipcInVideoPrm) );

    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVencModuleContext.encId, &encPrm, sizeof(encPrm));

    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVencModuleContext.ipcBitsOutRTOSId, &ipcBitsOutVideoPrm, sizeof(ipcBitsOutVideoPrm));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVencModuleContext.ipcBitsInHLOSId, &ipcBitsInHostPrm[0], sizeof(ipcBitsInHostPrm[0]));

    System_linkGetInfo(gVencModuleContext.ipcBitsInHLOSId,&bitsProducerLinkInfo);
    OSA_assert(bitsProducerLinkInfo.numQue = 1);
    ipcBitsOutHostPrm.baseCreateParams.outQueParams[0].nextLink = gVdecModuleContext.ipcBitsInRTOSId;

    if (bitsProducerLinkInfo.queInfo[0].numCh > gVencModuleContext.vencConfig.numPrimaryChn)
        bitsProducerLinkInfo.queInfo[0].numCh = gVencModuleContext.vencConfig.numPrimaryChn;

    printf ("Reducing bitsProducerLinkInfo.numCh to %d\n", bitsProducerLinkInfo.queInfo[0].numCh);

    MultiCh_ipcBitsInitCreateParams_BitsOutHLOS(&ipcBitsOutHostPrm,
                                               &bitsProducerLinkInfo.queInfo[0]);

    if(gVdecModuleContext.vdecConfig.forceUseDecChannelParams)
    {
        /* use channel info provided by user instead of from encoder */
        System_LinkChInfo *pChInfo;

        ipcBitsOutHostPrm.inQueInfo.numCh = gVdecModuleContext.vdecConfig.numChn;

        for(chId=0; chId<ipcBitsOutHostPrm.inQueInfo.numCh; chId++)
        {
            pChInfo = &ipcBitsOutHostPrm.inQueInfo.chInfo[chId];

            pChInfo->bufType        = 0; // NOT USED
            pChInfo->codingformat   = 0; // NOT USED
            pChInfo->dataFormat     = 0; // NOT USED
            pChInfo->memType        = 0; // NOT USED
            pChInfo->startX         = 0; // NOT USED
            pChInfo->startY         = 0; // NOT USED
            pChInfo->width          = gVdecModuleContext.vdecConfig.decChannelParams[chId].maxVideoWidth;
            pChInfo->height         = gVdecModuleContext.vdecConfig.decChannelParams[chId].maxVideoHeight;
            pChInfo->pitch[0]       = 0; // NOT USED
            pChInfo->pitch[1]       = 0; // NOT USED
            pChInfo->pitch[2]       = 0; // NOT USED
            pChInfo->scanFormat     = SYSTEM_SF_PROGRESSIVE;
        }
    }

    /* ipcBitsOut params - num of buffers, bitstream buffer size */
    for (chId=0; chId<ipcBitsOutHostPrm.inQueInfo.numCh; chId++)
    {
        ipcBitsOutHostPrm.maxQueueDepth[chId] = MAX_BUFFERING_QUEUE_LEN_PER_CH;
        ipcBitsOutHostPrm.chMaxReqBufSize[chId] = IPC_BITBUF_SIZE(
                                        ipcBitsOutHostPrm.inQueInfo.chInfo[chId].width, 
                                        ipcBitsOutHostPrm.inQueInfo.chInfo[chId].height
                                         );
        ipcBitsOutHostPrm.totalBitStreamBufferSize [chId] = 
                (ipcBitsOutHostPrm.chMaxReqBufSize[chId] * BIT_BUF_LENGTH_LIMIT_FACTOR_SD);
    }

    ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkId       = gVdecModuleContext.ipcBitsOutHLOSId;
    ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkQueId    = 0;
    ipcBitsInVideoPrm.baseCreateParams.numOutQue                    = 1;
    ipcBitsInVideoPrm.baseCreateParams.outQueParams[0].nextLink     = gVdecModuleContext.decId;
    MultiCh_ipcBitsInitCreateParams_BitsInRTOS(&ipcBitsInVideoPrm, TRUE);


    for (i=0; i<gVdecModuleContext.vdecConfig.numChn; i++) {
        decPrm.chCreateParams[i].format                 = IVIDEO_H264HP;
        decPrm.chCreateParams[i].profile                = IH264VDEC_PROFILE_ANY;
        decPrm.chCreateParams[i].processCallLevel       = VDEC_FRAMELEVELPROCESSCALL;
        decPrm.chCreateParams[i].targetMaxWidth         = ipcBitsOutHostPrm.inQueInfo.chInfo[i].width;
        decPrm.chCreateParams[i].targetMaxHeight        = ipcBitsOutHostPrm.inQueInfo.chInfo[i].height;
        decPrm.chCreateParams[i].defaultDynamicParams.targetFrameRate = gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.frameRate;
        decPrm.chCreateParams[i].defaultDynamicParams.targetBitRate   = gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.targetBitRate;
        decPrm.chCreateParams[i].tilerEnable = TRUE;
        decPrm.chCreateParams[i].enableWaterMarking = gVdecModuleContext.vdecConfig.decChannelParams[i].enableWaterMarking;
    }
    decPrm.inQueParams.prevLinkId    = gVdecModuleContext.ipcBitsInRTOSId;
    decPrm.inQueParams.prevLinkQueId = 0;
    decPrm.outQueParams.nextLink  = ipcOutVideoId;

    ipcOutVideoPrm.inQueParams.prevLinkId    = gVdecModuleContext.decId;
    ipcOutVideoPrm.inQueParams.prevLinkQueId = 0;
    ipcOutVideoPrm.numOutQue                 = 1;
    ipcOutVideoPrm.outQueParams[0].nextLink  = ipcInVpssId;
    ipcOutVideoPrm.notifyNextLink            = FALSE;
    ipcOutVideoPrm.notifyPrevLink            = TRUE;
    ipcOutVideoPrm.noNotifyMode              = TRUE;

    ipcInVpssPrm.inQueParams.prevLinkId    = ipcOutVideoId;
    ipcInVpssPrm.inQueParams.prevLinkQueId = 0;
    ipcInVpssPrm.numOutQue                 = 1;
    ipcInVpssPrm.outQueParams[0].nextLink  = mergeId[LIVE_DECODE_MERGE_LINK_IDX];
    ipcInVpssPrm.notifyNextLink            = TRUE;
    ipcInVpssPrm.notifyPrevLink            = FALSE;
    ipcInVpssPrm.noNotifyMode              = TRUE;

    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].numInQue                     = 2;
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[0].prevLinkId    = gProgressiveDVRUsecaseContext.swmsSelectId;
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[0].prevLinkQueId = 0;
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[1].prevLinkId    = ipcInVpssId;
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[1].prevLinkQueId = 0;

    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].outQueParams.nextLink        = dupId[LIVE_DECODE_DUP_LINK_IDX];
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].notifyNextLink               = TRUE;
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVdecModuleContext.ipcBitsOutHLOSId,&ipcBitsOutHostPrm,sizeof(ipcBitsOutHostPrm));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVdecModuleContext.ipcBitsInRTOSId,&ipcBitsInVideoPrm,sizeof(ipcBitsInVideoPrm));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVdecModuleContext.decId, &decPrm, sizeof(decPrm));

    MULTICH_PROGRESSIVEDVR_CREATE_LINK(ipcOutVideoId, &ipcOutVideoPrm, sizeof(ipcOutVideoPrm));
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(ipcInVpssId  , &ipcInVpssPrm  , sizeof(ipcInVpssPrm)  );
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(mergeId[LIVE_DECODE_MERGE_LINK_IDX], &mergePrm[LIVE_DECODE_MERGE_LINK_IDX], sizeof(mergePrm[LIVE_DECODE_MERGE_LINK_IDX]));

    dupPrm[LIVE_DECODE_DUP_LINK_IDX].inQueParams.prevLinkId         = mergeId[LIVE_DECODE_MERGE_LINK_IDX];
    dupPrm[LIVE_DECODE_DUP_LINK_IDX].inQueParams.prevLinkQueId      = 0;
    dupPrm[LIVE_DECODE_DUP_LINK_IDX].numOutQue                      = 2;
    dupPrm[LIVE_DECODE_DUP_LINK_IDX].outQueParams[0].nextLink       = gVdisModuleContext.swMsId[0];
    dupPrm[LIVE_DECODE_DUP_LINK_IDX].outQueParams[1].nextLink       = gVdisModuleContext.swMsId[1];
    dupPrm[LIVE_DECODE_DUP_LINK_IDX].notifyNextLink                 = TRUE;

    for(i=0; i<2; i++)
    {
        VDIS_DEV vdDevId = VDIS_DEV_HDMI;

        swMsPrm[i].inQueParams.prevLinkId    = dupId[LIVE_DECODE_DUP_LINK_IDX];
        swMsPrm[i].inQueParams.prevLinkQueId = i;
        swMsPrm[i].outQueParams.nextLink     = gVdisModuleContext.displayId[i];
        swMsPrm[i].maxInputQueLen            = SYSTEM_SW_MS_DEFAULT_INPUT_QUE_LEN + 4;
        swMsPrm[i].maxOutRes                 = VSYS_STD_1080P_60;
        swMsPrm[i].initOutRes                = gVdisModuleContext.vdisConfig.deviceParams[i].resolution;
        swMsPrm[i].lineSkipMode = FALSE;
        swMsPrm[i].enableLayoutGridDraw = gVdisModuleContext.vdisConfig.enableLayoutGridDraw;
        swMsPrm[i].numOutBuf = 4;

        MultiCh_swMsGetDefaultLayoutPrm(i, &swMsPrm[i], FALSE);
        if(i == 0)
        {
           vdDevId = VDIS_DEV_HDMI;
           swMsPrm[i].enableOuputDup = TRUE;
           swMsPrm[i].enableProcessTieWithDisplay = TRUE;
        }
        else if(i == 1)
        {
           vdDevId = VDIS_DEV_DVO2;
           swMsPrm[i].enableOuputDup = TRUE;
           swMsPrm[i].enableProcessTieWithDisplay = TRUE;
        }
        mulich_progressive_set_avsync_prm(&avsyncCfg[i],i,vdDevId);

        displayPrm[i].inQueParams[0].prevLinkId    = gVdisModuleContext.swMsId[i];
        displayPrm[i].inQueParams[0].prevLinkQueId = 0;
        displayPrm[i].displayRes                   = swMsPrm[i].initOutRes;
    }

    displayPrm[2].numInputQueues = 1;
    displayPrm[2].activeQueue    = 0;
    displayPrm[2].displayRes     = gVdisModuleContext.vdisConfig.deviceParams[VDIS_DEV_SD].resolution;

    displayPrm[2].inQueParams[0].prevLinkId = dupId[CAPTURE_DISPLAY_DUP_LINK_IDX];
    displayPrm[2].inQueParams[0].prevLinkQueId = 1;
    MULTICH_PROGRESSIVEDVR_CREATE_LINK(dupId[LIVE_DECODE_DUP_LINK_IDX], &dupPrm[LIVE_DECODE_DUP_LINK_IDX], sizeof(dupPrm[LIVE_DECODE_DUP_LINK_IDX]));

    for(i=0; i<2; i++)
        MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVdisModuleContext.swMsId[i]  , &swMsPrm[i], sizeof(swMsPrm[i]));

    for(i=0; i<2; i++)
        MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVdisModuleContext.displayId[i], &displayPrm[i], sizeof(displayPrm[i]));

    MULTICH_PROGRESSIVEDVR_CREATE_LINK(gVdisModuleContext.displayId[2], &displayPrm[2], sizeof(displayPrm[2]));
    multich_progressive_set_dec2disp_chmap(ipcInVpssId,mergeId[LIVE_DECODE_MERGE_LINK_IDX]);
    multich_progressive_set_dei2disp_chmap(mergeId[DEI_PREVIEW_CIF_MERGE_LINK_IDX]);
    gVdisModuleContext.setMosaicFxn     = MultiCh_progressiveDVRSetMosaicParams;
    Vdis_getMosaicParams(VDIS_DEV_HDMI,&gProgressiveDVRUsecaseContext.vdisMosaicPrms[VDIS_DEV_HDMI]);
    Vdis_getMosaicParams(VDIS_DEV_DVO2,&gProgressiveDVRUsecaseContext.vdisMosaicPrms[VDIS_DEV_DVO2]);
    Vdis_setMosaicParams(VDIS_DEV_HDMI,&gProgressiveDVRUsecaseContext.vdisMosaicPrms[VDIS_DEV_HDMI]);
    Vdis_setMosaicParams(VDIS_DEV_DVO2,&gProgressiveDVRUsecaseContext.vdisMosaicPrms[VDIS_DEV_DVO2]);

    if (!gVsysModuleContext.vsysConfig.enableSecondaryOut)
    {
        multich_progressive_disable_secondary_chnl();
    }

    if (!gVsysModuleContext.vsysConfig.enableMjpegEnc)
    {
        multich_progressive_disable_mjpeg_chnl();
    }

}

Void MultiCh_deleteProgressiveVcapVencVdecVdis()
{
    UInt32 i;

    for (i = 0; i < gProgressiveDVRUsecaseContext.createdLinkCount; i++)
    {
        System_linkDelete (gProgressiveDVRUsecaseContext.createdLinks[i]);
    }
    gProgressiveDVRUsecaseContext.createdLinkCount = 0;

    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    MultiCh_prfLoadCalcEnable(FALSE, TRUE, FALSE);

    Vcap_deleteVideoDecoder();
    gVdisModuleContext.setMosaicFxn     = NULL;
}


static
Void  multich_progressive_map_swms2deichnum(UInt32 swmsChNum,UInt32 *deiLinkID,UInt32 *deiChNum)
{
    Int i;

    *deiLinkID = SYSTEM_LINK_ID_INVALID;

    for (i = 0; i < OSA_ARRAYSIZE(gProgressiveDVRUsecaseContext.mergeChMap);i++)
    {
        if ((swmsChNum >= gProgressiveDVRUsecaseContext.mergeChMap[i].startChNum)
            &&
            (swmsChNum < gProgressiveDVRUsecaseContext.mergeChMap[i].startChNum + gProgressiveDVRUsecaseContext.mergeChMap[i].numCh))
        {
            break;
        }
    }
    if (i < OSA_ARRAYSIZE(gProgressiveDVRUsecaseContext.mergeChMap))
    {
        *deiLinkID =  gVcapModuleContext.deiId[i];
        *deiChNum  =  swmsChNum - gProgressiveDVRUsecaseContext.mergeChMap[i].startChNum;
    }

}

static
UInt32 multich_progressive_map_swms_channel2win(VDIS_DEV devId,UInt32 swMsChNum)
{
    UInt32 winNum = SYSTEM_SW_MS_INVALID_ID;
    Int i;

    for (i = 0; i < gProgressiveDVRUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(devId)].numWin;i++)
    {
        if (swMsChNum == gProgressiveDVRUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(devId)].winInfo[i].channelNum)
        {
            winNum = i;
            break;
        }
    }
    return winNum;
}

static
Bool  multich_progressive_is_invisible_channel(VDIS_DEV devId,UInt32 swMsChNum)
{
    if (multich_progressive_map_swms_channel2win(devId, swMsChNum) == SYSTEM_SW_MS_INVALID_ID)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static
Bool  multich_progressive_is_dualdisplay_channel(UInt32 swMsChNum)
{
    Bool isChMapped2HDMI = !(multich_progressive_is_invisible_channel(VDIS_DEV_HDMI,swMsChNum));
    Bool isChMapped2DVO2 =   !(multich_progressive_is_invisible_channel(VDIS_DEV_DVO2,swMsChNum));
    Bool dualDisplayChannel = FALSE;

    if (isChMapped2HDMI && isChMapped2DVO2)
    {
        dualDisplayChannel = TRUE;
    }
    return dualDisplayChannel;
}

static
Void multich_progressive_get_swms_channel_out_resolution(VDIS_DEV vdDevId, UInt32 swMsChNum,UInt32 *chOutWidth,UInt32 *chOutHeight)
{
    UInt32 winNum;

    *chOutWidth = 0;
    *chOutHeight = 0;
    if (!multich_progressive_is_invisible_channel(vdDevId,swMsChNum))
    {
        winNum = multich_progressive_map_swms_channel2win(vdDevId,swMsChNum);
        OSA_assert(winNum != SYSTEM_SW_MS_INVALID_ID);
        *chOutWidth = gProgressiveDVRUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(vdDevId)].winInfo[winNum].width;
        *chOutHeight = gProgressiveDVRUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(vdDevId)].winInfo[winNum].height;
    }
}

static
Void multich_progressive_enable_visible_deisc_channels(UInt32 deiId, UInt32 numCh, UInt32 chList[])
{
    DeiLink_ChannelInfo chParams;
    Int i;
    Int32 status;

    for (i = 0; i < numCh; i++)
    {
        chParams.streamId = DEI_LINK_OUT_QUE_DEI_SC;
        chParams.enable   = TRUE;
        chParams.channelId = chList[i];
        status =
        System_linkControl(gVcapModuleContext.deiId[deiId],DEI_LINK_CMD_ENABLE_CHANNEL,
                            &chParams,sizeof(chParams),TRUE);
        OSA_assert(status == 0);
    }


}


static
Void  multich_progressive_map_swms2deiprms(VDIS_DEV vdDevId, UInt32 swMsChNum)
{
    UInt32 deiLinkID,deiChNum;
    Int32 status;
    UInt32 swmsChannelOutWidth,swmsChannelOutHeight;
    UInt32 otherSwMsChannelOutWidth,otherSwMsChannelOutHeight;
    VDIS_DEV otherDevId;
    Int i;

    /* Match DEI preview output to swms output window resolution directly. */
    multich_progressive_map_swms2deichnum(swMsChNum,&deiLinkID,&deiChNum);
    if (deiLinkID != SYSTEM_LINK_ID_INVALID)
    {
        DeiLink_chDynamicSetOutRes deiOutRes;

        multich_progressive_get_swms_channel_out_resolution(vdDevId,swMsChNum,&swmsChannelOutWidth,&swmsChannelOutHeight);
        OSA_assert((swmsChannelOutWidth != 0) && (swmsChannelOutHeight != 0));

        deiOutRes.queId = DEI_LINK_OUT_QUE_DEI_SC;
        deiOutRes.chId  = deiChNum;
        OSA_COMPILETIME_ASSERT(OSA_ARRAYSIZE(deiOutRes.pitch) ==
                                OSA_ARRAYSIZE(gProgressiveDVRUsecaseContext.deiLinkInfo[MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(deiLinkID)].queInfo[DEI_LINK_OUT_QUE_DEI_SC].chInfo[deiChNum].pitch));
        for (i =0; i < OSA_ARRAYSIZE(deiOutRes.pitch);i++)
        {
            deiOutRes.pitch[i] = gProgressiveDVRUsecaseContext.deiLinkInfo[MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(deiLinkID)].queInfo[DEI_LINK_OUT_QUE_DEI_SC].chInfo[deiChNum].pitch[i];
        }
        if ((swmsChannelOutWidth < gProgressiveDVRUsecaseContext.deiLinkInfo[MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(deiLinkID)].queInfo[DEI_LINK_OUT_QUE_DEI_SC].chInfo[deiChNum].width)
            &&
            (swmsChannelOutHeight < gProgressiveDVRUsecaseContext.deiLinkInfo[MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(deiLinkID)].queInfo[DEI_LINK_OUT_QUE_DEI_SC].chInfo[deiChNum].height))
        {
            if (!multich_progressive_is_dualdisplay_channel(swMsChNum))
            {
                deiOutRes.width = swmsChannelOutWidth;
                deiOutRes.height = swmsChannelOutHeight;
            }
            else
            {
                otherDevId =  (vdDevId == VDIS_DEV_HDMI) ? VDIS_DEV_DVO2 : VDIS_DEV_HDMI;
                multich_progressive_get_swms_channel_out_resolution(otherDevId,swMsChNum,&otherSwMsChannelOutWidth,&otherSwMsChannelOutHeight);
                OSA_assert((otherSwMsChannelOutWidth != 0) && (otherSwMsChannelOutHeight != 0));
                if ((otherSwMsChannelOutWidth > gProgressiveDVRUsecaseContext.deiLinkInfo[MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(deiLinkID)].queInfo[DEI_LINK_OUT_QUE_DEI_SC].chInfo[deiChNum].width)
                    ||
                    (otherSwMsChannelOutHeight > gProgressiveDVRUsecaseContext.deiLinkInfo[MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(deiLinkID)].queInfo[DEI_LINK_OUT_QUE_DEI_SC].chInfo[deiChNum].height))
                {
                    otherSwMsChannelOutWidth = gProgressiveDVRUsecaseContext.deiLinkInfo[MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(deiLinkID)].queInfo[DEI_LINK_OUT_QUE_DEI_SC].chInfo[deiChNum].width;
                    otherSwMsChannelOutHeight = gProgressiveDVRUsecaseContext.deiLinkInfo[MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(deiLinkID)].queInfo[DEI_LINK_OUT_QUE_DEI_SC].chInfo[deiChNum].height;
                }
                deiOutRes.width  = MAX(swmsChannelOutWidth,otherSwMsChannelOutWidth);
                deiOutRes.height  = MAX(swmsChannelOutHeight,otherSwMsChannelOutHeight);
            }
        }
        else
        {
            deiOutRes.width = gProgressiveDVRUsecaseContext.deiLinkInfo[MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(deiLinkID)].queInfo[DEI_LINK_OUT_QUE_DEI_SC].chInfo[deiChNum].width;
            deiOutRes.height = gProgressiveDVRUsecaseContext.deiLinkInfo[MULTICH_PROGRESSIVEDVR_MAPDEILINKID2INDEX(deiLinkID)].queInfo[DEI_LINK_OUT_QUE_DEI_SC].chInfo[deiChNum].height;
        }
        printf("MULTICH_PROGRESSIVE_DVR:DEI Preview Output resolution matching SwMs window.DEI_LINKID[0x%X],CHNUM[%d],Width[%d]:Height[%d]\n",
                deiLinkID,deiOutRes.chId,deiOutRes.width,deiOutRes.height);
        status =
        System_linkControl(deiLinkID, DEI_LINK_CMD_SET_OUTPUTRESOLUTION, &(deiOutRes), sizeof(deiOutRes), TRUE);
        OSA_assert(status == 0);
    }
}

static
Void  multich_progressive_get_invisible_deipreview_channels_list(UInt32 deiId,UInt32 *invisbleChannelList,
                                                               UInt32 *numInvisibleCh,UInt32 maxCh)
{
    Int i;
    UInt32 swmsChNum;

    *numInvisibleCh = 0;

    for (i = 0; i < gProgressiveDVRUsecaseContext.mergeChMap[deiId].numCh ;i++)
    {
        swmsChNum = gProgressiveDVRUsecaseContext.mergeChMap[deiId].startChNum + i;
        if (multich_progressive_is_invisible_channel(VDIS_DEV_HDMI,swmsChNum)
            &&
            multich_progressive_is_invisible_channel(VDIS_DEV_DVO2,swmsChNum))
        {
            OSA_assert(*numInvisibleCh < maxCh);
            invisbleChannelList[*numInvisibleCh] = i;
            *numInvisibleCh += 1;
        }
    }
}

static
Void  multich_progressive_get_visible_deipreview_channels_list(UInt32 deiId,UInt32 visbleChannelList[static DEI_LINK_MAX_CH],
                                                               UInt32 *numVisibleCh,UInt32 maxCh)
{
    Int i;
    UInt32 swmsChNum;

    *numVisibleCh = 0;

    for (i = 0; i < gProgressiveDVRUsecaseContext.mergeChMap[deiId].numCh ;i++)
    {
        swmsChNum = gProgressiveDVRUsecaseContext.mergeChMap[deiId].startChNum + i;
        if (!(multich_progressive_is_invisible_channel(VDIS_DEV_HDMI,swmsChNum)
              &&
              multich_progressive_is_invisible_channel(VDIS_DEV_DVO2,swmsChNum)))
        {
            OSA_assert(*numVisibleCh < maxCh);
            visbleChannelList[*numVisibleCh] = i;
            *numVisibleCh += 1;
        }
    }
}

typedef struct MutichProgressiveDVR_deiSwitchCh_s
{
    UInt32 deiId;
    UInt32 deiChNum;
    UInt32 width;
    UInt32 height;
} MutichProgressiveDVR_deiSwitchCh_s;


static
Bool multich_progressive_is_switch_channel(UInt32 chId,
                                           MutichProgressiveDVR_deiSwitchCh_s switchList[static MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_MAX_CHANNELS],
                                           UInt32 numChPerDei,
                                           UInt32 numSwitchCh,
                                           UInt32 *switchIdx)
{
    Int i;
    Bool doSwitch = FALSE;

    for (i = 0; i < numSwitchCh; i++)
    {
        if (chId == ((switchList[i].deiId * numChPerDei) + switchList[i].deiChNum))
        {
            doSwitch = TRUE;
            *switchIdx = i;
            break;
        }
    }
    return doSwitch;
}


static
Void multich_progressive_disable_switched_dei_preview_ch(UInt32 numCh,
                                                         MutichProgressiveDVR_deiSwitchCh_s switchList[static MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_MAX_CHANNELS])
{
    Int i;
    DeiLink_ChannelInfo chParams;
    Int32 status;

    for (i = 0; i < numCh; i++)
    {
        chParams.streamId = DEI_LINK_OUT_QUE_DEI_SC;
        chParams.channelId     = switchList[i].deiChNum;
        chParams.enable = FALSE;
        printf("MULTICH_PROGRESSIVE_DVR:DEI Preview Output disable switch channel.DEI_LINKID[0x%X],CHNUM[%d]\n",
                gVcapModuleContext.deiId[switchList[i].deiId],chParams.channelId);
        status =
        System_linkControl(gVcapModuleContext.deiId[switchList[i].deiId],DEI_LINK_CMD_DISABLE_CHANNEL,
                            &chParams,sizeof(chParams),TRUE);
        OSA_assert(status == 0);
    }
}

static
Void multich_progressive_apply_dei_preview_switch(UInt32 numCh,
                                                  MutichProgressiveDVR_deiSwitchCh_s switchList[static MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_MAX_CHANNELS])
{
    Int i;
    Int32 status;
    SelectLink_OutQueChInfo selectInfo;

    memset(&selectInfo,0,sizeof(selectInfo));
    selectInfo.outQueId = 0;
    status = System_linkControl(gProgressiveDVRUsecaseContext.swmsSelectId,
                                SELECT_LINK_CMD_GET_OUT_QUE_CH_INFO,
                                &selectInfo,
                                sizeof(selectInfo),
                                TRUE);
    OSA_assert(status == 0);
    for (i = 0; i < selectInfo.numOutCh; i++)
    {
        UInt32 switchIdx = 0;

        if (multich_progressive_is_switch_channel(i,switchList,gProgressiveDVRUsecaseContext.numChPerDei,numCh,&switchIdx))
        {
            selectInfo.inChNum[i] = gProgressiveDVRUsecaseContext.cifSelectStartCh + i;
            printf("MULTICH_PROGRESSIVE_DVR:DEI Preview Output switch: FROM [DEI_%d:CH_%d] -> CIF CH_%d\n",
                    switchList[switchIdx].deiId,switchList[switchIdx].deiChNum,
                   i);
        }
        else
        {
            selectInfo.inChNum[i] = i;
        }
    }
    status = System_linkControl(gProgressiveDVRUsecaseContext.swmsSelectId,
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectInfo,
                                sizeof(selectInfo),
                                TRUE);
    OSA_assert(status == 0);
    multich_progressive_disable_switched_dei_preview_ch(numCh,switchList);
}


static
Void multich_progressive_get_dei_preview_ch_resolution(UInt32 deiId,
                                                       UInt32 deiChNum,
                                                       UInt32 *width,
                                                       UInt32 *height)
{
    Int32 status;
    DeiLink_chDynamicSetOutRes outRes;


    memset(&outRes,0,sizeof(outRes));

    outRes.chId = deiChNum;
    outRes.queId = DEI_LINK_OUT_QUE_DEI_SC;


    status =
    System_linkControl(gVcapModuleContext.deiId[deiId],
                       DEI_LINK_CMD_GET_OUTPUTRESOLUTION,
                       &outRes,sizeof(outRes),TRUE);
    OSA_assert(status == 0);
    *width = outRes.width;
    *height = outRes.height;
}

static
Void multich_progressive_select_dei_preview_switch_ch(UInt32 numCh,
                                                      MutichProgressiveDVR_deiSwitchCh_s switchList[static MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_MAX_CHANNELS],
                                                      UInt32 compareDeiId,
                                                      UInt32 compareDeiCh,
                                                      UInt32 compareWidth,
                                                      UInt32 compareHeight)
{
    Int i;

    for (i = 0; i < numCh; i++)
    {
        if ((compareWidth < switchList[i].width) && (compareHeight < switchList[i].height))
        {
            switchList[i].width = compareWidth;
            switchList[i].height = compareHeight;
            switchList[i].deiId = compareDeiId;
            switchList[i].deiChNum = compareDeiCh;
        }
        break;
    }
}



static
Void multich_progressive_get_dei_preview_channels_to_switch(UInt32 numCh,
                                                            MutichProgressiveDVR_deiSwitchCh_s switchList[static MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_MAX_CHANNELS],
                                                            UInt32 numVisibleChDei[static 2],
                                                            UInt32 visbleChannelListDei[static 2][DEI_LINK_MAX_CH])
{
    Int i;
    UInt32 width,height;
    UInt32 deiId,deiChNum;
    UInt32 compareStartChNum;

    OSA_assert((numCh > 0 ) && (numCh <= MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_MAX_CHANNELS));

    for (i = 0; i < numCh;i++)
    {
        if (i < numVisibleChDei[0])
        {
            switchList[i].deiId = 0;
            switchList[i].deiChNum = visbleChannelListDei[0][i];
            multich_progressive_get_dei_preview_ch_resolution(0,
                                                              visbleChannelListDei[0][i],
                                                              &switchList[i].width,
                                                              &switchList[i].height);
            compareStartChNum = i;
        }
        else
        {
            OSA_assert(numVisibleChDei[1] > i - numVisibleChDei[0]);
            switchList[i].deiId = 1;
            switchList[i].deiChNum = visbleChannelListDei[1][i - numVisibleChDei[0]];
            multich_progressive_get_dei_preview_ch_resolution(1,
                                                              visbleChannelListDei[1][i - numVisibleChDei[0]],
                                                              &switchList[i].width,
                                                              &switchList[i].height);
            compareStartChNum = i - numVisibleChDei[0];
        }
    }
    compareStartChNum++;

    for (deiId = switchList[numCh - 1].deiId; deiId < 2; deiId++)
    {
        for (deiChNum = compareStartChNum; deiChNum < numVisibleChDei[deiId];deiChNum++)
        {
            multich_progressive_get_dei_preview_ch_resolution(deiId,
                                                              visbleChannelListDei[deiId][deiChNum],
                                                              &width,
                                                              &height);
            multich_progressive_select_dei_preview_switch_ch(numCh,
                                                             switchList,
                                                             deiId,
                                                             visbleChannelListDei[deiId][deiChNum],
                                                             width,
                                                             height);
        }

    }
}


static
Void multich_progressive_switch_deisc_deivip()
{
    UInt32 numVisibleChDei[2] = {0};
    UInt32 visbleChannelListDei[2][DEI_LINK_MAX_CH];
    UInt32 numVisibleDecChannels = 0;
    UInt32 numInvisibleDecChannels = 0;
    MutichProgressiveDVR_deiSwitchCh_s switchList[MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_MAX_CHANNELS];
    UInt32 numSwitchCh = 0;


    memset(&switchList,0,sizeof(switchList));
    multich_progressive_get_visible_deipreview_channels_list(0,
                                                             visbleChannelListDei[0],
                                                             &numVisibleChDei[0],
                                                             OSA_ARRAYSIZE(visbleChannelListDei[0]));

    multich_progressive_get_visible_deipreview_channels_list(1,
                                                             visbleChannelListDei[1],
                                                             &numVisibleChDei[1],
                                                             OSA_ARRAYSIZE(visbleChannelListDei[1]));

    multich_progressive_get_decode_visibility_channels_list(gProgressiveDVRUsecaseContext.invisbleDecChannelList,
                                                          &numInvisibleDecChannels,
                                                          OSA_ARRAYSIZE(gProgressiveDVRUsecaseContext.invisbleDecChannelList),
                                                          gProgressiveDVRUsecaseContext.visbleDecChannelList,
                                                          &numVisibleDecChannels,
                                                          OSA_ARRAYSIZE(gProgressiveDVRUsecaseContext.visbleDecChannelList));

    printf("MULTICH_PROGRESSIVE_DVR:%d:%d:%d\n",
            numVisibleChDei[0],numVisibleChDei[1],numVisibleDecChannels);

   if (((numVisibleChDei[0] + numVisibleChDei[1]) >= MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_LIVE_CHCOUNT_THRESHOLD)
       &&
       (numVisibleDecChannels > MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_PLAYBACK_CHCOUNT_THRESHOLD))
   {
       numSwitchCh =  MIN((numVisibleChDei[0] + numVisibleChDei[1]),MULTICH_PROGRESSIVE_DVR_PREVIEW_DISPLAY_SWITCH_LIVE_CHCOUNT_DISABLE);
       printf("MULTICH_PROGRESSIVE_DVR:DEI Preview Output switch: NUMSWITCHCH[%d]\n",
               numSwitchCh);
       multich_progressive_get_dei_preview_channels_to_switch(numSwitchCh,
                                                               switchList,
                                                               numVisibleChDei,
                                                               visbleChannelListDei);

   }
   multich_progressive_enable_visible_deisc_channels(0,numVisibleChDei[0],visbleChannelListDei[0]);
   multich_progressive_enable_visible_deisc_channels(1,numVisibleChDei[1],visbleChannelListDei[1]);
   multich_progressive_apply_dei_preview_switch(numSwitchCh,switchList);

}

static
Void  multich_progressive_disable_invisible_deipreview_channels()
{
    Int i;
    UInt32 invisbleChannelList[DEI_LINK_MAX_CH];
    UInt32 numInvisibleCh = 0;
    DeiLink_ChannelInfo chParams;
    Int32 status;

    multich_progressive_get_invisible_deipreview_channels_list(0,
                                                             invisbleChannelList,
                                                             &numInvisibleCh,
                                                             OSA_ARRAYSIZE(invisbleChannelList));
    for (i =0; i < numInvisibleCh;i++)
    {
        chParams.streamId = DEI_LINK_OUT_QUE_DEI_SC;
        chParams.channelId     = invisbleChannelList[i];
        chParams.enable        = FALSE;
        printf("MULTICH_PROGRESSIVE_DVR:DEI Preview Output disable invisible channel.DEI_LINKID[0x%X],CHNUM[%d]\n",
                gVcapModuleContext.deiId[0],chParams.channelId);
        status =
        System_linkControl(gVcapModuleContext.deiId[0],DEI_LINK_CMD_DISABLE_CHANNEL,
                            &chParams,sizeof(chParams),TRUE);
        OSA_assert(status == 0);
    }

    multich_progressive_get_invisible_deipreview_channels_list(1,
                                                             invisbleChannelList,
                                                             &numInvisibleCh,
                                                             OSA_ARRAYSIZE(invisbleChannelList));
    for (i =0; i < numInvisibleCh;i++)
    {
        chParams.streamId = DEI_LINK_OUT_QUE_DEI_SC;
        chParams.channelId  = invisbleChannelList[i];
        chParams.enable        = FALSE;
        printf("MULTICH_PROGRESSIVE_DVR:DEI Preview Output disable invisible channel.DEI_LINKID[0x%X],CHNUM[%d]\n",
                gVcapModuleContext.deiId[1],chParams.channelId);
        status =
        System_linkControl(gVcapModuleContext.deiId[1],DEI_LINK_CMD_DISABLE_CHANNEL,
                            &chParams,sizeof(chParams),TRUE);
        OSA_assert(status == 0);
    }
}

static
Void  multich_progressive_get_decode_visibility_channels_list(UInt32 *invisbleChannelList,
                                                           UInt32 *numInvisibleCh,UInt32 maxInvisibleCh,
                                                           UInt32 *visbleChannelList,
                                                           UInt32 *numVisibleCh,UInt32 maxVisibleCh)
{
    Int i;
    UInt32 swmsChNum;
    UInt32 swmsChNumSD;

    *numInvisibleCh = 0;

    for (i = 0; i < gVdecModuleContext.vdecConfig.numChn ;i++)
    {
        Vdec_mapDec2DisplayChId(VDIS_DEV_HDMI,i,&swmsChNum);
        Vdec_mapDec2DisplayChId(VDIS_DEV_DVO2,i,&swmsChNumSD);
        OSA_assert(swmsChNum == swmsChNumSD);
        if (multich_progressive_is_invisible_channel(VDIS_DEV_HDMI,swmsChNum)
            &&
            multich_progressive_is_invisible_channel(VDIS_DEV_DVO2,swmsChNum))
        {
            OSA_assert(*numInvisibleCh < maxInvisibleCh);
            invisbleChannelList[*numInvisibleCh] = i;
            *numInvisibleCh += 1;
        }
        else
        {
            OSA_assert(*numVisibleCh < maxVisibleCh);
            visbleChannelList[*numVisibleCh] = i;
            *numVisibleCh += 1;
        }
    }

}

static
Void  multich_progressive_enable_visible_decode_channels()
{
    Int i;
    UInt32 numInvisibleCh = 0;
    UInt32 numVisibleCh = 0;
    DecLink_ChannelInfo decChInfo;
    Int32 status;

    multich_progressive_get_decode_visibility_channels_list(gProgressiveDVRUsecaseContext.invisbleDecChannelList,
                                                          &numInvisibleCh,
                                                          OSA_ARRAYSIZE(gProgressiveDVRUsecaseContext.invisbleDecChannelList),
                                                          gProgressiveDVRUsecaseContext.visbleDecChannelList,
                                                          &numVisibleCh,
                                                          OSA_ARRAYSIZE(gProgressiveDVRUsecaseContext.visbleDecChannelList));
    for (i =0; i < numVisibleCh;i++)
    {
        decChInfo.chId     = gProgressiveDVRUsecaseContext.visbleDecChannelList[i];
        printf("MULTICH_PROGRESSIVE_DVR:Decode enable visible channel.DEC_LINKID[0x%X],CHNUM[%d]\n",
                gVdecModuleContext.decId,decChInfo.chId);
        status =
        System_linkControl(gVdecModuleContext.decId,DEC_LINK_CMD_ENABLE_CHANNEL,
                            &decChInfo,sizeof(decChInfo),TRUE);
        OSA_assert(status == 0);
    }

    for (i =0; i < numInvisibleCh;i++)
    {
        decChInfo.chId     = gProgressiveDVRUsecaseContext.invisbleDecChannelList[i];
        printf("MULTICH_PROGRESSIVE_DVR:Decode disable invisible channel.DEC_LINKID[0x%X],CHNUM[%d]\n",
                gVdecModuleContext.decId,decChInfo.chId);
        status =
        System_linkControl(gVdecModuleContext.decId,DEC_LINK_CMD_DISABLE_CHANNEL,
                            &decChInfo,sizeof(decChInfo),TRUE);
        OSA_assert(status == 0);
    }
}

static
Int32 MultiCh_progressiveDVRSetMosaicParams(VDIS_DEV vdDevId, VDIS_MOSAIC_S *psVdMosaicParam )
{
    UInt32 winId, chId;
    UInt32 swMsId = SYSTEM_LINK_ID_INVALID;
    SwMsLink_LayoutPrm *vdisLayoutPrm;

    swMsId = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(vdDevId)];
    if(swMsId==SYSTEM_LINK_ID_INVALID)
        return -1;

    vdisLayoutPrm = &gProgressiveDVRUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(vdDevId)];

    /* Get display resolution and coordinates */
    vdisLayoutPrm->numWin = psVdMosaicParam->numberOfWindows;
    vdisLayoutPrm->onlyCh2WinMapChanged = psVdMosaicParam->onlyCh2WinMapChanged;
    vdisLayoutPrm->outputFPS = psVdMosaicParam->outputFPS;

    /* Assign each windows coordinates, size and mapping */
    for(winId=0; winId<vdisLayoutPrm->numWin; winId++)
    {
        vdisLayoutPrm->winInfo[winId].channelNum         = psVdMosaicParam->chnMap[winId];
        vdisLayoutPrm->winInfo[winId].bufAddrOffset[0u]      = -1;
        vdisLayoutPrm->winInfo[winId].bufAddrOffset[1u]      = -1;
        vdisLayoutPrm->winInfo[winId].width              = psVdMosaicParam->winList[winId].width;
        vdisLayoutPrm->winInfo[winId].height             = psVdMosaicParam->winList[winId].height;
        vdisLayoutPrm->winInfo[winId].startX             = psVdMosaicParam->winList[winId].start_X;
        vdisLayoutPrm->winInfo[winId].startY             = psVdMosaicParam->winList[winId].start_Y;
        vdisLayoutPrm->winInfo[winId].bypass             = FALSE;
        chId = psVdMosaicParam->chnMap[winId];

        if(chId < gVdisModuleContext.vdisConfig.numChannels)
        {
            Vdis_setChn2WinMap(vdDevId, chId,winId);

            if(Vdis_isEnableChn(vdDevId,chId) == FALSE)
            {
                vdisLayoutPrm->winInfo[winId].channelNum = SYSTEM_SW_MS_INVALID_ID;
            }
        }
        multich_progressive_map_swms2deiprms(vdDevId,vdisLayoutPrm->winInfo[winId].channelNum);
    }

    Vdis_swMs_PrintLayoutParams(vdDevId, vdisLayoutPrm);
    System_linkControl(swMsId, SYSTEM_SW_MS_LINK_CMD_SWITCH_LAYOUT, (vdisLayoutPrm), sizeof(*vdisLayoutPrm), TRUE);

    multich_progressive_disable_invisible_deipreview_channels();
    multich_progressive_enable_visible_decode_channels();
    multich_progressive_switch_deisc_deivip();
    return 0;
}

