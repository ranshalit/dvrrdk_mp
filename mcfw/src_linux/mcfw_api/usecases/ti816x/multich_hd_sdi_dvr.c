/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/


#include "mcfw/src_linux/mcfw_api/usecases/multich_common.h"
#include "mcfw/src_linux/mcfw_api/usecases/multich_ipcbits.h"
#include "mcfw/src_linux/mcfw_api/ti_vdis_priv.h"
#include "mcfw/src_linux/devices/tw2968/src/tw2968_priv.h"
#include "mcfw/src_linux/devices/tvp5158/src/tvp5158_priv.h"
#include "mcfw/interfaces/link_api/system_tiler.h"
#include "mcfw/interfaces/link_api/avsync_hlos.h"
#include "mcfw/interfaces/ti_vdis_timings.h"

/* =============================================================================
 * Externs
 * =============================================================================
 */

/* =============================================================================
 * Use case code
 * =============================================================================
 */
/**

               +--------------+
               |              |
               |              |
               |   Capture    |
               |              |
               |              |
               +--------------+
                  +         +
                  |         |
          VIP0    |         |VIP1
          1ch HD  |         |4ch SD
            @     |         | @
         1080P/   |         |60 fields
         1080i    |         |
                  V         V
     +--------------------------------------+
     |                                      |
     |             Select                   |
     |                                      |
     +-+--------+-------------+-----------+-+
       |        |             |           |
      0|        |1           2|          3|
   +----        |             |           |
   |            |             |           |
   |            V             v           v
   v
+--------+  +--------+    +--------+   +---------+
|HD  SC_3|  |D1 SC_2 |    |HD  SC_3|   | D1 SC_2 |
|DEI     |  |DEI     |    |DEI     |   | DEI     |
|Enabled |  |Enabled |    |Disabled|   | Bypass  |
+--------+  +--------+    +--------+   +---------+
    +           +             +            +
    |           |             |            |
    |           |             |            |
    |           v             |            v
    |        +------+         |         +--------+
    |        |      |         |         |        |
    |        | NSF  |         |         | NSF    |
    |        |      |         |         |        |
    |        +------+         |         +--------+
    |           +             |            +
    |           |             |            |
    |           |             |            |                                          (Configure ch FPS to
    |           |             |            |                                           select ch to be
    |           |             |            |                      +----------------+    exported)           +--------------+
    |           |             |            |                      |                |                    +   |              |
    |           |             |            |            +-------->|  IpcFramesOut  |+---------------------->|  IpcFramesIn |
    |           |             |            |            |         |                |     ProcessLink        |              |
    |           |             |            |            |         +----------------+                        +--------------+
    |           |             |            |            |
    |           |             |            |            |
    |           |             |            |            |
    v           v             v            |            |
                                           v            +
  +-------------------------------------------+      +-------+    +-----------+            +-----------+
  |                                           |      |       |    |           |            |           |
  |        Pre Encode Merge Link              |      |       |    |           |            |           |
  |                                           |----->|Select |--->|EncodeLink |+---------->|IpcBitsOut |
  |                                           |      |       |    |           |            |           |
  |                                           |      |       |    |           |            |           |
  |                                           |      |       |    |           |            +-----------+
  +-------------------------------------------+      +-------+    +-----------+
                                                        +
                                                        |
                                                        |
                                                        |
                                                        |
                                                        |
                                                        v

                                                    +---------+   +-----------+            +-----------+
   +-------------------------------------------+    |         |   |           |            |           |
   |                                           |    |         |   |           |            |           |
   |                                           |    | PreSwMs |<-+|DecodeLink |<----------+|IpcBitsIn  |
   |             Dup                           |<--+| Merge   |   |           |            |           |
   |                                           |    |         |   |           |            |           |
   |                                           |    |         |   |           |            +-----------+
   +--------+---------------------------+------+    +---------+   +-----------+
            v                           v
         +------+                    +------+
         |      |                    |      |
         | SwMS0|                    | SwMS1|
         |      |                    |      |
         +--+---+                    +--+---+
            |                           |
            v                           v
      +----------+                +----------+
      |Display 0 |                |Display 1 |
      |          |                |          |
      +----------+                +----------+
*/


#define     MAX_NUM_CAPTURE_DEVICES        (4)

#define     NUM_MUX_SD_CAPTURE_CHANNELS    (4)
#define     NUM_NONMUX_HD_CAPTURE_CHANNELS (1)
#define     MAX_NUM_ENCODE_CHANNELS          (NUM_MUX_SD_CAPTURE_CHANNELS + NUM_NONMUX_HD_CAPTURE_CHANNELS)

#define     HD_CAPTURE_START_CHANNEL_NUM          (0)
#define     SD_CAPTURE_START_CHANNEL_NUM          (HD_CAPTURE_START_CHANNEL_NUM + NUM_NONMUX_HD_CAPTURE_CHANNELS)
#define     SIMULATE_HD_CAPTURE_START_CHANNEL_NUM (SD_CAPTURE_START_CHANNEL_NUM + NUM_MUX_SD_CAPTURE_CHANNELS)

#define     NUM_SD_DECODE_CHANNELS                (NUM_MUX_SD_CAPTURE_CHANNELS)
#define     NUM_HD_DECODE_CHANNELS                (NUM_NONMUX_HD_CAPTURE_CHANNELS)
#define     SD_DECODE_CHANNEL_WIDTH               (720)
#define     SD_DECODE_CHANNEL_HEIGHT              (576)
#define     HD_DECODE_CHANNEL_WIDTH               (1920)
#define     HD_DECODE_CHANNEL_HEIGHT              (1080)
#define     SD_DECODE_START_CH                    (0)
#define     HD_DECODE_START_CH                    (SD_DECODE_START_CH + NUM_SD_DECODE_CHANNELS)

#define     MULTICH_HDSDI_DVR_USECASE_MAX_NUM_LINKS       (64)

#define     NUM_BUFS_PER_CH_CAPTURE              (8)
#define     NUM_BUFS_PER_CH_DEI                  (6)
#define     NUM_BUFS_PER_CH_NSF                  (6)
#define     NUM_BUFS_PER_CH_DEC_SD               (6)
#define     NUM_BUFS_PER_CH_DEC_HD               (4)
#define     NUM_BUFS_PER_CH_SWMS_HD              (4)
#define     NUM_BUFS_PER_CH_BITSOUT_SD           (5)
#define     NUM_BUFS_PER_CH_BITSOUT_HD           (4)

#define     MAX_BUFFERING_QUEUE_LEN_PER_CH           (50)

#define     BIT_BUF_LENGTH_LIMIT_FACTOR_SD            (6)
#define     BIT_BUF_LENGTH_LIMIT_FACTOR_HD            (4)

#define     ENC_LINK_SD_STREAM_POOL_ID                (0)
#define     ENC_LINK_HD_STREAM_POOL_ID                (1)

#define     IPCBITSOUT_LINK_SD_STREAM_POOL_ID    (0)
#define     IPCBITSOUT_LINK_HD_STREAM_POOL_ID    (1)

#define     TILER_ENABLE_ENCODE                  (TRUE)
#define     TILER_ENABLE_DECODE_SD               (TRUE)
#define     TILER_ENABLE_DECODE_HD               (FALSE)

#define     NUM_DEI_LINK                          (4)
#define     DEI_LINK_IDX_D1_DEIENABLED            (0)
#define     DEI_LINK_IDX_HD_DEIENABLED            (1)
#define     DEI_LINK_IDX_D1_DEIDISABLED           (2)
#define     DEI_LINK_IDX_HD_DEIDISABLED           (3)

/* =============================================================================
 * Externs
 * =============================================================================
 */


/* =============================================================================
 * Use case code
 * =============================================================================
 */

static SystemVideo_Ivahd2ChMap_Tbl systemVid_encDecIvaChMapTbl =
{
    .isPopulated = 1,
    .ivaMap[0] =
    {
        .EncNumCh  = 4,
        .EncChList = {0, 1, 2, 3,},
        .DecNumCh  = 0,
        .DecChList = {0},
    },
    .ivaMap[1] =
    {
        .EncNumCh  = 1,
        .EncChList = {4,},
        .DecNumCh  = 0,
        .DecChList = {0, },
    },
    .ivaMap[2] =
    {
        .EncNumCh  = 0,
        .EncChList = {0,},
        .DecNumCh  = 2,
        .DecChList = {0, 1,},
    },
};

/** Merge Link Info */
#define     NUM_MERGE_LINK                         (3)


/**  D1 interlaced/progressive merge
  *   DEI_ENABLED---------------------DEI_OUT_QUE_VIP_SC-->Q0--|
  *                                                            | -D1_INTERLACE_PROGRESSIVE_MERGE_LINK
  *   DEI_DISABLED--------------------DEI_OUT_QUE_VIP_SC-->Q1--|
  */
#define     D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX                 (2)
#define     D1_INTERLACED_PROGRESSIVE_MERGE_LINK_NUM_INQUE           (2)
#define     D1_INTERLACED_PROGRESSIVE_MERGE_LINK_DEI_ENABLE_D1_QIDX  (0)
#define     D1_INTERLACED_PROGRESSIVE_MERGE_LINK_DEI_DISABLE_D1_QIDX (1)


#define     INTERLACE_PROGRESSIVE_MERGE_LINK_IDX                     0
#define     INTERLACE_PROGRESSIVE_MERGE_LINK_NUM_INQUE              (4)
#define     INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_ENABLE_D1_QIDX     (0)
#define     INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_ENABLE_HD_QIDX     (1)
#define     INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_DISABLE_D1_QIDX    (2)
#define     INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_DISABLE_HD_QIDX    (3)

/**  Pre encode merge
 *   PROGRESSIVE_CAPTURE---------------------0-->Q1--|
 *                                                   | -PRE_ENCODE_MERGE_LINK_IDX
 *   INTERLACED_CAPTURE_SCLR_DEI_SELECT - ---0-->Q0--|
 *
 */
#define     LIVE_DECODE_MERGE_LINK_IDX              1
#define     LIVE_DECODE_MERGE_LINK_NUM_INQUE       (2)
#define     LIVE_DECODE_MERGE_LINK_PREVIEW_QIDX    (0)
#define     LIVE_DECODE_MERGE_LINK_PLAYBACK_QIDX   (1)


#define     NUM_DUP_LINK                           2
#define     PRE_SWMS_DUP_LINK_IDX                  0
#define     PRE_ENCODE_DUP_LINK_IDX                1

#define     PRE_ENCODE_DUP_LINK_NUM_OUTQUE         3
#define     PRE_ENCODE_DUP_LINK_ENCODE_QIDX        (0)
#define     PRE_ENCODE_DUP_LINK_IPCFRAMESOUT_QIDX  (1)
#define     PRE_ENCODE_DUP_LINK_SWMS_QIDX          (2)


#define     NUM_SELECT_LINK                        2
 /**  Pre dei select
  *   PROGRESSIVE_CAPTURE---------------------0-->Q0--|
  *                                                   | -PRE_ENCODE_MERGE_LINK_IDX
  *   INTERLACED_CAPTURE_SCLR_DEI_SELECT -----1-->Q0--|
  *
  */
#define     PRE_DEI_SELECT_LINK_IDX                  0
#define     PRE_DEI_SELECT_LINK_NUM_OUTQUE           4
#define     PRE_DEI_SELECT_LINK_DEI_D1_ENABLE_QIDX   0
#define     PRE_DEI_SELECT_LINK_DEI_D1_DISABLE_QIDX  1
#define     PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX   2
#define     PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX  3

 /**  Pre encode select
  *   PROGRESSIVE_CAPTURE---------------------0-->Q0--|
  *                                                   | -PRE_ENCODE_MERGE_LINK_IDX
  *   INTERLACED_CAPTURE_SCLR_DEI_SELECT -----1-->Q0--|
  *
  */
#define     PRE_ENCODE_SELECT_LINK_IDX             1
#define     PRE_ENCODE_SELECT_LINK_NUM_OUTQUE      1
#define     PRE_ENCODE_SELECT_PROGRESSIVE_INPUT_INQUEIDX (0)
#define     PRE_ENCODE_SELECT_INTERLACED_INPUT_INQUEIDX  (1)


#define     NSF_LINK_PROGRESSIVE_OUT_QUE_IDX              (0)
#define     NSF_LINK_INTERLACED_OUT_QUE_IDX               (1)



#define     MULTICH_HDSDIDVR_MAPDEILINKID2INDEX(linkID)   (((linkID) == SYSTEM_LINK_ID_DEI_HQ_0) ? 0 : 1)

#define     MULTICH_HDSDIDVR_ENC_DEC_LOOPBACK             (TRUE)
#define     MULTICH_HDSDIDVR_SIMULATE_HD_CAPTURE          (TRUE)
#define     MULTICH_HDSDIDVR_FAKE_HD_CAPTURE              (TRUE)

typedef struct MultichHDSDIDVR_Context
{
    UInt32 mergeId[NUM_MERGE_LINK];
    UInt32 dupId[NUM_DUP_LINK];
    UInt32 ipcOutVpssId, ipcInVpssId;
    UInt32 ipcOutVideoId, ipcInVideoId;
    UInt32 selectId[NUM_SELECT_LINK];
    UInt32 createdLinkCount;
    UInt32 createdLinks[MULTICH_HDSDI_DVR_USECASE_MAX_NUM_LINKS];
    CaptureLink_CreateParams    capturePrm;
    DeiLink_CreateParams        deiPrm[MAX_DEI_LINK];
    MergeLink_CreateParams      mergePrm[NUM_MERGE_LINK];
    DupLink_CreateParams        dupPrm[NUM_DUP_LINK];
    SwMsLink_CreateParams       swMsPrm[VDIS_DEV_MAX];
    DisplayLink_CreateParams    displayPrm[VDIS_DEV_MAX];
    IpcLink_CreateParams        ipcOutVpssPrm;
    IpcLink_CreateParams        ipcInVpssPrm;
    IpcLink_CreateParams        ipcOutVideoPrm;
    IpcLink_CreateParams        ipcInVideoPrm;
    EncLink_CreateParams        encPrm;
    DecLink_CreateParams        decPrm;
    NsfLink_CreateParams        nsfPrm;
    IpcBitsOutLinkHLOS_CreateParams   ipcBitsOutHostPrm;
    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutVideoPrm;
    IpcBitsInLinkHLOS_CreateParams    ipcBitsInHostPrm[1];
    IpcBitsInLinkRTOS_CreateParams    ipcBitsInVideoPrm;
    IpcFramesOutLinkRTOS_CreateParams   ipcFramesOutVpssPrm;
    IpcFramesInLinkHLOS_CreateParams    ipcFramesInHostPrm;
    SelectLink_CreateParams             selectPrm[NUM_SELECT_LINK];
    VCAP_DEVICE_CREATE_PARAM_S vidDecVideoModeArgs[MAX_NUM_CAPTURE_DEVICES];
    AvsyncLink_LinkSynchConfigParams       avsyncCfg[2];
    UInt32                                 captureFps;
    MergeLink_InLinkChInfo                 mergeChMap[2];
    SwMsLink_LayoutPrm                     swmsLayoutPrm[VDIS_DEV_MAX];
    System_LinkInfo                        deiLinkInfo[2];
    VDIS_MOSAIC_S                          vdisMosaicPrms[VDIS_DEV_MAX];
    UInt32                                 invisbleDecChannelList[DEC_LINK_MAX_CH];
    UInt32                                 visbleDecChannelList[DEC_LINK_MAX_CH];
    IVIDEO_Format                          encChannel2CodecTypeMap[MAX_NUM_ENCODE_CHANNELS];
    System_LinkInfo                        ipcBitsInLinkInfo;
    UInt32                                 numPreDeiSelectInputChannelsD1;
    UInt32                                 numPreDeiSelectInputChannelsHD;
    UInt32                                 encodeSwitchNumCh;
    Bool                                   isProgressiveHDCapture;
    NullSrcLink_CreateParams               nullSrcPrms;
    UInt32                                 nullSrcLinkId;
}  MultichHDSDIDVR_Context;

MultichHDSDIDVR_Context gHDSDIDVRUsecaseContext =
{
    .createdLinkCount           = 0,
    .encChannel2CodecTypeMap    = {IVIDEO_MPEG4SP,IVIDEO_MJPEG,IVIDEO_H264HP,IVIDEO_H264HP,IVIDEO_H264HP}
};

#ifdef MAX
#undef MAX
#endif

#define MAX(a,b) ((a) > (b) ? (a) : (b))

#define MULTICH_HDSDIDVR_GET_CAPTURE_FIELDS_PER_SEC()           (gHDSDIDVRUsecaseContext.captureFps)
#define MULTICH_HDSDIDVR_GET_CAPTURE_FRAMES_PER_SEC()           (gHDSDIDVRUsecaseContext.captureFps/2)
#define MULTICH_HDSDIDVR_IS_PROGRESSIVE_HD_CAPTURE()            (gHDSDIDVRUsecaseContext.)
static
Int32 MultiCh_hdsdiDVRSetMosaicParams(VDIS_DEV vdDevId, VDIS_MOSAIC_S *psVdMosaicParam );
static
Void multich_hdsdidvr_select_all_progressive_encode();
static
Void multich_hdsdidvr_select_all_interlaced_encode();
static
Int32 multich_hdsdidvr_set_wrbk_capture_prms(UInt32 inW, UInt32 inH, UInt32 outW, UInt32 outH);
static
Int32 multich_hdsdidvr_start_wrbk_capture();
static
Int32 multich_hdsdidvr_stop_wrbk_capture();


static Void multich_hdsdidvr_register_created_link(MultichHDSDIDVR_Context *pContext,
                                                    UInt32 linkID)
{
    OSA_assert(pContext->createdLinkCount < OSA_ARRAYSIZE(pContext->createdLinks));
    pContext->createdLinks[pContext->createdLinkCount] = linkID;
    pContext->createdLinkCount++;
}

#define MULTICH_HDSDIDVR_CREATE_LINK(linkID,createPrm,createPrmSize)           \
    do                                                                          \
    {                                                                           \
        System_linkCreate(linkID,createPrm,createPrmSize);                      \
        multich_hdsdidvr_register_created_link(&gHDSDIDVRUsecaseContext,      \
                                                linkID);                        \
    } while (0)

static
Void multich_hdsdidvr_reset_link_prms()
{
    int i;

    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHDSDIDVRUsecaseContext.ipcOutVpssPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHDSDIDVRUsecaseContext.ipcInVpssPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHDSDIDVRUsecaseContext.ipcOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gHDSDIDVRUsecaseContext.ipcInVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams,gHDSDIDVRUsecaseContext.ipcBitsOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,gHDSDIDVRUsecaseContext.ipcBitsInHostPrm[0]);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,gHDSDIDVRUsecaseContext.ipcBitsInHostPrm[1]);
    MULTICH_INIT_STRUCT(IpcBitsInLinkRTOS_CreateParams,gHDSDIDVRUsecaseContext.ipcBitsInVideoPrm);
    IpcBitsOutLinkHLOS_CreateParams_Init(&gHDSDIDVRUsecaseContext.ipcBitsOutHostPrm);
    DecLink_CreateParams_Init(&gHDSDIDVRUsecaseContext.decPrm);
    EncLink_CreateParams_Init(&gHDSDIDVRUsecaseContext.encPrm);
    CaptureLink_CreateParams_Init(&gHDSDIDVRUsecaseContext.capturePrm);
    for (i = 0; i < OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.displayPrm);i++)
    {
        DisplayLink_CreateParams_Init(&gHDSDIDVRUsecaseContext.displayPrm[i]);
    }
    for (i = 0; i < OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.swMsPrm);i++)
    {
        SwMsLink_CreateParams_Init(&gHDSDIDVRUsecaseContext.swMsPrm[i]);
    }

     for (i = 0; i < OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.deiPrm);i++)
     {
         DeiLink_CreateParams_Init(&gHDSDIDVRUsecaseContext.deiPrm[i]);
     }
     for (i = 0; i < OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.avsyncCfg);i++)
     {
         AvsyncLink_LinkSynchConfigParams_Init(&gHDSDIDVRUsecaseContext.avsyncCfg[i]);
     }
     MULTICH_INIT_STRUCT(IpcFramesOutLinkRTOS_CreateParams,gHDSDIDVRUsecaseContext.ipcFramesOutVpssPrm);
     MULTICH_INIT_STRUCT(IpcFramesInLinkHLOS_CreateParams,gHDSDIDVRUsecaseContext.ipcFramesInHostPrm);
     for (i = 0; i < OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.selectPrm);i++)
     {
         MULTICH_INIT_STRUCT(SelectLink_CreateParams,gHDSDIDVRUsecaseContext.selectPrm[i]);
     }
     MULTICH_INIT_STRUCT(NsfLink_CreateParams,gHDSDIDVRUsecaseContext.nsfPrm);
     memset(&gHDSDIDVRUsecaseContext.nullSrcPrms,0,sizeof(gHDSDIDVRUsecaseContext.nullSrcPrms));

}

static
Void multich_hdsdidvr_set_select_predei_prm(SelectLink_CreateParams *selectPrm,UInt32 numD1Ch,UInt32 numHDCh,UInt32 D1start,UInt32 HDstart)
{
    Int i;

    selectPrm->numOutQue = PRE_DEI_SELECT_LINK_NUM_OUTQUE;
    OSA_COMPILETIME_ASSERT(PRE_DEI_SELECT_LINK_NUM_OUTQUE == 4);

    gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsD1 = numD1Ch;

    selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_D1_ENABLE_QIDX].numOutCh = numD1Ch;
    selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_D1_ENABLE_QIDX].outQueId = PRE_DEI_SELECT_LINK_DEI_D1_ENABLE_QIDX;
    for (i = 0; i < selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_D1_ENABLE_QIDX].numOutCh;i++)
    {
        selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_D1_ENABLE_QIDX].inChNum[i] = D1start + i;
    }

    selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_D1_DISABLE_QIDX].numOutCh = numD1Ch;
    selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_D1_DISABLE_QIDX].outQueId = PRE_DEI_SELECT_LINK_DEI_D1_DISABLE_QIDX;
    for (i = 0; i < selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_D1_DISABLE_QIDX].numOutCh;i++)
    {
        selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_D1_DISABLE_QIDX].inChNum[i] = D1start + i;
    }

    gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsHD = numHDCh;

    selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX].numOutCh = numHDCh;
    selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX].outQueId = PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX;
    #ifdef MULTICH_HDSDIDVR_SIMULATE_HD_CAPTURE
    HDstart = SIMULATE_HD_CAPTURE_START_CHANNEL_NUM;
    #endif
    for (i = 0; i < selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX].numOutCh;i++)
    {
        selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX].inChNum[i] = HDstart + i;
    }

    selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX].numOutCh = numHDCh;
    selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX].outQueId = PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX;
    for (i = 0; i < selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX].numOutCh;i++)
    {
        selectPrm->outQueChInfo[PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX].inChNum[i] = HDstart + i;
    }
}


static
Void multich_hdsdidvr_set_select_preencode_prm(SelectLink_CreateParams *selectPrm,UInt32 prevLinkId)
{
    System_LinkInfo selectPrevLinkInfo;
    Int32 status;
    Int i;

    status = System_linkGetInfo(prevLinkId,&selectPrevLinkInfo);
    OSA_assert(status == OSA_SOK);
    OSA_assert(selectPrevLinkInfo.numQue == 1);
    selectPrm->numOutQue = PRE_ENCODE_SELECT_LINK_NUM_OUTQUE;
    OSA_COMPILETIME_ASSERT(PRE_ENCODE_SELECT_LINK_NUM_OUTQUE == 1);
    selectPrm->outQueChInfo[0].numOutCh = selectPrevLinkInfo.queInfo[0].numCh/2;
    selectPrm->outQueChInfo[0].outQueId = 0;
    for (i = 0; i < selectPrm->outQueChInfo[0].numOutCh;i++)
    {
        selectPrm->outQueChInfo[0].inChNum[i] = i;
    }
}


static
Void multich_hdsdidvr_set_nullsrc_prm(NullSrcLink_CreateParams *nullSrcPrms)
{
    Int i;
    nullSrcPrms->tilerEnable = FALSE;
    nullSrcPrms->timerPeriod = 16;
    nullSrcPrms->inputInfo.numCh = MAX_NUM_ENCODE_CHANNELS;
    #ifdef MULTICH_HDSDIDVR_SIMULATE_HD_CAPTURE
    nullSrcPrms->inputInfo.numCh++;
    #endif
    for (i = HD_CAPTURE_START_CHANNEL_NUM; i < HD_CAPTURE_START_CHANNEL_NUM + NUM_NONMUX_HD_CAPTURE_CHANNELS; i++)
    {
        nullSrcPrms->inputInfo.chInfo[i].width  = HD_DECODE_CHANNEL_WIDTH;
        nullSrcPrms->inputInfo.chInfo[i].height = HD_DECODE_CHANNEL_HEIGHT;
        nullSrcPrms->inputInfo.chInfo[i].bufType = SYSTEM_BUF_TYPE_VIDFRAME;
        nullSrcPrms->inputInfo.chInfo[i].dataFormat = SYSTEM_DF_YUV420SP_UV;
        nullSrcPrms->inputInfo.chInfo[i].memType    = SYSTEM_MT_NONTILEDMEM;
        nullSrcPrms->inputInfo.chInfo[i].pitch[0]   = nullSrcPrms->inputInfo.chInfo[i].width;
        nullSrcPrms->inputInfo.chInfo[i].pitch[1]   = nullSrcPrms->inputInfo.chInfo[i].width;
        nullSrcPrms->inputInfo.chInfo[i].scanFormat = SYSTEM_SF_PROGRESSIVE;
        nullSrcPrms->inputInfo.chInfo[i].startX = 0;
        nullSrcPrms->inputInfo.chInfo[i].startY = 0;
    }
    for (i = SD_CAPTURE_START_CHANNEL_NUM; i < SD_CAPTURE_START_CHANNEL_NUM + NUM_MUX_SD_CAPTURE_CHANNELS; i++)
    {
        nullSrcPrms->inputInfo.chInfo[i].width  = SD_DECODE_CHANNEL_WIDTH;
        nullSrcPrms->inputInfo.chInfo[i].height = SD_DECODE_CHANNEL_HEIGHT;
        nullSrcPrms->inputInfo.chInfo[i].bufType = SYSTEM_BUF_TYPE_VIDFRAME;
        nullSrcPrms->inputInfo.chInfo[i].dataFormat = SYSTEM_DF_YUV422I_YUYV;
        nullSrcPrms->inputInfo.chInfo[i].memType    = SYSTEM_MT_NONTILEDMEM;
        nullSrcPrms->inputInfo.chInfo[i].pitch[0]   = nullSrcPrms->inputInfo.chInfo[i].width * 2;
        nullSrcPrms->inputInfo.chInfo[i].scanFormat = SYSTEM_SF_INTERLACED;
        nullSrcPrms->inputInfo.chInfo[i].startX = 0;
        nullSrcPrms->inputInfo.chInfo[i].startY = 0;
    }
    for (i = SIMULATE_HD_CAPTURE_START_CHANNEL_NUM; i < SIMULATE_HD_CAPTURE_START_CHANNEL_NUM + NUM_NONMUX_HD_CAPTURE_CHANNELS; i++)
    {
        nullSrcPrms->inputInfo.chInfo[i].width  = HD_DECODE_CHANNEL_WIDTH;
        nullSrcPrms->inputInfo.chInfo[i].height = HD_DECODE_CHANNEL_HEIGHT;
        nullSrcPrms->inputInfo.chInfo[i].bufType = SYSTEM_BUF_TYPE_VIDFRAME;
        nullSrcPrms->inputInfo.chInfo[i].dataFormat = SYSTEM_DF_YUV420SP_UV;
        nullSrcPrms->inputInfo.chInfo[i].memType    = SYSTEM_MT_NONTILEDMEM;
        nullSrcPrms->inputInfo.chInfo[i].pitch[0]   = nullSrcPrms->inputInfo.chInfo[i].width;
        nullSrcPrms->inputInfo.chInfo[i].pitch[1]   = nullSrcPrms->inputInfo.chInfo[i].width;
        nullSrcPrms->inputInfo.chInfo[i].scanFormat = SYSTEM_SF_PROGRESSIVE;
        nullSrcPrms->inputInfo.chInfo[i].startX = 0;
        nullSrcPrms->inputInfo.chInfo[i].startY = 0;
    }
}


static
Void multich_hdsdidvr_set_capture_prm(CaptureLink_CreateParams *capturePrm)
{
    CaptureLink_VipInstParams         *pCaptureInstPrm;
    CaptureLink_OutParams             *pCaptureOutPrm;
    UInt32 vipInstId;

    capturePrm->numVipInst                 = 2;

    capturePrm->tilerEnable                = FALSE;
    capturePrm->numBufsPerCh               = NUM_BUFS_PER_CH_CAPTURE;
    capturePrm->numExtraBufs               = 0;
    capturePrm->fakeHdMode                 = FALSE;
    capturePrm->enableSdCrop               = FALSE;
    capturePrm->doCropInCapture            = FALSE;

    capturePrm->isPalMode = Vcap_isPalMode();

    vipInstId = 0;
    pCaptureInstPrm                     = &capturePrm->vipInst[vipInstId];
    pCaptureInstPrm->vipInstId          = SYSTEM_CAPTURE_INST_VIP0_PORTA;
    pCaptureInstPrm->videoCaptureMode   = SYSTEM_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_EMBEDDED_SYNC;
    pCaptureInstPrm->videoIfMode        = SYSTEM_CAPT_VIDEO_IF_MODE_16BIT;
    #ifdef MULTICH_HDSDIDVR_FAKE_HD_CAPTURE
    /* If not using real HD capture disabled timestamp else capture driver asserts due to wrong fps */
    pCaptureInstPrm->enableTimestampInInterrupt = FALSE;
    #endif
    pCaptureInstPrm->inScanFormat       = SYSTEM_SF_PROGRESSIVE;
    pCaptureInstPrm->videoDecoderId     = 0;
    pCaptureInstPrm->inDataFormat       = SYSTEM_DF_YUV422P;
    pCaptureInstPrm->standard           = SYSTEM_STD_1080P_60;
    pCaptureInstPrm->numOutput          = 1;

    pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];
    pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV420SP_UV;
    pCaptureOutPrm->scEnable            = FALSE;
    pCaptureOutPrm->scOutWidth          = 0;
    pCaptureOutPrm->scOutHeight         = 0;
    pCaptureOutPrm->outQueId            = 0;

    vipInstId = 1;
    pCaptureInstPrm                     = &capturePrm->vipInst[vipInstId];
    pCaptureInstPrm->vipInstId          = SYSTEM_CAPTURE_INST_VIP1_PORTA;
    pCaptureInstPrm->videoDecoderId     = 0;
    pCaptureInstPrm->videoCaptureMode   = SYSTEM_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_PIXEL_MUX_EMBEDDED_SYNC;
    pCaptureInstPrm->videoIfMode        = SYSTEM_CAPT_VIDEO_IF_MODE_8BIT;
    pCaptureInstPrm->inScanFormat       = SYSTEM_SF_INTERLACED;
    pCaptureInstPrm->inDataFormat       = SYSTEM_DF_YUV422P;
    pCaptureInstPrm->standard           = SYSTEM_STD_MUX_4CH_D1;
    pCaptureInstPrm->numOutput          = 1;

    pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];
    pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV422I_YUYV;
    pCaptureOutPrm->scEnable            = FALSE;
    pCaptureOutPrm->scOutWidth          = 0;
    pCaptureOutPrm->scOutHeight         = 0;
    pCaptureOutPrm->outQueId            = 0;

#ifdef MULTICH_HDSDIDVR_SIMULATE_HD_CAPTURE
    capturePrm->numVipInst++;
    gVcapModuleContext.isWrbkCaptEnable = TRUE;
    pCaptureInstPrm                     = &capturePrm->vipInst[2];
    pCaptureInstPrm->enableTimestampInInterrupt = TRUE;
    pCaptureInstPrm->vipInstId          = SYSTEM_CAPTURE_INST_SC5_WB2;
    pCaptureInstPrm->videoDecoderId     = 0;
    pCaptureInstPrm->videoCaptureMode   = SYSTEM_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_EMBEDDED_SYNC;
    pCaptureInstPrm->videoIfMode        = SYSTEM_CAPT_VIDEO_IF_MODE_16BIT;
    pCaptureInstPrm->inScanFormat       = SYSTEM_SF_PROGRESSIVE;
    pCaptureInstPrm->inDataFormat       = SYSTEM_DF_RGB24_888;
    pCaptureInstPrm->standard           = SYSTEM_STD_1080P_60;
    pCaptureInstPrm->numOutput          = 1;

    pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];
    pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV422I_YUYV;
    pCaptureOutPrm->scEnable            = FALSE;
    pCaptureOutPrm->scOutWidth          = HD_DECODE_CHANNEL_WIDTH;
    pCaptureOutPrm->scOutHeight         = HD_DECODE_CHANNEL_HEIGHT;
    pCaptureOutPrm->outQueId            = 0;
#endif

}


//static
Void multich_hdsdidvr_set_ipcframesout2host_prm(IpcFramesOutLinkRTOS_CreateParams *ipcFramesOutPrm)
{
    ipcFramesOutPrm->baseCreateParams.inputFrameRate = MULTICH_HDSDIDVR_GET_CAPTURE_FIELDS_PER_SEC();
    ipcFramesOutPrm->baseCreateParams.outputFrameRate = MULTICH_HDSDIDVR_GET_CAPTURE_FIELDS_PER_SEC();
    ipcFramesOutPrm->baseCreateParams.noNotifyMode    = FALSE;
    ipcFramesOutPrm->baseCreateParams.notifyNextLink  = TRUE;
    ipcFramesOutPrm->baseCreateParams.notifyPrevLink  = TRUE;
    ipcFramesOutPrm->baseCreateParams.notifyProcessLink = TRUE;
}

//static
Void multich_hdsdidvr_set_ipcframesinhost_prm(IpcFramesInLinkHLOS_CreateParams *ipcFramesInPrm)
{
    ipcFramesInPrm->baseCreateParams.inputFrameRate = MULTICH_HDSDIDVR_GET_CAPTURE_FIELDS_PER_SEC();
    ipcFramesInPrm->baseCreateParams.outputFrameRate = MULTICH_HDSDIDVR_GET_CAPTURE_FIELDS_PER_SEC();
    ipcFramesInPrm->baseCreateParams.noNotifyMode    = FALSE;
    ipcFramesInPrm->baseCreateParams.notifyNextLink  = FALSE;
    ipcFramesInPrm->baseCreateParams.notifyPrevLink  = TRUE;
    ipcFramesInPrm->baseCreateParams.notifyProcessLink = FALSE;
    ipcFramesInPrm->exportOnlyPhyAddr                  = TRUE;
    ipcFramesInPrm->cbFxn                              = Vcap_ipcFramesInCbFxn;
    ipcFramesInPrm->cbCtx                              = &gVcapModuleContext;
}

static
UInt32 multich_hdsdidvr_get_videodecoder_device_id()
{
    OSA_I2cHndl i2cHandle;
    Int32 status;
    UInt32 twl_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TW2968_DRV,0);
    UInt32 tvp_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TVP5158_DRV,0);
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;
    UInt32 chipId;
    UInt32 deviceId = ~0u;

    status = OSA_i2cOpen(&i2cHandle, I2C_DEFAULT_INST_ID);
    OSA_assert(status==0);

    numRegs = 0;
    regAddr[numRegs] = DEVICE_TW2968_REG_DEVICE_ID;
    regValue[numRegs] = 0;
    numRegs++;

    regAddr[numRegs] = DEVICE_TW2968_REG_REV_ID;
    regValue[numRegs] = 0;
    numRegs++;

    status = OSA_i2cRead8(&i2cHandle,twl_i2c_addr,regAddr,regValue,numRegs);
    if (status == 0)
    {
        chipId =  (((UInt32) (regValue[0]>>6)&0x3) << 5) | ( (regValue[1] >> 3) & 0x1F ) ;
        printf("\nTWL_CHIP_ID_READ:0x%x\n",chipId);
        if (chipId == DEVICE_TW2968_CHIP_ID)
        {
            deviceId = DEVICE_VID_DEC_TW2968_DRV;
        }
    }
    if (deviceId == ~0u)
    {
        numRegs = 0;
        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_MSB;
        regValue[numRegs] = 0;
        numRegs++;

        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_LSB;
        regValue[numRegs] = 0;
        numRegs++;

        status = OSA_i2cRead8(&i2cHandle,tvp_i2c_addr,regAddr,regValue,numRegs);
        if (status == 0)
        {
            chipId = ( ( UInt32 ) regValue[0] << 8 ) | regValue[1];
            printf("\nTVP_CHIP_ID_READ:0x%x\n",chipId);
            if (DEVICE_TVP5158_CHIP_ID == chipId)
            {
                deviceId = DEVICE_VID_DEC_TVP5158_DRV;
            }
        }
    }
    OSA_assert(deviceId != ~0u);
    status = OSA_i2cClose(&i2cHandle);
    OSA_assert(status==0);
    return deviceId;
}

static
Void multich_hdsdidvr_configure_extvideodecoder_prm()
{
    int i;
    VCAP_VIDEO_SOURCE_STATUS_S vidSourceStatus;
    UInt32 deviceId;
    UInt32 numCaptureDevices;

    deviceId = multich_hdsdidvr_get_videodecoder_device_id();
    if (deviceId == DEVICE_VID_DEC_TW2968_DRV)
    {
        numCaptureDevices = 2;
    }
    else
    {
        numCaptureDevices = 4;
    }

    for(i = 0; i < numCaptureDevices; i++)
    {
        gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].deviceId         = deviceId;
        if(deviceId == DEVICE_VID_DEC_TW2968_DRV)
        {
            /* consider first device connected to VIP0 Port A and second device connected to VIP1 PortA */
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i*2;
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].numChInDevice    = 8;
        }
        else
        {
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i;
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].numChInDevice    = 4;
        }
        if ((gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].vipInstId        == SYSTEM_CAPTURE_INST_VIP0_PORTA)
            ||
            (gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].vipInstId        == SYSTEM_CAPTURE_INST_VIP0_PORTB))
        {
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoIfMode        = DEVICE_CAPT_VIDEO_IF_MODE_16BIT;
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoDataFormat    = SYSTEM_DF_YUV422P;
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.standard           = SYSTEM_STD_1080P_60;
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
                                    DEVICE_CAPT_VIDEO_CAPTURE_MODE_SINGLE_CH_NON_MUX_DISCRETE_SYNC_HSYNC_VSYNC;
        }
        else
        {
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoIfMode        = DEVICE_CAPT_VIDEO_IF_MODE_8BIT;
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoDataFormat    = SYSTEM_DF_YUV422P;
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.standard           = SYSTEM_STD_MUX_4CH_D1;
            gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
                        DEVICE_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_PIXEL_MUX_EMBEDDED_SYNC;
        }
        gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoSystem        =
                                      DEVICE_VIDEO_DECODER_VIDEO_SYSTEM_AUTO_DETECT;
        gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCropEnable    = FALSE;
        gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoAutoDetectTimeout = -1;
    }
    Vcap_configVideoDecoder(&gHDSDIDVRUsecaseContext.vidDecVideoModeArgs[0],
                            numCaptureDevices);
    Vcap_getVideoSourceStatus(&vidSourceStatus);
    Vcap_setVideoSourceStatus(&vidSourceStatus);
}

static
Void multich_hdsdidvr_set_dei_prm(DeiLink_CreateParams *deiPrm,Bool bypassDei,Bool interlacedBypass,UInt32 outQueId)
{
    int deiChIdx;

    deiPrm->numBufsPerCh[outQueId] = NUM_BUFS_PER_CH_DEI;

    deiPrm->outScaleFactor[outQueId][0].scaleMode = DEI_SCALE_MODE_RATIO;
    deiPrm->outScaleFactor[outQueId][0].ratio.heightRatio.numerator = 1;
    deiPrm->outScaleFactor[outQueId][0].ratio.heightRatio.denominator = 1;
    deiPrm->outScaleFactor[outQueId][0].ratio.widthRatio.numerator = 1;
    deiPrm->outScaleFactor[outQueId][0].ratio.widthRatio.denominator = 1;
    for (deiChIdx = 1; deiChIdx < DEI_LINK_MAX_CH; deiChIdx++)
    {
        deiPrm->outScaleFactor[outQueId][deiChIdx] =
            deiPrm->outScaleFactor[outQueId][0];
    }
    deiPrm->enableOut[DEI_LINK_OUT_QUE_DEI_SC]                        = FALSE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_VIP_SC]                        = FALSE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT]          = FALSE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_DEI_SC_SECONDARY_OUT]          = FALSE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_DEI_SC_TERTIARY_OUT]           = FALSE;
    deiPrm->enableOut[outQueId]                           = TRUE;
    deiPrm->tilerEnable[outQueId]                         = TILER_ENABLE_ENCODE;
    deiPrm->comprEnable                                   = FALSE;
    deiPrm->setVipScYuv422Format                          = FALSE;
    deiPrm->enableInputFrameRateUpscale                   = FALSE;
    if (bypassDei)
    {
        deiPrm->enableDeiForceBypass                          = TRUE;
        deiPrm->interlacedBypassMode                          = interlacedBypass;
    }
    else
    {
        deiPrm->enableDeiForceBypass                          = FALSE;
        deiPrm->interlacedBypassMode                          = FALSE;
    }
}

static
Void multich_hdsdidvr_set_nsf_prm(NsfLink_CreateParams * nsfPrm)
{
    nsfPrm->bypassNsf = TRUE;
    nsfPrm->tilerEnable = TILER_ENABLE_ENCODE;
    nsfPrm->numBufsPerCh = NUM_BUFS_PER_CH_NSF;
    nsfPrm->numOutQue    = 2;
}



static
Void multich_hdsdidvr_set_enclink_prm(EncLink_CreateParams *encPrm)
{
    int i;
    EncLink_ChCreateParams *pLinkChPrm;
    EncLink_ChDynamicParams *pLinkDynPrm;
    VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
    VENC_CHN_PARAMS_S *pChPrm;

    encPrm->numBufPerCh[ENC_LINK_SD_STREAM_POOL_ID] = NUM_BUFS_PER_CH_BITSOUT_SD;
    encPrm->numBufPerCh[ENC_LINK_HD_STREAM_POOL_ID] = NUM_BUFS_PER_CH_BITSOUT_HD;
    /* Primary Stream Params - D1 */
    for (i=0; i < gVencModuleContext.vencConfig.numPrimaryChn; i++)
    {
        pLinkChPrm  = &encPrm->chCreateParams[i];
        pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

        pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
        pDynPrm     = &pChPrm->dynamicParam;

        OSA_assert(i < OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap));
        if (IVIDEO_H264HP == gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[i])
        {
            pLinkChPrm->format                  = IVIDEO_H264HP;
            pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
            pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
            pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
            pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
            pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
            pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
            pLinkChPrm->rateControlPreset       = pChPrm->rcType;
            pLinkChPrm->enableHighSpeed         = FALSE;
            pLinkChPrm->numTemporalLayer        = VENC_TEMPORAL_LAYERS_1;
            pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;

            pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
            pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
            pLinkDynPrm->interFrameInterval     = 1;
            pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
            pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
            pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
            pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
            pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
            pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
            pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
            pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
        }
        else
        {
            if(IVIDEO_MJPEG == gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[i])
            {
                pLinkChPrm->format                 = IVIDEO_MJPEG;
                pLinkChPrm->profile                = 0;
                pLinkChPrm->dataLayout             = VCODEC_FIELD_SEPARATED;
                pLinkChPrm->fieldMergeEncodeEnable = FALSE;
                pLinkChPrm->enableAnalyticinfo     = 0;
                pLinkChPrm->enableWaterMarking     = 0;
                pLinkChPrm->maxBitRate             = 0;
                pLinkChPrm->encodingPreset         = 0;
                pLinkChPrm->rateControlPreset      = 0;
                pLinkChPrm->enableSVCExtensionFlag = 0;
                pLinkChPrm->numTemporalLayer       = 0;

                pLinkDynPrm->intraFrameInterval    = 0;
                pLinkDynPrm->targetBitRate         = 100*1000*30;
                pLinkDynPrm->interFrameInterval    = 0;
                pLinkDynPrm->mvAccuracy            = 0;
                pLinkDynPrm->inputFrameRate        = pDynPrm->inputFrameRate;
                pLinkDynPrm->qpMin                 = 0;
                pLinkDynPrm->qpMax                 = 0;
                pLinkDynPrm->qpInit                = -1;
                pLinkDynPrm->vbrDuration           = 0;
                pLinkDynPrm->vbrSensitivity        = 0;
            }
            else
            {
                OSA_assert(IVIDEO_MPEG4SP == gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[i]);

                pLinkChPrm->format                  = IVIDEO_MPEG4SP;
                pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
                pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
                pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
                pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
                pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
                pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
                pLinkChPrm->rateControlPreset       = pChPrm->rcType;
                pLinkChPrm->enableHighSpeed         = FALSE;
                pLinkChPrm->numTemporalLayer        = VENC_TEMPORAL_LAYERS_1;
                pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;

                pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
                pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
                pLinkDynPrm->interFrameInterval     = 1;
                pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
                pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
                pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
                pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
                pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
                pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
                pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
                pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;

            }
        }
    }
    for (i=gVencModuleContext.vencConfig.numPrimaryChn; i < gVencModuleContext.vencConfig.numPrimaryChn + gVencModuleContext.vencConfig.numSecondaryChn; i++)
    {

        pLinkChPrm  = &encPrm->chCreateParams[i];
        pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

        pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
        pDynPrm     = &pChPrm->dynamicParam;

        OSA_assert(i < OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap));
        if (IVIDEO_H264HP == gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[i])
        {
            pLinkChPrm->format                  = IVIDEO_H264HP;
            pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
            pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
            pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
            pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
            pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
            pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
            pLinkChPrm->rateControlPreset       = pChPrm->rcType;
            pLinkChPrm->enableHighSpeed         = FALSE;
            pLinkChPrm->numTemporalLayer        = VENC_TEMPORAL_LAYERS_1;
            pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;

            pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
            pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
            pLinkDynPrm->interFrameInterval     = 1;
            pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
            pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
            pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
            pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
            pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
            pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
            pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
            pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
        }
        else
        {
            if(IVIDEO_MJPEG == gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[i])
            {
                pLinkChPrm->format                 = IVIDEO_MJPEG;
                pLinkChPrm->profile                = 0;
                pLinkChPrm->dataLayout             = VCODEC_FIELD_SEPARATED;
                pLinkChPrm->fieldMergeEncodeEnable = FALSE;
                pLinkChPrm->enableAnalyticinfo     = 0;
                pLinkChPrm->enableWaterMarking     = 0;
                pLinkChPrm->maxBitRate             = 0;
                pLinkChPrm->encodingPreset         = 0;
                pLinkChPrm->rateControlPreset      = 0;
                pLinkChPrm->enableSVCExtensionFlag = 0;
                pLinkChPrm->numTemporalLayer       = 0;

                pLinkDynPrm->intraFrameInterval    = 0;
                pLinkDynPrm->targetBitRate         = 100*1000*30;
                pLinkDynPrm->interFrameInterval    = 0;
                pLinkDynPrm->mvAccuracy            = 0;
                pLinkDynPrm->inputFrameRate        = pDynPrm->inputFrameRate;
                pLinkDynPrm->qpMin                 = 0;
                pLinkDynPrm->qpMax                 = 0;
                pLinkDynPrm->qpInit                = -1;
                pLinkDynPrm->vbrDuration           = 0;
                pLinkDynPrm->vbrSensitivity        = 0;
            }
            else
            {
                OSA_assert(IVIDEO_MPEG4SP == gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[i]);

                pLinkChPrm->format                  = IVIDEO_MPEG4SP;
                pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
                pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
                pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
                pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
                pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
                pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
                pLinkChPrm->rateControlPreset       = pChPrm->rcType;
                pLinkChPrm->enableHighSpeed         = FALSE;
                pLinkChPrm->numTemporalLayer        = VENC_TEMPORAL_LAYERS_1;
                pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;

                pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
                pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
                pLinkDynPrm->interFrameInterval     = 1;
                pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
                pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
                pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
                pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
                pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
                pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
                pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
                pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
            }
        }
    }
}


static
Void multich_hdsdidvr_set_swms_singlewin_layoutprm(SwMsLink_LayoutPrm *layoutInfo,VSYS_VIDEO_STANDARD_E outRes)
{
    SwMsLink_LayoutWinInfo *winInfo;
    UInt32 outWidth, outHeight, winId, widthAlign, heightAlign;

    MultiCh_swMsGetOutSize(outRes, &outWidth, &outHeight);
    widthAlign = 8;
    heightAlign = 1;

    /* init to known default */
    memset(layoutInfo, 0, sizeof(*layoutInfo));

    layoutInfo->onlyCh2WinMapChanged = FALSE;
    layoutInfo->outputFPS = MULTICH_HDSDIDVR_GET_CAPTURE_FRAMES_PER_SEC();
    layoutInfo->numWin = 1;
    winId = 0;

    winInfo = &layoutInfo->winInfo[winId];

    winInfo->width  = SystemUtils_floor(outWidth, widthAlign);
    winInfo->height = SystemUtils_floor(outHeight, heightAlign);
    winInfo->startX = 0;
    winInfo->startY = 0;
    winInfo->bypass = FALSE;
    winInfo->channelNum = 0 + winId;
}


static
Void multich_hdsdidvr_set_swms_prm(SwMsLink_CreateParams *swMsPrm,
                                    UInt32 swMsIdx)
{
    UInt32 devId;

    swMsPrm->numSwMsInst = 1;
    if (swMsIdx == 1)
    {
        swMsPrm->swMsInstId[0] = SYSTEM_SW_MS_SC_INST_DEI_SC; 
    }
    else
    {
        swMsPrm->swMsInstId[0] = SYSTEM_SW_MS_SC_INST_VIP0_DEI_SC; 
    }

    swMsPrm->maxInputQueLen            = SYSTEM_SW_MS_DEFAULT_INPUT_QUE_LEN + 6;
    swMsPrm->numOutBuf = NUM_BUFS_PER_CH_SWMS_HD;
    swMsPrm->enableProcessTieWithDisplay = TRUE;
    swMsPrm->enableOuputDup = TRUE;
    swMsPrm->outDataFormat  = SYSTEM_DF_YUV422I_YUYV;
    swMsPrm->maxOutRes  = VSYS_STD_1080P_60;
    if (0 == swMsIdx)
    {
        devId               = VDIS_DEV_HDMI;
    }
    else
    {
        devId               = VDIS_DEV_DVO2;
    }
    swMsPrm->initOutRes = gVdisModuleContext.vdisConfig.deviceParams[devId].resolution;
    swMsPrm->numOutBuf  = NUM_BUFS_PER_CH_SWMS_HD + 2;
    swMsPrm->lineSkipMode = FALSE;
    swMsPrm->enableLayoutGridDraw = gVdisModuleContext.vdisConfig.enableLayoutGridDraw;
    multich_hdsdidvr_set_swms_singlewin_layoutprm(&swMsPrm->layoutPrm,swMsPrm->initOutRes);
}

static
Void mulich_hdsdidvr_set_avsync_vidque_prm(Avsync_SynchConfigParams *queCfg,
                                            Int chnum,
                                            UInt32 avsStartChNum,
                                            UInt32 avsEndChNum)
{
    queCfg->chNum = chnum;
    queCfg->audioPresent = FALSE;
    if ((queCfg->chNum >= avsStartChNum)
        &&
        (queCfg->chNum <= avsEndChNum)
        &&
        (gVsysModuleContext.vsysConfig.enableAVsync))
    {
        queCfg->avsyncEnable = TRUE;
    }
    else
    {
        queCfg->avsyncEnable = FALSE;
    }
    queCfg->clkAdjustPolicy.refClkType = AVSYNC_REFCLKADJUST_NONE;
    queCfg->playTimerStartTimeout = 0;
    queCfg->playStartMode = AVSYNC_PLAYBACK_START_MODE_WAITSYNCH;
    queCfg->ptsInitMode   = AVSYNC_PTS_INIT_MODE_AUTO;
    queCfg->clkAdjustPolicy.clkAdjustLead = AVSYNC_VIDEO_TIMEBASESHIFT_MAX_LEAD_MS;
    queCfg->clkAdjustPolicy.clkAdjustLag = AVSYNC_VIDEO_TIMEBASESHIFT_MAX_LAG_MS;
    queCfg->vidSynchPolicy.playMaxLag    = 200;
}

static
Void mulich_hdsdidvr_set_avsync_prm(AvsyncLink_LinkSynchConfigParams *avsyncPrm,
                                     UInt32 swMsIdx,
                                     UInt32 prevLinkID,
                                     UInt32 prevLinkQueId)
{
    System_LinkInfo                   swmsInLinkInfo;
    Int i;
    Int32 status;

    if (0 == swMsIdx)
    {
        Vdis_getAvsyncConfig(VDIS_DEV_HDMI,avsyncPrm);
        avsyncPrm->displayLinkID        = Vdis_getDisplayId(VDIS_DEV_HDMI);
        avsyncPrm->videoSynchLinkID = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)];
    }
    else
    {
        Vdis_getAvsyncConfig(VDIS_DEV_DVO2,avsyncPrm);
        avsyncPrm->displayLinkID        = Vdis_getDisplayId(VDIS_DEV_DVO2);
        avsyncPrm->videoSynchLinkID = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)];
    }
    System_linkGetInfo(prevLinkID,&swmsInLinkInfo);
    OSA_assert(swmsInLinkInfo.numQue > prevLinkQueId);

    avsyncPrm->numCh            = swmsInLinkInfo.queInfo[prevLinkQueId].numCh;
    avsyncPrm->syncMasterChnum = AVSYNC_INVALID_CHNUM;
    for (i = 0; i < avsyncPrm->numCh;i++)
    {
        mulich_hdsdidvr_set_avsync_vidque_prm(&avsyncPrm->queCfg[i],
                                               i,
                                               MAX_NUM_ENCODE_CHANNELS,
                                               swmsInLinkInfo.queInfo[prevLinkQueId].numCh);
    }
    if (0 == swMsIdx)
    {
        Vdis_setAvsyncConfig(VDIS_DEV_HDMI,avsyncPrm);
    }
    else
    {
        Vdis_setAvsyncConfig(VDIS_DEV_DVO2,avsyncPrm);
    }

    status = Avsync_configSyncConfigInfo(avsyncPrm);
    OSA_assert(status == 0);

}


static
Void multich_hdsdidvr_set_declink_prms(DecLink_CreateParams *decPrm)
{
    int i;


    gVdecModuleContext.vdecConfig.numChn = (NUM_SD_DECODE_CHANNELS + NUM_HD_DECODE_CHANNELS);
    for (i=0; i<gVdecModuleContext.vdecConfig.numChn; i++)
    {
        OSA_assert(i < OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap));
        decPrm->chCreateParams[i].format                 = gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[i];
        decPrm->chCreateParams[i].profile                = IH264VDEC_PROFILE_ANY;
        decPrm->chCreateParams[i].enableWaterMarking     = 0;
        OSA_assert(gHDSDIDVRUsecaseContext.ipcBitsInLinkInfo.numQue == 1);
        if (SYSTEM_SF_INTERLACED == gHDSDIDVRUsecaseContext.ipcBitsInLinkInfo.queInfo[0].chInfo[i].scanFormat)
        {
            decPrm->chCreateParams[i].processCallLevel       = VDEC_FIELDLEVELPROCESSCALL;
        }
        else
        {
            decPrm->chCreateParams[i].processCallLevel       = VDEC_FRAMELEVELPROCESSCALL;
        }
        if ((i >= SD_DECODE_START_CH) && (i < (SD_DECODE_START_CH + NUM_SD_DECODE_CHANNELS)))
        {
            decPrm->chCreateParams[i].targetMaxWidth         = SD_DECODE_CHANNEL_WIDTH;
            decPrm->chCreateParams[i].targetMaxHeight        = SD_DECODE_CHANNEL_HEIGHT;
            decPrm->chCreateParams[i].numBufPerCh            = NUM_BUFS_PER_CH_DEC_SD;
            decPrm->chCreateParams[i].tilerEnable = TILER_ENABLE_DECODE_SD;
        }
        else
        {
            decPrm->chCreateParams[i].targetMaxWidth         = HD_DECODE_CHANNEL_WIDTH;
            decPrm->chCreateParams[i].targetMaxHeight        = HD_DECODE_CHANNEL_HEIGHT;
            decPrm->chCreateParams[i].numBufPerCh            = NUM_BUFS_PER_CH_DEC_HD;
            decPrm->chCreateParams[i].tilerEnable = TILER_ENABLE_DECODE_HD;
        }
        decPrm->chCreateParams[i].defaultDynamicParams.targetFrameRate = gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.frameRate;
        decPrm->chCreateParams[i].defaultDynamicParams.targetBitRate   = gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.targetBitRate;
    }
}

static
Void multich_hdsdidvr_set_ipcbitsout_hlos_prms(IpcBitsOutLinkHLOS_CreateParams * ipcBitsOutHostPrm,UInt32 ipcBitsInLinkId)
{
    int i;
    Int32 status;
    UInt32 numBufPerCh;

    status = System_linkGetInfo(ipcBitsInLinkId,&gHDSDIDVRUsecaseContext.ipcBitsInLinkInfo);
    OSA_assert(status == OSA_SOK);
    OSA_assert(gHDSDIDVRUsecaseContext.ipcBitsInLinkInfo.numQue == 1);


    for (i = 0;
         i < gHDSDIDVRUsecaseContext.ipcBitsInLinkInfo.queInfo[0].numCh;
         i++)
    {
        System_LinkChInfo *pChInfo;

        pChInfo = &ipcBitsOutHostPrm->inQueInfo.chInfo[i];

        pChInfo->bufType        = 0; // NOT USED
        pChInfo->codingformat   = 0; // NOT USED
        pChInfo->dataFormat     = 0; // NOT USED
        pChInfo->memType        = 0; // NOT USED
        pChInfo->startX         = 0; // NOT USED
        pChInfo->startY         = 0; // NOT USED
        pChInfo->width          = gHDSDIDVRUsecaseContext.ipcBitsInLinkInfo.queInfo[0].chInfo[i].width;
        pChInfo->height         = gHDSDIDVRUsecaseContext.ipcBitsInLinkInfo.queInfo[0].chInfo[i].height;
        pChInfo->pitch[0]       = 0; // NOT USED
        pChInfo->pitch[1]       = 0; // NOT USED
        pChInfo->pitch[2]       = 0; // NOT USED
        pChInfo->scanFormat     = gHDSDIDVRUsecaseContext.ipcBitsInLinkInfo.queInfo[0].chInfo[i].scanFormat;
        ipcBitsOutHostPrm->maxQueueDepth[i] =
            MAX_BUFFERING_QUEUE_LEN_PER_CH;
        ipcBitsOutHostPrm->chMaxReqBufSize[i] =
                gHDSDIDVRUsecaseContext.ipcBitsInLinkInfo.queInfo[0].chInfo[i].width * gHDSDIDVRUsecaseContext.ipcBitsInLinkInfo.queInfo[0].chInfo[i].height;
        if ((i >= SD_DECODE_START_CH) && (i < (SD_DECODE_START_CH + NUM_SD_DECODE_CHANNELS)))
        {
            numBufPerCh = NUM_BUFS_PER_CH_BITSOUT_SD;
        }
        else
        {
            numBufPerCh = NUM_BUFS_PER_CH_BITSOUT_HD;
        }
        ipcBitsOutHostPrm->totalBitStreamBufferSize[i] =
                ipcBitsOutHostPrm->chMaxReqBufSize[i] *  numBufPerCh;
    }
    ipcBitsOutHostPrm->baseCreateParams.noNotifyMode = FALSE;
    ipcBitsOutHostPrm->baseCreateParams.notifyNextLink = TRUE;
    ipcBitsOutHostPrm->baseCreateParams.numOutQue = 1;
    ipcBitsOutHostPrm->inQueInfo.numCh =
        (NUM_SD_DECODE_CHANNELS + NUM_HD_DECODE_CHANNELS);
}

static
Void multich_hdsdidvr_set_display_prms(DisplayLink_CreateParams *displayPrm,
                                        UInt32 maxOutRes)
{
    displayPrm->displayRes = maxOutRes;
}



static
Void multich_hdsdidvr_set_link_ids()
{
    int    i;

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_CAPTURE;
    gVcapModuleContext.ipcFramesOutVpssToHostId = SYSTEM_VPSS_LINK_ID_IPC_FRAMES_OUT_0;
    gVcapModuleContext.ipcFramesInHostId = SYSTEM_HOST_LINK_ID_IPC_FRAMES_IN_0;
    gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIENABLED]     = SYSTEM_LINK_ID_DEI_0;
    gVcapModuleContext.deiId[DEI_LINK_IDX_HD_DEIENABLED]     = SYSTEM_LINK_ID_DEI_HQ_0;
    gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIDISABLED]     = SYSTEM_LINK_ID_DEI_1;
    gVcapModuleContext.deiId[DEI_LINK_IDX_HD_DEIDISABLED]     = SYSTEM_LINK_ID_DEI_HQ_1;

    for (i = 0; i < NUM_MERGE_LINK;i++)
    {
        gHDSDIDVRUsecaseContext.mergeId[i] = SYSTEM_VPSS_LINK_ID_MERGE_0 + i;
    }
    for (i = 0; i < NUM_DUP_LINK;i++)
    {
        gHDSDIDVRUsecaseContext.dupId[i] = SYSTEM_VPSS_LINK_ID_DUP_0 + i;
    }

    gVencModuleContext.encId        = SYSTEM_LINK_ID_VENC_0;
    gVdecModuleContext.decId        = SYSTEM_LINK_ID_VDEC_0;

    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_0;
    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_1;

    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)] = SYSTEM_LINK_ID_DISPLAY_0; /* ON CHIP HDMI */
    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)] = SYSTEM_LINK_ID_DISPLAY_1; /* SD HDMI */

    gHDSDIDVRUsecaseContext.ipcOutVpssId = SYSTEM_VPSS_LINK_ID_IPC_OUT_M3_0;
    gHDSDIDVRUsecaseContext.ipcInVideoId = SYSTEM_VIDEO_LINK_ID_IPC_IN_M3_0;
    gHDSDIDVRUsecaseContext.ipcOutVideoId= SYSTEM_VIDEO_LINK_ID_IPC_OUT_M3_0;
    gHDSDIDVRUsecaseContext.ipcInVpssId  = SYSTEM_VPSS_LINK_ID_IPC_IN_M3_0;
    for (i = 0; i < NUM_SELECT_LINK;i++)
    {
        gHDSDIDVRUsecaseContext.selectId[i]     = SYSTEM_VPSS_LINK_ID_SELECT_0 + i;
    }
    gVencModuleContext.ipcBitsOutRTOSId  = SYSTEM_VIDEO_LINK_ID_IPC_BITS_OUT_0;
    gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;
    gVdecModuleContext.ipcBitsOutHLOSId  = SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0;
    gVdecModuleContext.ipcBitsInRTOSId  = SYSTEM_VIDEO_LINK_ID_IPC_BITS_IN_0;
    gVcapModuleContext.nsfId[0] = SYSTEM_LINK_ID_NSF_0;

    gVdisModuleContext.setMosaicFxn     = MultiCh_hdsdiDVRSetMosaicParams;
    gVcapModuleContext.setWrbkCaptScParamsFxn = multich_hdsdidvr_set_wrbk_capture_prms;
    gVcapModuleContext.startWrbkCaptFxn = multich_hdsdidvr_start_wrbk_capture;
    gVcapModuleContext.stopWrbkCaptFxn  = multich_hdsdidvr_stop_wrbk_capture;
    gHDSDIDVRUsecaseContext.nullSrcLinkId = SYSTEM_VPSS_LINK_ID_NULL_SRC_0;
}

static
Void multich_hdsdidvr_reset_link_ids()
{
    int    i;

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.ipcFramesOutVpssToHostId = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.ipcFramesInHostId = SYSTEM_LINK_ID_INVALID;

    gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIENABLED]     = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.deiId[DEI_LINK_IDX_HD_DEIENABLED]     = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIDISABLED]     = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIDISABLED]     = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.sclrId[0]    =  SYSTEM_LINK_ID_INVALID;
    for (i = 0; i < NUM_MERGE_LINK;i++)
    {
        gHDSDIDVRUsecaseContext.mergeId[i] = SYSTEM_LINK_ID_INVALID;
    }
    for (i = 0; i < NUM_DUP_LINK;i++)
    {
        gHDSDIDVRUsecaseContext.dupId[i] = SYSTEM_LINK_ID_INVALID;
    }

    gVencModuleContext.encId        = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.decId        = SYSTEM_LINK_ID_INVALID;

    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]      = SYSTEM_LINK_ID_INVALID;
    gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)]      = SYSTEM_LINK_ID_INVALID;

    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)] = SYSTEM_LINK_ID_INVALID; /* ON CHIP HDMI */
    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)] = SYSTEM_LINK_ID_INVALID; /* SD HDMI */

    gHDSDIDVRUsecaseContext.ipcOutVpssId = SYSTEM_LINK_ID_INVALID;
    gHDSDIDVRUsecaseContext.ipcInVideoId = SYSTEM_LINK_ID_INVALID;
    gHDSDIDVRUsecaseContext.ipcOutVideoId= SYSTEM_LINK_ID_INVALID;
    gHDSDIDVRUsecaseContext.ipcInVpssId  = SYSTEM_LINK_ID_INVALID;
    for (i = 0; i < NUM_SELECT_LINK;i++)
    {
        gHDSDIDVRUsecaseContext.selectId[i]     = SYSTEM_LINK_ID_INVALID;
    }
    gVencModuleContext.ipcBitsOutRTOSId  = SYSTEM_LINK_ID_INVALID;
    gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.ipcBitsOutHLOSId  = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.ipcBitsInRTOSId  = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.nsfId[0]            = SYSTEM_LINK_ID_INVALID;
    gVdisModuleContext.setMosaicFxn     = NULL;
    gVcapModuleContext.setWrbkCaptScParamsFxn = NULL;
    gVcapModuleContext.startWrbkCaptFxn = NULL;
    gVcapModuleContext.stopWrbkCaptFxn  = NULL;
    gHDSDIDVRUsecaseContext.nullSrcLinkId = SYSTEM_LINK_ID_INVALID;
}

static
Void multich_hdsdidvr_set_capture_fps(UInt32 *captureFps)
{
    Bool isPal = Vcap_isPalMode();

    if (isPal)
    {
        *captureFps = 50;
    }
    else
    {
        *captureFps = 60;
    }
}


static
Void multich_hdsdidvr_set_links_framerate()
{
    Int32 status;
    DeiLink_ChFpsParams params;
    UInt32 chId,deiId;

    for (deiId = 0; deiId < NUM_DEI_LINK; deiId++)
    {
        for (chId = 0; chId < gVcapModuleContext.vcapConfig.numChn;chId++)
        {
            /* Capture -> Dei */
            params.chId = chId;

            /* Stream 0 -VIP_SC_OUT_PRIMARY is inputfps/2 */
            if ((deiId == DEI_LINK_IDX_D1_DEIENABLED) || (deiId == DEI_LINK_IDX_D1_DEIDISABLED))
            {
                params.streamId = DEI_LINK_OUT_QUE_DEI_SC;
            }
            else
            {
                params.streamId = DEI_LINK_OUT_QUE_VIP_SC;
            }
            params.inputFrameRate  = MULTICH_HDSDIDVR_GET_CAPTURE_FIELDS_PER_SEC();
            if ((deiId == DEI_LINK_IDX_D1_DEIENABLED) || (deiId == DEI_LINK_IDX_HD_DEIENABLED))
            {
                params.outputFrameRate = MULTICH_HDSDIDVR_GET_CAPTURE_FRAMES_PER_SEC();
            }
            else
            {
                params.outputFrameRate = MULTICH_HDSDIDVR_GET_CAPTURE_FIELDS_PER_SEC();
            }
            status = System_linkControl(gVcapModuleContext.deiId[deiId], DEI_LINK_CMD_SET_FRAME_RATE,
                                        &params, sizeof(params), TRUE);
        }
    }
}

static
Void multich_hdsdidvr_set_hd_capture_info()
{
    gHDSDIDVRUsecaseContext.isProgressiveHDCapture = TRUE;
}

static
Int32 multich_hdsdidvr_set_wrbk_capture_prms(UInt32 inW, UInt32 inH, UInt32 outW, UInt32 outH)
{
    Int32 status = 0;
    CaptureLink_ScParams scPrms;

    printf("\nMULTICH_HD_SDI_DVR: %s:%d",__func__,__LINE__);
    if ((inW != outW) || (inH != outH))
    {
        scPrms.queId = 0;
        scPrms.chId = SIMULATE_HD_CAPTURE_START_CHANNEL_NUM;
        scPrms.scEnable = TRUE;
        scPrms.inWidth = inW;
        scPrms.inHeight = inH;
        scPrms.outWidth = outW;
        scPrms.outHeight = outH;

        if ((gVcapModuleContext.captureId != SYSTEM_LINK_ID_INVALID) &&
            (TRUE == gVcapModuleContext.isWrbkCaptEnable))
        {
            status = System_linkControl(
                        gVcapModuleContext.captureId,
                        CAPTURE_LINK_CMD_SET_SC_PARAMS,
                        &scPrms,
                        sizeof(scPrms),
                        TRUE);
            OSA_assert(status == 0);
        }
    }
    return status;
}

static
Int32 multich_hdsdidvr_start_wrbk_capture()
{
    Int32 status = 0;
    CaptureLink_CtrlWrbkCapt wrbkCapt;

    /* Assumption Write back capture is on Queue ID 1 and there is only one channel */
    wrbkCapt.queId = 0;
    wrbkCapt.chId = SIMULATE_HD_CAPTURE_START_CHANNEL_NUM;
    wrbkCapt.enable = TRUE;

    if ((gVcapModuleContext.captureId != SYSTEM_LINK_ID_INVALID) &&
        (TRUE == gVcapModuleContext.isWrbkCaptEnable))
    {
        status = System_linkControl(
                    gVcapModuleContext.captureId,
                    CAPTURE_LINK_CMD_ENABLE_WRBK_CAPT,
                    &wrbkCapt,
                    sizeof(wrbkCapt),
                    TRUE);
    }

    return (status);
}

static
Int32 multich_hdsdidvr_stop_wrbk_capture()
{
    Int32 status = 0;
    CaptureLink_CtrlWrbkCapt wrbkCapt;

    /* Assumption Write back capture is on Queue ID 1 and there is only one channel */
    wrbkCapt.queId = 0;
    wrbkCapt.chId = SIMULATE_HD_CAPTURE_START_CHANNEL_NUM;
    wrbkCapt.enable = FALSE;

    if ((gVcapModuleContext.captureId != SYSTEM_LINK_ID_INVALID) &&
        (TRUE == gVcapModuleContext.isWrbkCaptEnable))
    {
        status = System_linkControl(
                    gVcapModuleContext.captureId,
                    CAPTURE_LINK_CMD_ENABLE_WRBK_CAPT,
                    &wrbkCapt,
                    sizeof(wrbkCapt),
                    TRUE);
    }

    return (status);
}

static
Void multich_hdsdidvr_connect_links()
{
    multich_hdsdidvr_configure_extvideodecoder_prm();
    /**Capture Link**/
    multich_hdsdidvr_set_capture_prm(&gHDSDIDVRUsecaseContext.capturePrm);

    /* Capture ---Q0---> SelectPreDei */
    gHDSDIDVRUsecaseContext.capturePrm.outQueParams[0].nextLink   = gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX];
    gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX].inQueParams.prevLinkId = gVcapModuleContext.captureId;
    gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX].inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gVcapModuleContext.captureId,&gHDSDIDVRUsecaseContext.capturePrm,sizeof(gHDSDIDVRUsecaseContext.capturePrm));
    #ifdef MULTICH_HDSDIDVR_SIMULATE_HD_CAPTURE
    {
        multich_hdsdidvr_set_wrbk_capture_prms(HD_DECODE_CHANNEL_WIDTH, HD_DECODE_CHANNEL_HEIGHT, HD_DECODE_CHANNEL_WIDTH, HD_DECODE_CHANNEL_HEIGHT);
    }
    #endif


    /**After Capture is created set capture fps */
    multich_hdsdidvr_set_capture_fps(&gHDSDIDVRUsecaseContext.captureFps);
    multich_hdsdidvr_set_hd_capture_info();

    multich_hdsdidvr_set_nullsrc_prm(&gHDSDIDVRUsecaseContext.nullSrcPrms);
    #if 0
    gHDSDIDVRUsecaseContext.nullSrcPrms.outQueParams.nextLink = gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX];
    gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX].inQueParams.prevLinkId = gHDSDIDVRUsecaseContext.nullSrcLinkId;
    gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX].inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.nullSrcLinkId,&gHDSDIDVRUsecaseContext.nullSrcPrms,sizeof(gHDSDIDVRUsecaseContext.nullSrcPrms));
    #endif
    multich_hdsdidvr_set_select_predei_prm(&gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX],
                                           NUM_MUX_SD_CAPTURE_CHANNELS,
                                           NUM_NONMUX_HD_CAPTURE_CHANNELS,
                                           SD_CAPTURE_START_CHANNEL_NUM,
                                           HD_CAPTURE_START_CHANNEL_NUM);
    gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX].outQueParams[PRE_DEI_SELECT_LINK_DEI_D1_ENABLE_QIDX].nextLink = gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIENABLED];
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIENABLED].inQueParams.prevLinkId = gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX];
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIENABLED].inQueParams.prevLinkQueId = PRE_DEI_SELECT_LINK_DEI_D1_ENABLE_QIDX;

    gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX].outQueParams[PRE_DEI_SELECT_LINK_DEI_D1_DISABLE_QIDX].nextLink = gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIDISABLED];
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIDISABLED].inQueParams.prevLinkId = gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX];
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIDISABLED].inQueParams.prevLinkQueId = PRE_DEI_SELECT_LINK_DEI_D1_DISABLE_QIDX;

    gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX].outQueParams[PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX].nextLink = gVcapModuleContext.deiId[DEI_LINK_IDX_HD_DEIENABLED];
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIENABLED].inQueParams.prevLinkId = gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX];
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIENABLED].inQueParams.prevLinkQueId = PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX;

    gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX].outQueParams[PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX].nextLink = gVcapModuleContext.deiId[DEI_LINK_IDX_HD_DEIDISABLED];
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIDISABLED].inQueParams.prevLinkId = gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX];
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIDISABLED].inQueParams.prevLinkQueId = PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX],&gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX],sizeof(gHDSDIDVRUsecaseContext.selectPrm[PRE_DEI_SELECT_LINK_IDX]));

    multich_hdsdidvr_set_dei_prm(&gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIENABLED],FALSE,FALSE,DEI_LINK_OUT_QUE_DEI_SC);
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIENABLED].outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink = gHDSDIDVRUsecaseContext.mergeId[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX].numInQue = D1_INTERLACED_PROGRESSIVE_MERGE_LINK_NUM_INQUE;
    gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_DEI_ENABLE_D1_QIDX].prevLinkId = gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIENABLED];
    gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_DEI_ENABLE_D1_QIDX].prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC;
    MULTICH_HDSDIDVR_CREATE_LINK(gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIENABLED],&gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIENABLED],sizeof(gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIENABLED]));

    multich_hdsdidvr_set_dei_prm(&gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIDISABLED],TRUE,TRUE,DEI_LINK_OUT_QUE_DEI_SC);
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIDISABLED].outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink = gHDSDIDVRUsecaseContext.mergeId[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX].numInQue = D1_INTERLACED_PROGRESSIVE_MERGE_LINK_NUM_INQUE;
    gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_DEI_DISABLE_D1_QIDX].prevLinkId = gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIDISABLED];
    gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_DEI_DISABLE_D1_QIDX].prevLinkQueId = DEI_LINK_OUT_QUE_DEI_SC;
    MULTICH_HDSDIDVR_CREATE_LINK(gVcapModuleContext.deiId[DEI_LINK_IDX_D1_DEIDISABLED],&gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIDISABLED],sizeof(gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_D1_DEIDISABLED]));

    multich_hdsdidvr_set_dei_prm(&gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIENABLED],FALSE,FALSE,DEI_LINK_OUT_QUE_VIP_SC);
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIENABLED].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = gHDSDIDVRUsecaseContext.mergeId[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].numInQue = INTERLACE_PROGRESSIVE_MERGE_LINK_NUM_INQUE;
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_ENABLE_HD_QIDX].prevLinkId = gVcapModuleContext.deiId[DEI_LINK_IDX_HD_DEIENABLED];
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_ENABLE_HD_QIDX].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;
    MULTICH_HDSDIDVR_CREATE_LINK(gVcapModuleContext.deiId[DEI_LINK_IDX_HD_DEIENABLED],&gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIENABLED],sizeof(gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIENABLED]));

    multich_hdsdidvr_set_dei_prm(&gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIDISABLED],TRUE,(gHDSDIDVRUsecaseContext.isProgressiveHDCapture ? FALSE : TRUE),DEI_LINK_OUT_QUE_VIP_SC);
    gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIDISABLED].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = gHDSDIDVRUsecaseContext.mergeId[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].numInQue = INTERLACE_PROGRESSIVE_MERGE_LINK_NUM_INQUE;
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_DISABLE_HD_QIDX].prevLinkId = gVcapModuleContext.deiId[DEI_LINK_IDX_HD_DEIDISABLED];
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_DISABLE_HD_QIDX].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;
    MULTICH_HDSDIDVR_CREATE_LINK(gVcapModuleContext.deiId[DEI_LINK_IDX_HD_DEIDISABLED],&gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIDISABLED],sizeof(gHDSDIDVRUsecaseContext.deiPrm[DEI_LINK_IDX_HD_DEIDISABLED]));

    gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX].numInQue = D1_INTERLACED_PROGRESSIVE_MERGE_LINK_NUM_INQUE;
    gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX].notifyNextLink = TRUE;
    gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX].outQueParams.nextLink = gVcapModuleContext.nsfId[0];
    gHDSDIDVRUsecaseContext.nsfPrm.inQueParams.prevLinkId = gHDSDIDVRUsecaseContext.mergeId[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.nsfPrm.inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.mergeId[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX],&gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX],sizeof(gHDSDIDVRUsecaseContext.mergePrm[D1_INTERLACED_PROGRESSIVE_MERGE_LINK_IDX]));

    multich_hdsdidvr_set_nsf_prm(&gHDSDIDVRUsecaseContext.nsfPrm);
    gHDSDIDVRUsecaseContext.nsfPrm.outQueParams[NSF_LINK_PROGRESSIVE_OUT_QUE_IDX].nextLink = gHDSDIDVRUsecaseContext.mergeId[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_ENABLE_D1_QIDX].prevLinkId = gVcapModuleContext.nsfId[0];
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_ENABLE_D1_QIDX].prevLinkQueId = NSF_LINK_PROGRESSIVE_OUT_QUE_IDX;
    gHDSDIDVRUsecaseContext.nsfPrm.outQueParams[NSF_LINK_INTERLACED_OUT_QUE_IDX].nextLink = gHDSDIDVRUsecaseContext.mergeId[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_DISABLE_D1_QIDX].prevLinkId = gVcapModuleContext.nsfId[0];
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].inQueParams[INTERLACE_PROGRESSIVE_MERGE_LINK_DEI_DISABLE_D1_QIDX].prevLinkQueId = NSF_LINK_INTERLACED_OUT_QUE_IDX;
    MULTICH_HDSDIDVR_CREATE_LINK(gVcapModuleContext.nsfId[0],&gHDSDIDVRUsecaseContext.nsfPrm,sizeof(gHDSDIDVRUsecaseContext.nsfPrm));

    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].numInQue = INTERLACE_PROGRESSIVE_MERGE_LINK_NUM_INQUE;
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].notifyNextLink = TRUE;
    gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX].outQueParams.nextLink = gHDSDIDVRUsecaseContext.selectId[PRE_ENCODE_SELECT_LINK_IDX];
    gHDSDIDVRUsecaseContext.selectPrm[PRE_ENCODE_SELECT_LINK_IDX].inQueParams.prevLinkId = gHDSDIDVRUsecaseContext.mergeId[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.selectPrm[PRE_ENCODE_SELECT_LINK_IDX].inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.mergeId[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX],&gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX],sizeof(gHDSDIDVRUsecaseContext.mergePrm[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX]));

    multich_hdsdidvr_set_select_preencode_prm(&gHDSDIDVRUsecaseContext.selectPrm[PRE_ENCODE_SELECT_LINK_IDX],gHDSDIDVRUsecaseContext.mergeId[INTERLACE_PROGRESSIVE_MERGE_LINK_IDX]);
    gHDSDIDVRUsecaseContext.selectPrm[PRE_ENCODE_SELECT_LINK_IDX].outQueParams[0].nextLink = gHDSDIDVRUsecaseContext.dupId[PRE_ENCODE_DUP_LINK_IDX];
    gHDSDIDVRUsecaseContext.dupPrm[PRE_ENCODE_DUP_LINK_IDX].inQueParams.prevLinkId    = gHDSDIDVRUsecaseContext.selectId[PRE_ENCODE_SELECT_LINK_IDX];
    gHDSDIDVRUsecaseContext.dupPrm[PRE_ENCODE_DUP_LINK_IDX].inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.selectId[PRE_ENCODE_SELECT_LINK_IDX],&gHDSDIDVRUsecaseContext.selectPrm[PRE_ENCODE_SELECT_LINK_IDX],sizeof(gHDSDIDVRUsecaseContext.selectPrm[PRE_ENCODE_SELECT_LINK_IDX]));

    gHDSDIDVRUsecaseContext.dupPrm[PRE_ENCODE_DUP_LINK_IDX].notifyNextLink = TRUE;
    gHDSDIDVRUsecaseContext.dupPrm[PRE_ENCODE_DUP_LINK_IDX].numOutQue = PRE_ENCODE_DUP_LINK_NUM_OUTQUE;

    gHDSDIDVRUsecaseContext.dupPrm[PRE_ENCODE_DUP_LINK_IDX].outQueParams[PRE_ENCODE_DUP_LINK_ENCODE_QIDX].nextLink = gHDSDIDVRUsecaseContext.ipcOutVpssId;
    gHDSDIDVRUsecaseContext.ipcOutVpssPrm.inQueParams.prevLinkId = gHDSDIDVRUsecaseContext.dupId[PRE_ENCODE_DUP_LINK_IDX];
    gHDSDIDVRUsecaseContext.ipcOutVpssPrm.inQueParams.prevLinkQueId = PRE_ENCODE_DUP_LINK_ENCODE_QIDX;

    gHDSDIDVRUsecaseContext.dupPrm[PRE_ENCODE_DUP_LINK_IDX].outQueParams[PRE_ENCODE_DUP_LINK_IPCFRAMESOUT_QIDX].nextLink = gVcapModuleContext.ipcFramesOutVpssToHostId;
    gHDSDIDVRUsecaseContext.ipcFramesOutVpssPrm.baseCreateParams.inQueParams.prevLinkId = gHDSDIDVRUsecaseContext.dupId[PRE_ENCODE_DUP_LINK_IDX];
    gHDSDIDVRUsecaseContext.ipcFramesOutVpssPrm.baseCreateParams.inQueParams.prevLinkQueId = PRE_ENCODE_DUP_LINK_IPCFRAMESOUT_QIDX;

    gHDSDIDVRUsecaseContext.dupPrm[PRE_ENCODE_DUP_LINK_IDX].outQueParams[PRE_ENCODE_DUP_LINK_SWMS_QIDX].nextLink = gHDSDIDVRUsecaseContext.mergeId[LIVE_DECODE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX].numInQue = LIVE_DECODE_MERGE_LINK_NUM_INQUE;
    gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[LIVE_DECODE_MERGE_LINK_PREVIEW_QIDX].prevLinkId = gHDSDIDVRUsecaseContext.dupId[PRE_ENCODE_DUP_LINK_IDX];
    gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[LIVE_DECODE_MERGE_LINK_PREVIEW_QIDX].prevLinkQueId = PRE_ENCODE_DUP_LINK_SWMS_QIDX;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.dupId[PRE_ENCODE_DUP_LINK_IDX],&gHDSDIDVRUsecaseContext.dupPrm[PRE_ENCODE_DUP_LINK_IDX],sizeof(gHDSDIDVRUsecaseContext.dupPrm[PRE_ENCODE_DUP_LINK_IDX]));

     multich_hdsdidvr_set_ipcframesout2host_prm(&gHDSDIDVRUsecaseContext.ipcFramesOutVpssPrm);
     gHDSDIDVRUsecaseContext.ipcFramesOutVpssPrm.baseCreateParams.equallyDivideChAcrossOutQues = TRUE;
     gHDSDIDVRUsecaseContext.ipcFramesOutVpssPrm.baseCreateParams.numOutQue = 1;
     gHDSDIDVRUsecaseContext.ipcFramesOutVpssPrm.baseCreateParams.outQueParams[0].nextLink = gVcapModuleContext.ipcFramesInHostId;
     gHDSDIDVRUsecaseContext.ipcFramesOutVpssPrm.baseCreateParams.processLink = SYSTEM_LINK_ID_INVALID;
     gHDSDIDVRUsecaseContext.ipcFramesInHostPrm.baseCreateParams.inQueParams.prevLinkId = gVcapModuleContext.ipcFramesOutVpssToHostId;
     gHDSDIDVRUsecaseContext.ipcFramesInHostPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
     MULTICH_HDSDIDVR_CREATE_LINK(gVcapModuleContext.ipcFramesOutVpssToHostId,&gHDSDIDVRUsecaseContext.ipcFramesOutVpssPrm,sizeof(gHDSDIDVRUsecaseContext.ipcFramesOutVpssPrm));

     multich_hdsdidvr_set_ipcframesinhost_prm(&gHDSDIDVRUsecaseContext.ipcFramesInHostPrm);
     MULTICH_HDSDIDVR_CREATE_LINK(gVcapModuleContext.ipcFramesInHostId,&gHDSDIDVRUsecaseContext.ipcFramesInHostPrm,sizeof(gHDSDIDVRUsecaseContext.ipcFramesInHostPrm));

    /* ipcOutVpssIdisOutVpssId ---Q0---> ipcInVideoId */
    gHDSDIDVRUsecaseContext.ipcOutVpssPrm.outQueParams[0].nextLink  = gHDSDIDVRUsecaseContext.ipcInVideoId;
    gHDSDIDVRUsecaseContext.ipcOutVpssPrm.notifyNextLink            = FALSE;
    gHDSDIDVRUsecaseContext.ipcOutVpssPrm.notifyPrevLink            = TRUE;
    gHDSDIDVRUsecaseContext.ipcOutVpssPrm.noNotifyMode              = TRUE;
    gHDSDIDVRUsecaseContext.ipcInVideoPrm.inQueParams.prevLinkId    = gHDSDIDVRUsecaseContext.ipcOutVpssId;
    gHDSDIDVRUsecaseContext.ipcInVideoPrm.inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.ipcOutVpssId,
                                  &gHDSDIDVRUsecaseContext.ipcOutVpssPrm,
                                  sizeof(gHDSDIDVRUsecaseContext.ipcOutVpssPrm));


    /* ipcInVideoId ---Q0---> encId */
    gHDSDIDVRUsecaseContext.ipcInVideoPrm.numOutQue                 = 1;
    gHDSDIDVRUsecaseContext.ipcInVideoPrm.outQueParams[0].nextLink  = gVencModuleContext.encId;
    gHDSDIDVRUsecaseContext.ipcInVideoPrm.notifyNextLink            = TRUE;
    gHDSDIDVRUsecaseContext.ipcInVideoPrm.notifyPrevLink            = FALSE;
    gHDSDIDVRUsecaseContext.ipcInVideoPrm.noNotifyMode              = TRUE;
    gHDSDIDVRUsecaseContext.encPrm.inQueParams.prevLinkId    = gHDSDIDVRUsecaseContext.ipcInVideoId;
    gHDSDIDVRUsecaseContext.encPrm.inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.ipcInVideoId,
                                  &gHDSDIDVRUsecaseContext.ipcInVideoPrm,
                                  sizeof(gHDSDIDVRUsecaseContext.ipcInVideoPrm));


    /* encId ---Q0---> ipcBitsOutRTOSId */
    multich_hdsdidvr_set_enclink_prm(&gHDSDIDVRUsecaseContext.encPrm);
    gHDSDIDVRUsecaseContext.encPrm.outQueParams.nextLink     = gVencModuleContext.ipcBitsOutRTOSId;
    gHDSDIDVRUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkId = gVencModuleContext.encId;
    gHDSDIDVRUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gVencModuleContext.encId,
                                  &gHDSDIDVRUsecaseContext.encPrm,
                                  sizeof(gHDSDIDVRUsecaseContext.encPrm));

    /* ipcBitsOutVideoId ---Q0---> ipcBitsInHostId */
    gHDSDIDVRUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.numOutQue                 = 1;
    gHDSDIDVRUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.outQueParams[0].nextLink = gVencModuleContext.ipcBitsInHLOSId;
    MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&gHDSDIDVRUsecaseContext.ipcBitsOutVideoPrm,
                                               TRUE);
    gHDSDIDVRUsecaseContext.ipcBitsInHostPrm[0].baseCreateParams.inQueParams.prevLinkId = gVencModuleContext.ipcBitsOutRTOSId;
    gHDSDIDVRUsecaseContext.ipcBitsInHostPrm[0].baseCreateParams.inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gVencModuleContext.ipcBitsOutRTOSId,
                                  &gHDSDIDVRUsecaseContext.ipcBitsOutVideoPrm,
                                  sizeof(gHDSDIDVRUsecaseContext.ipcBitsOutVideoPrm));
    MultiCh_ipcBitsInitCreateParams_BitsInHLOS(&gHDSDIDVRUsecaseContext.ipcBitsInHostPrm[0]);
    MULTICH_HDSDIDVR_CREATE_LINK(gVencModuleContext.ipcBitsInHLOSId,
                                  &gHDSDIDVRUsecaseContext.ipcBitsInHostPrm[0],
                                  sizeof(gHDSDIDVRUsecaseContext.ipcBitsInHostPrm[0]));

    /* ipcBitsOutHostId ---Q0---> ipcBitsInRtosId */
    multich_hdsdidvr_set_ipcbitsout_hlos_prms(&gHDSDIDVRUsecaseContext.ipcBitsOutHostPrm,gVencModuleContext.ipcBitsInHLOSId);
    gHDSDIDVRUsecaseContext.ipcBitsOutHostPrm.baseCreateParams.outQueParams[0].nextLink = gVdecModuleContext.ipcBitsInRTOSId;
    gHDSDIDVRUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkId       = gVdecModuleContext.ipcBitsOutHLOSId;
    gHDSDIDVRUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkQueId    = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gVdecModuleContext.ipcBitsOutHLOSId,
                                  &gHDSDIDVRUsecaseContext.ipcBitsOutHostPrm,
                                  sizeof(gHDSDIDVRUsecaseContext.ipcBitsOutHostPrm));

    /* ipcBitsInRtosId ---Q0---> decId */
    gHDSDIDVRUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.numOutQue                    = 1;
    gHDSDIDVRUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.outQueParams[0].nextLink     = gVdecModuleContext.decId;
    MultiCh_ipcBitsInitCreateParams_BitsInRTOS(&gHDSDIDVRUsecaseContext.ipcBitsInVideoPrm, TRUE);
    gHDSDIDVRUsecaseContext.decPrm.inQueParams.prevLinkId    = gVdecModuleContext.ipcBitsInRTOSId;
    gHDSDIDVRUsecaseContext.decPrm.inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gVdecModuleContext.ipcBitsInRTOSId,
                                  &gHDSDIDVRUsecaseContext.ipcBitsInVideoPrm,
                                  sizeof(gHDSDIDVRUsecaseContext.ipcBitsInVideoPrm));

    /* decId---Q0--->ipcOutVideoId*/
    multich_hdsdidvr_set_declink_prms(&gHDSDIDVRUsecaseContext.decPrm);
    gHDSDIDVRUsecaseContext.decPrm.outQueParams.nextLink  = gHDSDIDVRUsecaseContext.ipcOutVideoId;
    gHDSDIDVRUsecaseContext.ipcOutVideoPrm.inQueParams.prevLinkId    = gVdecModuleContext.decId;
    gHDSDIDVRUsecaseContext.ipcOutVideoPrm.inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gVdecModuleContext.decId,
                                  &gHDSDIDVRUsecaseContext.decPrm,
                                  sizeof(gHDSDIDVRUsecaseContext.decPrm));

    /*ipcOutVideoId---Q0-->ipcInVpssId*/
    gHDSDIDVRUsecaseContext.ipcOutVideoPrm.numOutQue                 = 1;
    gHDSDIDVRUsecaseContext.ipcOutVideoPrm.outQueParams[0].nextLink  = gHDSDIDVRUsecaseContext.ipcInVpssId;
    gHDSDIDVRUsecaseContext.ipcOutVideoPrm.notifyNextLink            = FALSE;
    gHDSDIDVRUsecaseContext.ipcOutVideoPrm.notifyPrevLink            = TRUE;
    gHDSDIDVRUsecaseContext.ipcOutVideoPrm.noNotifyMode              = TRUE;
    gHDSDIDVRUsecaseContext.ipcInVpssPrm.inQueParams.prevLinkId    = gHDSDIDVRUsecaseContext.ipcOutVideoId;
    gHDSDIDVRUsecaseContext.ipcInVpssPrm.inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.ipcOutVideoId,
                                  &gHDSDIDVRUsecaseContext.ipcOutVideoPrm,
                                  sizeof(gHDSDIDVRUsecaseContext.ipcOutVideoPrm));

    /*ipcInVpssId---Q0--> mergeId[LIVE_DECODE_MERGE_LINK_IDX] */
    gHDSDIDVRUsecaseContext.ipcInVpssPrm.numOutQue                 = 1;
    gHDSDIDVRUsecaseContext.ipcInVpssPrm.outQueParams[0].nextLink  = gHDSDIDVRUsecaseContext.mergeId[LIVE_DECODE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX].numInQue = LIVE_DECODE_MERGE_LINK_NUM_INQUE;
    gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[LIVE_DECODE_MERGE_LINK_PLAYBACK_QIDX].prevLinkId = gHDSDIDVRUsecaseContext.ipcInVpssId;
    gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[LIVE_DECODE_MERGE_LINK_PLAYBACK_QIDX].prevLinkQueId = 0;
    gHDSDIDVRUsecaseContext.ipcInVpssPrm.notifyNextLink            = TRUE;
    gHDSDIDVRUsecaseContext.ipcInVpssPrm.notifyPrevLink            = FALSE;
    gHDSDIDVRUsecaseContext.ipcInVpssPrm.noNotifyMode              = TRUE;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.ipcInVpssId,
                                  &gHDSDIDVRUsecaseContext.ipcInVpssPrm,
                                  sizeof(gHDSDIDVRUsecaseContext.ipcInVpssPrm));


    gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX].notifyNextLink = TRUE;
    gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX].numInQue = LIVE_DECODE_MERGE_LINK_NUM_INQUE;
    gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX].outQueParams.nextLink = gHDSDIDVRUsecaseContext.dupId[PRE_SWMS_DUP_LINK_IDX];
    gHDSDIDVRUsecaseContext.dupPrm[PRE_SWMS_DUP_LINK_IDX].inQueParams.prevLinkId = gHDSDIDVRUsecaseContext.mergeId[LIVE_DECODE_MERGE_LINK_IDX];
    gHDSDIDVRUsecaseContext.dupPrm[PRE_SWMS_DUP_LINK_IDX].inQueParams.prevLinkQueId = 0;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.mergeId[LIVE_DECODE_MERGE_LINK_IDX],
                                  &gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX],
                                  sizeof(gHDSDIDVRUsecaseContext.mergePrm[LIVE_DECODE_MERGE_LINK_IDX]));

    /*dupId[DECODE_DUP_LINK_IDX]---Q0--> swMsId[0] */
    gHDSDIDVRUsecaseContext.dupPrm[PRE_SWMS_DUP_LINK_IDX].numOutQue                      = 2;
    gHDSDIDVRUsecaseContext.dupPrm[PRE_SWMS_DUP_LINK_IDX].outQueParams[0].nextLink       = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)];
    gHDSDIDVRUsecaseContext.swMsPrm[0].inQueParams.prevLinkId    = gHDSDIDVRUsecaseContext.dupId[PRE_SWMS_DUP_LINK_IDX];
    gHDSDIDVRUsecaseContext.swMsPrm[0].inQueParams.prevLinkQueId = 0;

    /*dupId[LIVE_DECODE_DUP_LINK_IDX]---Q1--> swMsId[1] */
    gHDSDIDVRUsecaseContext.dupPrm[PRE_SWMS_DUP_LINK_IDX].numOutQue                      = 2;
    gHDSDIDVRUsecaseContext.dupPrm[PRE_SWMS_DUP_LINK_IDX].outQueParams[1].nextLink       = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)];
    gHDSDIDVRUsecaseContext.dupPrm[PRE_SWMS_DUP_LINK_IDX].notifyNextLink                 = TRUE;
    gHDSDIDVRUsecaseContext.swMsPrm[1].inQueParams.prevLinkId    = gHDSDIDVRUsecaseContext.dupId[PRE_SWMS_DUP_LINK_IDX];
    gHDSDIDVRUsecaseContext.swMsPrm[1].inQueParams.prevLinkQueId = 1;
    MULTICH_HDSDIDVR_CREATE_LINK(gHDSDIDVRUsecaseContext.dupId[PRE_SWMS_DUP_LINK_IDX],
                                  &gHDSDIDVRUsecaseContext.dupPrm[PRE_SWMS_DUP_LINK_IDX],
                                  sizeof(gHDSDIDVRUsecaseContext.dupPrm[PRE_SWMS_DUP_LINK_IDX]));

    /* Avsync configuration for SwMs[0] */
    mulich_hdsdidvr_set_avsync_prm(&gHDSDIDVRUsecaseContext.avsyncCfg[0],
                                    0,
                                    gHDSDIDVRUsecaseContext.dupId[PRE_SWMS_DUP_LINK_IDX],
                                    0);
    /*swMsId[0]---Q0--> displayId[VDIS_DEV_HDMI] */
    gHDSDIDVRUsecaseContext.swMsPrm[0].outQueParams.nextLink     = Vdis_getDisplayId(VDIS_DEV_HDMI);
    gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)].numInputQueues = 1;
    gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)].inQueParams[0].prevLinkId    = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)];
    gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)].inQueParams[0].prevLinkQueId = 0;
    multich_hdsdidvr_set_swms_prm(&gHDSDIDVRUsecaseContext.swMsPrm[0],
                                    0);
    gHDSDIDVRUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)] = gHDSDIDVRUsecaseContext.swMsPrm[0].layoutPrm;
    MULTICH_HDSDIDVR_CREATE_LINK(gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)],
                                 &gHDSDIDVRUsecaseContext.swMsPrm[0],
                                 sizeof(gHDSDIDVRUsecaseContext.swMsPrm[0]));

    /* Avsync configuration for SwMs[1] */
    mulich_hdsdidvr_set_avsync_prm(&gHDSDIDVRUsecaseContext.avsyncCfg[1],
                                    1,
                                    gHDSDIDVRUsecaseContext.dupId[PRE_SWMS_DUP_LINK_IDX],
                                    1);
    /*swMsId[1]---Q1--> displayId[VDIS_DEV_SD] */
    gHDSDIDVRUsecaseContext.swMsPrm[1].outQueParams.nextLink     = Vdis_getDisplayId(VDIS_DEV_DVO2);
    gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)].numInputQueues = 1;
    gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)].inQueParams[0].prevLinkId    = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)];
    gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)].inQueParams[0].prevLinkQueId = 0;
    multich_hdsdidvr_set_swms_prm(&gHDSDIDVRUsecaseContext.swMsPrm[1],
                                    1);
    gHDSDIDVRUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)] = gHDSDIDVRUsecaseContext.swMsPrm[1].layoutPrm;
    MULTICH_HDSDIDVR_CREATE_LINK(gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)],
                                  &gHDSDIDVRUsecaseContext.swMsPrm[1],
                                  sizeof(gHDSDIDVRUsecaseContext.swMsPrm[1]));

    multich_hdsdidvr_set_display_prms(&gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)],
                                        gHDSDIDVRUsecaseContext.swMsPrm[0].initOutRes);
    MULTICH_HDSDIDVR_CREATE_LINK(Vdis_getDisplayId(VDIS_DEV_HDMI),
                                  &gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)],
                                  sizeof(gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_HDMI)]));
    multich_hdsdidvr_set_display_prms(&gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)],
                                        gHDSDIDVRUsecaseContext.swMsPrm[1].initOutRes);
    MULTICH_HDSDIDVR_CREATE_LINK(Vdis_getDisplayId(VDIS_DEV_DVO2),
                                  &gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)],
                                  sizeof(gHDSDIDVRUsecaseContext.displayPrm[Vdis_getDisplayContextIndex(VDIS_DEV_DVO2)]));
}

static
void multich_hdsdidvr_set_dec2disp_chmap()
{
    MergeLink_InLinkChInfo inChInfo;

    MergeLink_InLinkChInfo_Init(&inChInfo);
    inChInfo.inLinkID = gHDSDIDVRUsecaseContext.ipcInVpssId;
    System_linkControl(gHDSDIDVRUsecaseContext.mergeId[LIVE_DECODE_MERGE_LINK_IDX],
                       MERGE_LINK_CMD_GET_INPUT_LINK_CHINFO,
                       &inChInfo,
                       sizeof(inChInfo),
                       TRUE);
    OSA_assert(inChInfo.numCh == gVdecModuleContext.vdecConfig.numChn);

    MultiCh_setDec2DispMap(VDIS_DEV_HDMI,gVdecModuleContext.vdecConfig.numChn,0,MAX_NUM_ENCODE_CHANNELS);
    MultiCh_setDec2DispMap(VDIS_DEV_DVO2,gVdecModuleContext.vdecConfig.numChn,0,MAX_NUM_ENCODE_CHANNELS);
}


static
void multich_hdsdidvr_set_encswitchstartch()
{
    gHDSDIDVRUsecaseContext.encodeSwitchNumCh   = MAX_NUM_ENCODE_CHANNELS;
}


Void MultiCh_createHDSDIDVR()
{

    multich_hdsdidvr_reset_link_prms();
    multich_hdsdidvr_set_link_ids();
    printf("\n********* Entered usecase HDSDIDVR <816x> Cap/Enc/Dec/Dis \n\n");

    MultiCh_detectBoard();

    System_linkControl(
        SYSTEM_LINK_ID_M3VPSS,
        SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES,
        NULL,
        0,
        TRUE
        );

    System_linkControl(
        SYSTEM_LINK_ID_M3VIDEO,
        SYSTEM_COMMON_CMD_SET_CH2IVAHD_MAP_TBL,
        &systemVid_encDecIvaChMapTbl,
        sizeof(SystemVideo_Ivahd2ChMap_Tbl),
        TRUE
    );

    if ((FALSE == TILER_ENABLE_ENCODE) && (FALSE == TILER_ENABLE_DECODE_SD) && (FALSE == TILER_ENABLE_DECODE_HD))
    {
        SystemTiler_disableAllocator();
    }
    multich_hdsdidvr_connect_links();
    multich_hdsdidvr_set_links_framerate();
    multich_hdsdidvr_set_dec2disp_chmap();
    multich_hdsdidvr_set_encswitchstartch();
    multich_hdsdidvr_select_all_progressive_encode();
//    multich_hdsdidvr_select_all_interlaced_encode();
}

Void MultiCh_deleteHDSDIDVR()
{
    UInt32 i;

    for (i = 0; i < gHDSDIDVRUsecaseContext.createdLinkCount; i++)
    {
        System_linkDelete (gHDSDIDVRUsecaseContext.createdLinks[i]);
    }
    gHDSDIDVRUsecaseContext.createdLinkCount = 0;
    multich_hdsdidvr_reset_link_ids();

    Vcap_deleteVideoDecoder();
    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    MultiCh_prfLoadCalcEnable(FALSE, TRUE, FALSE);

    if ((FALSE == TILER_ENABLE_ENCODE) && (FALSE == TILER_ENABLE_DECODE_SD) && (FALSE == TILER_ENABLE_DECODE_HD))
    {
        SystemTiler_enableAllocator();
    }
}

static
UInt32 multich_hdsdidvr_map_swms_channel2win(VDIS_DEV devId,UInt32 swMsChNum)
{
    UInt32 winNum = SYSTEM_SW_MS_INVALID_ID;
    Int i;

    for (i = 0; i < gHDSDIDVRUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(devId)].numWin;i++)
    {
        if (swMsChNum == gHDSDIDVRUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(devId)].winInfo[i].channelNum)
        {
            winNum = i;
            break;
        }
    }
    return winNum;
}

static
Bool  multich_hdsdidvr_is_invisible_channel(VDIS_DEV devId,UInt32 swMsChNum)
{
    if (multich_hdsdidvr_map_swms_channel2win(devId, swMsChNum) == SYSTEM_SW_MS_INVALID_ID)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}



static
Void  multich_hdsdidvr_get_decode_visibility_channels_list(UInt32 *invisbleChannelList,
                                                           UInt32 *numInvisibleCh,UInt32 maxInvisibleCh,
                                                           UInt32 *visbleChannelList,
                                                           UInt32 *numVisibleCh,UInt32 maxVisibleCh)
{
    Int i;
    UInt32 swmsChNumHDMI;
    UInt32 swmsChNumDVO2;

    *numInvisibleCh = 0;

    for (i = 0; i < gVdecModuleContext.vdecConfig.numChn ;i++)
    {
        Vdec_mapDec2DisplayChId(VDIS_DEV_HDMI,i,&swmsChNumHDMI);
        Vdec_mapDec2DisplayChId(VDIS_DEV_DVO2,i,&swmsChNumDVO2);
        OSA_assert(swmsChNumHDMI == swmsChNumDVO2);
        if (multich_hdsdidvr_is_invisible_channel(VDIS_DEV_HDMI,swmsChNumHDMI)
            &&
            multich_hdsdidvr_is_invisible_channel(VDIS_DEV_DVO2,swmsChNumDVO2))
        {
            OSA_assert(*numInvisibleCh < maxInvisibleCh);
            invisbleChannelList[*numInvisibleCh] = i;
            *numInvisibleCh += 1;
        }
        else
        {
            OSA_assert(*numVisibleCh < maxVisibleCh);
            visbleChannelList[*numVisibleCh] = i;
            *numVisibleCh += 1;
        }
    }
}

static
Void  multich_hdsdidvr_enable_visible_decode_channels()
{
    Int i;
    UInt32 numInvisibleCh = 0;
    UInt32 numVisibleCh = 0;
    DecLink_ChannelInfo decChInfo;
    Int32 status;

    multich_hdsdidvr_get_decode_visibility_channels_list(gHDSDIDVRUsecaseContext.invisbleDecChannelList,
                                                          &numInvisibleCh,
                                                          OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.invisbleDecChannelList),
                                                          gHDSDIDVRUsecaseContext.visbleDecChannelList,
                                                          &numVisibleCh,
                                                          OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.visbleDecChannelList));
    for (i =0; i < numVisibleCh;i++)
    {
        decChInfo.chId     = gHDSDIDVRUsecaseContext.visbleDecChannelList[i];
        printf("MULTICH_HDSDI_DVR:Decode enable visible channel.DEC_LINKID[0x%X],CHNUM[%d]\n",
                gVdecModuleContext.decId,decChInfo.chId);
        status =
        System_linkControl(gVdecModuleContext.decId,DEC_LINK_CMD_ENABLE_CHANNEL,
                            &decChInfo,sizeof(decChInfo),TRUE);
        OSA_assert(status == 0);
    }

    for (i =0; i < numInvisibleCh;i++)
    {
        decChInfo.chId     = gHDSDIDVRUsecaseContext.invisbleDecChannelList[i];
        printf("MULTICH_HDSDI_DVR:Decode disable invisible channel.DEC_LINKID[0x%X],CHNUM[%d]\n",
                gVdecModuleContext.decId,decChInfo.chId);
        status =
        System_linkControl(gVdecModuleContext.decId,DEC_LINK_CMD_DISABLE_CHANNEL,
                            &decChInfo,sizeof(decChInfo),TRUE);
        OSA_assert(status == 0);
    }
}

static
Int32 MultiCh_hdsdiDVRSetMosaicParams(VDIS_DEV vdDevId, VDIS_MOSAIC_S *psVdMosaicParam )
{
    UInt32 winId, chId;
    UInt32 swMsId = SYSTEM_LINK_ID_INVALID;
    SwMsLink_LayoutPrm *vdisLayoutPrm;

    swMsId = gVdisModuleContext.swMsId[Vdis_getDisplayContextIndex(vdDevId)];
    if(swMsId==SYSTEM_LINK_ID_INVALID)
        return -1;

    vdisLayoutPrm = &gHDSDIDVRUsecaseContext.swmsLayoutPrm[Vdis_getDisplayContextIndex(vdDevId)];

    /* Get display resolution and coordinates */
    vdisLayoutPrm->numWin = psVdMosaicParam->numberOfWindows;
    vdisLayoutPrm->onlyCh2WinMapChanged = psVdMosaicParam->onlyCh2WinMapChanged;
    vdisLayoutPrm->outputFPS = psVdMosaicParam->outputFPS;

    /* Assign each windows coordinates, size and mapping */
    for(winId=0; winId<vdisLayoutPrm->numWin; winId++)
    {
        vdisLayoutPrm->winInfo[winId].channelNum         = psVdMosaicParam->chnMap[winId];
        vdisLayoutPrm->winInfo[winId].bufAddrOffset[0u]      = -1;
        vdisLayoutPrm->winInfo[winId].bufAddrOffset[1u]      = -1;
        vdisLayoutPrm->winInfo[winId].width              = psVdMosaicParam->winList[winId].width;
        vdisLayoutPrm->winInfo[winId].height             = psVdMosaicParam->winList[winId].height;
        vdisLayoutPrm->winInfo[winId].startX             = psVdMosaicParam->winList[winId].start_X;
        vdisLayoutPrm->winInfo[winId].startY             = psVdMosaicParam->winList[winId].start_Y;
        vdisLayoutPrm->winInfo[winId].bypass             = FALSE;
        chId = psVdMosaicParam->chnMap[winId];

        if(chId < gVdisModuleContext.vdisConfig.numChannels)
        {
            Vdis_setChn2WinMap(vdDevId, chId,winId);

            if(Vdis_isEnableChn(vdDevId,chId) == FALSE)
            {
                vdisLayoutPrm->winInfo[winId].channelNum = SYSTEM_SW_MS_INVALID_ID;
            }
        }
    }

    Vdis_swMs_PrintLayoutParams(vdDevId, vdisLayoutPrm);
    System_linkControl(swMsId, SYSTEM_SW_MS_LINK_CMD_SWITCH_LAYOUT, (vdisLayoutPrm), sizeof(*vdisLayoutPrm), TRUE);

    multich_hdsdidvr_enable_visible_decode_channels();
    return 0;
}

static
Void multich_hdsdidvr_set_enc_ch_params(UInt32 chNum,EncLink_ChCreateParams *pLinkChPrm)
{
    EncLink_ChDynamicParams *pLinkDynPrm;
    VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
    VENC_CHN_PARAMS_S *pChPrm;

    pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

    pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[chNum];
    pDynPrm     = &pChPrm->dynamicParam;

    OSA_assert(chNum < OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap));
    if (IVIDEO_H264HP == gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[chNum])
    {
        pLinkChPrm->format                  = IVIDEO_H264HP;
        pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[chNum];
        pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
        pLinkChPrm->rateControlPreset       = pChPrm->rcType;
        pLinkChPrm->enableHighSpeed         = FALSE;
        pLinkChPrm->numTemporalLayer        = VENC_TEMPORAL_LAYERS_1;
        pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;

        pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
        pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
        pLinkDynPrm->interFrameInterval     = 1;
        pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
        pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
        pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
        pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
        pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
        pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
        pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
        pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
    }
    else
    {
        if(IVIDEO_MJPEG == gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[chNum])
        {
            pLinkChPrm->format                 = IVIDEO_MJPEG;
            pLinkChPrm->profile                = 0;
            pLinkChPrm->dataLayout             = VCODEC_FIELD_SEPARATED;
            pLinkChPrm->fieldMergeEncodeEnable = FALSE;
            pLinkChPrm->enableAnalyticinfo     = 0;
            pLinkChPrm->enableWaterMarking     = 0;
            pLinkChPrm->maxBitRate             = 0;
            pLinkChPrm->encodingPreset         = 0;
            pLinkChPrm->rateControlPreset      = 0;
            pLinkChPrm->enableSVCExtensionFlag = 0;
            pLinkChPrm->numTemporalLayer       = 0;

            pLinkDynPrm->intraFrameInterval    = 0;
            pLinkDynPrm->targetBitRate         = 100*1000*30;
            pLinkDynPrm->interFrameInterval    = 0;
            pLinkDynPrm->mvAccuracy            = 0;
            pLinkDynPrm->inputFrameRate        = pDynPrm->inputFrameRate;
            pLinkDynPrm->qpMin                 = 0;
            pLinkDynPrm->qpMax                 = 0;
            pLinkDynPrm->qpInit                = -1;
            pLinkDynPrm->vbrDuration           = 0;
            pLinkDynPrm->vbrSensitivity        = 0;
        }
        else
        {
            OSA_assert(IVIDEO_MPEG4SP == gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[chNum]);
            pLinkChPrm->format                  = IVIDEO_MPEG4SP;
            pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[chNum];
            pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
            pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
            pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
            pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
            pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
            pLinkChPrm->rateControlPreset       = pChPrm->rcType;
            pLinkChPrm->enableHighSpeed         = FALSE;
            pLinkChPrm->numTemporalLayer        = VENC_TEMPORAL_LAYERS_1;
            pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;

            pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
            pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
            pLinkDynPrm->interFrameInterval     = 1;
            pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
            pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
            pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
            pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
            pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
            pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
            pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
            pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
        }
    }
}

static
Void multich_hdsdidvr_set_dec_ch_params(UInt32 chNum,DecLink_addChannelInfo *decPrm,Bool isInterlaced)
{
    memset(decPrm,0,sizeof(*decPrm));


    decPrm->chId = chNum;
    decPrm->createPrm.dpbBufSizeInFrames = IH264VDEC_DPB_NUMFRAMES_AUTO;
    decPrm->createPrm.algCreateStatus = DEC_LINK_ALG_CREATE_STATUS_CREATE;
    decPrm->createPrm.decodeFrameType = VDEC_DECODE_ALL;


    if(chNum < OSA_ARRAYSIZE(gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap))
    {
        decPrm->createPrm.format                 = gHDSDIDVRUsecaseContext.encChannel2CodecTypeMap[chNum];
    }
    if (isInterlaced)
    {
        decPrm->createPrm.processCallLevel       = VDEC_FIELDLEVELPROCESSCALL;
        decPrm->chInfo.scanFormat      = 0;
    }
    else
    {
        decPrm->createPrm.processCallLevel       = VDEC_FRAMELEVELPROCESSCALL;
        decPrm->chInfo.scanFormat      = 1;
    }
    if ((chNum >= SD_DECODE_START_CH) && (chNum < (SD_DECODE_START_CH + NUM_SD_DECODE_CHANNELS)))
    {
        decPrm->createPrm.targetMaxWidth  = SD_DECODE_CHANNEL_WIDTH;
        decPrm->createPrm.targetMaxHeight = SD_DECODE_CHANNEL_HEIGHT;
        decPrm->chInfo.width           = SD_DECODE_CHANNEL_WIDTH;
        decPrm->chInfo.height          = SD_DECODE_CHANNEL_HEIGHT;
        decPrm->createPrm.numBufPerCh            = NUM_BUFS_PER_CH_DEC_SD;
        decPrm->createPrm.tilerEnable            = TILER_ENABLE_DECODE_SD;
    }
    else
    {
        decPrm->createPrm.targetMaxWidth  = HD_DECODE_CHANNEL_WIDTH;
        decPrm->createPrm.targetMaxHeight = HD_DECODE_CHANNEL_HEIGHT;
        decPrm->chInfo.width           = HD_DECODE_CHANNEL_WIDTH;
        decPrm->chInfo.height          = HD_DECODE_CHANNEL_HEIGHT;
        decPrm->createPrm.numBufPerCh            = NUM_BUFS_PER_CH_DEC_HD;
        decPrm->createPrm.tilerEnable            = TILER_ENABLE_DECODE_HD;
    }
    decPrm->createPrm.defaultDynamicParams.targetBitRate = gVdecModuleContext.vdecConfig.decChannelParams[chNum].dynamicParam.targetBitRate;
    decPrm->createPrm.defaultDynamicParams.targetFrameRate = gVdecModuleContext.vdecConfig.decChannelParams[chNum].dynamicParam.frameRate;
    decPrm->createPrm.fieldMergeDecodeEnable = FALSE;
    decPrm->createPrm.displayDelay = 2;
    switch (decPrm->createPrm.format)
    {
        case IVIDEO_H264HP: /* H264 */
            decPrm->createPrm.profile = IH264VDEC_PROFILE_ANY;
            break;
        case IVIDEO_MPEG4SP: /* MPEG4 */
            decPrm->createPrm.format = IVIDEO_MPEG4ASP;
            decPrm->createPrm.profile = 0;
            /* Display delay need to set anything otherthan decode order
             * otherwise XDM flush doesn't work for MPEG4 decoder */
            decPrm->createPrm.displayDelay = -1;
            break;
        case IVIDEO_MJPEG: /* MJPEG */
            decPrm->createPrm.profile = 0;
            decPrm->createPrm.displayDelay = 0;
            break;
        default: /* D1 */
            printf("\r\nCodec Type: %d, returning \n", decPrm->createPrm.format);
            OSA_assert(FALSE);
            break;
    }
}


static
Void multich_hdsdidvr_select_all_progressive_encode()
{
    SelectLink_OutQueChInfo selectChInfo;
    Int32 status;
    Int i;
    UInt32 encodeSelectStartCh;
    EncLink_ChSwitchCodecTypeParams encSwitchParams;

    OSA_assert(gHDSDIDVRUsecaseContext.encodeSwitchNumCh == gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsD1 + gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsHD);

    #ifdef MULTICH_HDSDIDVR_ENC_DEC_LOOPBACK
    for (i = 0; i < gHDSDIDVRUsecaseContext.encodeSwitchNumCh; i++)
    {
        DecLink_ChannelInfo params = {0};

        params.chId = i;

        status = System_linkControl(gVdecModuleContext.decId, DEC_LINK_CMD_DELETE_CHANNEL,
                       &params, sizeof(params), TRUE);
        OSA_assert(status == OSA_SOK);
    }
    #endif

    for (i = 0; i < gHDSDIDVRUsecaseContext.encodeSwitchNumCh; i++)
    {
        encSwitchParams.chId = i;
        encSwitchParams.switchCodecFlag = TRUE;
        encSwitchParams.algCreatePrm.overrideInputScanFormat  = TRUE;
        encSwitchParams.algCreatePrm.fieldPicEncode    = FALSE;
        multich_hdsdidvr_set_enc_ch_params(encSwitchParams.chId,&encSwitchParams.algCreatePrm);
        status = System_linkControl(gVencModuleContext.encId,
                                    ENC_LINK_CMD_SWITCH_CODEC_CHANNEL,
                                    &encSwitchParams,
                                    sizeof(encSwitchParams),
                                    TRUE);
        OSA_assert(status == OSA_SOK);
    }

    #ifdef MULTICH_HDSDIDVR_ENC_DEC_LOOPBACK
    for (i = 0; i < gHDSDIDVRUsecaseContext.encodeSwitchNumCh; i++)
    {
        DecLink_addChannelInfo decCreatePrms = {0};
        multich_hdsdidvr_set_dec_ch_params(i,&decCreatePrms,FALSE);
        status = System_linkControl(gVdecModuleContext.decId, DEC_LINK_CMD_CREATE_CHANNEL,
                          &decCreatePrms, sizeof(decCreatePrms), TRUE);
        OSA_assert(status == OSA_SOK);
    }
    #endif

    selectChInfo.outQueId = PRE_DEI_SELECT_LINK_DEI_D1_DISABLE_QIDX;
    selectChInfo.numOutCh = 0;
    status = System_linkControl(gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX],
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectChInfo,
                                sizeof(selectChInfo),
                                TRUE);
    OSA_assert(status == OSA_SOK);

    selectChInfo.outQueId = PRE_DEI_SELECT_LINK_DEI_D1_ENABLE_QIDX;
    selectChInfo.numOutCh = gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsD1;
    for (i = 0; i < selectChInfo.numOutCh;i++)
    {
        selectChInfo.inChNum[i] = SD_CAPTURE_START_CHANNEL_NUM + i;
    }
    status = System_linkControl(gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX],
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectChInfo,
                                sizeof(selectChInfo),
                                TRUE);

    if (TRUE == gHDSDIDVRUsecaseContext.isProgressiveHDCapture)
    {
        selectChInfo.outQueId = PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX;
    }
    else
    {
        selectChInfo.outQueId = PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX;
    }
    selectChInfo.numOutCh = 0;
    status = System_linkControl(gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX],
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectChInfo,
                                sizeof(selectChInfo),
                                TRUE);
    OSA_assert(status == OSA_SOK);
    if (TRUE == gHDSDIDVRUsecaseContext.isProgressiveHDCapture)
    {
        selectChInfo.outQueId = PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX;
    }
    else
    {
        selectChInfo.outQueId = PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX;
    }

    selectChInfo.numOutCh = gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsHD;
    for (i = 0; i < selectChInfo.numOutCh;i++)
    {
        selectChInfo.inChNum[i] = HD_CAPTURE_START_CHANNEL_NUM + i;
        #ifdef MULTICH_HDSDIDVR_SIMULATE_HD_CAPTURE
        selectChInfo.inChNum[i] = SIMULATE_HD_CAPTURE_START_CHANNEL_NUM + i;
        #endif
    }
    status = System_linkControl(gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX],
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectChInfo,
                                sizeof(selectChInfo),
                                TRUE);


    selectChInfo.outQueId = 0;
    selectChInfo.numOutCh = gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsD1 + gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsHD;
    encodeSelectStartCh = PRE_ENCODE_SELECT_PROGRESSIVE_INPUT_INQUEIDX * selectChInfo.numOutCh;
    for (i = 0; i < selectChInfo.numOutCh;i++)
    {
        selectChInfo.inChNum[i] = encodeSelectStartCh + i;
        if (i == HD_DECODE_START_CH)
        {
            if (gHDSDIDVRUsecaseContext.isProgressiveHDCapture)
            {
                /* If progressive HD capture we should get the HD from DEI_BYPASS queue index */
                selectChInfo.inChNum[i] = (PRE_ENCODE_SELECT_INTERLACED_INPUT_INQUEIDX * selectChInfo.numOutCh) + i;
            }
        }
    }
    status = System_linkControl(gHDSDIDVRUsecaseContext.selectId[PRE_ENCODE_SELECT_LINK_IDX],
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectChInfo,
                                sizeof(selectChInfo),
                                TRUE);
    OSA_assert(status == 0);
}

static
Void multich_hdsdidvr_select_all_interlaced_encode()
{
    SelectLink_OutQueChInfo selectChInfo;
    Int32 status;
    Int i;
    UInt32 encodeSelectStartCh;
    EncLink_ChSwitchCodecTypeParams encSwitchParams;
    UInt32 numChToSwitch = 0;

    selectChInfo.outQueId = PRE_DEI_SELECT_LINK_DEI_D1_ENABLE_QIDX;
    selectChInfo.numOutCh = 0;
    status = System_linkControl(gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX],
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectChInfo,
                                sizeof(selectChInfo),
                                TRUE);
    OSA_assert(status == OSA_SOK);

    selectChInfo.outQueId = PRE_DEI_SELECT_LINK_DEI_D1_DISABLE_QIDX;
    selectChInfo.numOutCh = gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsD1;
    for (i = 0; i < selectChInfo.numOutCh;i++)
    {
        selectChInfo.inChNum[i] =  SD_CAPTURE_START_CHANNEL_NUM + i;
    }
    status = System_linkControl(gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX],
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectChInfo,
                                sizeof(selectChInfo),
                                TRUE);
    numChToSwitch = gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsD1;

    selectChInfo.outQueId = PRE_DEI_SELECT_LINK_DEI_HD_ENABLE_QIDX;
    selectChInfo.numOutCh = 0;
    status = System_linkControl(gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX],
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectChInfo,
                                sizeof(selectChInfo),
                                TRUE);
    OSA_assert(status == OSA_SOK);

    selectChInfo.outQueId = PRE_DEI_SELECT_LINK_DEI_HD_DISABLE_QIDX;
    selectChInfo.numOutCh = gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsHD;
    for (i = 0; i < selectChInfo.numOutCh;i++)
    {
        selectChInfo.inChNum[i] =  HD_CAPTURE_START_CHANNEL_NUM + i;
        #ifdef MULTICH_HDSDIDVR_SIMULATE_HD_CAPTURE
        selectChInfo.inChNum[i] = SIMULATE_HD_CAPTURE_START_CHANNEL_NUM + i;
        #endif
    }
    status = System_linkControl(gHDSDIDVRUsecaseContext.selectId[PRE_DEI_SELECT_LINK_IDX],
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectChInfo,
                                sizeof(selectChInfo),
                                TRUE);
    numChToSwitch += gHDSDIDVRUsecaseContext.numPreDeiSelectInputChannelsHD;

    selectChInfo.outQueId = 0;
    selectChInfo.numOutCh = numChToSwitch;
    encodeSelectStartCh = PRE_ENCODE_SELECT_INTERLACED_INPUT_INQUEIDX * selectChInfo.numOutCh;
    for (i = 0; i < selectChInfo.numOutCh;i++)
    {
        selectChInfo.inChNum[i] = encodeSelectStartCh + i;
    }
    status = System_linkControl(gHDSDIDVRUsecaseContext.selectId[PRE_ENCODE_SELECT_LINK_IDX],
                                SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                &selectChInfo,
                                sizeof(selectChInfo),
                                TRUE);
    for (i = 0; i < numChToSwitch; i++)
    {
        encSwitchParams.chId = i;
        encSwitchParams.switchCodecFlag = TRUE;
        encSwitchParams.algCreatePrm.overrideInputScanFormat  = TRUE;
        encSwitchParams.algCreatePrm.fieldPicEncode    = TRUE;
        multich_hdsdidvr_set_enc_ch_params(encSwitchParams.chId,&encSwitchParams.algCreatePrm);
        /* MPEG4 doesnt support field picture encoding */
        if (IVIDEO_MPEG4SP != encSwitchParams.algCreatePrm.format)
        {
            if ((!gHDSDIDVRUsecaseContext.isProgressiveHDCapture) ||
                (i != HD_DECODE_START_CH))
            {
                #ifdef MULTICH_HDSDIDVR_ENC_DEC_LOOPBACK
                {
                    DecLink_ChannelInfo params = {0};
                    params.chId = i;

                    status = System_linkControl(gVdecModuleContext.decId, DEC_LINK_CMD_DELETE_CHANNEL,
                                                &params, sizeof(params), TRUE);
                    OSA_assert(status == OSA_SOK);
                }
                #endif

                status = System_linkControl(gVencModuleContext.encId,
                                            ENC_LINK_CMD_SWITCH_CODEC_CHANNEL,
                                            &encSwitchParams,
                                            sizeof(encSwitchParams),
                                            TRUE);
                OSA_assert(status == OSA_SOK);
                #ifdef MULTICH_HDSDIDVR_ENC_DEC_LOOPBACK
                {
                    DecLink_addChannelInfo decCreatePrms = {0};

                    multich_hdsdidvr_set_dec_ch_params(i,&decCreatePrms,TRUE);
                    status = System_linkControl(gVdecModuleContext.decId, DEC_LINK_CMD_CREATE_CHANNEL,
                                       &decCreatePrms, sizeof(decCreatePrms), TRUE);
                    OSA_assert(status == OSA_SOK);
                }
                #endif
            }
        }
    }
}


Void MultiCh_hdsdidvrSwitchInterlacedProgressiveSwitch(Bool switchToInterlaced)
{
    if (switchToInterlaced)
    {
        multich_hdsdidvr_select_all_interlaced_encode(FALSE);
    }
    else
    {
        multich_hdsdidvr_select_all_progressive_encode(FALSE);
    }
}
