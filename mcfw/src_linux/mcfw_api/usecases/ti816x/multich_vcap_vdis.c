/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/
/*
                        Capture (YUV422I) 16CH D1 60fps
                          |
                          |
                        NSF (YUV420SP) -------------------+
                          |                               |
                          |                               |
  NULL SRC (YUV420SP)  DEIH (VIP-SC YUV420 )     DEI (VIP-SC YUV420 )
          |               |                               |
          +------------+  |  +----------------------------+
                       |  |  |
                       |  |  |
                        MERGE
                          |
                          |
                         DUP
                         |||
         +---------------+|+------------+
         |                |             |
         |                |             |
      SW Mosaic       SW Mosaic         |
      (SC5 YUV422I)  (SC5 YUV422I)      |
         |                |             |
 GRPX0   |       GRPX1,2  |             |
    |    |           |    |             |
    On-Chip HDMI    Off-Chip HDMI  SDTV (NTSC)
      1080p60         1080p60        480i60
*/

#include "mcfw/src_linux/mcfw_api/usecases/multich_common.h"
#include "mcfw/src_linux/devices/tw2968/src/tw2968_priv.h"
#include "mcfw/src_linux/devices/tvp5158/src/tvp5158_priv.h"

/* =============================================================================
 * Externs
 * =============================================================================
 */


#define     NUM_CAPTURE_DEVICES          (4)


static
UInt32 multich_vcapvdis_get_videodecoder_device_id()
{
    OSA_I2cHndl i2cHandle;
    Int32 status;
    UInt32 twl_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TW2968_DRV,0);
    UInt32 tvp_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TVP5158_DRV,0);
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;
    UInt32 chipId;
    UInt32 deviceId = ~0u;

    status = OSA_i2cOpen(&i2cHandle, I2C_DEFAULT_INST_ID);
    OSA_assert(status==0);

    numRegs = 0;
    regAddr[numRegs] = DEVICE_TW2968_REG_DEVICE_ID;
    regValue[numRegs] = 0;
    numRegs++;

    regAddr[numRegs] = DEVICE_TW2968_REG_REV_ID;
    regValue[numRegs] = 0;
    numRegs++;

    status = OSA_i2cRead8(&i2cHandle,twl_i2c_addr,regAddr,regValue,numRegs);
    if (status == 0)
    {
        chipId =  (((UInt32) (regValue[0]>>6)&0x3) << 5) | ( (regValue[1] >> 3) & 0x1F ) ;
        printf("\nTWL_CHIP_ID_READ:0x%x\n",chipId);
        if (chipId == DEVICE_TW2968_CHIP_ID)
        {
            deviceId = DEVICE_VID_DEC_TW2968_DRV;
        }
    }
    if (deviceId == ~0u)
    {
        numRegs = 0;
        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_MSB;
        regValue[numRegs] = 0;
        numRegs++;

        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_LSB;
        regValue[numRegs] = 0;
        numRegs++;

        status = OSA_i2cRead8(&i2cHandle,tvp_i2c_addr,regAddr,regValue,numRegs);
        if (status == 0)
        {
            chipId = ( ( UInt32 ) regValue[0] << 8 ) | regValue[1];
            printf("\nTVP_CHIP_ID_READ:0x%x\n",chipId);
            if (DEVICE_TVP5158_CHIP_ID == chipId)
            {
                deviceId = DEVICE_VID_DEC_TVP5158_DRV;
            }
        }
    }
    OSA_assert(deviceId != ~0u);
    status = OSA_i2cClose(&i2cHandle);
    OSA_assert(status==0);
    return deviceId;
}

/* =============================================================================
 * Use case code
 * =============================================================================
 */

Void MultiCh_createVcapVdis()
{
    CaptureLink_CreateParams    capturePrm;
    NsfLink_CreateParams        nsfPrm;
    DeiLink_CreateParams        deiPrm[2];
    NullSrcLink_CreateParams    nullSrcPrm;
    MergeLink_CreateParams      mergePrm;
    DupLink_CreateParams        dupPrm;
    static SwMsLink_CreateParams       swMsPrm[VDIS_DEV_MAX];
    DisplayLink_CreateParams    displayPrm[VDIS_DEV_MAX];

    CaptureLink_VipInstParams *pCaptureInstPrm;
    CaptureLink_OutParams     *pCaptureOutPrm;

    VCAP_DEVICE_CREATE_PARAM_S vidDecVideoModeArgs[NUM_CAPTURE_DEVICES];

    UInt32 grpxId[VDIS_DEV_MAX];
    UInt32 nullId;
    UInt32 mergeId, dupId;
    UInt32 deiOutQue;

    UInt32 vipInstId;
    UInt32 i;
    UInt32 numSubChains;
    Bool enableSdtv;

    UInt32 numCaptureDevices;
    UInt32 deviceId;

    for (i = 0; i < VDIS_DEV_MAX; i++)
    {
        MULTICH_INIT_STRUCT(DisplayLink_CreateParams,displayPrm[i]);
        MULTICH_INIT_STRUCT(SwMsLink_CreateParams ,swMsPrm[i]);
    }

    MultiCh_detectBoard();

    System_linkControl(
        SYSTEM_LINK_ID_M3VPSS,
        SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES,
        NULL,
        0,
        TRUE
        );

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_CAPTURE;
    gVcapModuleContext.nsfId[0]     = SYSTEM_LINK_ID_NSF_0;
    gVcapModuleContext.deiId[0]     = SYSTEM_LINK_ID_DEI_HQ_0;
    gVcapModuleContext.deiId[1]     = SYSTEM_LINK_ID_DEI_0;
    gVcapModuleContext.nullSrcId    = SYSTEM_VPSS_LINK_ID_NULL_SRC_0;
    mergeId                         = SYSTEM_VPSS_LINK_ID_MERGE_0;
    dupId                           = SYSTEM_VPSS_LINK_ID_DUP_0;

    gVdisModuleContext.swMsId[0]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_0;
    gVdisModuleContext.swMsId[1]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_1;

    swMsPrm[0].numSwMsInst   = 1;
    swMsPrm[1].numSwMsInst   = 1;
    swMsPrm[0].swMsInstId[0] = SYSTEM_SW_MS_SC_INST_SC5;
    swMsPrm[1].swMsInstId[0] = SYSTEM_SW_MS_SC_INST_SC5;

    gVdisModuleContext.displayId[0] = SYSTEM_LINK_ID_DISPLAY_0; // ON CHIP HDMI
    gVdisModuleContext.displayId[1] = SYSTEM_LINK_ID_DISPLAY_1; // OFF CHIP HDMI
    gVdisModuleContext.displayId[2] = SYSTEM_LINK_ID_DISPLAY_2; // OFF CHIP HDMI
    grpxId[0]                       = SYSTEM_LINK_ID_GRPX_0;
    grpxId[1]                       = SYSTEM_LINK_ID_GRPX_1;
#if 0    /* Enabling graphics only for ON CHIP HDMI an OFF CHIP HDMI*/
    grpxId[2]                       = SYSTEM_LINK_ID_GRPX_2;
#endif
    nullId                          = SYSTEM_VPSS_LINK_ID_NULL_0;

    numSubChains             = 2;
    deiOutQue                = DEI_LINK_OUT_QUE_VIP_SC;
    enableSdtv               = FALSE;

    CaptureLink_CreateParams_Init(&capturePrm);

    capturePrm.numVipInst    = 2 * numSubChains;
    capturePrm.outQueParams[0].nextLink = gVcapModuleContext.nsfId[0];

    capturePrm.tilerEnable              = FALSE;
    capturePrm.enableSdCrop             = FALSE;

    for(vipInstId=0; vipInstId<capturePrm.numVipInst; vipInstId++)
    {
        pCaptureInstPrm                     = &capturePrm.vipInst[vipInstId];
        pCaptureInstPrm->vipInstId          = (SYSTEM_CAPTURE_INST_VIP0_PORTA+vipInstId)%SYSTEM_CAPTURE_INST_MAX;
        pCaptureInstPrm->videoDecoderId     = SYSTEM_DEVICE_VID_DEC_TVP5158_DRV;
        pCaptureInstPrm->inDataFormat       = SYSTEM_DF_YUV422P;
        pCaptureInstPrm->standard           = SYSTEM_STD_MUX_4CH_D1;
        pCaptureInstPrm->numOutput          = 1;

        pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];
        pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV422I_YUYV;
        pCaptureOutPrm->scEnable            = FALSE;
        pCaptureOutPrm->scOutWidth          = 0;
        pCaptureOutPrm->scOutHeight         = 0;
        pCaptureOutPrm->outQueId            = 0;
    }
    deviceId = multich_vcapvdis_get_videodecoder_device_id();
    if (deviceId == DEVICE_VID_DEC_TW2968_DRV)
    {
        numCaptureDevices = 2;
    }
    else
    {
        numCaptureDevices = 4;
    }

    for(i = 0; i < numCaptureDevices; i++)
    {
        vidDecVideoModeArgs[i].deviceId         = deviceId;
        if(deviceId == DEVICE_VID_DEC_TW2968_DRV)
        {
            /* consider first device connected to VIP0 Port A and second device connected to VIP1 PortA */
            vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i*2;
            vidDecVideoModeArgs[i].numChInDevice    = 8;
        }
        else
        {
            vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i;
            vidDecVideoModeArgs[i].numChInDevice    = 4;
        }
        vidDecVideoModeArgs[i].modeParams.videoIfMode        = DEVICE_CAPT_VIDEO_IF_MODE_8BIT;
        vidDecVideoModeArgs[i].modeParams.videoDataFormat    = SYSTEM_DF_YUV422P;
        vidDecVideoModeArgs[i].modeParams.standard           = SYSTEM_STD_MUX_4CH_D1;
        vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
                    DEVICE_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_PIXEL_MUX_EMBEDDED_SYNC;
        vidDecVideoModeArgs[i].modeParams.videoSystem        =
                                      DEVICE_VIDEO_DECODER_VIDEO_SYSTEM_AUTO_DETECT;
        vidDecVideoModeArgs[i].modeParams.videoCropEnable    = FALSE;
        vidDecVideoModeArgs[i].modeParams.videoAutoDetectTimeout = -1;
    }

    Vcap_configVideoDecoder(vidDecVideoModeArgs, numCaptureDevices);

    NsfLink_CreateParams_Init(&nsfPrm);
    nsfPrm.bypassNsf                 = FALSE;
    nsfPrm.tilerEnable               = FALSE;
    nsfPrm.inQueParams.prevLinkId    = gVcapModuleContext.captureId;
    nsfPrm.inQueParams.prevLinkQueId = 0;
    nsfPrm.numOutQue                 = numSubChains;
    nsfPrm.outQueParams[0].nextLink  = gVcapModuleContext.deiId[0];
    nsfPrm.outQueParams[1].nextLink  = gVcapModuleContext.deiId[1];

    nullSrcPrm.outQueParams.nextLink      = mergeId;
    nullSrcPrm.timerPeriod                = 33;
    nullSrcPrm.inputInfo.numCh            = 16;
    for(i=0; i<nullSrcPrm.inputInfo.numCh; i++)
    {
        System_LinkChInfo *pChInfo;

        pChInfo = &nullSrcPrm.inputInfo.chInfo[i];

        pChInfo->dataFormat = SYSTEM_DF_YUV420SP_UV;
        pChInfo->memType    = SYSTEM_MT_NONTILEDMEM;
        pChInfo->startX     = 48;
        pChInfo->startY     = 32;
        pChInfo->width      = 720;
        pChInfo->height     = 576;
        pChInfo->pitch[0]   = SystemUtils_align(pChInfo->width+pChInfo->startX, SYSTEM_BUFFER_ALIGNMENT);
        pChInfo->pitch[1]   = pChInfo->pitch[0];
        pChInfo->pitch[2]   = 0;
        pChInfo->scanFormat = SYSTEM_SF_PROGRESSIVE;
    }

    for(i=0; i<numSubChains; i++)
    {
        Int32 chId;

        DeiLink_CreateParams_Init(&deiPrm[i]);

        deiPrm[i].inQueParams.prevLinkId                        = gVcapModuleContext.nsfId[0];
        deiPrm[i].inQueParams.prevLinkQueId                     = i;
        deiPrm[i].outQueParams[deiOutQue].nextLink              = mergeId;
        deiPrm[i].outQueParams[deiOutQue^1].nextLink            = nullId;
        deiPrm[i].enableOut[deiOutQue]                          = TRUE;
        deiPrm[i].enableOut[deiOutQue^1]                        = FALSE;
        deiPrm[i].tilerEnable[DEI_LINK_OUT_QUE_VIP_SC]          = FALSE;

        deiPrm[i].comprEnable                                   = FALSE;
        deiPrm[i].setVipScYuv422Format                          = FALSE;

        /* DEI Path Scalar ratio is 1:2, D1 => CIF */
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].scaleMode = DEI_SCALE_MODE_RATIO;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.widthRatio.numerator    = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.widthRatio.denominator  = 2;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.heightRatio.numerator   = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.heightRatio.denominator         = 2;
        for (chId=1; chId < DEI_LINK_MAX_CH; chId++)
            deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][chId] = deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0];

        /* VIP Scalar ratio is 1:1 */
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].scaleMode = DEI_SCALE_MODE_RATIO;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.widthRatio.numerator    = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.widthRatio.denominator  = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.heightRatio.numerator   = 1;
        deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.heightRatio.denominator     = 1;
        for (chId=1; chId < DEI_LINK_MAX_CH; chId++)
            deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId] = deiPrm[i].outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0];

        mergePrm.numInQue                     = numSubChains;
        mergePrm.inQueParams[i].prevLinkId    = gVcapModuleContext.deiId[i];
        mergePrm.inQueParams[i].prevLinkQueId = deiOutQue;
        mergePrm.outQueParams.nextLink        = dupId;
        mergePrm.notifyNextLink               = TRUE;

        dupPrm.inQueParams.prevLinkId         = mergeId;
        dupPrm.inQueParams.prevLinkQueId      = 0;
        dupPrm.numOutQue                      = numSubChains;
        dupPrm.outQueParams[i].nextLink       = gVdisModuleContext.swMsId[i];
        dupPrm.notifyNextLink                 = TRUE;
    }

    mergePrm.numInQue                     = 3;
    mergePrm.inQueParams[i].prevLinkId    = gVcapModuleContext.nullSrcId;
    mergePrm.inQueParams[i].prevLinkQueId = 0;

    for(i=0; i<numSubChains; i++)
    {
        swMsPrm[i].inQueParams.prevLinkId    = dupId;
        swMsPrm[i].inQueParams.prevLinkQueId = i;
        swMsPrm[i].outQueParams.nextLink     = gVdisModuleContext.displayId[i];
        swMsPrm[i].maxInputQueLen            = SYSTEM_SW_MS_DEFAULT_INPUT_QUE_LEN;
        swMsPrm[i].maxOutRes                 = VSYS_STD_1080P_60;
        swMsPrm[i].initOutRes                = gVdisModuleContext.vdisConfig.deviceParams[i].resolution;
        swMsPrm[i].lineSkipMode = TRUE;

        swMsPrm[i].enableLayoutGridDraw = gVdisModuleContext.vdisConfig.enableLayoutGridDraw;

        MultiCh_swMsGetDefaultLayoutPrm(0, &swMsPrm[i], FALSE); /* Since only live preview is there, show it on both displays */

        displayPrm[i].inQueParams[0].prevLinkId    = gVdisModuleContext.swMsId[i];
        displayPrm[i].inQueParams[0].prevLinkQueId = 0;
        displayPrm[i].displayRes                = swMsPrm[i].initOutRes;
    }

    if(enableSdtv)
    {
        dupPrm.numOutQue                      = 3;
        dupPrm.outQueParams[2].nextLink       = gVdisModuleContext.displayId[2];
    }

    displayPrm[2].numInputQueues = 1;
    displayPrm[2].activeQueue    = 0;
    displayPrm[2].inQueParams[0].prevLinkId    = dupId;
    displayPrm[2].inQueParams[0].prevLinkQueId = 2;
    displayPrm[2].displayRes = gVdisModuleContext.vdisConfig.deviceParams[VDIS_DEV_SD].resolution;



    capturePrm.isPalMode = Vcap_isPalMode();


    System_linkCreate (gVcapModuleContext.captureId, &capturePrm, sizeof(capturePrm));
    System_linkCreate(gVcapModuleContext.nsfId[0] , &nsfPrm, sizeof(nsfPrm));

    for(i=0; i<numSubChains; i++)
        System_linkCreate(gVcapModuleContext.deiId[i]  , &deiPrm[i], sizeof(deiPrm[i]));

    System_linkCreate(gVcapModuleContext.nullSrcId , &nullSrcPrm, sizeof(nullSrcPrm));
    System_linkCreate(mergeId   , &mergePrm  , sizeof(mergePrm));
    System_linkCreate(dupId     , &dupPrm    , sizeof(dupPrm));

    for(i=0; i<numSubChains; i++)
        System_linkCreate(gVdisModuleContext.swMsId[i]  , &swMsPrm[i], sizeof(swMsPrm[i]));

    for(i=0; i<numSubChains; i++)
        System_linkCreate(gVdisModuleContext.displayId[i], &displayPrm[i], sizeof(displayPrm[i]));

    if(enableSdtv)
    {
        System_linkCreate(gVdisModuleContext.displayId[2], &displayPrm[2], sizeof(displayPrm[2]));
    }

    MultiCh_memPrintHeapStatus();

}


Void MultiCh_deleteVcapVdis()
{
    UInt32 grpxId[VDIS_DEV_MAX];
    UInt32 nullId;
    UInt32 mergeId, dupId;
    UInt32 i;
    UInt32 numSubChains;
    UInt32 enableSdtv   = FALSE;

    numSubChains = 2;

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_CAPTURE;
    gVcapModuleContext.nsfId[0]     = SYSTEM_LINK_ID_NSF_0;
    gVcapModuleContext.deiId[0]     = SYSTEM_LINK_ID_DEI_HQ_0;
    gVcapModuleContext.deiId[1]     = SYSTEM_LINK_ID_DEI_0;
    gVcapModuleContext.nullSrcId    = SYSTEM_VPSS_LINK_ID_NULL_SRC_0;
    mergeId      = SYSTEM_VPSS_LINK_ID_MERGE_0;
    dupId        = SYSTEM_VPSS_LINK_ID_DUP_0;

    gVdisModuleContext.swMsId[0]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_0;
    gVdisModuleContext.swMsId[1]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_1;

    gVdisModuleContext.displayId[0] = SYSTEM_LINK_ID_DISPLAY_0; // ON CHIP HDMI
    gVdisModuleContext.displayId[1] = SYSTEM_LINK_ID_DISPLAY_1; // OFF CHIP HDMI
    gVdisModuleContext.displayId[2] = SYSTEM_LINK_ID_DISPLAY_2; // OFF CHIP HDMI
    grpxId[0]    = SYSTEM_LINK_ID_GRPX_0;
    grpxId[1]    = SYSTEM_LINK_ID_GRPX_1;
#if 0    /* Enabling graphics only for ON CHIP HDMI an OFF CHIP HDMI*/
    grpxId[2]    = SYSTEM_LINK_ID_GRPX_2;
#endif
    nullId       = SYSTEM_VPSS_LINK_ID_NULL_0;

    System_linkDelete(gVcapModuleContext.captureId);
    System_linkDelete(gVcapModuleContext.nsfId[0]);

    for(i=0; i<numSubChains; i++)
        System_linkDelete(gVcapModuleContext.deiId[i] );

    System_linkDelete(gVcapModuleContext.nullSrcId);
    System_linkDelete(mergeId);
    System_linkDelete(dupId);

    for(i=0; i<numSubChains; i++)
        System_linkDelete(gVdisModuleContext.swMsId[i] );

    for(i=0; i<numSubChains; i++)
        System_linkDelete(gVdisModuleContext.displayId[i]);

    if(enableSdtv)
    {
        System_linkDelete(gVdisModuleContext.displayId[2]);
    }

    Vcap_deleteVideoDecoder();
    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    MultiCh_prfLoadCalcEnable(FALSE, TRUE, FALSE);

}
