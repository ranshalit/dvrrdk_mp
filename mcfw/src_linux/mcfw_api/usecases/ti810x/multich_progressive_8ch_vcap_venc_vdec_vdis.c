/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2009 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/
/*------------------------------ D1/CIF <h.264> + QCIF <h.264> + D1 <MJPEG> -------------------------------------


                                                 Capture (YUV422I) 8CH D1 60fps
                                                              |
                                                              |
                                                        NSF_AFTER_CAPTURE
                                                            YUV420
                                                              |
              -----------------------------------------------------------------------------------------------------------
                                                       CAPTURE_DUP_LINK_IDX
              -----------------------------------------------------------------------------------------------------------
               |                       |                             |                       |                        |
               |                       |                             |                       |                        |
               |                       | 8xD1                        | 8xD1                  |                        |
               |                 ------------                        |                       |                        |
               |                 Select Link                         |                       |                        |
               |                 ------------                        |                       |                        |
               |                       | 4xD1                        |                       |                        |
               |                       |                             | DEI input at 30 fps   | 8xD1                   | 8xD1 420 30fps (Even fields)
               |           -------------------------                 |                       | 30fps                  |
 8D1 420 30fps |              DEI_0  <name-DEI2>           -------------------               | (Even fields)          |
               |                <DEI-SC1>                  DEI_1 <name-DEI3>                 |                        |
  (Even fields)|           -------------------------       DEI-SC1    VIP0_SC0               |                   --------------------
               |                       | 4xD1              -------------------               |                    DEI_HQ_1 <name-DEI1>
               |                       | 30fps                             |        ---------------------------   (VIP1-SC4)    DEI_SC2
               |                       |                                   |          DEI_HQ_0 <name-DEI0>       --------------------
               |                       |                                   |         (VIP1-SC4)    DEI_SC2            |8D1 420 1fps MJPEG
               |                       |                                   |        ---------------------------       |(YUV420)
               |                       |                                   |              |                           |
               |                       |                                   |              |                           |
               |                       |                                   |              |                           |
               |                       |                                   |              |                           |
               |                       |                                   |              |                           |
               |                       |                                   |              |                           |
               |                       |                                   |              |                           |
               |                       |                                   |              |                           |
               |                       |                                   |              |                           |
               |                       |                                   |              |                           |
               |                       |                             D1/CIF|        8QCIF |                           |
               |                       |                              H.264|        H.264 |                           |8D1 420 1fps MJPEG
               |                       |                             30fps |        30fps |                           |
               |                       |                                ---------------------------------------------------
               |                       |                                            D1_CIF_QCIF_MERGE_LINK_IDX
               |                       |                                ----------------------------------------------------
               |                       |                                         |
               |                       |                                         |
               |                       |                                         |
               |                       |                                     ALG_LINK <OSD, SCD Algs>
               |                       |                                         |
               |                       |                                         |
       CH4..11 |                       |                                         |
               |                       |CH0..3                           IPCM3OUT(VPSS)---IPCM3IN(VID)---ENC---IPCBITS_RTOSOUT(VID)---IPCBITS_HLOSIN(HOST)---FILEOUT
               |                       |                                                                                                                           |
               |                       |                                                                                                                           |
               |                       |                   CH12..19                                                                                                |
               |                       |                   ------------IPCM3IN(VPSS)------IPCM3OUT(VID)------DEC------IPCBITS_RTOSIN(VID)-------IPCBITS_HLOSOUT(HOST)
               |                       |4D1                |
        8D1    |                       |30fps              |
        30fps  |                       |                   | 8ch D1/CIF 30fps
            ----------------------------------------------------
                       LIVE_DECODE_MERGE_LINK_IDX
            ----------------------------------------------------
                                       |
                                       |CH0..19
                            ------------------------
                            LIVE_DECODE_DUP_LINK_IDX
                            ------------------------
                                      |||
                                      |||
                      +---------------+|+----------------+
                      |                                  |
                      |                                  |
                 SW Mosaic 1                       SW Mosaic 0
              (DEI-SC1 YUV422I)                     (SC5 YUV422I)
                      |                                  |
                      |                                  |
                      |                                  |
                      |                                  |
                -------------                       -------------
                  DISPLAY 1                           DISPLAY 0
                -------------                       -------------   <tied>
                   <SDTV>                          <On-Chip HDMI> --------- <Off-Chip HDMI>
                  PAL/NTSC                            1080p60                 1080p60
*/

#include "mcfw/src_linux/mcfw_api/usecases/multich_common.h"
#include "mcfw/src_linux/mcfw_api/usecases/multich_ipcbits.h"


#include "mcfw/interfaces/link_api/system_tiler.h"

#ifndef DDR_MEM_256M
#define TILER_ENABLE        TRUE
#else
#define TILER_ENABLE        FALSE
#endif
#define NUM_SCD_CHANNELS    (8)

#define TIED_DISPLAY

#ifdef TIED_DISPLAY
#define NUM_SWMS_INST       (1u)
#else
#define NUM_SWMS_INST       (2u)
#endif

/* =============================================================================
 * Externs
 * =============================================================================
 */


/* =============================================================================
 * Use case code
 * =============================================================================
 */
#ifndef DDR_MEM_256M
#define     ENABLE_NSF_AFTER_CAPTURE
#endif
#define     ENABLE_DEI_D1_PREVIEW
#define     NUM_NSF_LINK                     2
#define     NSF_AFTER_SCALAR_LINK            1
#define     NSF_AFTER_CAPTURE                0

#define     NUM_LIVE_CH                      8
#ifdef ENABLE_DEI_D1_PREVIEW
#define     NUM_DEI_LIVE_CH                  4
#else
#define     NUM_DEI_LIVE_CH                  0
#endif
#define     NUM_DEI_HALF_CH                  8
#define     NUM_DEI_D1_CH                    8
#define     NUM_DEI_QCIF_CH                  8
#define     NUM_DEI_MJPEG_CH                 8

#define     NUM_SELECT_LINK                  1
#define     DEI_LIVE_SELECT                  0

#define     NUM_DEI_LINK                     4
#define     DEI_VIP0_SC_DEI_SC_FOR_D1        0
#define     DEI_BYPASS_VIP1_SC               1
#define     DEI_VIP0_SC_DEI_SC               2
#define     DEI_BYPASS_VIP1_SC_FOR_MJPEG     3


#define     NUM_MERGE_LINK                   2
#define     D1_CIF_QCIF_MERGE_LINK_IDX       0
#define     LIVE_DECODE_MERGE_LINK_IDX       1

#define     NUM_DUP_LINK                     2
#define     CAPTURE_DUP_LINK_IDX             0
#define     LIVE_DECODE_DUP_LINK_IDX         1

#define     NUM_CAPTURE_BUFFERS              8
#define     NUM_NSF_BUFFERS                  6
#define     NUM_ENCODE_D1_BUFFERS            3
#define     NUM_ENCODE_CIF_BUFFERS           3
#define     NUM_DECODE_BUFFERS               3
#define     NUM_SWMS_MAX_BUFFERS             4
#define     NUM_PRI_ENC_IN_BUFFERS           4
#define     NUM_SEC_ENC_IN_BUFFERS           3

#define     MAX_BUFFERING_QUEUE_LEN_PER_CH    (50)

#define     BIT_BUF_LENGTH_LIMIT_FACTOR_SD    (6)

#define     NUM_IPC_LINK                     4
#define     IPC_OUT_VPSS_LINK_IDX            0
#define     IPC_IN_VPSS_LINK_IDX             1
#define     IPC_OUT_VIDEO_LINK_IDX           2
#define     IPC_IN_VIDEO_LINK_IDX            3

#define     NUM_CAPTURE_DEVICES              2

static SystemVideo_Ivahd2ChMap_Tbl systemVid_encDecIvaChMapTbl =
{
    .isPopulated = 1,
    .ivaMap[0] =
    {
        .EncNumCh  = 24,
        .EncChList = {
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 23},
        .DecNumCh  = 8,
        .DecChList = {0, 1, 2, 3, 4, 5, 6, 7},
    },
};

static const Uint32 m_D1_Ch  [NUM_DEI_D1_CH]   = {0, 1, 2, 3, 4, 5, 6, 7};
static const Uint32 m_QCIF_Ch[NUM_DEI_QCIF_CH] = {0, 1, 2, 3, 4, 5, 6, 7};

/* Whether or not each channel should be de-interlaced or not */
static Bool m_PreviewFullFpsCh[NUM_LIVE_CH];

static Bool m_prevDecRun;

static Void disableNonDisplayVdecChannels(void);



// For primary stream
static Void set_DEI_VIP0_SC_D1_CH_params  (
    DeiLink_CreateParams    *deiPrm,
    Uint32                   prevLinkId,
    Uint32                   prevLinkQ,
    Uint32                   nextLinkIdFor_DEI_SC,
    Uint32                   nextLinkIdFor_VIP_SC)
{
    Int32 chId;
    DeiLink_ChSetEvenOddEvenPatternDeiParams paramsPattern;

    deiPrm->inQueParams.prevLinkId    = prevLinkId;
    deiPrm->inQueParams.prevLinkQueId = prevLinkQ;

    for (chId = 0; chId < NUM_DEI_D1_CH; chId++)
    {
        /* Preview */
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][chId].scaleMode                     = DEI_SCALE_MODE_RATIO;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][chId].ratio.widthRatio.numerator    = 1;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][chId].ratio.widthRatio.denominator  = 1;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][chId].ratio.heightRatio.numerator   = 1;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][chId].ratio.heightRatio.denominator = 1;

        /* D1 */
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].scaleMode                     = DEI_SCALE_MODE_RATIO;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.widthRatio.numerator    = 1;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.widthRatio.denominator  = 1;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.heightRatio.numerator   = 1;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.heightRatio.denominator = 1;
    }

    deiPrm->numBufsPerCh[DEI_LINK_OUT_QUE_DEI_SC] = NUM_PRI_ENC_IN_BUFFERS;
    deiPrm->numBufsPerCh[DEI_LINK_OUT_QUE_VIP_SC] = NUM_PRI_ENC_IN_BUFFERS;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_DEI_SC] = FALSE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_VIP_SC] = TRUE;

    deiPrm->outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink = nextLinkIdFor_DEI_SC;
    deiPrm->outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = nextLinkIdFor_VIP_SC;

    deiPrm->tilerEnable[DEI_LINK_OUT_QUE_VIP_SC] = TILER_ENABLE;
    deiPrm->comprEnable          = FALSE;
    deiPrm->setVipScYuv422Format = FALSE;

    deiPrm->inputDeiFrameRate  = 60;
    deiPrm->outputDeiFrameRate = 30;

    deiPrm->inputFrameRate[DEI_LINK_OUT_QUE_DEI_SC] = 30;
    deiPrm->inputFrameRate[DEI_LINK_OUT_QUE_VIP_SC] = 30;

    deiPrm->outputFrameRate[DEI_LINK_OUT_QUE_DEI_SC] = 30;      // Preview
    deiPrm->outputFrameRate[DEI_LINK_OUT_QUE_VIP_SC] = 30;      // D1

    deiPrm->enableDeiForceBypass = FALSE;


    for (chId = 0; chId < NUM_DEI_D1_CH; chId++)
    {
        paramsPattern.chId   = chId;
        paramsPattern.enable = TRUE;
        System_linkControl(
            gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC_FOR_D1],
            DEI_LINK_CMD_SET_EVEN_ODD_EVEN_PATTERN_DEI,
            &paramsPattern,
            sizeof(paramsPattern),
            TRUE);

        printf("DEI EOE pattern is set for ch%d!\n", chId);
    }
}



// For secondary stream
static Void set_DEI_BYPASS_VIP1_SC_params(
    DeiLink_CreateParams    *deiPrm,
    Uint32                   prevLinkId,
    Uint32                   prevLinkQ,
    Uint32                   nextLinkIdFor_DEI_SC,
    Uint32                   nextLinkIdFor_VIP_SC)
{
    Int32 chId;

    deiPrm->inQueParams.prevLinkId    = prevLinkId;
    deiPrm->inQueParams.prevLinkQueId = prevLinkQ;

    for (chId = 0; chId < NUM_DEI_QCIF_CH; chId++)
    {
        /* QCIF */
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].scaleMode                     = DEI_SCALE_MODE_RATIO;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.widthRatio.numerator    = 1;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.widthRatio.denominator  = 2;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.heightRatio.numerator   = 1;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.heightRatio.denominator = 1;
    }

    deiPrm->numBufsPerCh[DEI_LINK_OUT_QUE_VIP_SC] = NUM_SEC_ENC_IN_BUFFERS;

    deiPrm->enableOut[DEI_LINK_OUT_QUE_DEI_SC] = FALSE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_VIP_SC] = TRUE;

    deiPrm->outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = nextLinkIdFor_VIP_SC;

    deiPrm->tilerEnable[DEI_LINK_OUT_QUE_VIP_SC] = FALSE;   // SCD doesn't work with TILE
    deiPrm->comprEnable           = FALSE;
    deiPrm->setVipScYuv422Format  = FALSE;

    deiPrm->inputFrameRate[DEI_LINK_OUT_QUE_VIP_SC]  = 30;
    deiPrm->outputFrameRate[DEI_LINK_OUT_QUE_VIP_SC] = 2;      // QCIF

    deiPrm->enableDeiForceBypass = TRUE;

}

// For MJPEG stream
static Void set_DEI_BYPASS_VIP1_SC_MJPEG_params(
    DeiLink_CreateParams    *deiPrm,
    Uint32                   prevLinkId,
    Uint32                   prevLinkQ,
    Uint32                   nextLinkIdFor_DEI_SC,
    Uint32                   nextLinkIdFor_VIP_SC)
{
    Int32 chId;

    deiPrm->inQueParams.prevLinkId    = prevLinkId;
    deiPrm->inQueParams.prevLinkQueId = prevLinkQ;

    for (chId = 0; chId < NUM_DEI_MJPEG_CH; chId++)
    {
        /* MJPEG */
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].scaleMode                     = DEI_SCALE_MODE_RATIO;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.widthRatio.numerator    = 1;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.widthRatio.denominator  = 1;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.heightRatio.numerator   = 2;
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][chId].ratio.heightRatio.denominator = 1;
    }

    deiPrm->numBufsPerCh[DEI_LINK_OUT_QUE_VIP_SC] = 1;

    deiPrm->enableOut[DEI_LINK_OUT_QUE_DEI_SC] = FALSE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_VIP_SC] = TRUE;

    deiPrm->outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = nextLinkIdFor_VIP_SC;

    deiPrm->tilerEnable[DEI_LINK_OUT_QUE_VIP_SC] = TILER_ENABLE;
    deiPrm->comprEnable           = FALSE;
    deiPrm->setVipScYuv422Format  = FALSE;

    deiPrm->inputFrameRate[DEI_LINK_OUT_QUE_VIP_SC]  = 30;
    deiPrm->outputFrameRate[DEI_LINK_OUT_QUE_VIP_SC] = 30;

    deiPrm->inputDeiFrameRate  = 30;
    deiPrm->outputDeiFrameRate = 1;

    deiPrm->enableDeiForceBypass = TRUE;

}



// For preview
#ifdef ENABLE_DEI_D1_PREVIEW
static Void set_DEI_VIP0_SC_DEI_SC_params (
    SelectLink_CreateParams *selectPrm,
    DeiLink_CreateParams    *deiPrm,
    Uint32                   prevLinkId,
    Uint32                   prevLinkQ,
    Uint32                   nextLinkIdFor_DEI_SC,
    Uint32                   nextLinkIdFor_VIP_SC,
    UInt32                   deiSelectId,
    UInt32                   deiId)
{
    Int32 chId;
    DeiLink_ChSetEvenOddEvenPatternDeiParams paramsPattern;

    selectPrm->numOutQue = 1;

    selectPrm->inQueParams.prevLinkId    = prevLinkId;
    selectPrm->inQueParams.prevLinkQueId = prevLinkQ;

    selectPrm->outQueParams[0].nextLink = deiId;
    selectPrm->outQueChInfo[0].outQueId = 0;
    selectPrm->outQueChInfo[0].numOutCh = NUM_DEI_LIVE_CH;

    for (chId = 0; chId < NUM_DEI_LIVE_CH; chId++)
    {
        selectPrm->outQueChInfo[0].inChNum[chId] = chId;
    }

    deiPrm->inQueParams.prevLinkId    = deiSelectId;
    deiPrm->inQueParams.prevLinkQueId = 0;

    /* Set Output Scaling at DEI based on ratio */
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].scaleMode =
        DEI_SCALE_MODE_RATIO;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.widthRatio.numerator    = 1;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.widthRatio.denominator  = 1;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.heightRatio.numerator   = 1;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0].ratio.heightRatio.denominator = 1;
    for (chId = 1; chId < DEI_LINK_MAX_CH; chId++)
    {
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][chId] =
            deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_DEI_SC][0];
    }

    deiPrm->enableOut[DEI_LINK_OUT_QUE_DEI_SC] = TRUE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_VIP_SC] = TRUE;

    deiPrm->generateBlankOut[DEI_LINK_OUT_QUE_DEI_SC] = FALSE;
    deiPrm->generateBlankOut[DEI_LINK_OUT_QUE_VIP_SC] = TRUE;

    deiPrm->outQueParams[DEI_LINK_OUT_QUE_DEI_SC].nextLink = nextLinkIdFor_DEI_SC;
    deiPrm->outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink = nextLinkIdFor_VIP_SC;

    deiPrm->tilerEnable[DEI_LINK_OUT_QUE_VIP_SC] = TILER_ENABLE;
    deiPrm->comprEnable          = FALSE;
    deiPrm->setVipScYuv422Format = FALSE;

    deiPrm->inputFrameRate[DEI_LINK_OUT_QUE_DEI_SC]  = 30;
    deiPrm->outputFrameRate[DEI_LINK_OUT_QUE_DEI_SC] = 30;

    deiPrm->inputFrameRate[DEI_LINK_OUT_QUE_VIP_SC]  = 30;
    deiPrm->outputFrameRate[DEI_LINK_OUT_QUE_VIP_SC] = 30;

    deiPrm->inputDeiFrameRate  = 60;
    deiPrm->outputDeiFrameRate = 30;

    deiPrm->enableDeiForceBypass = FALSE;

    for (chId = 0; chId < NUM_DEI_LIVE_CH; chId++)
    {
        paramsPattern.chId   = chId;
        paramsPattern.enable = TRUE;
        System_linkControl(
            gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC],
            DEI_LINK_CMD_SET_EVEN_ODD_EVEN_PATTERN_DEI,
            &paramsPattern,
            sizeof(paramsPattern),
            TRUE);

        printf("DEI EOE pattern is set for ch%d!\n", chId);
    }

}
#endif


static Void setLinkIds(
        Bool   enableAlgLink,
        UInt32 mergeId[],
        UInt32 dupId[],
        UInt32 ipcId[],
        UInt32 selectId[])
{
    gVcapModuleContext.captureId = SYSTEM_LINK_ID_CAPTURE;

    if (enableAlgLink)
    {
        gVcapModuleContext.dspAlgId[0] = SYSTEM_LINK_ID_VIDEO_ALG_0;
    }

    gVcapModuleContext.nsfId[NSF_AFTER_SCALAR_LINK]        = SYSTEM_LINK_ID_INVALID;
#ifdef ENABLE_NSF_AFTER_CAPTURE
    gVcapModuleContext.nsfId[NSF_AFTER_CAPTURE]            = SYSTEM_LINK_ID_NSF_1;
#else
    gVcapModuleContext.nsfId[NSF_AFTER_CAPTURE]            = SYSTEM_LINK_ID_INVALID;
#endif
    selectId[DEI_LIVE_SELECT]  = SYSTEM_VPSS_LINK_ID_SELECT_0;

    gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC]        = SYSTEM_LINK_ID_DEI_0;
    gVcapModuleContext.deiId[DEI_BYPASS_VIP1_SC]        = SYSTEM_LINK_ID_DEI_HQ_0;
    gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC_FOR_D1] = SYSTEM_LINK_ID_DEI_1;
    gVcapModuleContext.deiId[DEI_BYPASS_VIP1_SC_FOR_MJPEG]= SYSTEM_LINK_ID_DEI_HQ_1;

    gVcapModuleContext.sclrId[0] = SYSTEM_LINK_ID_SCLR_INST_0;
    gVencModuleContext.encId     = SYSTEM_LINK_ID_VENC_0;
    gVdecModuleContext.decId     = SYSTEM_LINK_ID_VDEC_0;

    gVdisModuleContext.swMsId[0] = SYSTEM_LINK_ID_SW_MS_MULTI_INST_0;
    gVdisModuleContext.swMsId[1] = SYSTEM_LINK_ID_SW_MS_MULTI_INST_1;

    gVdisModuleContext.displayId[0] = SYSTEM_LINK_ID_DISPLAY_0; /* ON AND OFF CHIP HDMI */
    gVdisModuleContext.displayId[1] = SYSTEM_LINK_ID_DISPLAY_2; /* SDTV */

    mergeId[LIVE_DECODE_MERGE_LINK_IDX] = SYSTEM_VPSS_LINK_ID_MERGE_0;
    mergeId[D1_CIF_QCIF_MERGE_LINK_IDX] = SYSTEM_VPSS_LINK_ID_MERGE_1;

    dupId[CAPTURE_DUP_LINK_IDX]     = SYSTEM_VPSS_LINK_ID_DUP_0;
    dupId[LIVE_DECODE_DUP_LINK_IDX] = SYSTEM_VPSS_LINK_ID_DUP_1;

    ipcId[IPC_OUT_VPSS_LINK_IDX]  = SYSTEM_VPSS_LINK_ID_IPC_OUT_M3_0;
    ipcId[IPC_IN_VIDEO_LINK_IDX]  = SYSTEM_VIDEO_LINK_ID_IPC_IN_M3_0;
    ipcId[IPC_OUT_VIDEO_LINK_IDX] = SYSTEM_VIDEO_LINK_ID_IPC_OUT_M3_0;
    ipcId[IPC_IN_VPSS_LINK_IDX]   = SYSTEM_VPSS_LINK_ID_IPC_IN_M3_0;

    gVencModuleContext.ipcBitsOutRTOSId = SYSTEM_VIDEO_LINK_ID_IPC_BITS_OUT_0;
    gVencModuleContext.ipcBitsInHLOSId  = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;
    gVdecModuleContext.ipcBitsOutHLOSId = SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0;
    gVdecModuleContext.ipcBitsInRTOSId  = SYSTEM_VIDEO_LINK_ID_IPC_BITS_IN_0;
    gVcapModuleContext.ipcBitsInHLOSId  = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_1;
}

static Void setMJPEGEncChParams(
        EncLink_CreateParams *encPrm,
        Int32                 startChNum,
        Int32                 endChNum)
{
    Int32 i;

    EncLink_ChCreateParams *pLinkChPrm;
    EncLink_ChDynamicParams *pLinkDynPrm;
    VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
    VENC_CHN_PARAMS_S *pChPrm;

    for (i=startChNum; i<endChNum; i++)
    {
         pLinkChPrm  = &encPrm->chCreateParams[i];
         pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

         pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
         pDynPrm     = &pChPrm->dynamicParam;

         pLinkChPrm->format                 = IVIDEO_MJPEG;
         pLinkChPrm->profile                = 0;
         pLinkChPrm->dataLayout             = VCODEC_FIELD_SEPARATED;
         pLinkChPrm->fieldMergeEncodeEnable = FALSE;
         pLinkChPrm->enableAnalyticinfo     = 0;
         pLinkChPrm->enableWaterMarking     = 0;
         pLinkChPrm->maxBitRate             = 0;
         pLinkChPrm->encodingPreset         = 0;
         pLinkChPrm->rateControlPreset      = 0;

         pLinkDynPrm->intraFrameInterval    = 0;
         pLinkDynPrm->targetBitRate         = 100*1000;
         pLinkDynPrm->interFrameInterval    = 0;
         pLinkDynPrm->mvAccuracy            = 0;
         pLinkDynPrm->inputFrameRate        = 1;
         pLinkDynPrm->qpMin                 = 0;
         pLinkDynPrm->qpMax                 = 0;
         pLinkDynPrm->qpInit                = -1;
         pLinkDynPrm->vbrDuration           = 0;
         pLinkDynPrm->vbrSensitivity        = 0;
    }
}



static Void setH264EncChParams(
        EncLink_CreateParams *encPrm,
        Int32                 startChNum,
        Int32                 endChNum)
{
    Int32 i;

    EncLink_ChCreateParams   *pLinkChPrm;
    EncLink_ChDynamicParams  *pLinkDynPrm;
    VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
    VENC_CHN_PARAMS_S        *pChPrm;

    for (i = startChNum; i < endChNum; i++)
    {
        pLinkChPrm  = &encPrm->chCreateParams[i];
        pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

        pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
        pDynPrm     = &pChPrm->dynamicParam;

        pLinkChPrm->format                  = IVIDEO_H264HP;
        pLinkChPrm->profile                 =
            gVencModuleContext.vencConfig.h264Profile[i];
        pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
        pLinkChPrm->enableWaterMarking      = pChPrm->enableWaterMarking;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
        pLinkChPrm->rateControlPreset       = pChPrm->rcType;
        pLinkChPrm->enableHighSpeed         = TRUE;

        pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
        pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
        pLinkDynPrm->interFrameInterval     = 1;
        pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
        pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
        pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
        pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
        pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
        pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
        pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
        pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
    }
}



static void skipOddFieldAtSC(UInt32 numChs)
{
    SclrLink_chDynamicSkipFidType prm;
    UInt32 chId;

    for (chId = 0; chId < numChs; chId++)
    {
        prm.chId = chId;
        prm.fidType = 1;

        System_linkControl(
            gVcapModuleContext.sclrId[0],
            SCLR_LINK_CMD_SKIP_FID_TYPE,
            &prm,
            sizeof(prm),
            TRUE);
    }
}



static void liveDeiFlush(UInt32 numCh, UInt32 flushChs[])
{
    DeiLink_ChFlushParams prm;
    UInt32 chId;

    for (chId = 0; chId < numCh; chId++)
    {
        prm.chId = flushChs[chId];

        System_linkControl(
            gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC],
            DEI_LINK_CMD_FLUSH_CHANNEL_INPUT,
            &prm,
            sizeof(prm),
            TRUE);
    }
}



static void setDeiHalfChs(UInt32 numDEIHalfChs, UInt32 deiHalfChs[])
{
    UInt32              chId, chDei;
    Bool                enable;
    DeiLink_ChannelInfo prm;

    for (chId = 0; chId < NUM_LIVE_CH; chId++)
    {
        enable = FALSE;
        for (chDei = 0; chDei < numDEIHalfChs; chDei++)
        {
            if (chId == deiHalfChs[chDei])
            {
                enable = TRUE;
            }
        }
        prm.channelId = chId;
        prm.streamId  = DEI_LINK_OUT_QUE_DEI_SC;
        prm.enable    = enable;
        System_linkControl(
            gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC_FOR_D1],
            enable ? DEI_LINK_CMD_ENABLE_CHANNEL : DEI_LINK_CMD_DISABLE_CHANNEL,
            &prm,
            sizeof(prm),
            TRUE);
    }
}



static void selectDeiLiveChs(UInt32 numSelectChs, UInt32 selectChs[])
{
    UInt32 selectId[NUM_SELECT_LINK], chId, numFlushChs, flushChs[NUM_DEI_LIVE_CH];
    SelectLink_OutQueChInfo prm;

    if (numSelectChs > NUM_DEI_LIVE_CH)
    {
        return;
    }

    selectId[DEI_LIVE_SELECT] = SYSTEM_VPSS_LINK_ID_SELECT_0;

    prm.outQueId = 0;
    prm.numOutCh = numSelectChs;
    for (chId = 0; chId < numSelectChs; chId++)
    {
        prm.inChNum[chId] = selectChs[chId];
    }

    System_linkControl(
        selectId[DEI_LIVE_SELECT],
        SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
        &prm,
        sizeof(prm),
        TRUE);

    numFlushChs = NUM_DEI_LIVE_CH - numSelectChs;

    for (chId = 0; chId < numFlushChs; chId++)
    {
        flushChs[chId] = (NUM_DEI_LIVE_CH - 1) - chId;
    }

    liveDeiFlush(numFlushChs, flushChs);
}



static void getDeiLiveChIds(UInt32 *numSelectChs, UInt32 selectChs[])
{
    SelectLink_OutQueChInfo prm;
    UInt32 selectId[NUM_SELECT_LINK], chId;

    selectId[DEI_LIVE_SELECT] = SYSTEM_VPSS_LINK_ID_SELECT_0;

    prm.outQueId = 0;
    prm.numOutCh = 0;

    System_linkControl(
        selectId[DEI_LIVE_SELECT],
        SELECT_LINK_CMD_GET_OUT_QUE_CH_INFO,
        &prm,
        sizeof(prm),
        TRUE);

    *numSelectChs = prm.numOutCh;
    for (chId = 0; chId < *numSelectChs; chId++)
    {
        selectChs[chId] = prm.inChNum[chId];
    }
}



static Void disableNonDisplayVdecChannels(void)
{
    UInt32                  playBackChsPresent[VDEC_CHN_MAX], numChs = 0;
    Int32                   ch, winId, i;
    UInt32                  swMsId = 0;
    SwMsLink_LayoutPrm      vdisLayoutPrm;
    VDIS_MOSAIC_S           vdMosaicParam;
    Bool                    is_dec_run = FALSE;

    swMsId = gVdisModuleContext.swMsId[0];
    System_linkControl(
            swMsId,
            SYSTEM_SW_MS_LINK_CMD_GET_LAYOUT_PARAMS,
            &(vdisLayoutPrm),
            sizeof(vdisLayoutPrm),
            TRUE);
    vdMosaicParam.numberOfWindows = vdisLayoutPrm.numWin;
    for (winId = 0; winId < vdisLayoutPrm.numWin; winId++)
    {
        vdMosaicParam.chnMap[winId] = vdisLayoutPrm.winInfo[winId].channelNum;
    }
    MultiCh_progressive8ChVcapVencVdecVdisSwmsChReMap(&vdMosaicParam);

    printf ("Playback Chs in HDMI Display  ==> ");
    for (winId = 0; winId < vdisLayoutPrm.numWin; winId++)
    {
        if ((vdMosaicParam.chnMap[winId] != VDIS_CHN_INVALID) &&
            (vdMosaicParam.chnMap[winId] >= gVencModuleContext.vencConfig.numPrimaryChn))
        {
            playBackChsPresent[numChs] =
                vdMosaicParam.chnMap[winId] -
                gVencModuleContext.vencConfig.numPrimaryChn;
            printf("%d ", playBackChsPresent[numChs]);
            numChs++;
        }
    }
    printf ("\n");

#ifndef TIED_DISPLAY
    swMsId = gVdisModuleContext.swMsId[1];
    System_linkControl(
            swMsId,
            SYSTEM_SW_MS_LINK_CMD_GET_LAYOUT_PARAMS,
            &(vdisLayoutPrm),
            sizeof(vdisLayoutPrm),
            TRUE);
    vdMosaicParam.numberOfWindows = vdisLayoutPrm.numWin;
    for (winId = 0; winId < vdisLayoutPrm.numWin; winId++)
    {
        vdMosaicParam.chnMap[winId] = vdisLayoutPrm.winInfo[winId].channelNum;
    }
    MultiCh_progressive8ChVcapVencVdecVdisSwmsChReMap(&vdMosaicParam);

    printf("Playback Chs in SD Display  ==> ");
    for (winId = 0; winId < vdisLayoutPrm.numWin; winId++)
    {
        if ((vdMosaicParam.chnMap[winId] != VDIS_CHN_INVALID) &&
            (vdMosaicParam.chnMap[winId] >=
             gVencModuleContext.vencConfig.numPrimaryChn))
        {
            playBackChsPresent[numChs] =
                vdMosaicParam.chnMap[winId] -
                gVencModuleContext.vencConfig.numPrimaryChn;
            printf("%d ", playBackChsPresent[numChs]);
            numChs++;
        }
    }
    printf("\n");
#endif

    for (ch = 0; ch < gVdecModuleContext.vdecConfig.numChn; ch++)
    {
        for (i = 0; i < numChs; i++)
        {
            if (playBackChsPresent[i] == ch)
                break;
        }
        if (i >= numChs)
        {
            Vdec_disableChn(ch);
        }
        else
        {
            /* This will enable  previously disabled channel if any */
            Vdec_enableChn(ch);
            is_dec_run = TRUE;
        }
    }
    printf ("\n");
#if 0
    /* IVA voltage/frequency down scaling for power reduction in case decoder
       doesn't have to run. */
    if (m_prevDecRun != is_dec_run)
    {
        VSYS_IVA_FREQ_VOLTAGE_S ivaFreqVoltageCmd;
        ivaFreqVoltageCmd.iva  = 0;
        ivaFreqVoltageCmd.freq = is_dec_run ? 410 : 266;
        ivaFreqVoltageCmd.voltage =
            is_dec_run ? VSYS_IVA_VOLTAGE_HIGH : VSYS_IVA_VOLTAGE_LOW;
        Vsys_setIVAFreqVoltage(&ivaFreqVoltageCmd);
    }
#endif
    m_prevDecRun = is_dec_run;
}



/* This usecase assumes secondary out, MJPEG are enabled */
Void MultiCh_createProgressive8ChVcapVencVdecVdis()
{
    CaptureLink_CreateParams          capturePrm;
    NsfLink_CreateParams              nsfPrm[NUM_NSF_LINK];
    DeiLink_CreateParams              deiPrm[NUM_DEI_LINK];
    MergeLink_CreateParams            mergePrm[NUM_MERGE_LINK];
    DupLink_CreateParams              dupPrm[NUM_DUP_LINK];
    static SwMsLink_CreateParams      swMsPrm[VDIS_DEV_MAX];
    DisplayLink_CreateParams          displayPrm[VDIS_DEV_MAX];
    IpcLink_CreateParams              ipcOutVpssPrm;
    IpcLink_CreateParams              ipcInVpssPrm;
    IpcLink_CreateParams              ipcOutVideoPrm;
    IpcLink_CreateParams              ipcInVideoPrm;
    EncLink_CreateParams              encPrm;
    DecLink_CreateParams              decPrm;
    IpcBitsOutLinkHLOS_CreateParams   ipcBitsOutHostPrm;
    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutVideoPrm;
    IpcBitsInLinkHLOS_CreateParams    ipcBitsInHostPrm[2];
    IpcBitsInLinkRTOS_CreateParams    ipcBitsInVideoPrm;
    CaptureLink_VipInstParams         *pCaptureInstPrm;
    CaptureLink_OutParams             *pCaptureOutPrm;
    IpcFramesInLinkRTOS_CreateParams  ipcFramesInDspPrm;
    IpcFramesOutLinkRTOS_CreateParams ipcFramesOutVpssPrm;
    AlgLink_CreateParams              dspAlgPrm;
    SclrLink_CreateParams             sclrPrm;
    System_LinkInfo                   bitsProducerLinkInfo;
    SelectLink_CreateParams           selectPrm[NUM_SELECT_LINK];
    UInt32                            mergeId[NUM_MERGE_LINK];
    UInt32                            dupId[NUM_DUP_LINK];
    UInt32                            ipcId[NUM_IPC_LINK];
    UInt32                            selectId[NUM_SELECT_LINK];
    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutDspPrm;
    UInt32                            ipcBitsOutDSPId;
    VCAP_DEVICE_CREATE_PARAM_S        vidDecVideoModeArgs[NUM_CAPTURE_DEVICES];
    UInt32                            vipInstId;
    UInt32                            i;
    Bool                              enableAlgLink;
    Bool                              enableOsd;
    Bool                              enableScd;
    UInt32                            chId;


    enableScd     = gVsysModuleContext.vsysConfig.enableScd;
    enableOsd     = gVsysModuleContext.vsysConfig.enableOsd;
#ifndef DDR_MEM_256M
    enableScd     = TRUE;
#else
    enableScd     = FALSE;
#endif
    enableAlgLink = enableScd | enableOsd;

    MULTICH_INIT_STRUCT(IpcLink_CreateParams,ipcOutVpssPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,ipcInVpssPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,ipcOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,ipcInVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsOutLinkHLOS_CreateParams, ipcBitsOutHostPrm);
    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams, ipcBitsOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams, ipcBitsOutDspPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams, ipcBitsInHostPrm[0]);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams, ipcBitsInHostPrm[1]);
    MULTICH_INIT_STRUCT(IpcBitsInLinkRTOS_CreateParams, ipcBitsInVideoPrm);
    MULTICH_INIT_STRUCT(DecLink_CreateParams, decPrm);
    MULTICH_INIT_STRUCT(IpcFramesInLinkRTOS_CreateParams, ipcFramesInDspPrm);
    MULTICH_INIT_STRUCT(IpcFramesOutLinkRTOS_CreateParams, ipcFramesOutVpssPrm);
    MULTICH_INIT_STRUCT(EncLink_CreateParams, encPrm);
    MULTICH_INIT_STRUCT(AlgLink_CreateParams, dspAlgPrm);
    MULTICH_INIT_STRUCT(SclrLink_CreateParams, sclrPrm);

    for (i = 0; i < VDIS_DEV_MAX; i++)
    {
        MULTICH_INIT_STRUCT(DisplayLink_CreateParams, displayPrm[i]);
        MULTICH_INIT_STRUCT(SwMsLink_CreateParams, swMsPrm[i]);
    }

    for (i = 0; i < NUM_NSF_LINK; i++)
    {
        MULTICH_INIT_STRUCT(NsfLink_CreateParams, nsfPrm[i]);
    }

    for (i = 0; i < NUM_DEI_LINK; i++)
    {
        MULTICH_INIT_STRUCT(DeiLink_CreateParams, deiPrm[i]);
    }

    for (i = 0; i < NUM_SELECT_LINK; i++)
    {
        MULTICH_INIT_STRUCT(SelectLink_CreateParams, selectPrm[i]);
    }

    printf("\n********* Entered usecase 8CH <810x> Enc/Dec OSD %s SCD %s \n\n",
            enableOsd == TRUE ? "Enabled" : "Disabled",
            enableScd == TRUE ? "Enabled" : "Disabled");

    MultiCh_detectBoard();

#if     (TILER_ENABLE == FALSE)
    /* Temp Fix - Disable tiler allocator for this usecase so that tiler memory can
       be reused for non-tiled allocation */
    SystemTiler_disableAllocator();
#endif


    System_linkControl(
            SYSTEM_LINK_ID_M3VPSS,
            SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES,
            NULL,
            0,
            TRUE);
    System_linkControl(
        SYSTEM_LINK_ID_M3VIDEO,
        SYSTEM_COMMON_CMD_SET_CH2IVAHD_MAP_TBL,
        &systemVid_encDecIvaChMapTbl,
        sizeof(SystemVideo_Ivahd2ChMap_Tbl),
        TRUE);

    ipcBitsOutDSPId = SYSTEM_DSP_LINK_ID_IPC_BITS_OUT_0;
    setLinkIds(enableAlgLink, mergeId, dupId, ipcId, selectId);

    swMsPrm[0].numSwMsInst = 1;
    swMsPrm[1].numSwMsInst = 1;
#ifdef TIED_DISPLAY
    swMsPrm[1].swMsInstId[0] = SYSTEM_SW_MS_SC_INST_SC5;
    swMsPrm[0].swMsInstId[0] = SYSTEM_SW_MS_SC_INST_DEIHQ_SC_NO_DEI;
#else
    swMsPrm[0].swMsInstId[0] = SYSTEM_SW_MS_SC_INST_SC5;
    swMsPrm[1].swMsInstId[0] = SYSTEM_SW_MS_SC_INST_DEIHQ_SC_NO_DEI;
#endif


    CaptureLink_CreateParams_Init(&capturePrm);
#ifdef ENABLE_NSF_AFTER_CAPTURE
    capturePrm.outQueParams[0].nextLink = gVcapModuleContext.nsfId[NSF_AFTER_CAPTURE];
    capturePrm.outQueParams[1].nextLink = gVdisModuleContext.displayId[1];
#else
    capturePrm.outQueParams[0].nextLink = dupId[CAPTURE_DUP_LINK_IDX];
    capturePrm.outQueParams[1].nextLink = gVdisModuleContext.displayId[1];
#endif

    /* This is for TVP5158 Audio Channels - Change it to 16 if there are 16 audio
       channels connected in cascade */
    capturePrm.numVipInst       = 2;
    capturePrm.tilerEnable      = FALSE;
    capturePrm.numBufsPerCh     = NUM_CAPTURE_BUFFERS;
    for (vipInstId = 0; vipInstId < capturePrm.numVipInst; vipInstId++)
    {
        pCaptureInstPrm                 = &capturePrm.vipInst[vipInstId];
        pCaptureInstPrm->vipInstId      = SYSTEM_CAPTURE_INST_VIP0_PORTA + vipInstId;
        pCaptureInstPrm->videoDecoderId = SYSTEM_DEVICE_VID_DEC_TVP5158_DRV;
        pCaptureInstPrm->inDataFormat   = SYSTEM_DF_YUV422P;
        pCaptureInstPrm->standard       = SYSTEM_STD_MUX_4CH_D1;
        pCaptureInstPrm->numOutput      = 1;

        pCaptureOutPrm                  = &pCaptureInstPrm->outParams[0];
        pCaptureOutPrm->dataFormat      = SYSTEM_DF_YUV422I_YUYV;
        pCaptureOutPrm->scEnable        = FALSE;
        pCaptureOutPrm->scOutWidth      = 0;
        pCaptureOutPrm->scOutHeight     = 0;
        pCaptureOutPrm->outQueId        = 0;
     }

#ifdef TIED_DISPLAY
    capturePrm.numVipInst ++;
    gVcapModuleContext.isWrbkCaptEnable = TRUE;
    pCaptureInstPrm                     = &capturePrm.vipInst[2];
    pCaptureInstPrm->vipInstId          = SYSTEM_CAPTURE_INST_SC5_WB2;
    pCaptureInstPrm->videoDecoderId     = 0;
    pCaptureInstPrm->inDataFormat       = SYSTEM_DF_YUV422P;
    pCaptureInstPrm->standard           = SYSTEM_STD_576P;
    pCaptureInstPrm->numOutput          = 1;

    pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];
    pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV422I_YUYV;
    pCaptureOutPrm->scEnable            = TRUE;
    pCaptureOutPrm->scOutWidth          = 720;
    pCaptureOutPrm->scOutHeight         = 480;
    pCaptureOutPrm->outQueId            = 1;
#endif

    for (i = 0; i < NUM_CAPTURE_DEVICES; i++)
    {
        vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i;
        vidDecVideoModeArgs[i].deviceId         = DEVICE_VID_DEC_TVP5158_DRV;
        vidDecVideoModeArgs[i].numChInDevice    = 4;

        vidDecVideoModeArgs[i].modeParams.videoIfMode            =
            DEVICE_CAPT_VIDEO_IF_MODE_8BIT;
        vidDecVideoModeArgs[i].modeParams.videoDataFormat        = SYSTEM_DF_YUV422P;
        vidDecVideoModeArgs[i].modeParams.standard               = SYSTEM_STD_MUX_4CH_D1;
        vidDecVideoModeArgs[i].modeParams.videoCaptureMode       =
            DEVICE_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_PIXEL_MUX_EMBEDDED_SYNC;
        vidDecVideoModeArgs[i].modeParams.videoSystem            =
            DEVICE_VIDEO_DECODER_VIDEO_SYSTEM_AUTO_DETECT;
        vidDecVideoModeArgs[i].modeParams.videoCropEnable        = FALSE;
        vidDecVideoModeArgs[i].modeParams.videoAutoDetectTimeout = -1;
    }
    Vcap_configVideoDecoder(vidDecVideoModeArgs, NUM_CAPTURE_DEVICES);

#ifdef ENABLE_NSF_AFTER_CAPTURE
    nsfPrm[NSF_AFTER_CAPTURE].bypassNsf                 = TRUE;
    nsfPrm[NSF_AFTER_CAPTURE].tilerEnable               = FALSE;
    nsfPrm[NSF_AFTER_CAPTURE].inQueParams.prevLinkId    =
        gVcapModuleContext.captureId;
    nsfPrm[NSF_AFTER_CAPTURE].inQueParams.prevLinkQueId = 0;
    nsfPrm[NSF_AFTER_CAPTURE].numOutQue                 = 1;
    nsfPrm[NSF_AFTER_CAPTURE].outQueParams[0].nextLink  =
        dupId[CAPTURE_DUP_LINK_IDX];
    nsfPrm[NSF_AFTER_CAPTURE].numBufsPerCh              = NUM_NSF_BUFFERS;

    dupPrm[CAPTURE_DUP_LINK_IDX].inQueParams.prevLinkId    =
        gVcapModuleContext.nsfId[NSF_AFTER_CAPTURE];
#else
    dupPrm[CAPTURE_DUP_LINK_IDX].inQueParams.prevLinkId    = gVcapModuleContext.captureId;
#endif
    dupPrm[CAPTURE_DUP_LINK_IDX].inQueParams.prevLinkQueId = 0;
    dupPrm[CAPTURE_DUP_LINK_IDX].notifyNextLink            = TRUE;
    dupPrm[CAPTURE_DUP_LINK_IDX].outQueParams[0].nextLink  =
        mergeId[LIVE_DECODE_MERGE_LINK_IDX];
    dupPrm[CAPTURE_DUP_LINK_IDX].outQueParams[1].nextLink  =
        gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC_FOR_D1];
    dupPrm[CAPTURE_DUP_LINK_IDX].outQueParams[2].nextLink  =
        gVcapModuleContext.deiId[DEI_BYPASS_VIP1_SC];
    dupPrm[CAPTURE_DUP_LINK_IDX].outQueParams[3].nextLink  =
        gVcapModuleContext.deiId[DEI_BYPASS_VIP1_SC_FOR_MJPEG];
        //gVcapModuleContext.sclrId[0];
#ifdef ENABLE_DEI_D1_PREVIEW
    dupPrm[CAPTURE_DUP_LINK_IDX].outQueParams[4].nextLink  =
        selectId[DEI_LIVE_SELECT];
    dupPrm[CAPTURE_DUP_LINK_IDX].numOutQue                 = 5;
#else
    dupPrm[CAPTURE_DUP_LINK_IDX].numOutQue                 = 4;
#endif
    /****************** 1x1 Live Preview DEI0 Settings ***********************/
#ifdef ENABLE_DEI_D1_PREVIEW
    set_DEI_VIP0_SC_DEI_SC_params(
        &selectPrm[DEI_LIVE_SELECT],
        &deiPrm[DEI_VIP0_SC_DEI_SC],
        dupId[CAPTURE_DUP_LINK_IDX],
        4,
        mergeId[LIVE_DECODE_MERGE_LINK_IDX],
        SYSTEM_LINK_ID_INVALID,
        selectId[DEI_LIVE_SELECT],
        gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC]);
#endif
    /******************  D1 DEI0 Settigs *************************************/
    set_DEI_VIP0_SC_D1_CH_params(
        &deiPrm[DEI_VIP0_SC_DEI_SC_FOR_D1],
        dupId[CAPTURE_DUP_LINK_IDX],
        1,
        SYSTEM_LINK_ID_INVALID,
        mergeId[D1_CIF_QCIF_MERGE_LINK_IDX]);

    /*************** QCIF DEI1_VIP1_SC Settings ******************************/
    set_DEI_BYPASS_VIP1_SC_params(
        &deiPrm[DEI_BYPASS_VIP1_SC],
        dupId[CAPTURE_DUP_LINK_IDX],
        2,
        SYSTEM_LINK_ID_INVALID,
        mergeId[D1_CIF_QCIF_MERGE_LINK_IDX]);

    /*************** MJPEG DEI1_VIP1_SC Settings ******************************/
    set_DEI_BYPASS_VIP1_SC_MJPEG_params(
        &deiPrm[DEI_BYPASS_VIP1_SC_FOR_MJPEG],
        dupId[CAPTURE_DUP_LINK_IDX],
        3,
        SYSTEM_LINK_ID_INVALID,
        mergeId[D1_CIF_QCIF_MERGE_LINK_IDX]);
#if 0
    /*************** 8ch/1fps MJPEG SC5 Settings *******************/
    sclrPrm.enableLineSkipSc                             = FALSE;
    sclrPrm.inputFrameRate                               = 30;
    sclrPrm.outputFrameRate                              = 1;
    sclrPrm.inQueParams.prevLinkId                       =
        dupId[CAPTURE_DUP_LINK_IDX];
    sclrPrm.inQueParams.prevLinkQueId                    = 3;
    sclrPrm.outQueParams.nextLink                        =
        gVcapModuleContext.nsfId[NSF_AFTER_SCALAR_LINK];
    sclrPrm.scaleMode                                    = DEI_SCALE_MODE_RATIO;
    sclrPrm.outScaleFactor.ratio.heightRatio.numerator   = 2;
    sclrPrm.outScaleFactor.ratio.heightRatio.denominator = 1;
    sclrPrm.outScaleFactor.ratio.widthRatio.numerator    = 1;
    sclrPrm.outScaleFactor.ratio.widthRatio.denominator  = 1;
    sclrPrm.tilerEnable                                  = FALSE; // force tiler disable;
    sclrPrm.numBufsPerCh                                 = 1;

    nsfPrm[NSF_AFTER_SCALAR_LINK].bypassNsf                 = TRUE;
    nsfPrm[NSF_AFTER_SCALAR_LINK].tilerEnable               = FALSE;//TILER_ENABLE_ENC;
    nsfPrm[NSF_AFTER_SCALAR_LINK].inQueParams.prevLinkId    =
        gVcapModuleContext.sclrId[0];
    nsfPrm[NSF_AFTER_SCALAR_LINK].inQueParams.prevLinkQueId = 0;
    nsfPrm[NSF_AFTER_SCALAR_LINK].numOutQue                 = 1;
    nsfPrm[NSF_AFTER_SCALAR_LINK].outQueParams[0].nextLink  =
        mergeId[D1_CIF_QCIF_MERGE_LINK_IDX];
    nsfPrm[NSF_AFTER_SCALAR_LINK].numBufsPerCh              = 1;
#endif
    /* CH0..7 - D1 */
    mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX].inQueParams[0].prevLinkId    =
        gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC_FOR_D1];
    mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX].inQueParams[0].prevLinkQueId =
        DEI_LINK_OUT_QUE_VIP_SC;
    /* CH0..7 - QCIF */
    mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX].inQueParams[1].prevLinkId    =
        gVcapModuleContext.deiId[DEI_BYPASS_VIP1_SC];
    mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX].inQueParams[1].prevLinkQueId =
        DEI_LINK_OUT_QUE_VIP_SC;
    /* CH0..7 - D1 FOR MJPEG */
    mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX].inQueParams[2].prevLinkId    =
        gVcapModuleContext.deiId[DEI_BYPASS_VIP1_SC_FOR_MJPEG];
        //gVcapModuleContext.nsfId[NSF_AFTER_SCALAR_LINK];
    mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX].inQueParams[2].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;

    mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX].notifyNextLink        = TRUE;
    mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX].numInQue              = 3;
    mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX].outQueParams.nextLink =
            ipcId[IPC_OUT_VPSS_LINK_IDX];

    ipcOutVpssPrm.inQueParams.prevLinkId    = mergeId[D1_CIF_QCIF_MERGE_LINK_IDX];
    ipcOutVpssPrm.inQueParams.prevLinkQueId = 0;

    ipcOutVpssPrm.numOutQue                 = 1;
    ipcOutVpssPrm.outQueParams[0].nextLink  = ipcId[IPC_IN_VIDEO_LINK_IDX];
    ipcOutVpssPrm.notifyNextLink            = TRUE;
    ipcOutVpssPrm.notifyPrevLink            = TRUE;
    ipcOutVpssPrm.noNotifyMode              = FALSE;

    ipcInVideoPrm.inQueParams.prevLinkId    = ipcId[IPC_OUT_VPSS_LINK_IDX];
    ipcInVideoPrm.inQueParams.prevLinkQueId = 0;
    ipcInVideoPrm.numOutQue                 = 1;
    if (enableAlgLink)
    {
        ipcInVideoPrm.outQueParams[0].nextLink  = gVcapModuleContext.dspAlgId[0];

        dspAlgPrm.enableOSDAlg                  = TRUE;
        dspAlgPrm.inQueParams.prevLinkId        = ipcId[IPC_IN_VIDEO_LINK_IDX];
        dspAlgPrm.inQueParams.prevLinkQueId     = 0;
        dspAlgPrm.outQueParams[ALG_LINK_FRAMES_OUT_QUE].nextLink =
                gVencModuleContext.encId;
    }
    else {
        ipcInVideoPrm.outQueParams[0].nextLink  = gVencModuleContext.encId;
    }
    ipcInVideoPrm.notifyNextLink            = TRUE;
    ipcInVideoPrm.notifyPrevLink            = TRUE;
    ipcInVideoPrm.noNotifyMode              = FALSE;

    EncLink_CreateParams_Init(&encPrm);

    encPrm.numBufPerCh[0] = NUM_ENCODE_D1_BUFFERS;
    encPrm.numBufPerCh[1] = NUM_ENCODE_CIF_BUFFERS;

    /* Primary Stream Params*/
    setH264EncChParams(
        &encPrm,
        0,
        gVencModuleContext.vencConfig.numPrimaryChn);

    /* Secondary Stream Params */
    setH264EncChParams(
        &encPrm,
        gVencModuleContext.vencConfig.numPrimaryChn,
        (gVencModuleContext.vencConfig.numPrimaryChn
         + gVencModuleContext.vencConfig.numSecondaryChn));

    /* MJPEG  Params */
    setMJPEGEncChParams(
        &encPrm,
        gVencModuleContext.vencConfig.numPrimaryChn
        + gVencModuleContext.vencConfig.numSecondaryChn,
        VENC_CHN_MAX);

    if (enableAlgLink)
    {
        encPrm.inQueParams.prevLinkId    = gVcapModuleContext.dspAlgId[0];
        encPrm.inQueParams.prevLinkQueId = ALG_LINK_FRAMES_OUT_QUE;
    }
    else
    {
        encPrm.inQueParams.prevLinkId    = ipcId[IPC_IN_VIDEO_LINK_IDX];
        encPrm.inQueParams.prevLinkQueId = 0;
    }
    encPrm.outQueParams.nextLink     = gVencModuleContext.ipcBitsOutRTOSId;

    ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkId    =
        gVencModuleContext.encId;
    ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsOutVideoPrm.baseCreateParams.numOutQue                 = 1;
    ipcBitsOutVideoPrm.baseCreateParams.outQueParams[0].nextLink  =
        gVencModuleContext.ipcBitsInHLOSId;
    MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&ipcBitsOutVideoPrm, TRUE);

    ipcBitsInHostPrm[0].baseCreateParams.inQueParams.prevLinkId    =
        gVencModuleContext.ipcBitsOutRTOSId;
    ipcBitsInHostPrm[0].baseCreateParams.inQueParams.prevLinkQueId = 0;
    MultiCh_ipcBitsInitCreateParams_BitsInHLOS(&ipcBitsInHostPrm[0]);

    if (enableOsd)
    {
        Int32 chId;

        dspAlgPrm.enableOSDAlg = TRUE;

        for (chId = 0; chId < ALG_LINK_OSD_MAX_CH; chId++)
        {
            AlgLink_OsdChWinParams * chWinPrm =
                &dspAlgPrm.osdChCreateParams[chId].chDefaultParams;

            /* set osd window max width and height */
            dspAlgPrm.osdChCreateParams[chId].maxWidth  = EXAMPLE_OSD_WIN_MAX_WIDTH;
            dspAlgPrm.osdChCreateParams[chId].maxHeight = EXAMPLE_OSD_WIN_MAX_HEIGHT;

            chWinPrm->chId       = chId;
            chWinPrm->numWindows = 0;
        }
    }
    dspAlgPrm.enableSCDAlg                                = enableScd;
    dspAlgPrm.outQueParams[ALG_LINK_SCD_OUT_QUE].nextLink = SYSTEM_LINK_ID_INVALID;

    if (enableScd)
    {
        UInt32                   i, startChId;
        AlgLink_ScdCreateParams *pScdCreatePrm;
        AlgLink_ScdChParams     *pScdChPrm;

        pScdCreatePrm = &dspAlgPrm.scdCreateParams;

        pScdCreatePrm->maxWidth                  = 176;
        pScdCreatePrm->maxHeight                 = 144;
        pScdCreatePrm->maxStride                 = 192;
        pScdCreatePrm->numValidChForSCD          = NUM_SCD_CHANNELS;
        pScdCreatePrm->numSecs2WaitB4Init        = 3;
        pScdCreatePrm->numSecs2WaitB4FrmAlert    = 1;
        pScdCreatePrm->inputFrameRate            = Vcap_isPalMode() ? 25 : 30;
        pScdCreatePrm->outputFrameRate           = 5;
        pScdCreatePrm->numSecs2WaitAfterFrmAlert = 1;
        pScdCreatePrm->enableTamperNotify        = FALSE;

        /* enable SCD only for QCIF CHs */
        startChId = 8;
        for (i = 0; i < pScdCreatePrm->numValidChForSCD; i++)
        {
            pScdChPrm = &pScdCreatePrm->chDefaultParams[i];

            pScdChPrm->chId               = startChId;
            pScdChPrm->mode               =
                ALG_LINK_SCD_DETECTMODE_MONITOR_FULL_FRAME;
            pScdChPrm->frmIgnoreLightsON  = FALSE;
            pScdChPrm->frmIgnoreLightsOFF = FALSE;
            pScdChPrm->frmSensitivity     = ALG_LINK_SCD_SENSITIVITY_VERYHIGH;
            pScdChPrm->frmEdgeThreshold   = 0;
            pScdChPrm->blkNumBlksInFrame  = 0;

            startChId++;
        }
    }


    capturePrm.isPalMode = Vcap_isPalMode();


    System_linkCreate(
            gVcapModuleContext.captureId,
            &capturePrm,
            sizeof(capturePrm));

#ifdef TIED_DISPLAY
    {
        UInt32 inW, inH, outW, outH;

        MultiCh_swMsGetOutSize(
            gVdisModuleContext.vdisConfig.deviceParams[VDIS_DEV_HDMI].resolution,
            &inW,
            &inH);
        MultiCh_swMsGetOutSize(
            gVdisModuleContext.vdisConfig.deviceParams[VDIS_DEV_SD].resolution,
            &outW,
            &outH);

        Vcap_setWrbkCaptScParams(inW, inH, outW, outH);
    }
#endif

#ifdef ENABLE_NSF_AFTER_CAPTURE
    System_linkCreate(
        gVcapModuleContext.nsfId[NSF_AFTER_CAPTURE],
        &nsfPrm[NSF_AFTER_CAPTURE],
        sizeof(nsfPrm[NSF_AFTER_CAPTURE]));
#endif
    System_linkCreate(
        dupId[CAPTURE_DUP_LINK_IDX],
        &dupPrm[CAPTURE_DUP_LINK_IDX],
        sizeof(dupPrm[CAPTURE_DUP_LINK_IDX]));
#ifdef ENABLE_DEI_D1_PREVIEW
    System_linkCreate(
        selectId[DEI_LIVE_SELECT],
        &selectPrm[DEI_LIVE_SELECT],
        sizeof(selectPrm[DEI_LIVE_SELECT]));
    System_linkCreate(
        gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC],
        &deiPrm[DEI_VIP0_SC_DEI_SC],
        sizeof(deiPrm[DEI_VIP0_SC_DEI_SC]));
#endif
    System_linkCreate(
        gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC_FOR_D1],
        &deiPrm[DEI_VIP0_SC_DEI_SC_FOR_D1],
        sizeof(deiPrm[DEI_VIP0_SC_DEI_SC_FOR_D1]));
    System_linkCreate(
        gVcapModuleContext.deiId[DEI_BYPASS_VIP1_SC],
        &deiPrm[DEI_BYPASS_VIP1_SC],
        sizeof(deiPrm[DEI_BYPASS_VIP1_SC]));
    System_linkCreate(
        gVcapModuleContext.deiId[DEI_BYPASS_VIP1_SC_FOR_MJPEG],
        &deiPrm[DEI_BYPASS_VIP1_SC_FOR_MJPEG],
        sizeof(deiPrm[DEI_BYPASS_VIP1_SC_FOR_MJPEG]));
#if 0
    System_linkCreate(gVcapModuleContext.sclrId[0], &sclrPrm, sizeof(sclrPrm));
    System_linkCreate(
        gVcapModuleContext.nsfId[NSF_AFTER_SCALAR_LINK],
        &nsfPrm[NSF_AFTER_SCALAR_LINK],
        sizeof(nsfPrm[NSF_AFTER_SCALAR_LINK]));
#endif
    System_linkCreate(
        mergeId[D1_CIF_QCIF_MERGE_LINK_IDX],
        &mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX],
        sizeof(mergePrm[D1_CIF_QCIF_MERGE_LINK_IDX]));
    System_linkCreate(
        ipcId[IPC_OUT_VPSS_LINK_IDX],
        &ipcOutVpssPrm,
        sizeof(ipcOutVpssPrm));
    System_linkCreate(
        ipcId[IPC_IN_VIDEO_LINK_IDX],
        &ipcInVideoPrm,
        sizeof(ipcInVideoPrm));
    if (enableAlgLink)
    {
        System_linkCreate(
            gVcapModuleContext.dspAlgId[0],
            &dspAlgPrm,
            sizeof(dspAlgPrm));
    }
    System_linkCreate(
        gVencModuleContext.encId,
        &encPrm,
        sizeof(encPrm));
    System_linkCreate(
        gVencModuleContext.ipcBitsOutRTOSId,
        &ipcBitsOutVideoPrm,
        sizeof(ipcBitsOutVideoPrm));
    System_linkCreate(
        gVencModuleContext.ipcBitsInHLOSId,
        &ipcBitsInHostPrm,
        sizeof(ipcBitsInHostPrm));

    System_linkGetInfo(gVencModuleContext.ipcBitsInHLOSId,&bitsProducerLinkInfo);
    OSA_assert(bitsProducerLinkInfo.numQue == 1);
    ipcBitsOutHostPrm.baseCreateParams.outQueParams[0].nextLink =
        gVdecModuleContext.ipcBitsInRTOSId;

    printf("\n\n========bitsProducerLinkInfo============\n");
    printf(
        "numQ %d, numCh %d\n",
        bitsProducerLinkInfo.numQue,
        bitsProducerLinkInfo.queInfo[0].numCh);
    {
        Int32 i;
        for (i = 0; i < bitsProducerLinkInfo.queInfo[0].numCh; i++)
        {
            printf(
                "Ch [%d] Width %d, Height %d\n",
                i,
                bitsProducerLinkInfo.queInfo[0].chInfo[i].width,
                bitsProducerLinkInfo.queInfo[0].chInfo[i].height);
        }
    }
    printf("\n====================\n\n");

    if (bitsProducerLinkInfo.queInfo[0].numCh >
            gVencModuleContext.vencConfig.numPrimaryChn)
    {
        bitsProducerLinkInfo.queInfo[0].numCh =
            gVencModuleContext.vencConfig.numPrimaryChn;
    }

    printf("Reducing bitsProducerLinkInfo.numCh to %d\n",
            bitsProducerLinkInfo.queInfo[0].numCh);

    MultiCh_ipcBitsInitCreateParams_BitsOutHLOS(
            &ipcBitsOutHostPrm,
            &bitsProducerLinkInfo.queInfo[0]);

    if (0)//(gVdecModuleContext.vdecConfig.forceUseDecChannelParams)
    {
        /* use channel info provided by user instead of from encoder */
        System_LinkChInfo *pChInfo;

        ipcBitsOutHostPrm.inQueInfo.numCh = gVdecModuleContext.vdecConfig.numChn;

        for (chId = 0; chId < ipcBitsOutHostPrm.inQueInfo.numCh; chId++)
        {
            pChInfo = &ipcBitsOutHostPrm.inQueInfo.chInfo[chId];

            /* Not Used - Start */
            pChInfo->bufType        = 0;
            pChInfo->codingformat   = 0;
            pChInfo->dataFormat     = 0;
            pChInfo->memType        = 0;
            pChInfo->startX         = 0;
            pChInfo->startY         = 0;
            pChInfo->pitch[0]       = 0;
            pChInfo->pitch[1]       = 0;
            pChInfo->pitch[2]       = 0;
            /* Not Used - End */

            pChInfo->width      =
                gVdecModuleContext.vdecConfig.decChannelParams[chId].maxVideoWidth;
            pChInfo->height     =
                gVdecModuleContext.vdecConfig.decChannelParams[chId].maxVideoHeight;
            pChInfo->scanFormat = SYSTEM_SF_PROGRESSIVE;
        }
    }

    /* ipcBitsOut params - num of buffers, bitstream buffer size */
    for (chId=0; chId<ipcBitsOutHostPrm.inQueInfo.numCh; chId++)
    {
        ipcBitsOutHostPrm.maxQueueDepth[chId] = MAX_BUFFERING_QUEUE_LEN_PER_CH;
        ipcBitsOutHostPrm.chMaxReqBufSize[chId] = IPC_BITBUF_SIZE(
                                        ipcBitsOutHostPrm.inQueInfo.chInfo[chId].width, 
                                        ipcBitsOutHostPrm.inQueInfo.chInfo[chId].height
                                         );
        ipcBitsOutHostPrm.totalBitStreamBufferSize [chId] = 
                (ipcBitsOutHostPrm.chMaxReqBufSize[chId] * BIT_BUF_LENGTH_LIMIT_FACTOR_SD);
    }

    ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkId    =
        gVdecModuleContext.ipcBitsOutHLOSId;
    ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    ipcBitsInVideoPrm.baseCreateParams.numOutQue                 = 1;
    ipcBitsInVideoPrm.baseCreateParams.outQueParams[0].nextLink  =
        gVdecModuleContext.decId;
    MultiCh_ipcBitsInitCreateParams_BitsInRTOS(&ipcBitsInVideoPrm, TRUE);

    for (i = 0; i < gVdecModuleContext.vdecConfig.numChn; i++)
    {
        decPrm.chCreateParams[i].format                 = IVIDEO_H264HP;
        decPrm.chCreateParams[i].profile                = IH264VDEC_PROFILE_ANY;
        decPrm.chCreateParams[i].processCallLevel       = VDEC_FRAMELEVELPROCESSCALL;
        decPrm.chCreateParams[i].targetMaxWidth         =
            ipcBitsOutHostPrm.inQueInfo.chInfo[i].width;
        decPrm.chCreateParams[i].targetMaxHeight        =
            ipcBitsOutHostPrm.inQueInfo.chInfo[i].height;
        decPrm.chCreateParams[i].defaultDynamicParams.targetFrameRate =
            gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.frameRate;
        decPrm.chCreateParams[i].defaultDynamicParams.targetBitRate   =
            gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.targetBitRate;
        /* Max ref frames is only 2 as this is closed loop decoder */
        decPrm.chCreateParams[i].dpbBufSizeInFrames = 1;
        decPrm.chCreateParams[i].numBufPerCh        = NUM_DECODE_BUFFERS;
        decPrm.chCreateParams[i].tilerEnable        = TILER_ENABLE;
        decPrm.chCreateParams[i].enableWaterMarking = 
            gVdecModuleContext.vdecConfig.decChannelParams[i].enableWaterMarking;
    }
    decPrm.inQueParams.prevLinkId    = gVdecModuleContext.ipcBitsInRTOSId;
    decPrm.inQueParams.prevLinkQueId = 0;
    decPrm.outQueParams.nextLink     = ipcId[IPC_OUT_VIDEO_LINK_IDX];

    ipcOutVideoPrm.inQueParams.prevLinkId    = gVdecModuleContext.decId;
    ipcOutVideoPrm.inQueParams.prevLinkQueId = 0;
    ipcOutVideoPrm.numOutQue                 = 1;
    ipcOutVideoPrm.outQueParams[0].nextLink  = ipcId[IPC_IN_VPSS_LINK_IDX];
    ipcOutVideoPrm.notifyNextLink            = TRUE;
    ipcOutVideoPrm.notifyPrevLink            = TRUE;
    ipcOutVideoPrm.noNotifyMode              = FALSE;

    ipcInVpssPrm.inQueParams.prevLinkId    = ipcId[IPC_OUT_VIDEO_LINK_IDX];;
    ipcInVpssPrm.inQueParams.prevLinkQueId = 0;
    ipcInVpssPrm.numOutQue                 = 1;
    ipcInVpssPrm.outQueParams[0].nextLink  = mergeId[LIVE_DECODE_MERGE_LINK_IDX];
    ipcInVpssPrm.notifyNextLink            = TRUE;
    ipcInVpssPrm.notifyPrevLink            = TRUE;
    ipcInVpssPrm.noNotifyMode              = FALSE;

    i = 0;
#ifdef ENABLE_DEI_D1_PREVIEW
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[i].prevLinkId    =
        gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC];   // DEI 60 fps
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[i].prevLinkQueId =
        DEI_LINK_OUT_QUE_DEI_SC;
    i++;
#endif
    /*
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[i].prevLinkId    =
        gVcapModuleContext.deiId[DEI_VIP0_SC_DEI_SC_FOR_D1];    // DEI 30 fps
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[i].prevLinkQueId =
        DEI_LINK_OUT_QUE_DEI_SC;
    i++;
    */
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[i].prevLinkId    =
        dupId[CAPTURE_DUP_LINK_IDX];    // captured even field
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[i].prevLinkQueId = 0;
    i++;
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[i].prevLinkId    =
        ipcId[IPC_IN_VPSS_LINK_IDX];    // decoded by IVA-HD
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].inQueParams[i].prevLinkQueId = 0;
    i++;
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].numInQue                     = i;
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].outQueParams.nextLink        =
        dupId[LIVE_DECODE_DUP_LINK_IDX];
    mergePrm[LIVE_DECODE_MERGE_LINK_IDX].notifyNextLink               = TRUE;

    dupPrm[LIVE_DECODE_DUP_LINK_IDX].inQueParams.prevLinkId    =
        mergeId[LIVE_DECODE_MERGE_LINK_IDX];
    dupPrm[LIVE_DECODE_DUP_LINK_IDX].inQueParams.prevLinkQueId = 0;
    dupPrm[LIVE_DECODE_DUP_LINK_IDX].numOutQue                 = 1;
    dupPrm[LIVE_DECODE_DUP_LINK_IDX].outQueParams[0].nextLink  =
        gVdisModuleContext.swMsId[0];
    dupPrm[LIVE_DECODE_DUP_LINK_IDX].outQueParams[1].nextLink  =
        gVdisModuleContext.swMsId[1];
    dupPrm[LIVE_DECODE_DUP_LINK_IDX].notifyNextLink            = TRUE;

    for (i = 0; i < NUM_SWMS_INST; i ++)
    {
        swMsPrm[i].inQueParams.prevLinkId      = dupId[LIVE_DECODE_DUP_LINK_IDX];
        swMsPrm[i].inQueParams.prevLinkQueId   = i;
        swMsPrm[i].outQueParams.nextLink       = gVdisModuleContext.displayId[i];
        swMsPrm[i].maxInputQueLen              = 4;
        swMsPrm[i].maxOutRes                   = VSYS_STD_1080P_60;
        swMsPrm[i].initOutRes                  =
            gVdisModuleContext.vdisConfig.deviceParams[i].resolution;
        swMsPrm[i].numOutBuf                   = NUM_SWMS_MAX_BUFFERS;
        swMsPrm[i].enableProcessTieWithDisplay = (i == 0) ? FALSE : FALSE;
        if (i == 1)
        {
            swMsPrm[i].numOutBuf  = NUM_SWMS_MAX_BUFFERS + 2;
            swMsPrm[i].maxOutRes  = VSYS_STD_PAL;
            swMsPrm[i].initOutRes =
                gVdisModuleContext.vdisConfig.deviceParams[VDIS_DEV_SD].resolution;
        }

#ifdef  SYSTEM_USE_TILER
        swMsPrm[i].lineSkipMode = FALSE; // Double pitch not possible in tiler mode; so Line skip not possible
#else
        swMsPrm[i].lineSkipMode = TRUE; // Set to TRUE for Enable low cost scaling
#endif
        swMsPrm[i].enableLayoutGridDraw =
            gVdisModuleContext.vdisConfig.enableLayoutGridDraw;

        MultiCh_swMsGetDefaultLayoutPrm(VDIS_DEV_HDMI, &swMsPrm[0], TRUE);
        MultiCh_swMsGetDefaultLayoutPrm(VDIS_DEV_SD,   &swMsPrm[1], TRUE);

        displayPrm[i].inQueParams[0].prevLinkId    = gVdisModuleContext.swMsId[i];
        displayPrm[i].inQueParams[0].prevLinkQueId = 0;
        displayPrm[i].displayRes                   = swMsPrm[i].initOutRes;
        if (i == 1)
        {
            displayPrm[i].displayRes                   =
                gVdisModuleContext.vdisConfig.deviceParams[VDIS_DEV_SD].resolution;
            displayPrm[i].forceFieldSeparatedInputMode = TRUE;
        }
    }

#ifdef TIED_DISPLAY
    displayPrm[1].inQueParams[0].prevLinkId    = gVcapModuleContext.captureId;
    displayPrm[1].inQueParams[0].prevLinkQueId = 1;
    displayPrm[1].displayRes                = gVdisModuleContext.vdisConfig.deviceParams[VDIS_DEV_SD].resolution;
    displayPrm[1].forceFieldSeparatedInputMode = TRUE;
#endif

    System_linkCreate(
            gVdecModuleContext.ipcBitsOutHLOSId,
            &ipcBitsOutHostPrm,
            sizeof(ipcBitsOutHostPrm));
    System_linkCreate(
            gVdecModuleContext.ipcBitsInRTOSId,
            &ipcBitsInVideoPrm,
            sizeof(ipcBitsInVideoPrm));
    System_linkCreate(gVdecModuleContext.decId, &decPrm, sizeof(decPrm));

    System_linkCreate(
            ipcId[IPC_OUT_VIDEO_LINK_IDX],
            &ipcOutVideoPrm,
            sizeof(ipcOutVideoPrm));
    System_linkCreate(
            ipcId[IPC_IN_VPSS_LINK_IDX],
            &ipcInVpssPrm,
            sizeof(ipcInVpssPrm));
    System_linkCreate(
            mergeId[LIVE_DECODE_MERGE_LINK_IDX],
            &mergePrm[LIVE_DECODE_MERGE_LINK_IDX],
            sizeof(mergePrm[LIVE_DECODE_MERGE_LINK_IDX]));
    System_linkCreate(
            dupId[LIVE_DECODE_DUP_LINK_IDX],
            &dupPrm[LIVE_DECODE_DUP_LINK_IDX],
            sizeof(dupPrm[LIVE_DECODE_DUP_LINK_IDX]));

    for (i = 0; i < NUM_SWMS_INST; i ++)
    {
        System_linkCreate(
                gVdisModuleContext.swMsId[i],
                &swMsPrm[i],
                sizeof(swMsPrm[i]));
    }

    for (i = 0; i < 2; i++)  /* Both tied VENCs HDMI and DVO2 are handled by single link instance */
    {
        System_linkCreate(
                gVdisModuleContext.displayId[i],
                &displayPrm[i],
                sizeof(displayPrm[i]));
    }

    skipOddFieldAtSC(NUM_LIVE_CH);


    MultiCh_progressive8ChVcapVencVdecVdisSwitchLayout(
            VDIS_DEV_HDMI,
            &swMsPrm[0].layoutPrm);
    MultiCh_progressive8ChVcapVencVdecVdisSwitchLayout(
            VDIS_DEV_SD,
            &swMsPrm[1].layoutPrm);
    {
        MergeLink_InLinkChInfo inChInfo;

        MergeLink_InLinkChInfo_Init(&inChInfo);
        inChInfo.inLinkID = ipcId[IPC_IN_VPSS_LINK_IDX];
        System_linkControl(mergeId[LIVE_DECODE_MERGE_LINK_IDX],
                           MERGE_LINK_CMD_GET_INPUT_LINK_CHINFO,
                           &inChInfo,
                           sizeof(inChInfo),
                           TRUE);
        OSA_assert(inChInfo.numCh == gVdecModuleContext.vdecConfig.numChn);
        MultiCh_setDec2DispMap(
                VDIS_DEV_HDMI,
                gVdecModuleContext.vdecConfig.numChn,
                0,
                inChInfo.startChNum);
        MultiCh_setDec2DispMap(
                VDIS_DEV_SD,
                gVdecModuleContext.vdecConfig.numChn,
                0,
                inChInfo.startChNum);
    }

    /* To make sure IVAHD freq/voltage is set at the beginning */
    m_prevDecRun = 2;
}



Void MultiCh_deleteProgressive8ChVcapVencVdecVdis()
{
    UInt32 i;
    UInt32 mergeId[NUM_MERGE_LINK];
    UInt32 dupId[NUM_DUP_LINK];
    UInt32 selectId[NUM_SELECT_LINK];
    UInt32 ipcOutVpssId, ipcInVpssId;
    UInt32 ipcOutVideoId, ipcInVideoId;
    UInt32 ipcBitsOutDSPId;
    Bool   enableAlgLink = gVsysModuleContext.vsysConfig.enableOsd;

    mergeId[LIVE_DECODE_MERGE_LINK_IDX] = SYSTEM_VPSS_LINK_ID_MERGE_0;
    mergeId[D1_CIF_QCIF_MERGE_LINK_IDX] = SYSTEM_VPSS_LINK_ID_MERGE_1;

    dupId[CAPTURE_DUP_LINK_IDX]         = SYSTEM_VPSS_LINK_ID_DUP_0;
    dupId[LIVE_DECODE_DUP_LINK_IDX]     = SYSTEM_VPSS_LINK_ID_DUP_1;

    selectId[DEI_LIVE_SELECT]  = SYSTEM_VPSS_LINK_ID_SELECT_0;

    ipcOutVpssId = SYSTEM_VPSS_LINK_ID_IPC_OUT_M3_0;
    ipcInVideoId = SYSTEM_VIDEO_LINK_ID_IPC_IN_M3_0;
    ipcOutVideoId= SYSTEM_VIDEO_LINK_ID_IPC_OUT_M3_0;
    ipcInVpssId  = SYSTEM_VPSS_LINK_ID_IPC_IN_M3_0;

    ipcBitsOutDSPId = SYSTEM_DSP_LINK_ID_IPC_BITS_OUT_0;

    gVdecModuleContext.ipcBitsOutHLOSId = SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0;
    gVdecModuleContext.ipcBitsInRTOSId = SYSTEM_VIDEO_LINK_ID_IPC_BITS_IN_0;

#if 1
    System_linkDelete(gVcapModuleContext.captureId);
    for (i = 0; i < NUM_NSF_LINK; i++)
    {
        if (gVcapModuleContext.nsfId[i] != SYSTEM_LINK_ID_INVALID)
        {
            System_linkDelete(gVcapModuleContext.nsfId[i]);
        }
    }

    for (i = 0; i < NUM_DEI_LINK; i++)
    {
        if (gVcapModuleContext.deiId[i] != SYSTEM_LINK_ID_INVALID)
        {
            System_linkDelete(gVcapModuleContext.deiId[i]);
        }
    }


    //System_linkDelete(gVcapModuleContext.sclrId[0]);
#else
    if (gVsysModuleContext.vsysConfig.enableScd)
    {
        System_linkDelete(ipcBitsOutDSPId);
    }

    Vcap_delete();
#endif
    for (i = 0; i < NUM_SELECT_LINK; i++)
    {
        System_linkDelete(selectId[i]);
    }

    System_linkDelete(ipcOutVpssId );
    System_linkDelete(ipcInVideoId );

    if (enableAlgLink)
    {
        System_linkDelete(gVcapModuleContext.dspAlgId[0]);
    }

    System_linkDelete(gVencModuleContext.encId);
    System_linkDelete(gVencModuleContext.ipcBitsOutRTOSId);
    System_linkDelete(gVencModuleContext.ipcBitsInHLOSId);
    System_linkDelete(gVdecModuleContext.ipcBitsOutHLOSId);
    System_linkDelete(gVdecModuleContext.ipcBitsInRTOSId);
    System_linkDelete(gVdecModuleContext.decId);

    System_linkDelete(ipcOutVideoId);
    System_linkDelete(ipcInVpssId  );

    for (i = 0; i < NUM_SWMS_INST; i ++)
    {
        System_linkDelete(gVdisModuleContext.swMsId[i]);
    }

    for (i = 0; i < 2; i++)
    {
        System_linkDelete(gVdisModuleContext.displayId[i]);
    }

    for (i = 0; i < NUM_DUP_LINK; i++)
    {
        if (dupId[i] != SYSTEM_LINK_ID_INVALID)
        {
            System_linkDelete(dupId[i]);
        }
    }
    for (i = 0; i < NUM_MERGE_LINK; i++)
    {
        if (mergeId[i] != SYSTEM_LINK_ID_INVALID)
        {
           System_linkDelete(mergeId[i]);
        }
    }

    if (gVsysModuleContext.enableFastUsecaseSwitch == FALSE)
    {
        Vcap_deleteVideoDecoder();
    }

    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    MultiCh_prfLoadCalcEnable(FALSE, TRUE, FALSE);

#if     (TILER_ENABLE == FALSE)
    /* Reenable tiler allocator that was disabled by this usecase */
    SystemTiler_enableAllocator();
#endif
}



Void MultiCh_progressive8ChVcapVencVdecVdisSetSWMSOutputFPS(
        SwMsLink_LayoutPrm *pSwMsLayout)
{
    Bool livePresent = FALSE;

    /* Set the requestFPS to 60/50 only if 1x1 layout */
    if (pSwMsLayout->outputFPS >= 50)
    {
        /** since in 8CH use-case DEI will never give 60fps **/
        #if 0
        if (pSwMsLayout->numWin == 1)
        {
            if (pSwMsLayout->winInfo[0].channelNum < gVcapModuleContext.numChannels)
            {
                livePresent = TRUE;
            }
        }
        #endif
        if (livePresent == FALSE)
        {
            pSwMsLayout->outputFPS = pSwMsLayout->outputFPS / 2;
        }
    }

    printf("++++++++++ LIVE CHANNEL %s.... Setting SWMS outputFPS to %d\n",
            livePresent == TRUE ? "PRESENT" : "NOT PRESENT",
            pSwMsLayout->outputFPS);

}



Int32 MultiCh_progressive8ChVcapVencVdecVdisSwitchLayout(
        VDIS_DEV            vdDevId,
        SwMsLink_LayoutPrm *pSwMsLayout)
{
    UInt32 winId, chId, channelId;
    UInt32 swMsId    = SYSTEM_LINK_ID_INVALID;
    UInt32 displayId = SYSTEM_LINK_ID_INVALID;
    Bool   changeDisplayInputMode = FALSE;
    UInt32 numSelectLiveChs, numDEIHalfChs;
    UInt32 selectLiveChs[NUM_DEI_LIVE_CH];
    UInt32 selectDEIHalfChs[NUM_DEI_HALF_CH];

    if (vdDevId == VDIS_DEV_HDMI ||
        vdDevId == VDIS_DEV_DVO2 ||
        vdDevId == VDIS_DEV_HDCOMP)
    {
        swMsId = gVdisModuleContext.swMsId[0];
    }
    else if (vdDevId == VDIS_DEV_SD)
    {
#ifdef TIED_DISPLAY
        swMsId = SYSTEM_LINK_ID_INVALID;
        return -1;
#endif
        swMsId = gVdisModuleContext.swMsId[1];

        /* for DM814x, switch to 60fps input, interlaced 60fps output when
            psVdMosaicParam->outputFPS is set to 60fps */
        changeDisplayInputMode = TRUE;
        displayId = gVdisModuleContext.displayId[1];
    }
    else
    {
        swMsId = SYSTEM_LINK_ID_INVALID;
        return -1;
    }

    MultiCh_progressive8ChVcapVencVdecVdisSetSWMSOutputFPS(pSwMsLayout);

    /*
        At SW Moisac,
         0       DEI LIVE output ch - only for 1x1 preview
         1.. 8 - DEI output CHs - can be dynamically mapped by select link
         9..16 - Capture CHs
        17..24 - Decode CHs

        For user,
        0..7  - Capture CHs
        8..15 - Decode CHs
    */

    numSelectLiveChs = numDEIHalfChs = 0;

    memset(m_PreviewFullFpsCh, 0, sizeof(m_PreviewFullFpsCh));
    channelId = 0;
    for (winId = 0; winId < pSwMsLayout->numWin; winId++)
    {
        chId = pSwMsLayout->winInfo[winId].channelNum;
        printf("++++++++ chId %d\n", chId);
        printf("++++++++ %s\n",
                pSwMsLayout->winInfo[winId].bypass ? "BYPASS" : "DEI");

        if (chId != SYSTEM_SW_MS_INVALID_ID)
        {
            /* valid Ch mapping */
            if (chId < NUM_LIVE_CH                          &&
                pSwMsLayout->winInfo[winId].bypass == FALSE &&
                numSelectLiveChs < NUM_DEI_LIVE_CH          &&
                (pSwMsLayout->numWin == 4 || pSwMsLayout->numWin == 1))
            {
                /* Use DEI 60fps Chs */
                selectLiveChs[numSelectLiveChs] = chId;
                //chId                            = 0;
                m_PreviewFullFpsCh[channelId]     = TRUE;
                channelId = chId;
                numSelectLiveChs++;

                printf("++++++++ DEI 60 fps\n");
                printf("++++++++ winId %d => chId %d\n", winId, channelId);
            }
            else if (chId < NUM_LIVE_CH                          &&
                     pSwMsLayout->winInfo[winId].bypass == FALSE &&
                     numDEIHalfChs < NUM_DEI_HALF_CH)
            {
                /* Use DEI 30fps Chs */
                selectDEIHalfChs[numDEIHalfChs] = chId;
                channelId = chId + NUM_DEI_LIVE_CH;
                numDEIHalfChs++;
                printf("++++++++ DEI 30 fps\n");
                printf("++++++++ winId %d => chId %d\n", winId, channelId);
            }
            else
            {
                /* Playback CH or dont need DEI'ed Chs */
                //chId += NUM_DEI_LIVE_CH;// + NUM_DEI_HALF_CH;
                channelId = chId + NUM_DEI_LIVE_CH;
                printf("++++++++ Captured or Decoder\n");
                printf("++++++++ winId %d => chId %d\n", winId, channelId);
            }
        }

        pSwMsLayout->winInfo[winId].channelNum = channelId;

    }

    if (vdDevId != VDIS_DEV_SD)
    {
        /* Dont select CHs for SD display, since HDMI will select the CHs and SD
           display should not override them */
        selectDeiLiveChs(numSelectLiveChs, selectLiveChs);
        setDeiHalfChs(numDEIHalfChs, selectDEIHalfChs);
    }

    System_linkControl(
            swMsId,
            SYSTEM_SW_MS_LINK_CMD_SWITCH_LAYOUT,
            pSwMsLayout,
            sizeof(*pSwMsLayout), TRUE);

    if (changeDisplayInputMode)
    {
        DisplayLink_SwitchInputMode prm;

        prm.enableFieldSeparatedInputMode =
            (pSwMsLayout->outputFPS >= 50) ?  TRUE : FALSE;

        System_linkControl(
                displayId,
                DISPLAY_LINK_CMD_SWITCH_INPUT_MODE,
                &prm,
                sizeof(prm),
                TRUE);
    }

    /* Disable Vdec Channels which are not in display */
    disableNonDisplayVdecChannels();

    return 0;
}



void MultiCh_progressive8ChVcapVencVdecVdisSwmsChReMap(
        VDIS_MOSAIC_S *psVdMosaicParam)
{
    /*
        SW Moisac Ch ID     -> McFW CH ID
        >= NUM_DEI_LIVE_CH     swMsChId  - NUM_DEI_LIVE_CH
        <  NUM_DEI_LIVE_CH      swMsChId
     */
    Int32  winId, chId;
    UInt32 numSelectChs;
    UInt32 selectChs[NUM_DEI_LIVE_CH];

    getDeiLiveChIds(&numSelectChs, selectChs);

    for (winId = 0; winId < psVdMosaicParam->numberOfWindows; winId++)
    {
        chId = psVdMosaicParam->chnMap[winId];

        if (chId >= NUM_DEI_LIVE_CH)
        {
            if (chId != (UInt32)(VDIS_CHN_INVALID) )
            {
                chId -= NUM_DEI_LIVE_CH;
            }
        }
        else
        {
            if (chId < numSelectChs)
            {
                chId = selectChs[chId];
            }
        }

        psVdMosaicParam->chnMap[winId] = chId;

        printf("++++++++ Remap winId %d chId %d\n", winId, chId);
    }
}



/*
    At capture level, channels
              0 ~ 3,  strmId = 0 map to De-interlaced channels <frame rate control not possible>
              4 ~ 11, strmId = 0 map to capture channels <frame rate control not possible>
              0 ~ 7,  strmId = 1 map to D1+CIF channels from DEI_HQ
              8 ~ 15, strmId = 2 map to CIF channels from DEI_HQ
 */
Int32 MultiCh_progressive8ChVcapVencVdecVdisSetCapFrameRate(
        VCAP_CHN  vcChnId,
        VCAP_STRM vcStrmId,
        Int32     inputFrameRate,
        Int32     outputFrameRate)
{
    DeiLink_ChFpsParams                      paramsFps;
    Int32                                    status = ERROR_FAIL;
    UInt32                                   dei    = DEI_VIP0_SC_DEI_SC;

    paramsFps.chId            = vcChnId;
    paramsFps.inputFrameRate  = inputFrameRate;
    paramsFps.outputFrameRate = outputFrameRate;

    switch (vcStrmId)
    {
    case 0:
        dei                = DEI_VIP0_SC_DEI_SC;
        paramsFps.streamId = DEI_LINK_OUT_QUE_DEI_SC;
        if(paramsFps.inputFrameRate >= 50)
        {
            /* make it 25fps or 30fps since DEI output is never more than this in this usecase */
            paramsFps.inputFrameRate /= 2;
        }
        break;
    case 1:
        dei                = DEI_VIP0_SC_DEI_SC_FOR_D1;
        paramsFps.streamId = DEI_LINK_OUT_QUE_VIP_SC;
        break;
    case 2:
        dei                = DEI_BYPASS_VIP1_SC;
        paramsFps.streamId = DEI_LINK_OUT_QUE_VIP_SC;
        break;
    }

    if (gVcapModuleContext.deiId[dei] != SYSTEM_LINK_ID_INVALID)
    {
        status = System_linkControl(
                gVcapModuleContext.deiId[dei],
                DEI_LINK_CMD_SET_FRAME_RATE,
                &paramsFps,
                sizeof(paramsFps),
                TRUE);
    }

    return status;
}



Int32 MultiCh_progressive8ChVcapVencVdecVdis_enableDisableCapChn(
        VCAP_CHN  vcChnId,
        VCAP_STRM vcStrmId,
        Bool      enableChn)
{
    DeiLink_ChannelInfo channelInfo;
    char  *onOffName[] = { "OFF ", "ON" };
    UInt32 cmdId;
    Int32  status = ERROR_FAIL;
    Int32  deiIdx;

    if (enableChn)
        cmdId = DEI_LINK_CMD_ENABLE_CHANNEL;
    else
        cmdId = DEI_LINK_CMD_DISABLE_CHANNEL;

    switch (vcStrmId)
    {
        case 0:
            printf("Preview path enable / disable not possible\n");
            break;

        case 1:
        case 2:
            if (vcChnId == 0 || vcChnId == 1)
                deiIdx = DEI_VIP0_SC_DEI_SC_FOR_D1;
            else
                deiIdx = DEI_BYPASS_VIP1_SC;

            if (vcStrmId == 1)
                channelInfo.streamId  = DEI_LINK_OUT_QUE_VIP_SC;
            else
                channelInfo.streamId  = DEI_LINK_OUT_QUE_DEI_SC;
            channelInfo.channelId = vcChnId;
            channelInfo.enable    = enableChn;
            if (gVcapModuleContext.deiId[deiIdx] !=
                    SYSTEM_LINK_ID_INVALID)
            {
                status = System_linkControl(
                        gVcapModuleContext.deiId[deiIdx],
                        cmdId,
                        &(channelInfo),
                        sizeof(DeiLink_ChannelInfo),
                        TRUE);
            }
            printf(" VCAP: CH%d STRM%d = [%s]\n",
                    vcChnId,
                    vcStrmId,
                    onOffName[enableChn]);
            break;
    }
    return status;
}



Int32 MultiCh_progressive8ChVcapVencVdecVdis_setCapDynamicParamChn(
        VCAP_CHN                  vcChnId,
        VCAP_CHN_DYNAMIC_PARAM_S *psCapChnDynaParam,
        VCAP_PARAMS_E             paramId)
{
    DeiLink_chDynamicSetOutRes params = {0};
    Int32                      status = ERROR_FAIL;
    UInt32                     maxWidth, maxHeight;
    UInt32                     deiId = DEI_BYPASS_VIP1_SC;

    params.width  = psCapChnDynaParam->chDynamicRes.width;
    params.height = psCapChnDynaParam->chDynamicRes.height;
    params.queId  = DEI_LINK_OUT_QUE_VIP_SC;

    if (paramId == VCAP_RESOLUTION)
    {
        /* Validate parameters */
        if (psCapChnDynaParam->chDynamicRes.pathId == 1)
        {
            deiId       = DEI_VIP0_SC_DEI_SC_FOR_D1;
            params.chId = vcChnId;
            maxWidth    = 704;
            maxHeight   = (Vcap_isPalMode() == TRUE) ? 576 : 480;
            if (params.width > maxWidth || params.height > maxHeight)
            {
                printf("Ch%d (%dx%d) : Max resolution is D1 !!!!!!\n",
                        vcChnId,
                        params.width,
                        params.height);
                return ERROR_FAIL;
            }
        }
        else
        {
            deiId       = DEI_BYPASS_VIP1_SC;
            params.chId = vcChnId;
            maxWidth    = 352;
            maxHeight   = (Vcap_isPalMode() == TRUE) ? 288 : 240;
            if (params.width > maxWidth || params.height > maxHeight)
            {
                printf("Ch%d (%dx%d) : Max resolution is QCIF !!!!!!\n",
                        vcChnId,
                        params.width,
                        params.height);
                return ERROR_FAIL;
            }
        }
        status = System_linkControl(
                gVcapModuleContext.deiId[deiId],
                DEI_LINK_CMD_GET_OUTPUTRESOLUTION,
                &params,
                sizeof(params),
                TRUE);
        params.width  = SystemUtils_align(psCapChnDynaParam->chDynamicRes.width, 16);
        params.height = psCapChnDynaParam->chDynamicRes.height;
        printf ("Setting DEI %d, Ch %d, Q %d with new resolution - %d X %d!!!!!!\n",
            deiId, params.chId, params.queId, params.width, params.height);
        status = System_linkControl(
                gVcapModuleContext.deiId[deiId],
                DEI_LINK_CMD_SET_OUTPUTRESOLUTION,
                &(params),
                sizeof(params),
                TRUE);
    }
    return ERROR_NONE;
}



Int32 MultiCh_progressive8ChVcapVencVdecVdis_getCapDynamicParamChn(
        VCAP_CHN                  vcChnId,
        VCAP_CHN_DYNAMIC_PARAM_S *psCapChnDynaParam,
        VCAP_PARAMS_E             paramId)
{
    DeiLink_chDynamicSetOutRes params = {0};
    Int32  status = ERROR_FAIL, deiId = DEI_BYPASS_VIP1_SC;
    Uint32 ch;
    Bool   found;

    if (psCapChnDynaParam->chDynamicRes.pathId == 1)
    {
        found = FALSE;
        for (ch = 0; ch < NUM_DEI_D1_CH; ch++)
        {
            if (vcChnId == m_D1_Ch[ch])
            {
                deiId       = DEI_VIP0_SC_DEI_SC_FOR_D1;
                params.chId = ch;
                found       = TRUE;
            }
        }
        if (!found)     // one of the 6 CIF channels
        {
            deiId        = DEI_BYPASS_VIP1_SC;
            params.queId = DEI_LINK_OUT_QUE_VIP_SC;
            for (ch = 0; ch < NUM_DEI_QCIF_CH; ch++)
            {
                if (vcChnId == ch)
                {
                    params.chId = ch;
                }
            }
        }
    }
    else
    {
        params.queId = DEI_LINK_OUT_QUE_DEI_SC;
        params.chId  = vcChnId;
    }

    status = System_linkControl(
            gVcapModuleContext.deiId[deiId],
            DEI_LINK_CMD_GET_OUTPUTRESOLUTION,
            &params,
            sizeof(params),
            TRUE);
    psCapChnDynaParam->chDynamicRes.width  = params.width;
    psCapChnDynaParam->chDynamicRes.height = params.height;

    return status;
}

