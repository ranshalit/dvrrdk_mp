/*******************************************************************************
 *                                                                             *
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 *                                                                             *
 ******************************************************************************/

/**
    \file  multich_car_dvr.c
    \brief Usecase file for Car DVR usecase
*/

/* =============================================================================
 * Use case code
 * =============================================================================
 */

/** --------------------------- Car DVR usecase -----------------------------------
   +--------+              +--------+
   |        |              |        |
   |        |              |        |
   | BitsOut|              |Capture |
   |  HLOS  |              |        |
   |        |              |        |
   +---+----+              + ------ +
       |                    +      +Ch (1 - 4)
       |              1 chD1|      |
       |                    |      v
       |                    |     +-------+
       |                    |     | Dei   |
       |                    |     |YUV420 |
       |                    |     +-------+
       |                    |      +
       |              Ch0   |      |
       |                    V      V
       |                +-------+ ++--------+
       v Ch0            |       | |         |
   +--------+           |DEI    | |Composite|
   |        |           |YUV420 | |  SwMs   |
   | Decode |           +-------+ +---------+
   |        |           Ch0 |     +
   +--------+               |     |
        +                   |     |
        |                   |     |
        |                   |     |
        |                   |     |
        v                   |     |
   +---------+              |     |
   |         |            Q1|     | ch0 YUV420
   | SWMS    |              |     | Q0
   | Playback|              v     v
   +---------+              +--------+
        |                   | Merge  |
        |                   +--------+
        |                       |
        |                       |                                        Ch0,1
        |                       |                                +-------+      +------+     +--------+
        |                       |                          Ch0,1 |       | Q0   |      |     |        |
        |                       |                                |       |+---> |      |     |        |
        |                       |Ch 0,1<----<<<PROCESS_LINK>>>-->|Dup    |      |Merge |+--->| Encode |+------>
        |                       v Q1          IPC_FRAMES_OUT     |       |      |      |     |        |       Ch0
        |        Q0       +--------+                             |       |+---> |      |Ch0  |        |         1
       +----------------->| Merge  |                             |       | Q1   |      |  1  +--------+         3
                   Ch0    +--------+                             +-------+      +------+  2    Disable Ch2      4
                               +                                          Ch0,1    ^      3
                               |Ch0,1,2                                            |Q2    4
                           +--------+                                              |
                           | SELECT |                                              |
                           +--------+                                              |
                               v Ch(0/1/2)                                         | IPC_IN_M3
                          +--------+                                               |
                          |        |                                               |
                          |        |                                               |
                          |  SWOSD |                                               |
                          |        |                                               |
                          +--------+                                               |
                               +                                                   |
                               |                                                   |
                               |Ch(0/1/2)                                          |
                               v                                                   |
                         +--------+                                                |
                         |        |        Ch(0/1/2)                               |
                         |  Dup   |+-----------------------------------------------+
                         |        |     IPC_OUT_M3
                         +--------+
                              +
                              |
                              |Ch(0/1/2)
                              v
                         +--------+
                         |  SD    |
                         | Display|(Configure Display to select 0/1/2)
                         |        |
                         +--------+
*/

#include "mcfw/src_linux/mcfw_api/usecases/multich_common.h"
#include "mcfw/src_linux/mcfw_api/usecases/multich_ipcbits.h"
#include "mcfw/src_linux/devices/tw2968/src/tw2968_priv.h"
#include "mcfw/src_linux/devices/tvp5158/src/tvp5158_priv.h"
#include "mcfw/src_linux/mcfw_api/ti_vdis_priv.h"
#include "mcfw/interfaces/ti_vdis_timings.h"

#define TILER_ENABLE    FALSE


/* =============================================================================
 * Externs
 * =============================================================================
 */

/* =============================================================================
 * Use case code
 * =============================================================================
 */
static SystemVideo_Ivahd2ChMap_Tbl systemVid_encDecIvaChMapTbl =
{
    .isPopulated = 1,
    .ivaMap[0] =
    {
        .EncNumCh  = 16,
        .EncChList = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 , 14, 15},
        .DecNumCh  = 16,
        .DecChList = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 , 14, 15},
    },

};

#define     MULTICH_CAR_DVR_USECASE_MAX_NUM_LINKS       (64)
#define     MULTICH_CAR_DVR_USECASE_NUM_DISPLAY_CH      (3)

#define     MAX_NUM_CAPTURE_DEVICES                     (4)

/** Merge Link Info */
#define     NUM_MERGE_LINK                              (5)
/**  DEI CIFCOMPOSITE Merge
 *   SWMS0 ---DEI_DEI_SC_Q------->Q0--|
 *                                    |-DEI_SC_CIF_MERGE_LINK_IDX
 *   DEI0  ---DEI_VIP_SC_SEC_Q--->Q1--|
 */
#define     D1_CIFCOMPOSITE_MERGE_LINK_IDX              (0)

#define     D1_CIFCOMPOSITE_MERGE_LINK_NUM_INQUE        (2)
#define     D1_CIFCOMPOSITE_MERGE_LINK_SWMS_COMPOSE_QIDX         (0)
#define     D1_CIFCOMPOSITE_MERGE_LINK_DEI0_QIDX        (1)


/**  LIVE_PLAYBACK_MERGE
 *   SWMS_LIVE --------------------------------(Q0)--------------------->Q1--|-DEI_SC_CIF_MERGE_LINK_IDX
 *                                                                           |
 *   IPC_IN_M3 --------------------------------(Q0)--------------------->Q0--|
 */
#define     LIVE_PLAYBACK_MERGE_LINK_IDX                (1)
#define     LIVE_PLAYBACK_MERGE_LINK_NUM_INQUE          (2)
#define     LIVE_PLAYBACK_MERGE_LINK_SWMSLIVE_QIDX      (1)
#define     LIVE_PLAYBACK_MERGE_LINK_PLAYBACKCH_QIDX    (0)


/**  PRE_ENCODE_MERGE
 *   IPC_FRAMES_IN -------------------------------(Q0)------->Q0--|
 *                                                                |
                                                                  |-PRE_ENCODE_MERGE_LINK_IDX
 *                                                                |
 *   IPC_IN_M3 -----------------------------------(Q0)------->Q1--|
 */
#define     PRE_ENCODE_MERGE_LINK_IDX                   (2)
#define     PRE_ENCODE_MERGE_LINK_NUM_INQUE             (2)
#define     PRE_ENCODE_MERGE_LINK_IPCFRAMESIN_QIDX      (0)
#define     PRE_ENCODE_MERGE_LINK_IPCINM3_QIDX          (1)


/**  FG_BG_ENCODE_DUP_MERGE
 *   FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX ---(Q0)------->Q0--|
 *                                                                |
 *   FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX----(Q1)------->Q1--|-PRE_ENCODE_MERGE_LINK_IDX
 */
#define     FG_BG_ENCODE_DUP_MERGE_LINK_IDX             (3)
#define     FG_BG_ENCODE_DUP_MERGE_LINK_NUM_INQUE       (2)
#define     FG_BG_ENCODE_DUP_MERGE_LINK_FGBGDUPQ0_QIDX (0)
#define     FG_BG_ENCODE_DUP_MERGE_LINK_FGBGDUPQ1_QIDX (1)

 /**  PRE_DISPLAY_SELECT_MERGE
  *   DEI_D1_VIP_SC_OUT ---(Q0)------->Q0--|
  *                                        |-PRE_DISPLAY_SELECT_MERGE_IDX
  *   DISPLAY_SWMS_OUT  ---(Q0)------->Q1--|
  */
 #define     PRE_DISPLAY_SELECT_MERGE_LINK_IDX              (4)
 #define     PRE_DISPLAY_SELECT_MERGE_LINK_NUM_INQUE        (2)
 #define     PRE_DISPLAY_SELECT_MERGE_LINK_D1DEILIVE_QIDX   (0)
 #define     PRE_DISPLAY_SELECT_MERGE_LINK_DISPLAYSWMS_QIDX (1)

#define     NUM_DUP_LINK                                (2)
#define     FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX   (0)
#define     DISPLAY_ENCODE_DUP_LINK_IDX                 (1)

#define     NUM_SELECT_LINK                             (1)
#define     DISPLAY_SELECT_LINK_IDX                     (0)

#define     NUM_DEI_LINK                                (2)
#define     D1_DEI_LINK_IDX                             (0)
#define     CIF_DEI_LINK_IDX                            (1)

#define     NUM_VPSS_FRAMES_OUT_LINK                    (1)
#define     OSD_FRAMESOUT_LINK_IDX                      (0)

#define     NUM_OUT_M3_LINK                             (2)
#define     CAPTURE_OUT_M3_LINK_IDX                     (0)
#define     DISPLAY_OUT_M3_LINK_IDX                     (1)

#define     NUM_CAPTURE_BUFFERS                         (8)
#define     NUM_ENCODE_D1_BUFFERS                       (6)
#define     NUM_ENCODE_CIF_BUFFERS                      (6)
#define     NUM_DECODE_BUFFERS                          (6)
#define     NUM_SWMS_MAX_BUFFERS                        (8)

#define     BIT_BUF_LENGTH_LIMIT_FACTOR_SD      (6)

#define     MAX_BUFFERING_QUEUE_LEN_PER_CH           (50)

#define     BACKGROUND_CHANNEL_ENCODE_FPS               (5)

typedef enum  MultichCarDVR_LayOutId
{
    MULTICHCARDVR_LAYOUT_ID_1x1,
    MULTICHCARDVR_LAYOUT_ID_2x2
} MultichCarDVR_LayOutId;

typedef struct MultichCarDVR_Context
{
    UInt32 createdLinks[MULTICH_CAR_DVR_USECASE_MAX_NUM_LINKS];
    UInt32 createdLinkCount;
    VCAP_DEVICE_CREATE_PARAM_S vidDecVideoModeArgs[MAX_NUM_CAPTURE_DEVICES];
    AvsyncLink_LinkSynchConfigParams avsyncPrms;
    CaptureLink_CreateParams    capturePrm;
    DeiLink_CreateParams        deiPrm[NUM_DEI_LINK];
    MergeLink_CreateParams      mergePrm[NUM_MERGE_LINK];
    DupLink_CreateParams        dupPrm[NUM_DUP_LINK];
    SwMsLink_CreateParams       swMsPrm[VDIS_DEV_MAX];
    DisplayLink_CreateParams    displayPrm[VDIS_DEV_MAX];
    IpcLink_CreateParams        ipcOutVpssPrm[NUM_OUT_M3_LINK];
    IpcLink_CreateParams        ipcInVpssPrm;
    IpcLink_CreateParams        ipcOutVideoPrm;
    IpcLink_CreateParams        ipcInVideoPrm[NUM_OUT_M3_LINK];
    EncLink_CreateParams        encPrm;
    DecLink_CreateParams        decPrm;
    IpcBitsOutLinkHLOS_CreateParams   ipcBitsOutHostPrm;
    IpcBitsOutLinkRTOS_CreateParams   ipcBitsOutVideoPrm;
    IpcBitsInLinkHLOS_CreateParams    ipcBitsInHostPrm;
    IpcBitsInLinkRTOS_CreateParams    ipcBitsInVideoPrm;
    IpcFramesInLinkRTOS_CreateParams  ipcFramesInDspPrm;
    IpcFramesOutLinkRTOS_CreateParams ipcFramesOutVpssPrm[NUM_VPSS_FRAMES_OUT_LINK];
    AlgLink_CreateParams              dspAlgPrm;
    System_LinkInfo                   bitsProducerLinkInfo;
    SelectLink_CreateParams           selectPrm[NUM_SELECT_LINK];
    UInt32 mergeId[NUM_MERGE_LINK];
    UInt32 dupId[NUM_DUP_LINK];
    UInt32 ipcOutVpssId[NUM_OUT_M3_LINK];
    UInt32 ipcInVpssId;
    UInt32 ipcOutVideoId;
    UInt32 ipcInVideoId[NUM_OUT_M3_LINK];
    UInt32  captureFps;
    UInt32  selectLinkId[NUM_SELECT_LINK];
    SwMsLink_LayoutPrm   swmsLayoutPrm;
    MultichCarDVR_LayOutId displayCh2LayoutIdMap[MULTICH_CAR_DVR_USECASE_NUM_DISPLAY_CH];
}  MultichCarDVR_Context;

MultichCarDVR_Context gCarDVRUsecaseContext =
{
    .createdLinkCount           = 0,
    .displayCh2LayoutIdMap      = {MULTICHCARDVR_LAYOUT_ID_1x1,MULTICHCARDVR_LAYOUT_ID_1x1,MULTICHCARDVR_LAYOUT_ID_2x2}
};

static Void multich_cardvr_register_created_link(MultichCarDVR_Context *pContext,
                                                 UInt32 linkID)
{
    OSA_assert(pContext->createdLinkCount < OSA_ARRAYSIZE(pContext->createdLinks));
    pContext->createdLinks[pContext->createdLinkCount] = linkID;
    pContext->createdLinkCount++;
}

#define MULTICH_CARDVR_CREATE_LINK(linkID,createPrm,createPrmSize)              \
    do                                                                          \
    {                                                                           \
        System_linkCreate(linkID,createPrm,createPrmSize);                      \
        multich_cardvr_register_created_link(&gCarDVRUsecaseContext,            \
                                             linkID);                           \
    } while (0)


static
Void mulich_cardvr_set_avsync_vidque_prm(Avsync_SynchConfigParams *queCfg,
                                            Int chnum,
                                            UInt32 avsStartChNum,
                                            UInt32 avsEndChNum,
                                            Bool avsyncEnable)
{
    queCfg->chNum = chnum;
    queCfg->audioPresent = FALSE;
    if ((queCfg->chNum >= avsStartChNum)
        &&
        (queCfg->chNum <= avsEndChNum)
        &&
        (gVsysModuleContext.vsysConfig.enableAVsync)
        &&
        (avsyncEnable))
    {
        queCfg->avsyncEnable = TRUE;
    }
    else
    {
        queCfg->avsyncEnable = FALSE;
    }
    queCfg->clkAdjustPolicy.refClkType = AVSYNC_REFCLKADJUST_NONE;
    queCfg->playTimerStartTimeout = 0;
    queCfg->playStartMode = AVSYNC_PLAYBACK_START_MODE_WAITSYNCH;
    queCfg->clkAdjustPolicy.clkAdjustLead = AVSYNC_VIDEO_TIMEBASESHIFT_MAX_LEAD_MS;
    queCfg->clkAdjustPolicy.clkAdjustLag = AVSYNC_VIDEO_TIMEBASESHIFT_MAX_LAG_MS;
    queCfg->vidSynchPolicy.playMaxLag    = 200;
    queCfg->ptsInitMode   = AVSYNC_PTS_INIT_MODE_APP;
}

static
Void mulich_cardvr_set_avsync_prm(AvsyncLink_LinkSynchConfigParams *avsyncPrm,
                                  UInt32 prevLinkID,
                                  UInt32 prevLinkQueId,
                                  UInt32 swMsId)
{
    System_LinkInfo                   swmsInLinkInfo;
    Int i;
    Int32 status;
    Bool avsyncEnable;

    if ((0 == swMsId) || (1 == swMsId))
        avsyncEnable  = FALSE;
    else
        avsyncEnable  = TRUE;

    Vdis_getAvsyncConfig(VDIS_DEV_SD,avsyncPrm);
    if ((0 == swMsId) || (1 == swMsId))
        avsyncPrm->displayLinkID        = SYSTEM_LINK_ID_INVALID;
    else
        avsyncPrm->displayLinkID        = Vdis_getDisplayId(VDIS_DEV_SD);
    avsyncPrm->videoSynchLinkID = gVdisModuleContext.swMsId[swMsId];
    System_linkGetInfo(prevLinkID,&swmsInLinkInfo);
    OSA_assert(swmsInLinkInfo.numQue > prevLinkQueId);

    avsyncPrm->numCh            = swmsInLinkInfo.queInfo[prevLinkQueId].numCh;
    avsyncPrm->syncMasterChnum = 0;
    for (i = 0; i < avsyncPrm->numCh;i++)
    {
        mulich_cardvr_set_avsync_vidque_prm(&avsyncPrm->queCfg[i],
                                             i,
                                             0,
                                             0,
                                             avsyncEnable);
    }
    Vdis_setAvsyncConfig(VDIS_DEV_SD,avsyncPrm);

    status = Avsync_configSyncConfigInfo(avsyncPrm);
    OSA_assert(status == 0);

}

static
UInt32 multich_cardvr_get_videodecoder_device_id()
{
    OSA_I2cHndl i2cHandle;
    Int32 status;
    UInt32 twl_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TW2968_DRV,0);
    UInt32 tvp_i2c_addr = Device_getVidDecI2cAddr(DEVICE_VID_DEC_TVP5158_DRV,0);
    UInt8 regAddr[8];
    UInt8 regValue[8];
    UInt8 numRegs;
    UInt32 chipId;
    UInt32 deviceId = ~0u;

    status = OSA_i2cOpen(&i2cHandle, I2C_DEFAULT_INST_ID);
    OSA_assert(status==0);

    numRegs = 0;
    regAddr[numRegs] = DEVICE_TW2968_REG_DEVICE_ID;
    regValue[numRegs] = 0;
    numRegs++;

    regAddr[numRegs] = DEVICE_TW2968_REG_REV_ID;
    regValue[numRegs] = 0;
    numRegs++;

    status = OSA_i2cRead8(&i2cHandle,twl_i2c_addr,regAddr,regValue,numRegs);
    if (status == 0)
    {
        chipId =  (((UInt32) (regValue[0]>>6)&0x3) << 5) | ( (regValue[1] >> 3) & 0x1F ) ;
        printf("\nTWL_CHIP_ID_READ:0x%x\n",chipId);
        if (chipId == DEVICE_TW2968_CHIP_ID)
        {
            deviceId = DEVICE_VID_DEC_TW2968_DRV;
        }
    }
    if (deviceId == ~0u)
    {
        numRegs = 0;
        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_MSB;
        regValue[numRegs] = 0;
        numRegs++;

        regAddr[numRegs] = DEVICE_TVP5158_REG_CHIP_ID_LSB;
        regValue[numRegs] = 0;
        numRegs++;

        status = OSA_i2cRead8(&i2cHandle,tvp_i2c_addr,regAddr,regValue,numRegs);
        if (status == 0)
        {
            chipId = ( ( UInt32 ) regValue[0] << 8 ) | regValue[1];
            printf("\nTVP_CHIP_ID_READ:0x%x\n",chipId);
            if (DEVICE_TVP5158_CHIP_ID == chipId)
            {
                deviceId = DEVICE_VID_DEC_TVP5158_DRV;
            }
        }
    }
    OSA_assert(deviceId != ~0u);
    status = OSA_i2cClose(&i2cHandle);
    OSA_assert(status==0);
    return deviceId;
}

static
Void multich_cardvr_configure_extvideodecoder_prm()
{
    int i;
    UInt32 numCaptureDevices;
    UInt32 deviceId;

    deviceId = multich_cardvr_get_videodecoder_device_id();
    if (deviceId == DEVICE_VID_DEC_TW2968_DRV)
    {
        numCaptureDevices = 2;
    }
    else
    {
        numCaptureDevices = 4;
    }

    for(i = 0; i < numCaptureDevices; i++)
    {
        gCarDVRUsecaseContext.vidDecVideoModeArgs[i].deviceId         = deviceId;

        if(deviceId == DEVICE_VID_DEC_TW2968_DRV)
        {
            /* consider first device connected to VIP0 Port A and second device connected to VIP1 PortA */
            gCarDVRUsecaseContext.vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i*2;
            gCarDVRUsecaseContext.vidDecVideoModeArgs[i].numChInDevice    = 8;
        }
        else
        {
            gCarDVRUsecaseContext.vidDecVideoModeArgs[i].vipInstId        = SYSTEM_CAPTURE_INST_VIP0_PORTA+i;
            gCarDVRUsecaseContext.vidDecVideoModeArgs[i].numChInDevice    = 4;
        }
        gCarDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoIfMode        = DEVICE_CAPT_VIDEO_IF_MODE_8BIT;
        gCarDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoDataFormat    = SYSTEM_DF_YUV422P;
        if (i == 0)
        {
            gCarDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.standard           = SYSTEM_STD_MUX_4CH_D1;
        }
        else
        {
            gCarDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.standard           = SYSTEM_STD_MUX_4CH_D1;
        }
        gCarDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCaptureMode   =
                    DEVICE_CAPT_VIDEO_CAPTURE_MODE_MULTI_CH_PIXEL_MUX_EMBEDDED_SYNC;
        gCarDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoSystem        =
                                      DEVICE_VIDEO_DECODER_VIDEO_SYSTEM_AUTO_DETECT;
        gCarDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoCropEnable    = FALSE;
        gCarDVRUsecaseContext.vidDecVideoModeArgs[i].modeParams.videoAutoDetectTimeout = -1;
    }
    Vcap_configVideoDecoder(&gCarDVRUsecaseContext.vidDecVideoModeArgs[0],
                            numCaptureDevices);
}

static
Void multich_cardvr_set_link_ids()
{
    Int i;

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_CAPTURE;
    #ifdef TI_816X_BUILD
    gVcapModuleContext.deiId[D1_DEI_LINK_IDX]     = SYSTEM_LINK_ID_DEI_HQ_0;
    gVcapModuleContext.deiId[CIF_DEI_LINK_IDX]     = SYSTEM_LINK_ID_DEI_HQ_1;
    #else
    gVcapModuleContext.deiId[D1_DEI_LINK_IDX]     = SYSTEM_LINK_ID_DEI_0;
    gVcapModuleContext.deiId[CIF_DEI_LINK_IDX]     = SYSTEM_LINK_ID_DEI_1;
    #endif
    gVcapModuleContext.dspAlgId[0] = SYSTEM_LINK_ID_ALG_0  ;
    gVcapModuleContext.ipcFramesOutVpssId[OSD_FRAMESOUT_LINK_IDX] = SYSTEM_VPSS_LINK_ID_IPC_FRAMES_OUT_0;
    gVcapModuleContext.ipcFramesInDspId[0]   = SYSTEM_DSP_LINK_ID_IPC_FRAMES_IN_0;


    gVencModuleContext.encId        = SYSTEM_LINK_ID_VENC_0;
    gVdecModuleContext.decId        = SYSTEM_LINK_ID_VDEC_0;

    gVdisModuleContext.swMsId[0]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_0;
    gVdisModuleContext.swMsId[1]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_1;
    gVdisModuleContext.swMsId[2]      = SYSTEM_LINK_ID_SW_MS_MULTI_INST_2;
    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)] = SYSTEM_LINK_ID_DISPLAY_2; /* SDTV */

    gCarDVRUsecaseContext.mergeId[D1_CIFCOMPOSITE_MERGE_LINK_IDX] = SYSTEM_VPSS_LINK_ID_MERGE_0;
    gCarDVRUsecaseContext.mergeId[LIVE_PLAYBACK_MERGE_LINK_IDX]   = SYSTEM_VPSS_LINK_ID_MERGE_1;
    gCarDVRUsecaseContext.mergeId[FG_BG_ENCODE_DUP_MERGE_LINK_IDX] = SYSTEM_VPSS_LINK_ID_MERGE_2;
    gCarDVRUsecaseContext.mergeId[PRE_ENCODE_MERGE_LINK_IDX]   = SYSTEM_VIDEO_LINK_ID_MERGE_0;
    gCarDVRUsecaseContext.mergeId[PRE_DISPLAY_SELECT_MERGE_LINK_IDX]   = SYSTEM_VPSS_LINK_ID_MERGE_3;

    gCarDVRUsecaseContext.dupId[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX] = SYSTEM_VPSS_LINK_ID_DUP_0;
    gCarDVRUsecaseContext.dupId[DISPLAY_ENCODE_DUP_LINK_IDX]               = SYSTEM_VPSS_LINK_ID_DUP_1;

    for (i = 0; i < NUM_OUT_M3_LINK;i++)
    {
        gCarDVRUsecaseContext.ipcOutVpssId[i] = SYSTEM_VPSS_LINK_ID_IPC_OUT_M3_0 + i;
        gCarDVRUsecaseContext.ipcInVideoId[i] = SYSTEM_VIDEO_LINK_ID_IPC_IN_M3_0 + i;
    }
    gCarDVRUsecaseContext.ipcOutVideoId= SYSTEM_VIDEO_LINK_ID_IPC_OUT_M3_0;
    gCarDVRUsecaseContext.ipcInVpssId  = SYSTEM_VPSS_LINK_ID_IPC_IN_M3_0;

    gVencModuleContext.ipcBitsOutRTOSId  = SYSTEM_VIDEO_LINK_ID_IPC_BITS_OUT_0;
    gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_HOST_LINK_ID_IPC_BITS_IN_0;
    gVdecModuleContext.ipcBitsOutHLOSId  = SYSTEM_HOST_LINK_ID_IPC_BITS_OUT_0;
    gVdecModuleContext.ipcBitsInRTOSId  = SYSTEM_VIDEO_LINK_ID_IPC_BITS_IN_0;

    gCarDVRUsecaseContext.selectLinkId[DISPLAY_SELECT_LINK_IDX] = SYSTEM_VPSS_LINK_ID_SELECT_0;

}

static
Void multich_cardvr_reset_link_ids()
{
    Int i;

    gVcapModuleContext.captureId    = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.deiId[D1_DEI_LINK_IDX]     = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.deiId[CIF_DEI_LINK_IDX]     = SYSTEM_LINK_ID_INVALID;

    gVcapModuleContext.dspAlgId[0] = SYSTEM_LINK_ID_INVALID  ;
    gVcapModuleContext.ipcFramesOutVpssId[OSD_FRAMESOUT_LINK_IDX] = SYSTEM_LINK_ID_INVALID;
    gVcapModuleContext.ipcFramesInDspId[0]   = SYSTEM_LINK_ID_INVALID;


    gVencModuleContext.encId        = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.decId        = SYSTEM_LINK_ID_INVALID;

    gVdisModuleContext.swMsId[0]      = SYSTEM_LINK_ID_INVALID;
    gVdisModuleContext.swMsId[1]      = SYSTEM_LINK_ID_INVALID;
    gVdisModuleContext.swMsId[2]      = SYSTEM_LINK_ID_INVALID;
    gVdisModuleContext.displayId[Vdis_getDisplayContextIndex(VDIS_DEV_SD)] = SYSTEM_LINK_ID_INVALID; /* SDTV */

    gCarDVRUsecaseContext.mergeId[D1_CIFCOMPOSITE_MERGE_LINK_IDX] = SYSTEM_LINK_ID_INVALID;
    gCarDVRUsecaseContext.mergeId[LIVE_PLAYBACK_MERGE_LINK_IDX]   = SYSTEM_LINK_ID_INVALID;
    gCarDVRUsecaseContext.mergeId[PRE_ENCODE_MERGE_LINK_IDX]   = SYSTEM_LINK_ID_INVALID;
    gCarDVRUsecaseContext.mergeId[FG_BG_ENCODE_DUP_MERGE_LINK_IDX] = SYSTEM_LINK_ID_INVALID;
    gCarDVRUsecaseContext.mergeId[PRE_DISPLAY_SELECT_MERGE_LINK_IDX]   = SYSTEM_LINK_ID_INVALID;

    gCarDVRUsecaseContext.dupId[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX] = SYSTEM_LINK_ID_INVALID;
    gCarDVRUsecaseContext.dupId[DISPLAY_ENCODE_DUP_LINK_IDX]               = SYSTEM_LINK_ID_INVALID;

    for (i = 0; i < NUM_OUT_M3_LINK;i++)
    {
        gCarDVRUsecaseContext.ipcOutVpssId[i] = SYSTEM_LINK_ID_INVALID;
        gCarDVRUsecaseContext.ipcInVideoId[i] = SYSTEM_LINK_ID_INVALID;
    }
    gCarDVRUsecaseContext.ipcOutVideoId= SYSTEM_LINK_ID_INVALID;
    gCarDVRUsecaseContext.ipcInVpssId  = SYSTEM_LINK_ID_INVALID;

    gVencModuleContext.ipcBitsOutRTOSId  = SYSTEM_LINK_ID_INVALID;
    gVencModuleContext.ipcBitsInHLOSId   = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.ipcBitsOutHLOSId  = SYSTEM_LINK_ID_INVALID;
    gVdecModuleContext.ipcBitsInRTOSId  = SYSTEM_LINK_ID_INVALID;

    gCarDVRUsecaseContext.selectLinkId[DISPLAY_SELECT_LINK_IDX] = SYSTEM_LINK_ID_INVALID;

}

static
Void multich_cardvr_set_capture_fps(UInt32 *captureFps)
{
    Bool isPal = Vcap_isPalMode();

    if (isPal)
    {
        *captureFps = 50;
    }
    else
    {
        *captureFps = 60;
    }
}

#define MULTICH_CARDVR_GET_CAPTURE_FIELDS_PER_SEC()           (gCarDVRUsecaseContext.captureFps)
#define MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC()           (gCarDVRUsecaseContext.captureFps/2)


static
UInt32 multich_cardvr_enclink_mapch2fps(UInt32 chNum)
{
    UInt32 fps = 0;

    switch (chNum)
    {
        case 0:
        case 1:
        case 4:
            fps = MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC();
            break;
        case 2:
            fps = 0;
            break;
        case 3:
            fps = BACKGROUND_CHANNEL_ENCODE_FPS;
            break;
        default:
            fps = 0;
            break;
    }
    return (fps * 1000);
}

static
Void multich_cardvr_set_links_framerate()
{
    Int32 status;
    DeiLink_ChFpsParams params;
    UInt32 chId;

    for (chId = 0; chId < gVcapModuleContext.vcapConfig.numChn;chId++)
    {
        /* Capture -> Dei */
        params.chId = chId;
        params.inputFrameRate = MULTICH_CARDVR_GET_CAPTURE_FIELDS_PER_SEC();

        /* Stream 0 -DEI_SC_OUT is inputfps/2 */
        params.streamId = DEI_LINK_OUT_QUE_DEI_SC;
        params.outputFrameRate = MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC();
        status = System_linkControl(gVcapModuleContext.deiId[0], DEI_LINK_CMD_SET_FRAME_RATE,
                                    &params, sizeof(params), TRUE);
        status = System_linkControl(gVcapModuleContext.deiId[1], DEI_LINK_CMD_SET_FRAME_RATE,
                                    &params, sizeof(params), TRUE);
        /* Stream 1 -VIP_SC_OUT_PRIMARY is inputfps/2 */
        params.streamId = DEI_LINK_OUT_QUE_VIP_SC;
        params.inputFrameRate = MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC();
        params.outputFrameRate = MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC();
        status = System_linkControl(gVcapModuleContext.deiId[0], DEI_LINK_CMD_SET_FRAME_RATE,
                                    &params, sizeof(params), TRUE);
        status = System_linkControl(gVcapModuleContext.deiId[1], DEI_LINK_CMD_SET_FRAME_RATE,
                                    &params, sizeof(params), TRUE);

        /* Stream 1 -VIP_SC_OUT_PRIMARY is inputfps/2 */
        params.streamId = DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT;
        params.inputFrameRate = MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC();
        params.outputFrameRate = MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC();
        status = System_linkControl(gVcapModuleContext.deiId[0], DEI_LINK_CMD_SET_FRAME_RATE,
                                    &params, sizeof(params), TRUE);
        status = System_linkControl(gVcapModuleContext.deiId[1], DEI_LINK_CMD_SET_FRAME_RATE,
                                    &params, sizeof(params), TRUE);
    }
    for (chId=0; chId < gVencModuleContext.vencConfig.numPrimaryChn +
                        gVencModuleContext.vencConfig.numSecondaryChn; chId++)
    {
        UInt32 fps;

        fps = multich_cardvr_enclink_mapch2fps(chId);
        if (0 == fps)
        {
            EncLink_ChannelInfo chDisablePrm;
            chDisablePrm.chId = chId;
            status = System_linkControl(gVencModuleContext.encId, ENC_LINK_CMD_DISABLE_CHANNEL,
                                    &chDisablePrm, sizeof(chDisablePrm), TRUE);
        }
        else
        {
            EncLink_ChFpsParams fpsPrm;

            fpsPrm.chId = chId;
            fpsPrm.targetFps = fps;
            fpsPrm.targetBitRate = 0;
            status = System_linkControl(gVencModuleContext.encId, ENC_LINK_CMD_SET_CODEC_FPS,
                                        &fpsPrm, sizeof(fpsPrm), TRUE);
        }
    }
}

static
Void multich_cardvr_reset_link_prms()
{
    int i;

    for (i = 0; i < NUM_OUT_M3_LINK; i++)
    {
        MULTICH_INIT_STRUCT(IpcLink_CreateParams,gCarDVRUsecaseContext.ipcOutVpssPrm[i]);
        MULTICH_INIT_STRUCT(IpcLink_CreateParams,gCarDVRUsecaseContext.ipcInVideoPrm[i]);
    }
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gCarDVRUsecaseContext.ipcInVpssPrm);
    MULTICH_INIT_STRUCT(IpcLink_CreateParams,gCarDVRUsecaseContext.ipcOutVideoPrm);

    MULTICH_INIT_STRUCT(IpcBitsOutLinkHLOS_CreateParams,gCarDVRUsecaseContext.ipcBitsOutHostPrm);
    MULTICH_INIT_STRUCT(IpcBitsOutLinkRTOS_CreateParams,gCarDVRUsecaseContext.ipcBitsOutVideoPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkHLOS_CreateParams,gCarDVRUsecaseContext.ipcBitsInHostPrm);
    MULTICH_INIT_STRUCT(IpcBitsInLinkRTOS_CreateParams,gCarDVRUsecaseContext.ipcBitsInVideoPrm);
    MULTICH_INIT_STRUCT(DecLink_CreateParams, gCarDVRUsecaseContext.decPrm);
    MULTICH_INIT_STRUCT(IpcFramesInLinkRTOS_CreateParams,gCarDVRUsecaseContext.ipcFramesInDspPrm);
    for (i = 0; i < NUM_VPSS_FRAMES_OUT_LINK; i++)
    {
        MULTICH_INIT_STRUCT(IpcFramesOutLinkRTOS_CreateParams,
                            gCarDVRUsecaseContext.ipcFramesOutVpssPrm[i]);
    }
    MULTICH_INIT_STRUCT(EncLink_CreateParams, gCarDVRUsecaseContext.encPrm);
    MULTICH_INIT_STRUCT(AlgLink_CreateParams, gCarDVRUsecaseContext.dspAlgPrm);
    for (i = 0; i < VDIS_DEV_MAX;i++)
    {
        MULTICH_INIT_STRUCT(DisplayLink_CreateParams,
                            gCarDVRUsecaseContext.displayPrm[i]);
        MULTICH_INIT_STRUCT(SwMsLink_CreateParams ,gCarDVRUsecaseContext.swMsPrm[i]);
    }

    for (i = 0; i < NUM_DEI_LINK; i++)
    {
        MULTICH_INIT_STRUCT(DeiLink_CreateParams, gCarDVRUsecaseContext.deiPrm[i]);
    }
    MULTICH_INIT_STRUCT(AvsyncLink_LinkSynchConfigParams,gCarDVRUsecaseContext.avsyncPrms);
    CaptureLink_CreateParams_Init(&gCarDVRUsecaseContext.capturePrm);
    EncLink_CreateParams_Init(&gCarDVRUsecaseContext.encPrm);
    for (i = 0; i < NUM_SELECT_LINK;i++)
    {
        MULTICH_INIT_STRUCT(SelectLink_CreateParams, gCarDVRUsecaseContext.selectPrm[i]);
    }
}

static
Void multich_cardvr_set_pre_display_select_prm(SelectLink_CreateParams *selectPrm)
{
    selectPrm->numOutQue = 1;
    selectPrm->outQueChInfo[0].numOutCh = 1;
    selectPrm->outQueChInfo[0].outQueId = 0;
    selectPrm->outQueChInfo[0].inChNum[0] = PRE_DISPLAY_SELECT_MERGE_LINK_D1DEILIVE_QIDX;
}

static
Void multich_cardvr_set_capture_prm(CaptureLink_CreateParams *capturePrm)
{
    UInt32 vipInstId;
    CaptureLink_VipInstParams         *pCaptureInstPrm;
    CaptureLink_OutParams             *pCaptureOutPrm;

    vipInstId = 0;

    /* This is for TVP5158 Audio Channels - Change it to 16 if there are 16 audio channels connected in cascade */
    capturePrm->numVipInst                 = 2;
    capturePrm->tilerEnable                = FALSE;
    capturePrm->numBufsPerCh               = NUM_CAPTURE_BUFFERS;
    capturePrm->enableSdCrop               = FALSE;

    capturePrm->isPalMode                  = Vcap_isPalMode();


    pCaptureInstPrm                     = &capturePrm->vipInst[0];
    pCaptureInstPrm->vipInstId          = (SYSTEM_CAPTURE_INST_VIP0_PORTA+
                                          vipInstId)%SYSTEM_CAPTURE_INST_MAX;
    pCaptureInstPrm->videoDecoderId     = SYSTEM_DEVICE_VID_DEC_TVP5158_DRV;
    pCaptureInstPrm->inDataFormat       = SYSTEM_DF_YUV422P;
    pCaptureInstPrm->standard           = SYSTEM_STD_MUX_4CH_D1;
    pCaptureInstPrm->numOutput          = 1;
    pCaptureInstPrm->numChPerOutput     = 1;

    pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];
    pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV422I_YUYV;
    pCaptureOutPrm->scEnable            = FALSE;
    pCaptureOutPrm->scOutWidth          = 0;
    pCaptureOutPrm->scOutHeight         = 0;
    pCaptureOutPrm->outQueId            = 0;

    vipInstId = 1;
    pCaptureInstPrm                     = &capturePrm->vipInst[1];
    pCaptureInstPrm->vipInstId          = (SYSTEM_CAPTURE_INST_VIP0_PORTA+
                                          vipInstId)%SYSTEM_CAPTURE_INST_MAX;
    pCaptureInstPrm->videoDecoderId     = SYSTEM_DEVICE_VID_DEC_TVP5158_DRV;
    pCaptureInstPrm->inDataFormat       = SYSTEM_DF_YUV422P;
    pCaptureInstPrm->standard           = SYSTEM_STD_MUX_4CH_D1;
    pCaptureInstPrm->numOutput          = 1;

    pCaptureOutPrm                      = &pCaptureInstPrm->outParams[0];
    pCaptureOutPrm->dataFormat          = SYSTEM_DF_YUV422I_YUYV;
    pCaptureOutPrm->scEnable            = FALSE;
    pCaptureOutPrm->scOutWidth          = 0;
    pCaptureOutPrm->scOutHeight         = 0;
    pCaptureOutPrm->outQueId            = 1;
}

static
Void multich_cardvr_set_dei_prm(DeiLink_CreateParams *deiPrm)
{
    int i;

    /* Set Output Scaling at DEI based on ratio */
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].scaleMode = DEI_SCALE_MODE_RATIO;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.widthRatio.numerator = 1;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.widthRatio.denominator = 1;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.heightRatio.numerator = 1;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0].ratio.heightRatio.denominator = 1;
    for (i = 1; i < DEI_LINK_MAX_CH; i++)
    {
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][i] =
                     deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC][0];
    }
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0].scaleMode = DEI_SCALE_MODE_RATIO;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0].ratio.widthRatio.numerator = 1;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0].ratio.widthRatio.denominator = 1;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0].ratio.heightRatio.numerator = 1;
    deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0].ratio.heightRatio.denominator = 1;
    for (i = 1; i < DEI_LINK_MAX_CH; i++)
    {
        deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][i] =
                     deiPrm->outScaleFactor[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT][0];
    }
    deiPrm->enableOut[DEI_LINK_OUT_QUE_DEI_SC]                        = FALSE;
    deiPrm->generateBlankOut[DEI_LINK_OUT_QUE_DEI_SC]                 = FALSE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_VIP_SC]                        = FALSE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT]          = TRUE;
    deiPrm->generateBlankOut[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT]   = FALSE;
    deiPrm->enableOut[DEI_LINK_OUT_QUE_VIP_SC]                        = TRUE;
    deiPrm->tilerEnable[DEI_LINK_OUT_QUE_VIP_SC]          = TILER_ENABLE;
    deiPrm->comprEnable                                   = FALSE;
    deiPrm->setVipScYuv422Format                          = FALSE;
}


static
Void multich_cardvr_set_swms_singlewin_layoutprm(SwMsLink_LayoutPrm *layoutInfo,VSYS_VIDEO_STANDARD_E outRes)
{
    SwMsLink_LayoutWinInfo *winInfo;
    UInt32 outWidth, outHeight, winId, widthAlign, heightAlign;

    MultiCh_swMsGetOutSize(outRes, &outWidth, &outHeight);
    widthAlign = 8;
    heightAlign = 1;

    /* init to known default */
    memset(layoutInfo, 0, sizeof(*layoutInfo));

    layoutInfo->onlyCh2WinMapChanged = FALSE;
    layoutInfo->outputFPS = MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC();
    layoutInfo->numWin = 1;
    winId = 0;

    winInfo = &layoutInfo->winInfo[winId];

    winInfo->width  = SystemUtils_floor(outWidth, widthAlign);
    winInfo->height = SystemUtils_floor(outHeight, heightAlign);
    winInfo->startX = 0;
    winInfo->startY = 0;
    winInfo->bypass = FALSE;
    winInfo->channelNum = 0 + winId;
}

static
Void multich_cardvr_set_swms_2x2_layoutprm(SwMsLink_LayoutPrm *layoutInfo,VSYS_VIDEO_STANDARD_E outRes)
{
    SwMsLink_LayoutWinInfo *winInfo;
    UInt32 outWidth, outHeight, winId, widthAlign, heightAlign;
    UInt32 rowMax,colMax,row,col;

    MultiCh_swMsGetOutSize(outRes, &outWidth, &outHeight);
    widthAlign = 8;
    heightAlign = 1;

    /* init to known default */
    memset(layoutInfo, 0, sizeof(*layoutInfo));

    layoutInfo->onlyCh2WinMapChanged = FALSE;
    layoutInfo->outputFPS = MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC();
    rowMax = 2;
    colMax = 2;

    if(rowMax != 0)
    {
        layoutInfo->numWin = rowMax * colMax;

        for(row=0; row<rowMax; row++)
        {
            for(col=0; col<colMax; col++)
            {
                winId = row*colMax+col;

                winInfo = &layoutInfo->winInfo[winId];
                winInfo->width = VsysUtils_floor(outWidth/colMax, widthAlign);
                winInfo->height = VsysUtils_floor(outHeight/rowMax, heightAlign);
                winInfo->startX = winInfo->width*col;
                winInfo->startY = winInfo->height*row;

                if (col == colMax - 1) /* the last col */
                {
                    winInfo->width = outWidth - winInfo->width * (colMax - 1);
                }

                if(winId < 4)
                    winInfo->channelNum = winId;
                else
                    winInfo->channelNum = SYSTEM_SW_MS_INVALID_ID;
                winInfo->bypass = FALSE;
            }
       }

    }
}


static
Void multich_cardvr_set_swms_prm(SwMsLink_CreateParams *swMsPrm,UInt32 swMsId)
{
    swMsPrm->numSwMsInst = 1;
    swMsPrm->outDataFormat = SYSTEM_DF_YUV420SP_UV;
    /* use AUX scaler (SC2), since SC1 is used for DEI */
    swMsPrm->swMsInstId[0] = SYSTEM_SW_MS_SC_INST_VIP1_SC_NO_DEI;
    swMsPrm->maxOutRes                 = VSYS_STD_PAL;
    if (Vcap_isPalMode())
    {
        swMsPrm->initOutRes                = VSYS_STD_PAL;
    }
    else
    {
        swMsPrm->initOutRes                = VSYS_STD_NTSC;
    }
    swMsPrm->numOutBuf                 = NUM_SWMS_MAX_BUFFERS;
    swMsPrm->lineSkipMode = FALSE; // Set to TRUE for Enable low cost scaling
    swMsPrm->enableLayoutGridDraw = FALSE;
    swMsPrm->enableOuputDup = FALSE;
    if ((0 == swMsId) || (1 == swMsId))
    {
        multich_cardvr_set_swms_2x2_layoutprm(&swMsPrm->layoutPrm,swMsPrm->initOutRes);
        swMsPrm->enableProcessTieWithDisplay = FALSE;
    }
    else
    {
        multich_cardvr_set_swms_singlewin_layoutprm(&swMsPrm->layoutPrm,swMsPrm->initOutRes);
        swMsPrm->enableProcessTieWithDisplay = TRUE;
        swMsPrm->outputBufModified =  TRUE;
    }
    swMsPrm->layoutPrm.outputFPS = MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC();
    /* Compensate for 10% timerPeriod compensation in SwMs */
    swMsPrm->layoutPrm.outputFPS -= MULTICH_CARDVR_GET_CAPTURE_FRAMES_PER_SEC() / 10;
}



static
Void multich_cardvr_set_declink_prms(DecLink_CreateParams *decPrm)
{
    int i;

    for (i=0; i<gVdecModuleContext.vdecConfig.numChn; i++)
    {
        decPrm->chCreateParams[i].format                 = IVIDEO_H264HP;
        decPrm->chCreateParams[i].profile                = IH264VDEC_PROFILE_ANY;
        decPrm->chCreateParams[i].processCallLevel       = VDEC_FRAMELEVELPROCESSCALL;
        decPrm->chCreateParams[i].targetMaxWidth         = gVdecModuleContext.vdecConfig.decChannelParams[i].maxVideoWidth;
        decPrm->chCreateParams[i].targetMaxHeight        = gVdecModuleContext.vdecConfig.decChannelParams[i].maxVideoHeight;
        decPrm->chCreateParams[i].defaultDynamicParams.targetFrameRate = gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.frameRate;
        decPrm->chCreateParams[i].defaultDynamicParams.targetBitRate   = gVdecModuleContext.vdecConfig.decChannelParams[i].dynamicParam.targetBitRate;
        decPrm->chCreateParams[i].numBufPerCh = NUM_DECODE_BUFFERS;
        decPrm->chCreateParams[i].displayDelay = 2;
        decPrm->chCreateParams[i].tilerEnable = TILER_ENABLE;
        decPrm->chCreateParams[i].enableWaterMarking = gVdecModuleContext.vdecConfig.decChannelParams[i].enableWaterMarking;
    }
}


static
Void multich_cardvr_set_ipcbitsout_hlos_prms(IpcBitsOutLinkHLOS_CreateParams * ipcBitsOutHostPrm)
{
    int i;

    for (i = 0;
         i < (gVdecModuleContext.vdecConfig.numChn);
         i++)
    {
        System_LinkChInfo *pChInfo;

        pChInfo = &ipcBitsOutHostPrm->inQueInfo.chInfo[i];

        pChInfo->bufType        = 0; // NOT USED
        pChInfo->codingformat   = 0; // NOT USED
        pChInfo->dataFormat     = 0; // NOT USED
        pChInfo->memType        = 0; // NOT USED
        pChInfo->startX         = 0; // NOT USED
        pChInfo->startY         = 0; // NOT USED
        pChInfo->width          = gVdecModuleContext.vdecConfig.decChannelParams[i].maxVideoWidth;
        pChInfo->height         = gVdecModuleContext.vdecConfig.decChannelParams[i].maxVideoHeight;
        pChInfo->pitch[0]       = 0; // NOT USED
        pChInfo->pitch[1]       = 0; // NOT USED
        pChInfo->pitch[2]       = 0; // NOT USED
        pChInfo->scanFormat     = SYSTEM_SF_PROGRESSIVE;

        ipcBitsOutHostPrm->maxQueueDepth[i] = 
            MAX_BUFFERING_QUEUE_LEN_PER_CH;
        ipcBitsOutHostPrm->chMaxReqBufSize[i] = (pChInfo->width * pChInfo->height); 
        ipcBitsOutHostPrm->totalBitStreamBufferSize [i] = 
                (ipcBitsOutHostPrm->chMaxReqBufSize[i] * BIT_BUF_LENGTH_LIMIT_FACTOR_SD);
    }
    ipcBitsOutHostPrm->baseCreateParams.noNotifyMode = FALSE;
    ipcBitsOutHostPrm->baseCreateParams.notifyNextLink = TRUE;
    ipcBitsOutHostPrm->baseCreateParams.numOutQue = 1;
    ipcBitsOutHostPrm->inQueInfo.numCh =
        (gVdecModuleContext.vdecConfig.numChn);
}

Void multich_cardvr_set_osd_prm(AlgLink_CreateParams *dspAlgPrm)
{

    int chId;

    dspAlgPrm->enableOSDAlg = TRUE;
    for(chId = 0; chId < ALG_LINK_OSD_MAX_CH; chId++)
    {
        AlgLink_OsdChWinParams * chWinPrm = &dspAlgPrm->osdChCreateParams[chId].chDefaultParams;

        /* set osd window max width and height */
        dspAlgPrm->osdChCreateParams[chId].maxWidth  = EXAMPLE_OSD_WIN_MAX_WIDTH;
        dspAlgPrm->osdChCreateParams[chId].maxHeight = EXAMPLE_OSD_WIN_MAX_HEIGHT;

        chWinPrm->chId = chId;
        chWinPrm->numWindows = 0;
    }
}

static
Void multich_cardvr_set_enclink_prm(EncLink_CreateParams *encPrm)
{

    EncLink_ChCreateParams *pLinkChPrm;
    EncLink_ChDynamicParams *pLinkDynPrm;
    VENC_CHN_DYNAMIC_PARAM_S *pDynPrm;
    VENC_CHN_PARAMS_S *pChPrm;
    int i;

    encPrm->numBufPerCh[0] = NUM_ENCODE_D1_BUFFERS;
    encPrm->numBufPerCh[1] = NUM_ENCODE_CIF_BUFFERS;

    /* Primary Stream Params - D1 */
    for (i=0; i<gVencModuleContext.vencConfig.numPrimaryChn +
                gVencModuleContext.vencConfig.numSecondaryChn; i++)
    {
        pLinkChPrm  = &encPrm->chCreateParams[i];
        pLinkDynPrm = &pLinkChPrm->defaultDynamicParams;

        pChPrm      = &gVencModuleContext.vencConfig.encChannelParams[i];
        pDynPrm     = &pChPrm->dynamicParam;

        pLinkChPrm->format                  = IVIDEO_H264HP;
        pLinkChPrm->profile                 = gVencModuleContext.vencConfig.h264Profile[i];
        pLinkChPrm->dataLayout              = VCODEC_FIELD_SEPARATED;
        pLinkChPrm->fieldMergeEncodeEnable  = FALSE;
        pLinkChPrm->enableAnalyticinfo      = pChPrm->enableAnalyticinfo;
        pLinkChPrm->enableWaterMarking      = pChPrm->enableWaterMarking;
        pLinkChPrm->maxBitRate              = pChPrm->maxBitRate;
        pLinkChPrm->encodingPreset          = pChPrm->encodingPreset;
        pLinkChPrm->rateControlPreset       = pChPrm->rcType;
        pLinkChPrm->numTemporalLayer        = pChPrm->numTemporalLayer;
        pLinkChPrm->enableSVCExtensionFlag  = pChPrm->enableSVCExtensionFlag;

        pLinkDynPrm->intraFrameInterval     = pDynPrm->intraFrameInterval;
        pLinkDynPrm->targetBitRate          = pDynPrm->targetBitRate;
        pLinkDynPrm->interFrameInterval     = 1;
        pLinkDynPrm->mvAccuracy             = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
        pLinkDynPrm->inputFrameRate         = pDynPrm->inputFrameRate;
        pLinkDynPrm->rcAlg                  = pDynPrm->rcAlg;
        pLinkDynPrm->qpMin                  = pDynPrm->qpMin;
        pLinkDynPrm->qpMax                  = pDynPrm->qpMax;
        pLinkDynPrm->qpInit                 = pDynPrm->qpInit;
        pLinkDynPrm->vbrDuration            = pDynPrm->vbrDuration;
        pLinkDynPrm->vbrSensitivity         = pDynPrm->vbrSensitivity;
    }
}

static
Void multich_cardvr_set_sd_display_res(VSYS_VIDEO_STANDARD_E resolution)
{
    /* Work around for VENC tying getting broken when using sysfs cmd to change resolution */
    Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX0, VDIS_OFF);
    Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX1, VDIS_OFF);

    Vdis_setResolution(VDIS_DEV_SD,resolution);

    Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX0, VDIS_ON);
    Vdis_sysfsCmd(3, VDIS_SYSFSCMD_SET_GRPX, VDIS_SYSFS_GRPX1, VDIS_ON);

}

static
Void multich_cardvr_set_display_prms(DisplayLink_CreateParams *displayPrm)
{
    if (Vcap_isPalMode() && (Vdis_getResolution(VDIS_DEV_SD) != VSYS_STD_PAL))
    {
        multich_cardvr_set_sd_display_res(VSYS_STD_PAL);
    }

    displayPrm->displayRes = gVdisModuleContext.vdisConfig.deviceParams[VDIS_DEV_SD].resolution;
    displayPrm->maxDriverQueLength = 3;
    displayPrm->queueInISRFlag     = TRUE;
}

static
Void multich_cardvr_connect_links()
{
    multich_cardvr_configure_extvideodecoder_prm();

    /**Capture Link**/
    multich_cardvr_set_capture_prm(&gCarDVRUsecaseContext.capturePrm);
    /* Capture --> Q0 --> D1_DEI_LINK_IDX */
    gCarDVRUsecaseContext.capturePrm.outQueParams[0].nextLink   = gVcapModuleContext.deiId[D1_DEI_LINK_IDX];
    gCarDVRUsecaseContext.deiPrm[D1_DEI_LINK_IDX].inQueParams.prevLinkId = gVcapModuleContext.captureId;
    gCarDVRUsecaseContext.deiPrm[D1_DEI_LINK_IDX].inQueParams.prevLinkQueId  = 0;

    /* Capture --> Q1 --> CIF_DEI_LINK_IDX */
    gCarDVRUsecaseContext.capturePrm.outQueParams[1].nextLink   = gVcapModuleContext.deiId[CIF_DEI_LINK_IDX];
    gCarDVRUsecaseContext.deiPrm[CIF_DEI_LINK_IDX].inQueParams.prevLinkId = gVcapModuleContext.captureId;
    gCarDVRUsecaseContext.deiPrm[CIF_DEI_LINK_IDX].inQueParams.prevLinkQueId  = 1;
    MULTICH_CARDVR_CREATE_LINK (gVcapModuleContext.captureId, &gCarDVRUsecaseContext.capturePrm,
                                sizeof(gCarDVRUsecaseContext.capturePrm));
    /**After Capture is created set capture fps */
    multich_cardvr_set_capture_fps(&gCarDVRUsecaseContext.captureFps);

    /* D1_DEI_LINK_IDX --DEI_LINK_OUT_QUE_VIP_SC--> D1_CIFCOMPOSITE_MERGE_LINK_IDX */
    multich_cardvr_set_dei_prm(&gCarDVRUsecaseContext.deiPrm[D1_DEI_LINK_IDX]);
    gCarDVRUsecaseContext.deiPrm[D1_DEI_LINK_IDX].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink  = gCarDVRUsecaseContext.mergeId[D1_CIFCOMPOSITE_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[D1_CIFCOMPOSITE_MERGE_LINK_IDX].inQueParams[D1_CIFCOMPOSITE_MERGE_LINK_DEI0_QIDX].prevLinkId = gVcapModuleContext.deiId[D1_DEI_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[D1_CIFCOMPOSITE_MERGE_LINK_IDX].inQueParams[D1_CIFCOMPOSITE_MERGE_LINK_DEI0_QIDX].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;
    gCarDVRUsecaseContext.deiPrm[D1_DEI_LINK_IDX].outQueParams[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT].nextLink  = gCarDVRUsecaseContext.mergeId[PRE_DISPLAY_SELECT_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX].numInQue = PRE_DISPLAY_SELECT_MERGE_LINK_NUM_INQUE;
    gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX].inQueParams[PRE_DISPLAY_SELECT_MERGE_LINK_D1DEILIVE_QIDX].prevLinkId = gVcapModuleContext.deiId[D1_DEI_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX].inQueParams[PRE_DISPLAY_SELECT_MERGE_LINK_D1DEILIVE_QIDX].prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT;


    MULTICH_CARDVR_CREATE_LINK(gVcapModuleContext.deiId[D1_DEI_LINK_IDX], &gCarDVRUsecaseContext.deiPrm[D1_DEI_LINK_IDX], sizeof(gCarDVRUsecaseContext.deiPrm[D1_DEI_LINK_IDX]));

    multich_cardvr_set_dei_prm(&gCarDVRUsecaseContext.deiPrm[CIF_DEI_LINK_IDX]);
    gCarDVRUsecaseContext.deiPrm[CIF_DEI_LINK_IDX].outQueParams[DEI_LINK_OUT_QUE_VIP_SC].nextLink            = gVdisModuleContext.swMsId[0];
    gCarDVRUsecaseContext.swMsPrm[0].inQueParams.prevLinkId = gVcapModuleContext.deiId[CIF_DEI_LINK_IDX];
    gCarDVRUsecaseContext.swMsPrm[0].inQueParams.prevLinkQueId = DEI_LINK_OUT_QUE_VIP_SC;
    gCarDVRUsecaseContext.deiPrm[CIF_DEI_LINK_IDX].outQueParams[DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT].nextLink            = gCarDVRUsecaseContext.mergeId[LIVE_PLAYBACK_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[LIVE_PLAYBACK_MERGE_LINK_IDX].inQueParams[LIVE_PLAYBACK_MERGE_LINK_SWMSLIVE_QIDX].prevLinkId = gVcapModuleContext.deiId[CIF_DEI_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[LIVE_PLAYBACK_MERGE_LINK_IDX].inQueParams[LIVE_PLAYBACK_MERGE_LINK_SWMSLIVE_QIDX].prevLinkQueId =DEI_LINK_OUT_QUE_VIP_SC_SECONDARY_OUT;

    MULTICH_CARDVR_CREATE_LINK(gVcapModuleContext.deiId[CIF_DEI_LINK_IDX], &gCarDVRUsecaseContext.deiPrm[CIF_DEI_LINK_IDX], sizeof(gCarDVRUsecaseContext.deiPrm[CIF_DEI_LINK_IDX]));

/*
 *  Disable avsync configuration for capture compositor component
    mulich_cardvr_set_avsync_prm(&gCarDVRUsecaseContext.avsyncPrms[0],
                                 gCarDVRUsecaseContext.swMsPrm[0].inQueParams.prevLinkId,
                                 gCarDVRUsecaseContext.swMsPrm[0].inQueParams.prevLinkQueId,
                                 0);
*/

    multich_cardvr_set_swms_prm(&gCarDVRUsecaseContext.swMsPrm[0],0);
    gCarDVRUsecaseContext.swMsPrm[0].outQueParams.nextLink     = gCarDVRUsecaseContext.mergeId[D1_CIFCOMPOSITE_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[D1_CIFCOMPOSITE_MERGE_LINK_IDX].inQueParams[D1_CIFCOMPOSITE_MERGE_LINK_SWMS_COMPOSE_QIDX].prevLinkId = gVdisModuleContext.swMsId[0];
    gCarDVRUsecaseContext.mergePrm[D1_CIFCOMPOSITE_MERGE_LINK_IDX].inQueParams[D1_CIFCOMPOSITE_MERGE_LINK_SWMS_COMPOSE_QIDX].prevLinkQueId =0;
    MULTICH_CARDVR_CREATE_LINK(gVdisModuleContext.swMsId[0], &gCarDVRUsecaseContext.swMsPrm[0], sizeof(gCarDVRUsecaseContext.swMsPrm[0]));

    gCarDVRUsecaseContext.mergePrm[D1_CIFCOMPOSITE_MERGE_LINK_IDX].notifyNextLink                  = TRUE;
    gCarDVRUsecaseContext.mergePrm[D1_CIFCOMPOSITE_MERGE_LINK_IDX].numInQue = D1_CIFCOMPOSITE_MERGE_LINK_NUM_INQUE;
    gCarDVRUsecaseContext.mergePrm[D1_CIFCOMPOSITE_MERGE_LINK_IDX].outQueParams.nextLink  = gCarDVRUsecaseContext.dupId[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX];
    gCarDVRUsecaseContext.dupPrm[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX].inQueParams.prevLinkId = gCarDVRUsecaseContext.mergeId[D1_CIFCOMPOSITE_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.dupPrm[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX].inQueParams.prevLinkQueId = 0;
    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.mergeId[D1_CIFCOMPOSITE_MERGE_LINK_IDX], &gCarDVRUsecaseContext.mergePrm[D1_CIFCOMPOSITE_MERGE_LINK_IDX],
                               sizeof(gCarDVRUsecaseContext.mergePrm[D1_CIFCOMPOSITE_MERGE_LINK_IDX]));

    gCarDVRUsecaseContext.dupPrm[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX].numOutQue = 2;
    gCarDVRUsecaseContext.dupPrm[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX].notifyNextLink = TRUE;
    gCarDVRUsecaseContext.dupPrm[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX].outQueParams[0].nextLink = gCarDVRUsecaseContext.mergeId[FG_BG_ENCODE_DUP_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[FG_BG_ENCODE_DUP_MERGE_LINK_IDX].inQueParams[FG_BG_ENCODE_DUP_MERGE_LINK_FGBGDUPQ0_QIDX].prevLinkId = gCarDVRUsecaseContext.dupId[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[FG_BG_ENCODE_DUP_MERGE_LINK_IDX].inQueParams[FG_BG_ENCODE_DUP_MERGE_LINK_FGBGDUPQ0_QIDX].prevLinkQueId = 0;
    gCarDVRUsecaseContext.dupPrm[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX].outQueParams[1].nextLink = gCarDVRUsecaseContext.mergeId[FG_BG_ENCODE_DUP_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[FG_BG_ENCODE_DUP_MERGE_LINK_IDX].inQueParams[FG_BG_ENCODE_DUP_MERGE_LINK_FGBGDUPQ1_QIDX].prevLinkId = gCarDVRUsecaseContext.dupId[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[FG_BG_ENCODE_DUP_MERGE_LINK_IDX].inQueParams[FG_BG_ENCODE_DUP_MERGE_LINK_FGBGDUPQ1_QIDX].prevLinkQueId = 1;
    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.dupId[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX],
                      &gCarDVRUsecaseContext.dupPrm[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.dupPrm[FOREGROUND_BACKGROUND_ENCODE_DUP_LINK_IDX]));

    gCarDVRUsecaseContext.mergePrm[FG_BG_ENCODE_DUP_MERGE_LINK_IDX].numInQue = FG_BG_ENCODE_DUP_MERGE_LINK_NUM_INQUE;
    gCarDVRUsecaseContext.mergePrm[FG_BG_ENCODE_DUP_MERGE_LINK_IDX].notifyNextLink = TRUE;
    gCarDVRUsecaseContext.mergePrm[FG_BG_ENCODE_DUP_MERGE_LINK_IDX].outQueParams.nextLink = gCarDVRUsecaseContext.ipcOutVpssId[CAPTURE_OUT_M3_LINK_IDX];
    gCarDVRUsecaseContext.ipcOutVpssPrm[CAPTURE_OUT_M3_LINK_IDX].inQueParams.prevLinkId =
                         gCarDVRUsecaseContext.mergeId[FG_BG_ENCODE_DUP_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.ipcOutVpssPrm[CAPTURE_OUT_M3_LINK_IDX].inQueParams.prevLinkQueId = 0;
    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.mergeId[FG_BG_ENCODE_DUP_MERGE_LINK_IDX],
                      &gCarDVRUsecaseContext.mergePrm[FG_BG_ENCODE_DUP_MERGE_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.mergePrm[FG_BG_ENCODE_DUP_MERGE_LINK_IDX]));

    /* Redirect to VIDEO-M3 for Capture channel encode */
    gCarDVRUsecaseContext.ipcOutVpssPrm[CAPTURE_OUT_M3_LINK_IDX].outQueParams[0].nextLink  = gCarDVRUsecaseContext.ipcInVideoId[CAPTURE_OUT_M3_LINK_IDX];
    gCarDVRUsecaseContext.ipcInVideoPrm[CAPTURE_OUT_M3_LINK_IDX].inQueParams.prevLinkId      = gCarDVRUsecaseContext.ipcOutVpssId[CAPTURE_OUT_M3_LINK_IDX];
    gCarDVRUsecaseContext.ipcInVideoPrm[CAPTURE_OUT_M3_LINK_IDX].inQueParams.prevLinkQueId   = 0;
    gCarDVRUsecaseContext.ipcOutVpssPrm[CAPTURE_OUT_M3_LINK_IDX].notifyPrevLink            = TRUE;
    gCarDVRUsecaseContext.ipcOutVpssPrm[CAPTURE_OUT_M3_LINK_IDX].notifyNextLink            = TRUE;
    gCarDVRUsecaseContext.ipcOutVpssPrm[CAPTURE_OUT_M3_LINK_IDX].notifyProcessLink         = FALSE;
    gCarDVRUsecaseContext.ipcOutVpssPrm[CAPTURE_OUT_M3_LINK_IDX].noNotifyMode              = FALSE;
    gCarDVRUsecaseContext.ipcOutVpssPrm[CAPTURE_OUT_M3_LINK_IDX].numOutQue                 = 1;

    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.ipcOutVpssId[CAPTURE_OUT_M3_LINK_IDX], &gCarDVRUsecaseContext.ipcOutVpssPrm[CAPTURE_OUT_M3_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.ipcOutVpssPrm[CAPTURE_OUT_M3_LINK_IDX]));

    gCarDVRUsecaseContext.ipcInVideoPrm[CAPTURE_OUT_M3_LINK_IDX].outQueParams[0].nextLink    = gCarDVRUsecaseContext.mergeId[PRE_ENCODE_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[PRE_ENCODE_MERGE_LINK_IDX].inQueParams[PRE_ENCODE_MERGE_LINK_IPCFRAMESIN_QIDX].prevLinkId = gCarDVRUsecaseContext.ipcInVideoId[CAPTURE_OUT_M3_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[PRE_ENCODE_MERGE_LINK_IDX].inQueParams[PRE_ENCODE_MERGE_LINK_IPCFRAMESIN_QIDX].prevLinkQueId = 0;
    gCarDVRUsecaseContext.ipcInVideoPrm[CAPTURE_OUT_M3_LINK_IDX].notifyPrevLink              = TRUE;
    gCarDVRUsecaseContext.ipcInVideoPrm[CAPTURE_OUT_M3_LINK_IDX].notifyNextLink              = TRUE;
    gCarDVRUsecaseContext.ipcInVideoPrm[CAPTURE_OUT_M3_LINK_IDX].noNotifyMode                = FALSE;
    gCarDVRUsecaseContext.ipcInVideoPrm[CAPTURE_OUT_M3_LINK_IDX].numOutQue                   = 1;

    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.ipcInVideoId[CAPTURE_OUT_M3_LINK_IDX], &gCarDVRUsecaseContext.ipcInVideoPrm[CAPTURE_OUT_M3_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.ipcInVideoPrm[CAPTURE_OUT_M3_LINK_IDX]));

    multich_cardvr_set_ipcbitsout_hlos_prms(&gCarDVRUsecaseContext.ipcBitsOutHostPrm);
    gCarDVRUsecaseContext.ipcBitsOutHostPrm.baseCreateParams.outQueParams[0].nextLink = gVdecModuleContext.ipcBitsInRTOSId;
    gCarDVRUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkId       = gVdecModuleContext.ipcBitsOutHLOSId;
    gCarDVRUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.inQueParams.prevLinkQueId    = 0;

    MULTICH_CARDVR_CREATE_LINK(gVdecModuleContext.ipcBitsOutHLOSId,
                      &gCarDVRUsecaseContext.ipcBitsOutHostPrm,
                      sizeof(gCarDVRUsecaseContext.ipcBitsOutHostPrm));

    gCarDVRUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.numOutQue                    = 1;
    gCarDVRUsecaseContext.ipcBitsInVideoPrm.baseCreateParams.outQueParams[0].nextLink     = gVdecModuleContext.decId;
    gCarDVRUsecaseContext.decPrm.inQueParams.prevLinkId    = gVdecModuleContext.ipcBitsInRTOSId;
    gCarDVRUsecaseContext.decPrm.inQueParams.prevLinkQueId = 0;
    MultiCh_ipcBitsInitCreateParams_BitsInRTOS(&gCarDVRUsecaseContext.ipcBitsInVideoPrm, TRUE);

    MULTICH_CARDVR_CREATE_LINK(gVdecModuleContext.ipcBitsInRTOSId,
                      &gCarDVRUsecaseContext.ipcBitsInVideoPrm,
                      sizeof(gCarDVRUsecaseContext.ipcBitsInVideoPrm));
    multich_cardvr_set_declink_prms(&gCarDVRUsecaseContext.decPrm);
    gCarDVRUsecaseContext.decPrm.outQueParams.nextLink  = gCarDVRUsecaseContext.ipcOutVideoId;
    gCarDVRUsecaseContext.ipcOutVideoPrm.inQueParams.prevLinkId    = gVdecModuleContext.decId;
    gCarDVRUsecaseContext.ipcOutVideoPrm.inQueParams.prevLinkQueId = 0;

    MULTICH_CARDVR_CREATE_LINK(gVdecModuleContext.decId,
                      &gCarDVRUsecaseContext.decPrm,
                      sizeof(gCarDVRUsecaseContext.decPrm));

    gCarDVRUsecaseContext.ipcOutVideoPrm.numOutQue                 = 1;
    gCarDVRUsecaseContext.ipcOutVideoPrm.outQueParams[0].nextLink  = gCarDVRUsecaseContext.ipcInVpssId;
    gCarDVRUsecaseContext.ipcInVpssPrm.inQueParams.prevLinkId    = gCarDVRUsecaseContext.ipcOutVideoId;
    gCarDVRUsecaseContext.ipcInVpssPrm.inQueParams.prevLinkQueId = 0;
    gCarDVRUsecaseContext.ipcOutVideoPrm.notifyNextLink            = TRUE;
    gCarDVRUsecaseContext.ipcOutVideoPrm.notifyPrevLink            = TRUE;
    gCarDVRUsecaseContext.ipcOutVideoPrm.noNotifyMode              = FALSE;


    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.ipcOutVideoId,
                      &gCarDVRUsecaseContext.ipcOutVideoPrm,
                      sizeof(gCarDVRUsecaseContext.ipcOutVideoPrm));


    gCarDVRUsecaseContext.ipcInVpssPrm.numOutQue                 = 1;
    gCarDVRUsecaseContext.ipcInVpssPrm.outQueParams[0].nextLink  = gCarDVRUsecaseContext.mergeId[LIVE_PLAYBACK_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[LIVE_PLAYBACK_MERGE_LINK_IDX].inQueParams[LIVE_PLAYBACK_MERGE_LINK_PLAYBACKCH_QIDX].prevLinkId = gCarDVRUsecaseContext.ipcInVpssId;
    gCarDVRUsecaseContext.mergePrm[LIVE_PLAYBACK_MERGE_LINK_IDX].inQueParams[LIVE_PLAYBACK_MERGE_LINK_PLAYBACKCH_QIDX].prevLinkQueId = 0;
    gCarDVRUsecaseContext.ipcInVpssPrm.notifyNextLink            = TRUE;
    gCarDVRUsecaseContext.ipcInVpssPrm.notifyPrevLink            = TRUE;
    gCarDVRUsecaseContext.ipcInVpssPrm.noNotifyMode              = FALSE;

    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.ipcInVpssId,
                      &gCarDVRUsecaseContext.ipcInVpssPrm,
                      sizeof(gCarDVRUsecaseContext.ipcInVpssPrm));

    gCarDVRUsecaseContext.mergePrm[LIVE_PLAYBACK_MERGE_LINK_IDX].notifyNextLink                  = TRUE;
    gCarDVRUsecaseContext.mergePrm[LIVE_PLAYBACK_MERGE_LINK_IDX].numInQue = LIVE_PLAYBACK_MERGE_LINK_NUM_INQUE;
    gCarDVRUsecaseContext.mergePrm[LIVE_PLAYBACK_MERGE_LINK_IDX].outQueParams.nextLink  = gVdisModuleContext.swMsId[2];
    gCarDVRUsecaseContext.swMsPrm[2].inQueParams.prevLinkId = gCarDVRUsecaseContext.mergeId[LIVE_PLAYBACK_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.swMsPrm[2].inQueParams.prevLinkQueId = 0;

    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.mergeId[LIVE_PLAYBACK_MERGE_LINK_IDX],
                      &gCarDVRUsecaseContext.mergePrm[LIVE_PLAYBACK_MERGE_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.mergePrm[LIVE_PLAYBACK_MERGE_LINK_IDX]));

    mulich_cardvr_set_avsync_prm(&gCarDVRUsecaseContext.avsyncPrms,
                                 gCarDVRUsecaseContext.swMsPrm[2].inQueParams.prevLinkId,
                                 gCarDVRUsecaseContext.swMsPrm[2].inQueParams.prevLinkQueId,
                                 2);

    multich_cardvr_set_swms_prm(&gCarDVRUsecaseContext.swMsPrm[2],2);
    gCarDVRUsecaseContext.swMsPrm[2].outQueParams.nextLink      = gCarDVRUsecaseContext.mergeId[PRE_DISPLAY_SELECT_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX].numInQue = PRE_DISPLAY_SELECT_MERGE_LINK_NUM_INQUE;
    gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX].inQueParams[PRE_DISPLAY_SELECT_MERGE_LINK_DISPLAYSWMS_QIDX].prevLinkId = gVdisModuleContext.swMsId[2];
    gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX].inQueParams[PRE_DISPLAY_SELECT_MERGE_LINK_DISPLAYSWMS_QIDX].prevLinkQueId = 0;

    MULTICH_CARDVR_CREATE_LINK(gVdisModuleContext.swMsId[2],
                      &gCarDVRUsecaseContext.swMsPrm[2],
                      sizeof(gCarDVRUsecaseContext.swMsPrm[2]));


    gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX].notifyNextLink = TRUE;
    gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX].numInQue = PRE_DISPLAY_SELECT_MERGE_LINK_NUM_INQUE;
    gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX].outQueParams.nextLink = gCarDVRUsecaseContext.selectLinkId[DISPLAY_SELECT_LINK_IDX];
    gCarDVRUsecaseContext.selectPrm[DISPLAY_SELECT_LINK_IDX].inQueParams.prevLinkId = gCarDVRUsecaseContext.mergeId[PRE_DISPLAY_SELECT_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.selectPrm[DISPLAY_SELECT_LINK_IDX].inQueParams.prevLinkQueId = 0;
    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.mergeId[PRE_DISPLAY_SELECT_MERGE_LINK_IDX],
                      &gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.mergePrm[PRE_DISPLAY_SELECT_MERGE_LINK_IDX]));

    multich_cardvr_set_pre_display_select_prm(&gCarDVRUsecaseContext.selectPrm[DISPLAY_SELECT_LINK_IDX]);
    gCarDVRUsecaseContext.selectPrm[DISPLAY_SELECT_LINK_IDX].outQueParams[0].nextLink =  gVcapModuleContext.ipcFramesOutVpssId[OSD_FRAMESOUT_LINK_IDX];
    gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX].baseCreateParams.inQueParams.prevLinkId   = gCarDVRUsecaseContext.selectLinkId[DISPLAY_SELECT_LINK_IDX];
    gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX].baseCreateParams.inQueParams.prevLinkQueId = 0;
    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.selectLinkId[DISPLAY_SELECT_LINK_IDX],
                      &gCarDVRUsecaseContext.selectPrm[DISPLAY_SELECT_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.selectPrm[DISPLAY_SELECT_LINK_IDX]));
    /* Redirect to DSP for OSD */
    gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX].baseCreateParams.outQueParams[0].nextLink  = gCarDVRUsecaseContext.dupId[DISPLAY_ENCODE_DUP_LINK_IDX];
    gCarDVRUsecaseContext.dupPrm[DISPLAY_ENCODE_DUP_LINK_IDX].inQueParams.prevLinkId = gVcapModuleContext.ipcFramesOutVpssId[OSD_FRAMESOUT_LINK_IDX];
    gCarDVRUsecaseContext.dupPrm[DISPLAY_ENCODE_DUP_LINK_IDX].inQueParams.prevLinkQueId      = 0;
    gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX].baseCreateParams.processLink               = gVcapModuleContext.ipcFramesInDspId[0];
    gCarDVRUsecaseContext.ipcFramesInDspPrm.baseCreateParams.inQueParams.prevLinkId      = gVcapModuleContext.ipcFramesOutVpssId[OSD_FRAMESOUT_LINK_IDX];
    gCarDVRUsecaseContext.ipcFramesInDspPrm.baseCreateParams.inQueParams.prevLinkQueId   = 0;
    gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX].baseCreateParams.notifyPrevLink            = TRUE;
    gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX].baseCreateParams.notifyNextLink            = TRUE;
    gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX].baseCreateParams.notifyProcessLink         = TRUE;
    gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX].baseCreateParams.noNotifyMode              = FALSE;
    gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX].baseCreateParams.numOutQue                 = 1;

    MULTICH_CARDVR_CREATE_LINK(gVcapModuleContext.ipcFramesOutVpssId[OSD_FRAMESOUT_LINK_IDX],
                      &gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.ipcFramesOutVpssPrm[OSD_FRAMESOUT_LINK_IDX]));

    gCarDVRUsecaseContext.ipcFramesInDspPrm.baseCreateParams.outQueParams[0].nextLink    = gVcapModuleContext.dspAlgId[0];
    gCarDVRUsecaseContext.dspAlgPrm.inQueParams.prevLinkId = gVcapModuleContext.ipcFramesInDspId[0];
    gCarDVRUsecaseContext.dspAlgPrm.inQueParams.prevLinkQueId = 0;
    gCarDVRUsecaseContext.ipcFramesInDspPrm.baseCreateParams.notifyPrevLink              = TRUE;
    gCarDVRUsecaseContext.ipcFramesInDspPrm.baseCreateParams.notifyNextLink              = TRUE;
    gCarDVRUsecaseContext.ipcFramesInDspPrm.baseCreateParams.noNotifyMode                = FALSE;
    gCarDVRUsecaseContext.ipcFramesInDspPrm.baseCreateParams.numOutQue                   = 1;

    MULTICH_CARDVR_CREATE_LINK(gVcapModuleContext.ipcFramesInDspId[0],
                      &gCarDVRUsecaseContext.ipcFramesInDspPrm,
                      sizeof(gCarDVRUsecaseContext.ipcFramesInDspPrm));

    multich_cardvr_set_osd_prm(&gCarDVRUsecaseContext.dspAlgPrm);
    MULTICH_CARDVR_CREATE_LINK(gVcapModuleContext.dspAlgId[0],
                      &gCarDVRUsecaseContext.dspAlgPrm,
                      sizeof(gCarDVRUsecaseContext.dspAlgPrm));

    gCarDVRUsecaseContext.dupPrm[DISPLAY_ENCODE_DUP_LINK_IDX].numOutQue                      = 2;
    gCarDVRUsecaseContext.dupPrm[DISPLAY_ENCODE_DUP_LINK_IDX].outQueParams[0].nextLink       = Vdis_getDisplayId(VDIS_DEV_SD);
    gCarDVRUsecaseContext.displayPrm[VDIS_DEV_SD].inQueParams[0].prevLinkId    = gCarDVRUsecaseContext.dupId[DISPLAY_ENCODE_DUP_LINK_IDX];
    gCarDVRUsecaseContext.displayPrm[VDIS_DEV_SD].inQueParams[0].prevLinkQueId = 0;
    gCarDVRUsecaseContext.dupPrm[DISPLAY_ENCODE_DUP_LINK_IDX].outQueParams[1].nextLink       = gCarDVRUsecaseContext.ipcOutVpssId[DISPLAY_OUT_M3_LINK_IDX];
    gCarDVRUsecaseContext.ipcOutVpssPrm[DISPLAY_OUT_M3_LINK_IDX].inQueParams.prevLinkId       = gCarDVRUsecaseContext.dupId[DISPLAY_ENCODE_DUP_LINK_IDX];
    gCarDVRUsecaseContext.ipcOutVpssPrm[DISPLAY_OUT_M3_LINK_IDX].inQueParams.prevLinkQueId    = 1;
    gCarDVRUsecaseContext.dupPrm[DISPLAY_ENCODE_DUP_LINK_IDX].notifyNextLink                 = TRUE;

    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.dupId[DISPLAY_ENCODE_DUP_LINK_IDX],
                      &gCarDVRUsecaseContext.dupPrm[DISPLAY_ENCODE_DUP_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.dupPrm[DISPLAY_ENCODE_DUP_LINK_IDX]));

    gCarDVRUsecaseContext.ipcOutVpssPrm[DISPLAY_OUT_M3_LINK_IDX].numOutQue                    = 1;
    gCarDVRUsecaseContext.ipcOutVpssPrm[DISPLAY_OUT_M3_LINK_IDX].outQueParams[0].nextLink     = gCarDVRUsecaseContext.ipcInVideoId[DISPLAY_OUT_M3_LINK_IDX];
    gCarDVRUsecaseContext.ipcInVideoPrm[DISPLAY_OUT_M3_LINK_IDX].inQueParams.prevLinkId       = gCarDVRUsecaseContext.ipcOutVpssId[DISPLAY_OUT_M3_LINK_IDX];
    gCarDVRUsecaseContext.ipcInVideoPrm[DISPLAY_OUT_M3_LINK_IDX].inQueParams.prevLinkQueId    = 0;
    gCarDVRUsecaseContext.ipcOutVpssPrm[DISPLAY_OUT_M3_LINK_IDX].notifyNextLink               = TRUE;
    gCarDVRUsecaseContext.ipcOutVpssPrm[DISPLAY_OUT_M3_LINK_IDX].notifyPrevLink               = TRUE;
    gCarDVRUsecaseContext.ipcOutVpssPrm[DISPLAY_OUT_M3_LINK_IDX].noNotifyMode                 = FALSE;

    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.ipcOutVpssId[DISPLAY_OUT_M3_LINK_IDX],
                      &gCarDVRUsecaseContext.ipcOutVpssPrm[DISPLAY_OUT_M3_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.ipcOutVpssPrm[DISPLAY_OUT_M3_LINK_IDX]));

    gCarDVRUsecaseContext.ipcInVideoPrm[DISPLAY_OUT_M3_LINK_IDX].numOutQue                    = 1;
    gCarDVRUsecaseContext.ipcInVideoPrm[DISPLAY_OUT_M3_LINK_IDX].outQueParams[0].nextLink     = gCarDVRUsecaseContext.mergeId[PRE_ENCODE_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[PRE_ENCODE_MERGE_LINK_IDX].inQueParams[PRE_ENCODE_MERGE_LINK_IPCINM3_QIDX].prevLinkId = gCarDVRUsecaseContext.ipcInVideoId[DISPLAY_OUT_M3_LINK_IDX];
    gCarDVRUsecaseContext.mergePrm[PRE_ENCODE_MERGE_LINK_IDX].inQueParams[PRE_ENCODE_MERGE_LINK_IPCINM3_QIDX].prevLinkQueId = 0;
    gCarDVRUsecaseContext.ipcInVideoPrm[DISPLAY_OUT_M3_LINK_IDX].notifyNextLink               = TRUE;
    gCarDVRUsecaseContext.ipcInVideoPrm[DISPLAY_OUT_M3_LINK_IDX].notifyPrevLink               = TRUE;
    gCarDVRUsecaseContext.ipcInVideoPrm[DISPLAY_OUT_M3_LINK_IDX].noNotifyMode                 = FALSE;

    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.ipcInVideoId[DISPLAY_OUT_M3_LINK_IDX],
                      &gCarDVRUsecaseContext.ipcInVideoPrm[DISPLAY_OUT_M3_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.ipcInVideoPrm[DISPLAY_OUT_M3_LINK_IDX]));

    gCarDVRUsecaseContext.mergePrm[PRE_ENCODE_MERGE_LINK_IDX].notifyNextLink                  = TRUE;
    gCarDVRUsecaseContext.mergePrm[PRE_ENCODE_MERGE_LINK_IDX].numInQue = PRE_ENCODE_MERGE_LINK_NUM_INQUE;
    gCarDVRUsecaseContext.mergePrm[PRE_ENCODE_MERGE_LINK_IDX].outQueParams.nextLink  = gVencModuleContext.encId;
    gCarDVRUsecaseContext.encPrm.inQueParams.prevLinkId    = gCarDVRUsecaseContext.mergeId[PRE_ENCODE_MERGE_LINK_IDX];
    gCarDVRUsecaseContext.encPrm.inQueParams.prevLinkQueId = 0;

    MULTICH_CARDVR_CREATE_LINK(gCarDVRUsecaseContext.mergeId[PRE_ENCODE_MERGE_LINK_IDX],
                      &gCarDVRUsecaseContext.mergePrm[PRE_ENCODE_MERGE_LINK_IDX],
                      sizeof(gCarDVRUsecaseContext.mergePrm[PRE_ENCODE_MERGE_LINK_IDX]));

    multich_cardvr_set_enclink_prm(&gCarDVRUsecaseContext.encPrm);
    gCarDVRUsecaseContext.encPrm.outQueParams.nextLink     = gVencModuleContext.ipcBitsOutRTOSId;
    gCarDVRUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkId = gVencModuleContext.encId;
    gCarDVRUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;

    MULTICH_CARDVR_CREATE_LINK(gVencModuleContext.encId,
                      &gCarDVRUsecaseContext.encPrm,
                      sizeof(gCarDVRUsecaseContext.encPrm));


    gCarDVRUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.numOutQue                 = 1;
    gCarDVRUsecaseContext.ipcBitsOutVideoPrm.baseCreateParams.outQueParams[0].nextLink = gVencModuleContext.ipcBitsInHLOSId;
    MultiCh_ipcBitsInitCreateParams_BitsOutRTOS(&gCarDVRUsecaseContext.ipcBitsOutVideoPrm,
                                               TRUE);
    MULTICH_CARDVR_CREATE_LINK(gVencModuleContext.ipcBitsOutRTOSId,
                      &gCarDVRUsecaseContext.ipcBitsOutVideoPrm,
                      sizeof(gCarDVRUsecaseContext.ipcBitsOutVideoPrm));

    gCarDVRUsecaseContext.ipcBitsInHostPrm.baseCreateParams.inQueParams.prevLinkId = gVencModuleContext.ipcBitsOutRTOSId;
    gCarDVRUsecaseContext.ipcBitsInHostPrm.baseCreateParams.inQueParams.prevLinkQueId = 0;
    MultiCh_ipcBitsInitCreateParams_BitsInHLOS(&gCarDVRUsecaseContext.ipcBitsInHostPrm);

    MULTICH_CARDVR_CREATE_LINK(gVencModuleContext.ipcBitsInHLOSId,
                      &gCarDVRUsecaseContext.ipcBitsInHostPrm,
                      sizeof(gCarDVRUsecaseContext.ipcBitsInHostPrm));

    multich_cardvr_set_display_prms(&gCarDVRUsecaseContext.displayPrm[VDIS_DEV_SD]);

    MULTICH_CARDVR_CREATE_LINK(Vdis_getDisplayId(VDIS_DEV_SD),
                      &gCarDVRUsecaseContext.displayPrm[VDIS_DEV_SD],
                      sizeof(gCarDVRUsecaseContext.displayPrm[VDIS_DEV_SD]));




}


static
void multich_cardvr_set_dec2disp_chmap()
{
    MergeLink_InLinkChInfo inChInfo;

    MergeLink_InLinkChInfo_Init(&inChInfo);
    inChInfo.inLinkID = gCarDVRUsecaseContext.ipcInVpssId;
    System_linkControl(gCarDVRUsecaseContext.mergeId[LIVE_PLAYBACK_MERGE_LINK_IDX],
                       MERGE_LINK_CMD_GET_INPUT_LINK_CHINFO,
                       &inChInfo,
                       sizeof(inChInfo),
                       TRUE);
    OSA_assert(inChInfo.numCh == gVdecModuleContext.vdecConfig.numChn);

    MultiCh_setDec2DispMap(VDIS_DEV_SD,gVdecModuleContext.vdecConfig.numChn,0,inChInfo.startChNum);
}


Void MultiCh_createCarDVR()
{
    printf("\n********* Entered usecase D1 + 4 CIF CarDVR usecase \n\n");

    MultiCh_detectBoard();

    System_linkControl(
        SYSTEM_LINK_ID_M3VPSS,
        SYSTEM_M3VPSS_CMD_RESET_VIDEO_DEVICES,
        NULL,
        0,
        TRUE
        );
    System_linkControl(
        SYSTEM_LINK_ID_M3VIDEO,
        SYSTEM_COMMON_CMD_SET_CH2IVAHD_MAP_TBL,
        &systemVid_encDecIvaChMapTbl,
        sizeof(SystemVideo_Ivahd2ChMap_Tbl),
        TRUE
    );

    multich_cardvr_reset_link_prms();
    multich_cardvr_set_link_ids();

    multich_cardvr_connect_links();
    multich_cardvr_set_links_framerate();
    multich_cardvr_set_dec2disp_chmap();
}

Void MultiCh_deleteCarDVR()
{
    UInt32 i;

    for (i = 0; i < gCarDVRUsecaseContext.createdLinkCount; i++)
    {
        System_linkDelete (gCarDVRUsecaseContext.createdLinks[i]);
    }
    gCarDVRUsecaseContext.createdLinkCount = 0;
    multich_cardvr_reset_link_ids();

    Vcap_deleteVideoDecoder();
    /* Print the HWI, SWI and all tasks load */
    /* Reset the accumulated timer ticks */
    MultiCh_prfLoadCalcEnable(FALSE, TRUE, FALSE);
}

Void MultiCh_carDVRSwitchDisplayCh(Int displayChId)
{
    SelectLink_OutQueChInfo selectChInfo;


    if (displayChId < MULTICH_CAR_DVR_USECASE_NUM_DISPLAY_CH)
    {
        Int i;
        Int32 status;

        if (displayChId == 1)
        {
            selectChInfo.outQueId = 0;
            selectChInfo.numOutCh = 1;
            selectChInfo.inChNum[0] = PRE_DISPLAY_SELECT_MERGE_LINK_D1DEILIVE_QIDX;

            status = System_linkControl(gCarDVRUsecaseContext.selectLinkId[DISPLAY_SELECT_LINK_IDX],
                                        SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                        &selectChInfo,
                                        sizeof(selectChInfo),
                                        TRUE);

            OSA_assert(status == OSA_SOK);
        }
        else
        {
            /* Channel 0: 1x1 playback channel          swms display
             * Channel 1: 1x1 1 D1 live preview channel swms display
             * Channel 2: 2x2 4 CIF live preview channel swms display
             */
            MultichCarDVR_LayOutId layOutId = gCarDVRUsecaseContext.displayCh2LayoutIdMap[displayChId];
            if (MULTICHCARDVR_LAYOUT_ID_1x1 == layOutId)
            {
               multich_cardvr_set_swms_singlewin_layoutprm(&gCarDVRUsecaseContext.swmsLayoutPrm,Vdis_getResolution(VDIS_DEV_SD));
               gCarDVRUsecaseContext.swmsLayoutPrm.winInfo[0].channelNum = 0;
            }
            else
            {
                multich_cardvr_set_swms_2x2_layoutprm(&gCarDVRUsecaseContext.swmsLayoutPrm,Vdis_getResolution(VDIS_DEV_SD));
            }
            for (i = 0; i < gCarDVRUsecaseContext.swmsLayoutPrm.numWin;i++)
            {
                gCarDVRUsecaseContext.swmsLayoutPrm.winInfo[i].channelNum = 1 + i;
            }
            status =
                System_linkControl(gVdisModuleContext.swMsId[2],
                                   SYSTEM_SW_MS_LINK_CMD_SWITCH_LAYOUT,
                                   &gCarDVRUsecaseContext.swmsLayoutPrm,
                                   sizeof(gCarDVRUsecaseContext.swmsLayoutPrm), TRUE);
            OSA_assert(status == OSA_SOK);
            selectChInfo.outQueId = 0;
            selectChInfo.numOutCh = 1;
            selectChInfo.inChNum[0] = PRE_DISPLAY_SELECT_MERGE_LINK_DISPLAYSWMS_QIDX;

            status = System_linkControl(gCarDVRUsecaseContext.selectLinkId[DISPLAY_SELECT_LINK_IDX],
                                        SELECT_LINK_CMD_SET_OUT_QUE_CH_INFO,
                                        &selectChInfo,
                                        sizeof(selectChInfo),
                                        TRUE);

            OSA_assert(status == OSA_SOK);
        }
    }
}

