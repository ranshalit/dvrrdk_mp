#include <print_pll.h>

// #define PRINT_RAW_CONTENTS  /* Enable this to get read values */
// #define  PRINT_MORE_INFO         /* Enable this to read more PLL values */

#define MAX_PRINT_BUF       256

int  memFd;
char printBuf[MAX_PRINT_BUF];

void printValues(char *format, ...)
{
    va_list vaArgPtr;

    va_start(vaArgPtr, format);
    vsnprintf(printBuf, MAX_PRINT_BUF, format, vaArgPtr);
    va_end(vaArgPtr);
    printf (printBuf);
}

unsigned int ReadMem32Bit(unsigned int addr)
{
    void *map_base;
    unsigned int data;
    unsigned int size;
    off_t target;
    volatile unsigned int *virt_addr;

    target = addr;
    size = 4;

    /* Map one page */
    map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, memFd, target & ~MAP_MASK);
    if(map_base == (void *) -1)
    {
        printValues ("Could not open the mem file \n");//FATAL; FATAL;
    }
    virt_addr = (unsigned int*)(map_base + (target & MAP_MASK));
    data = *virt_addr;
#ifdef  PRINT_RAW_CONTENTS
    printValues ("Phy Addr : 0x%0.8x Data : 0x%0.8x\n", target,  data);
#endif
    munmap(map_base, MAP_SIZE);
    return data;
}


#if defined(TI_814X_BUILD) || defined(TI_8107_BUILD)
int print_pll_status(char *module, unsigned int status,unsigned int  pll_base)
{
    unsigned int temp, MN2, M2N, M2, N, M;
    temp = status;
    float freq = 0;

    if(((temp >> 4) & 0x00000001) == 0x1)
    {
        MN2=ReadMem32Bit(pll_base+MN2DIV);
        M = MN2 & 0xFFF;
        M2N=ReadMem32Bit(pll_base+M2NDIV);
        if(pll_base == MODENA_PLL_BASE)
        {
            N = M2N & 0x3F;
            M2 = (M2N>>16) & 0x1F;
        }
        else
        {
            N = M2N & 0xFF;
            M2 = (M2N>>16) & 0x7F;
        }
        freq =  ( M * 20.0 ) / ( (N+1) * M2 );

        printValues("%s Freq  : %5.1f MHz ( M x 20 ) / ( (N+1) x M2 )\n", module, freq);
        printValues("\t%s_M   : %d\n", module, M);
        printValues("\t%s_N   : %d\n", module, N);
        printValues("\t%s_M2  : %d\n", module, M2);
    }
    else
    {
        printValues("%s: \t is NOT Locked \n", module);
    }
    printValues("\n");
    return freq;
}

void GetPllStatus (void)
{
    unsigned int status, value;

    printValues("\n\n--------- PLL Values ---------\n");
    status = ReadMem32Bit(MODENAPLL_STATUS);
    print_pll_status("MODENA", status, MODENA_PLL_BASE);

    status = ReadMem32Bit(DSP_PLL_BASE+STATUS);
    print_pll_status("DSP", status,DSP_PLL_BASE);

    status = ReadMem32Bit(IVA_PLL_BASE+STATUS);
    print_pll_status("IVA", status,IVA_PLL_BASE);

    status = ReadMem32Bit(ISS_PLL_BASE+STATUS);
    value = print_pll_status("ISS", status, ISS_PLL_BASE);
    printValues("M3 Freq : %d MHz\n", (value/2));

    status = ReadMem32Bit(L3_PLL_BASE+STATUS);
    print_pll_status("L3", status, L3_PLL_BASE);

    status = ReadMem32Bit(DSS_PLL_BASE+STATUS);
    print_pll_status("DSS", status, DSS_PLL_BASE);

    status = ReadMem32Bit(SGX_PLL_BASE+STATUS);
    print_pll_status("SGX", status, SGX_PLL_BASE);

    status = ReadMem32Bit(USB_PLL_BASE+STATUS);
    print_pll_status("USB", status, USB_PLL_BASE);

    status = ReadMem32Bit(VIDEO_0_PLL_BASE+STATUS);
    print_pll_status("VIDEO_0", status, VIDEO_0_PLL_BASE);

    status = ReadMem32Bit(VIDEO_1_PLL_BASE+STATUS);
    print_pll_status("VIDEO_1", status, VIDEO_1_PLL_BASE);

    status = ReadMem32Bit(HDMI_PLL_BASE+STATUS);
    print_pll_status("HDMI", status, HDMI_PLL_BASE);

    status = ReadMem32Bit(DDR_PLL_BASE+STATUS);
    print_pll_status("DDR", status, DDR_PLL_BASE);

    status = ReadMem32Bit(AUDIO_PLL_BASE+STATUS);
    print_pll_status("AUDIO", status, AUDIO_PLL_BASE);

}
#else

void getdivPLL(char *module, unsigned int pll_ctrl,unsigned int pll_div)
{
    unsigned int mult, div, pre_div;
    float freq;

    mult    = (ReadMem32Bit(pll_ctrl) & 0xFFFF0000)>>16;
    div     = (ReadMem32Bit(pll_div) & 0xFF);
    pre_div = (ReadMem32Bit(pll_ctrl) & 0xFF00)>>8;
    freq    = (27 * mult)/(div * pre_div);
    printValues("%s Freq        : %8.2f MHz (Mult %d, Div %d, PreDiv %d)\n\n",
            module, freq, mult, div, pre_div);
}

float readPLL(char *module, unsigned int pll_ctrl, unsigned int pll_freq, unsigned int pll_div, unsigned int prcm_div)
{
    unsigned int MAIN_N, PREDIV, MDIV, INTFREQ, ctrl_reg, freq_reg;
    float FRACFREQ, FREQ, IPFREQ = 0;

    ctrl_reg = ReadMem32Bit(pll_ctrl);
    if ((ctrl_reg & 0x00000008) == 0x8)
    {
        freq_reg    = ReadMem32Bit(pll_freq);
        MAIN_N      = (ctrl_reg & 0xFFFF0000) >> 16;
        PREDIV      = (ctrl_reg & 0xFF00)>>8;
        FRACFREQ    = ((float)(freq_reg & 0xFFFFFF))/ ((float) (0x1000000-1));
        INTFREQ     = (freq_reg & 0xF000000) >>24;
        FREQ        = FRACFREQ + INTFREQ;
        MDIV        = (ReadMem32Bit(pll_div) & 0xFF);
        IPFREQ      = (((float) (27 * MAIN_N * 8)) / ((float) ((PREDIV * FREQ) * MDIV))/(float)prcm_div);

        printValues("%s Freq        : %-8.2f MHz\n", module, IPFREQ);
        printValues("\tMAIN_N       : %d\n", MAIN_N);
        printValues("\tPREDIV       : %d\n", PREDIV);
        printValues("\tINTFREQ      : %d\n", INTFREQ);
        printValues("\tFRACFREQ     : %-8.2f\n", FRACFREQ);
        printValues("\tFREQ         : %-8.2f\n", FREQ);
        printValues("\tMDIV         : %d\n", MDIV);
        printValues("\tFreq Reg     : 0x%8X\n", freq_reg);
    }
    else
    {
        printValues("%s: \t is NOT Locked \n", module);
    }
    printValues("\n");
    return IPFREQ;
}

void GetPllStatus (void)
{
    float   freq_sysclk[24],
            value_freq_B3,
            value_freq_D1,
            freq_mcasp0 = 0,
            freq_mcasp1 = 0,
            freq_mcasp2 = 0,
            freq_mcbsp = 0;
    int     prcm_div[24],
            prcm_div_B3,
            prcm_div_D1,
            mcasp0_clk,
            mcasp1_clk,
            mcasp2_clk,
            mcbsp_clk;
#ifdef PRINT_MORE_INFO
    float   value_freq_C1;
    int     prcm_div_C1,
#endif

    int i;

    for (i = 0; i < 24; i++)
        prcm_div[i] = -1;

    for (i = 0; i < 24; i++)
        freq_sysclk[i] = -1;

    printValues("\n\n--------- PLL Values ---------\n");

    /* For FREQ1 */
    prcm_div[0] = (ReadMem32Bit(CM_SYSCLK1_CLKSEL) & 0x07) + 1;
    freq_sysclk[0] = readPLL("SYSCLK1 (DSP)", MAINPLL_CTRL, MAINPLL_FREQ1, MAINPLL_DIV1,  prcm_div[0]);

    /* For FREQ2 */
    prcm_div[1] = (ReadMem32Bit(CM_SYSCLK2_CLKSEL) & 0x07) + 1;
    freq_sysclk[1] = readPLL("SYSCLK2 (A8)", MAINPLL_CTRL, MAINPLL_FREQ2, MAINPLL_DIV2, prcm_div[1]);

    prcm_div[22] = (ReadMem32Bit(CM_SYSCLK23_CLKSEL) & 0x07) + 1;
    freq_sysclk[22] = readPLL("SYSCLK23 (SGX)", MAINPLL_CTRL, MAINPLL_FREQ2, MAINPLL_DIV2, prcm_div[22]);

    /* For FREQ3 */
    prcm_div[2] = (ReadMem32Bit(CM_SYSCLK3_CLKSEL) & 0x07) + 1;
    freq_sysclk[2] = readPLL("SYSCLK3 (HDVICP)", MAINPLL_CTRL, MAINPLL_FREQ3, MAINPLL_DIV3, prcm_div[2]);

    /* For FREQ4 */
    prcm_div[3] = (ReadMem32Bit(CM_SYSCLK4_CLKSEL) & 0x01) + 1;
    freq_sysclk[3] = readPLL("SYSCLK4 (L3 Fast)", MAINPLL_CTRL, MAINPLL_FREQ4, MAINPLL_DIV4, prcm_div[3]);

    prcm_div[4] = (ReadMem32Bit(CM_SYSCLK5_CLKSEL) & 0x01) + 1;
    freq_sysclk[4] = readPLL("SYSCLK5 (Ducati M3/ HDVPSS)", MAINPLL_CTRL, MAINPLL_FREQ4, MAINPLL_DIV4, prcm_div[4]);

#ifdef PRINT_MORE_INFO
    prcm_div[5] = ((ReadMem32Bit(CM_SYSCLK6_CLKSEL) & 0x01) + 1) * 2;
    freq_sysclk[5] = readPLL("SYSCLK6", MAINPLL_CTRL, MAINPLL_FREQ4, MAINPLL_DIV4, prcm_div[5]);

    prcm_div[6] = (ReadMem32Bit(CM_SYSCLK7_CLKSEL) & 0x03);
    if (prcm_div[6] == 0)
        prcm_div[6] = 5;
    else if (prcm_div[6] == 1)
        prcm_div[6] = 6;
    else if (prcm_div[6] == 2)
        prcm_div[6] = 8;
    else if (prcm_div[6] == 3)
        prcm_div[6] = 16;

    freq_sysclk[6] = readPLL("SYSCLK7", MAINPLL_CTRL, MAINPLL_FREQ4, MAINPLL_DIV4, prcm_div[6]);
#endif

    /* For FREQ5 */
    prcm_div[23] = (ReadMem32Bit(CM_SYSCLK24_CLKSEL) & 0x07) + 1;
    freq_sysclk[23] = readPLL("SYSCLK24 (EMAC)", MAINPLL_CTRL, MAINPLL_FREQ5, MAINPLL_DIV5, prcm_div[23]);

    getdivPLL("USB", MAINPLL_CTRL, MAINPLL_DIV6);
    getdivPLL("AUDIO PLL Reference clock", MAINPLL_CTRL, MAINPLL_DIV7);

#ifdef PRINT_MORE_INFO
    prcm_div[9] = (ReadMem32Bit(CM_SYSCLK10_CLKSEL) & 0x01) + 1;
    freq_sysclk[9] = readPLL("SYSCLK10 (SPI,I2C,SD,UART)", DDRPLL_CTRL, DDRPLL_FREQ2, DDRPLL_DIV2, prcm_div[9]);

    freq_sysclk[8] = readPLL("SYSCLK9 (CEC CLOCK)", DDRPLL_CTRL, DDRPLL_FREQ2, DDRPLL_DIV2, 3);

    freq_sysclk[7] = readPLL("SYSCLK8", DDRPLL_CTRL, DDRPLL_FREQ3, DDRPLL_DIV3, 1); /* default and only value of divider is 1 */
    getdivPLL("DDR", DDRPLL_CTRL, DDRPLL_DIV1);
#endif

    /********** Video *************/
    prcm_div[10] = (ReadMem32Bit(CM_SYSCLK11_CLKSEL) & 0x01) + 1;
    freq_sysclk[10] = readPLL("SYSCLK11 (Video)", VIDEOPLL_CTRL, VIDEOPLL_FREQ1, VIDEOPLL_DIV1, prcm_div[10]);

    prcm_div_D1 = (ReadMem32Bit(CM_VPD1_CLKSEL) & 0x07) + 1;
    value_freq_D1 = (freq_sysclk[10] * prcm_div[10]) / ((float) prcm_div_D1);

    /* For FREQ2 */
    prcm_div[12] = (ReadMem32Bit(CM_SYSCLK13_CLKSEL) & 0x07) + 1;
    freq_sysclk[12] = readPLL("SYSCLK13 (HD_VENC_D_CLK)", VIDEOPLL_CTRL, VIDEOPLL_FREQ2, VIDEOPLL_DIV2, prcm_div[12]);

    prcm_div_B3 = (ReadMem32Bit(CM_VPB3_CLKSEL) & 0x03);
    if (prcm_div_B3 == 0)
        prcm_div_B3 = 1;
    else if (prcm_div_B3 == 1)
        prcm_div_B3 = 2;
    if (prcm_div_B3 == 2)
        prcm_div_B3 = 22;

    value_freq_B3 = (freq_sysclk[12]*prcm_div[12]) / ((float) prcm_div_B3);

    /* For FREQ3 */
    prcm_div[14] = (ReadMem32Bit(CM_SYSCLK15_CLKSEL) & 0x07) + 1;
    freq_sysclk[14] = readPLL("SYSCLK15 (HD_VENC_A_CLK)", VIDEOPLL_CTRL, VIDEOPLL_FREQ3, VIDEOPLL_DIV3, prcm_div[14]);

#ifdef PRINT_MORE_INFO
    prcm_div_C1 = (ReadMem32Bit(CM_VPC1_CLKSEL) & 0x03);
    if (prcm_div_C1 == 0)
        prcm_div_C1 = 1;
    else if (prcm_div_C1 == 1)
        prcm_div_C1 = 2;
    if (prcm_div_C1 == 2)
        prcm_div_C1 = 22;

    value_freq_C1 = freq_sysclk[14] / ((float) prcm_div_C1);


    prcm_div[13] = (ReadMem32Bit(CM_SYSCLK14_CLKSEL) & 0x03);
    if (prcm_div[13] == 0)
        freq_sysclk[13] = value_freq_B3;
    else if (prcm_div[13] == 1)
        freq_sysclk[13] = CLKIN;
    else if (prcm_div[13] == 2)
        freq_sysclk[13] = value_freq_C1;
    printValues("SYSCLK14 : %f Mhz\n", freq_sysclk[13]);

    prcm_div[15] = (ReadMem32Bit(CM_SYSCLK16_CLKSEL) & 0x01);
    if (prcm_div[15] == 0)
        freq_sysclk[15] = value_freq_D1;
    else if (prcm_div[15] == 1)
        freq_sysclk[15] = value_freq_B3;

    printValues("SYSCLK16 : %f Mhz\n", freq_sysclk[15]);
#endif

    prcm_div[18] = (ReadMem32Bit(CM_SYSCLK19_CLKSEL) & 0x07) + 1;
    freq_sysclk[18] = readPLL("SYSCLK19 (Audio)", AUDIOPLL_CTRL, AUDIOPLL_FREQ2, AUDIOPLL_DIV2,  prcm_div[18]);

    prcm_div[19] = (ReadMem32Bit(CM_SYSCLK20_CLKSEL) & 0x07) + 1;
    freq_sysclk[19] = readPLL("SYSCLK20 (Audio)", AUDIOPLL_CTRL, AUDIOPLL_FREQ3, AUDIOPLL_DIV3, prcm_div[19]);

    prcm_div[20] = (ReadMem32Bit(CM_SYSCLK21_CLKSEL) & 0x07) + 1;
    freq_sysclk[20] = readPLL("SYSCLK21 (Audio)", AUDIOPLL_CTRL, AUDIOPLL_FREQ4, AUDIOPLL_DIV4, prcm_div[20]);

    prcm_div[21] = (ReadMem32Bit(CM_SYSCLK22_CLKSEL) & 0x07) + 1;
    freq_sysclk[21] = readPLL("SYSCLK22 (Audio)", AUDIOPLL_CTRL, AUDIOPLL_FREQ5, AUDIOPLL_DIV5,  prcm_div[21]);

    mcasp0_clk = ReadMem32Bit(CM_AUDIOCLK_MCASP0_CLKSEL) & 0x03;
    switch(mcasp0_clk)
    {
       case 0:
        freq_mcasp0 = freq_sysclk[19];break;
       case 1:
        freq_mcasp0 = freq_sysclk[20];break;
       case 2:
        freq_mcasp0 = freq_sysclk[21];break;
    }
    printValues("MCASP0 CLK = %f\n", freq_mcasp0);

    mcasp1_clk = ReadMem32Bit(CM_AUDIOCLK_MCASP1_CLKSEL) & 0x03;
    switch(mcasp1_clk)
    {
       case 0:
        freq_mcasp1 = freq_sysclk[19];break;
       case 1:
        freq_mcasp1 = freq_sysclk[20];break;
       case 2:
        freq_mcasp1 = freq_sysclk[21];break;
    }
    printValues("MCASP1 CLK = %f\n", freq_mcasp1);

    mcasp2_clk = ReadMem32Bit(CM_AUDIOCLK_MCASP2_CLKSEL) & 0x03;
    switch(mcasp2_clk)
    {
       case 0:
        freq_mcasp2 = freq_sysclk[19];break;
       case 1:
        freq_mcasp2 = freq_sysclk[20];break;
       case 2:
        freq_mcasp2 = freq_sysclk[21];break;
    }
    printValues("MCASP2 CLK = %f\n", freq_mcasp2);

    mcbsp_clk = ReadMem32Bit(CM_AUDIOCLK_MCBSP_CLKSEL) & 0x03;
    switch(mcbsp_clk)
    {
       case 0:
        freq_mcbsp = freq_sysclk[19];break;
       case 1:
        freq_mcbsp = freq_sysclk[20];break;
       case 2:
        freq_mcbsp = freq_sysclk[21];break;
    }
    printValues("MCBSP CLK = %f\n", freq_mcbsp);
}
#endif

int main(int argc, char **argv)
{
    if((memFd = open("/dev/mem", O_RDWR | O_SYNC)) == -1)
    {
        printValues ("Could not open /dev/mem - exiting...\n");
        exit(0);
    }
    printValues("/dev/mem opened.\n");

    GetPllStatus ();
    close(memFd);

    return 0;
}


