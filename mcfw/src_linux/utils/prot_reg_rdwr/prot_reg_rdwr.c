/*
 * oma-reg-debug: debugfs interface for registers
 *
 * debugfs interface for accessing all memory locations in supervisor mode
 *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Author: Mansoor Ahamed <mansoor.ahamed@ti.com>
 *
 * NOTE: this module should work on all 32-bit CPU architectures.
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/clk.h>
#include <linux/io.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/debugfs.h>

#define MAXCOLUMN 100 /* for short messages */

static DEFINE_MUTEX(prot_reg_debug_lock);
static struct dentry *prot_reg_debug_root;
static char *read_buf;
static ssize_t read_count;

static int __prot_reg_do(int write, char *buf)
{
	int err = 0;
	int ram = 0;
	int i, j;
	char c;
	unsigned int start;
	unsigned int data;
	ssize_t count;
	void __iomem *base;
	void __iomem *ptr;
	unsigned int pfn;
	char *p = read_buf;

	sscanf(buf, "%c %x %x %d", &c, &start, &data, &count);

	/* we should not ioremap() RAM which is visible to Linux.
	 * see file arch/arm/mm/ioremap.c function __arm_ioremap_pfn_caller()
	 * for more information.
	 */
	pfn = __phys_to_pfn(start);
	if (pfn_valid(pfn))
		ram = 1;

	if (ram) {
		base = __va(start);
	} else {
		base = ioremap_nocache(start, SZ_4K);
		if (IS_ERR(base)) {
			err = -ENOMEM;
			goto err_out;
		}
	}

	ptr = base;

	if (write) {
		if (count <= 0) {
			*(unsigned int *)ptr = data;
		} else {
			for (i = 0; i < count; i++) {
				*(unsigned int *)ptr = data;
				ptr += 4;
			}
		}
		read_count = 0;
	} else { /* read */
		/* read takes only 3 args */
		count = data;

		p += sprintf(p, "\n----------- H E X     V I E W -----------");
		p += sprintf(p, "\nAddress\t\t\t\tData\n");
		p += sprintf(p, "------------------------------------------\n");
		p += sprintf(p, "%4.4x:%4.4x\t:\t",
			((start&0xFFFF0000) >> 16), (start&0xFFFF));
		for (j = 0; j < count; j++) {
			if ((j%4 == 0) && (j != 0)) {
				p += sprintf(p, "\n%4.4x:%4.4x\t:\t",
					(((start+(j*4))&0xFFFF0000) >> 16),
						((start+(j*4))&0xFFFF));
			}
			p += sprintf(p, "%8.8x  ",
				*(unsigned int *)(ptr + j  * 4));
			if (j == count) {
				p += sprintf(p, "\n");
				break;
			}

		}
		p += sprintf(p, "\n----------------------------------------\n");
		read_count = p - read_buf;
		printk(KERN_EMERG "%s", read_buf);
	}

	if (!ram)
		iounmap(base);

	return 0;
err_out:
	return err;
}

static ssize_t prot_reg_debug_read_mem(struct file *file,
		char __user *userbuf, size_t count, loff_t *ppos)
{
	ssize_t bytes = 0;
	if (read_count) {
		bytes = simple_read_from_buffer(userbuf, count, ppos,
				(const void *)read_buf, read_count);
	}

	return bytes;
}

static ssize_t prot_reg_debug_write_mem(struct file *file,
	const char __user *userbuf, size_t count, loff_t *ppos)
{
	char *p, *buf;
	int err;

	count = min_t(size_t, count, PAGE_SIZE);

	buf = (char *)__get_free_page(GFP_KERNEL);
	if (!buf)
		return -ENOMEM;
	p = buf;

	mutex_lock(&prot_reg_debug_lock);

	if (copy_from_user(p, userbuf, count)) {
		count =  -EFAULT;
		goto err_out;
	}

	switch (p[0]) {
	case 'w':
		err = __prot_reg_do(1, p);
		if (err < 0) {
			count = err;
			goto err_out;
		}
		break;

	default:
		err = __prot_reg_do(0, p);
		if (err < 0) {
			count = err;
			goto err_out;
		}
		break;
	}

err_out:
	mutex_unlock(&prot_reg_debug_lock);
	free_page((unsigned int)buf);

	return count;
}

static int prot_reg_debug_open_generic(struct inode *inode, struct file *file)
{
	file->private_data = inode->i_private;
	return 0;
}

static const struct file_operations prot_reg_debug_fops = {
		.open = prot_reg_debug_open_generic,
		.read = prot_reg_debug_read_mem,
		.write = prot_reg_debug_write_mem,
		.llseek = generic_file_llseek,
	};

static int __init prot_reg_debug_init(void)
{
	struct dentry *dent;
	int err = 0;

	read_buf = (char *)__get_free_page(GFP_KERNEL);
	if (!read_buf)
		return -ENOMEM;

	prot_reg_debug_root = debugfs_create_dir("prot_reg", NULL);
	if (!prot_reg_debug_root)
		return -ENOMEM;

	dent = debugfs_create_file("mem", 600, prot_reg_debug_root,
		NULL, &prot_reg_debug_fops);
	if (!dent) {
		err = -ENOMEM;
		goto err_out;
	}
	read_count = 0;
	printk(KERN_INFO "Successfully registered prot_reg DebugFS entry\n");
	return 0;

err_out:
	printk(KERN_INFO "prot_reg DebugFS registration failed\n");
	free_page((unsigned int)read_buf);
	debugfs_remove_recursive(prot_reg_debug_root);
	return err;
}
module_init(prot_reg_debug_init)

static void __exit prot_reg_debugfs_exit(void)
{
	free_page((unsigned int)read_buf);
	debugfs_remove_recursive(prot_reg_debug_root);
	printk(KERN_INFO "prot_reg DebugFS removed\n");
}
module_exit(prot_reg_debugfs_exit)

MODULE_DESCRIPTION("protected register space read,write access in kernel space");
MODULE_AUTHOR("Mansoor Ahamed <mansoor.ahamed@ti.com>");
MODULE_LICENSE("GPL v2");
