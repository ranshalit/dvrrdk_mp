#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x7a0cf9e5, "module_layout" },
	{ 0x838573e1, "generic_file_llseek" },
	{ 0x2124331d, "debugfs_remove_recursive" },
	{ 0xb45ffa01, "debugfs_create_file" },
	{ 0x5278a6f4, "debugfs_create_dir" },
	{ 0x528c709d, "simple_read_from_buffer" },
	{ 0x4302d0eb, "free_pages" },
	{ 0x62b72b0d, "mutex_unlock" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xfbc74f64, "__copy_from_user" },
	{ 0xe16b893b, "mutex_lock" },
	{ 0x93fca811, "__get_free_pages" },
	{ 0x15331242, "omap_iounmap" },
	{ 0xea147363, "printk" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0xe914e41e, "strcpy" },
	{ 0xe9ce8b95, "omap_ioremap" },
	{ 0x51e77c97, "pfn_valid" },
	{ 0x42224298, "sscanf" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

