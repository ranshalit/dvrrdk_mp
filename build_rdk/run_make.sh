
function print_help {
	echo " "
	echo " Usage:"
	echo " # ./run_build.sh <platform> <make target>"
	echo " "
	echo " Valid <platform> are,"
	echo " ti816x-evm : DM816X_TI_EVM build"
	echo " ti816x-dvr : DM816X_UD_DVR build"
	echo " ti814x-evm : DM814X_TI_EVM build"
	echo " ti814x-dvr : DM814X_BCH_DVR build"
	echo " ti810x-evm : DM810X_TI_EVM build"
	echo " ti810x-dvr : DM810X_UD_DVR build"
	echo " all        : Builds DM816x DVR , DM814x EVM , DM810x DVR"
	echo " "
	echo " IMPORTANT NOTE:"
	echo " A 'clean' is required before switching from EVM to DVR on a given platform"
	echo " "
	echo " <make target> is passed directly to the 'Makefile', refer to the makefile for valid targets"
	echo " "
	echo " Example valid <make target> are,"
	echo " <none>     : Incremental Build of the platform"
	echo " clean      : Clean's the platform"
	echo " all        : Clean's and build of the platform"
	echo " "
	echo " Examples," 
	echo " ./run_build.sh ti816x-dvr   -- incrementally build DM816x DVR"
	echo " ./run_build.sh all          -- incrementally build all platforms"
	echo " ./run_build.sh all clean    -- clean all platforms"
	echo " "
}

function build_platform {
	echo "#### "
	echo "#### [$1] platform build [$2] !!!"
	echo "#### "
#	make -s $2 DVR_RDK_BOARD_TYPE=$1
	make  $2 DVR_RDK_BOARD_TYPE=$1
	if [ "$?" -eq 0 ]; then
		echo "#### [$1] platform build [$2] Done !!!"
		echo "#### "
	else
		echo "#### "
		echo "#### [$1] platform build [$2] ERROR !!!"
		echo "#### "
                exit 1
	fi
}

if [ "$1" == "all" ];
then
	echo "#### "
	echo "#### Building All platforms !!!"
	echo "#### "
	build_platform DM810X_UD_DVR $2
	build_platform DM814X_TI_EVM $2
	build_platform DM816X_UD_DVR $2
	echo "#### "
	echo "#### All platforms build [$2] Done !!!"
	echo "#### "

	if [ "$2" == "clean" ];
	then
		echo "#### "
		echo "#### Force deleting ./build and ./lib folder !!!"
		echo "#### "
		rm -rf build/ lib/
		echo "#### "
		echo "#### Force deleting Done !!!"
		echo "#### "
	fi
fi

if [ "$1" == "ti816x-dvr" ];
then
	build_platform DM816X_UD_DVR $2
fi

if [ "$1" == "ti816x-evm" ];
then
	build_platform DM816X_TI_EVM $2
fi

if [ "$1" == "ti814x-dvr" ];
then
	build_platform DM814X_BCH_DVR $2
fi

if [ "$1" == "ti814x-evm" ];
then
	build_platform DM814X_TI_EVM $2
fi

if [ "$1" == "ti810x-dvr" ];
then
	build_platform DM810X_UD_DVR $2
fi

if [ "$1" == "ti810x-evm" ];
then
	build_platform DM810X_TI_EVM $2
fi

if [ "$1" == "help" ];
then
	print_help
fi

if [ "$1" == "" ];
then
	print_help
fi

exit
