#!/bin/bash
#see the end of file for standard work flow

prefix=$(date +%Y-%m-%d-%H%M)
tarfolder=${prefix}_review
#remove this folder if it exist
rm $tarfolder -rf

gitstfile="$tarfolder/gitst.txt"
modi="modified"
orig="orig"

mkdir $tarfolder
git status > $gitstfile

##########################################################
#function to put file $1(maybe include path) into folder $2
############################################################
put1to2 () {
    if [ $# -eq 2 ];then
        dir=$tarfolder/$2/$(dirname $1)
        mkdir -p $dir
        cp $1 $dir
    else
        echo "wrong number of parameter for call put1to2,called with $#, but need 2"
        exit 1
    fi


}



#do real work to get original file and modified file
exec 3<> $gitstfile
while read line <&3
do {
    fullpath=$(echo $line | sed -n 's/^#.*new file:[ ]*//p')

    if [ ! -z "$fullpath" ];then
        #here we handle the new files
        #only need to copy them to the modified folder
        put1to2 $fullpath $modi
    else
        fullpath=$(echo $line | sed -n 's/^#.*modified:[ ]*//p')
        if [ ! -z "$fullpath" ];then
            #here we handle the modified file, copy them to modified folder
            put1to2 $fullpath $modi

            #here we get the original file and put it into original folder
            dir=$tarfolder/$orig/$(dirname $fullpath)
            mkdir -p $dir
            origfile=$tarfolder/$orig/$fullpath
            git show HEAD:$fullpath > $origfile
        else

            fullpath=$(echo $line | sed -n 's/^#.*deleted:[ ]*//p')
            if [ ! -z "$fullpath" ];then
                #here we handle the delete file
                dir=$tarfolder/$orig/$(dirname $fullpath)
                mkdir -p $dir
                origfile=$tarfolder/$orig/$fullpath
                git show HEAD:$fullpath > $origfile
            fi
        fi

    fi
}
done
exec 3>&-

echo "will create review tar ball $tarfolder"
tar zcf ${tarfolder}.tgz $tarfolder

exit 0

#Here is a recommend work flow.
#1. make a branch by “git checkout –b job1”, modify the code and test it.
#2. before you want commit, run “./review.sh”
#3. do “git commit –am xxxx” to store the change you did in job1 
#4. do “git checkout –b job2” to go on working on other issue and also wait for feedback of this change
#
#After 1 day or sometime after you got enough confidence and it’s time to commit the code do
#5. do “git stash” to store the unfinished change on job2
#6. do “git checkout –b job1” to go back to job1
#7. do “git pull” to get any of the changes other people do
#8. do “git rebase origin” to make the date base linear.
#9. change any of the conflict files.
#10. do “git commit –am xxxx” to check in the changed files
#11. do “git push origin shawn_1018:dvr_int_branch” to push it to remote branch
#12. do git checkout job2 to go back work on job2                                                    
